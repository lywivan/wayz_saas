package com.wayz.service;

import com.wayz.constants.PageVo;
import com.wayz.entity.vo.application.ApplicationVo;
import com.wayz.exception.WayzException;

/**
 * 应用服务接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
public interface ApplicationService {
    /**
     * 查询应用
     *
     * @param myId 我的标识
     * @param name 名称
     * @param starNum 星级
     * @param isEnable 是否有效
     * @return 应用列表
     */
    public PageVo<ApplicationVo> queryApplication(Long myId, String name, Integer starNum, Boolean isEnable, Integer startIndex, Integer pageSize)
            throws WayzException;

    /**
     * 获取应用
     *
     * @param myId 我的标识
     * @param id 应用标识
     * @return 应用
     * @throws YungeException 平台异常
     */
    public ApplicationVo getApplication(Long myId, Long id) throws WayzException;

    /**
     * 创建应用
     *
     * @param myId 我的标识
     * @param name 名称
     * @param icon 图标
     * @param href 页面链接
     * @param starNum 星级
     * @param description 描述
     * @param images 应用截图
     * @param isEnable 是否有效
     * @return 应用标识
     * @throws YungeException 平台异常
     */
    public Long createApplication(Long myId, String name, String icon,String href,Integer starNum,
                                  String description, String images, Boolean isEnable) throws WayzException;

    /**
     * 修改应用
     *
     * @param myId 我的标识
     * @param id 标识
     * @param name 名称
     * @param icon 图标
     * @param href 页面链接
     * @param starNum 星级
     * @param description 描述
     * @param images 应用截图
     * @param isEnable 是否有效
     * @throws YungeException 平台异常
     */
    public void modifyApplication(Long myId, Long id, String name, String icon,String href, Integer starNum,
                                  String description, String images, Boolean isEnable) throws WayzException;

    /**
     * 删除应用
     *
     * @param myId 我的标识
     * @param id 应用标识
     * @throws YungeException 平台异常
     */
    public void deleteApplication(Long myId, Long id) throws WayzException;
}
