package com.wayz.service;

import com.wayz.constants.ViewKeyValue;
import com.wayz.exception.WayzException;

import java.util.List;

/**
 * 内容渠道服务接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
public interface ChannelService {
    /**
     * 查询内容渠道视图
     *
     * @return 渠道列表
     */
    public List<ViewKeyValue> queryChannelViewList(String name) throws WayzException;
}
