package com.wayz.service;

import com.wayz.constants.PageVo;
import com.wayz.constants.ViewKeyValue;
import com.wayz.entity.vo.content.ShopContentVo;
import com.wayz.exception.WayzException;

import java.util.List;

/**
 * 商城内容服务接口
 *
 * @author wade.liu@wayz.ai
 */
public interface ContentService {
    /**
     * 文件类型
     * @param myId
     * @return
     * @throws WayzException
     */
    public List<ViewKeyValue> queryMaterialType(Long myId) throws WayzException;

    /**
     * 审核状态
     * @param myId
     * @return
     * @throws YungeException
     */
    public List<ViewKeyValue> queryAuditType(Long myId) throws WayzException;

    /**
     * 查询商城内容
     *
     * @param myId       我的标识
     * @param channelId  渠道标识
     * @param categoryId 分类标识
     * @param beginPrice 起始价格
     * @param endPrice   结束价格
     * @param type       文件类型(1:图片 2:视频 3:音频 4:网页 5:office文档)
     * @return 商城内容列表
     * @throws YungeException 平台异常
     */
    public PageVo<ShopContentVo> queryShopContent(Long myId, Long channelId, Long categoryId, Double beginPrice, Double endPrice, Short type, Integer startIndex, Integer pageSize, Short status);

    /**
     * 创建商城内容
     *
     * @param myId       我的标识
     * @param channelId  渠道标识
     * @param categoryId 分类标识
     * @param name       渠道名称
     * @param price      价格(元)
     * @param type       文件类型(1:图片 2:视频 3:音频 4:网页 5:office文档)
     * @param url        文件链接
     * @param previewUrl 预览链接
     * @param filemd5    文件检验码MD5
     * @param fileSize   文件长度(字节)
     * @param duration   时长(秒)
     * @param width      分辨率宽度
     * @param height     分辨率宽度
     * @return 内容标识
     * @throws YungeException 平台异常
     */
    public Long createShop(Long myId, Long channelId, Long categoryId, String name,String title,String description, Double price, Short type, String url, String previewUrl, String filemd5, Long fileSize, Integer duration, Integer width, Integer height) throws WayzException;

    /**
     * 删除商城内容
     *
     * @param myId 我的标识
     * @param id   商城内容标识
     * @throws WayzException 平台异常
     */
    public void deleteShop(Long myId, Long id) throws WayzException;

    /**
     * 修改商城内容
     *
     * @param myId       我的标识
     * @param id         商城内容标识
     * @param channelId  渠道标识
     * @param categoryId 分类标识
     * @param name       渠道名称
     * @param price      价格(元)
     * @param type       文件类型(1:图片 2:视频 3:音频 4:网页 5:office文档)
     * @param url        文件链接
     * @param previewUrl 预览链接
     * @param filemd5    文件检验码MD5
     * @param fileSize   文件长度(字节)
     * @param duration   时长(秒)
     * @param width      分辨率宽度
     * @param height     分辨率宽度
     * @throws YungeException 平台异常
     */
    public void modifyShop(Long myId, Long id, Long channelId, Long categoryId, String name,String title,String description,Double price, Short type,Short auditStatus, String url, String previewUrl, String filemd5, Long fileSize, Integer duration, Integer width, Integer height) throws WayzException;


    /**
     * 获取商城内容
     *
     * @param myId 我的标识
     * @param id   商城内容标识
     * @return 商城内容
     * @throws YungeException 平台异常
     */
    public ShopContentVo getShopContent(Long myId, Long id);


    /**
     * 内容审核
     * @param myId    审核人
     * @param id      主键ID
     * @param status  0:未审核; 1:审核通过; 2: 审核未通过
     * @return
     * @throws YungeException
     */
    public void auditorContent(Long myId,Long id,String remark,Short status) throws WayzException;
}
