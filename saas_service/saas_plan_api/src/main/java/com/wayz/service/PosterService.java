package com.wayz.service;

import com.wayz.constants.PageVo;
import com.wayz.constants.ViewMap;
import com.wayz.entity.dto.poster.*;
import com.wayz.entity.vo.poster.PosterCategoryVo;
import com.wayz.entity.vo.poster.PosterResolutionVo;
import com.wayz.entity.vo.poster.PosterTempletDetailVo;
import com.wayz.entity.vo.poster.PosterTempletVo;
import com.wayz.exception.WayzException;

import java.util.List;

/**
 * 海报服务接口
 * 
 */
public interface PosterService {

	/**
	 * 查询分辨率
	 * 
	 * @param myId 我的标识
	 * @return 分辨率列表
	 * @throws WayzException 平台异常
	 */
	public List<ViewMap> queryPosterResolutionView(Long myId, Long companyId) throws WayzException;

	/**
	 * 获取分辨率
	 * 
	 * @param myId 我的标识
	 * @param id 分辨率标识
	 * @return 分辨率
	 * @throws WayzException 平台异常
	 */
	public PosterResolutionVo getPosterResolution(Long myId, Long id) throws WayzException;

	/**
	 * 查询分辨率
	 * 
	 * @param myId 我的标识
	 * @return 分辨率列表
	 * @throws WayzException 平台异常
	 */
	public PageVo<PosterResolutionVo> queryPosterResolution(Long myId, PosterResolutionPageDto posterResolutionPageDto) throws WayzException;

	/**
	 * 创建分辨率
	 * 
	 * @param myId 我的标识
	 * @throws WayzException 平台异常
	 */
	public Long createPosterResolution(Long myId, PosterResolutionDto PosterResolutionDto)
			throws WayzException;

	/**
	 * 删除分辨率
	 * 
	 * @param myId 我的标识
	 * @param id 分辨率标识
	 * @throws WayzException 平台异常
	 */
	public void deletePosterResolution(Long myId, Long companyId, Long id) throws WayzException;

	/**
	 * 查询海报类别
	 * 
	 * @param myId 我的标识
	 * @param id 海报类别标识
	 * @return 海报类别列表
	 * @throws WayzException 平台异常
	 */
	public List<PosterCategoryVo> queryPosterCategory(Long myId, Long companyId, Long id) throws WayzException;

	/**
	 * 创建海报类别
	 * 
	 * @param myId 我的标识
	 * @throws WayzException 平台异常
	 */
	public Long createPosterCategory(Long myId, PosterCategoryDto posterCategoryDto) throws WayzException;

	/**
	 * 修改海报类别
	 * 
	 * @param myId 我的标识
	 * @throws WayzException 平台异常
	 */
	public void modifyPosterCategory(Long myId, PosterCategoryDto posterCategoryDto) throws WayzException;

	/**
	 * 删除海报类别
	 * 
	 * @param myId 我的标识
	 * @param id 海报类别标识
	 * @throws WayzException 平台异常
	 */
	public void deletePosterCategory(Long myId, Long id) throws WayzException;

	/**
	 * 查询海报模板
	 * 
	 * @param myId 我的标识
	 * @return 海报模板分页
	 * @throws WayzException 平台异常
	 */
	PageVo<PosterTempletVo> queryPosterTemplet(Long myId, PosterTempletPageDto posterTempletPageDto);

	/**
	 * 查询海报模板视图列表
	 * 
	 * @param myId 我的标识
	 * @return 视图列表
	 * @throws WayzException 平台异常
	 */
	public List<ViewMap> queryPosterTempletView(Long myId, Long companyId) throws WayzException;

	/**
	 * 创建海报模板
	 * 
	 * @param myId 我的标识
	 * @throws WayzException 平台异常
	 */
	public void createPosterTemplet(Long myId, PosterTempletDto posterTempletDto) throws WayzException;

	/**
	 * 修改海报模板
	 * 
	 * @param myId 我的标识
	 * @throws WayzException 平台异常
	 */
	public void modifyPosterTemplet(Long myId, PosterTempletDto posterTempletDto)
			throws WayzException;
	
	/**
	 *  更新海报模板发布状态
	 * 
	 * @param myId 我的标识
	 * @param id 模板标识
	 * @param isPublish 是否发布
	 * @throws WayzException 平台异常
	 */
	public void updatePosterTempletStatus(Long myId, Long id, Boolean isPublish)throws WayzException;
	
	/**
	 *  更新海报模板所属公司
	 * 
	 * @param myId 我的标识
	 * @param id 模板标识
	 * @param companyId 所属公司
	 * @throws WayzException 平台异常
	 */
	public void updatePosterCompanyIdByTempletId(Long myId, Long id, Long companyId)throws WayzException;

	/**
	 * 删除海报模板
	 * 
	 * @param myId 我的标识
	 * @param id 海报模板标识
	 * @throws WayzException 平台异常
	 */
	public void deletePosterTemplet(Long myId, Long id) throws WayzException;

	/**
	 * 获取海报模板详情
	 * 
	 * @param myId 我的标识
	 * @param templetId 模板标识
	 * @return 海报模板详情
	 * @throws WayzException 平台异常
	 */
	public PosterTempletDetailVo getPosterTempletDetail(Long myId, Long templetId) throws WayzException;

}
