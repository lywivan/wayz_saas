package com.wayz.service;


import com.wayz.constants.PageVo;
import com.wayz.constants.ViewKeyValue;
import com.wayz.entity.pojo.category.DContentCategory;
import com.wayz.entity.vo.category.ContentCategoryVo;
import com.wayz.exception.WayzException;

import java.util.List;

/**
 * 内容分类服务接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
public interface ContentCategoryService {

    /**
     * 查询内容分类视图
     *
     * @return 分类列表
     * @throws WayzException 平台异常
     */
    public List<ViewKeyValue> queryContentCategoryView(String name) throws WayzException;

    /**
     * 查询内容分类
     *
     * @param myId 我的标识
     * @return 分类列表
     * @throws WayzException 平台异常
     */
    PageVo<ContentCategoryVo> queryContentCategory(Long myId,String name,Integer startIndex, Integer pageSize);

    /**
     * 创建内容分类
     *
     * @param myId 我的标识
     * @param name 分类名称
     * @param description 描述
     * @return 分类标识
     * @throws WayzException 平台异常
     */
    public Long createContentCategory(Long myId, String name, String description) throws WayzException;

    /**
     * 删除内容分类
     *
     * @param myId 我的标识
     * @param id 内容分类标识
     * @throws WayzException 平台异常
     */
    public void deleteCategory(Long myId, Long id) throws WayzException;

    /**
     * 修改内容分类
     *
     * @param myId 我的标识
     * @param id 分类标识
     * @param name 分类名称
     * @param description 描述
     * @throws WayzException 平台异常
     */
    public void modifyCategory(Long myId, Long id, String name, String description) throws WayzException;

    /**
     * 获取内容分类
     *
     * @param myId 我的标识
     * @param id 分类标识
     * @return 内容分类
     * @throws WayzException 平台异常
     */
    public DContentCategory getContentCategory(Long myId, Long id);
}
