package com.wayz.entity.dto.poster;

import com.wayz.constants.PageDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 海报模板摘要类
 * 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PosterTempletPageDto extends PageDto implements Serializable {

	/** 公司标识 */
	private Long companyId = null;
	/** 分辨率标识 */
	private Long resolutionId = null;
	/** 行业标识 */
	private Long categoryId = null;
	/** 模板名称 */
	private String name = null;
	/** 是否发布 */
	private Boolean isPublish = false;

}
