package com.wayz.entity.pojo.category;

import java.io.*;
import java.sql.*;

/**
 * 内容分类类
 * 
 * @author wade.liu@wayz.ai
 *
 */
public class DContentCategory implements Serializable {

	/** 标识 */
	private Long id = null;
	/** 名称 */
	private String name = null;
	/** 描述 */
	private String description = null;
	/** 创建时间 */
	private Timestamp createdTime = null;
	/** 修改时间 */
	private Timestamp modifiedTime = null;

	/**
	 * 获取标识
	 * 
	 * @return 标识
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 设置标识
	 * 
	 * @param id 标识
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 获取名称
	 * 
	 * @return 名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置名称
	 * 
	 * @param name 名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 获取描述
	 * 
	 * @return 描述
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 设置描述
	 * 
	 * @param description 描述
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 获取创建时间
	 * 
	 * @return 创建时间
	 */
	public Timestamp getCreatedTime() {
		return createdTime;
	}

	/**
	 * 设置创建时间
	 * 
	 * @param createdTime 创建时间
	 */
	public void setCreatedTime(Timestamp createdTime) {
		this.createdTime = createdTime;
	}

	/**
	 * 获取修改时间
	 * 
	 * @return 修改时间
	 */
	public Timestamp getModifiedTime() {
		return modifiedTime;
	}

	/**
	 * 设置修改时间
	 * 
	 * @param modifiedTime 修改时间
	 */
	public void setModifiedTime(Timestamp modifiedTime) {
		this.modifiedTime = modifiedTime;
	}

}
