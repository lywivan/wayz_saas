package com.wayz.entity.vo.poster;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 海报分辨率类
 * 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PosterResolutionVo implements Serializable {

	/** 分辨率标识 */
	private Long id = null;
	/** 名称 */
	private String name = null;
	/** 宽(pix) */
	private Integer width = null;
	/** 高(pix) */
	private Integer height = null;
	/** 排序值(升序排列) */
	private Integer sort = null;
	/** 创建人 */
	private Long creatorId = null;
	/** 创建时间 */
	private String createdTime = null;

}
