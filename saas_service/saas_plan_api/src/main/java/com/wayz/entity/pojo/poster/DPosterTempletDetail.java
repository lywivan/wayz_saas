package com.wayz.entity.pojo.poster;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 海报模板详情类
 * 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DPosterTempletDetail implements Serializable {

	/** 模板标识 */
	private Long id = null;
	/** 公司标识 */
	private Long companyId = null;
	/** 行业标识 */
	private Long categoryId = null;
	/** 行业标识 */
	private String categoryName = null;
	/** 分辨率标识 */
	private Long resolutionId = null;
	/** 分辨率名称 */
	private String resolutionName = null;
	/** 模板名称 */
	private String name = null;
	/** 预览URL */
	private String previewUrl = null;
	/** 描述 */
	private String description = null;
	/** 模板背景 */
	private String background = null;
	/** 模板背景音乐 */
	private String backgroundAudio = null;
	/** 模板背景音乐名称 */
	private String audioName = null;
	/** 创建人 */
	private Long creatorId = null;
	/** 创建时间 */
	private String createdTime = null;
	/** 修改时间 */
	private String modifiedTime = null;
	/** 广告位配置列表 */
	private List<DPosterTempletAdslot> adslotList = null;

}
