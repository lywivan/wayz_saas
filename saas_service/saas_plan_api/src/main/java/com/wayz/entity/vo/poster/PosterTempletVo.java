package com.wayz.entity.vo.poster;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 海报模板摘要类
 * 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PosterTempletVo implements Serializable {

	/** 模板标识 */
	private Long id = null;
	/** 公司标识 */
	private Long companyId = null;
	private String companyName = null;
	/** 分辨率标识 */
	private Long resolutionId = null;
	/** 分辨率名称 */
	private String resolutionName = null;
	/** 行业标识 */
	private Long categoryId = null;
	/** 行业名称 */
	private String categoryName = null;
	/** 模板名称 */
	private String name = null;
	/** 预览URL */
	private String previewUrl = null;
	/** 描述 */
	private String description = null;
	/** 模板背景 */
	private String background = null;
	/** 是否发布 */
	private Boolean isPublish = false;
	/** 创建时间 */
	private String createdTime = null;
	/** 修改时间 */
	private String modifiedTime = null;

}
