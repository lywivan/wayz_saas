package com.wayz.entity.dto.application;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 应用类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApplicationDto implements Serializable {

	/** 应用标识 */
	private Long id = null;
	/** 名称 */
	private String name = null;
	/** 图标 */
	private String icon = null;
	/** 页面链接 */
	private String href = null;
	/** 星级 */
	private Integer starNum = null;
	/** 描述 */
	private String description = null;
	/** 应用截图 */
	private String images = null;


}
