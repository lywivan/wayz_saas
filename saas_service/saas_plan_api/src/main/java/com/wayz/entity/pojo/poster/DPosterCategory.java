package com.wayz.entity.pojo.poster;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 海报类别类
 * 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DPosterCategory implements Serializable {

	/** 海报类别标识 */
	private Long id = null;
	/** 海报类别名称 */
	private String name = null;
	/** 海报类别名称 */
	private String description = null;
	/** 是否删除 */
	private Boolean isDeleted = null;
	/** 创建人 */
	private Long creatorId = null;
	/** 创建时间 */
	private String createdTime = null;
}
