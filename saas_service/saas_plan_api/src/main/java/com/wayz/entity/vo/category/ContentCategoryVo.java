package com.wayz.entity.vo.category;

import java.io.Serializable;

/**
 * 内容分类类
 * 
 * @author wade.liu@wayz.ai
 *
 */
public class ContentCategoryVo implements Serializable {

	/** 分类标识 */
	private Long id = null;
	/** 名称 */
	private String name = null;
	/** 描述 */
	private String description = null;
	/** 创建时间 */
	private String createdTime = null;
	/** 修改时间 */
	private String modifiedTime = null;

	/**
	 * 获取分类标识
	 * 
	 * @return 分类标识
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 设置分类标识
	 * 
	 * @param id 分类标识
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 获取名称
	 * 
	 * @return 名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置名称
	 * 
	 * @param name 名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 获取描述
	 * 
	 * @return 描述
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 设置描述
	 * 
	 * @param description 描述
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 获取创建时间
	 * 
	 * @return 创建时间
	 */
	public String getCreatedTime() {
		return createdTime;
	}

	/**
	 * 设置创建时间
	 * 
	 * @param createdTime 创建时间
	 */
	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}

	/**
	 * 获取修改时间
	 * 
	 * @return 修改时间
	 */
	public String getModifiedTime() {
		return modifiedTime;
	}

	/**
	 * 设置修改时间
	 * 
	 * @param modifiedTime 修改时间
	 */
	public void setModifiedTime(String modifiedTime) {
		this.modifiedTime = modifiedTime;
	}

}
