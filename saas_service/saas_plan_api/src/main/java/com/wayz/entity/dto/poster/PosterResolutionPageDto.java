package com.wayz.entity.dto.poster;

import com.wayz.constants.PageDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 海报分辨率类
 * 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PosterResolutionPageDto extends PageDto implements Serializable {

	/** 公司标识 */
	private Long companyId = null;
	/** 宽(pix) */
	private Integer width = null;
	/** 高(pix) */
	private Integer height = null;

}
