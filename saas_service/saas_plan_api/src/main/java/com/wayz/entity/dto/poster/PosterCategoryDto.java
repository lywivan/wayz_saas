package com.wayz.entity.dto.poster;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 海报类别类
 * 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PosterCategoryDto implements Serializable {

	/** 公司标识 */
	private Long id = null;
	/** 公司标识 */
	private Long companyId = null;
	/** 类别名称 */
	private String name = null;
	/** 类别备注 */
	private String description = null;
}
