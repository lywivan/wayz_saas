package com.wayz.entity.dto.content;

import java.io.*;
import java.sql.*;

/**
 * 商城内容类
 * 
 * @author wade.liu@wayz.ai
 *
 */
public class ShopContentDto implements Serializable {

	/** 文件标识 */
	private Long id = null;
	/** 内容渠道标识 */
	private Long channelId = null;
	/** 分类标识 */
	private Long categoryId = null;
	/** 文件名称 */
	private String name = null;
	/** 内容标题 */
	private String title = null;
	/** 内容简介 */
	private String description = null;
	/** 售卖价格 */
	private Double price = null;
	/** 文件类型(1:图片 2:视频 3:音频 4:网页 5:office文档) */
	private Short type = null;
	/** 审核状态(0:未审核; 1:审核通过; 2: 审核未通过) */
	private Short auditStatus = null;
	/** 文件链接 */
	private String url = null;
	/** 预览url */
	private String previewUrl = null;
	/** 文件检验码MD5 */
	private String filemd5 = null;
	/** 文件长度(字节) */
	private Long fileSize = null;
	/** 文件属性:时长(秒) */
	private Integer duration = null;
	/** 文件属性:宽度 */
	private Integer width = null;
	/** 文件属性:高度 */
	private Integer height = null;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getChannelId() {
		return channelId;
	}

	public Short getAuditStatus() {
		return auditStatus;
	}

	public void setAuditStatus(Short auditStatus) {
		this.auditStatus = auditStatus;
	}

	public void setChannelId(Long channelId) {
		this.channelId = channelId;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Short getType() {
		return type;
	}

	public void setType(Short type) {
		this.type = type;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getPreviewUrl() {
		return previewUrl;
	}

	public void setPreviewUrl(String previewUrl) {
		this.previewUrl = previewUrl;
	}

	public String getFilemd5() {
		return filemd5;
	}

	public void setFilemd5(String filemd5) {
		this.filemd5 = filemd5;
	}

	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}
}
