package com.wayz.entity.vo.application;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 应用类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApplicationVo implements Serializable {


	/** 标识 */
	private Long id = null;
	/** 名称 */
	private String name = null;
	/** 图标 */
	private String icon = null;
	/** 页面链接 */
	private String href = null;
	/** 标题 */
	private String title = null;
	/** 星级 */
	private Integer starNum = null;
	/** 描述 */
	private String description = null;
	/** 应用截图 */
	private String images = null;
	/** 是否有效 */
	private Boolean isEnable = null;
	/** 购买计数 */
	private Integer buyCount = null;
	/** 创建时间 */
	private String createdTime = null;
	/** 修改时间 */
	private String modifiedTime = null;
	/** 是否购买 */
	private Boolean isBuy = false;


}
