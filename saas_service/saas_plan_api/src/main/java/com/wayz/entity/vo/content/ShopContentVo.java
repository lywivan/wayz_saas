package com.wayz.entity.vo.content;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 商城内容类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ShopContentVo implements Serializable {

	/** 商城内容标识 */
	private Long id = null;
	/** 渠道标识 */
	private Long channelId = null;
	/** 渠道名称 */
	private String channelName = null;
	/** 分类标识 */
	private Long categoryId = null;
	/** 分类名称 */
	private String categoryName = null;
	/** 内容名称 */
	private String name = null;
	/** 内容标题 */
	private String title = null;
	/** 内容简介 */
	private String description = null;
	/** 价格(元) */
	private Double price = null;
	/** 文件类型(1:图片 2:视频 3:音频 4:网页 5:office文档) */
	private Short type = null;
	/** 文件链接 */
	private String url = null;
	/** 预览链接 */
	private String previewUrl = null;
	/** 文件检验码MD5 */
	private String filemd5 = null;
	/** 文件长度(字节) */
	private Long fileSize = null;
	/** 时长(秒) */
	private Integer duration = null;
	/** 分辨率宽度 */
	private Integer width = null;
	/** 分辨率宽度 */
	private Integer height = null;
	/** 创建时间 */
	private String createdTime = null;
	/** 是否购买 */
	private Boolean isBuy = false;
	/** 审核状态(0:未审核; 1:审核通过; 2: 审核未通过) */
	private Short auditStatus = null;
	/** 审核备注 */
	private String remark = null;



}
