package com.wayz.entity.dto.content;

import com.wayz.constants.PageDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 商城内容类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ShopContentPageDto extends PageDto implements Serializable {

	/** 渠道标识 */
	private Long channelId = null;
	/** 分类标识 */
	private Long categoryId = null;
	/** 分类名称 */
	private Double beginPrice = null;
	/** 分类名称 */
	private Double endPrice = null;
	/** 文件类型(1:图片 2:视频 3:音频 4:网页 5:office文档) */
	private Short type = null;
	/** 审核备注 */
	private Short status = null;

}
