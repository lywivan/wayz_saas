package com.wayz.entity.pojo.application;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 应用类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DApplication implements Serializable {

	/** 标识 */
	private Long id = null;
	/** 名称 */
	private String name = null;
	/** 图标 */
	private String icon = null;
	/** 页面链接 */
	private String href = null;
	/** 星级 */
	private Integer starNum = null;
	/** 描述 */
	private String description = null;
	/** 应用截图 */
	private String images = null;
	/** 是否有效 */
	private Boolean isEnable = null;
	/** 购买计数 */
	private Integer buyCount = null;
	/** 创建时间 */
	private Timestamp createdTime = null;
	/** 修改时间 */
	private Timestamp modifiedTime = null;
}
