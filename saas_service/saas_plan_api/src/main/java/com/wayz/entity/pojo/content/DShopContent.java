package com.wayz.entity.pojo.content;

import java.io.*;
import java.sql.*;

/**
 * 商城内容类
 * 
 * @author wade.liu@wayz.ai
 *
 */
public class DShopContent implements Serializable {

	/** 文件标识 */
	private Long id = null;
	/** 内容渠道标识 */
	private Long channelId = null;
	/** 渠道名称 */
	private String channelName = null;
	/** 分类标识 */
	private Long categoryId = null;
	/** 分类名称 */
	private String categoryName = null;
	/** 文件名称 */
	private String name = null;
	/** 内容标题 */
	private String title = null;
	/** 内容简介 */
	private String description = null;
	/** 售卖价格 */
	private Double price = null;
	/** 文件类型(1:图片 2:视频 3:音频 4:网页 5:office文档) */
	private Short type = null;
	/** 文件链接 */
	private String url = null;
	/** 预览url */
	private String previewUrl = null;
	/** 文件检验码MD5 */
	private String filemd5 = null;
	/** 文件长度(字节) */
	private Long fileSize = null;
	/** 文件属性:时长(秒) */
	private Integer duration = null;
	/** 文件属性:宽度 */
	private Integer width = null;
	/** 文件属性:高度 */
	private Integer height = null;
	/** 是否删除 */
	private Boolean isDeleted = null;
	/** 创建人标识 */
	private Long creatorId = null;
	/** 审核人标识 */
	private Long auditorId = null;
	/** 审核状态(0:未审核; 1:审核通过; 2: 审核未通过) */
	private Short auditStatus = null;
	/** 审核时间 */
	private Timestamp auditTime = null;
	/** 审核备注 */
	private String remark = null;
	/** 创建时间 */
	private Timestamp createdTime = null;
	/** 修改时间 */
	private Timestamp modifiedTime = null;


	/**
	 * 获取文件标识
	 * 
	 * @return 文件标识
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 设置文件标识
	 * 
	 * @param id 文件标识
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 获取内容渠道标识
	 * 
	 * @return 内容渠道标识
	 */
	public Long getChannelId() {
		return channelId;
	}

	/**
	 * 设置内容渠道标识
	 * 
	 * @param channelId 内容渠道标识
	 */
	public void setChannelId(Long channelId) {
		this.channelId = channelId;
	}

	/**
	 * 获取分类标识
	 * 
	 * @return 分类标识
	 */
	public Long getCategoryId() {
		return categoryId;
	}

	/**
	 * 设置分类标识
	 * 
	 * @param categoryId 分类标识
	 */
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	/**
	 * 获取文件名称
	 * 
	 * @return 文件名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置文件名称
	 * 
	 * @param name 文件名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 获取售卖价格
	 * 
	 * @return 售卖价格
	 */
	public Double getPrice() {
		return price;
	}

	/**
	 * 设置售卖价格
	 * 
	 * @param price 售卖价格
	 */
	public void setPrice(Double price) {
		this.price = price;
	}

	/**
	 * 获取文件类型(1:图片 2:视频 3:音频 4:网页 5:office文档)
	 * 
	 * @return 文件类型(1:图片 2:视频 3:音频 4:网页 5:office文档)
	 */
	public Short getType() {
		return type;
	}

	/**
	 * 设置文件类型(1:图片 2:视频 3:音频 4:网页 5:office文档)
	 * 
	 * @param type 文件类型(1:图片 2:视频 3:音频 4:网页 5:office文档)
	 */
	public void setType(Short type) {
		this.type = type;
	}

	/**
	 * 获取文件链接
	 * 
	 * @return 文件链接
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * 设置文件链接
	 * 
	 * @param url 文件链接
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * 获取预览url
	 * 
	 * @return 预览url
	 */
	public String getPreviewUrl() {
		return previewUrl;
	}

	/**
	 * 设置预览url
	 * 
	 * @param previewUrl 预览url
	 */
	public void setPreviewUrl(String previewUrl) {
		this.previewUrl = previewUrl;
	}

	/**
	 * 获取文件检验码MD5
	 * 
	 * @return 文件检验码MD5
	 */
	public String getFilemd5() {
		return filemd5;
	}

	/**
	 * 设置文件检验码MD5
	 * 
	 * @param filemd5 文件检验码MD5
	 */
	public void setFilemd5(String filemd5) {
		this.filemd5 = filemd5;
	}

	/**
	 * 获取文件长度(字节)
	 * 
	 * @return 文件长度(字节)
	 */
	public Long getFileSize() {
		return fileSize;
	}

	/**
	 * 设置文件长度(字节)
	 * 
	 * @param fileSize 文件长度(字节)
	 */
	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	/**
	 * 获取文件属性:时长(秒)
	 * 
	 * @return 文件属性:时长(秒)
	 */
	public Integer getDuration() {
		return duration;
	}

	/**
	 * 设置文件属性:时长(秒)
	 * 
	 * @param duration 文件属性:时长(秒)
	 */
	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	/**
	 * 获取文件属性:宽度
	 * 
	 * @return 文件属性:宽度
	 */
	public Integer getWidth() {
		return width;
	}

	/**
	 * 设置文件属性:宽度
	 * 
	 * @param width 文件属性:宽度
	 */
	public void setWidth(Integer width) {
		this.width = width;
	}

	/**
	 * 获取文件属性:高度
	 * 
	 * @return 文件属性:高度
	 */
	public Integer getHeight() {
		return height;
	}

	/**
	 * 设置文件属性:高度
	 * 
	 * @param height 文件属性:高度
	 */
	public void setHeight(Integer height) {
		this.height = height;
	}

	/**
	 * 获取是否删除
	 * 
	 * @return 是否删除
	 */
	public Boolean getIsDeleted() {
		return isDeleted;
	}

	/**
	 * 设置是否删除
	 * 
	 * @param isDeleted 是否删除
	 */
	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	/**
	 * 获取创建人标识
	 * 
	 * @return 创建人标识
	 */
	public Long getCreatorId() {
		return creatorId;
	}

	/**
	 * 设置创建人标识
	 * 
	 * @param creatorId 创建人标识
	 */
	public void setCreatorId(Long creatorId) {
		this.creatorId = creatorId;
	}

	/**
	 * 获取审核人标识
	 * 
	 * @return 审核人标识
	 */
	public Long getAuditorId() {
		return auditorId;
	}

	/**
	 * 设置审核人标识
	 * 
	 * @param auditorId 审核人标识
	 */
	public void setAuditorId(Long auditorId) {
		this.auditorId = auditorId;
	}

	/**
	 * 获取审核状态(0:未审核; 1:审核通过; 2: 审核未通过)
	 * 
	 * @return 审核状态(0:未审核; 1:审核通过; 2: 审核未通过)
	 */
	public Short getAuditStatus() {
		return auditStatus;
	}

	/**
	 * 设置审核状态(0:未审核; 1:审核通过; 2: 审核未通过)
	 * 
	 * @param auditStatus 审核状态(0:未审核; 1:审核通过; 2: 审核未通过)
	 */
	public void setAuditStatus(Short auditStatus) {
		this.auditStatus = auditStatus;
	}

	/**
	 * 获取审核时间
	 * 
	 * @return 审核时间
	 */
	public Timestamp getAuditTime() {
		return auditTime;
	}

	/**
	 * 设置审核时间
	 * 
	 * @param auditTime 审核时间
	 */
	public void setAuditTime(Timestamp auditTime) {
		this.auditTime = auditTime;
	}

	/**
	 * 获取审核备注
	 * 
	 * @return 审核备注
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * 设置审核备注
	 * 
	 * @param remark 审核备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * 获取创建时间
	 * 
	 * @return 创建时间
	 */
	public Timestamp getCreatedTime() {
		return createdTime;
	}

	/**
	 * 设置创建时间
	 * 
	 * @param createdTime 创建时间
	 */
	public void setCreatedTime(Timestamp createdTime) {
		this.createdTime = createdTime;
	}

	/**
	 * 获取修改时间
	 * 
	 * @return 修改时间
	 */
	public Timestamp getModifiedTime() {
		return modifiedTime;
	}

	/**
	 * 设置修改时间
	 * 
	 * @param modifiedTime 修改时间
	 */
	public void setModifiedTime(Timestamp modifiedTime) {
		this.modifiedTime = modifiedTime;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Boolean getDeleted() {
		return isDeleted;
	}

	public void setDeleted(Boolean deleted) {
		isDeleted = deleted;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
