package com.wayz.entity.dto.application;

import com.wayz.constants.PageDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 商城内容类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ShopContentPageDto extends PageDto implements Serializable {

	/** 名称 */
	private String name = null;
	/** 星级 */
	private Integer starNum = null;
	/** 是否有效 */
	private Boolean isEnable = null;

}
