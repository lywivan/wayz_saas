package com.wayz.service;

import com.wayz.constants.CAuditMediaStatus;
import com.wayz.constants.PageVo;
import com.wayz.constants.ViewKeyValue;
import com.wayz.entity.dto.cibn.MediaCibnDto;
import com.wayz.entity.pojo.cibn.DAdvPosition;
import com.wayz.entity.pojo.cibn.DCompanyCibn;
import com.wayz.entity.pojo.cibn.DMediaCibnSnapshot;
import com.wayz.entity.vo.cibn.AdvPositionVo;
import com.wayz.exception.WayzException;
import com.wayz.mapper.DAdvPositionDAO;
import com.wayz.mapper.DCompanyCibnDAO;
import com.wayz.mapper.DCompanyCibnSnapshotDAO;
import com.wayz.mapper.DMediaCibnSnapshotDAO;
import com.wayz.redis.RMediaCibnSnapshotId;
import com.wayz.util.TimeHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author wade.liu@wayz.ai
 * @Date: 2019/12/10 下午6:22
 */
@org.apache.dubbo.config.annotation.Service(version = "1.0.0", protocol = "dubbo")
@RefreshScope
@Service("advPositionCibnService")
public class AdvPositionCibnServiceImpl implements AdvPositionCibnService {

    /**
     * 媒体点位DAO
     */
    @Resource(name = "dAdvPositionDAO")
    private DAdvPositionDAO dAdvPositionDAO;

    /**
     * 公司申请DAO
     */
    @Resource(name = "dCompanyCibnDAO")
    private DCompanyCibnDAO dCompanyCibnDAO;

    /**
     * 公司备案快照DAO
     */
    @Resource(name = "dCompanyCibnSnapshotDAO")
    private DCompanyCibnSnapshotDAO dCompanyCibnSnapshotDAO;

    /**
     * 媒体快照Redis
     */
    @Resource(name = "rMediaCibnSnapshotId")
    private RMediaCibnSnapshotId rMediaCibnSnapshotId;

    /**
     * 媒体点位备案快照DAO
     */
    @Resource(name = "dMediaCibnSnapshotDAO")
    private DMediaCibnSnapshotDAO dMediaCibnSnapshotDAO;

    /**
     * 审核状态
     * @param myId
     * @return
     */
    @Override
    public List<ViewKeyValue> queryAuditType(Long myId) throws WayzException {
        List<ViewKeyValue> viewList = new ArrayList<ViewKeyValue>();
        ViewKeyValue view = null;
        for (CAuditMediaStatus cAuditMediaStatus : CAuditMediaStatus.values()) {
            view = new ViewKeyValue();
            view.setId(cAuditMediaStatus.getValue());
            view.setName(cAuditMediaStatus.getDescription());
            viewList.add(view);
        }
        return viewList;
    }

    /**
     * 查询媒体点位
     *
     * @param auditStatus
     * @param playCode
     * @param name
     * @param startIndex
     * @param pageSize
     * @return
     */
    @Override
    public PageVo<AdvPositionVo> queryPosition(Long companyId, Long id, String playCode, String name, Boolean isBound,
                                               Boolean isEnable, Boolean isOnline, Short status, Short auditStatus, Integer startIndex, Integer pageSize)
            throws WayzException {
        // 初始化
        PageVo<AdvPositionVo> list = new PageVo<>();
        Integer count = dAdvPositionDAO.count(companyId, null, id, playCode, name, isBound, isEnable, isOnline, status,
                auditStatus);
        if (count <= 0) {
            return list;
        }
        // 数据转换
        List<DAdvPosition> dAdvPositionList = dAdvPositionDAO.query(companyId, id, playCode, name, isBound, isEnable,
                isOnline, status, auditStatus, startIndex, pageSize);
        List<AdvPositionVo> advPositions = assembleData(dAdvPositionList);
        // 返回数据
        list.setTotalCount(count);
        list.setList(advPositions);
        return list;
    }

    /**
     * 数据转换
     *
     * @param dList
     * @return
     */
    private List<AdvPositionVo> assembleData(List<DAdvPosition> dList) {
        List<AdvPositionVo> list = new ArrayList<>();
        if (!dList.isEmpty()) {
            for (DAdvPosition dAdvPosition : dList) {
                AdvPositionVo bean = new AdvPositionVo();
                BeanUtils.copyProperties(dAdvPosition, bean);
                if (dAdvPosition.getAuditTime() != null) {
                    bean.setAuditTime(TimeHelper.getTimestamp(dAdvPosition.getAuditTime()));
                }
                if (dAdvPosition.getStartTime() != null) {
                    bean.setStartTime(TimeHelper.getTime(dAdvPosition.getStartTime()));
                }
                if (dAdvPosition.getCloseTime() != null) {
                    bean.setCloseTime(TimeHelper.getTime(dAdvPosition.getCloseTime()));
                }
                bean.setStatusName(CAuditMediaStatus.getDescription(dAdvPosition.getAuditStatus()));
                bean.setCreatedTime(TimeHelper.getTimestamp(dAdvPosition.getCreatedTime()));
                list.add(bean);
            }
        }
        return list;
    }

    /**
     * 媒体点位详情
     * @param id 媒体点位ID
     * @return
     */
    @Override
    public AdvPositionVo getPosition(Long id) throws WayzException {
        DAdvPosition dAdvPosition = dAdvPositionDAO.get(id);
        if (dAdvPosition == null) {
            throw new WayzException("设备不存在");
        }
        AdvPositionVo advPosition = new AdvPositionVo();
        BeanUtils.copyProperties(dAdvPosition, advPosition);
        if (dAdvPosition.getAuditTime() != null) {
            advPosition.setAuditTime(TimeHelper.getTimestamp(dAdvPosition.getAuditTime()));
        }
        if (dAdvPosition.getStartTime() != null) {
            advPosition.setStartTime(TimeHelper.getTime(dAdvPosition.getStartTime()));
        }
        if (dAdvPosition.getCloseTime() != null) {
            advPosition.setCloseTime(TimeHelper.getTime(dAdvPosition.getCloseTime()));
        }
        advPosition.setStatusName(CAuditMediaStatus.getDescription(dAdvPosition.getAuditStatus()));
        advPosition.setCreatedTime(TimeHelper.getTimestamp(dAdvPosition.getCreatedTime()));
        return advPosition;
    }

    /**
     * 媒体点位备案列表 --> 管理端
     * @param auditStatus 审核状态
     * @param code 设备编码
     * @param name 公司名称
     * @param startIndex  开始角标
     * @param pageSize    每页大小
     * @return
     * @throws WayzException
     */
    @Override
    public PageVo<AdvPositionVo> queryAdminPositionCibnList(Short auditStatus, String code, String name, Integer startIndex, Integer pageSize) throws WayzException {

        // 初始化
        PageVo<AdvPositionVo> resultList = new PageVo<>();
        Integer count = dAdvPositionDAO.countMediaCIBN(null, auditStatus, name);
        if (count <= 0){
            return resultList;
        }
        // 数据转换
        List<DAdvPosition> dAdvPositionList = dAdvPositionDAO.queryMediaCIBNList(null, auditStatus, name, startIndex, pageSize);
        List<AdvPositionVo> advPositions = assembleData(dAdvPositionList);
        // 返回数据
        resultList.setTotalCount(count);
        resultList.setList(advPositions);
        return resultList;
    }

    /**
     * 审核媒体点位申请
     * @param dto
     * @throws WayzException
     */
    @Override
    public void auditMediaCibn(MediaCibnDto dto) throws WayzException {

        // 1. 申请媒体点位
        DAdvPosition modify = new DAdvPosition();
        modify.setId(dto.getId());
        modify.setAuditTime(TimeHelper.getCurrentTimestamp());
        modify.setModifiedTime(TimeHelper.getCurrentTimestamp());
        modify.setAuditStatus(dto.getAuditStatus());
        modify.setRemark(dto.getRemark());
        dAdvPositionDAO.modifyAudit(modify);
        if (dto.getAuditStatus() == CAuditMediaStatus.PASS_AUDIT.getValue()){
            DAdvPosition dAdvPosition = dAdvPositionDAO.get(dto.getId());
            DCompanyCibn dCompanyCibn = dCompanyCibnDAO.get(dto.getCompanyId());

            // 2. 维护公司审核记录 点位数
            dCompanyCibnDAO.resetCibnNum(dto.getCompanyId());

            // 3. 公司快照表维护
            dCompanyCibnSnapshotDAO.resetSnapshotCibnNum(dto.getCompanyId());

            // 4.媒体点位插入
            Long id = rMediaCibnSnapshotId.increment();
            DMediaCibnSnapshot create = new DMediaCibnSnapshot();
            create.setScreenSize(dAdvPosition.getScreenSize());
            create.setSceneImage(dAdvPosition.getSceneImage());
            create.setName(dAdvPosition.getCompanyName());
            create.setAddress(dAdvPosition.getAddress());
            create.setCode(dAdvPosition.getPlayCode());
            create.setValidUntilDate(dCompanyCibn.getValidUntilDate());
            create.setAuditTime(TimeHelper.getCurrentTimestamp());
            create.setOriginId(dto.getId());
            dMediaCibnSnapshotDAO.create(id,create);
        }
    }
}
