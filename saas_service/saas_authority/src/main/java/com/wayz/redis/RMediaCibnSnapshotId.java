package com.wayz.redis;

/**
 * 点位备案标识接口
 * 
 * @author BML
 *
 */
public interface RMediaCibnSnapshotId {

	/**
	 * 递增点位备案标识
	 * 
	 * @return 点位备案标识
	 */
	public Long increment();

}
