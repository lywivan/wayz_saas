package com.wayz.redis.impl;

import javax.annotation.Resource;

import com.wayz.redis.RMediaCibnSnapshotId;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

/**
 * 点位备案标识实现类
 * 
 * @author BML
 *
 */
@Repository("rMediaCibnSnapshotId")
public class RMediaCibnSnapshotIdImpl implements RMediaCibnSnapshotId {

	/** Redis模板 */
	@Resource(name = "redisTemplate")
	private RedisTemplate<String, String> redisTemplate = null;

	/** 键值格式 */
	private static final String KEY = "platform:Id:MediaCibnSnapshot";

	/**
	 * 递增点位备案标识
	 * 
	 * @return 点位备案标识
	 */
	@Override
	public Long increment() {
		return redisTemplate.opsForValue().increment(KEY, 1L);
	}

}
