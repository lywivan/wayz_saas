package com.wayz.mapper;

import java.util.List;

import com.wayz.entity.pojo.cibn.DAdvPosition;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 媒体位DAO接口
 * 
 * @author BML
 *
 */
@Repository("dAdvPositionDAO")
public interface DAdvPositionDAO {
	/**
	 * 统计媒体位数量语句
	 *
	 * @param companyId 公司ID
	 * @param name 媒体位名称
	 * @return
	 */
	public Integer count(@Param("companyId") Long companyId, @Param("departPath") String departPath, @Param("id") Long id, @Param("playCode") String playCode,
						 @Param("name") String name, @Param("isBound") Boolean isBound, @Param("isEnable") Boolean isEnable,
						 @Param("isOnLine") Boolean isOnLine, @Param("status") Short status, @Param("auditStatus") Short auditStatus);

	/**
	 * 媒体位列表
	 *
	 * @param companyId 公司列表
	 * @param name 媒体位名称
	 * @param startIndex 开始角标
	 * @param pageSize 每页大小
	 * @return
	 */
	public List<DAdvPosition> query(@Param("companyId") Long companyId, @Param("id") Long id,
									@Param("playCode") String playCode, @Param("name") String name, @Param("isBound") Boolean isBound,
									@Param("isEnable") Boolean isEnable, @Param("isOnLine") Boolean isOnLine, @Param("status") Short status,
									@Param("auditStatus") Short auditStatus, @Param("startIndex") Integer startIndex, @Param("pageSize") Integer pageSize);


	/**
	 * 获取媒体位
	 *
	 * @param id 网点标识
	 * @return 媒体位
	 */
	public DAdvPosition get(@Param("id") Long id);

	/**
	 * 统计申请数量语句
	 * @param companyId 公司ID
	 * @param auditStatus  审核状态
	 * @param name  媒体位名称
	 * @return
	 */
	public Integer countMediaCIBN(@Param("companyId") Long companyId,@Param("auditStatus") Short auditStatus,@Param("name") String name);


	/**
	 * 媒体点位申请列表
	 * @param companyId 公司列表
	 * @param auditStatus 审核状态
	 * @param name 媒体位名称
	 * @param startIndex 开始角标
	 * @param pageSize 每页大小
	 * @return
	 */
	public List<DAdvPosition> queryMediaCIBNList(@Param("companyId") Long companyId, @Param("auditStatus") Short auditStatus, @Param("name") String name, @Param("startIndex") Integer startIndex, @Param("pageSize") Integer pageSize);

	/**
	 * 审核媒体点位申请
	 * @param modify
	 * @return
	 */
	public Integer modifyAudit(@Param("modify") DAdvPosition modify);
}
