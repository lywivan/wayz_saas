package com.wayz.mapper;

import com.wayz.entity.pojo.cibn.DMediaCibnSnapshot;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 媒体点位快照DAO接口
 * 
 * @author BML
 *
 */
@Repository("dMediaCibnSnapshotDAO")
public interface DMediaCibnSnapshotDAO {
	/**
	 * 创建媒体点位快照
	 *
	 * @param id 主键ID
	 * @param create 媒体点位快照创建
	 * @return 创建行数
	 */
	public Integer create(@Param("id") Long id, @Param("create") DMediaCibnSnapshot create);
}
