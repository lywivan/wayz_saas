package com.wayz.service;

import com.wayz.AdminProviderApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Set;

@SpringBootTest(classes = AdminProviderApplication.class)
@RunWith(SpringRunner.class)
public class MenuAdminServiceImplTest {
    /** 菜单标识 */
    @Autowired
    private RedisTemplate redisTemplate = null;

    @Test
    public void checkMyMenu() {
        Set<String> entries = redisTemplate.opsForSet().members("platform:List:Admin:RRoleMenuList:2");
        //Map<String,String> entries = redisTemplate.opsForHash().entries("platform:Object:Admin:User:2");
        System.out.println(entries);
    }
}