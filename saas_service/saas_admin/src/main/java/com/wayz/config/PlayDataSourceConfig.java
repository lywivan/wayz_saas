//package com.wayz.config;
//
//import org.apache.ibatis.session.SqlSessionFactory;
//import org.mybatis.spring.SqlSessionFactoryBean;
//import org.mybatis.spring.SqlSessionTemplate;
//import org.mybatis.spring.annotation.MapperScan;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.boot.jdbc.DataSourceBuilder;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
//
//import javax.sql.DataSource;
//
//@Configuration
////扫描本数据源自己的路径
//@MapperScan(basePackages = "com.wayz.play",
//        sqlSessionFactoryRef = "playSqlSessionFactory")
//public class PlayDataSourceConfig {
//    /**
//     * 配置数据源，读取配置文件
//     * @return
//     */
//    @Bean("playDataSource")
//    @ConfigurationProperties(prefix = "spring.datasource.play")
//    public DataSource playDataSource(){
//        return DataSourceBuilder.create().build();
//    }
//
//    /**
//     * 配置SqlSessionFactory
//     * @param dataSource
//     * @return
//     * @throws Exception
//     */
//    @Bean("playSqlSessionFactory")
//    public SqlSessionFactory playSqlSessionFactory(@Qualifier("playDataSource") DataSource dataSource) throws Exception {
//        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
//        sqlSessionFactoryBean.setDataSource(dataSource);
//        org.apache.ibatis.session.Configuration configuration = new org.apache.ibatis.session.Configuration();
//        //开启驼峰命名
//        configuration.setMapUnderscoreToCamelCase(true);
//        sqlSessionFactoryBean.setConfiguration(configuration);
//        //扫描xml文件
//        sqlSessionFactoryBean.setMapperLocations(new PathMatchingResourcePatternResolver()
//                .getResources("classpath*:play/*.xml")
//        );
//        return sqlSessionFactoryBean.getObject();
//    }
//
//    /**
//     * 配置SqlSessionTemplate（可省略）
//     * @param sqlSessionFactory
//     * @return
//     */
//    @Bean("playSqlSessionTemplate")
//    public SqlSessionTemplate playSqlSessionTemplate(@Qualifier("playSqlSessionFactory") SqlSessionFactory sqlSessionFactory){
//        return new SqlSessionTemplate(sqlSessionFactory);
//    }
//}