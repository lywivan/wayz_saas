package com.wayz.job;

import com.wayz.exception.WayzException;
import com.wayz.service.AccountValid;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * 账户检查任务
 *
 */
@Component
@Slf4j
public class AccountJob {
    /** 账户有效检查接口 */
    @Autowired
    private AccountValid accountValid;

    @XxlJob("account-expire")
    public ReturnT<String> demoJobHandler(String param) throws Exception {
        try {
            startExecute();
        }
        catch (WayzException e) {
        }
        return ReturnT.SUCCESS;
    }

    /**
     *
     * 执行任务
     *
     * @throws WayzException
     */
    private void startExecute() throws WayzException {
        try {
            // 检查无效账户
            accountValid.checkTestingAccount();

            // 处理过期点位
            accountValid.invalidPosition();

            // 处理过期点位
            accountValid.invalidCoupon();

            // 处理即将到期 通知
            accountValid.remindExpire();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
