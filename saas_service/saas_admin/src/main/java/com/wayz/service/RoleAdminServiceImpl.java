package com.wayz.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.wayz.constants.CUserStatus;
import com.wayz.constants.PageVo;
import com.wayz.constants.ViewMap;
import com.wayz.entity.dto.role.RoleDto;
import com.wayz.entity.dto.role.RoleMenuIdsDto;
import com.wayz.entity.dto.role.RolePageDto;
import com.wayz.entity.pojo.role.DRoleAdmin;
import com.wayz.entity.vo.role.RoleAdminVo;
import com.wayz.exception.WayzException;
import com.wayz.mapper.DRoleAdminDAO;
import com.wayz.redis.BeanIdKeyEnum;
import com.wayz.redis.RedisUtils;
import com.wayz.util.TimeHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.apache.dubbo.config.annotation.Service;


/**
 * 角色服务实现类
 * 
 * @author:mike.ma
 *
 */
@Service(version = "1.0.0", protocol = "dubbo")
@RefreshScope
public class RoleAdminServiceImpl implements RoleAdminService {

	/** 标识相关 */
	/** 用户标记标识 */
	@Resource(name = "redisUtils")
	private RedisUtils redisUtils = null;

	/** DAO相关 */
	/** 角色DAO */
	@Resource(name = "dRoleAdminDAO")
	private DRoleAdminDAO dRoleAdminDAO = null;

	//@Resource(name = "userAdminService")
	@Autowired
	private UserAdminService userAdminService = null;

	//@Resource(name = "menuAdminService")
	@Autowired
	private MenuAdminService menuAdminService = null;

	/**
	 * 查询角色视图
	 * 
	 * @param myId 我的标识
	 * @return 角色视图列表
	 * @throws WayzException 云歌广告平台异常
	 */
	@Override
	public List<ViewMap> queryRoleView(Long myId) throws WayzException {
		// 查询角色视图
		List<ViewMap> roleList = dRoleAdminDAO.queryRoleView();
		// 返回数据
		return roleList;
	}

	/**
	 * 查询角色
	 * 
	 * @param myId 我的标识
	 * @return 角色分页
	 * @throws WayzException 云歌广告平台异常
	 */
	@Override
	public PageVo<RoleAdminVo> queryRole(Long myId, RolePageDto rolePageDto)
			throws WayzException {
		// 初始化
		PageVo<RoleAdminVo> roleAdminPage = new PageVo<>();
		List<RoleAdminVo> roleList = new ArrayList<RoleAdminVo>();
		// 查询数据
		// 查询数据: 角色总数
		Integer totalCount = dRoleAdminDAO.count(rolePageDto.getName());
		if (totalCount <= 0) {
			return roleAdminPage;
		}
		// 查询数据: 角色列表
		List<DRoleAdmin> dRoleList = dRoleAdminDAO.query(rolePageDto.getName(), rolePageDto.getStartIndex(), rolePageDto.getPageSize());
		RoleAdminVo role;
		// 转化数据
		try {
			for (DRoleAdmin dRole : dRoleList) {
				// 初始化
				role = new RoleAdminVo();
				// 数据赋值
				role.setId(dRole.getId());
				role.setName(dRole.getName());
				role.setDescription(dRole.getDescription());
				role.setCreateTime(TimeHelper.getTimestamp(dRole.getCreatedTime()));
				// 添加数据
				roleList.add(role);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		// 赋值数据
		roleAdminPage.setTotalCount(totalCount);
		roleAdminPage.setList(roleList);
		// 返回数据
		return roleAdminPage;
	}

	/**
	 * 创建角色
	 * 
	 * @param myId 我的标识
	 * @return 角色标识
	 * @throws WayzException 云歌广告平台异常
	 */
	@Override
	public Long createRole(Long myId, RoleDto roleDto) throws WayzException {

		// 存入mysql
		DRoleAdmin dRoleAdminCreate = this.insertRole(roleDto);
		// 返回数据
		return dRoleAdminCreate.getId();
	}

	/**
	 * 写入mysql数据
	 * @return
	 * @throws WayzException
	 */
	private DRoleAdmin insertRole(RoleDto roleDto) throws WayzException {
		// 初始化
		// 检查名称
		Boolean exists = dRoleAdminDAO.existsName(roleDto.getName(), null);
		if (exists != null && exists.booleanValue()) {
			throw new WayzException("角色名称已经存在");
		}
		// Long roleId = rRoleId.increment();
		Long roleId = redisUtils.incrBy(BeanIdKeyEnum.ROLE_ADMIN.getKey(), RedisUtils.STEP_SIZE);
		// 创建角色
		DRoleAdmin dRoleAdminCreate = new DRoleAdmin();
		dRoleAdminCreate.setId(roleId);
		dRoleAdminCreate.setName(roleDto.getName());
		dRoleAdminCreate.setIsDeleted(false);
		dRoleAdminCreate.setStatus(CUserStatus.ENABLE.getValue());
		dRoleAdminCreate.setDescription(roleDto.getDescription());
		dRoleAdminDAO.create(dRoleAdminCreate);
		return dRoleAdminCreate;
	}

	/**
	 * 修改角色
	 * 
	 * @param myId 我的标识
	 * @throws WayzException 云歌广告平台异常
	 */
	@Override
	public void modifyRole(Long myId, RoleDto roleDto) throws WayzException {
		// 修改mysql信息
		this.updateRole(roleDto);
	}

	/**
	 * 修改myql数据角色信息
	 * @return
	 * @throws WayzException
	 */
	private DRoleAdmin updateRole(RoleDto roleDto) throws WayzException {
		// 检查名称
		Boolean exists = dRoleAdminDAO.existsName(roleDto.getName(), roleDto.getId());
		if (exists != null && exists.booleanValue()) {
			throw new WayzException("角色名称已经存在");
		}
		// 修改角色
		DRoleAdmin dRoleAdminModify = new DRoleAdmin();
		dRoleAdminModify.setId(roleDto.getId());
		dRoleAdminModify.setName(roleDto.getName());
		dRoleAdminModify.setDescription(roleDto.getDescription());
		dRoleAdminDAO.modify(dRoleAdminModify);

		return dRoleAdminModify;
	}

	/**
	 * 删除角色
	 * 
	 * @param myId 我的标识
	 * @param id 角色标识
	 * @throws WayzException 云歌广告平台异常
	 */
	@Override
	public void deleteRole(Long myId, Long id) throws WayzException {
		// 查询可用用户总数
		Integer userCount = userAdminService.countUserByRoleId(id);
		if (userCount > 0) {
			throw new WayzException(userCount + "位用户正使用此角色，不能删除");
		}
		dRoleAdminDAO.delete(id);
	}

	/**
	 * 给角色授权菜单方法 先清空再写入 创建和修改
	 * 
	 * @param myId
	 * @throws WayzException
	 */
	@Override
	public Integer authorityRole(Long myId, RoleMenuIdsDto roleMenuIdsDto) {
		Long roleId = roleMenuIdsDto.getRoleId();
		dRoleAdminDAO.deleteRoleMenu(roleId);
		redisUtils.delete(BeanIdKeyEnum.ROLE_MENU_HREF_ADMIN.getKey() + roleId);

		List<Long> menuIdList = roleMenuIdsDto.getMenuIdList();
		menuIdList.add(0L);
		List<String> hrefList = menuAdminService.queryMenuHref(menuIdList);
		Integer integer = dRoleAdminDAO.batchCreateRoleMenu(roleId, menuIdList);
		redisUtils.sAdd(BeanIdKeyEnum.ROLE_MENU_HREF_ADMIN.getKey() + roleId, hrefList);
		return integer;
	}
}
