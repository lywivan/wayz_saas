package com.wayz.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import com.mysql.cj.protocol.x.Notice;
import com.wayz.constants.CNoticeType;
import com.wayz.constants.PageVo;
import com.wayz.entity.dto.notice.NoticeDto;
import com.wayz.entity.pojo.notice.DNotice;
import com.wayz.entity.vo.notice.NoticeVo;
import com.wayz.exception.WayzException;
import com.wayz.mapper.DNoticeAdminDAO;
import com.wayz.mapper.DNoticeCacheKey;
import com.wayz.redis.BeanIdKeyEnum;
import com.wayz.redis.RedisUtils;
import com.wayz.util.TimeHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

/**
 * @author mike.ma@wayz.ai
 */
@Service("noticeService")
@org.apache.dubbo.config.annotation.Service(version = "1.0.0", protocol = "dubbo")
@RefreshScope
public class NoticeServiceImpl implements NoticeService {

    /** Redis工具 */
    @Resource(name = "redisUtils")
    private RedisUtils redisUtils = null;

    /** 属性相关 */
    /** 令牌超时时间(秒) */
    @Value("${user.token.timeout}")
    private Long tokenTimeout = null;
    /** 验证码超时时间(分钟) */
    @Value("${common.system.authCodeTimeout}")
    private Long authCodeTimeout = null;

    /** 消息通知DAO */
    @Resource(name = "dNoticeAdminDAO")
    private DNoticeAdminDAO dNoticeAdminDAO = null;


    /**
     * 消息列表
     * @param title        标题
     * @param startIndex   开始角标
     * @param pageSize     页面大小
     * @return
     */
    @Override
    public PageVo<NoticeVo> queryNotice(String title, Integer startIndex, Integer pageSize) {
        PageVo<NoticeVo> page = new PageVo<>();
        Integer totalCount = dNoticeAdminDAO.count(title);
        if (totalCount <= 0){
            return page;
        }
        List<NoticeVo> noticeList = new ArrayList<NoticeVo>();
        List<DNotice> dNoticesList = dNoticeAdminDAO.query(title, startIndex, pageSize);
        if (dNoticesList != null){
            for (DNotice dNotice:dNoticesList){
                NoticeVo notice = new NoticeVo();
                BeanUtils.copyProperties(dNotice, notice);
                notice.setStatusName(CNoticeType.getDescription(dNotice.getStatus()));
                notice.setPushTime(TimeHelper.getDate(dNotice.getPushTime()));
                noticeList.add(notice);
            }
        }
        // 赋值数据
        page.setTotalCount(totalCount);
        page.setList(noticeList);
        return page;
    }

    /**
     * 创建系统消息通知
     * @param NoticeDto
     * @throws WayzException
     */
    @Override
    public void createNotice(Long myId, NoticeDto NoticeDto) throws WayzException {
        Long noticeId = redisUtils.incrBy(BeanIdKeyEnum.NOTICE_SAAS.getKey(), RedisUtils.STEP_SIZE);
        // this.insertNotice(NoticeDto,noticeId,myId);

        DNotice dNotice = new DNotice();
        Date nowDate = new Date();
        BeanUtils.copyProperties(NoticeDto, dNotice);
        dNotice.setId(noticeId);
        dNotice.setType(1);
        dNotice.setCreatedTime(nowDate);
        dNotice.setOperationId(myId);

        //业务处理
        if (NoticeDto.getStatus() == 2){
            dNotice.setPushTime(nowDate);
            redisUtils.hPutBeanAsHash(DNoticeCacheKey.getNoticeCacheKey(),NoticeDto);
        }
        dNoticeAdminDAO.insertNotice(dNotice);
    }

    /**
     * 修改系统消息通知
     * @param NoticeDto
     * @throws WayzException
     */
    @Override
    public void modifyNotice(Long myId, NoticeDto NoticeDto) throws WayzException {
        // this.updateNotice(NoticeDto,myId);
        DNotice dNotice = new DNotice();
        Date nowDate = new Date();
        BeanUtils.copyProperties(NoticeDto, dNotice);
        dNotice.setCreatedTime(nowDate);
        dNotice.setOperationId(myId);
        if (NoticeDto.getStatus() == 2){
            dNotice.setPushTime(nowDate);
            redisUtils.hPutBeanAsHash(DNoticeCacheKey.getNoticeCacheKey(),dNotice);
        }
        dNoticeAdminDAO.modifyNotice(dNotice);
    }

    /**
     * 消息通知详情
     * @param id
     * @return
     */
    @Override
    public NoticeVo getNotice(Long id) {
        NoticeVo noticeVo = new NoticeVo();
        DNotice notice = dNoticeAdminDAO.getNotice(id);
        BeanUtils.copyProperties(notice, noticeVo);
        noticeVo.setStatusName(CNoticeType.getDescription(notice.getStatus()));
        return noticeVo;
    }

    /**
     * 删除系统消息通知
     * @param noticeId
     */
    @Override
    public void deleteNotice(Long noticeId) {
        dNoticeAdminDAO.deleteNotice(noticeId);
    }

    /**
     * 创建消息
     * @param NoticeDto
     * @param noticeId
     * @param operationId
     * @return
     * @throws WayzException
     */
//    private DNotice insertNotice(NoticeDto NoticeDto,Long noticeId,Long operationId) throws WayzException{
//        DNotice dNotice = new DNotice();
//        Date nowDate = new Date();
//        BeanUtils.copyProperties(NoticeDto, dNotice);
//        dNotice.setId(noticeId);
//        dNotice.setType(1);
//        dNotice.setCreatedTime(nowDate);
//        dNotice.setOperationId(operationId);
//
//        //业务处理
//        if (NoticeDto.getStatus() == 2){
//            dNotice.setPushTime(nowDate);
//            redisUtils.hPutBeanAsHash(DNoticeCacheKey.getNoticeCacheKey(),NoticeDto);
//        }
//        dNoticeAdminDAO.insertNotice(dNotice);
//        return dNotice;
//    }

    /**
     * 修改消息
     * @param NoticeDto
     * @param operationId
     */
//    private void updateNotice(NoticeDto NoticeDto,Long operationId) throws WayzException{
//        DNotice dNotice = new DNotice();
//        Date nowDate = new Date();
//        BeanUtils.copyProperties(NoticeDto, dNotice);
//        dNotice.setCreatedTime(nowDate);
//        dNotice.setOperationId(operationId);
//        if (NoticeDto.getStatus() == 2){
//            dNotice.setPushTime(nowDate);
//            redisUtils.hPutBeanAsHash(DNoticeCacheKey.getNoticeCacheKey(),dNotice);
//        }
//        dNoticeAdminDAO.modifyNotice(dNotice);
//    }

}
