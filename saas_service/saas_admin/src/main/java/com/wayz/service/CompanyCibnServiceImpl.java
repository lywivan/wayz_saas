package com.wayz.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.wayz.constants.CAuditStatus;
import com.wayz.constants.PageVo;
import com.wayz.constants.ViewKeyValue;
import com.wayz.entity.dto.cibn.CompanyCibnDto;
import com.wayz.entity.pojo.cibn.DCompanyCibn;
import com.wayz.entity.pojo.cibn.DCompanyCibnSnapshot;
import com.wayz.entity.pojo.company.DCompany;
import com.wayz.entity.vo.cibn.CompanyCibnVo;
import com.wayz.exception.WayzException;
import com.wayz.mapper.DCompanyCibnDAO;
import com.wayz.mapper.DCompanyCibnSnapshotDAO;
import com.wayz.mapper.DCompanyDAO;
import com.wayz.redis.BeanIdKeyEnum;
import com.wayz.redis.RedisUtils;
import com.wayz.util.GenSerial;
import com.wayz.util.TimeHelper;

import org.springframework.beans.BeanUtils;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

/**
 * CIBN服务实现类
 *
 * @author mike.ma@wayz.ai
 */
@Service("companyCibnService")
@org.apache.dubbo.config.annotation.Service(version = "1.0.0", protocol = "dubbo")
@RefreshScope
public class CompanyCibnServiceImpl implements CompanyCibnService {

	/**
	 * Redis工具类
	 */
	@Resource(name = "redisUtils")
	private RedisUtils redisUtils;

	/**
	 * 公司DAO
	 */
	@Resource(name = "dCompanyDAO")
	private DCompanyDAO dCompanyDAO;
	/**
	 * CIBN DAO
	 */
	@Resource(name = "dCompanyCibnDAO")
	private DCompanyCibnDAO dCompanyCibnDAO;
	/**
	 * 公司备案快照DAO
	 */
	@Resource(name = "dCompanyCibnSnapshotDAO")
	private DCompanyCibnSnapshotDAO dCompanyCibnSnapshotDAO;
	
	/**
	 * 审核状态
	 *
	 * @param myId
	 * @return
	 * @throws WayzException
	 */
	@Override
	public List<ViewKeyValue> queryAuditType(Long myId) throws WayzException {
		List<ViewKeyValue> viewList = new ArrayList<ViewKeyValue>();
		ViewKeyValue view = null;
		for (CAuditStatus cAuditStatus : CAuditStatus.values()) {
			view = new ViewKeyValue();
			view.setName(cAuditStatus.getDescription());
			view.setId(cAuditStatus.getValue());
			viewList.add(view);
		}
		return viewList;
	}

	/**
	 * 申请记录列表
	 *
	 * @param companyName 公司名称
	 * @param auditStatus 审核状态
	 * @param industryId 行业ID
	 * @return
	 * @throws WayzException
	 */
	@Override
	public PageVo<CompanyCibnVo> query(String companyName, Short auditStatus, Long industryId, Integer startIndex,
									   Integer pageSize) throws WayzException {
		// 初始化
		PageVo<CompanyCibnVo> resultList = new PageVo<>();
		List<CompanyCibnVo> cibnList = new ArrayList<>();
		Integer count = dCompanyCibnDAO.count(companyName, auditStatus, industryId);
		if (count <= 0) {
			return resultList;
		}

		List<DCompanyCibn> dCompanyCibnsList = dCompanyCibnDAO.query(companyName, auditStatus, industryId,
				startIndex, pageSize);
		CompanyCibnVo companyCibn;
		if (!dCompanyCibnsList.isEmpty()) {
			for (DCompanyCibn dCompanyCibn : dCompanyCibnsList) {
				companyCibn = new CompanyCibnVo();
				BeanUtils.copyProperties(dCompanyCibn, companyCibn);
				companyCibn.setCreatedTime(TimeHelper.getTimestamp(dCompanyCibn.getCreatedTime()));
				if (dCompanyCibn.getAuditTime() != null) {
					companyCibn.setAuditTime(TimeHelper.getTimestamp(dCompanyCibn.getAuditTime()));
				}
				cibnList.add(companyCibn);
			}
		}
		// 返回数据
		resultList.setTotalCount(count);
		resultList.setList(cibnList);
		return resultList;
	}


	/**
	 * 创建申请记录
	 *
	 * @param companyCibnDto
	 * @return
	 * @throws WayzException
	 */
	@Override
	public Long createCibn(CompanyCibnDto companyCibnDto) throws WayzException {
		Long companyId = companyCibnDto.getCompanyId();
		DCompanyCibn create = new DCompanyCibn();
		BeanUtils.copyProperties(companyCibnDto, create);
		create.setChannel("SAAS");
		String recordNumber = GenSerial.generateNewCode(8);
		recordNumber = String.format("%s" + recordNumber, "SA");
		create.setRecordNumber(recordNumber);
		create.setValidUntilDate(TimeHelper.getDate(TimeHelper.getNextYearDate()));
		dCompanyCibnDAO.create(companyId, create);
		return companyId;
	}

	/**
	 * 修改申请记录
	 *
	 * @param companyCibnDto
	 * @throws WayzException
	 */
	@Override
	public void modifyCibn(CompanyCibnDto companyCibnDto) throws WayzException {
		DCompanyCibn dCompanyCibn = dCompanyCibnDAO.get(companyCibnDto.getCompanyId());
		if (dCompanyCibn.getAuditStatus() == CAuditStatus.PENDING_AUDIT.getValue()
				|| dCompanyCibn.getAuditStatus() == CAuditStatus.PASS_AUDIT.getValue()) {
			throw new WayzException("审核中,已驳回不允许修改");
		}
		DCompanyCibn modify = new DCompanyCibn();
		BeanUtils.copyProperties(companyCibnDto, modify);
		dCompanyCibnDAO.modify(modify);
	}
	/**
	 * 审核申请记录
	 *
	 * @param companyCibnDto
	 * @throws WayzException
	 */
	@Override
	public void auditCibn(CompanyCibnDto companyCibnDto) throws WayzException {
		// 1.公司申请备案
		DCompanyCibn modify = new DCompanyCibn();
		BeanUtils.copyProperties(companyCibnDto, modify);
		if (companyCibnDto.getAuditStatus() == CAuditStatus.PASS_AUDIT.getValue()){
			modify.setValidUntilDate(TimeHelper.getTimestamp(TimeHelper.getNextYearDate()));
		}
		dCompanyCibnDAO.modifyAudit(modify);

		DCompanyCibn dCompanyCibn = dCompanyCibnDAO.get(modify.getCompanyId());

		// 2.快照表维护
		if (companyCibnDto.getAuditStatus() == CAuditStatus.PASS_AUDIT.getValue()){
			DCompany dCompany = dCompanyDAO.get(companyCibnDto.getCompanyId());
			Long id = redisUtils.incrBy(BeanIdKeyEnum.CIBN_SNAPSHOT.getKey(), RedisUtils.STEP_SIZE); // rCibnSnapshotId.increment();
			DCompanyCibnSnapshot create = new DCompanyCibnSnapshot();
			create.setCompanyId(companyCibnDto.getCompanyId());
			create.setName(dCompany.getName());
			create.setRecordNumber(dCompanyCibn.getRecordNumber());
			create.setEquipmentNumber(dCompanyCibn.getEquipmentNumber());
			create.setContacts(dCompany.getContacts());
			create.setTelephone(dCompany.getTelephone());
			create.setAddress(dCompany.getAddress());
			create.setChannel("SAAS");
			create.setOriginId(companyCibnDto.getCompanyId());
			create.setValidUntilDate(TimeHelper.getDate(TimeHelper.getNextYearDate()));
			create.setAuditTime(TimeHelper.getCurrentTimestamp());
			dCompanyCibnSnapshotDAO.create(id,create);
		}
	}

	/**
	 * 申请记录详情 -- 管理端
	 *
	 * @param companyId
	 * @return
	 */
	@Override
	public CompanyCibnVo getAdminCibn(Long companyId) throws WayzException {
		DCompanyCibn dCompanyCibn = dCompanyCibnDAO.get(companyId);
		if (dCompanyCibn == null) {
			throw new WayzException("申请记录有误");
		}
		CompanyCibnVo companyCibn = new CompanyCibnVo();
		BeanUtils.copyProperties(dCompanyCibn, companyCibn);
		companyCibn.setCreatedTime(TimeHelper.getTimestamp(dCompanyCibn.getCreatedTime()));
		if (dCompanyCibn.getAuditTime() != null) {
			companyCibn.setAuditTime(TimeHelper.getTimestamp(dCompanyCibn.getAuditTime()));
		}
		return companyCibn;
	}

	/**
	 * 申请记录详情 -- 服务端
	 *
	 * @param companyId
	 * @return
	 */
	@Override
	public CompanyCibnVo getUserCibn(Long companyId) throws WayzException {
		DCompanyCibn dCompanyCibn = dCompanyCibnDAO.get(companyId);
		CompanyCibnVo companyCibnVo = new CompanyCibnVo();
		if (dCompanyCibn != null) {
			BeanUtils.copyProperties(dCompanyCibn, companyCibnVo);
			companyCibnVo.setCreatedTime(TimeHelper.getDate(dCompanyCibn.getCreatedTime()));
			return companyCibnVo;
		}
		return null;
	}

}
