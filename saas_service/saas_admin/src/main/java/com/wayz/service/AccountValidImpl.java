package com.wayz.service;

import com.wayz.constants.CCompanyStatus;
import com.wayz.constants.CPositionStatus;
import com.wayz.constants.CShortMessageType;
import com.wayz.entity.pojo.base.DLongAndInt;
import com.wayz.entity.pojo.company.DCompany;
import com.wayz.entity.pojo.position.DAdvPosition;
import com.wayz.entity.redis.company.RCompany;
import com.wayz.entity.redis.play.RPlayer;
import com.wayz.exception.WayzException;
import com.wayz.mapper.DAdvPositionDAO;
import com.wayz.mapper.DCompanyCouponDAO;
import com.wayz.mapper.DCompanyDAO;
import com.wayz.redis.RCompanyObject;
import com.wayz.redis.RPlayerObject;
import com.wayz.redis.RPositionIdPlayerCodeValue;
import com.wayz.service.AccountValid;
import com.wayz.service.SmsService;
import com.wayz.util.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author wade.liu@wayz.ai
 *
 */
@Service("accountValid")
public class AccountValidImpl implements AccountValid {
	/** 日志 */
	private static final Logger LOGGER = LoggerFactory.getLogger("ACCOUNT_TASK");

	/** DAO相关 */
	/** 公司DAO接口 */
	@Resource(name = "dCompanyDAO")
	private DCompanyDAO dCompanyDAO = null;
	/** 媒体位DAO接口 */
	@Resource(name = "dAdvPositionDAO")
	private DAdvPositionDAO dAdvPositionDAO = null;
	/** 公司代金券DAO接口 */
	@Resource(name = "dCompanyCouponDAO")
	private DCompanyCouponDAO dCompanyCouponDAO = null;

	/** 缓存接口 */
	/** 公司对象 */
	@Resource(name = "rCompanyObject")
	private RCompanyObject rCompanyObject = null;
	/** 播控对象 */
	@Resource(name = "rPlayerObject")
	private RPlayerObject rPlayerObject = null;
	/** 媒体位标识播控器编码值 */
	@Resource(name = "rPositionIdPlayerCodeValue")
	private RPositionIdPlayerCodeValue rPositionIdPlayerCodeValue = null;

	/** 服务相关 */
	/** 短信服务 */
	@Autowired
	private SmsService smsService = null;

	/*
	 * 检查试用账户 过期失效
	 * 
	 * @see
	 */
	@Override
	public void checkTestingAccount() throws WayzException {
		// 获取已过期的试用账户
		List<DCompany> dCompanyList = dCompanyDAO.queryTestingAndInvalid();
		if (CollectionUtils.isEmpty(dCompanyList)) {
			return;
		}
		for (DCompany dCompany : dCompanyList) {
			LOGGER.debug("处理账户过期:" + dCompany.getName());

			// 失效媒体位
			invalidPositionByCompany(dCompany.getId());

			// 失效账户
			dCompanyDAO.modifyStatus(dCompany.getId(), CCompanyStatus.EXPIRED.getValue());
			rCompanyObject.setShort(dCompany.getId(), RCompany.STATUS, CCompanyStatus.EXPIRED.getValue());

			// 发送短信通知 TODO
		}

	}

	/**
	 * 失效账户下所有媒体位
	 * 
	 * @param companyId 公司标识
	 */
	private void invalidPositionByCompany(Long companyId) {
		// 获取账户下媒体位
		List<Long> positionIdList = dAdvPositionDAO.queryIdByCompany(companyId);
		if (CollectionUtils.isEmpty(positionIdList)) {
			return;
		}
		// 设置账户下媒体位无效
		dAdvPositionDAO.invalidByCompany(companyId);

		// 维护缓存
		for (Long id : positionIdList) {
			String code = rPositionIdPlayerCodeValue.get(id);
			if (StringUtils.isEmpty(code)) {
				continue;
			}
			rPlayerObject.setShort(code, RPlayer.STATUS, CPositionStatus.EXPIRED.getValue());
		}
	}

	/*
	 * 设置过期点位状态
	 */
	@Override
	public void invalidPosition() throws WayzException {
		// 获取过期的点位列表
		List<DAdvPosition> dPositionList = dAdvPositionDAO.queryInvalidList();
		if (CollectionUtils.isEmpty(dPositionList)) {
			return;
		}

		// 设置数据库 点位过期
		dAdvPositionDAO.updateInvalidList();
		Set<Long> companyIdList = new HashSet<Long>();
		for (DAdvPosition dPosition : dPositionList) {
			companyIdList.add(dPosition.getCompanyId());
			// 维护设备缓存
			String code = rPositionIdPlayerCodeValue.get(dPosition.getId());
			if (StringUtils.isEmpty(code)) {
				continue;
			}
			rPlayerObject.setShort(code, RPlayer.STATUS, CPositionStatus.EXPIRED.getValue());
		}

		// 通知租户过期
	}

	/**
	 * 
	 * 处理代金券失效
	 * 
	 * @throws WayzException
	 */
	@Override
	public void invalidCoupon() throws WayzException {
		dCompanyCouponDAO.invalidCoupon();
	}

	/**
	 *
	 * 设备到期提醒
	 *
	 * @throws WayzException
	 */
	@Override
	public void remindExpire() throws WayzException {
		// 统计即将到期的点位
		List<DLongAndInt> dCompanyList = dAdvPositionDAO.statWillExpire();
		if (CollectionUtils.isEmpty(dCompanyList)) {
			return;
		}
		// 依次提醒
		for (DLongAndInt dVal : dCompanyList) {
			RCompany rCompany = rCompanyObject.get(dVal.getId());
			if (StringUtils.isEmpty(rCompany.getTelephone())) {
				continue;
			}
			smsService.sendMessage(rCompany.getTelephone(), CShortMessageType.POSITION_EXPIRE, rCompany.getName() + ","
					+ dVal.getVal());
		}
	}
}
