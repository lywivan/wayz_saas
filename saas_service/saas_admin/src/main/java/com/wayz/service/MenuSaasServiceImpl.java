package com.wayz.service;

import com.wayz.entity.pojo.menu.DMenu;
import com.wayz.entity.redis.play.RFunctionVersion;
import com.wayz.entity.vo.menu.MenuVo;
import com.wayz.mapper.DMenuSaasDAO;
import com.wayz.redis.RFunctionVersionObject;
import com.wayz.redis.RedisUtils;
import com.wayz.util.CollectionUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.BeanUtils;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @className: MenuSaasServiceImpl
 * @description: saas菜单 <功能详细描述>
 * @author wade.liu@wayz.ai
 * @date: 2019/6/21
 */
@Service(version = "1.0.0", protocol = "dubbo")
@RefreshScope
public class MenuSaasServiceImpl implements MenuSaasService {

	@Resource(name = "dMenuSaasDAO")
	private DMenuSaasDAO dMenuSaasDAO;

	@Resource(name = "redisUtils")
	private RedisUtils redisUtils;
	@Resource(name = "rFunctionVersionObject")
	private RFunctionVersionObject rFunctionVersionObject;

	/**
	 * 查询全部的saas菜单
	 * 
	 * @param myId 我的标识
	 * @return
	 */
	@Override
	public List<MenuVo> queryAllMenu(Long myId) {
		List<MenuVo> menuSaasList = new ArrayList<>();
		List<DMenu> dMenuSaasList = dMenuSaasDAO.queryAll();
		this.resolveMyMenuTree(menuSaasList, dMenuSaasList, -1L);
		return menuSaasList;
	}

	/**
	 *
	 * <p>树形转换</p>
	 * 
	 * @param parentId
	 * @date 2016年12月2日
	 */
	private void resolveMyMenuTree(List<MenuVo> menuSaasList, List<DMenu> dMenuSaasList, Long parentId) {
		if (!CollectionUtils.isEmpty(dMenuSaasList)) {
			for (DMenu dMenu : dMenuSaasList) {
				if (dMenu.getParentId().equals(parentId)) {
					// 初始化
					MenuVo children = new MenuVo();

					// 设置参数
					children.setId(dMenu.getId());
					children.setParentId(dMenu.getParentId());
					children.setName(dMenu.getName());
					children.setHref(dMenu.getHref());
					children.setIcon(dMenu.getIcon());
					// children.setSort(dUserMyMenu.getSort());
					// 子节点
					children.setChildList(new ArrayList<MenuVo>());
					// 添加
					menuSaasList.add(children);
					// 递归
					resolveMyMenuTree(children.getChildList(), dMenuSaasList, dMenu.getId());
				}
			}
		}
	}

	/**
	 * 查询版本选中的菜单
	 * 
	 * @param myId
	 * @param versionId
	 * @return
	 */
	@Override
	public List<MenuVo> queryMenuByVersionId(Long myId, Long versionId) {

		// 获取角色ID
		Long roleId = rFunctionVersionObject.getLong(versionId, RFunctionVersion.ROLEID);
		// 查询当前版本选中的菜单
		List<DMenu> dMenuSaasList = dMenuSaasDAO.queryMenuByVersionId(roleId);
		if (CollectionUtils.isEmpty(dMenuSaasList)) {
			return new ArrayList<>(0);
		}
		// 树形转化
		List<MenuVo> menuSaasList = new ArrayList<>();
		MenuVo menuSaas;
		for (DMenu dMenuSaas : dMenuSaasList) {
			menuSaas = new MenuVo();
			BeanUtils.copyProperties(dMenuSaas, menuSaas);
			menuSaasList.add(menuSaas);
		}
		return menuSaasList;
	}

	/**
	 * 查询全部菜单编号
	 * 
	 * @return
	 */
	@Override
	public List<Long> queryMenuIdList() {
		return dMenuSaasDAO.queryMenuIdList();
	}
}
