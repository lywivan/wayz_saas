package com.wayz.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.wayz.constants.CMenuStatus;
import com.wayz.constants.CMenuType;

import com.wayz.entity.redis.meun.RUserAdmin;
import com.wayz.entity.pojo.menu.DMenu;
import com.wayz.entity.vo.menu.MenuVo;
import com.wayz.exception.WayzException;
import com.wayz.mapper.DMenuAdminDAO;
import com.wayz.redis.BeanIdKeyEnum;
import com.wayz.redis.RedisUtils;
import com.wayz.util.CollectionUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.BeanUtils;

import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * 菜单服务实现类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Service(version = "1.0.0", protocol = "dubbo")
@RefreshScope
public class MenuAdminServiceImpl implements MenuAdminService  {

		/** DAO相关 */
		/** 菜单DAO接口 */
		@Resource(name = "dMenuAdminDAO")
		private DMenuAdminDAO dMenuAdminDAO = null;

		/** 对象相关 */
		/** 菜单标识 */
		@Resource(name = "redisUtils")
		private RedisUtils redisUtils = null;

		/**
		 * 查询我的菜单
		 *
		 * @param myId 我的标识
		 * @return 我的菜单列表
		 * @throws WayzException 云歌广告平台异常
		 */
		@Override
		public List<MenuVo> queryMyMenu(Long myId) {
			// 初始化
			List<MenuVo> myMenuList = new ArrayList<MenuVo>();
			RUserAdmin rUserAdmin = redisUtils.hGetAllAsBean(RUserAdmin.getKey(myId), RUserAdmin.class);
			List<DMenu> roleMenusList = dMenuAdminDAO.queryMenuRole(rUserAdmin.getRoleId(), CMenuType.MENU.getValue());
			resolveMyMenuTree(myMenuList, roleMenusList, -1L);
			// 返回数据
			return myMenuList;
		}

		/**
		 *
		 * <p>树形转换</p>
		 *
		 * @param menu
		 * @param dMenuAdminList
		 * @param parentId
		 * @date 2016年12月2日
		 */
		private void resolveMyMenuTree(List<MenuVo> menu, List<DMenu> dMenuAdminList, Long parentId) {
			if (dMenuAdminList != null && dMenuAdminList.size() > 0) {
				for (DMenu dMenuAdmin : dMenuAdminList) {
					if (dMenuAdmin.getParentId().equals(parentId)) {
						// 初始化
						MenuVo children = new MenuVo();

						// 设置参数
						children.setId(dMenuAdmin.getId());
						children.setParentId(dMenuAdmin.getParentId());
						children.setName(dMenuAdmin.getName());
						children.setHref(dMenuAdmin.getHref());
						children.setIcon(dMenuAdmin.getIcon());
						// children.setSort(dUserMyMenu.getSort());
						// 子节点
						children.setChildList(new ArrayList<MenuVo>());
						// 添加
						menu.add(children);
						// 递归
						resolveMyMenuTree(children.getChildList(), dMenuAdminList, dMenuAdmin.getId());
					}
				}
			}
		}

		/**
		 * 查询全部菜单-树状结构返回
		 *
		 * @param myId 我的标识
		 * @return 我的菜单列表
		 * @throws WayzException 云歌广告平台异常
		 */
		@Override
		public List<MenuVo> queryAllMenu(Long myId) {// 初始化
			// 查询数据: 菜单列表
			List<DMenu> dMenuAdminList = dMenuAdminDAO.queryByStatus(CMenuStatus.SHOWED.getValue());
			List<MenuVo> myMenuList = new ArrayList<MenuVo>();
			resolveMyMenuTree(myMenuList, dMenuAdminList, -1L);
			// 返回数据
			return myMenuList;
		}

		/**
		 * 查询我的菜单
		 *
		 * @param myId 我的标识
		 * @param href 菜单链接
		 * @return 检查结果
		 * @throws WayzException 云歌广告平台异常
		 */
		@Override
		public Boolean checkMyMenu(Long myId, String href) throws WayzException {
			// 获取用户
			RUserAdmin rUserAdmin = redisUtils.hGetAllAsBean(RUserAdmin.getKey(myId), RUserAdmin.class);
			Long roleId = rUserAdmin.getRoleId();
			// 检查菜单
			Boolean result = redisUtils.sIsMember(BeanIdKeyEnum.ROLE_MENU_HREF_ADMIN.getKey() + roleId, href);
			// 返回成功
			return result;
		}



		/**
		 * 删除菜单
		 *
		 * @param myId 我的客户标识
		 * @param id 菜单标识
		 * @throws WayzException 平台异常
		 */
		@Override
		public void deleteMenu(Long myId, Long id) throws WayzException {
			// 校验菜单标识是否存在
			DMenu dMenuAdmin = dMenuAdminDAO.get(id);
			if (dMenuAdmin == null) {
				throw new WayzException("菜单标识不存在!");
			}
			// 删除
			dMenuAdminDAO.delete(id);
		}

		/**
		 * 查询角色拥有的菜单和权限
		 *
		 * @param myId 我的标识
		 * @param roleId 角色标识
		 * @return 权限列表
		 * @throws WayzException 云歌广告平台异常
		 */
		@Override
		public List<DMenu> queryRoleMenu(Long myId, Long roleId) {
			// 初始化
			// 查询权限
			List<DMenu> dMenuAdmins = dMenuAdminDAO.queryMenuRole(roleId, null);

			if (CollectionUtils.isEmpty(dMenuAdmins)) {
				return new ArrayList<>(0);
			}
			List<DMenu> menuList = new ArrayList<>(dMenuAdmins.size());
			DMenu menu;
			for (DMenu dMenuAdmin : dMenuAdmins) {
				menu = new DMenu();
				BeanUtils.copyProperties(dMenuAdmin, menu);
				menuList.add(menu);
			}
			// 返回数据
			return menuList;
		}

		/**
		 * 查询我的视图权限
		 *
		 * @param myId 我的标识
		 * @param view 视图名称
		 * @return 视图权限列表
		 * @throws WayzException 云歌广告平台异常
		 */
		@Override
		public List<String> queryViewAuthority(Long myId, String view) throws WayzException {
			//查询出当前路径的菜单对象
			DMenu menuByHref = dMenuAdminDAO.getMenuByHref(view);
			if (menuByHref == null) {
				return new ArrayList<>(0);
			}
			//查询当前对象的所有权限
			RUserAdmin rUserAdmin = redisUtils.hGetAllAsBean(RUserAdmin.getKey(myId), RUserAdmin.class);
			return dMenuAdminDAO.queryHrefByRoleIdAndType(rUserAdmin.getRoleId(),CMenuType.SHOWED.getValue(),menuByHref.getId());
		}

		/**
		 * 根据菜单id批量获取菜单连接
		 * @param menuIdList
		 * @return
		 */
		@Override
		public List<String> queryMenuHref(List<Long> menuIdList) {
			return dMenuAdminDAO.queryMenuHref(menuIdList);
		}

		/**
		 * 初始化时使用
		 * @return
		 */
		@Override
		public List<Long> menuIdList() {
			List<Long> menuIdList = dMenuAdminDAO.queryAllMenuId();
			return menuIdList;
		}
}

