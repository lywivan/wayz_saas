package com.wayz.service;

import com.wayz.constants.PageVo;
import com.wayz.entity.dto.playVersion.PlayVersionDto;
import com.wayz.entity.pojo.playVersion.DPlayVersion;
import com.wayz.entity.redis.company.RCompany;
import com.wayz.entity.redis.playVersion.RPlayerVersion;
import com.wayz.entity.vo.playVersion.PlayVersionVo;
import com.wayz.exception.WayzException;
import com.wayz.mapper.PlayVersionDAO;
import com.wayz.redis.RCompanyObject;
import com.wayz.redis.RedisUtils;
import com.wayz.util.TimeHelper;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.BeanUtils;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @className: PlayVersionServiceImpl
 * @description: 播控版本 <功能详细描述>
 * @author: 须尽欢_____
 * @date: 2019/7/9
 */
@Service(version = "1.0.0", protocol = "dubbo")
@RefreshScope
public class PlayVersionServiceImpl implements PlayVersionService {

	@Resource(name = "playVersionDAO")
	private PlayVersionDAO playVersionDAO;

	@Resource(name = "redisUtils")
	private RedisUtils redisUtils = null;

	@Resource(name = "rCompanyObject")
	private RCompanyObject rCompanyObject;

	/**
	 * 上传播控版本
	 */
	@Override
	public Integer create(PlayVersionDto PlayVersionDto) throws WayzException {
		DPlayVersion PlayVersion = playVersionDAO.findByVersion(PlayVersionDto.getVersion());
		if (PlayVersion != null) {
			throw new WayzException("版本编号已存在");
		}
		PlayVersion = new DPlayVersion();
		BeanUtils.copyProperties(PlayVersionDto, PlayVersion);
		// DUploadFile dUploadFile =
		// fileUploadDAO.findByUrl(PlayVersionDto.getPackageUrl());
		// String filemd5 = dUploadFile.getFilemd5();
		// PlayVersion.setFileMd5(filemd5);
		Integer integer = playVersionDAO.create(PlayVersion);
		RPlayerVersion rPlayerVersion = new RPlayerVersion();
		BeanUtils.copyProperties(PlayVersionDto, rPlayerVersion);
		// rPlayerVersion.setFileMd5(filemd5);
		rPlayerVersion.setCreatedTime(TimeHelper.getDate());
		redisUtils.hPutBeanAsHash(RPlayerVersion.getKey(PlayVersionDto.getVersion()), rPlayerVersion);
		return integer;
	}

	@Override
	public PageVo<PlayVersionVo> queryAll(Integer startIndex, Integer pageSize) {
		PageVo<PlayVersionVo> PlayVersionDtoPage = new PageVo<>();
		Integer count = playVersionDAO.count();
		if (count == 0) {
			return PlayVersionDtoPage;
		}
		List<DPlayVersion> PlayVersionList = playVersionDAO.findAll(startIndex, pageSize);
		List<PlayVersionVo> PlayVersionDtoList = new ArrayList<>(PlayVersionList.size());
		for (DPlayVersion PlayVersion : PlayVersionList) {
			PlayVersionVo playVersionVo = new PlayVersionVo();
			BeanUtils.copyProperties(PlayVersion, playVersionVo);
			if (playVersionVo.getCompanyId() != null) {
				String cName = rCompanyObject.getString(playVersionVo.getCompanyId(), RCompany.NAME);
				playVersionVo.setCompanyName(cName);
			}
			playVersionVo.setCreatedTime(TimeHelper.getDate(PlayVersion.getCreatedTime()));
			PlayVersionDtoList.add(playVersionVo);
		}
		PlayVersionDtoPage.setTotalCount(count);
		PlayVersionDtoPage.setList(PlayVersionDtoList);
		return PlayVersionDtoPage;
	}

	@Override
	public Integer grantCompany(String version, List<Long> companyIdList) {
		playVersionDAO.deleteByVersion(version);
		return playVersionDAO.insertPlayVersionCompany(version, companyIdList);
	}

	@Override
	public List<Long> queryCompanyIdListByVersion(String version) {
		return playVersionDAO.findCompanyIdListByVersion(version);
	}
}
