package com.wayz.service;

import com.wayz.constants.ViewMap;
import com.wayz.mapper.DFunctionVersionDAO;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import javax.annotation.Resource;
import java.util.List;

/**
 * 功能版本服务实现类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Service(version = "1.0.0", protocol = "dubbo")
@RefreshScope
public class VersionServiceImpl implements VersionService {
	/** DAO 相关 */
	@Resource(name = "dFunctionVersionDAO")
	private DFunctionVersionDAO dFunctionVersionDAO;

	/**
	 * 查询功能版本视图
	 *
	 * @param myId
	 * @return
	 * @throws YungeException
	 */
	@Override
	public List<ViewMap> queryVersionView(Long myId) {
		return dFunctionVersionDAO.queryVersionViewMap();
	}

	/**
	 * 根据Id获取版本管理员角色Id
	 *
	 * @param myId
	 * @param versionId
	 * @return
	 */
	@Override
	public Long getRoleIdByVersionId(Long myId, Long versionId) {
		return dFunctionVersionDAO.getRoleIdById(versionId);
	}
}
