package com.wayz.service;

import com.wayz.constants.CUserStatus;
import com.wayz.constants.CUserType;
import com.wayz.entity.pojo.user.DUserSaas;
import com.wayz.entity.pojo.user.DUserSaasExt;
import com.wayz.entity.redis.user.RUser;
import com.wayz.entity.vo.company.CompanyVo;
import com.wayz.exception.WayzException;
import com.wayz.mapper.DUserSaasDAO;
import com.wayz.redis.RUserId;
import com.wayz.redis.RUserObject;
import com.wayz.util.FileMD5Helper;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import javax.annotation.Resource;

/**
 * @className: UserSaasServiceImpl
 * @description: saas用户 <功能详细描述>
 * @author wade.liu@wayz.ai
 * @date: 2019/6/27
 */
@Service(version = "1.0.0", protocol = "dubbo")
@RefreshScope
public class UserSaasServiceImpl implements UserSaasService {
	@Resource(name = "dUserSaasDAO")
	private DUserSaasDAO dUserSaasDAO;

	@Resource(name = "rUserId")
	private RUserId rUserId;

	@Resource(name = "rUserObject")
	private RUserObject rUserObject;

	@Value("${domain.name}")
	private String domainName = null;

	/**
	 * 创建用户
	 *
	 * @param myId 我的标识
	 * @param roleId 角色标识
	 * @param username 用户名称
	 * @param name 用户姓名
	 * @param phone 用户电话
	 * @param email 用户邮箱
	 * @return 用户标识
	 * @throws WayzException 云歌广告平台异常
	 */
	@Override
	public Long createUser(Long myId, Long companyId, Long roleId, String username, String name, String phone,
						   String email) throws WayzException {
//		DUserSaas create = this.insertUserSaas(myId, companyId, roleId, username, name, phone, email);
//		this.cacheUserSaas(create);
//		return create.getId();

		// 校验用户名是否存在 手机号
		Integer count = dUserSaasDAO.countUserSaasByPhone(phone);
		if (count > 0) {
			throw new WayzException("用户手机号已存在");
		}
		count = dUserSaasDAO.countUserSaasByUserName(username);
		if (count > 0) {
			throw new WayzException("用户名已存在");
		}
		Long id = rUserId.increment();
		DUserSaas dUserSaasCreate = new DUserSaas();
		dUserSaasCreate.setId(id);
		dUserSaasCreate.setCompanyId(companyId);
		dUserSaasCreate.setDepartId(0L);
		dUserSaasCreate.setDepartPath("0/");
		dUserSaasCreate.setRoleId(roleId);
		dUserSaasCreate.setType(CUserType.MAIN_ACCOUNT.getValue());
		dUserSaasCreate.setCreatorId(myId);
		dUserSaasCreate.setName(name);
		dUserSaasCreate.setStatus(CUserStatus.ENABLE.getValue());
		dUserSaasCreate.setIsDeleted(false);
		dUserSaasCreate.setUsername(username);
		dUserSaasCreate.setPassword(FileMD5Helper.getMD5String("123456"));
		dUserSaasCreate.setPhone(phone);
		dUserSaasCreate.setEmail(email);
		dUserSaasDAO.create(dUserSaasCreate);

		RUser rUser = new RUser();
		BeanUtils.copyProperties(dUserSaasCreate, rUser);
		rUserObject.set(dUserSaasCreate.getId(), rUser);

		return id;
	}
 
	/**
	 * 将用户信息存入数据库
	 *
	 * @param myId
	 * @param companyId
	 * @param roleId
	 * @param username
	 * @param name
	 * @param phone
	 * @param email
	 * @return
	 * @throws WayzException
	 */
//	private DUserSaas insertUserSaas(Long myId, Long companyId, Long roleId, String username, String name,
//										   String phone, String email) throws WayzException {
//		// 校验用户名是否存在 手机号
//		Integer count = dUserSaasDAO.countUserSaasByPhone(phone);
//		if (count > 0) {
//			throw new WayzException("用户手机号已存在");
//		}
//		count = dUserSaasDAO.countUserSaasByUserName(username);
//		if (count > 0) {
//			throw new WayzException("用户名已存在");
//		}
//		Long id = rUserId.increment();
//		DUserSaas dUserSaasCreate = new DUserSaas();
//		dUserSaasCreate.setId(id);
//		dUserSaasCreate.setCompanyId(companyId);
//		dUserSaasCreate.setDepartId(0L);
//		dUserSaasCreate.setDepartPath("0/");
//		dUserSaasCreate.setRoleId(roleId);
//		dUserSaasCreate.setType(CUserType.MAIN_ACCOUNT.getValue());
//		dUserSaasCreate.setCreatorId(myId);
//		dUserSaasCreate.setName(name);
//		dUserSaasCreate.setStatus(CUserStatus.ENABLE.getValue());
//		dUserSaasCreate.setIsDeleted(false);
//		dUserSaasCreate.setUsername(username);
//		dUserSaasCreate.setPassword(FileMD5Helper.getMD5String("123456"));
//		dUserSaasCreate.setPhone(phone);
//		dUserSaasCreate.setEmail(email);
//		dUserSaasDAO.create(dUserSaasCreate);
//		return dUserSaasCreate;
//	}

	/**
	 * 存入缓存
	 *
	 * @param dUserSaasCreate
	 */
//	void cacheUserSaas(DUserSaas dUserSaasCreate) {
//		RUser rUser = new RUser();
//		BeanUtils.copyProperties(dUserSaasCreate, rUser);
//		rUserObject.set(dUserSaasCreate.getId(), rUser);
//	}

	/**
	 * 获取公司主账户
	 *
	 * @param companyId 公司标识
	 * @return 云歌应答
	 * @throws
	 */
	@Override
	public CompanyVo getCompanyMainAccount(Long companyId, Short type) throws WayzException {
		CompanyVo companyVO = new CompanyVo();
		DUserSaas companyMainAccount = dUserSaasDAO.getCompanyMainAccount(companyId, type);
		if (companyMainAccount == null) {
			return null;
		}
		BeanUtils.copyProperties(companyMainAccount, companyVO);
		companyVO.setDomainName(domainName);
		return companyVO;
	}

	/**
	 * 变更用户角色
	 *
	 * @param id
	 * @param roleId
	 * @return
	 */
	@Override
	public Integer updateUserSaasRoleId(Long id, Long roleId) {
		return dUserSaasDAO.updateUserSaasRoleId(id, roleId);
	}

	/**
	 * 根据手机号查询用户
	 *
	 * @param phone
	 * @return
	 */
	@Override
	public DUserSaas getUserSaasByPhone(String phone) {
		DUserSaasExt userSaasByPhone = dUserSaasDAO.getUserSaasByPhone(phone);
		if (userSaasByPhone == null) {
			return null;
		}
		DUserSaas userSaas = new DUserSaas();
		BeanUtils.copyProperties(userSaasByPhone, userSaas);
		return userSaas;
	}

	/**
	 * 变更用户手机号
	 *
	 * @param id 用户标识
	 * @param phone 手机号
	 * @return
	 */
	@Override
	public void modifyUserPhone(Long id, String phone) {
		dUserSaasDAO.modifyUserPhone(id, phone);
	}

	/**
	 * 修改联系人
	 *
	 * @param id 用户标识
	 * @param contacts 联系人
	 */
	@Override
	public void modifyUserName(Long id, String contacts) {
		dUserSaasDAO.modifyUserName(id, contacts);
	}

	/**
	 * 重置主账号密码
	 *
	 * @param companyId
	 * @return
	 * @throws
	 */
	@Override
	public void resetUserPassword(Long companyId, Short type, String newPassword) throws WayzException {
		DUserSaas companyMainAccount = dUserSaasDAO.getCompanyMainAccount(companyId, type);
		if (companyMainAccount == null) {
			throw new WayzException("账户异常");
		}
		dUserSaasDAO.modifyUserPassword(companyMainAccount.getId(), newPassword);
	}
}
