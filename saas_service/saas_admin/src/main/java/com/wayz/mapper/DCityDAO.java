package com.wayz.mapper;

import java.util.List;

import com.wayz.entity.pojo.home.DCity;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 城市DAO接口
 * 
 * @author mike.ma@wayz.ai
 *
 */
@Repository("dCityDAO")
public interface DCityDAO {

	/**
	 * 获取城市信息
	 * 
	 * @param id 城市标识
	 * @return 城市信息
	 */
	public DCity get(@Param("id") Long id);

	/**
	 * 查询城市信息
	 * 
	 * @param provinceId 省份标识
	 * @return 城市信息列表
	 */
	public List<DCity> query(@Param("provinceId") Long provinceId);

}
