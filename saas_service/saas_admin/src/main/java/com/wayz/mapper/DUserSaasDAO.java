package com.wayz.mapper;

import com.wayz.entity.pojo.user.DUserSaas;
import com.wayz.entity.pojo.user.DUserSaasExt;
import com.wayz.exception.WayzException;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @className: DUserSaasDAO
 * @description: saas用户 <功能详细描述>
 * @author wade.liu@wayz.ai
 * @date: 2019/6/27
 */
@Repository("dUserSaasDAO")
public interface DUserSaasDAO {
	/**
	 * 根据用户手机号统计用户数量
	 *
	 * @param phone
	 * @return
	 */
	Integer countUserSaasByPhone(@Param("phone") String phone);

	/**
	 * 根据用户名统计用户数量
	 *
	 * @param userName
	 * @return
	 */
	Integer countUserSaasByUserName(@Param("userName") String userName);

	/**
	 * 创建saas用户
	 *
	 * @param dUserSaasCreate
	 * @return
	 */
	Integer create(@Param("create") DUserSaas dUserSaasCreate);

	/**
	 * 根据角色标识 检查是否存在用户
	 *
	 * @param roleId 角色标识
	 * @return 是否存在
	 * @throws WayzException
	 */
	Boolean existByRoleId(@Param("roleId") Long roleId) throws WayzException;

	/**
	 * 获取公司主账户
	 *
	 * @return 公司主账户
	 */
	DUserSaas getCompanyMainAccount(@Param("companyId") Long companyId, @Param("type") Short type)
			throws WayzException;

	/**
	 * 更新用户角色
	 *
	 * @param id
	 * @param roleId
	 * @return
	 */
	Integer updateUserSaasRoleId(@Param("id") Long id, @Param("roleId") Long roleId);

	/**
	 * 根据手机号查询用户信息
	 *
	 * @param phone
	 * @return
	 */
	DUserSaasExt getUserSaasByPhone(@Param("phone") String phone);

	/**
	 * 变更用户手机号
	 *
	 * @param id 用户标识
	 * @param phone 手机号
	 * @return
	 */
	void modifyUserPhone(@Param("id") Long id, @Param("phone") String phone);

	/**
	 * 修改联系人
	 * @param id 用户标识
	 * @param contacts 联系人
	 */
	void modifyUserName(@Param("id") Long id,@Param("contacts") String contacts);

	void modifyUserPassword(@Param("id") Long id, @Param("password") String password);
}
