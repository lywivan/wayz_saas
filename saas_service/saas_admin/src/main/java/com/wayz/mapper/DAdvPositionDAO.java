package com.wayz.mapper;

import com.wayz.entity.pojo.base.DLongAndInt;
import com.wayz.entity.pojo.position.DAdvPosition;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 
 * <p>媒体位DAO接口</p>
 * 
 * @author mahao
 */
@Repository("dAdvPositionDAO")
public interface DAdvPositionDAO {
	/**
	 * 查询公司账户下有效点位
	 *
	 * @param companyId 公司标识
	 * @return 媒体位标识列表
	 */
	public List<Long> queryIdByCompany(@Param("companyId") Long companyId);

	/**
	 * 设置账户下点位为无效
	 *
	 * @param companyId 公司标识
	 */
	public void invalidByCompany(@Param("companyId") Long companyId);

	/**
	 * 查询已过期的点位
	 *
	 * @return 过期点位列表
	 */
	public List<DAdvPosition> queryInvalidList();

	/**
	 * 更新已过期点位
	 */
	public void updateInvalidList();

	/**
	 * 统计即将到期设备数
	 *
	 * @return
	 */
	public List<DLongAndInt> statWillExpire();

}
