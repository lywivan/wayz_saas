package com.wayz.mapper;

import com.wayz.entity.dto.user.UserAdminDto;
import com.wayz.entity.pojo.user.DUserAdmin;
import com.wayz.entity.pojo.user.DUserAdminExt;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户DAO接口
 * 
 * @author yinjy
 *
 */
@Repository("dUserAdminDAO")
public interface DUserAdminDAO {

	/**
	 * 获取用户扩展
	 *
	 * @param id 用户标识
	 * @return 用户扩展
	 */
	DUserAdminExt getExt(@Param("id") Long id);

	/**
	 * 根据手机号获取用户信息
	 *
	 * @param username 手机号
	 * @return
	 */
	DUserAdmin getByUsernameOrPhone(@Param("username") String username);

	/**
	 * 是否存在手机号
	 *
	 * @param id 用户标识
	 * @param phone 手机号
	 * @return 是否存在
	 */
	Boolean existsPhone(@Param("phone") String phone, @Param("id") Long id);

	/**
	 * 存在用户名
	 *
	 * @return 是否存在
	 */
	boolean existsUsername(@Param("username") String username);

	/**
	 * 创建用户信息
	 *
	 * @param create 用户创建信息
	 * @return 创建行数
	 */
	Integer create(@Param("create") DUserAdmin create);

	/**
	 * 根据条件查询用户扩展
	 *
	 * @param status 用户状态(0:禁用;1:启用)
	 * @param name 用户姓名(模糊匹配)
	 * @param startIndex 开始序号
	 * @param pageSize 页面大小
	 * @return 用户分页
	 */
	List<DUserAdminExt> queryExtByCondition(@Param("status") Short status, @Param("name") String name,
											@Param("startIndex") Integer startIndex, @Param("pageSize") Integer pageSize);

	/**
	 * 根据条件统计用户
	 * @param status 用户状态(0:禁用;1:启用)
	 * @param name 用户姓名(模糊匹配)
	 * @return 用户总数
	 */
	Integer countByCondition(@Param("status") Short status, @Param("name") String name);

	/**
	 * 获取用户信息
	 *
	 * @param id 用户标识
	 * @return 用户信息
	 */
	DUserAdmin get(@Param("id") Long id);

	/**
	 * 修改用户信息
	 *
	 * @param modify 用户修改信息
	 * @return 修改行数
	 */
	Integer modify(@Param("modify") DUserAdmin modify);

	/**
	 * 修改用户的 启用/禁用 状态
	 *
	 * @param id
	 * @param status
	 */
	void updateUserStatus(@Param("id") Long id, @Param("status") Short status);

	/**
	 * 修改密码
	 * @param id
	 * @param password
	 * @return
	 */
	Integer modifyPassword(@Param("id") Long id,@Param("password") String password);

	/**
	 * 删除用户
	 * @param id
	 */
	Integer deleteUser(Long id);

	/**
	 * 统计指定角色的用户数量
	 * @param roleId
	 * @return
	 */
	Integer countUserByRoleId(@Param("roleId")Long roleId);
}
