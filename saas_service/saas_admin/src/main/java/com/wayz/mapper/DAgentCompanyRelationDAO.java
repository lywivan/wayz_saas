package com.wayz.mapper;

import com.wayz.constants.ViewKeyValue;
import com.wayz.entity.pojo.agent.DAgentCompanyRelation;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 代理商关联DAO接口
 * 
 * @author mike.ma@wayz.ai
 */
@Repository("dAgentCompanyRelationDAO")
public interface DAgentCompanyRelationDAO {

	/**
	 * 创建代理商关联
	 * @param create
	 * @return
	 */
	public Integer create(@Param("create") DAgentCompanyRelation create);

	/**
	 * 删除代理商关联
	 * @param companyId 公司ID
	 * @return
	 */
	public Integer delete(@Param("companyId") Long companyId);

	/**
	 * 获取代理商关联
	 *
	 * @param companyId 公司Id
	 * @return 代理商公司联系
	 */
	public DAgentCompanyRelation get(@Param("companyId") Long companyId);

	/**
	 * 代理商列表id name
	 * @return
	 */
	public List<ViewKeyValue> queryAgentCompanyView();

}
