package com.wayz.mapper;

import com.wayz.entity.pojo.menu.DMenu;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * saas菜单管理
 */
@Repository("dMenuSaasDAO")
public interface DMenuSaasDAO {

	/**
	 * 查询所有菜单
	 *
	 * @return 菜单列表
	 */
	List<DMenu> queryAll();

	/**
	 * 修改是否位叶子节点
	 * @param isLeaf
	 */
	void updateMenuIsLeaf(@Param("isLeaf") Boolean isLeaf, @Param("id") Long id);

	/**
	 * 查询指定版本所选择的菜单
	 * @param roleId
	 * @return
	 */
	List<DMenu> queryMenuByVersionId(@Param("roleId") Long roleId);

	/**
	 * 查询全部菜单编号
	 * @return
	 */
    List<Long> queryMenuIdList();
}
