package com.wayz.mapper;

import com.wayz.entity.pojo.menu.DMenu;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 菜单DAO接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("dMenuAdminDAO")
public interface DMenuAdminDAO {

	/**
	 * 获取菜单
	 * 
	 * @param id 菜单标识
	 * @return 菜单
	 */
	public DMenu get(@Param("id") Long id);

	/**
	 * 查询所有菜单
	 * 
	 * @return 菜单列表
	 */
	public List<DMenu> queryAll();

	/**
	 * 删除
	 * 
	 * @param id 菜单标识
	 */
	public void delete(@Param("id") Long id);

	/**
	 * 修改
	 * 
	 * @param modify
	 */
	public void modify(@Param("modify") DMenu modify);

	// =========================整理完接口================================

	/**
	 * 创建菜单
	 *
	 * @param create
	 */
	void create(@Param("create") DMenu create);

	/**
	 * 修改是否位叶子节点
	 * 
	 * @param isLeaf
	 */
	void updateMenuIsLeaf(@Param("isLeaf") Boolean isLeaf, @Param("id") Long id);

	/**
	 * 根据菜单状态查询菜单
	 *
	 * @param status 菜单状态
	 * @return 菜单列表
	 */
	List<DMenu> queryByStatus(@Param("status") Short status);

	/**
	 * 查询角色对应的全部菜单
	 * 
	 * @param roleId
	 * @param type
	 * @return
	 */
	List<DMenu> queryMenuRole(@Param("roleId") Long roleId, @Param("type") Short type);

	/**
	 * 根据连接查询menu数据
	 * 
	 * @param href
	 * @return
	 */
	DMenu getMenuByHref(@Param("href") String href);

	/**
	 * 查询角色拥有的菜单下的权限
	 * 
	 * @param roleId
	 * @param type
	 * @param parentId
	 * @return
	 */
	List<String> queryHrefByRoleIdAndType(@Param("roleId") Long roleId, @Param("type") Short type,
			@Param("parentId") Long parentId);

	/**
	 * 初始化时使用查询所有菜单Id
	 * 
	 * @return
	 */
	List<Long> queryAllMenuId();

	/**
	 * 根据菜单id批量获取菜单连接
	 * @param menuIdList
	 * @return
	 */
    List<String> queryMenuHref(@Param("menuIdList") List<Long> menuIdList);
}
