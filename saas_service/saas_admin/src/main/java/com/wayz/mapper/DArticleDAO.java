package com.wayz.mapper;

import java.util.List;

import com.wayz.entity.pojo.home.DArticle;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 新闻DAO接口
 * 
 * @author mike.ma@wayz.ai
 *
 */
@Repository("dArticleDAO")
public interface DArticleDAO {

	/**
	 * 获取新闻
	 * 
	 * @param id 标识
	 * @return 新闻
	 */
	public DArticle get(@Param("id") Long id);

	/**
	 * 创建新闻
	 * 
	 * @param id 标识
	 * @param create 新闻创建
	 * @return 创建行数
	 */
	public Integer create(@Param("id") Long id, @Param("create") DArticle create);

	/**
	 * 修改新闻
	 * 
	 * @param modify 新闻修改
	 * @return 修改行数
	 */
	public Integer modify(@Param("modify") DArticle modify);

	/**
	 * 
	 * 存在title
	 * 
	 * @param title
	 * @return
	 */
	public Boolean existTitle(@Param("title") String title);

	/**
	 * 
	 * 查询分页
	 * 
	 * @param categoryId
	 * @param title
	 * @param isPush
	 * @param startIndex
	 * @param pageSize
	 * @return
	 */
	public List<DArticle> query(@Param("categoryId") Short categoryId, @Param("title") String title,
			@Param("isPush") Boolean isPush, @Param("startIndex") Integer startIndex,
			@Param("pageSize") Integer pageSize);

	/**
	 * 
	 * 查询分页总数
	 * 
	 * @param categoryId
	 * @param title
	 * @param isPush
	 * @return
	 */
	public Integer count(@Param("categoryId") Short categoryId, @Param("title") String title,
			@Param("isPush") Boolean isPush);

}
