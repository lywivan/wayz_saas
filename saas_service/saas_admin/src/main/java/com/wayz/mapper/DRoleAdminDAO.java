package com.wayz.mapper;

import com.wayz.constants.ViewMap;
import com.wayz.entity.pojo.role.DRoleAdmin;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 角色DAO接口
 * 
 * @author:mike.ma
 *
 */
@Repository("dRoleAdminDAO")
public interface DRoleAdminDAO {

	/**
	 * 创建角色信息
	 *
	 * @param create 角色创建信息
	 * @return 创建行数
	 */
	Integer create( @Param("create") DRoleAdmin create);

	/**
	 * 修改角色信息
	 *
	 * @param modify 角色修改信息
	 * @return 修改行数
	 */
	Integer modify(@Param("modify") DRoleAdmin modify);

	/**
	 * 删除角色
	 * @param id
	 * @return
	 */
	Integer delete(@Param("id") Long id);

	/**
	 * 查询角色视图
	 * @return
	 */
	List<ViewMap> queryRoleView();

	/**
	 * 存在角色名称
	 *
	 * @param name 角色名称
	 * @return 是否存在
	 */
	Boolean existsName(@Param("name") String name,@Param("id")Long id);

	/**
	 * 批量插入角色和菜单对应数据
	 * @param roleId
	 * @param menuIdList
	 * @return
	 */
    Integer batchCreateRoleMenu(@Param("roleId") Long roleId,@Param("menuIdList") List<Long> menuIdList);

	/**
	 * 统计角色数量
	 * @return 角色总数
	 */
	Integer count(@Param("name") String name);

	/**
	 * 查询角色
	 *
	 * @param startIndex 开始序号
	 * @param pageSize 页面大小
	 * @return 角色列表
	 */
	List<DRoleAdmin> query(@Param("name") String name,
						   @Param("startIndex") Integer startIndex, @Param("pageSize") Integer pageSize);

	/**
	 * 删除角色菜单关联表数据
	 * @param roleId
	 */
	void deleteRoleMenu(@Param("roleId") Long roleId);
}
