package com.wayz.mapper;

import java.util.List;

import com.wayz.entity.pojo.home.DFormData;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 表单数据DAO接口
 * 
 * @author mike.ma@wayz.ai
 *
 */
@Repository("dFormDataDAO")
public interface DFormDataDAO {

	/**
	 * 获取表单数据
	 * 
	 * @param id 标识
	 * @return 表单数据
	 */
	public DFormData get(@Param("id") Long id);

	/**
	 * 创建表单数据
	 * 
	 * @param id 标识
	 * @param create 表单数据创建
	 * @return 创建行数
	 */
	public Integer create(@Param("id") Long id, @Param("create") DFormData create);

	/**
	 * 修改表单数据
	 * 
	 * @param modify 表单数据修改
	 * @return 修改行数
	 */
	public Integer modify(@Param("modify") DFormData modify);

	/**
	 * 查询分页
	 * 
	 * @param type
	 * @param provinceId
	 * @param cityId
	 * @param companyName
	 * @param telephone
	 * @param startIndex
	 * @param pageSize
	 * @return
	 */
	public List<DFormData> query(@Param("type") Short type, @Param("provinceId") Long provinceId,
			@Param("cityId") Long cityId, @Param("companyName") String companyName,
			@Param("telephone") String telephone, @Param("startIndex") Integer startIndex,
			@Param("pageSize") Integer pageSize);

	/**
	 * 查询分页总数
	 * 
	 * @param type
	 * @param provinceId
	 * @param cityId
	 * @param companyName
	 * @param telephone
	 * @return
	 */
	public Integer count(@Param("type") Short type, @Param("provinceId") Long provinceId, @Param("cityId") Long cityId,
			@Param("companyName") String companyName, @Param("telephone") String telephone);
}