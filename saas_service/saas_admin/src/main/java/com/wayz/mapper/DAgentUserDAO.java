package com.wayz.mapper;

import com.wayz.entity.pojo.agent.DAgentUser;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.*;

/**
 * 代理商用户DAO接口
 * 
 * @author mike.ma@wayz.ai
 */
@Repository("dAgentUserDAO")
public interface DAgentUserDAO {

	/**
	 * 获取代理商用户
	 * 
	 * @param id 用户标识
	 * @return 代理商用户
	 */
	public DAgentUser get(@Param("id") Long id);

	/**
	 * 获取代理商用户
	 * @param agentId
	 * @return
	 */
	public DAgentUser getByAgentId(@Param("agentId") Long agentId);

	/**
	 * 创建代理商用户
	 * 
	 * @param id 用户标识
	 * @param create 代理商用户创建
	 * @return 创建行数
	 */
	public Integer create(@Param("id") Long id, @Param("create") DAgentUser create);

	/**
	 * 修改代理商用户
	 * 
	 * @param modify 代理商用户修改
	 * @return 修改行数
	 */
	public Integer modify(@Param("modify") DAgentUser modify);

	/**
	 * 根据手机号  检查是否存在用户
	 * @param phone
	 * @return
	 */
	public Boolean existByPhone(@Param("phone") String phone);

	/**
	 * 更新密码
	 * @param id
	 * @param newPassword
	 */
	public void updatePassword(@Param("id") Long id, @Param("newPassword") String newPassword);
}
