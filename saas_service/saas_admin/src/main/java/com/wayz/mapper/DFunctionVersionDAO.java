package com.wayz.mapper;

import com.wayz.constants.ViewMap;
import com.wayz.entity.pojo.function.DFunctionVersion;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 功能版本DAO接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("dFunctionVersionDAO")
public interface DFunctionVersionDAO {

	//=========================整理后的接口================================
	/**
	 * 修改功能版本
	 *
	 * @param modify 功能版本修改
	 * @return 修改行数
	 */
	Integer modify(@Param("modify") DFunctionVersion modify);
	/**
	 * 创建功能版本
	 *
	 * @param create 功能版本创建
	 * @return 创建行数
	 */
	Integer create(@Param("create") DFunctionVersion create);

	/**
	 * 写入版本和菜单关联表数据
	 * @param versionId
	 * @param menuIdList
	 */
	void insertVersionMenu(@Param("versionId") Long versionId,@Param("menuIdList") List<Long> menuIdList);

	/**
	 * 查询所有功能版本
	 * @return
	 */
	List<DFunctionVersion> queryAllFunctionVersion(@Param("name")String name);

	/**
	 * 查询版本功能视图
	 * @return
	 */
	List<ViewMap> queryVersionViewMap();

	/**
	 * 版本名是否存在
	 * @param name
	 * @return
	 */
	Integer countVersionByName(@Param("name") String name);

	/**
	 * 根据版本名称查询版本信息
	 * @param name
	 * @return
	 */
	DFunctionVersion getVersionByName(@Param("name") String name);

	/**
	 * 根据Id查询版本信息
	 * @param id
	 * @return
	 */
	DFunctionVersion getVersionById(@Param("id") Long id);

	/**
	 * 根据Id删除版本
	 * @param id
	 * @return
	 */
    Integer deleteVersionById(@Param("id")Long id);

    /** 根据ID获取版本管理员角色编号
	 * @param id
	 * @return
	 */
	Long getRoleIdById(@Param("id") Long id);
}
