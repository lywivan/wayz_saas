package com.wayz.mapper;

import java.util.List;

import com.wayz.entity.pojo.role.DRoleSaas;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @className: DRoleSaasDao
 * @description: saas角色 <功能详细描述>
 * @author wade.liu@wayz.ai
 * @date: 2019/6/26
 */
@Repository("dRoleSaasDao")
public interface DRoleSaasDAO {
	/**
	 * 创建角色
	 * 
	 * @param dRoleSaasCreate
	 * @return
	 */
	Integer create(@Param("create") DRoleSaas dRoleSaasCreate);

	/**
	 * 写入角色菜单关联表数据
	 * 
	 * @param roleId
	 * @param menuIdList
	 * @return
	 */
	Integer insertRoleMenu(@Param("roleId") Long roleId, @Param("menuIdList") List<Long> menuIdList);

	/**
	 * 逻辑删除角色
	 * 
	 * @param roleId 角色标识
	 * @return
	 */
	Integer delete(@Param("roleId") Long roleId);


	/**
	 * 删除角色菜单关联
	 * @param roleId 角色ID
	 */
	void deleteMenuRoleId(@Param("roleId") Long roleId);
}
