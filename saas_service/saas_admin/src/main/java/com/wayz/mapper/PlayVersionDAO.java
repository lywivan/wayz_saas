package com.wayz.mapper;

import com.wayz.entity.pojo.playVersion.DPlayVersion;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @className: PlayVersionDAO
 * @description: 播控版本
 * <功能详细描述>
 * @author: 须尽欢_____
 * @date: 2019/7/9
 */
@Repository("playVersionDAO")
public interface PlayVersionDAO {

    Integer create( @Param("create") DPlayVersion PlayVersion);

    DPlayVersion findByVersion(@Param("version") String version);

    Integer count();

    List<DPlayVersion> findAll(@Param("startIndex")Integer startIndex, @Param("pageSize")Integer pageSize);

    /**
     * 根据版本编号删除与公司得对应关系表
     * @param version
     * @return
     */
    Integer deleteByVersion(@Param("version") String version);

    /**
     * 写入播控版本和公司关联表数据
     * @param version
     * @param companyIdList
     * @return
     */
    Integer insertPlayVersionCompany(@Param("version") String version ,
                                     @Param("companyIdList") List<Long> companyIdList);

    /**
     * 查询当前版本有那些公司在使用
     * @param version
     * @return
     */
    List<Long> findCompanyIdListByVersion(@Param("version") String version);
}
