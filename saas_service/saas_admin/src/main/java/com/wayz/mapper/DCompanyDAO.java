/**
 * xxxx
 *
 * @author:ivan.liu
 */
package com.wayz.mapper;

import com.wayz.constants.ViewMap;
import com.wayz.entity.pojo.company.DCompany;
import com.wayz.entity.pojo.company.DCompanyExt;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 公司DAO接口
 *
 * @author yinjy
 *
 */
@Repository("dCompanyDAO")
public interface DCompanyDAO {

    /**
     * 查询公司视图
     *
     * @return
     */
    List<ViewMap> findViewMap();

    /**
     * 获取公司详情
     *
     * @param id
     * @return
     */
    DCompanyExt getCompanyExtById(@Param("id") Long id);

    /**
     * 创建用户播控截屏日志表
     *
     * @param dbNamePlay
     * @param id
     * @return
     */
    Integer createUserPlaySnapshot(@Param("dbNamePlay") String dbNamePlay, @Param("id") Long id);

    /**
     * 根据公司名称获取公司信息
     *
     * @param name 公司名称
     * @return
     */
    DCompany getByName(@Param("name") String name);

    /**
     * 创建公司信息
     *
     * @param create 公司创建信息
     * @return 创建行数
     */
    Integer create(@Param("create") DCompanyExt create);

    /**
     * 统计使用某版本的公司数量
     *
     * @param versionId
     * @return
     */
    Integer countCompanyByVersionId(@Param("versionId") Long versionId);

    /**
     * 修改公司信息
     *
     * @param modify 公司修改信息
     * @return 修改行数
     */
    Integer modify(@Param("modify") DCompanyExt modify);

    /**
     * 更新公司状态(1:启用 0:禁用;)
     *
     * @param companyId
     * @param status
     * @return
     */
    Integer updateCompanyStatus(@Param("companyId") Long companyId, @Param("status") Short status,
                                @Param("validUntilDate") String validUntilDate, @Param("isWatermark") Boolean isWatermark);

    /**
     * 修改公司下点位有效期
     *
     * @param companyId 公司标识
     * @param validUntilDate 有效期至
     */
    void updatePositionUntilDate(@Param("companyId") Long companyId, @Param("validUntilDate") String validUntilDate);

    /**
     * 查询公司账户下有效点位
     *
     * @param companyId 公司标识
     * @return 媒体位标识列表
     */
    public List<Long> queryPositionIdByCompany(@Param("companyId") Long companyId);

    /**
     * 根据条件查询公司信息
     *
     * @param status 公司状态(0:禁用;1:启用)
     * @param sourceType 公司创建来源 1：管理员 2：用户自主
     * @param name 公司名称(模糊匹配)
     * @return 公司数量
     */
    Integer countByCondition(@Param("status") Short status, @Param("sourceType") Short sourceType,
                             @Param("name") String name, @Param("versionId") Long versionId, @Param("phone") String phone,
                             @Param("agentId") Long agentId);

    /**
     * 根据条件查询公司信息
     *
     * @param status 公司状态(0:禁用;1:启用)
     * @param name 公司名称(模糊匹配)
     * @param startIndex 开始序号
     * @param pageSize 页面大小
     * @return 公司信息列表
     */
    List<DCompany> queryExtByCondition(@Param("status") Short status, @Param("sourceType") Short sourceType,
                                       @Param("name") String name, @Param("versionId") Long versionId, @Param("phone") String phone,
                                       @Param("agentId") Long agentId, @Param("startIndex") Integer startIndex, @Param("pageSize") Integer pageSize);

    /**
     * 更新OEM相关配置
     *
     * @param logo
     * @param domain
     * @param websiteTitle
     * @return
     */
    Integer insertCompanyOEM(@Param("id") Long id, @Param("logo") String logo, @Param("domain") String domain,
                             @Param("websiteTitle") String websiteTitle);

    /**
     * 获取公司信息
     *
     * @param id 公司标识
     * @return 公司信息
     */
    DCompany get(@Param("id") Long id);

    /**
     * 逻辑删除公司
     *
     * @param id
     * @return
     */
    Integer deleteCompanyById(@Param("id") Long id);

    /**
     * 查询试用且过期账户
     *
     * @return 账户列表
     */
    public List<DCompany> queryTestingAndInvalid();

    /**
     *
     * 修改公司状态
     *
     * @param id 公司标识
     * @param status 状态
     * @return
     */
    public Integer modifyStatus(@Param("id") Long id, @Param("status") Short status);
}
