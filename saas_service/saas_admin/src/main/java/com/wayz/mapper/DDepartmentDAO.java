package com.wayz.mapper;

import com.wayz.entity.pojo.company.DDepartmentExt;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 部门DAO接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("dDepartmentDAO")
public interface DDepartmentDAO {
	/**
	 * 创建部门
	 *
	 * @param id 标识
	 * @param create 部门创建
	 * @return 创建行数
	 */
	public Integer create(@Param("companyId") Long companyId, @Param("id") Long id,
						  @Param("create") DDepartmentExt dDepartmentExt);
}
