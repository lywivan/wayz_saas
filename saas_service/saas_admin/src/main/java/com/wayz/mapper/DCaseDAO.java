package com.wayz.mapper;

import java.util.List;

import com.wayz.entity.pojo.home.DCase;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 合作案例内容DAO接口
 * 
 * @author mike.ma@wayz.ai
 *
 */
@Repository("dCaseDAO")
public interface DCaseDAO {

	/**
	 * 获取合作案例内容
	 * 
	 * @param id 案例标识
	 * @return 合作案例内容
	 */
	public DCase get(@Param("id") Long id);

	/**
	 * 获取合作案例内容
	 * 
	 * @param title 标题名称
	 * @return 合作案例内容
	 */
	public DCase getByTitle(@Param("title") String title);

	/**
	 * 创建合作案例内容
	 * 
	 * @param id 案例标识
	 * @param create 合作案例内容创建
	 * @return 创建行数
	 */
	public Integer create(@Param("id") Long id, @Param("create") DCase create);

	/**
	 * 修改合作案例内容
	 * 
	 * @param modify 合作案例内容修改
	 * @return 修改行数
	 */
	public Integer modify(@Param("modify") DCase modify);

	/**
	 * 获取合作案例内容数量
	 * 
	 * @param title 标题名称(模糊查询)
	 * @return
	 */
	public Integer count(@Param("title") String title, @Param("isPush") Boolean isPush);

	/**
	 * 查询合作案例内容
	 * 
	 * @param title 标题名称(模糊查询)
	 * @return 合作案例内容列表
	 */
	public List<DCase> query(@Param("title") String title, @Param("isPush") Boolean isPush,
			@Param("startIndex") Integer startIndex, @Param("pageSize") Integer pageSize);

	/**
	 * 删除合作案列内容
	 * 
	 * @param id 合作案例内容标识
	 */
	// public void delete(@Param("id") Long id);

}
