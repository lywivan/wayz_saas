package com.wayz.mapper;

import org.springframework.stereotype.Repository;

/**
 * 公司优惠券DAO接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("dCompanyCouponDAO")
public interface DCompanyCouponDAO {

	/**
	 * 失效公司代金券
	 * 
	 * @return 修改行数
	 */
	public Integer invalidCoupon();

}
