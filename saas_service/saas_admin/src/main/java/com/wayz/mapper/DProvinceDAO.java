package com.wayz.mapper;

import java.util.List;

import com.wayz.entity.pojo.home.DProvince;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 省份DAO接口
 * 
 * @author mike.ma@wayz.ai
 *
 */
@Repository("dProvinceDAO")
public interface DProvinceDAO {

	/**
	 * 获取省份信息
	 * 
	 * @param id 省份标识
	 * @return 省份信息
	 */
	public DProvince get(@Param("id") Long id);

	/**
	 * 查询省份信息
	 *
	 * @return 省份信息列表
	 */
	public List<DProvince> query();

}
