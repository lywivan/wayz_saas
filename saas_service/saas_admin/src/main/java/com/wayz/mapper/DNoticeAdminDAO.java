package com.wayz.mapper;

import com.wayz.entity.pojo.notice.DNotice;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author mike.ma@wayz.ai
 */
@Repository("dNoticeAdminDAO")
public interface DNoticeAdminDAO {

    /**
     * 根据标题统计消息数量
     *
     * @param title
     * @return
     */
    Integer count(@Param("title") String title);

    /**
     * 消息列表
     *
     * @param title
     * @param startIndex
     * @param pageSize
     * @return
     */
    List<DNotice> query(@Param("title") String title, @Param("startIndex") Integer startIndex, @Param("pageSize") Integer pageSize);

    /**
     * 物理删除消息通知
     *
     * @param noticeId
     */
    void deleteNotice(@Param("noticeId") Long noticeId);

    /**
     * 创建消息
     *
     * @param notice
     * @return
     */
    Integer insertNotice(@Param("notice") DNotice notice);

    /**
     * 修改消息
     *
     * @param notice
     * @return
     */
    Integer modifyNotice(@Param("notice") DNotice notice);

    /**
     * 获取消息
     *
     * @param id
     * @return
     */
    DNotice getNotice(@Param("id") Long id);

}
