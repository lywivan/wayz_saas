package com.wayz.mapper;

import com.wayz.entity.pojo.agent.DAgentCompany;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.*;

import java.util.List;

/**
 * 代理商DAO接口
 * 
 * @author mike.ma@wayz.ai
 */
@Repository("dAgentCompanyDAO")
public interface DAgentCompanyDAO {

	/**
	 * 获取代理商
	 * 
	 * @param id 代理商标识
	 * @return 代理商
	 */
	public DAgentCompany get(@Param("id") Long id);

	/**
	 * 创建代理商
	 * 
	 * @param id 代理商标识
	 * @param create 代理商创建
	 * @return 创建行数
	 */
	public Integer create(@Param("id") Long id, @Param("create") DAgentCompany create);

	/**
	 * 修改代理商
	 * 
	 * @param modify 代理商修改
	 * @return 修改行数
	 */
	public Integer modify(@Param("modify") DAgentCompany modify);

	/**
	 * 获取角色ID
	 * @return
	 */
	public Long getAgentRoleId();

	/**
	 * 根据名称  检查代理商是否存在
	 * @param name
	 * @return
	 */
    public Boolean existByName(@Param("name") String name);

	/**
	 * 根据手机号  检查代理商是否存在
	 * @param telephone
	 * @return
	 */
	public Boolean existByPhone(@Param("telephone") String telephone);

	/**
	 * 根据条件统计代理商
	 * @param name     代理商名称
	 * @param phone    手机号
	 * @param status   状态
	 * @return
	 */
	
    public Integer countAgentCompany(@Param("name") String name, @Param("phone") String phone, @Param("status") Short status, 
    		@Param("provinceId") Long provinceId, @Param("cityId") Long cityId, @Param("categoryId") Long categoryId);

	/**
	 * 分页查询代理商列表
	 * @param name     代理商名称
	 * @param phone    手机号
	 * @param status   状态
	 * @param startIndex   开始角标
	 * @param pageSize     每页大小
	 * @return
	 */
	public List<DAgentCompany> queryAgentCompany(@Param("name") String name, @Param("phone") String phone,
			@Param("status") Short status, @Param("provinceId") Long provinceId, @Param("cityId") Long cityId, 
			@Param("categoryId") Long categoryId, @Param("startIndex") Integer startIndex, @Param("pageSize") Integer pageSize);


	/**
	 * 修改代理商状态
	 * @param id    代理商ID
	 * @param status 代理商状态(1:启用 2:禁用;)
	 * @return
	 */
    public Integer updateAgentStatus(@Param("id") Long id, @Param("status") Short status);

	/**
	 * OEM配置
	 * @param id
	 * @param logo
	 * @param domain
	 * @param websiteTitle
	 */
    public void updateAgentOEM(@Param("id") Long id, @Param("logo") String logo, @Param("domain") String domain, @Param("websiteTitle") String websiteTitle);

	/**
	 * 统计代理商下公司媒体位数量
	 * @param id
	 * @return
	 */
	public Integer countPositionLimit(@Param("id") Long id);
}
