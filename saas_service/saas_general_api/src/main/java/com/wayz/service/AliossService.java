package com.wayz.service;

import com.wayz.entity.pojo.UploadFileInfo;
import com.wayz.entity.pojo.UploadFileKeyInfo;
import com.wayz.exception.WayzException;

import java.io.FileNotFoundException;

/**
 * 阿里云开放式存储服务接口
 * 
 * @author yinjy
 *
 */
public interface AliossService {

	/**
	 * 上传文件记录
	 * 
	 * @param myId 客户标识
	 * @param fileName 文件名称
	 * @param fileSize 文件大小
	 * @param url 访问路径
	 * @param fileType 文件类型
	 * @param fileMd5
	 * @param url
	 * @throws WayzException 社区健身异常
	 */
	public void createUploadFileRecord(Long myId, String fileName, long fileSize, Short fileType, String fileMd5,
			String url) throws WayzException;

	/**
	 * 上传文件
	 * 
	 * @param myId 客户标识
	 * @param fileBytes 输入流
	 * @param fileName 文件名称
	 * @param fileSize 文件大小
	 * @param contentType 内容类型
	 * @param fileType 文件类型
	 * @return 文件名称
	 * @throws WayzException 社区健身异常
	 */
	public UploadFileInfo uploadFile(Long myId, String filePath, String fileName, long fileSize, String contentType,
									 Short fileType) throws WayzException, FileNotFoundException;

	/**
	 * 删除文件
	 * 
	 * @param fileName 文件全路径
	 * @return 是否成功
	 */
	public Boolean deleteFile(String fileName) throws WayzException;

	/**
	 * 下载文件
	 * 
	 * @param myId 客户标识
	 * @param url url
	 * @param fileName 文件名称
	 * @return 文件名称
	 * @throws WayzException 社区健身异常
	 */
	public void downloadFile(Long myId, String url, String fileName) throws WayzException;

	/**
	 * 调用STS API 获取阿里云临时安全令牌
	 * 
	 * @return
	 * @throws WayzException
	 */
	public UploadFileKeyInfo getUploadToken(Long companyId, Long fileSize) throws WayzException;

	/**
	 * 设置软链接
	 * 
	 * @param targetObject 目标资源
	 * @param symlink 软链接
	 */
	public void setSymlink(String targetObject, String symlink) throws WayzException;
}
