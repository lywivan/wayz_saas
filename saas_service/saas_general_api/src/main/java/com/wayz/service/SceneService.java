package com.wayz.service;

import com.wayz.constants.PageVo;
import com.wayz.constants.ViewMap;
import com.wayz.entity.vo.SceneVo;
import com.wayz.exception.WayzException;

import java.util.List;


/**
 * 场景服务接口
 *
 * @author mike.ma@wayz.ai
 */
public interface SceneService {

    /**
     * 查询场景视图(一级)
     *
     * @param myId 我的客户标识
     * @return 场景列表
     * @throws WayzException 平台异常
     */
    public List<ViewMap> querySceneView(Long myId, Long companyId) throws WayzException;

    /**
     * 查询场景
     *
     * @param myId 我的客户标识
     * @param name 名称(模糊查询)
     * @return 场景列表
     * @throws WayzException 平台异常
     */
    public PageVo<SceneVo> queryScene(Long myId, Long companyId, String name, Integer startIndex, Integer pageSize)
            throws WayzException;

    /**
     * 获取场景
     *
     * @param myId 我的标识
     * @param id   场景标识
     * @return 场景
     * @throws WayzException 平台异常
     */
    public SceneVo getScene(Long myId, Long id) throws WayzException;

    /**
     * 创建场景
     *
     * @param myId        我的标识
     * @param name        名称
     * @param description 描述
     * @throws WayzException 平台异常
     */
    public void createScene(Long myId, Long companyId, String name, String description) throws WayzException;

    /**
     * 修改场景
     *
     * @param myId        我的客户标识
     * @param id          场景标识
     * @param name        名称
     * @param description 描述
     * @throws WayzException 平台异常
     */
    public void modifyScene(Long myId, Long companyId, Long id, String name, String description) throws WayzException;

    /**
     * 删除场景
     *
     * @param myId 我的标识
     * @param id   场景标识
     * @throws WayzException 平台异常
     */
    public void deleteScene(Long myId, Long id) throws WayzException;

}
