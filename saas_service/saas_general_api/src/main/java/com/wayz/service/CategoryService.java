package com.wayz.service;

import com.wayz.entity.vo.CategoryVo;
import com.wayz.exception.WayzException;

import java.util.List;


/**
 * 行业服务接口
 *
 * @author mike.ma@wayz.ai
 */
public interface CategoryService {

    /**
     * 查询行业类别
     *
     * @param myId 我的标识
     * @param id 行业类别标识
     * @return 行业类别列表
     * @throws WayzException 平台异常
     */
    public List<CategoryVo> queryCategory(Long myId, Long companyId, Long id) throws WayzException;

    /**
     * 创建行业类别
     *
     * @param myId 我的标识
     * @param name 行业类别名称
     * @param description 行业类别描述
     * @throws WayzException 平台异常
     */
    public Long createCategory(Long myId, Long companyId, String name, String description) throws WayzException;

    /**
     * 修改行业类别
     *
     * @param myId 我的标识
     * @param id 行业类别标识
     * @param name 行业类别名称
     * @param description 行业类别描述
     * @throws WayzException 平台异常
     */
    public void modifyCategory(Long myId, Long companyId, Long id, String name, String description)
            throws WayzException;

    /**
     * 删除行业类别
     *
     * @param myId 我的标识
     * @param id 行业类别标识
     * @throws WayzException 平台异常
     */
    public void deleteCategory(Long myId, Long id) throws WayzException;
    
}
