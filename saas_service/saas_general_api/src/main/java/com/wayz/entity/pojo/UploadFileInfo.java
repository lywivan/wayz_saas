package com.wayz.entity.pojo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 上传文件信息
 * 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UploadFileInfo implements Serializable {
	
	/** 文件上传状态(1:上传中; 2:上传成功; 3:上传失败) */
	private Short status = null;
	/** 上传文件日志中的标志 */
	private Long fileId = null;
	/** 访问地址 */
	private String url = null;
	/** md5加密串 */
	private String fileMd5 = null;
}
