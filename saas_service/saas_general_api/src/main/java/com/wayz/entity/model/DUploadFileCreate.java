package com.wayz.entity.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.*;
import java.sql.*;

/**
 * 文件上传记录创建类
 * 
 *  
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DUploadFileCreate implements Serializable {

	/** 客户标识 */
	private Long customerId = null;
	/** 文件状态(1:上传中;2:上传成功;3:上传失败) */
	private Short status = null;
	/** 文件类型(0:其他文件;1:用户头像;2:媒体位; 3:广告素材) */
	private Short type = null;
	/** 文件名称 */
	private String name = null;
	/** 文件url */
	private String url = null;
	/** 文件大小(字节) */
	private Long size = null;
	/** 文件MD5校验码 */
	private String filemd5 = null;
	/** 失败原因 */
	private String cause = null;
	/** 创建时间 */
	private Timestamp createdTime = null;

}
