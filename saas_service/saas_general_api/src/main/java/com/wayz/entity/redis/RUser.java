package com.wayz.entity.redis;

import java.text.MessageFormat;

/**
 * 用户类
 * 
 * @author yinjy
 *
 */
public class RUser {

	/** 属性相关 */
	/** 角色标识 */
	private Long roleId = null;
	/** 用户类型 */
	private Short type = null;
	/** 公司标识 */
	private Long companyId = null;
	/** 用户状态(0:禁用;1:启用) */
	private Short status = null;
	/** 用户名称 */
	private String username = null;
	/** 用户姓名 */
	private String name = null;
	/** 用户电话 */
	private String phone = null;
	/** 用户职务 */
	private String title = null;
	/** 用户邮箱 */
	private String email = null;
	/** 用户头像 */
	private String avatar = null;
	/** 部门标识 */
	private Long departId = null;
	/** 部门路径 */
	private String departPath = null;

	/** 常量相关 */
	/** 角色标识 */
	public static final String ROLEID = "roleId";
	/** 用户类型 */
	public static final String TYPE = "rype";
	/** 公司标识 */
	public static final String COMPANYID = "companyId";
	/** 用户状态(0:禁用;1:启用) */
	public static final String STATUS = "status";
	/** 用户名称 */
	public static final String USERNAME = "username";
	/** 用户姓名 */
	public static final String NAME = "name";
	/** 用户电话 */
	public static final String PHONE = "phone";
	/** 用户职务 */
	public static final String TITLE = "title";
	/** 用户邮箱 */
	public static final String EMAIL = "email";
	/** 用户头像 */
	public static final String AVATAR = "avatar";
	/** 部门标识 */
	public static final String DEPARTID = "departId";
	/** 部门路径 */
	public static final String DEPARTPATH = "departPath";

	/** 键值格式 */
	private static final MessageFormat KEY_FORMAT = new MessageFormat("yunge:Object:User:{0}");

	/**
	 * 获取角色标识
	 * 
	 * @return 角色标识
	 */
	public Long getRoleId() {
		return roleId;
	}

	/**
	 * 设置角色标识
	 * 
	 * @param roleId 角色标识
	 */
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	/**
	 * 获取用户类型
	 *
	 * @return 用户类型
	 */
	public Short getType() {
		return type;
	}

	/**
	 * 设置用户类型
	 *
	 * @param type 用户类型
	 */
	public void setType(Short type) {
		this.type = type;
	}

	/**
	 * 获取公司标识
	 *
	 * @return 公司标识
	 */
	public Long getCompanyId() {
		return companyId;
	}

	/**
	 * 设置公司标识
	 *
	 * @param companyId 公司标识
	 */
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	/**
	 * 获取用户状态(0:禁用;1:启用)
	 * 
	 * @return 用户状态(0:禁用;1:启用)
	 */
	public Short getStatus() {
		return status;
	}

	/**
	 * 设置用户状态(0:禁用;1:启用)
	 * 
	 * @param status 用户状态(0:禁用;1:启用)
	 */
	public void setStatus(Short status) {
		this.status = status;
	}

	/**
	 * 获取用户名称
	 * 
	 * @return 用户名称
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * 设置用户名称
	 * 
	 * @param username 用户名称
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * 获取用户姓名
	 * 
	 * @return 用户姓名
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置用户姓名
	 * 
	 * @param name 用户姓名
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 获取用户电话
	 * 
	 * @return 用户电话
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * 设置用户电话
	 * 
	 * @param phone 用户电话
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * 获取用户职务
	 * 
	 * @return 用户职务
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * 设置用户职务
	 * 
	 * @param title 用户职务
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 获取用户邮箱
	 * 
	 * @return 用户邮箱
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * 设置用户邮箱
	 * 
	 * @param email 用户邮箱
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * 获取用户头像
	 * 
	 * @return 用户头像
	 */
	public String getAvatar() {
		return avatar;
	}

	/**
	 * 设置用户头像
	 * 
	 * @param avatar 用户头像
	 */
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public Long getDepartId() {
		return departId;
	}

	public void setDepartId(Long departId) {
		this.departId = departId;
	}

	public String getDepartPath() {
		return departPath;
	}

	public void setDepartPath(String departPath) {
		this.departPath = departPath;
	}

	/**
	 * 获取key值方法
	 * 
	 * @param id
	 * @return
	 */
	public static String getKey(Long id) {
		return KEY_FORMAT.format(new Long[] { id });
	}

}
