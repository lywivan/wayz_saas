package com.wayz.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 场景类
 * 
 *  @author mike.ma@wayz.ai
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SceneVo implements Serializable {

	/** 场景标识 */
	private Long id = null;
	/** 场景名称 */
	private String name = null;
	/** 描述 */
	private String description = null;
	/**公司编号*/
	private Long companyId;
	/** 创建时间 */
	private String createdTime = null;
	/** 修改时间 */
	private String modifiedTime = null;

}
