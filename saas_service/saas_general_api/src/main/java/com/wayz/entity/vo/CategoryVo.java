package com.wayz.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 行业类别类
 *
 * @author mike.ma@wayz.ai
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CategoryVo implements Serializable {

	/** 行业类别标识 */
	private Long id = null;
	/**公司id*/
	private Long companyId = null;
	/** 行业类别名称 */
	private String name = null;
	/** 行业类别名称 */
	private String description = null;

}
