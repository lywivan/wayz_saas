/**
 * 
 */
package com.wayz.entity.pojo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 上传文件信息
 * 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UploadFileKeyInfo implements Serializable {

	/** 是否上传至OSS,false时上传至服务器 */
	private Boolean isUploadOss = null;
	/** 失效时间?? */
	private String expiration = null;
	/** 安全令牌 */
	private String securityToken = null;
	/** 秘钥 */
	private String accessKeySecret = null;
	/** Key标识 */
	private String accessKeyId = null;
	/** 请求标识 */
	private String requestId = null;
	/** 服务端点 */
	private String endPoint = null;
	/** 容器名称 */
	private String bucketName = null;
	/** 公司素材空间大小(Byte) */
	private Long materialLimit = null;
	/** 公司单个素材大小(Byte) */
	private Long singleFileLimit = null;

}
