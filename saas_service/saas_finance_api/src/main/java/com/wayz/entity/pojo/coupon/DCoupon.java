package com.wayz.entity.pojo.coupon;

import lombok.Data;

import java.io.*;
import java.sql.*;

/**
 * 优惠券类
 * 
 * @author yinjy
 *
 */
@Data
public class DCoupon implements Serializable {

	/** 标识 */
	private Long id = null;
	/** 名称 */
	private String name = null;
	/** 标题描述 */
	private String title = null;
	/** 产品类型(0:充值;1:点位新购; 2:点位续期; 3:空间; 4:直播服务) */
	private Short productType = null;
	/** 产品类型(0:充值; 1:点位新购; 2:点位续期; 3:空间; 4:直播服务) */
	private String productTypeName = null;
	/** 最低消费限制 */
	private Double fullAmount = null;
	/** 可减免金额 */
	private Double reduceAmount = null;
	/** 发放数量(0表示无限制) */
	private Integer provideNumber = null;
	/** 剩余数量(0表示无限制) */
	private Integer balanceNumber = null;
	/** 有效天数 */
	private Integer validDays = null;
	/** 是否可用 */
	private Boolean isEnable = null;
	/** 是否公开 */
	private Boolean isPublic = null;
	/** 创建人标识 */
	private Long creatorId = null;
	/** 创建人 */
	private String creatorName = null;
	/** 创建时间 */
	private Timestamp createdTime = null;
	/** 修改时间 */
	private Timestamp modifiedTime = null;
}
