package com.wayz.entity.dto.coupon;

import com.wayz.constants.PageDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @className: CouponPageDto
 * @description: 代金卷类 <功能详细描述>
 * @author wade.liu@wayz.ai
 * @date: 2019/6/26
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CouponPageDto extends PageDto implements Serializable {

	/** 代金卷类型 */
	private Short productType = null;
	/** 代金卷状态 */
	private Boolean isEnable = null;

}
