package com.wayz.entity.pojo.coupon;

import java.io.*;
import java.util.*;

/**
 * 代金券分页类
 * 
 * @author yinjy
 *
 */
public class CouponPage implements Serializable {

	/** 代金券总数 */
	private Integer totalCount = null;
	/** 代金券列表 */
	private List<DCoupon> couponList = null;

	/**
	 * 获取代金券总数
	 * 
	 * @return 代金券总数
	 */
	public Integer getTotalCount() {
		return totalCount;
	}

	/**
	 * 设置代金券总数
	 * 
	 * @param totalCount 代金券总数
	 */
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	/**
	 * 获取代金券列表
	 * 
	 * @return 代金券列表
	 */
	public List<DCoupon> getCouponList() {
		return couponList;
	}

	/**
	 * 设置代金券列表
	 * 
	 * @param couponList 代金券列表
	 */
	public void setCouponList(List<DCoupon> couponList) {
		this.couponList = couponList;
	}

}
