package com.wayz.entity.dto.device;

import java.io.Serializable;

/**
 * 设备租赁价格类
 * 
 * @author wade.liu@wayz.ai
 *
 */
public class DTenancyPriceDto implements Serializable {

	/** 标识 */
	private Long id = null;
	/** 天数 */
	private Integer days = null;
	/** 显示名称 */
	private String name = null;
	/** 原价 */
	private Double salePrice = null;
	/** 优惠价 */
	private Double discountPrice = null;

	/**
	 * 获取标识
	 * 
	 * @return 标识
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 设置标识
	 * 
	 * @param id 标识
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 获取天数
	 * 
	 * @return 天数
	 */
	public Integer getDays() {
		return days;
	}

	/**
	 * 设置天数
	 * 
	 * @param days 天数
	 */
	public void setDays(Integer days) {
		this.days = days;
	}

	/**
	 * 获取显示名称
	 * 
	 * @return 显示名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置显示名称
	 * 
	 * @param name 显示名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 获取原价
	 * 
	 * @return 原价
	 */
	public Double getSalePrice() {
		return salePrice;
	}

	/**
	 * 设置原价
	 * 
	 * @param salePrice 原价
	 */
	public void setSalePrice(Double salePrice) {
		this.salePrice = salePrice;
	}

	/**
	 * 获取优惠价
	 * 
	 * @return 优惠价
	 */
	public Double getDiscountPrice() {
		return discountPrice;
	}

	/**
	 * 设置优惠价
	 * 
	 * @param discountPrice 优惠价
	 */
	public void setDiscountPrice(Double discountPrice) {
		this.discountPrice = discountPrice;
	}
}
