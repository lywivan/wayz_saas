package com.wayz.entity.pojo.coupon;

import java.io.*;
import java.sql.*;

/**
 * 公司优惠券类
 * 
 * @author wade.liu@wayz.ai
 *
 */
public class DCompanyCoupon implements Serializable {

	/** 标识 */
	private Long id = null;
	/** 公司标识 */
	private Long companyId = null;
	/** 代金券标识 */
	private Long couponId = null;
	/** 名称 */
	private String name = null;
	/** 标题描述 */
	private String title = null;
	/** 产品类型(0:充值;1:点位新购; 2:点位续期; 3:空间; 4:直播服务) */
	private Short productType = null;
	/** 最低消费限制 */
	private Double fullAmount = null;
	/** 可减免金额 */
	private Double reduceAmount = null;
	/** 状态(0:已失效; 1:可用; 2:已使用; ) */
	private Short status = null;
	/** 开始日期 */
	private Date beginDate = null;
	/** 截止日期 */
	private Date expireDate = null;
	/** 提供人标识 */
	private Long providerId = null;
	/** 创建时间 */
	private Timestamp createdTime = null;
	/** 修改时间 */
	private Timestamp modifiedTime = null;

	/**
	 * 获取标识
	 * 
	 * @return 标识
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 设置标识
	 * 
	 * @param id 标识
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 获取公司标识
	 * 
	 * @return 公司标识
	 */
	public Long getCompanyId() {
		return companyId;
	}

	/**
	 * 设置公司标识
	 * 
	 * @param companyId 公司标识
	 */
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	/**
	 * 获取代金券标识
	 * 
	 * @return 代金券标识
	 */
	public Long getCouponId() {
		return couponId;
	}

	/**
	 * 设置代金券标识
	 * 
	 * @param couponId 代金券标识
	 */
	public void setCouponId(Long couponId) {
		this.couponId = couponId;
	}

	/**
	 * 获取名称
	 * 
	 * @return 名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置名称
	 * 
	 * @param name 名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 获取标题描述
	 * 
	 * @return 标题描述
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * 设置标题描述
	 * 
	 * @param title 标题描述
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 获取产品类型(0:充值;1:点位新购; 2:点位续期; 3:空间; 4:直播服务)
	 * 
	 * @return 产品类型(0:充值;1:点位新购; 2:点位续期; 3:空间; 4:直播服务)
	 */
	public Short getProductType() {
		return productType;
	}

	/**
	 * 设置产品类型(0:充值;1:点位新购; 2:点位续期; 3:空间; 4:直播服务)
	 * 
	 * @param productType 产品类型(0:充值;1:点位新购; 2:点位续期; 3:空间; 4:直播服务)
	 */
	public void setProductType(Short productType) {
		this.productType = productType;
	}

	/**
	 * 获取最低消费限制
	 * 
	 * @return 最低消费限制
	 */
	public Double getFullAmount() {
		return fullAmount;
	}

	/**
	 * 设置最低消费限制
	 * 
	 * @param fullAmount 最低消费限制
	 */
	public void setFullAmount(Double fullAmount) {
		this.fullAmount = fullAmount;
	}

	/**
	 * 获取可减免金额
	 * 
	 * @return 可减免金额
	 */
	public Double getReduceAmount() {
		return reduceAmount;
	}

	/**
	 * 设置可减免金额
	 * 
	 * @param reduceAmount 可减免金额
	 */
	public void setReduceAmount(Double reduceAmount) {
		this.reduceAmount = reduceAmount;
	}

	/**
	 * 获取状态(0:已失效; 1:可用; 2:已使用; )
	 * 
	 * @return 状态(0:已失效; 1:可用; 2:已使用; )
	 */
	public Short getStatus() {
		return status;
	}

	/**
	 * 设置状态(0:已失效; 1:可用; 2:已使用; )
	 * 
	 * @param status 状态(0:已失效; 1:可用; 2:已使用; )
	 */
	public void setStatus(Short status) {
		this.status = status;
	}

	/**
	 * 获取开始日期
	 * 
	 * @return 开始日期
	 */
	public Date getBeginDate() {
		return beginDate;
	}

	/**
	 * 设置开始日期
	 * 
	 * @param beginDate 开始日期
	 */
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	/**
	 * 获取截止日期
	 * 
	 * @return 截止日期
	 */
	public Date getExpireDate() {
		return expireDate;
	}

	/**
	 * 设置截止日期
	 * 
	 * @param expireDate 截止日期
	 */
	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	/**
	 * 获取提供人标识
	 * 
	 * @return 提供人标识
	 */
	public Long getProviderId() {
		return providerId;
	}

	/**
	 * 设置提供人标识
	 * 
	 * @param providerId 提供人标识
	 */
	public void setProviderId(Long providerId) {
		this.providerId = providerId;
	}

	/**
	 * 获取创建时间
	 * 
	 * @return 创建时间
	 */
	public Timestamp getCreatedTime() {
		return createdTime;
	}

	/**
	 * 设置创建时间
	 * 
	 * @param createdTime 创建时间
	 */
	public void setCreatedTime(Timestamp createdTime) {
		this.createdTime = createdTime;
	}

	/**
	 * 获取修改时间
	 * 
	 * @return 修改时间
	 */
	public Timestamp getModifiedTime() {
		return modifiedTime;
	}

	/**
	 * 设置修改时间
	 * 
	 * @param modifiedTime 修改时间
	 */
	public void setModifiedTime(Timestamp modifiedTime) {
		this.modifiedTime = modifiedTime;
	}

}
