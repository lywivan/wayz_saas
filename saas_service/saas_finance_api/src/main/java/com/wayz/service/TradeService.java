package com.wayz.service;

import com.wayz.constants.ViewKeyValue;
import com.wayz.entity.pojo.device.DTenancyPrice;
import com.wayz.entity.pojo.space.DSpacePrice;
import com.wayz.exception.WayzException;

import java.util.List;

/**
 * 交易服务接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
public interface TradeService {
    /**
     * 查询交易类型
     *
     * @param myId 我的标识
     * @param type 交易分类(0:所有;1:充值;2:消费)
     * @return 交易类型列表
     * @throws WayzException 平台异常
     */
    public List<ViewKeyValue> queryTradeType(Long myId, Short type) throws WayzException;

    /**
     * 创建媒体续期价格
     *
     * @param myId 我的标识
     * @param days 天数
     * @param name 显示名称
     * @param salePrice 原价
     * @param discountPrice 优惠价
     * @return 价格标识
     * @throws YungeException 平台异常
     */
    public Long createDevicePrice(Long myId, Integer days, String name, Double salePrice, Double discountPrice)
            throws WayzException;

    /**
     * 修改媒体续期价格
     *
     * @param myId 我的标识
     * @param id 标识
     * @param days 天数
     * @param name 显示名称
     * @param salePrice 原价
     * @param discountPrice 优惠价
     */
    public void modifyDevicePrice(Long myId, Long id, Integer days, String name, Double salePrice, Double discountPrice)
            throws WayzException;

    /**
     * 删除媒体续期价格
     *
     * @param myId 我的标识
     * @param id 标识
     * @throws YungeException 平台异常
     */
    public void deleteDevicePrice(Long myId, Long id) throws WayzException;

    /**
     * 查询媒体续期价格
     *
     * @param myId 我的标识
     * @return 价格列表
     * @throws YungeException 平台异常
     */
    public List<DTenancyPrice> queryDevicePrice(Long myId) throws WayzException;

    /**
     * 创建空间价格
     *
     * @param myId 我的客户标识
     * @param space 空间(Gb)
     * @param name 显示名称
     * @param salePrice 原价
     * @param discountPrice 优惠价
     * @return 空间
     * @throws YungeException 平台异常
     */
    public Integer createSpacePrice(Long myId, Integer space, String name, Double salePrice, Double discountPrice)
            throws WayzException;

    /**
     * 修改空间价格
     *
     * @param myId 我的客户标识
     * @param space 空间
     * @param name 显示名称
     * @param salePrice 原价
     * @param discountPrice 优惠价
     * @throws YungeException 平台异常
     */
    public void modifySpacePrice(Long myId, Integer space, String name, Double salePrice, Double discountPrice)
            throws WayzException;

    /**
     * 删除空间价格
     *
     * @param myId 我的客户标识
     * @param space 空间
     * @throws YungeException 平台异常
     */
    public void deleteSpacePrice(Long myId, Integer space) throws WayzException;

    /**
     * 查询空间价格
     *
     * @param myId 我的标识
     * @return 价格列表
     * @throws YungeException 平台异常
     */
    public List<DSpacePrice> querySpacePrice(Long myId) throws WayzException;
}
