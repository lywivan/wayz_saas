package com.wayz.service;

import com.wayz.entity.pojo.coupon.CouponPage;
import com.wayz.exception.WayzException;

/**
 * 代金券服务接口
 * 
 * @author yinjy
 *
 */
public interface CouponService {

	/**
	 * 查询代金券
	 *
	 * @param myId 我的标识
	 * @param productType 产品类型(0:充值; 1:点位新购; 2:点位续期; 3:空间; 4:直播服务)
	 * @param status 状态(1:可用; 2:已失效)
	 * @return 代金券分页
	 * @throws WayzException 平台异常
	 */
	public CouponPage queryCoupon(Long myId, Short productType, Boolean isEnable, Integer startIndex, Integer pageSize)
			throws WayzException;

	/**
	 * 创建代金券
	 *
	 * @param myId 我的标识
	 * @param name 名称
	 * @param title 标题描述
	 * @param productType 产品类型(0:充值; 1:点位新购; 2:点位续期; 3:空间; 4:直播服务)
	 * @param fullAmount 最低消费限制
	 * @param reduceAmount 可减免金额
	 * @param balanceNumber 剩余数量
	 * @param beginDate 开始日期
	 * @param expireDate 截止日期
	 * @param isPublic 是否公开
	 * @return 代金券标识
	 * @throws YungeException 平台异常
	 */
	public Long createCoupon(Long myId, String name, String title, Short productType, Double fullAmount,
							 Double reduceAmount, Integer provideNumber, Integer validDays, Boolean isPublic)
			throws WayzException;

	/**
	 * 修改代金券
	 *
	 * @param myId 我的标识
	 * @param id 标识
	 * @param name 名称
	 * @param title 标题描述
	 * @param productType 产品类型(0:充值; 1:点位新购; 2:点位续期; 3:空间; 4:直播服务)
	 * @param fullAmount 最低消费限制
	 * @param reduceAmount 可减免金额
	 * @param provideNumber 发放数量
	 * @param beginDate 开始日期
	 * @param expireDate 截止日期
	 * @param isPublic 是否公开
	 * @throws YungeException 平台异常
	 */
	public void modifyCoupon(Long myId, Long id, String name, String title, Short productType, Double fullAmount,
							 Double reduceAmount, Integer provideNumber, Integer validDays, Boolean isPublic) throws WayzException;

	/**
	 * 失效代金券
	 *
	 * @param myId 我的标识
	 * @param id 标识
	 * @throws YungeException 平台异常
	 */
	public void enableCoupon(Long myId, Long id, Boolean isEnable) throws WayzException;

	/**
	 * 分配代金券
	 *
	 * @param myId 我的标识
	 * @param couponId 代金券标识
	 * @param companyId 公司标识
	 * @param days 有效天数
	 * @throws YungeException 平台异常
	 */
	public void distributeCoupon(Long myId, Long couponId, Long companyId, Integer days) throws WayzException;
}
