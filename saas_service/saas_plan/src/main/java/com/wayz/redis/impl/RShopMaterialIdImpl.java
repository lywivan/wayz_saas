package com.wayz.redis.impl;

import javax.annotation.*;

import com.wayz.redis.RShopMaterialId;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.*;

/**
 * 商城素材标识实现类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("rShopMaterialId")
public class RShopMaterialIdImpl implements RShopMaterialId {

	/** Redis模板 */
	@Resource(name = "redisTemplate")
	private RedisTemplate<String, String> redisTemplate = null;

	/** 键值格式 */
	private static final String KEY = "platform:Id:ShopMaterial";

	/**
	 * 递增商城素材标识
	 * 
	 * @return 商城素材标识
	 */
	@Override
	public Long increment() {
		return redisTemplate.opsForValue().increment(KEY, 1L);
	}

}
