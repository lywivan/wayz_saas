package com.wayz.redis;

/**
 * 应用标识接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
public interface RApplicationId {

	/**
	 * 递增应用标识
	 * 
	 * @return 应用标识
	 */
	public Long increment();

}
