package com.wayz.redis.impl;

import com.wayz.redis.RApplicationId;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * 应用标识实现类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("rApplicationId")
public class RApplicationIdImpl implements RApplicationId {

	/** Redis模板 */
	@Resource(name = "redisTemplate")
	private RedisTemplate<String, String> redisTemplate = null;

	/** 键值格式 */
	private static final String KEY = "platform:Id:Application";

	/**
	 * 递增应用标识
	 * 
	 * @return 应用标识
	 */
	@Override
	public Long increment() {
		return redisTemplate.opsForValue().increment(KEY, 1L);
	}

}
