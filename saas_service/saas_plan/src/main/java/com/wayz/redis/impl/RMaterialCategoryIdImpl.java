package com.wayz.redis.impl;

import javax.annotation.*;

import com.wayz.redis.RMaterialCategoryId;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.*;

/**
 * 素材分类标识实现类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("rMaterialCategoryId")
public class RMaterialCategoryIdImpl implements RMaterialCategoryId {

	/** Redis模板 */
	@Resource(name = "redisTemplate")
	private RedisTemplate<String, String> redisTemplate = null;

	/** 键值格式 */
	private static final String KEY = "platform:Id:MaterialCategory";

	/**
	 * 递增素材分类标识
	 * 
	 * @return 素材分类标识
	 */
	@Override
	public Long increment() {
		return redisTemplate.opsForValue().increment(KEY, 1L);
	}

}
