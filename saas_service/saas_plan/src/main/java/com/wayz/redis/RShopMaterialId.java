package com.wayz.redis;

/**
 * 商城素材标识接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
public interface RShopMaterialId {

	/**
	 * 递增商城素材标识
	 * 
	 * @return 商城素材标识
	 */
	public Long increment();

}
