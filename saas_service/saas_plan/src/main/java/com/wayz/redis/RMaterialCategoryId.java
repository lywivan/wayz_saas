package com.wayz.redis;

/**
 * 素材分类标识接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
public interface RMaterialCategoryId {

	/**
	 * 递增素材分类标识
	 * 
	 * @return 素材分类标识
	 */
	public Long increment();

}
