package com.wayz.service;

import com.wayz.constants.PageVo;
import com.wayz.constants.ViewKeyValue;
import com.wayz.entity.pojo.category.DContentCategory;
import com.wayz.entity.vo.category.ContentCategoryVo;
import com.wayz.exception.WayzException;
import com.wayz.mapper.DContentCategoryDAO;
import com.wayz.redis.RMaterialCategoryId;
import com.wayz.util.TimeHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 内容分类服务实现类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Service("contentCategoryService")
@org.apache.dubbo.config.annotation.Service(version = "1.0.0", protocol = "dubbo")
@RefreshScope
public class ContentCategoryServiceImpl implements ContentCategoryService {
    /**
     * 内容分类DAO
     */
    @Resource(name = "dContentCategoryDAO")
    private DContentCategoryDAO dContentCategoryDAO;

    /**
     * 内容分类Redis
     */
    @Resource(name = "rMaterialCategoryId")
    private RMaterialCategoryId rMaterialCategoryId;


    /**
     * 查询内容分类视图
     *
     * @return 分类列表
     * @throws WayzException 平台异常
     */
    @Override
    public List<ViewKeyValue> queryContentCategoryView(String name) throws WayzException {
        // 初始化
        List<ViewKeyValue> categorylList = new ArrayList<>();
        Integer countCategory = dContentCategoryDAO.count(name);
        if (countCategory <= 0){
            return categorylList;
        }
        // 返回数据
        return dContentCategoryDAO.queryListView(name);
    }

    /**
     * 查询内容分类
     *
     * @param myId 我的客户标识
     * @return 分类列表
     * @throws WayzException 平台异常
     */
    @Override
    public PageVo<ContentCategoryVo> queryContentCategory(Long myId, String name, Integer startIndex, Integer pageSize) {
        // 初始化
        PageVo<ContentCategoryVo> categoryPage = new PageVo<>();
        List<ContentCategoryVo> resultList = new ArrayList<ContentCategoryVo>();

        Integer countCategory = dContentCategoryDAO.count(name);
        if (countCategory <= 0){
            return categoryPage;
        }
        List<DContentCategory> dCategoryList = dContentCategoryDAO.queryListCategory(name,startIndex, pageSize);
        ContentCategoryVo contentCategory;
        if (!dCategoryList.isEmpty()){
            for (DContentCategory dContentCategory:dCategoryList){
                contentCategory = new ContentCategoryVo();
                BeanUtils.copyProperties(dContentCategory, contentCategory);
                contentCategory.setCreatedTime(TimeHelper.getTimestamp(dContentCategory.getCreatedTime()));
                resultList.add(contentCategory);
            }
        }
        // 返回数据
        categoryPage.setTotalCount(countCategory);
        categoryPage.setList(resultList);

        return categoryPage;
    }

    /**
     * 创建内容分类
     *
     * @param myId 我的客户标识
     * @param name 分类名称
     * @param description 描述
     * @return 分类标识
     * @throws WayzException 平台异常
     */
    @Override
    public Long createContentCategory(Long myId, String name, String description) throws WayzException {
        //初始化

        Long id = rMaterialCategoryId.increment();
        DContentCategory dCreate = new DContentCategory();
        dCreate.setName(name);
        dCreate.setDescription(description);
        dContentCategoryDAO.create(id,dCreate);
        // 返回数据
        return id;
    }

    /**
     * 删除内容分类
     *
     * @param myId 我的客户标识
     * @param id 内容分类标识
     * @throws WayzException 平台异常
     */
    @Override
    public void deleteCategory(Long myId, Long id) throws WayzException {
        DContentCategory dContentCategory = dContentCategoryDAO.get(id);
        if (dContentCategory == null){
            throw new WayzException("分类不存在");
        }
        dContentCategoryDAO.deleteCategory(id);
    }

    /**
     * 修改内容分类
     *
     * @param myId 我的客户标识
     * @param id 分类标识
     * @param name 分类名称
     * @param description 描述
     * @throws WayzException 平台异常
     */
    @Override
    public void modifyCategory(Long myId, Long id, String name, String description) throws WayzException {
        DContentCategory dModify = new DContentCategory();
        dModify.setId(id);
        dModify.setName(name);
        dModify.setDescription(description);
        dContentCategoryDAO.modify(dModify);
    }

    /**
     * 获取内容分类
     *
     * @param myId 我的客户标识
     * @param id 分类标识
     * @return 内容分类
     * @throws WayzException 平台异常
     */
    @Override
    public DContentCategory getContentCategory(Long myId, Long id){
        // 初始化
        DContentCategory contentCategory = dContentCategoryDAO.get(id);
        // 返回数据
        return contentCategory;
    }
}
