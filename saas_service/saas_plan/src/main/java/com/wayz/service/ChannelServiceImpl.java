package com.wayz.service;

import com.wayz.constants.ViewKeyValue;
import com.wayz.exception.WayzException;
import com.wayz.mapper.DContentChannelDAO;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 内容渠道服务实现类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Service("channelService")
@org.apache.dubbo.config.annotation.Service(version = "1.0.0", protocol = "dubbo")
@RefreshScope
public class ChannelServiceImpl implements ChannelService {

    /**
     * 渠道DAO
     */
    @Resource(name = "dContentChannelDAO")
    private DContentChannelDAO dContentChannelDAO;

    /**
     * 查询内容渠道视图
     *
     * @return 渠道列表
     */
    @Override
    public List<ViewKeyValue> queryChannelViewList(String name) throws WayzException {
        // 初始化
        List<ViewKeyValue> channelList = new ArrayList<>();
        Integer countChannel = dContentChannelDAO.countChannel(name);
        if (countChannel <= 0){
            return channelList;
        }
        // 返回数据
        return dContentChannelDAO.queryListView(name);
    }

}
