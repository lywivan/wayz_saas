package com.wayz.service;

import com.wayz.constants.CAuditStatus;
import com.wayz.constants.CPlanMaterialType;
import com.wayz.constants.PageVo;
import com.wayz.constants.ViewKeyValue;
import com.wayz.entity.pojo.content.DShopContent;
import com.wayz.entity.vo.content.ShopContentVo;
import com.wayz.exception.WayzException;
import com.wayz.mapper.DShopContentDAO;
import com.wayz.redis.RShopMaterialId;
import com.wayz.util.TimeHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 商城内容服务实现类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Service("contentService")
@org.apache.dubbo.config.annotation.Service(version = "1.0.0", protocol = "dubbo")
@RefreshScope
public class ContentServiceImpl implements ContentService {

    /**
     * 商城类DAO
     */
    @Resource(name = "dShopContentDAO")
    private DShopContentDAO dShopContentDAO;

    /**
     * 商城Redis
     */
    @Resource(name = "rShopMaterialId")
    private RShopMaterialId rShopMaterialId;

    /**
     * 文件类型
     *
     * @param myId
     * @return
     * @throws WayzException
     */
    @Override
    public List<ViewKeyValue> queryMaterialType(Long myId) throws WayzException {
        // 初始化
        List<ViewKeyValue> viewList = new ArrayList<ViewKeyValue>();
        for (CPlanMaterialType cMaterialType : CPlanMaterialType.values()) {
            ViewKeyValue view = new ViewKeyValue();
            view.setName(cMaterialType.getDescription());
            view.setId(cMaterialType.getValue());
            viewList.add(view);
        }
        return viewList;
    }

    /**
     * 审核状态
     *
     * @param myId
     * @return
     * @throws YungeException
     */
    @Override
    public List<ViewKeyValue> queryAuditType(Long myId) throws WayzException {
        List<ViewKeyValue> viewList = new ArrayList<ViewKeyValue>();
        ViewKeyValue view = null;
        for (CAuditStatus cAuditStatus : CAuditStatus.values()) {
            view = new ViewKeyValue();
            view.setId(cAuditStatus.getValue());
            view.setName(cAuditStatus.getDescription());
            viewList.add(view);
        }
        return viewList;
    }

    /**
     * 查询商城内容
     *
     * @param myId 我的客户标识
     * @param channelId 渠道标识
     * @param categoryId 分类标识
     * @param beginPrice 起始价格
     * @param endPrice 结束价格
     * @param type 文件类型(1:图片 2:视频 3:音频 4:网页 5:office文档)
     * @return 商城内容列表
     * @throws YungeException 平台异常
     */
    @Override
    public PageVo<ShopContentVo> queryShopContent(Long myId, Long channelId, Long categoryId, Double beginPrice,
                                                  Double endPrice, Short type, Integer startIndex, Integer pageSize, Short status) {
        // 初始化
        PageVo<ShopContentVo> resultList = new PageVo<>();
        List<ShopContentVo> shopList = new ArrayList<>();

        Integer countShopByCondition = dShopContentDAO.count(channelId, categoryId, beginPrice, endPrice, type, status);
        if (countShopByCondition <= 0) {
            return resultList;
        }

        List<DShopContent> shopContentList = dShopContentDAO.query(channelId, categoryId, beginPrice, endPrice, type,
                startIndex, pageSize, status);
        ShopContentVo shopContent;
        if (!shopContentList.isEmpty()) {
            for (DShopContent dShopContent : shopContentList) {
                shopContent = new ShopContentVo();
                BeanUtils.copyProperties(dShopContent, shopContent);
                shopContent.setCreatedTime(TimeHelper.getTimestamp(dShopContent.getCreatedTime()));
                shopList.add(shopContent);
            }
        }
        // 返回数据
        resultList.setTotalCount(countShopByCondition);
        resultList.setList(shopList);
        return resultList;
    }

    /**
     * 创建商城内容
     *
     * @param myId 我的客户标识
     * @param channelId 渠道标识
     * @param categoryId 分类标识
     * @param name 渠道名称
     * @param price 价格(元)
     * @param type 文件类型(1:图片 2:视频 3:音频 4:网页 5:office文档)
     * @param url 文件链接
     * @param previewUrl 预览链接
     * @param filemd5 文件检验码MD5
     * @param fileSize 文件长度(字节)
     * @param duration 时长(秒)
     * @param width 分辨率宽度
     * @param height 分辨率宽度
     * @return 内容标识
     * @throws WayzException 平台异常
     */
    @Override
    public Long createShop(Long myId, Long channelId, Long categoryId, String name, String title, String description,
                           Double price, Short type, String url, String previewUrl, String filemd5, Long fileSize, Integer duration,
                           Integer width, Integer height) throws WayzException {
        // 初始化
        Long id = rShopMaterialId.increment();
        DShopContent dCreate = new DShopContent();
        dCreate.setChannelId(channelId);
        dCreate.setCategoryId(categoryId);
        dCreate.setName(name);
        dCreate.setPrice(price);
        dCreate.setType(type);
        dCreate.setUrl(url);
        dCreate.setPreviewUrl(previewUrl);
        dCreate.setFileSize(fileSize);
        dCreate.setDuration(duration);
        dCreate.setWidth(width);
        dCreate.setHeight(height);
        dCreate.setDescription(description);
        dCreate.setTitle(title);
        dCreate.setCreatorId(myId);
        dCreate.setFilemd5(filemd5);
        dShopContentDAO.create(id, dCreate);

        // 返回数据
        return id;
    }

    /**
     * 删除商城内容
     *
     * @param myId 我的客户标识
     * @param id 商城内容标识
     * @throws WayzException 平台异常
     */
    @Override
    public void deleteShop(Long myId, Long id) throws WayzException {
        DShopContent dShopContent = dShopContentDAO.get(id);
        if (dShopContent == null) {
            throw new WayzException("内容不存在");
        }
        dShopContentDAO.deleteById(id);
    }

    /**
     * 修改商城内容
     *
     * @param myId 我的客户标识
     * @param id 商城内容标识
     * @param channelId 渠道标识
     * @param categoryId 分类标识
     * @param name 渠道名称
     * @param price 价格(元)
     * @param type 文件类型(1:图片 2:视频 3:音频 4:网页 5:office文档)
     * @param url 文件链接
     * @param previewUrl 预览链接
     * @param filemd5 文件检验码MD5
     * @param fileSize 文件长度(字节)
     * @param duration 时长(秒)
     * @param width 分辨率宽度
     * @param height 分辨率宽度
     * @throws YungeException 平台异常
     */
    @Override
    public void modifyShop(Long myId, Long id, Long channelId, Long categoryId, String name, String title,
                           String description, Double price, Short type, Short auditStatus, String url, String previewUrl,
                           String filemd5, Long fileSize, Integer duration, Integer width, Integer height) throws WayzException {
        DShopContent dShopContent = dShopContentDAO.get(id);
        if (dShopContent == null) {
            throw new WayzException("内容不存在");
        }

        if (dShopContent.getAuditStatus() == 1) {
            throw new WayzException("审核通过暂不允许修改");
        }
        // 修改
        DShopContent dModify = new DShopContent();
        dModify.setId(id);
        dModify.setChannelId(channelId);
        dModify.setCategoryId(categoryId);
        dModify.setName(name);
        dModify.setTitle(title);
        dModify.setDescription(description);
        dModify.setPrice(price);
        dModify.setType(type);
        dModify.setAuditStatus(auditStatus);
        dModify.setUrl(url);
        dModify.setPreviewUrl(previewUrl);
        dModify.setFilemd5(filemd5);
        dModify.setFileSize(fileSize);
        dModify.setDuration(duration);
        dModify.setWidth(width);
        dModify.setHeight(height);
        dShopContentDAO.modify(dModify);
    }

    /**
     * 获取商城内容
     *
     * @param myId 我的客户标识
     * @param id 商城内容标识
     * @return 商城内容
     * @throws YungeException 平台异常
     */
    @Override
    public ShopContentVo getShopContent(Long myId, Long id) {
        // 初始化
        ShopContentVo content = new ShopContentVo();
        DShopContent shopContent = dShopContentDAO.get(id);
        if (shopContent != null) {
            BeanUtils.copyProperties(shopContent, content);
            content.setCreatedTime(TimeHelper.getTimestamp(shopContent.getCreatedTime()));
        }

        // 返回数据
        return content;
    }

    /**
     * 内容审核
     *
     * @param myId 审核人
     * @param id 主键ID
     * @param status 0:未审核; 1:审核通过; 2: 审核未通过
     * @return
     * @throws WayzException
     */
    @Override
    public void auditorContent(Long myId, Long id, String remark, Short status) throws WayzException {
        dShopContentDAO.auditorContent(myId, id, remark, status);
    }
}
