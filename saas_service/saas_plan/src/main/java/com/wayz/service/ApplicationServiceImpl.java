package com.wayz.service;


import com.wayz.constants.PageVo;
import com.wayz.entity.pojo.application.DApplication;
import com.wayz.entity.vo.application.ApplicationVo;
import com.wayz.exception.WayzException;
import com.wayz.mapper.DApplicationDAO;
import com.wayz.redis.RApplicationId;
import com.wayz.util.TimeHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 应用服务实现类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Service("applicationService")
@org.apache.dubbo.config.annotation.Service(version = "1.0.0", protocol = "dubbo")
@RefreshScope
public class ApplicationServiceImpl implements ApplicationService {

    /**
     * 应用DAO
     */
    @Resource(name = "dApplicationDAO")
    private DApplicationDAO dApplicationDAO;

    /**
     * 应用Redis
     */
    @Resource(name = "rApplicationId")
    private RApplicationId rApplicationId;

    /**
     * 查询应用
     *
     * @param myId 我的客户标识
     * @param name 名称
     * @param starNum 星级
     * @param isEnable 是否有效
     * @return 应用列表
     */
    @Override
    public PageVo<ApplicationVo> queryApplication(Long myId, String name, Integer starNum, Boolean isEnable, Integer startIndex, Integer pageSize)
            throws WayzException {
        // 初始化
        PageVo<ApplicationVo> resultList = new PageVo<>();
        List<ApplicationVo> applicationList = new ArrayList<>();
        Integer count = dApplicationDAO.count(name, starNum, isEnable);
        if (count <= 0){
            return resultList;
        }
        List<DApplication> dApplicationsList = dApplicationDAO.queryApplicationList(name, starNum, isEnable, startIndex, pageSize);

        ApplicationVo application;
        if (!dApplicationsList.isEmpty()){
            for (DApplication dApplication : dApplicationsList){
                application = new ApplicationVo();
                BeanUtils.copyProperties(dApplication, application);
                application.setCreatedTime(TimeHelper.getTimestamp(dApplication.getCreatedTime()));
                if (dApplication.getCreatedTime() != null){
                    application.setModifiedTime(TimeHelper.getTimestamp(dApplication.getModifiedTime()));
                }
                applicationList.add(application);
            }
        }
        resultList.setTotalCount(count);
        resultList.setList(applicationList);
        // 返回数据
        return resultList;
    }


    /**
     * 获取应用
     *
     * @param myId 我的客户标识
     * @param id 应用标识
     * @return 应用
     * @throws YungeException 平台异常
     */
    @Override
    public ApplicationVo getApplication(Long myId, Long id) throws WayzException {
        // 初始化
        ApplicationVo application = new ApplicationVo();
        DApplication dApplication = dApplicationDAO.get(id);
        if (dApplication != null){
            BeanUtils.copyProperties(dApplication, application);
            application.setCreatedTime(TimeHelper.getTimestamp(dApplication.getCreatedTime()));
            if (dApplication.getCreatedTime() != null){
                application.setModifiedTime(TimeHelper.getTimestamp(dApplication.getModifiedTime()));
            }

        }
        // 返回数据
        return application;
    }


    /**
     * 创建应用
     *
     * @param myId 我的客户标识
     * @param name 名称
     * @param icon 图标
     * @param href 页面链接
     * @param starNum 星级
     * @param description 描述
     * @param images 应用截图
     * @param isEnable 是否有效
     * @return 应用标识
     * @throws YungeException 平台异常
     */
    @Override
    public Long createApplication(Long myId, String name, String icon,String href,Integer starNum,
                                  String description, String images, Boolean isEnable) throws WayzException {
        // 初始化
        Long id = rApplicationId.increment();
        DApplication create = new DApplication();
        create.setName(name);
        create.setIcon(icon);
        create.setHref(href);
        create.setStarNum(starNum);
        create.setDescription(description);
        create.setImages(images);
        create.setIsEnable(isEnable);
        dApplicationDAO.create(id,create);
        // 返回数据
        return id;
    }

    /**
     * 修改应用
     *
     * @param myId 我的客户标识
     * @param id 标识
     * @param name 名称
     * @param icon 图标
     * @param href 页面链接
     * @param starNum 星级
     * @param description 描述
     * @param images 应用截图
     * @param isEnable 是否有效
     * @throws YungeException 平台异常
     */
    @Override
    public void modifyApplication(Long myId, Long id, String name, String icon,String href,Integer starNum,
                                  String description, String images, Boolean isEnable) throws WayzException {
        DApplication dApplication = dApplicationDAO.get(id);
        if (dApplication == null){
            throw new WayzException("应用不存在");
        }
        DApplication modify = new DApplication();
        modify.setId(id);
        modify.setName(name);
        modify.setIcon(icon);
        modify.setHref(href);
        modify.setStarNum(starNum);
        modify.setDescription(description);
        modify.setImages(images);
        dApplicationDAO.modify(modify);
    }

    /**
     * 删除应用
     *
     * @param myId 我的客户标识
     * @param id 应用标识
     * @throws YungeException 平台异常
     */
    @Override
    public void deleteApplication(Long myId, Long id) throws WayzException {
        DApplication dApplication = dApplicationDAO.get(id);
        if (dApplication == null){
            throw new WayzException("应用不存在");
        }
        if (dApplication.getBuyCount() > 0){
            throw new WayzException("已被使用的应用不允许删除");
        }
        dApplicationDAO.delete(id);
    }
}
