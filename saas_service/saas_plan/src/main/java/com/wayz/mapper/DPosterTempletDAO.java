package com.wayz.mapper;

import java.util.List;

import com.wayz.constants.ViewMap;
import com.wayz.entity.pojo.poster.DPosterTemplet;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 海报模板DAO接口
 * 
 */
@Repository("dPosterTempletDAO")
public interface DPosterTempletDAO {

	/**
	 * 获取广告模板
	 * 
	 * @param id 模板标识
	 * @return 广告模板
	 */
	public DPosterTemplet get(@Param("id") Long id);

	/**
	 * 创建广告模板
	 * 
	 * @param id 模板标识
	 * @param create 广告模板创建
	 * @return 创建行数
	 */
	public Integer create(@Param("create") DPosterTemplet create);

	/**
	 * 修改广告模板
	 * 
	 * @param modify 广告模板修改
	 * @return 修改行数
	 */
	public Integer modify(@Param("modify") DPosterTemplet modify);

	/**
	 * 查询视图
	 * 
	 * @return
	 */
	public List<ViewMap> queryPosterTempletView(@Param("companyId") Long companyId);

	/**
	 * 统计数量
	 * 
	 * @param name 模板名称(模糊查询)
	 * @param resolutionId 分辨率标识
	 * @return
	 */
	public Integer count(@Param("companyId") Long companyId, @Param("name") String name,
			@Param("resolutionId") Long resolutionId, @Param("categoryId") Long categoryId, @Param("isPublish") Boolean isPublish);

	/**
	 * 查询模板信息
	 * 
	 * @param name 模板名称(模糊查询)
	 * @param resolutionId 分辨率标识
	 * @param startIndex 开始序号
	 * @param pageSize 页面大小
	 * @return
	 */
	public List<DPosterTemplet> query(@Param("companyId") Long companyId, @Param("name") String name,
			@Param("resolutionId") Long resolutionId, @Param("categoryId") Long categoryId, @Param("isPublish") Boolean isPublish, 
			@Param("startIndex") Integer startIndex, @Param("pageSize") Integer pageSize);

	/**
	 * 删除
	 * 
	 * @param id
	 */
	public void delete(@Param("id") Long id);
	
	/**
	 *  更新海报模板发布状态
	 *
	 * @param id 模板标识
	 * @param isPublish 是否发布
	 */
	public void updatePosterTempletStatus(@Param("id") Long id, @Param("isPublish") Boolean isPublish);
	
	/**
	 *  更新海报模板所属公司
	 */
	public void updatePosterCompanyIdByTempletId(@Param("id") Long id,@Param("companyId") Long companyId);
	
	/**
	 * 物理删除
	 * 
	 * @param id
	 */
	public void updateDelete(@Param("id") Long id);

	/**
	 * 查询名称唯一
	 * 
	 * @param name
	 * @param id
	 * @return
	 */
	public Boolean queryUnique(@Param("companyId") Long companyId, @Param("name") String name, @Param("id") Long id);

}
