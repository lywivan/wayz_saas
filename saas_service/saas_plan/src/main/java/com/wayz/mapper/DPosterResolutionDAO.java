package com.wayz.mapper;

import java.util.List;

import com.wayz.constants.ViewMap;
import com.wayz.entity.pojo.poster.DPosterResolution;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 海报分辨率DAO接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("dPosterResolutionDAO")
public interface DPosterResolutionDAO {

	/**
	 * 获取海报分辨率
	 * 
	 * @param id 分辨率标识
	 * @return 海报分辨率
	 */
	public DPosterResolution get(@Param("id") Long id);

	/**
	 * 创建海报分辨率
	 *
	 * @param create 海报分辨率创建
	 * @return 创建行数
	 */
	public Integer create(@Param("create") DPosterResolution create);

	/**
	 * 修改海报分辨率
	 * 
	 * @param modify 海报分辨率修改
	 * @return 修改行数
	 */
	public Integer modify(@Param("modify") DPosterResolution modify);

	/**
	 * 查询分辨率视图信息
	 * 
	 * @return 分辨率视图
	 */
	public List<ViewMap> queryResolutionView();

	/**
	 * 校验分辨率是否存在
	 * 
	 * @param name 分辨率名称
	 * @param id 分辨率标识
	 * @return
	 */
	public boolean existsName(@Param("name") String name, @Param("id") Long id);

	/**
	 * 查询分辨率列表
	 * 
	 * @param width 宽(pix)
	 * @param height 高(pix)
	 * @param startIndex 开始序号
	 * @param pageSize 页面大小
	 * @return
	 */
	public List<DPosterResolution> query(@Param("width") Integer width, @Param("height") Integer height,
										 @Param("startIndex") Integer startIndex, @Param("pageSize") Integer pageSize);

	/**
	 * 查询分辨率数量
	 * 
	 * @param width 宽(pix)
	 * @param height 高(pix)
	 * @return
	 */
	public Integer count(@Param("width") Integer width, @Param("height") Integer height);

	/**
	 * 根据名称获取分辨率信息
	 * 
	 * @param name 名称
	 * @return
	 */
	public DPosterResolution getByName(@Param("name") String name);

	/**
	 * 统计模板使用该分辨率数量
	 *
	 * @return
	 */
	Integer countPosterTempletByResolutionId(@Param("resolutionId") Long resolutionId);

	/**
	 * 删除海报分辨率
	 * 
	 * @param id 分辨率标识
	 * @return 海报分辨率
	 */
	public void delete(@Param("id") Long id);

}
