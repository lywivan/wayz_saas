package com.wayz.mapper;

import com.wayz.entity.pojo.application.DApplication;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 应用DAO接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("dApplicationDAO")
public interface DApplicationDAO {
	/**
	 * 统计应用数量
	 * @param name 名称
	 * @param starNum 星级
	 * @param isEnable 是否有效
	 * @return
	 */
	public Integer count(@Param("name") String name, @Param("starNum") Integer starNum, @Param("isEnable") Boolean isEnable);

	/**
	 * 查询应用列表
	 * @param name 名称
	 * @param starNum 星级
	 * @param isEnable 是否有效
	 * @param startIndex 开始角标
	 * @param pageSize   每页数量
	 * @return
	 */
	public List<DApplication> queryApplicationList(@Param("name") String name, @Param("starNum") Integer starNum, @Param("isEnable") Boolean isEnable, @Param("startIndex") Integer startIndex, @Param("pageSize") Integer pageSize);


	/**
	 * 获取应用
	 *
	 * @param id 标识
	 * @return 应用
	 */
	public DApplication get(@Param("id") Long id);

	/**
	 * 创建应用
	 *
	 * @param id 标识
	 * @param create 应用创建
	 * @return 创建行数
	 */
	public Integer create(@Param("id") Long id, @Param("create") DApplication create);

	/**
	 * 修改应用
	 *
	 * @param modify 应用修改
	 * @return 修改行数
	 */
	public Integer modify(@Param("modify") DApplication modify);

	/**
	 * 删除应用
	 * @param id
	 */
	public void delete(@Param("id") Long id);
}
