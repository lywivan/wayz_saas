package com.wayz.mapper;

import com.wayz.constants.ViewKeyValue;
import com.wayz.entity.pojo.category.DContentCategory;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 内容分类DAO接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("dContentCategoryDAO")
public interface DContentCategoryDAO {
	/**
	 * 统计内容分类数量
	 * @return
	 */
	Integer count(@Param("name") String name);

	/**
	 * 查询内容分类视图
	 * @return
	 */
	List<ViewKeyValue> queryListView(@Param("name") String name);

	/**
	 * 内容列表分页
	 * @param startIndex
	 * @param pageSize
	 * @return
	 */
	List<DContentCategory> queryListCategory(@Param("name") String name, @Param("startIndex") Integer startIndex, @Param("pageSize") Integer pageSize);

	/**
	 * 创建内容分类
	 *
	 * @param id 标识
	 * @param create 内容分类创建
	 * @return 创建行数
	 */
	public Integer create(@Param("id") Long id, @Param("create") DContentCategory create);

	/**
	 * 获取内容分类
	 *
	 * @param id 标识
	 * @return 内容分类
	 */
	public DContentCategory get(@Param("id") Long id);

	/**
	 * 删除内容分类
	 * @param categoryId 分类ID
	 */
	void deleteCategory(@Param("categoryId") Long categoryId);

	/**
	 * 修改内容分类
	 *
	 * @param modify 内容分类修改
	 * @return 修改行数
	 */
	public Integer modify(@Param("modify") DContentCategory modify);

}
