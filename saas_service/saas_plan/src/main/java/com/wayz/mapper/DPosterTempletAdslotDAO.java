package com.wayz.mapper;

import java.util.List;

import com.wayz.entity.pojo.poster.DPosterTempletAdslot;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;


/**
 * 海报模板模块配置DAO接口
 * 
 */
@Repository("dPosterTempletAdslotDAO")
public interface DPosterTempletAdslotDAO {

	/**
	 * 获取广告模板分屏配置
	 * 
	 * @param posterTempletId 模板标识
	 * @param num 序号
	 * @return 广告模板分屏配置
	 */
	public DPosterTempletAdslot get(@Param("posterTempletId") Long posterTempletId, @Param("num") Integer num);

	/**
	 * 创建广告模板分屏配置
	 * 
	 * @param create 广告模板分屏配置创建
	 * @return 创建行数
	 */
	public Integer create(@Param("create") DPosterTempletAdslot create);

	/**
	 * 修改广告模板分屏配置
	 * 
	 * @param modify 广告模板分屏配置修改
	 * @return 修改行数
	 */
	public Integer modify(@Param("modify") DPosterTempletAdslot modify);

	/**
	 * 删除广告模板分屏配置
	 * 
	 * @param posterTempletId 模板标识
	 * @param num 序号
	 * @return 删除行数
	 */
	public Integer delete(@Param("posterTempletId") Long posterTempletId, @Param("num") Integer num);

	/**
	 * 查询
	 * 
	 * @param posterTempletId 模板标识
	 * @return 播控器模板广告位配置列表
	 */
	public List<DPosterTempletAdslot> query(@Param("posterTempletId") Long posterTempletId);

	/**
	 * 统计数量
	 * 
	 * @param posterTempletId 模板标识
	 * @return
	 */
	public Integer count(@Param("posterTempletId") Long posterTempletId);

}
