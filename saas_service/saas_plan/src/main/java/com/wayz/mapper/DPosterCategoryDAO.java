package com.wayz.mapper;

import java.util.List;

import com.wayz.entity.pojo.poster.DPosterCategory;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 海报类别DAO接口
 * 
 *
 */
@Repository("dPosterCategoryDAO")
public interface DPosterCategoryDAO {

	/**
	 * 获取海报类别
	 * 
	 * @param id 标识
	 * @return 海报类别
	 */
	public DPosterCategory get(@Param("id") Long id);

	/**
	 * 创建海报类别
	 * 
	 * @param id 标识
	 * @param create 海报类别创建
	 * @return 创建行数
	 */
	public Integer create(@Param("create") DPosterCategory create);

	/**
	 * 修改海报类别
	 * 
	 * @param modify 海报类别修改
	 * @return 修改行数
	 */
	public Integer modify(@Param("modify") DPosterCategory modify);

	/**
	 * 查询海报类别
	 * @return 海报类别列表
	 */
	public List<DPosterCategory> query(@Param("id") Long id);

	/**
	 * 校验海报类别名称是否存在
	 * 
	 * @param name 海报类别名称
	 * @param id 海报类别标识
	 * @return
	 */
	public boolean existsName(@Param("name") String name, @Param("id") Long id);

	
	/**
	 * 获取海报类别
	 * 
	 * @param name 海报类别名称
	 * @return 海报类别
	 */
	public DPosterCategory getByName(@Param("name") String name);
	
	/**
	 * 统计模板使用该类别数量
	 * 
	 * @param id
	 * @return
	 */
	Integer countPosterTempletByCategoryId(@Param("categoryId") Long categoryId);

}
