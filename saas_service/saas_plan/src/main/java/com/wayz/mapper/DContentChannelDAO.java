package com.wayz.mapper;

import com.wayz.constants.ViewKeyValue;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 内容渠道DAO接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("dContentChannelDAO")
public interface DContentChannelDAO {
	/**
	 * 统计渠道数量
	 * @return
	 */
	Integer countChannel(@Param("name") String name);

	/**
	 * 查询内容渠道视图
	 * @param name
	 * @return
	 */
	List<ViewKeyValue> queryListView(@Param("name") String name);

}
