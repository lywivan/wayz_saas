package com.wayz.mapper;

import java.util.List;

import com.wayz.entity.pojo.content.DShopContent;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 商城内容DAO接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("dShopContentDAO")
public interface DShopContentDAO {
	/**
	 * 商城内容数量
	 *
	 * @param channelId
	 * @param categoryId
	 * @param beginPrice
	 * @param endPrice
	 * @param type
	 * @return
	 */
	Integer count(@Param("channelId") Long channelId, @Param("categoryId") Long categoryId,
				  @Param("beginPrice") Double beginPrice, @Param("endPrice") Double endPrice, @Param("type") Short type,
				  @Param("status") Short status);

	/**
	 * 查询商城内容
	 *
	 * @param channelId
	 * @param categoryId
	 * @param beginPrice
	 * @param endPrice
	 * @param type
	 * @return
	 */
	List<DShopContent> query(@Param("channelId") Long channelId, @Param("categoryId") Long categoryId,
							 @Param("beginPrice") Double beginPrice, @Param("endPrice") Double endPrice, @Param("type") Short type,
							 @Param("startIndex") Integer startIndex, @Param("pageSize") Integer pageSize, @Param("status") Short status);

	/**
	 * 创建商城内容
	 *
	 * @param id 文件标识
	 * @param create 商城内容创建
	 * @return 创建行数
	 */
	public Integer create(@Param("id") Long id, @Param("create") DShopContent create);

	/**
	 * 逻辑删除商城
	 *
	 * @param id
	 */
	public void deleteById(@Param("id") Long id);

	/**
	 * 获取商城内容
	 *
	 * @param id 文件标识
	 * @return 商城内容
	 */
	public DShopContent get(@Param("id") Long id);

	/**
	 * 修改商城内容
	 *
	 * @param modify 商城内容修改
	 * @return 修改行数
	 */
	public Integer modify(@Param("modify") DShopContent modify);

	public void auditorContent(@Param("myId") Long myId, @Param("id") Long id, @Param("remark") String remark,
							   @Param("status") Short status);
}
