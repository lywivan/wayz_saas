package com.wayz.mapper;

import com.wayz.entity.pojo.auditor.DAuditorUser;
import com.wayz.entity.vo.AuditorUserVO;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.*;


/**
 * 审核方用户DAO接口
 * 
 */
@Repository("dAuditorUserDAO")
public interface DAuditorUserDAO {

	/**
	 * 获取审核方用户
	 * 
	 * @param id 用户标识
	 * @return 审核方用户
	 */
	AuditorUserVO get(@Param("id") Long id);

	/**
	 * 获取审核方用户
	 * 
	 * @param username
	 * @return 审核方用户
	 */
	AuditorUserVO getByUsernameOrPhone(@Param("username") String username);

	/**
	 * 创建审核方用户
	 *
	 * @param create 审核方用户创建
	 * @return 创建行数
	 */
	Integer create(@Param("create") DAuditorUser create);

	/**
	 * 修改审核方用户
	 * 
	 * @param modify 审核方用户修改
	 * @return 修改行数
	 */
	Integer modify(@Param("modify") DAuditorUser modify);

	/**
	 * 根据手机号 检查是否存在用户
	 * 
	 * @param telephone
	 * @return
	 */
	Boolean existByPhone(@Param("telephone") String telephone);

	/**
	 * 更新密码
	 * 
	 * @param id
	 * @param newPassword
	 */
	void updatePassword(@Param("id") Long id, @Param("newPassword") String newPassword);
}
