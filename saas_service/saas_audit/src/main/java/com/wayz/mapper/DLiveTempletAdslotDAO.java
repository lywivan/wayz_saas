package com.wayz.mapper;

import java.util.List;

import com.wayz.entity.pojo.live.DLiveTempletAdslot;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.*;

/**
 * 直播模板分屏配置DAO接口
 * 
 */
@Repository("dLiveTempletAdslotDAO")
public interface DLiveTempletAdslotDAO {

	/**
	 * 获取直播模板分屏配置
	 * 
	 * @param templetId 模板标识
	 * @param num 序号
	 * @return 直播模板分屏配置
	 */
	public DLiveTempletAdslot get(@Param("templetId") Long templetId, @Param("num") Integer num);

	/**
	 * 创建直播模板分屏配置
	 * 
	 * @param create 直播模板分屏配置创建
	 * @return 创建行数
	 */
	public Integer create(@Param("create") DLiveTempletAdslot create);

	/**
	 * 修改直播模板分屏配置
	 * 
	 * @param modify 直播模板分屏配置修改
	 * @return 修改行数
	 */
	public Integer modify(@Param("modify") DLiveTempletAdslot modify);

	/**
	 * 删除直播模板分屏配置
	 * 
	 * @param templetId 模板标识
	 * @return 删除行数
	 */
	public Integer delete(@Param("templetId") Long templetId);

	/**
	 * 查询
	 * 
	 * @param templetId 模板标识
	 * @return 播控器模板直播位配置列表
	 */
	public List<DLiveTempletAdslot> query(@Param("templetId") Long templetId);

	/**
	 * 统计数量
	 * 
	 * @param templetId 模板标识
	 * @return
	 */
	public Integer count(@Param("templetId") Long templetId);

}
