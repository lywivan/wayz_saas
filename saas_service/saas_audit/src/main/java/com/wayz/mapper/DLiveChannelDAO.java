package com.wayz.mapper;

import com.wayz.entity.pojo.live.DLiveChannel;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 直播渠道DAO接口
 * 
 */
@Repository("dLiveChannelDAO")
public interface DLiveChannelDAO {
    /**
     * 存在直播渠道名称
     *
     * @param name 直播渠道名称
     * @param id 直播渠道标识
     * @return 是否存在
     */
    public Boolean existsName(@Param("name") String name, @Param("id") Long id);

    /**
     * 创建直播渠道
     */
    public Integer create(@Param("id") Long id, @Param("name") String name);

    /**
     * 修改直播渠道
     */
    public Integer modify(@Param("id") Long id, @Param("name") String name);

    /**
     * 获取直播渠道
     *
     * @param id 直播渠道标识
     * @return 直播渠道信息
     */
    public DLiveChannel get(@Param("id") Long id);

    /**
     * 删除直播渠道
     *
     * @param id 直播渠道id
     * @return
     */
    public Integer delete(@Param("id") Long id);

    /**
     * 查询直播渠道
     *
     * @return 直播渠道列表
     */
    public List<DLiveChannel> query();
}
