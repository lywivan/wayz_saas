package com.wayz.mapper;

import java.util.List;

import com.wayz.constants.ViewMap;
import com.wayz.entity.pojo.live.DLiveResolution;
import com.wayz.entity.pojo.live.DLiveTemplet;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 直播模板DAO接口
 * 
 */
@Repository("dLiveTempletDAO")
public interface DLiveTempletDAO {

	/**
	 * 查询分辨率视图信息
	 * 
	 * @return 分辨率视图
	 */
	public List<ViewMap> queryLiveResolutionView();

	/**
	 * 获取屏幕分辨率
	 * 
	 * @param id 分辨率标识
	 * @return 屏幕分辨率
	 */
	public DLiveResolution getLiveResolution(@Param("id") Long id);

	/**
	 * 获取直播模板
	 * 
	 * @param id 模板标识
	 * @return 直播模板
	 */
	public DLiveTemplet get(@Param("id") Long id);

	/**
	 * 获取名称
	 * 
	 * @param backgroundAudio
	 * @return
	 */
	// public String getAudioNameByUrl(@Param("backgroundAudio") String
	// backgroundAudio);

	/**
	 * 创建直播模板
	 *
	 * @param create 直播模板创建
	 * @return 创建行数
	 */
	public Integer create(@Param("create") DLiveTemplet create);

	/**
	 * 修改直播模板
	 * 
	 * @param modify 直播模板修改
	 * @return 修改行数
	 */
	public Integer modify(@Param("modify") DLiveTemplet modify);

	/**
	 * 删除指定分辨率下所有模板
	 * 
	 * @param resolutionId 分辨率标识
	 */
	public void deleteByResolutionId(@Param("resolutionId") Long resolutionId);

	/**
	 * 查询名称唯一
	 * 
	 * @param name
	 * @param id
	 * @return
	 */
	public Boolean queryUnique(@Param("companyId") Long companyId, @Param("name") String name, @Param("id") Long id);

	/**
	 * 查询视图
	 * 
	 * @return
	 */
	public List<ViewMap> queryLiveTempletView(@Param("companyId") Long companyId);

	/**
	 * 统计数量
	 * 
	 * @param name 模板名称(模糊查询)
	 * @param resolutionId 分辨率标识
	 * @return
	 */
	public Integer count(@Param("companyId") Long companyId, @Param("name") String name,
			@Param("resolutionId") Long resolutionId);

	/**
	 * 查询模板信息
	 * 
	 * @param name 模板名称(模糊查询)
	 * @param resolutionId 分辨率标识
	 * @param startIndex 开始序号
	 * @param pageSize 页面大小
	 * @return
	 */
	public List<DLiveTemplet> query(@Param("companyId") Long companyId, @Param("name") String name,
			@Param("resolutionId") Long resolutionId, @Param("startIndex") Integer startIndex,
			@Param("pageSize") Integer pageSize);

}
