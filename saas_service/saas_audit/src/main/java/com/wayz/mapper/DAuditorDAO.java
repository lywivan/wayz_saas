package com.wayz.mapper;

import java.util.List;

import com.wayz.entity.dto.AuditorDto;
import com.wayz.entity.dto.AuditorQueryDto;
import com.wayz.entity.pojo.auditor.DAuditor;
import com.wayz.entity.vo.AuditorVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;


/**
 * 审核方DAO接口
 * 
 */
@Repository("dAuditorDAO")
public interface DAuditorDAO {

	/**
	 * 获取审核方
	 * 
	 * @param id 审核方标识
	 * @return 审核方
	 */
	public AuditorVO get(@Param("id") Long id);

	/**
	 * 根据名称 检查审核方是否存在
	 * 
	 * @param name
	 * @return
	 */
	public Boolean existByName(@Param("name") String name, @Param("id") Long id);

	/**
	 * 根据手机号 检查审核方是否存在
	 * 
	 * @param telephone
	 * @return
	 */
	public Boolean existByPhone(@Param("telephone") String telephone, @Param("id") Long id);

	/**
	 * 创建审核方
	 *
	 * @param create 审核方创建
	 * @return 创建行数
	 */
	public Integer create(@Param("create") DAuditor create);

	/**
	 * 修改审核方
	 * 
	 * @param modify 审核方修改
	 * @return 修改行数
	 */
	public Integer modify(@Param("modify") DAuditor modify);

	/**
	 * 根据条件统计审核方
	 *
	 * @return
	 */
	public Integer count(@Param("query") AuditorQueryDto query);

	/**
	 * 分页查询审核方列表
	 *
	 * @return
	 */
	public List<AuditorVO> query(@Param("query") AuditorQueryDto query);

	/**
	 * 修改审核方状态
	 * 
	 * @param id 审核方ID
	 * @param status 审核方状态(1:启用 2:禁用;)
	 * @return
	 */
	public Integer updateAuditorStatus(@Param("id") Long id, @Param("status") Short status);
}
