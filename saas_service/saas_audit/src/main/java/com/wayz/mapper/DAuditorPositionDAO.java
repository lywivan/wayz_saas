package com.wayz.mapper;

import com.wayz.entity.dto.AuditorPositionQueryDto;
import com.wayz.entity.vo.AuditorPositionVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * 审核方设备DAO接口
 * 
 * @author mahao
 *
 */
@Service("dAuditorPositionDAO")
public interface DAuditorPositionDAO {

	List<Long> queryPositionIdByAuditorId(@Param("auditorId") Long auditorId);

	List<AuditorPositionVO> querySelectablePosition(@Param("query") AuditorPositionQueryDto query,
													@Param("positionIdList") List<Long> positionIdList);

	Integer countSelectablePosition(@Param("query") AuditorPositionQueryDto query,
			@Param("positionIdList") List<Long> positionIdList);

	List<AuditorPositionVO> querySelectedPosition(@Param("query") AuditorPositionQueryDto query);

	Integer countSelectedPosition(@Param("query") AuditorPositionQueryDto query);

	void addPosition(@Param("auditorId") Long auditorId, @Param("positionIdList") List<Long> positionIdList);

	void deletePosition(@Param("auditorId") Long auditorId, @Param("positionIdList") List<Long> positionIdList);

	/**
	 * 根据已选择的广告位查询审核方标识列表 
	 */
	public List<Long> queryAuditorIdByPositionIdList(@Param("positionIdList") List<Long> positionIdList);
}
