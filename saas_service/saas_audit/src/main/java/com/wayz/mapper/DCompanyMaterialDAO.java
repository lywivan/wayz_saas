package com.wayz.mapper;

import com.wayz.entity.pojo.live.DCompanyMaterial;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 公司素材DAO接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("dCompanyMaterialDAO")
public interface DCompanyMaterialDAO {

	/**
	 * 获取公司素材
	 * 
	 * @param id 文件标识
	 * @return 公司素材
	 */
	public DCompanyMaterial get(@Param("id") Long id);

}
