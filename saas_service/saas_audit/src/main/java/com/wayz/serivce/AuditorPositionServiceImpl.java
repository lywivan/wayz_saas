package com.wayz.serivce;

import com.wayz.constants.PageVo;
import com.wayz.entity.dto.AuditorPositionQueryDto;
import com.wayz.entity.vo.AuditorPositionVO;
import com.wayz.entity.redis.company.RCompany;
import com.wayz.exception.WayzException;
import com.wayz.mapper.DAuditorDAO;
import com.wayz.mapper.DAuditorPositionDAO;
import com.wayz.redis.RCompanyObject;
import com.wayz.redis.RedisUtils;
import com.wayz.service.AuditorPositionService;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**
 * 审核方设备服务实现类
 *
 * @author mike.ma@wayz.ai
 */
@Service("auditorPositionService")
@org.apache.dubbo.config.annotation.Service(version = "1.0.0", protocol = "dubbo")
@RefreshScope
public class AuditorPositionServiceImpl implements AuditorPositionService {

	/** Redis工具类 */
	@Resource(name = "redisUtils")
	private RedisUtils redisUtils;
	/** 公司对象 */
	@Resource(name = "rCompanyObject")
	private RCompanyObject rCompanyObject = null;
	
	/** 审核方DAO */
	@Resource(name = "dAuditorDAO")
	private DAuditorDAO dAuditorDAO;
	/** 审核方设备DAO */
	@Resource(name = "dAuditorPositionDAO")
	private DAuditorPositionDAO dAuditorPositionDAO;

	@Override
	public PageVo<AuditorPositionVO> querySelectablePosition(Long myId, AuditorPositionQueryDto dto)
			throws WayzException {
		PageVo<AuditorPositionVO> page = new PageVo<>();

		// 查询已选择的标识列表
		List<Long> selectedPositionIdList = dAuditorPositionDAO.queryPositionIdByAuditorId(null);

		Integer totalCount = dAuditorPositionDAO.countSelectablePosition(dto, selectedPositionIdList);
		if (totalCount <= 0) {
			return page;
		}
		List<AuditorPositionVO> list = dAuditorPositionDAO.querySelectablePosition(dto, selectedPositionIdList);
		for (AuditorPositionVO vo : list) {
			vo.setCompanyName(rCompanyObject.getString(vo.getCompanyId(), RCompany.NAME));
		}
		page.setTotalCount(totalCount);
		page.setList(list);
		return page;
	}

	@Override
	public PageVo<AuditorPositionVO> querySelectedPosition(Long myId, AuditorPositionQueryDto dto)
			throws WayzException {
		PageVo<AuditorPositionVO> page = new PageVo<>();
		Integer totalCount = dAuditorPositionDAO.countSelectedPosition(dto);
		if (totalCount <= 0) {
			return page;
		}
		List<AuditorPositionVO> list = dAuditorPositionDAO.querySelectedPosition(dto);
		for (AuditorPositionVO vo : list) {
			vo.setCompanyName(rCompanyObject.getString(vo.getCompanyId(), RCompany.NAME));
		}
		page.setTotalCount(totalCount);
		page.setList(list);
		return page;
	}

	@Override
	public void addPosition(Long myId, Long auditorId, List<Long> positionIdList) throws WayzException {
		dAuditorPositionDAO.addPosition(auditorId, positionIdList);
	}

	@Override
	public void deletePosition(Long myId, Long auditorId, List<Long> positionIdList) throws WayzException {
		dAuditorPositionDAO.deletePosition(auditorId, positionIdList);
	}

}
