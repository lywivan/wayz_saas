package com.wayz.service;

import com.wayz.exception.WayzException;

/**
 * @className: InitService
 * @description: 初始化数据服务
 * <功能详细描述>
 * @author: 须尽欢_____
 * @date: 2019/7/1
 */
public interface InitService {

    String init() throws WayzException;
}
