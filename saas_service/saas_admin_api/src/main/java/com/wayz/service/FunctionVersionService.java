package com.wayz.service;

import com.wayz.constants.ViewMap;
import com.wayz.entity.vo.function.FunctionVersionVo;
import com.wayz.exception.WayzException;

import java.util.List;

/**
 * 功能版本服务接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
public interface FunctionVersionService {

	/**
	 * 修改功能版本
	 * 
	 * @param myId 我的标识
	 * @param id 版本标识
	 * @param name 版本名称
	 * @param description 描述
	 * @param userLimit 用户数限制
	 * @param materialLimit 素材空间限制
	 * @param singleFileLimit 单个文件大小限制
	 * @throws WayzException 平台异常
	 */
	void modifyFunctionVersion(Long myId, Long id, String name, String description, Integer userLimit,
			Long materialLimit, Long singleFileLimit) throws WayzException;

	/**
	 * 创建功能版本
	 *
	 * @param myId 我的标识
	 * @param name 版本名称
	 * @param description 描述
	 * @param userLimit 用户数限制
	 * @param materialLimit 素材空间限制
	 * @param singleFileLimit 单个文件大小限制
	 * @return 版本标识
	 * @throws WayzException 平台异常
	 */
	Long createFunctionVersion(Long myId, String name, String description, Integer userLimit, Long materialLimit,
			Long singleFileLimit) throws WayzException;

	/**
	 * 查询功能版本
	 *
	 * @param myId 我的标识
	 * @return 版本列表
	 * @throws WayzException 平台异常
	 */
	List<FunctionVersionVo> queryFunctionVersion(Long myId, String name) throws WayzException;

	/**
	 * 给管理员角色授权
	 * 
	 * @param versionId 版本标识
	 * @param menuIdList 权限菜单列表
	 */
	void setVersionMenuList(Long myId, Long versionId, List<Long> menuIdList) throws WayzException;

	/**
	 * 根据Id获取版本管理员角色Id
	 * 
	 * @param myId
	 * @param versionId
	 * @return
	 */
	Long getRoleIdByVersionId(Long myId, Long versionId);

	/**
	 * 查询版本视图
	 * 
	 * @param myId
	 * @return
	 * @throws WayzException
	 */
	List<ViewMap> queryVersionView(Long myId);

	/**
	 * 根据Id获取版本信息
	 * 
	 * @param myId
	 * @param id
	 * @return
	 */
	FunctionVersionVo getVersion(Long myId, Long id);

	/**
	 * 删除功能版本
	 *
	 * @param myId 我的标识
	 * @param id 版本标识
	 * @throws WayzException 平台异常
	 */
	void deleteFunctionVersion(Long myId, Long id) throws WayzException;


}
