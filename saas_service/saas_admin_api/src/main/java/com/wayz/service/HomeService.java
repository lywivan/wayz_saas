package com.wayz.service;

import com.wayz.constants.PageVo;
import com.wayz.constants.ViewMap;
import com.wayz.entity.vo.home.*;
import com.wayz.exception.WayzException;

import java.util.List;

/**
 * 官网首页服务接口
 * 
 * @author mike.ma@wayz.ai
 *
 */
public interface HomeService {

	/**
	 * 查询省份
	 * 
	 * @param myId 我的标识
	 * @param provinceId 省份标识
	 * @return 省份列表
	 * @throws WayzException 云歌广告平台异常
	 */
	public List<ProvinceVo> queryProvince(Long myId, Long provinceId) throws WayzException;

	/**
	 * 查询城市
	 * 
	 * @param myId 我的标识
	 * @param provinceId 省份标识
	 * @param cityId 城市标识
	 * @return 城市列表
	 * @throws WayzException 云歌广告平台异常
	 */
	List<CityVo> queryCity(Long myId, Long provinceId, Long cityId) throws WayzException;

	/**
	 * 查询新闻分类
	 * 
	 * @param myId 我的标识
	 * @return 客户视图列表
	 * @throws WayzException 平台异常
	 */
	List<ViewMap> queryArticleCategory(Long myId, Short id) throws WayzException;

	/**
	 * 创建新闻
	 * 
	 * @param myId 我的标识
	 * @param categoryId 分类报道
	 * @param title 标题
	 * @param titlePic 标题图片
	 * @param description 描述
	 * @param keyWords 关键词
	 * @param abstractContent 摘要
	 * @param content 新闻详情
	 * @param pushTime 发布时间
	 * @param isPush 是否发布(0=否,1=是)
	 * @param sort 排序
	 * @throws WayzException 平台异常
	 */
	void createArticle(Long myId, Short categoryId, String title, String titlePic, String description,
			String keyWords, String abstractContent, String content, String pushTime, Boolean isPush, Integer sort)
			throws WayzException;

	/**
	 * 修改新闻
	 * 
	 * @param myId 我的标识
	 * @param id 标识
	 * @param categoryId 分类报道
	 * @param title 标题
	 * @param titlePic 标题图片
	 * @param description 描述
	 * @param keyWords 关键词
	 * @param abstractContent 摘要
	 * @param content 新闻详情
	 * @param pushTime 发布时间
	 * @param isPush 是否发布(0=否,1=是)
	 * @param sort 排序
	 * @throws WayzException 平台异常
	 */
	void modifyArticle(Long myId, Long id, Short categoryId, String title, String titlePic, String description,
			String keyWords, String abstractContent, String content, String pushTime, Boolean isPush, Integer sort)
			throws WayzException;

	/**
	 * 删除新闻
	 * 
	 * @param myId 我的标识
	 * @param id 标识
	 * @throws WayzException 平台异常
	 */
	void delArticle(Long myId, Long id) throws WayzException;

	/**
	 * 修改发布
	 * 
	 * @param myId 我的标识
	 * @param id 标识
	 * @throws WayzException 平台异常
	 */
	void modifyArticlePush(Long myId, Long id) throws WayzException;

	/**
	 * 查询发布
	 * 
	 * @param myId 我的标识
	 * @param categoryId 分类报道
	 * @param title 标题
	 * @param isPush 是否发布(0=否,1=是)
	 * @return 新闻分页
	 * @throws WayzException 平台异常
	 */
	PageVo<ArticleVo> queryArticle(Long myId, Short categoryId, String title, Boolean isPush, Integer startIndex,
										  Integer pageSize) throws WayzException;

	/**
	 * 新闻详情
	 * 
	 * @param myId 我的标识
	 * @param id 标识
	 * @return 新闻详情
	 * @throws WayzException 平台异常
	 */
	ArticleVo getArticle(Long myId, Long id) throws WayzException;

	/**
	 * 获取合作案例
	 * 
	 * @param id 案例标识
	 * @return 合作案例
	 * @throws WayzException 平台异常
	 */
	CaseVo getCase(Long id) throws WayzException;

	/**
	 * 查询合作案例
	 *
	 * @param title 案例标题(模糊查询)
	 * @return 合作案例分页
	 * @throws WayzException 平台异常
	 */
	PageVo<CaseVo> queryCase(String title, Boolean isPush, Integer startIndex, Integer pageSize) throws WayzException;

	/**
	 * 修改合作案例
	 * 
	 * @param myId 我的标识
	 * @param id 案例标识
	 * @param title 标题
	 * @param subTitle 副标题
	 * @param titlePic 标题图片url
	 * @param imgUrl 列表图片url
	 * @param seoTitle seo标题
	 * @param keyWords 关键字
	 * @param description 描述
	 * @param content 新闻详情
	 * @param pushTime 发布时间
	 * @param isPush 是否发布(0=否,1=是)
	 * @param sort 排序
	 * @throws WayzException 平台异常
	 */
	void modifyCase(Long myId, Long id, String title, String subTitle, String titlePic, String imgUrl,
			String seoTitle, String keyWords, String description, String content, String pushTime, Boolean isPush,
			Integer sort) throws WayzException;

	/**
	 * 创建合作案例
	 * 
	 * @param myId 我的标识
	 * @param title 标题
	 * @param subTitle 副标题
	 * @param titlePic 标题图片url
	 * @param imgUrl 列表图片url
	 * @param seoTitle seo标题
	 * @param keyWords 关键字
	 * @param description 描述
	 * @param content 新闻详情
	 * @param pushTime 发布时间
	 * @param isPush 是否发布(0=否,1=是)
	 * @param sort 排序
	 * @throws WayzException 平台异常
	 */
	void createCase(Long myId, String title, String subTitle, String titlePic, String imgUrl, String seoTitle,
			String keyWords, String description, String content, String pushTime, Boolean isPush, Integer sort)
			throws WayzException;

	/**
	 * 删除合作案例
	 * 
	 * @param myId 我的标识
	 * @param id 案例标识
	 * @throws WayzException 平台异常
	 */
	void deleteCase(Long myId, Long id) throws WayzException;

	/**
	 * 
	 * <p>修改发布</p>
	 * 
	 * @throws WayzException
	 * @date 2017年9月23日
	 */
	void modifyCasePush(Long myId, Long id) throws WayzException;

	/**
	 * 查询表单数据
	 * 
	 * @param myId
	 * @param type 类型
	 * @param companyName 公司名称
	 * @param phone 手机号
	 * @param startIndex 开始序号
	 * @param pageSize 页面大小
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	PageVo<FormDataVo> queryFormData(Long myId, Short type, Long provinceId, Long cityId, String companyName,
												 String phone, Integer startIndex, Integer pageSize) throws WayzException;

	/**
	 * 查询表单数据分类
	 *
	 * @param myId 我的标识
	 * @return 表单数据视图列表
	 * @throws 平台异常
	 */
	List<ViewMap> queryFormDataCategory(Long myId, Short id) throws WayzException;
}
