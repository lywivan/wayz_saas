package com.wayz.service;

import com.wayz.constants.PageVo;
import com.wayz.constants.ViewMap;
import com.wayz.entity.dto.role.RoleDto;
import com.wayz.entity.dto.role.RoleMenuIdsDto;
import com.wayz.entity.dto.role.RolePageDto;
import com.wayz.entity.vo.role.RoleAdminVo;
import com.wayz.exception.WayzException;

import java.util.List;



/**
 * 角色服务接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
public interface RoleAdminService {

	/**
	 * 查询角色
	 * 
	 * @param myId 我的标识
	 * @param startIndex 开始序号
	 * @param pageSize 页面大小
	 * @return 角色分页
	 * @throws WayzException 云歌广告平台异常
	 */
	PageVo<RoleAdminVo> queryRole(Long myId, RolePageDto rolePageDto) throws WayzException;

	/**
	 * 查询角色视图
	 *
	 * @param myId 我的标识
	 * @return 角色视图列表
	 * @throws WayzException 云歌广告平台异常
	 */
	List<ViewMap> queryRoleView(Long myId) throws WayzException;

	/**
	 * 创建角色
	 * 
	 * @param myId 我的标识
	 * @return 角色标识
	 * @throws WayzException 云歌广告平台异常
	 */
	Long createRole(Long myId, RoleDto roleDto) throws WayzException;

	/**
	 * 修改角色
	 * 
	 * @param myId 我的标识
	 * @param id 角色标识
	 * @param name 角色名称
	 * @param description 角色描述
	 * @throws WayzException 云歌广告平台异常
	 */
	void modifyRole(Long myId, RoleDto roleDto) throws WayzException;

	/**
	 * 删除角色
	 * 
	 * @param myId 我的标识
	 * @param id 角色标识
	 * @throws WayzException 云歌广告平台异常
	 */
	void deleteRole(Long myId, Long id) throws WayzException;

	/**
	 * 给角色授权菜单
	 * @param myId
	 * @throws WayzException
	 */
    Integer authorityRole(Long myId, RoleMenuIdsDto roleMenuIdsDto);

}
