package com.wayz.service;


import com.wayz.constants.PageVo;
import com.wayz.entity.dto.notice.NoticeDto;
import com.wayz.entity.vo.notice.NoticeVo;
import com.wayz.exception.WayzException;

/**
 * 系统消息接口
 *
 * @author mike.ma@wayz.ai
 */
public interface NoticeService {

    /**
     * 消息列表
     *
     * @param title      标题
     * @param startIndex 开始角标
     * @param pageSize   页面大小
     * @return
     */
    PageVo<NoticeVo> queryNotice(String title, Integer startIndex, Integer pageSize);

    /**
     * 创建系统消息通知
     *
     * @param noticeDto
     * @throws WayzException
     */
    void createNotice(Long myId, NoticeDto noticeDto) throws WayzException;

    /**
     * 修改系统消息通知
     *
     * @param noticeDto
     * @throws WayzException
     */
    void modifyNotice(Long myId, NoticeDto noticeDto) throws WayzException;

    /**
     * 消息通知详情
     *
     * @param id
     * @return
     */
    NoticeVo getNotice(Long id);

    /**
     * 删除系统消息通知
     *
     * @param noticeId
     */
    void deleteNotice(Long noticeId);
}
