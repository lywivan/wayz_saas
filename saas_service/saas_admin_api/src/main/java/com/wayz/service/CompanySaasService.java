package com.wayz.service;

import com.wayz.constants.PageVo;
import com.wayz.constants.ViewMap;
import com.wayz.entity.dto.company.CompanyDto;
import com.wayz.entity.dto.company.CompanyPageDto;
import com.wayz.entity.pojo.company.DCompanyOEM;
import com.wayz.entity.pojo.company.TradeRecord;
import com.wayz.entity.vo.company.CompanyVo;
import com.wayz.exception.WayzException;

import java.util.List;

public interface CompanySaasService {

	// ===========================整理后接口=======================================

	/**
	 * 查询公司视图
	 * 
	 * @return
	 */
	List<ViewMap> queryViewMap();

	/**
	 * 获取公司详情
	 *
	 * @param myId 我的标识
	 * @param id 公司标识
	 * @return 公司详情
	 * @throws WayzException
	 */
	CompanyVo getCompanyDetail(Long myId, Long id) throws WayzException;

	/**
	 * 创建公司
	 *
	 * @throws WayzException 平台异常
	 */
	public Long createCompany(Long myId, CompanyDto companyDto) throws WayzException;

	/**
	 * 统计使用某版本的公司数量
	 *
	 * @param versionId
	 * @return
	 */
	Integer countCompanyByVersionId(Long versionId);

	/**
	 * 修改公司
	 *
	 * @param myId 我的标识
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	void modifyCompany(Long myId, CompanyDto companyDto) throws WayzException;

	/**
	 * 改变公司状态 (1:启用 2:禁用;)
	 *
	 * @param companyId
	 * @param status
	 * @return
	 */
	Integer modifyCompanyStatus(Long companyId, Short status, String validUntilDate, Boolean isWatermark);

	/**
	 * 查询公司列表
	 *
	 * @param companyPageDto 分页参数
	 * @return
	 */
	public PageVo<CompanyVo> queryCompany(Long myId, CompanyPageDto companyPageDto);

	/**
	 * OEM配置
	 *
	 * @param myId
	 * @param logo
	 * @param domain
	 * @param websiteTitle
	 */
	void updateCompanyOEM(Long myId, Long id, String logo, String domain, String websiteTitle);

	/**
	 * 获取公司OEM信息
	 *
	 * @param myId
	 * @param id
	 * @return
	 */
	DCompanyOEM getCompanyOEM(Long myId, Long id);

	/**
	 * 删除公司
	 *
	 * @param myId
	 * @param id
	 */
	void deleteCompany(Long myId, Long id);

	/**
	 * 公司充值
	 *
	 * @param myId 用户标识
	 * @param companyId 公司标识
	 * @param amount 充值金额(元)
	 * @param payment 实付金额(元)
	 * @param tradeType 充值类型(11:现金充值 12:红包充值 13:转账充值 14:退款充值)
	 * @param remark 备注
	 *
	 * @return 账户余额
	 * @throws WayzException 平台异常
	 */
	public Double rechargeCompany(Long myId, Long companyId, Double amount, Double payment, Short tradeType,
								  String remark) throws WayzException;

	/**
	 * 查询交易概要
	 *
	 * @param myId 我的标识
	 * @param companyId 公司标识
	 * @param operatorType 操作员类型类型(1:平台 2:代理 3:租户 )
	 * @param tradeType 交易类型(11:现金充值 12:红包充值 13:转账充值 14:退款充值 21:设备续期消费
	 *            22:空间扩展消费)
	 * @param beginDate 开始日期(YYYY-MM-DD)
	 * @param endDate 结束日期(YYYY-MM-DD)
	 * @param startIndex 开始序号
	 * @param pageSize 页面大小
	 * @return 交易概要分页
	 * @throws YungeException 平台异常
	 */
	public PageVo<TradeRecord> queryTradeRecord(Long myId, Long companyId, Short operatorType, Short tradeType,
												String beginDate, String endDate, Integer startIndex, Integer pageSize) throws WayzException;

	/**
	 * 获取公司主账户
	 *
	 * @param myId 我的标识
	 * @param id 公司标识
	 * @return 云歌应答
	 * @throws YungeException 云歌广告平台异常
	 */
	CompanyVo getCompanyMainAccount(Long myId, Long id) throws WayzException;

	/**
	 * 重置主账号密码
	 *
	 * @param myId
	 * @param id
	 * @return
	 * @throws YungeException
	 */
	void resetUserPassword(Long myId, Long id, String newPassword) throws WayzException;
}
