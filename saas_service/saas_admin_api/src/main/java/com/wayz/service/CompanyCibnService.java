package com.wayz.service;

import com.wayz.constants.PageVo;
import com.wayz.constants.ViewKeyValue;
import com.wayz.entity.dto.cibn.CompanyCibnDto;
import com.wayz.entity.vo.cibn.CompanyCibnVo;
import com.wayz.exception.WayzException;

import java.util.List;


/**
 * CIBN服务接口
 *
 * @author mike.ma@wayz.ai
 */
public interface CompanyCibnService {

    /**
     * 审核状态
     *
     * @param myId
     * @return
     * @throws WayzException
     */
    List<ViewKeyValue> queryAuditType(Long myId) throws WayzException;

    /**
     * 创建申请记录
     *
     * @param companyCibnDto
     * @return
     * @throws WayzException
     */
    Long createCibn(CompanyCibnDto companyCibnDto) throws WayzException;

    /**
     * 修改申请记录
     *
     * @param companyCibnDto
     * @throws WayzException
     */
    void modifyCibn(CompanyCibnDto companyCibnDto) throws WayzException;

    /**
     * 申请记录列表
     *
     * @param companyName 公司名称
     * @param auditStatus 审核状态
     * @param industryId  行业ID
     * @return
     * @throws WayzException
     */
    PageVo<CompanyCibnVo> query(String companyName, Short auditStatus, Long industryId, Integer startIndex, Integer pageSize) throws WayzException;

    /**
     * 审核申请记录
     *
     * @param companyCibnDto
     * @throws WayzException
     */
    void auditCibn(CompanyCibnDto companyCibnDto) throws WayzException;

    /**
     * 申请记录详情 -- 管理端
     *
     * @param companyId
     * @return
     */
    CompanyCibnVo getAdminCibn(Long companyId) throws WayzException;

    /**
     * 申请记录详情 -- 服务端
     *
     * @param companyId
     * @return
     */
    CompanyCibnVo getUserCibn(Long companyId) throws WayzException;

}
