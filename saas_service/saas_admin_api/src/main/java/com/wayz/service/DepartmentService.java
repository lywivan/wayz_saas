package com.wayz.service;

import com.wayz.exception.WayzException;

/**
 * 部分服务接口类
 * 
 * @Author: LGQ
 * @Date: 2020/10/21
 */
public interface DepartmentService {
    /**
     * 创建部门
     *
     * @param myId
     * @param companyId 公司标示
     * @param name 部门名称
     * @param parentId 部门父级标示
     * @param haveGroup 是否存在设备组
     * @return
     * @throws WayzException
     */
    public Long createDepartment(Long myId, Long companyId, String name, Long parentId) throws WayzException;
}
