package com.wayz.service;

import com.wayz.entity.dto.agent.AgentDto;
import com.wayz.exception.WayzException;

/**
 * 代理商服务接口类
 *
 * @author mike.ma@wayz.ai
 */
public interface AgentUserService {

    /**
     * 创建代理商用户
     *
     * @param myId
     * @param agentId
     * @param roleId
     * @param dto
     * @return
     * @throws WayzException
     */
    Long createUser(Long myId, Long agentId, Long roleId, AgentDto dto) throws WayzException;

    /**
     * 代理商重置密码
     *
     * @param id          代理商ID
     * @param newPassword 新的密码
     * @throws WayzException
     */
    void resetAgentUserPassword(Long myId, Long id, String newPassword) throws WayzException;
}
