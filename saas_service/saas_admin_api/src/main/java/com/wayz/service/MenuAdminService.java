package com.wayz.service;

import com.wayz.entity.pojo.menu.DMenu;
import com.wayz.entity.vo.menu.MenuVo;
import com.wayz.exception.WayzException;

import java.util.List;


/**
 * 菜单服务接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
public interface MenuAdminService {

	/**
	 * 查询全部菜单
	 *
	 * @param myId 我的标识
	 * @return 我的菜单列表
	 * @throws WayzException 云歌广告平台异常
	 */
	List<MenuVo> queryAllMenu(Long myId);

	/**
	 * 查询我的菜单
	 *
	 * @param myId 我的标识
	 * @return 我的菜单列表
	 * @throws WayzException 云歌广告平台异常
	 */
	List<MenuVo> queryMyMenu(Long myId);

	void deleteMenu(Long myId, Long id) throws WayzException;

	/**
	 * 查询角色权限
	 *
	 * @param myId 我的标识
	 * @param roleId 角色标识
	 * @return 权限列表
	 * @throws WayzException 云歌广告平台异常
	 */
	List<DMenu> queryRoleMenu(Long myId, Long roleId);

	/**
	 * 查询我的视图权限
	 *
	 * @param myId 我的标识
	 * @param view 视图名称
	 * @return 视图权限列表
	 * @throws WayzException 云歌广告平台异常
	 */
	List<String> queryViewAuthority(Long myId, String view) throws WayzException;

	/**
	 * 查询我的菜单
	 *
	 * @param myId 我的标识
	 * @param href 菜单链接
	 * @return 检查结果
	 * @throws WayzException 云歌广告平台异常
	 */
	Boolean checkMyMenu(Long myId, String href) throws WayzException;

	/**
	 * 根据菜单id批量获取菜单连接
	 * @param menuIdList
	 * @return
	 */
	List<String> queryMenuHref(List<Long> menuIdList);

	/**
	 * 初始化时使用，查询所有菜单id
	 * @return
	 */
	List<Long> menuIdList();

}
