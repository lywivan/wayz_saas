package com.wayz.service;

import com.wayz.exception.WayzException;

import java.util.List;

/**
 * @className: RoleSaasService
 * @description: saas角色 <功能详细描述>
 * @author wade.liu@wayz.ai
 * @date: 2019/6/26
 */
public interface RoleSaasService {

	/**
	 * 创建版本管理员角色
	 * 
	 * @param description
	 * @return
	 */
	Long createVersionRole(String description) throws WayzException;

	/**
	 * 给saas角色授权
	 * 
	 * @param roleId
	 * @param menuIdList
	 * @return
	 */
	Integer authorityRoleSaaa(Long roleId, List<Long> menuIdList);

	/**
	 * 删除saas版本管理员角色
	 * 
	 * @param description
	 * @return
	 */
	public void deleteVersionRole(Long roleId) throws WayzException;

	/**
	 * 删除角色菜单关联
	 * @param roleId 角色ID
	 */
	public void deleteMenuRoleId(Long roleId) throws WayzException;
}
