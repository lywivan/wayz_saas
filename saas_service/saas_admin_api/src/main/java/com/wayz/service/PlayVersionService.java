package com.wayz.service;

import com.wayz.constants.PageVo;
import com.wayz.entity.dto.playVersion.PlayVersionDto;
import com.wayz.entity.vo.playVersion.PlayVersionVo;
import com.wayz.exception.WayzException;

import java.util.List;

/**
 * @className: PlayVersionService
 * @description: 播控版本
 * <功能详细描述>
 * @author: 须尽欢_____
 * @date: 2019/7/9
 */
public interface PlayVersionService {

    Integer create(PlayVersionDto PlayVersionDto) throws WayzException;

    PageVo<PlayVersionVo> queryAll(Integer startIndex, Integer pageSize);

    /**
     * 给公司分配播控版本
     * @param version
     * @param companyIdList
     * @return
     */
    Integer grantCompany(String version, List<Long> companyIdList);

    /**
     * 查询那些公司在使用当前版本
     * @param version
     * @return
     */
    List<Long> queryCompanyIdListByVersion(String version);

}
