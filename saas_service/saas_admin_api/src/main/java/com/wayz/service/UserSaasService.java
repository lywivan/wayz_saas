package com.wayz.service;

import com.wayz.entity.pojo.user.DUserSaas;
import com.wayz.entity.vo.company.CompanyVo;
import com.wayz.exception.WayzException;

/**
 * @className: UserSaasService
 * @description: saas用户 <功能详细描述>
 * @author wade.liu@wayz.ai
 * @date: 2019/6/27
 */
public interface UserSaasService {
	/**
	 * 创建用户
	 *
	 * @param myId 我的标识
	 * @param roleId 角色标识
	 * @param username 用户名称
	 * @param name 用户姓名
	 * @param phone 用户电话
	 * @param email 用户邮箱
	 * @return 用户标识
	 * @throws WayzException 云歌广告平台异常
	 */
	Long createUser(Long myId, Long companyId, Long roleId, String username, String name, String phone, String email)
			throws WayzException;

	/**
	 * 获取公司主账户
	 *
	 * @param companyId 公司标识
	 * @return 公司主账户
	 * @throws
	 */
	CompanyVo getCompanyMainAccount(Long companyId, Short type) throws WayzException;

	/**
	 * 变更用户角色
	 *
	 * @param id
	 * @param roleId
	 * @return
	 */
	Integer updateUserSaasRoleId(Long id, Long roleId);

	/**
	 * 根据手机号查询用户
	 *
	 * @param phone
	 * @return
	 */
	DUserSaas getUserSaasByPhone(String phone);

	/**
	 * 变更用户手机号
	 *
	 * @param id 用户标识
	 * @param phone 手机号
	 * @return
	 */
	public void modifyUserPhone(Long id, String phone) throws WayzException;

	/**
	 * 修改联系人
	 * @param id 用户标识
	 * @param contacts 联系人
	 */
	public void modifyUserName(Long id, String contacts);

	/**
	 * 重置主账号密码
	 * @param companyId
	 * @return
	 * @throws
	 */
	public void resetUserPassword(Long companyId, Short type,String newPassword) throws WayzException;
}
