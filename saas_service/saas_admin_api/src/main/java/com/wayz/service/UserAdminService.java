package com.wayz.service;

import com.wayz.constants.PageVo;
import com.wayz.entity.dto.user.LoginDto;
import com.wayz.entity.dto.user.PageUserAdminDto;
import com.wayz.entity.dto.user.UserAdminDto;
import com.wayz.entity.vo.user.UserAdminVo;
import com.wayz.exception.WayzException;

/**
 * 用户服务接口
 * 
 * @author yinjy
 *
 */
public interface UserAdminService {

	/**
	 * 获取我的用户
	 *
	 * @param myId 我的标识
	 * @return 我的用户
	 * @throws WayzException 云歌广告平台异常
	 */
	UserAdminVo getMyUser(Long myId) throws WayzException;

	/**
	 * 获取验证码
	 *
	 * @param sessionId 会话标识
	 * @return 字节数组
	 * @throws WayzException 健身馆异常
	 */
	byte[] getAuthcode(String sessionId) throws WayzException;

	/**
	 * 登录
	 * @return 登录令牌
	 * @throws WayzException 云歌广告平台异常
	 */
	String login(LoginDto loginDto) throws WayzException;

	/**
	 * 获取我的标识
	 *
	 * @param token 登录令牌
	 * @return 我的标识
	 * @throws WayzException 云歌广告平台异常
	 */
	Long getMyId(String token) throws WayzException;

	/**
	 * 创建用户
	 *
	 * @param myId 我的标识
	 * @return 用户标识
	 * @throws WayzException 云歌广告平台异常
	 */
	Long createUser(Long myId, UserAdminDto userAdminDto) throws WayzException;

	/**
	 * 查询用户
	 *
	 * @param myId 我的标识
	 * @return 用户分页
	 * @throws WayzException 云歌广告平台异常
	 */
	PageVo<UserAdminVo> queryUser(Long myId, PageUserAdminDto pageUserAdminDto);

	/**
	 * 修改用户
	 *
	 * @param myId 我的标识
	 * @throws WayzException 云歌广告平台异常
	 */
	void modifyUser(Long myId, UserAdminDto userAdminDto) throws WayzException;

	/**
	 * 修改用户的 启用/禁用 状态
	 * @param myId
	 * @return
	 * @throws WayzException
	 */
	void updateUserStatus(Long myId, UserAdminDto userAdminDto) throws WayzException;

	/**
	 * 修改密码
	 *
	 * @param myId 我的标识
	 * @throws WayzException 云歌广告平台异常
	 */
	void modifyPassword(Long myId, UserAdminDto userAdminDto) throws WayzException;

	/**
	 * 删除用户
	 *
	 * @param myId 我的标识
	 * @param id 用户标识
	 * @throws WayzException 云歌广告平台异常
	 */
	void deleteUser(Long myId, Long id) throws WayzException;

	/**
	 * 统计指定角色的用户数量
	 * @param roleId
	 * @return
	 */
	Integer countUserByRoleId(Long roleId);
}
