package com.wayz.service;

import com.wayz.entity.vo.menu.MenuVo;
import com.wayz.exception.WayzException;

import java.util.List;


/**
 * @className: MenuSaasService
 * @description: saas菜单
 * <功能详细描述>
 * @author wade.liu@wayz.ai
 * @date: 2019/6/21
 */
public interface MenuSaasService {

    /**
     * 查询全部菜单
     *
     * @param myId 我的标识
     * @return 我的菜单列表
     * @throws WayzException 云歌广告平台    */
    List<MenuVo> queryAllMenu(Long myId);

    /**
     * 查询版本选中的菜单
     * @param myId
     * @param versionId
     * @return
     */
    List<MenuVo> queryMenuByVersionId(Long myId, Long versionId);

    /**
     * 查询全部菜单id集合
     * @return
     */
    List<Long> queryMenuIdList();
}
