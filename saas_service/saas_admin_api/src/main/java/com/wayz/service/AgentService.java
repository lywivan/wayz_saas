package com.wayz.service;

import com.wayz.constants.PageVo;
import com.wayz.constants.ViewKeyValue;
import com.wayz.entity.dto.agent.AgentDto;
import com.wayz.entity.vo.agent.AgentCompanyVo;
import com.wayz.entity.vo.agent.AgentOEMVo;
import com.wayz.entity.vo.agent.AgentVO;
import com.wayz.exception.WayzException;

import java.util.List;

/**
 * 代理商服务接口类
 *
 * @author mike.ma@wayz.ai
 */
public interface AgentService {
	/**
	 * 代理商列表id name
	 *
	 * @return
	 */
	List<ViewKeyValue> queryAgentCompanyView() throws WayzException;

	/**
	 * 代理商列表
	 *
	 * @param myId 我的ID
	 * @param name 代理商名称
	 * @param phone 手机号
	 * @param status 状态
	 * @param startIndex 开始角标
	 * @param pageSize 每页大小
	 * @return
	 */
	PageVo<AgentCompanyVo> queryAgentCompany(Long myId, String name, String phone, Short status, Long provinceId,
													Long cityId, Long categoryId, Integer startIndex, Integer pageSize) throws WayzException;

	/**
	 * 获取代理商详情
	 *
	 * @param myId 我的标识
	 * @param id 代理商标识
	 * @return
	 * @throws WayzException
	 */
	AgentCompanyVo getAgentCompany(Long myId, Long id) throws WayzException;

	/**
	 * 创建代理商
	 *
	 * @param myId 我的ID
	 * @param dto 创建实体类
	 * @throws WayzException
	 */
	void createAgentCompany(Long myId, AgentDto dto) throws WayzException;

	/**
	 * 修改代理商
	 *
	 * @param myId 我的ID
	 * @param dto 实体类
	 * @throws WayzException
	 */
	void modifyAgentCompany(Long myId, AgentDto dto) throws WayzException;

	/**
	 * 修改代理商状态
	 *
	 * @param id 代理商ID
	 * @param status 代理商状态(1:启用 2:禁用;)
	 * @return
	 */
	Integer modifyAgentStatus(Long id, Short status) throws WayzException;

	/**
	 * 获取代理商OEM
	 *
	 * @param myId
	 * @param id 代理商ID
	 * @return
	 * @throws WayzException
	 */
	AgentOEMVo getAgentOEM(Long myId, Long id) throws WayzException;
	
	/**
	 * 创建代理商和公司关系
	 *
	 * @param companyId 公司ID
	 * @param agentId 代理商ID
	 * @param isLogin 是否一键登录
	 * @throws WayzException
	 */
	void createAgentRelation(Long companyId, Long agentId, Boolean isLogin) throws WayzException;

	/**
	 * 删除代理商和公司关系
	 *
	 * @param companyId 公司ID
	 */
	void deleteAgentRelation(Long companyId) throws WayzException;

	/**
	 * 更新代理商和公司关系
	 *
	 * @param companyId 公司ID
	 * @param agentId 代理商ID
	 * @param isLogin 是否一键登录
	 */
	void updateAgentRelation(Long companyId, Long agentId, Boolean isLogin) throws WayzException;

	/**
	 * OEM配置
	 *
	 * @param myId
	 * @param id 代理商ID
	 * @param logo OEM logo
	 * @param domain OEM 域名
	 * @param websiteTitle OEM 名称
	 * @throws WayzException
	 */
	void createAgentOEM(Long myId, Long id, String logo, String domain, String websiteTitle)
			throws WayzException;

}
