package com.wayz.service;

import com.wayz.exception.WayzException;

/**
 * 
 * <p>账户计划任务</p>
 * 
 * @date 2020年11月16日
 * @author wade.liu@wayz.ai
 */
public interface AccountValid {

	/**
	 * 
	 * 执行测试账户失效任务
	 * 
	 * @throws WayzException
	 */
	public void checkTestingAccount() throws WayzException;

	/**
	 * 
	 * 处理设备失效
	 * 
	 * @throws WayzException
	 */
	public void invalidPosition() throws WayzException;

	/**
	 * 
	 * 处理代金券失效
	 * 
	 * @throws WayzException
	 */
	public void invalidCoupon() throws WayzException;

	/**
	 *
	 * 设备到期提醒
	 *
	 * @throws WayzException
	 */
	public void remindExpire() throws WayzException;
}
