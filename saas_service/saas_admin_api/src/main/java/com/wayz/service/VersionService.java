package com.wayz.service;

import com.wayz.constants.ViewMap;

import java.util.List;

/**
 * 功能版本服务接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
public interface VersionService {

	/**
	 * 查询版本视图
	 * 
	 * @param myId
	 * @return
	 * @throws YungeException
	 */
	List<ViewMap> queryVersionView(Long myId);

	/**
	 * 根据Id获取版本管理员角色Id
	 *
	 * @param myId
	 * @param versionId
	 * @return
	 */
	Long getRoleIdByVersionId(Long myId, Long versionId);

}
