package com.wayz.entity.vo.role;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @className: DRoleSaas
 * @description: saas角色
 * <功能详细描述>
 * @author wade.liu@wayz.ai
 * @date: 2019/6/26
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleSaasVo implements Serializable {

    /** 用户标识 */
    private Long id = null;
    /**公司编号*/
    private Long companyId;
    /** 角色类型：0管理员 1：公司自建角色 */
    private Short type = null;
    /** 角色状态(0:禁用;1:启用) */
    private Short status = null;
    /** 角色名称 */
    private String name = null;
    /** 角色描述 */
    private String description = null;
    /** 创建时间(YYYY-MM-DD HH:MM:SS) */
    private String createTime = null;
    /** 是否删除*/
    private Boolean isDeleted;

}
