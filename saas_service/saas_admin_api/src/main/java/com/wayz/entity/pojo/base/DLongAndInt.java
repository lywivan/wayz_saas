package com.wayz.entity.pojo.base;

import java.io.Serializable;

import lombok.Data;

/**
 * @author wade.liu@wayz.ai
 *
 */
@Data
public class DLongAndInt implements Serializable {
	/** 数据标识 */
	private Long id = null;
	/** 相应值 */
	private Integer val = null;
}
