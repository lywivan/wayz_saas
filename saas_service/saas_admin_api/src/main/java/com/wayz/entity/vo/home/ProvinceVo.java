package com.wayz.entity.vo.home;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 省份类
 * 
 * @author mike.ma@wayz.ai
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProvinceVo implements Serializable {

	/** 省份标识 */
	private Long id = null;
	/** 省份名称 */
	private String name = null;
	/** 省份简称 */
	private String sname = null;

}
