package com.wayz.entity.dto.function;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @className: VersionMenuIdsDto
 * @author wade.liu@wayz.ai
 * @date: 2019/6/26
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class VersionMenuIdsDto implements Serializable {

	/** 版本id */
	private Long versionId;
	/** 菜单ID集合 */
	private List<Long> menuIdList = null;

}
