package com.wayz.entity.pojo.function;

import java.io.Serializable;
import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @Description TODO
 *
 * @author wade.liu@wayz.ai
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DFunctionVersion implements Serializable {

	/** 版本标识 */
	private Long id = null;
	/** 版本名称 */
	private String name = null;
	/** 管理员角色编号 */
	private Long roleId;
	/** 描述 */
	private String description = null;
	/** 用户数限制 */
	private Integer userLimit = null;
	/** 素材空间限制 */
	private Long materialLimit = null;
	/** 单个文件限制(字节) */
	private Long singleFileLimit = null;
	/** 创建时间 */
	private Timestamp createdTime = null;
	/** 修改时间 */
	private Timestamp modifiedTime = null;

}
