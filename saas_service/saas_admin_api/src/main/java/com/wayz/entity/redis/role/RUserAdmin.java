package com.wayz.entity.redis.role;

import java.text.MessageFormat;

/**
 * 用户类
 * 
 * @author wade.liu@wayz.ai
 *
 */
public class RUserAdmin {

	/** 键值格式 */
	private static final MessageFormat KEY_FORMAT = new MessageFormat("platform:Object:Admin:User:{0}");

	/** 属性相关 */
	/** 角色标识 */
	private Long roleId = null;
	/** 用户状态(0:禁用;1:启用) */
	private Short status = null;
	/** 用户名称 */
	private String username = null;
	/** 用户姓名 */
	private String name = null;
	/** 用户电话 */
	private String phone = null;
	/** 用户职务 */
	private String title = null;
	/** 用户邮箱 */
	private String email = null;
	/** 用户头像 */
	private String avatar = null;

	/**
	 * 获取key值方法
	 * @param id
	 * @return
	 */
	public static String getKey(Long id){
		return KEY_FORMAT.format(new Long[] { id });
	}

	/** 常量相关 */
	/** 角色标识 */
	public static final String ROLEID = "roleId";
	/** 用户状态(0:禁用;1:启用) */
	public static final String STATUS = "status";
	/** 用户名称 */
	public static final String USERNAME = "username";
	/** 用户姓名 */
	public static final String NAME = "name";
	/** 用户电话 */
	public static final String PHONE = "phone";
	/** 用户职务 */
	public static final String TITLE = "title";
	/** 用户邮箱 */
	public static final String EMAIL = "email";
	/** 用户头像 */
	public static final String AVATAR = "avatar";

	/**
	 * 获取角色标识
	 *
	 * @return 角色标识
	 */
	public Long getRoleId() {
		return roleId;
	}

	/**
	 * 设置角色标识
	 *
	 * @param roleId 角色标识
	 */
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}



	/**
	 * 获取用户状态(0:禁用;1:启用)
	 *
	 * @return 用户状态(0:禁用;1:启用)
	 */
	public Short getStatus() {
		return status;
	}

	/**
	 * 设置用户状态(0:禁用;1:启用)
	 *
	 * @param status 用户状态(0:禁用;1:启用)
	 */
	public void setStatus(Short status) {
		this.status = status;
	}

	/**
	 * 获取用户名称
	 *
	 * @return 用户名称
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * 设置用户名称
	 *
	 * @param username 用户名称
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * 获取用户姓名
	 *
	 * @return 用户姓名
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置用户姓名
	 *
	 * @param name 用户姓名
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * 获取用户电话
	 *
	 * @return 用户电话
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * 设置用户电话
	 *
	 * @param phone 用户电话
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * 获取用户职务
	 *
	 * @return 用户职务
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * 设置用户职务
	 *
	 * @param title 用户职务
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 获取用户邮箱
	 *
	 * @return 用户邮箱
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * 设置用户邮箱
	 *
	 * @param email 用户邮箱
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * 获取用户头像
	 *
	 * @return 用户头像
	 */
	public String getAvatar() {
		return avatar;
	}

	/**
	 * 设置用户头像
	 *
	 * @param avatar 用户头像
	 */
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

}
