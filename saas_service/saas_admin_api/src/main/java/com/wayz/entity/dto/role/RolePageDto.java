package com.wayz.entity.dto.role;

import com.wayz.constants.PageDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @className: RolePageDto
 * @description: 角色分页 <功能详细描述>
 * @author wade.liu@wayz.ai
 * @date: 2019/6/26
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RolePageDto extends PageDto implements Serializable {

	/** 角色名称 */
	private String name;

}
