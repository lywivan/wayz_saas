package com.wayz.entity.pojo.company;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @className: CompanyOEM
 * @description: 公司OEM
 * <功能详细描述>
 * @author wade.liu@wayz.ai
 * @date: 2019/6/27
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DCompanyOEM implements Serializable {

    /** 公司编号*/
    private Long id;
    /** logo(OEM使用) */
    private String logo = null;
    /** 域名(OEM使用) */
    private String domain = null;
    /**平台名称*/
    private String websiteTitle;

}
