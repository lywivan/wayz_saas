package com.wayz.entity.dto.company;

import java.io.Serializable;
import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @className: CompanyCreateVo
 * @description: 公司创建类 <功能详细描述>
 * @author wade.liu@wayz.ai
 * @date: 2019/6/26
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CompanyDto implements Serializable {

	/** 公司id */
	private Long id;
	/** 公司名称 */
	private String name = null;
	/** 联系人 */
	private String contacts = null;
	/** 手机号 */
	private String telephone = null;
	/** 联系邮箱 */
	private String email = null;
	/** 通信地址 */
	private String address = null;
	/** 公司状态(1:启用 2:禁用 3:试用中 4:已过期) */
	private Short status = null;
	/** 版本标识 */
	private Long versionId = null;
	/** 是否需要数据权限功能 */
	private Boolean requireDataAuth = null;
	/** 是否需要广告审核功能 */
	private Boolean requirePlanAudit = null;
	/** 用户数限制 */
	private Integer userLimit = null;
	/** 媒体位数量限制 */
	private Integer positionLimit = null;
	/** 试用账户有效期至 */
	private String validUntilDate = null;
	/** 试用账户加水印 */
	private Boolean isWatermark = null;
	/** 用户名 */
	private String userName;
	/** 素材空间限制(Byte) */
	private Long materialLimit = null;
	/** 是否有广告 */
	private Boolean isAdPaln;
	/** 素材大小(MB) */
	private Integer materialSize;
	/** 是否一键登录 */
	private Boolean isLogin;
	/** 代理商ID */
	private Long agentId;

}
