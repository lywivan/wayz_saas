package com.wayz.entity.pojo.home;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 合作案例内容类
 * 
 * @author mike.ma@wayz.ai
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DCase implements Serializable {

	/** 案例标识 */
	private Long id = null;
	/** 标题 */
	private String title = null;
	private String subTitle = null;
	private String titlePic = null;
	/** 图片url */
	private String imgUrl = null;
	private String seoTitle = null;
	/** 关键词 */
	private String keyWords = null;
	/** 描述 */
	private String description = null;
	private String content = null;
	/** 发布时间 */
	private String pushTime = null;
	/** 是否已发布(0=否，1=是) */
	private Boolean isPush = null;
	/** 排序 */
	private Integer sort = null;
	/** 是否删除(0=否,1=是) */
	private Boolean isDel = null;
	/** 创建时间 */
	private Timestamp createdTime = null;
	/** 更新时间 */
	private Timestamp modifiedTime = null;

}
