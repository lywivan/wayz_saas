package com.wayz.entity.dto.playVersion;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @className: PlayVersionDto
 * @description: 播控版本 <功能详细描述>
 * @author wade.liu@wayz.ai
 * @date: 2019/7/9
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlayVersionDto implements Serializable {

	/** 版本号 */
	private String version;
	/** 下载地址 */
	private String packageUrl;
	/** md5验证码 */
	private String fileMd5;
	/** 版本描述 */
	private String description;
	/** 是否为通用版 */
	private Boolean isGeneral;
	/** 公司标识 */
	private Long companyId;
	/** 公司名称 */
	private String companyName;
	/** 创建时间 */
	private String createdTime;

}
