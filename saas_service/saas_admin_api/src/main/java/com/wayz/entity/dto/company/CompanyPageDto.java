package com.wayz.entity.dto.company;

import com.wayz.constants.PageDto;
import com.wayz.constants.PageVo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @className: CompanyCreateVo
 * @description: 公司创建类 <功能详细描述>
 * @author wade.liu@wayz.ai
 * @date: 2019/6/26
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CompanyPageDto extends PageDto implements Serializable {

	/** 公司名称 */
	private String name = null;
	/** 联系人手机号 */
	private String phone = null;
	/** 公司状态 */
	private Short status = null;
	/** 版本号 */
	private Long versionId = null;
	/** 公司创建来源 1：管理员 2：用户自主 */
	private Short sourceType = null;
	/** 代理商id */
	private Long agentId = null;

}
