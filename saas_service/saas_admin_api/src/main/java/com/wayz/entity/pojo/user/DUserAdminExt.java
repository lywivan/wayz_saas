package com.wayz.entity.pojo.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 *
 * @Description TODO
 *
 * @author wade.liu@wayz.ai
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class DUserAdminExt extends DUserAdmin {

	/** 公司名称 */
	private String companyName = null;
	/** 角色名称 */
	private String roleName = null;
	/** 角色类型 */
	private Short roleType = null;

}
