package com.wayz.entity.dto.role;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @className: RoleMenuIdsDto
 * @author wade.liu@wayz.ai
 * @date: 2019/6/26
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleMenuIdsDto implements Serializable {

	/** 角色id */
	private Long roleId;
	/** 菜单ID集合 */
	private List<Long> menuIdList = null;

}
