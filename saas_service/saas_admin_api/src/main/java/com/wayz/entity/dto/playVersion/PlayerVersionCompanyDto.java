/**
 * xxxx
 *
 * @author:ivan.liu
 */
package com.wayz.entity.dto.playVersion;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class PlayerVersionCompanyDto implements Serializable {
    //version 版本名称
    private String version;
    //companyIdList 公司编号
    private List<Long> companyIdList;
}
