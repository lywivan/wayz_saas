package com.wayz.entity.pojo.home;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 省份类
 * 
 * @author mike.ma@wayz.ai
 * 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DProvince {

	/** 省份标识 */
	private Long id = null;
	/** 省份名称 */
	private String name = null;
	/** 省份简称 */
	private String sname = null;
	/** 省份描述 */
	private String description = null;

}
