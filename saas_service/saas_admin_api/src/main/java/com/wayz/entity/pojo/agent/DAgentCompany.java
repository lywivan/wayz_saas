package com.wayz.entity.pojo.agent;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.*;
import java.sql.*;

/**
 * 代理商类
 * 
 * @author mike.ma@wayz.ai
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DAgentCompany implements Serializable {

	/** 代理商标识 */
	private Long id = null;
	/** 代理商名称 */
	private String name = null;
	/** 联系人 */
	private String contacts = null;
	/** 代理商电话 */
	private String telephone = null;
	/** 媒体位数量限制 */
	private Integer positionLimit = null;
	/** 代理类型 1：公司  2：个人 */
	private Short proxyType = null;
	/** 代理商状态(1:启用 2:禁用;) */
	private Short status = null;
	/** 身份证正面链接 */
	private String identityBverse = null;
	/** 身份证反面链接 */
	private String identityReverse = null;
	/** 营业执照链接 */
	private String businessLicense = null;
	/** logo(OEM使用) */
	private String logo = null;
	/** 域名(OEM使用) */
	private String domain = null;
	/** OEM平台标题 */
	private String websiteTitle = null;
	/** 行业类别标识 */
	private Long categoryId = null;
	/** 行业类别名称 */
	private String categoryName = null;
	/** 创建者标识 */
	private Long creatorId = null;
	/** 菜单描述 */
	private String description = null;
	/** 创建时间 */
	private Timestamp createdTime = null;
	/** 修改时间 */
	private Timestamp modifiedTime = null;
	/** 代理商地址省份 */
	private String provinceName = null;
	/** 代理商地址城市 */
	private String cityName = null;
	/** 代理商省份标识 */
	private Long provinceId = null;
	/** 代理商城市标识 */
	private Long cityId = null;

}
