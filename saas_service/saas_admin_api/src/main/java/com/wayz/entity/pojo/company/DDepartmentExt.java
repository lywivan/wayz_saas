package com.wayz.entity.pojo.company;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 部门创建类
 * 
 * @author wade.liu@wayz.ai
 *
 */
public class DDepartmentExt extends DDepartment implements Serializable {

	/** 名称 */
	private String name = null;
	/** 父标识 */
	private Long parentId = null;
	/** 节点路径 */
	private String path = null;
	/** 创建时间 */
	private Timestamp createdTime = null;

	/**
	 * 获取名称
	 * 
	 * @return 名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置名称
	 * 
	 * @param name 名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 获取父标识
	 * 
	 * @return 父标识
	 */
	public Long getParentId() {
		return parentId;
	}

	/**
	 * 设置父标识
	 * 
	 * @param parentId 父标识
	 */
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	/**
	 * 获取创建时间
	 * 
	 * @return 创建时间
	 */
	public Timestamp getCreatedTime() {
		return createdTime;
	}

	/**
	 * 设置创建时间
	 * 
	 * @param createdTime 创建时间
	 */
	public void setCreatedTime(Timestamp createdTime) {
		this.createdTime = createdTime;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

}
