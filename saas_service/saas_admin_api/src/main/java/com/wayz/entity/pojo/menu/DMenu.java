package com.wayz.entity.pojo.menu;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 *
 * @Description TODO
 *
 * @author wade.liu@wayz.ai
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DMenu {

	/** 菜单标识 */
	private Long id = null;
	/** 父菜单标识 */
	private Long parentId = null;
	/** 菜单类型：0：菜单  1：权限*/
	private Short type;
	/** 菜单状态(0:隐藏;1:显示) */
	private Short status = null;
	/** 菜单名称 */
	private String name = null;
	/** 菜单图标 */
	private String icon = null;
	/** 菜单链接 */
	private String href = null;
	/** 排序序号 */
	private Integer sortNo = null;
	/** 菜单描述 */
	private String description = null;
	/** 是否是叶子节点 */
	private Boolean isLeaf = null;

	private List<DMenu> childList;

}
