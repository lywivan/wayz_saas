package com.wayz.entity.pojo.company;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @className: DCompanyExt
 * @description: 公司详情 <功能详细描述>
 * @author wade.liu@wayz.ai
 * @date: 2019/7/1
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DCompanyExt extends DCompany {

	/** 公司标识 */
	private Long id = null;
	/** 公司主账户Id */
	private Long userId;
	/** 公司名称 */
	private String name = null;
	/** 用户名 */
	private String userName;
	/** 公司状态(1:启用 2:禁用 3:试用中 4:已过期) */
	private Short status = null;
	/** 联系人 */
	private String contacts = null;
	/** 手机号 */
	private String telephone = null;
	/** 联系邮箱 */
	private String email = null;
	/** 通信地址 */
	private String address = null;
	/** 版本标识 */
	private Long versionId = null;
	/** 版本名称 */
	private String versionName = null;
	/** 是否对接dsp广告 */
	private Boolean accessDsp = null;
	/** 试用账户有效期至 */
	private Date validUntilDate = null;
	/** 试用账户加水印 */
	private Boolean isWatermark = null;
	/** 是否需要数据权限功能 */
	private Boolean requireDataAuth = null;
	/** 是否需要广告审核功能 */
	private Boolean requirePlanAudit = null;
	/** 用户数限制 */
	private Integer userLimit = null;
	/** 每年广告投放数量 */
	private Integer planLimit = null;
	/** 素材空间限制(Byte) */
	private Long materialLimit = null;
	/** 公司来源 1：管理员 2：用户自主 */
	private Short sourceType = null;
	/** 是否一键登录 */
	private Boolean isLogin;
	/** 代理商ID */
	private Long agentId;
	/** 代理商名称 */
	private String agentName = null;
	/** 账户余额 */
	private Double balance = null;
	/** 创建者标识 */
	private Long creatorId = null;

}
