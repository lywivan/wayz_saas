package com.wayz.entity.vo.home;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 新闻类
 * 
 * @author mike.ma@wayz.ai
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ArticleVo implements Serializable {

	/** 标识 */
	private Long id = null;
	/** 分类报道 */
	private Short categoryId = null;
	/** 分类报道名称 */
	private String categoryName = null;
	/** 标题 */
	private String title = null;
	/** 标题图片 */
	private String titlePic = null;
	/** 描述 */
	private String description = null;
	/** 关键词 */
	private String keyWords = null;
	/** 摘要 */
	private String abstractContent = null;
	/** 新闻详情 */
	private String content = null;
	/** 发布时间 */
	private String pushTime = null;
	/** 是否发布(0=否,1=是) */
	private Boolean isPush = null;
	/** 排序 */
	private Integer sort = null;
	/** 创建时间 */
	private String createdTime = null;
	/** 修改 */
	private String modifiedTime = null;

}
