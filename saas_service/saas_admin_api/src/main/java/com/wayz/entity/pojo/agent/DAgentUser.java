package com.wayz.entity.pojo.agent;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.*;
import java.sql.*;

/**
 * 代理商用户类
 * 
 * @author mike.ma@wayz.ai
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DAgentUser implements Serializable {

	/** 用户标识 */
	private Long id = null;
	/** 角色标识 */
	private Long roleId = null;
	/** 公司标识 */
	private Long agentId = null;
	/** 创建人标识 */
	private Long creatorId = null;
	/** 用户名称 */
	private String username = null;
	/** 用户密码 */
	private String password = null;
	/** 用户姓名 */
	private String name = null;
	/** 用户电话 */
	private String phone = null;
	/** 创建时间 */
	private Timestamp createdTime = null;
	/** 修改时间 */
	private Timestamp modifiedTime = null;

}
