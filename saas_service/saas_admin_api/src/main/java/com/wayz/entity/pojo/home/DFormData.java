package com.wayz.entity.pojo.home;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

/**
 * 表单数据类
 *
 * @author mike.ma@wayz.ai
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DFormData {

	/** 标识 */
	private Long id = null;
	/** 联系人 */
	private String contact = null;
	/** 联系电话 */
	private String telephone = null;
	/** 公司名称 */
	private String companyName = null;
	/** 省份名称 */
	private String provinceName = null;
	/** 城市名称 */
	private String cityName = null;
	/** 地址 */
	private String address = null;
	/** 是否删除 */
	private Boolean isDeleted = null;
	/** 操作员标识 */
	private Long operatorId = null;
	/** 类型 */
	private Short type =null;
	/** 创建时间 */
	private Timestamp createdTime = null;
	/** 修改时间 */
	private Timestamp modifiedTime = null;

}
