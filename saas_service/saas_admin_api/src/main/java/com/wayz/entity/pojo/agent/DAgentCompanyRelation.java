package com.wayz.entity.pojo.agent;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.*;

/**
 * 代理商关联类
 * 
 * @author mike.ma@wayz.ai
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DAgentCompanyRelation implements Serializable {

	/** 公司Id */
	private Long companyId = null;
	/** 代理商Id */
	private Long agentId = null;
	/** 是否一键登录 */
	private Boolean isLogin = null;

}
