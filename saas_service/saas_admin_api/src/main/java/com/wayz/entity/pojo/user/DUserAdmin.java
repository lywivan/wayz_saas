package com.wayz.entity.pojo.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

/**
 *
 * @Description TODO
 *
 * @author wade.liu@wayz.ai
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DUserAdmin {

	/** 用户标识 */
	private Long id = null;
	/** 公司标识 */
	private Long companyId = null;
	/** 角色标识 */
	private Long roleId = null;
	/**用户类型：0：saas主账户 1：用户自建附属账户*/
	private Short userType;
	/** 用户状态(0:禁用;1:启用) */
	private Short status = null;
	/** 是否删除 */
	private Boolean isDeleted = null;
	/** 用户名称 */
	private String username = null;
	/** 用户密码 */
	private String password = null;
	/** 用户姓名 */
	private String name = null;
	/** 用户性别(0:保密;1:先生;2:女士) */
	private Short gender = null;
	/** 用户电话 */
	private String phone = null;
	/** 用户职务 */
	private String title = null;
	/** 用户邮箱 */
	private String email = null;
	/** 用户头像 */
	private String avatar = null;
	/** 注册时间 */
	private Timestamp registerTime = null;
	/** 注册地址 */
	private String registerIp = null;
	/** 最后登录时间 */
	private Timestamp lastLoginTime = null;
	/** 最后登录地址 */
	private String lastLoginIp = null;
	/** 用户描述 */
	private String description = null;
	/** 创建时间 */
	private Timestamp createdTime = null;
	/** 修改时间 */
	private Timestamp modifiedTime = null;
	/** 附加信息 */
	private String additional = null;
	private Long creatorId = null;

}
