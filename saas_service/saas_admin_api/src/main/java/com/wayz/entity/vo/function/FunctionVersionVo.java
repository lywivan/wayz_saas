package com.wayz.entity.vo.function;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @Description TODO
 *
 * @author wade.liu@wayz.ai
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FunctionVersionVo implements Serializable {

	/** 版本标识 */
	private Long id = null;
	/** 版本名称 */
	private String name = null;
	/** 描述 */
	private String description = null;
	/** 用户数限制 */
	private Integer userLimit = null;
	/** 素材空间限制 */
	private Long materialLimit = null;
	/** 单个视频大小 */
	private Long singleFileLimit = null;
	/** 创建时间 */
	private String createdTime = null;
	/** 修改时间 */
	private String modifiedTime = null;

}
