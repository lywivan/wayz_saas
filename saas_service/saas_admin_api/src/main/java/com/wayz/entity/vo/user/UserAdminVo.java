package com.wayz.entity.vo.user;

import java.io.Serializable;
import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @Description
 *
 * @author wade.liu@wayz.ai
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserAdminVo implements Serializable {

	/** 用户标识 */
	private Long id = null;
	/** 公司标识 */
	private Long companyId = null;
	/** 公司名称 */
	private String companyName = null;
	/** 角色标识 */
	private Long roleId = null;
	/** 角色类型 */
	private Short roleType = null;
	/** 角色名称 */
	private String roleName = null;
	/** 用户名称 */
	private String username = null;
	/** 用户状态(0:禁用;1:启用) */
	private Short status = null;
	/** 用户姓名 */
	private String name = null;
	/** 用户性别(0:保密;1:先生;2:女士) */
	private Short gender = null;
	/** 用户电话 */
	private String phone = null;
	/** 用户职务 */
	private String title = null;
	/** 用户邮箱 */
	private String email = null;
	/** 用户头像 */
	private String avatar = null;
	/** 最后登录时间 */
	private String lastLoginTime = null;
	/** 最后登录地址 */
	private String createdTime = null;

}
