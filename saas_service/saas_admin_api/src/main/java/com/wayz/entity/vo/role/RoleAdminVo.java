package com.wayz.entity.vo.role;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 角色类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleAdminVo implements Serializable {

	/** 用户标识 */
	private Long id = null;
	/** 角色类型（1:平台; 2:媒体主) */
	private String type = null;
	/** 角色状态(0:禁用;1:启用) */
	private String status = null;
	/** 角色名称 */
	private String name = null;
	/** 角色描述 */
	private String description = null;
	/** 创建时间(YYYY-MM-DD HH:MM:SS) */
	private String createTime = null;


}
