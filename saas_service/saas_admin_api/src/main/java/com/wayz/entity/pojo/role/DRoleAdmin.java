package com.wayz.entity.pojo.role;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

/**
 *
 * @Description TODO
 *
 * @author wade.liu@wayz.ai
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DRoleAdmin {

	/** 角色标识 */
	private Long id = null;
	/** 是否删除 */
	private Boolean isDeleted = null;
	/** 角色名称 */
	private String name = null;
	/** 角色描述 */
	private String description = null;
	/** 创建时间 */
	private Timestamp createdTime = null;
	/** 修改时间 */
	private Timestamp modifiedTime = null;
	/**状态  0：禁用  1 启用*/
	private Short status;
}
