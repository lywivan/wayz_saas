package com.wayz.entity.vo.cibn;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * CIBN返回类
 *
 * @author mike.ma@wayz.ai
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CompanyCibnVo implements Serializable {

    /** 公司标识 */
    private Long companyId = null;
    /** 公司名称 */
    private String companyName = null;
    /** 公司备案编号 */
    private String recordNumber = null;
    /** 备案设备数 */
    private Integer equipmentNumber = null;
    /** 行业标识 */
    private Long industryId = null;
    /** 行业名称 */
    private String industryName = null;
    /** 创建人标识 */
    private Long creatorId = null;
    /** 身份证正面链接 */
    private String identityBverse = null;
    /** 身份证反面链接 */
    private String identityReverse = null;
    /** 营业执照链接 */
    private String businessLicense = null;
    /** 审核人标识 */
    private Long auditorId = null;
    /** 有效期至 */
    private String validUntilDate = null;
    /** 审核状态(0:审核中; 1:已经通过; 2: 已驳回) */
    private Short auditStatus = null;
    /** 审核时间 */
    private String auditTime = null;
    /** 审核备注 */
    private String remark = null;
    /** 渠道名称 */
    private String channel = null;
    /** 原标识 */
    private Long originId = null;
    /** 创建时间 */
    private String createdTime = null;
    /** 修改时间 */
    private String modifiedTime = null;

}
