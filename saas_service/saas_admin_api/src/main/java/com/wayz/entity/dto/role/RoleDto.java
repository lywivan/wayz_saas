package com.wayz.entity.dto.role;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @className: RoleDto
 * @description: 角色类
 * @author wade.liu@wayz.ai
 * @date: 2019/7/9
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleDto implements Serializable {

	private Long id;
	/** 角色名称 */
	private String name;
	/** 角色备注 */
	private String description;
}
