package com.wayz.entity.vo.home;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 城市类
 * 
 * @author mike.ma@wayz.ai
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CityVo implements Serializable {

	/** 城市标识 */
	private Long id = null;
	/** 城市名称 */
	private String name = null;

}
