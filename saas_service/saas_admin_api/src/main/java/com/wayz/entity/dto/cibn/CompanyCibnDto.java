package com.wayz.entity.dto.cibn;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *  CIBN参数类
 *
 * @author mike.ma@wayz.ai
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CompanyCibnDto implements Serializable {

    /** 公司ID */
    private Long companyId = null;
    /** 公司名称 */
    private String companyName = null;
    /** 行业标识 */
    private Long industryId = null;
    /** 创建人标识 */
    private Long creatorId = null;
    /** 身份证正面链接 */
    private String identityBverse = null;
    /** 身份证反面链接 */
    private String identityReverse = null;
    /** 营业执照链接 */
    private String businessLicense = null;
    /** 审核人标识 */
    private Long auditorId = null;
    /** 审核状态(0:审核中; 1:已经通过; 2: 已驳回) */
    private Short auditStatus = null;
    /** 审核备注 */
    private String remark = null;

}
