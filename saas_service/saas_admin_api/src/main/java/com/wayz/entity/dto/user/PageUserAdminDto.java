package com.wayz.entity.dto.user;

import com.wayz.constants.PageDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @className: UserCreateVo
 * @description: 创建用户实体类
 *
 * @author wade.liu@wayz.ai
 * @date: 2019/6/28
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class PageUserAdminDto extends PageDto implements Serializable {

    /** 用户状态 */
    private Short status;
    /** 用户姓名 */
    private String name = null;

}
