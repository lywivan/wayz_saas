package com.wayz.entity.pojo.position;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import lombok.Data;

/**
 * 广告位类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Data
public class DAdvPosition implements Serializable {

	/** 广告位标识 */
	private Long id = null;
	/** 广告位名称 */
	private String name = null;
	/** 站点标识 */
	private Long groupId = null;
	/** 公司标识 */
	private Long companyId = null;
	/** 是否删除 */
	private Boolean isDeleted = null;
	/** 服务有效期至 */
	private Date validUntilDate = null;
	/** 创建时间 */
	private Timestamp createdTime = null;

}
