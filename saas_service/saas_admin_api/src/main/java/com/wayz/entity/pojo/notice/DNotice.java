package com.wayz.entity.pojo.notice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author mike.ma@wayz.ai
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DNotice implements Serializable {

    /**
     * 主键ID
     */
    private Long id = null;
    /**
     * 操作人ID
     */
    private Long operationId = null;
    /**
     * 消息类型1.系统通知
     */
    private Integer type = null;
    /**
     * 消息状态：1.创建中 2.已推送
     */
    private Short status = null;
    /**
     * 消息标题
     */
    private String title = null;
    /**
     * 消息内容
     */
    private String content = null;
    /**
     * 创建时间
     */
    private Date createdTime = null;
    /**
     * 推送时间
     */
    private Date pushTime = null;
    /**
     * 状态名称
     */
    private String statusName = null;

}
