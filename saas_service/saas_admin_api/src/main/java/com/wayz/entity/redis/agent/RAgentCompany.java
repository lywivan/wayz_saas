package com.wayz.entity.redis.agent;

import java.text.MessageFormat;

/**
 * 代理商缓存类
 *
 * @author mike.ma@wayz.ai
 */
public class RAgentCompany {

    /** 键值格式 */
    private static final MessageFormat KEY_FORMAT = new MessageFormat("platform:Object:Admin:AgentCompany:{0}");

    /** 代理商标识 */
    private Long agentId = null;
    /** 代理商名称 */
    private String name = null;
    /** 联系人 */
    private String contacts = null;
    /** 代理商电话 */
    private String telephone = null;
    /** 媒体位数量限制 */
    private Integer positionLimit = null;
    /** 剩余媒体位数量限制 */
    private Integer surplusPositionLimit = null;
    /** 代理类型 1：公司  2：个人 */
    private Short proxyType = null;
    /** 代理商状态(1:启用 2:禁用;) */
    private Short status = null;
    /** 身份证正面链接 */
    private String identityBverse = null;
    /** 身份证反面链接 */
    private String identityReverse = null;
    /** 营业执照链接 */
    private String businessLicense = null;
    /** 行业标识 */
    private Long categoryId = null;
    /** 市区标识 */
    private Long cityId = null;
    /** 创建者标识 */
    private Long creatorId = null;
    /** logo(OEM使用) */
    private String logo = null;
    /** 域名(OEM使用) */
    private String domain = null;
    /** 平台名称 */
    private String websiteTitle;


    /**
     * 获取key值方法
     * @param id
     * @return
     */
    public static String getKey(Long id){
        return KEY_FORMAT.format(new Long[] { id });
    }

    /** 常量相关 */
    /** 代理商标识 */
    public static final String AGENTID = "agentId";
    /** 代理商名称 */
    public static final String NAME = "name";
    /** 联系人 */
    public static final String CONTACTS = "contacts";
    /** 代理商电话 */
    public static final String TELEPHONE = "telephone";
    /** 媒体位数量限制 */
    public static final String POSITIONLIMIT = "positionLimit";
    /** 剩余媒体位数量限制 */
    public static final String SURPLUSPOSITIONLIMIT = "surplusPositionLimit";
    /** 代理类型 1：公司  2：个人 */
    public static final String PROXYTYPE = "proxyType";
    /** 代理商状态(1:启用 2:禁用;) */
    public static final String STATUS = "status";
    /** 身份证正面链接 */
    public static final String IDENTITYBVERSE = "identityBverse";
    /** 身份证反面链接 */
    public static final String IDENTITYREVERSE = "identityReverse";
    /** 营业执照链接 */
    public static final String BUSINESSLICENSE = "businessLicense";
    /** 行业标识 */
    public static final String CATEGORYID = "categoryId";
    /** 市区标识 */
    public static final String CITYID = "cityId";
    /** 创建者标识 */
    public static final String CREATORID = "creatorId";
    /** logo(OEM使用) */
    public static final String LOGO = "logo";
    /** 域名(OEM使用) */
    public static final String DOMAIN = "domain";
    /** 平台名称 */
    public static final String WEBSITETITLE = "websiteTitle";

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Integer getPositionLimit() {
        return positionLimit;
    }

    public void setPositionLimit(Integer positionLimit) {
        this.positionLimit = positionLimit;
    }

    public Integer getSurplusPositionLimit() {
        return surplusPositionLimit;
    }

    public void setSurplusPositionLimit(Integer surplusPositionLimit) {
        this.surplusPositionLimit = surplusPositionLimit;
    }

    public Short getProxyType() {
        return proxyType;
    }

    public void setProxyType(Short proxyType) {
        this.proxyType = proxyType;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public String getIdentityBverse() {
        return identityBverse;
    }

    public void setIdentityBverse(String identityBverse) {
        this.identityBverse = identityBverse;
    }

    public String getIdentityReverse() {
        return identityReverse;
    }

    public void setIdentityReverse(String identityReverse) {
        this.identityReverse = identityReverse;
    }

    public String getBusinessLicense() {
        return businessLicense;
    }

    public void setBusinessLicense(String businessLicense) {
        this.businessLicense = businessLicense;
    }

    public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getWebsiteTitle() {
        return websiteTitle;
    }

    public void setWebsiteTitle(String websiteTitle) {
        this.websiteTitle = websiteTitle;
    }

	public Long getCityId() {
		return cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}
}
