package com.wayz.entity.redis.agent;

import java.text.MessageFormat;

/**
 * 代理商用户缓存类
 *
 * @author mike.ma@wayz.ai
 */
public class RAgentUser {

    /** 键值格式 */
    private static final MessageFormat KEY_FORMAT = new MessageFormat("platform:Object:Admin:Agent:{0}");
    /** 用户标识 */
    private Long userId = null;
    /** 角色标识 */
    private Long roleId = null;
    /** 公司标识 */
    private Long agentId = null;
    /** 创建人标识 */
    private Long creatorId = null;
    /** 用户名称 */
    private String username = null;
    /** 用户密码 */
    private String password = null;
    /** 用户姓名 */
    private String name = null;
    /** 用户电话 */
    private String phone = null;

    /**
     * 获取key值方法
     * @param id
     * @return
     */
    public static String getKey(Long id){
        return KEY_FORMAT.format(new Long[] { id });
    }


    /** 常量相关 */
    public static final String USERID = "userId";
    public static final String ROLEID = "roleId";
    public static final String AGENTID = "agentId";
    public static final String CREATORID = "creatorId";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String NAME = "name";
    public static final String PHONE = "phone";


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
