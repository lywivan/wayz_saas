package com.wayz.entity.redis.playVersion;

import java.text.MessageFormat;

/**
 * 播放器版本类
 * 
 * @author wade.liu@wayz.ai
 *
 */
public class RPlayerVersion {

	/** 键值格式 */
	private static final MessageFormat KEY_FORMAT = new MessageFormat("platform:Object:PlayerVersion:{0}");

	/** 属性相关 */
	/** 版本 */
	private String version = null;
	/** 下载地址 */
	private String packageUrl = null;
	/** md5验证码 */
	private String fileMd5 = null;
	/** 是否为通用版 */
	private Boolean isGeneral = null;
	/** 公司标识 */
	private Long companyId = null;
	/** 版本描述 */
	private String description = null;
	/** 创建时间 */
	private String createdTime = null;

	/**
	 * 获取key值方法
	 * 
	 * @param id
	 * @return
	 */
	public static String getKey(String id) {
		return KEY_FORMAT.format(new String[] { id });
	}

	/**
	 * 获取版本
	 * 
	 * @return 版本
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * 设置版本
	 * 
	 * @param version 版本
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * 获取下载地址
	 * 
	 * @return 下载地址
	 */
	public String getPackageUrl() {
		return packageUrl;
	}

	/**
	 * 设置下载地址
	 * 
	 * @param packageUrl 下载地址
	 */
	public void setPackageUrl(String packageUrl) {
		this.packageUrl = packageUrl;
	}

	/**
	 * 获取md5验证码
	 * 
	 * @return md5验证码
	 */
	public String getFileMd5() {
		return fileMd5;
	}

	/**
	 * 设置md5验证码
	 * 
	 * @param fileMd5 md5验证码
	 */
	public void setFileMd5(String fileMd5) {
		this.fileMd5 = fileMd5;
	}

	/**
	 * 获取版本描述
	 * 
	 * @return 版本描述
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 设置版本描述
	 * 
	 * @param description 版本描述
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 获取是否为通用版
	 * 
	 * @return 是否为通用版
	 */
	public Boolean getIsGeneral() {
		return isGeneral;
	}

	/**
	 * 设置是否为通用版
	 * 
	 * @param isGeneral 是否为通用版
	 */
	public void setIsGeneral(Boolean isGeneral) {
		this.isGeneral = isGeneral;
	}

	/**
	 * 获取公司标识
	 * 
	 * @return 公司标识
	 */
	public Long getCompanyId() {
		return companyId;
	}

	/**
	 * 设置公司标识
	 * 
	 * @param companyId 公司标识
	 */
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	/**
	 * 获取创建时间
	 * 
	 * @return 创建时间
	 */
	public String getCreatedTime() {
		return createdTime;
	}

	/**
	 * 设置创建时间
	 * 
	 * @param createdTime 创建时间
	 */
	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}

}
