package com.wayz.entity.pojo.playVersion;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @className: PlayVersionPO
 * @description: 播控版本 <功能详细描述>
 * @author wade.liu@wayz.ai
 * @date: 2019/7/9
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DPlayVersion {

	/** 版本号 */
	private String version;
	/** 下载地址 */
	private String packageUrl;
	/** md5验证码 */
	private String fileMd5;
	/** 版本描述 */
	private String description;
	/** 是否为通用版 */
	private Boolean isGeneral = null;
	/** 公司标识 */
	private Long companyId = null;
	/** 创建时间 */
	private Date createdTime;

}
