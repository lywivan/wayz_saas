package com.wayz.entity.dto.user;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @className: UserCreateVo
 * @description: 创建用户实体类
 *
 * @author wade.liu@wayz.ai
 * @date: 2019/6/28
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class UserAdminDto implements Serializable {
    /** 用户标识 */
    private Long id = null;
    /** 角色标识 */
    private Long roleId = null;
    /** 用户名称 */
    private String username = null;
    /** 用户姓名 */
    private String name = null;
    /** 用户电话 */
    private String phone = null;
    /** 用户邮箱 */
    private String email = null;
    /**密码*/
    private String password;
    private String oldPassword;
    private String newPassword;
    private Short status;

}
