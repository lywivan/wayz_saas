package com.wayz.entity.vo.agent;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 代理商类
 *
 * @author mike.ma@wayz.ai
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AgentVO implements Serializable {

    /** 代理商标识 */
    private Long id = null;
    /** 代理商名称 */
    private String name = null;
    /** 联系人 */
    private String contacts = null;
    /** 代理商电话 */
    private String telephone = null;
    /** 媒体位数量限制 */
    private Integer positionLimit = null;
    /** 代理类型 1：公司  2：个人 */
    private Short proxyType = null;
    /** 代理商状态(1:启用 2:禁用;) */
    private Short status = null;
    /** 身份证正面链接 */
    private String identityBverse = null;
    /** 身份证反面链接 */
    private String identityReverse = null;
    /** 营业执照链接 */
    private String businessLicense = null;
    /** 分类标识 */
    private Long categoryId = null;
    /** 创建者标识 */
    private Long creatorId = null;
    /** 菜单描述 */
    private String description = null;
	/** 代理商省份标识 */
	private Long provinceId = null;
	/** 代理商城市标识 */
	private Long cityId = null;

}
