package com.wayz.entity.pojo.home;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 城市类
 * 
 * @author mike.ma@wayz.ai
 * 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DCity {

	/** 城市标识 */
	private Long id = null;
	/** 城市名称 */
	private String name = null;
	/** 省份标识 */
	private Long provinceId = null;
	/** 省份名称 */
	private String provinceName = null;

}
