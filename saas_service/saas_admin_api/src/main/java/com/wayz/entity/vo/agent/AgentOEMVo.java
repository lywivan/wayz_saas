package com.wayz.entity.vo.agent;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 代理商OEM类
 *
 * @author mike.ma@wayz.ai
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AgentOEMVo implements Serializable {

    /** 公司编号*/
    private Long id;
    /** logo(OEM使用) */
    private String logo = null;
    /** 域名(OEM使用) */
    private String domain = null;
    /**平台名称*/
    private String websiteTitle;

}
