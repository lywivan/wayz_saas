package com.wayz.entity.dto.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @description: 登陆DTO
 * @author wade.liu@wayz.ai
 * @date: 2019/6/28
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class LoginDto implements Serializable {

    private String sessionId = null;
    /** 用户名 */
    private String username = null;
    /** 用户密码 */
    private String password = null;
    /** 验证码 */
    private String authCode = null;
    /**IP地址*/
    private String ipAddress;

}
