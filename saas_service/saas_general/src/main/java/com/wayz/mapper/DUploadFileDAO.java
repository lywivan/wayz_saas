package com.wayz.mapper;

import com.wayz.entity.model.DUploadFile;
import com.wayz.entity.model.DUploadFileCreate;
import com.wayz.entity.model.DUploadFileModify;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.*;

/**
 * 文件上传记录DAO接口
 * 
 * @author yinjy
 *
 */
@Repository("dUploadFileDAO")
public interface DUploadFileDAO {

	/**
	 * 获取文件上传记录
	 * 
	 * @param id 文件标识
	 * @return 文件上传记录
	 */
	public DUploadFile get(@Param("id") Long id);

	/**
	 * 创建文件上传记录
	 * 
	 * @param id 文件标识
	 * @param create 文件上传记录创建
	 * @return 创建行数
	 */
	public Integer create(@Param("id") Long id, @Param("create") DUploadFileCreate create);

	/**
	 * 修改文件上传记录
	 * 
	 * @param modify 文件上传记录修改
	 * @return 修改行数
	 */
	public Integer modify(@Param("modify") DUploadFileModify modify);

}
