package com.wayz.service;

import com.wayz.entity.pojo.DCategory;
import com.wayz.entity.vo.CategoryVo;
import com.wayz.exception.WayzException;
import com.wayz.mapper.DCategoryDAO;
import com.wayz.redis.BeanIdKeyEnum;
import com.wayz.redis.RedisUtils;
import com.wayz.service.CategoryService;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * 行业服务实现类
 * 
 * @author mike.ma@wayz.ai
 *
 */
@Service("categoryService")
@org.apache.dubbo.config.annotation.Service(version = "1.0.0", protocol = "dubbo")
@RefreshScope
public class CategoryServiceImpl implements CategoryService {

	/**
	 * Redis工具类
	 */
	@Resource(name = "redisUtils")
	private RedisUtils redisUtils;

	/** DAO相关 */
	/** 行业分类 DAO */
	@Resource(name = "dCategoryDAO")
	private DCategoryDAO dCategoryDAO = null;


	/**
	 * 查询行业类别
	 * 
	 * @param id 类别标识
	 * @return 行业类别列表
	 * @throws WayzException 平台异常
	 */
	@Override
	public List<CategoryVo> queryCategory(Long myId, Long companyId, Long id) throws WayzException {

		// 初始化
		List<CategoryVo> categoryList = new ArrayList<CategoryVo>();

		// 获取数据
		List<DCategory> dCategoryList = dCategoryDAO.query(id, companyId);
		if (dCategoryList != null) {
			for (DCategory dCategory : dCategoryList) {
				// 初始化
				CategoryVo category = new CategoryVo();

				// 设置参数
				category.setId(dCategory.getId());
				category.setName(dCategory.getName());
				category.setDescription(dCategory.getDescription());

				// 添加
				categoryList.add(category);
			}
		}

		// 返回数据
		return categoryList;
	}

	/**
	 * 创建行业类别
	 * 
	 * @param myId 我的客户标识
	 * @param name 行业类别名称
	 * @param description 行业类别描述
	 * @throws WayzException 平台异常
	 */
	@Override
	public Long createCategory(Long myId, Long companyId, String name, String description) throws WayzException {
		// 校验行业类别名称是否存在
		DCategory dCategory = dCategoryDAO.getByName(name, companyId);
		if (dCategory != null) {
			throw new WayzException(MessageFormat.format("行业类别名称已存在，行业类别名称：{0}", name));
		}

		// 创建行业类别
		Long id = redisUtils.incrBy(BeanIdKeyEnum.CATEGORY.getKey(), RedisUtils.STEP_SIZE);
		DCategory create = new DCategory();
		create.setName(name);
		create.setDescription(description);
		create.setCompanyId(companyId);
		dCategoryDAO.create(id, create);

		// 返回标识
		return id;
	}

	/**
	 * 修改行业类别
	 * 
	 * @param myId 我的客户标识
	 * @param id 行业类别标识
	 * @param name 行业类别名称
	 * @param description 行业类别描述
	 * @throws WayzException 平台异常
	 */
	@Override
	public void modifyCategory(Long myId, Long companyId, Long id, String name, String description)
			throws WayzException {
		// 校验行业类别标识是否为空
		if (id == null) {
			throw new WayzException("行业类别标识为空！");
		}

		// 校验行业类别名称是否为空
		if (name == null || name.equals("")) {
			throw new WayzException("行业类别名称不能为空！");
		}

		// 校验行业类别是否存在
		DCategory dCategory = dCategoryDAO.get(id);
		if (dCategory == null) {
			throw new WayzException(MessageFormat.format("行业类别不存在，行业类别标识：{0}", id));
		}

		// 修改行业类别信息
		DCategory modify = new DCategory();
		modify.setId(id);
		modify.setName(name);
		modify.setDescription(description);
		dCategoryDAO.modify(modify);
	}

	/**
	 * 删除行业类别
	 * 
	 * @param myId 我的客户标识
	 * @param id 行业类别标识
	 * @throws WayzException 平台异常
	 */
	@Override
	public void deleteCategory(Long myId, Long id) throws WayzException {
		// 校验行业类别标识是否为空
		if (id == null) {
			throw new WayzException("行业类别标识为空！");
		}

		// 校验行业类别是否存在
		DCategory dCategory = dCategoryDAO.get(id);
		if (dCategory == null) {
			throw new WayzException(MessageFormat.format("行业类别不存在，行业类别标识：{0}", id));
		}

		// 设置行业类别为删除状态
		DCategory  modify = new DCategory();
		modify.setId(id);
		modify.setIsDeleted(true);
		dCategoryDAO.modify(modify);
	}

}
