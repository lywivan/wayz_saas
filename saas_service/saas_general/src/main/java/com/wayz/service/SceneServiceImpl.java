package com.wayz.service;

import com.wayz.constants.PageVo;
import com.wayz.constants.ViewMap;
import com.wayz.entity.pojo.DScene;
import com.wayz.entity.vo.SceneVo;
import com.wayz.exception.ExceptionCode;
import com.wayz.exception.WayzException;
import com.wayz.mapper.DSceneDAO;
import com.wayz.redis.BeanIdKeyEnum;
import com.wayz.redis.RedisUtils;
import com.wayz.service.SceneService;
import com.wayz.util.TimeHelper;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * 场景服务实现类
 * 
 * @author mike.ma@wayz.ai
 *
 */
@Service("sceneService")
@org.apache.dubbo.config.annotation.Service(version = "1.0.0", protocol = "dubbo")
@RefreshScope
public class SceneServiceImpl implements SceneService {

	/**
	 * Redis工具类
	 */
	@Resource(name = "redisUtils")
	private RedisUtils redisUtils;

	/** DAO相关 */
	/** 场景DAO */
	@Resource(name = "dSceneDAO")
	private DSceneDAO dSceneDAO = null;

	/**
	 * 查询场景视图(一级)
	 * 
	 * @param myId 我的客户标识
	 * @return 场景列表
	 * @throws WayzException 平台异常
	 */
	@Override
	public List<ViewMap> querySceneView(Long myId, Long companyId) throws WayzException {

		// 初始化
		List<ViewMap> sceneList = dSceneDAO.querySceneView(companyId);

		// 返回数据
		return sceneList;
	}

	/**
	 * 查询场景
	 * 
	 * @param myId 我的客户标识
	 * @param name 名称(模糊查询)
	 * @return 场景列表
	 * @throws WayzException 平台异常
	 */
	@Override
	public PageVo<SceneVo> queryScene(Long myId, Long companyId, String name, Integer startIndex, Integer pageSize)
			throws WayzException {
		// 初始化
		PageVo<SceneVo> page = new PageVo<>();
		List<SceneVo> sceneList = new ArrayList<SceneVo>();

		// 统计
		Integer totalCount = dSceneDAO.count(name, companyId);
		if (totalCount <= 0) {
			return page;
		}
		// 查询
		List<DScene> dScenes = dSceneDAO.query(name, companyId, startIndex, pageSize);
		if (dScenes != null) {
			for (DScene dSceneExt : dScenes) {
				// 初始化
				SceneVo scene = new SceneVo();

				// 设置参数
				scene.setId(dSceneExt.getId());
				scene.setName(dSceneExt.getName());
				scene.setCompanyId(dSceneExt.getCompanyId());
				scene.setDescription(dSceneExt.getDescription());
				scene.setCreatedTime(TimeHelper.getTimestamp(dSceneExt.getCreatedTime()));
				if (dSceneExt.getModifiedTime() != null) {
					scene.setModifiedTime(TimeHelper.getTimestamp(dSceneExt.getModifiedTime()));
				}
				sceneList.add(scene);
			}
		}
		// 赋值
		page.setTotalCount(totalCount);
		page.setList(sceneList);

		// 返回数据
		return page;
	}

	/**
	 * 获取场景
	 * 
	 * @param myId 我的客户标识
	 * @param id 场景标识
	 * @return 场景
	 * @throws WayzException 平台异常
	 */
	@Override
	public SceneVo getScene(Long myId, Long id) throws WayzException {
		// 获取
		DScene dScene = dSceneDAO.get(id);
		if (dScene == null) {
			throw new WayzException("无此场景!");
		}

		// 初始化
		SceneVo scene = new SceneVo();
		// 设置参数
		scene.setId(dScene.getId());
		scene.setName(dScene.getName());
		scene.setDescription(dScene.getDescription());
		scene.setCreatedTime(TimeHelper.getTimestamp(dScene.getCreatedTime()));
		if (dScene.getModifiedTime() != null) {
			scene.setModifiedTime(TimeHelper.getTimestamp(dScene.getModifiedTime()));
		}

		// 返回数据
		return scene;
	}

	/**
	 * 创建场景
	 * 
	 * @param myId 我的客户标识
	 * @param name 名称
	 * @param description 描述
	 * @throws WayzException 平台异常
	 */
	@Override
	public void createScene(Long myId, Long companyId, String name, String description) throws WayzException {
		// 校验名称唯一
		DScene dScene = dSceneDAO.getByName(name, companyId);
		if (dScene != null) {
			throw new WayzException("场景名称已存在!");
		}

		// 创建数据
		Long id = redisUtils.incrBy(BeanIdKeyEnum.CATEGORY.getKey(), RedisUtils.STEP_SIZE);
		DScene dSceneCreate = new DScene();
		dSceneCreate.setName(name);
		dSceneCreate.setCompanyId(companyId);
		dSceneCreate.setDescription(description);
		dSceneDAO.create(id, dSceneCreate);
	}

	/**
	 * 修改场景
	 * 
	 * @param myId 我的客户标识
	 * @param id 场景标识
	 * @param name 名称
	 * @param description 描述
	 * @throws WayzException 平台异常
	 */
	@Override
	public void modifyScene(Long myId, Long companyId, Long id, String name, String description) throws WayzException {
		// 校验名称唯一
		DScene dScene = dSceneDAO.getByName(name, companyId);
		if (dScene != null && !dScene.getId().equals(id)) {
			throw new WayzException(ExceptionCode.PARAMETER_ERROR, MessageFormat.format("场景名称:{0},已存在!", name));
		}

		// 修改数据
		DScene dSceneModify = new DScene();
		dSceneModify.setId(id);
		dSceneModify.setName(name);
		dSceneModify.setDescription(description);
		dSceneDAO.modify(dSceneModify);
	}

	/**
	 * 删除场景
	 * 
	 * @param myId 我的客户标识
	 * @param id 场景标识
	 * @throws WayzException 平台异常
	 */
	@Override
	public void deleteScene(Long myId, Long id) throws WayzException {
		// 校验存在
		DScene dScene = dSceneDAO.get(id);
		if (dScene == null) {
			throw new WayzException("场景不存在!");
		}

		// 删除
		DScene dSceneModify = new DScene();
		dSceneModify.setId(id);
		dSceneModify.setIsDeleted(true);
		dSceneDAO.modify(dSceneModify);
	}
}
