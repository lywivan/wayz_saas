package com.wayz.redis.impl;

import javax.annotation.*;

import com.wayz.redis.RUploadFileId;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.*;

/**
 * 文件上传记录标识实现类
 * 
 * @author yinjy
 *
 */
@Repository("rUploadFileId")
public class RUploadFileIdImpl implements RUploadFileId {

	/** Redis模板 */
	@Resource(name = "redisTemplate")
	private RedisTemplate<String, String> redisTemplate = null;

	/** 键值格式 */
	private static final String KEY = "platform:Id:UploadFile";

	/**
	 * 递增文件上传记录标识
	 * 
	 * @return 文件上传记录标识
	 */
	@Override
	public Long increment() {
		return redisTemplate.opsForValue().increment(KEY, 1L);
	}

}
