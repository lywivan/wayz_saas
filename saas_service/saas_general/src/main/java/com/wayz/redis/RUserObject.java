package com.wayz.redis;

import com.wayz.entity.redis.RUser;

/**
 * 用户对象接口
 * 
 * @author yinjy
 *
 */
public interface RUserObject {

	/**
	 * 设置用户对象
	 * 
	 * @param id 用户标识
	 * @param user 用户对象
	 */
	public void set(Long id, RUser user);

	/**
	 * 获取用户对象
	 * 
	 * @param id 用户标识
	 * @return 用户对象
	 */
	public RUser get(Long id);

	/**
	 * 删除用户对象
	 * 
	 * @param id 用户标识
	 */
	public void remove(Long id);

	/**
	 * 存在用户对象
	 * 
	 * @param id 用户标识
	 * @return 是否存在
	 */
	public boolean exist(Long id);

	/**
	 * 设置字符串值
	 * 
	 * @param id 用户标识
	 * @param name 参数名称
	 * @param value 字符串值
	 */
	public void setString(Long id, String name, String value);

	/**
	 * 获取字符串值
	 * 
	 * @param id 用户标识
	 * @param name 参数名称
	 * @return 字符串值
	 */
	public String getString(Long id, String name);

	/**
	 * 设置整型值
	 * 
	 * @param id 用户标识
	 * @param name 参数名称
	 * @param value 整型值
	 */
	public void setInteger(Long id, String name, Integer value);

	/**
	 * 获取整型值
	 * 
	 * @param id 用户标识
	 * @param name 参数名称
	 * @return 整型值
	 */
	public Integer getInteger(Long id, String name);

	/**
	 * 设置短整型值
	 * 
	 * @param id 用户标识
	 * @param name 参数名称
	 * @param value 短整型值
	 */
	public void setShort(Long id, String name, Short value);

	/**
	 * 获取短整型值
	 * 
	 * @param id 用户标识
	 * @param name 参数名称
	 * @return 短整型值
	 */
	public Short getShort(Long id, String name);

	/**
	 * 设置长整型值
	 * 
	 * @param id 用户标识
	 * @param name 参数名称
	 * @param value 长整型值
	 */
	public void setLong(Long id, String name, Long value);

	/**
	 * 获取长整型值
	 * 
	 * @param id 用户标识
	 * @param name 参数名称
	 * @return 长整型值
	 */
	public Long getLong(Long id, String name);

	/**
	 * 设置浮点型值
	 * 
	 * @param id 用户标识
	 * @param name 参数名称
	 * @param value 浮点型值
	 */
	public void setFloat(Long id, String name, Float value);

	/**
	 * 获取浮点型值
	 * 
	 * @param id 用户标识
	 * @param name 参数名称
	 * @return 浮点型值
	 */
	public Float getFloat(Long id, String name);

	/**
	 * 设置双精度浮点型值
	 * 
	 * @param id 用户标识
	 * @param name 参数名称
	 * @param value 双精度浮点型值
	 */
	public void setDouble(Long id, String name, Double value);

	/**
	 * 获取双精度浮点型值
	 * 
	 * @param id 用户标识
	 * @param name 参数名称
	 * @return 双精度浮点型值
	 */
	public Double getDouble(Long id, String name);

	/**
	 * 删除参数
	 * 
	 * @param id 用户标识
	 * @param name 参数名称
	 */
	public void remove(Long id, String name);

	/**
	 * 存在参数
	 * 
	 * @param id 用户标识
	 * @param name 参数名称
	 * @return 持久化异常
	 */
	public boolean exist(Long id, String name);

}
