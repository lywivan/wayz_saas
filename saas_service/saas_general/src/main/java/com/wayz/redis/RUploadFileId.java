package com.wayz.redis;

/**
 * 文件上传记录标识接口
 * 
 * @author yinjy
 *
 */
public interface RUploadFileId {

	/**
	 * 递增文件上传记录标识
	 * 
	 * @return 文件上传记录标识
	 */
	public Long increment();

}
