package com.wayz.service;

import com.wayz.constants.PageVo;
import com.wayz.entity.dto.AuditorPositionQueryDto;
import com.wayz.entity.vo.AuditorPositionVO;
import com.wayz.exception.WayzException;

import java.util.List;


/**
 * 审核方设备服务接口
 *
 * @author mike.ma@wayz.ai
 */
public interface AuditorPositionService {

    /**
	 * 查询场景方可选择审核设备
	 * 
	 */
    PageVo<AuditorPositionVO> querySelectablePosition(Long myId, AuditorPositionQueryDto dto) throws WayzException;

	/**
	 * 查询场景方已选择审核设备
	 * 
	 */
	PageVo<AuditorPositionVO> querySelectedPosition(Long myId, AuditorPositionQueryDto dto) throws WayzException;

	/**
	 * 添加场景方审核设备
	 * 
	 */
    void addPosition(Long myId, Long auditorId, List<Long> positionIdList) throws WayzException;

	/**
	 * 删除场景方审核设备
	 * 
	 */
    void deletePosition(Long myId, Long auditorId, List<Long> positionIdList) throws WayzException;

}
