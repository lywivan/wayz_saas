package com.wayz.service;

import com.wayz.constants.PageVo;
import com.wayz.entity.dto.AuditorDto;
import com.wayz.entity.dto.AuditorQueryDto;
import com.wayz.entity.vo.AuditorVO;
import com.wayz.exception.WayzException;

/**
 * 审核方服务接口
 *
 * @author mike.ma@wayz.ai
 */
public interface AuditorService {

    /**
     * 审核方列表
     *
     * @param myId            我的ID
     * @param dto 实体类
     * @return
     */
    PageVo<AuditorVO> queryAuditor(Long myId, AuditorQueryDto dto) throws WayzException;

    /**
     * 获取审核方详情
     *
     * @param myId 我的标识
     * @param id   审核方标识
     * @return
     */
    AuditorVO getAuditor(Long myId, Long id) throws WayzException;

    /**
     * 创建审核方
     *
     * @param myId       我的ID
     * @param dto 实体类
     * @throws WayzException
     */
    void createAuditor(Long myId, AuditorDto dto) throws WayzException;

    /**
     * 修改审核方
     *
     * @param myId       我的ID
     * @param dto 实体类
     * @throws WayzException
     */
    void modifyAuditor(Long myId, AuditorDto dto) throws WayzException;

    /**
     * 修改审核方状态
     *
     * @param myId   我的ID
     * @param id     审核方ID
     * @param status 审核方状态(1:启用 0:禁用;)
     * @return
     */
    Integer modifyAuditorStatus(Long myId, Long id, Short status) throws WayzException;

    /**
     * 删除审核方
     *
     * @param myId 我的ID
     * @throws WayzException
     */
    void deleteAuditor(Long myId, Long id) throws WayzException;

    /**
     * 审核方重置密码
     *
     * @param myId        我的ID
     * @param username    审核方名称
     * @param newPassword 新的密码
     * @throws WayzException
     */
    void resetAuditorPassword(Long myId, String username, String newPassword) throws WayzException;

}
