package com.wayz.service;

import com.wayz.constants.PageVo;
import com.wayz.constants.ViewMap;
import com.wayz.entity.dto.live.LiveTempletDto;
import com.wayz.entity.dto.live.LiveTempletPageDto;
import com.wayz.entity.pojo.live.DLiveChannel;
import com.wayz.entity.vo.live.LiveResolutionVo;
import com.wayz.entity.vo.live.LiveTempletDetailVo;
import com.wayz.entity.vo.live.LiveTempletVo;
import com.wayz.exception.WayzException;

import java.util.List;

/**
 * 直播服务接口
 * 
 */
public interface LiveService {
    /**
     * 创建直播渠道
     *
     * @param myId 我的标识
     * @param name 名称
     * @return 标识
     */
    public Long createLiveChannel(Long myId, String name) throws WayzException;

    /**
     * 修改直播渠道
     *
     * @param myId 我的标识
     * @param id 标识
     * @param name 名称
     * @throws WayzException 平台异常
     */
    public void modifyLiveChannel(Long myId, Long id, String name) throws WayzException;

    /**
     * 删除直播渠道
     *
     * @param myId 我的标识
     * @param id 标识
     * @throws WayzException 平台异常
     */
    public void deleteLiveChannel(Long myId, Long id) throws WayzException;

    /**
     * 查询直播渠道
     *
     * @param myId 我的标识
     * @return 列表
     * @throws WayzException 平台异常
     */
    public List<DLiveChannel> queryLiveChannel(Long myId) throws WayzException;

    /**
     * 查询分辨率
     *
     * @param myId 我的标识
     * @return 分辨率列表
     * @throws WayzException 平台异常
     */
    public List<ViewMap> queryLiveResolutionView(Long myId) throws WayzException;

    /**
     * 获取分辨率
     *
     * @param myId 我的标识
     * @param id 分辨率标识
     * @return 分辨率
     * @throws WayzException 平台异常
     */
    public LiveResolutionVo getLiveResolution(Long myId, Long id) throws WayzException;

    /**
     * 查询直播模板
     *
     * @param myId 我的标识
     * @return 直播模板分页
     * @throws WayzException 平台异常
     */
    public PageVo<LiveTempletVo> queryLivePlanTemplet(Long myId, LiveTempletPageDto liveTempletPageDto);

    /**
     * 查询直播模板视图列表
     *
     * @param myId 我的标识
     * @return 视图列表
     * @throws WayzException 平台异常
     */
    public List<ViewMap> queryLivePlanTempletView(Long myId, Long companyId) throws WayzException;

    /**
     * 创建直播模板
     *
     * @param myId 我的标识
     * @throws WayzException 平台异常
     */
    public void createLivePlanTemplet(Long myId, LiveTempletDto liveTempletDto) throws WayzException;

    /**
     * 修改直播模板
     *
     * @param myId 我的标识
     * @throws WayzException 平台异常
     */
    public void modifyLivePlanTemplet(Long myId, LiveTempletDto liveTempletDto) throws WayzException;

    /**
     * 删除直播模板
     *
     * @param myId 我的标识
     * @param id 直播模板标识
     * @throws WayzException 平台异常
     */
    public void deleteLivePlanTemplet(Long myId, Long id) throws WayzException;

    /**
     * 获取直播模板详情
     *
     * @param myId 我的标识
     * @param templetId 模板标识
     * @return 直播模板详情
     * @throws WayzException 平台异常
     */
    public LiveTempletDetailVo getLivePlanTempletDetail(Long myId, Long templetId) throws WayzException;

}
