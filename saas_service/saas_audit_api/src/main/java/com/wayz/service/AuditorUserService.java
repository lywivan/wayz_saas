package com.wayz.service;

import com.wayz.entity.pojo.auditor.DAuditorUser;
import com.wayz.entity.vo.AuditorUserVO;
import com.wayz.exception.WayzException;

/**
 * 审核方用户服务接口
 *
 * @author mike.ma@wayz.ai
 */
public interface AuditorUserService {

	/**
	 * 创建审核方用户
	 * 
	 * @param myId
	 * @param dto
	 * @return
	 * @throws WayzException
	 */
	public Long createUser(Long myId, DAuditorUser dto) throws WayzException;

	/**
	 * 审核方重置密码
	 * 
	 * @param username 审核方用户名
	 * @param newPassword 新的密码
	 * @throws WayzException
	 */
	void resetUserPassword(String username, String newPassword) throws WayzException;

	/**
	 * 修改密码
	 * 
	 * @param myId 我的ID
	 * @param oldPassword 原始密码
	 * @param newPassword 新的密码
	 */
	void modifyPassword(Long myId, String oldPassword, String newPassword) throws WayzException;

	/**
	 * 获取myId
	 * 
	 * @param token 登录令牌
	 * @return myId
	 * @throws WayzException 平台异常
	 */
	Long getMyId(String token) throws WayzException;

	/**
	 * 获取当前用户公司标志
	 * 
	 * @param myId 我的用户标志
	 * @return 公司标志
	 */
	Long getCompanyId(Long myId);

	/**
	 * 登录
	 * 
	 * @param username
	 * @param password 
	 * @param ipAddress
	 * @return 用户信息
	 * @throws WayzException 平台异常
	 */
	AuditorUserVO login(String username, String password, String ipAddress) throws WayzException;

	/**
	 * 登出
	 * 
	 * @param token 登录令牌
	 * @throws WayzException 平台异常
	 */
	void logout(Long myId, String token) throws WayzException;
}
