package com.wayz.entity.pojo.live;

import java.io.Serializable;
import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 公司素材类
 * 
 *  
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DCompanyMaterial implements Serializable {

	/** 文件标识 */
	private Long id = null;
	/** 公司标识 */
	private Long companyId = null;
	/** 标签ID */
	private Long labelId = null;
	/** 客户标识 */
	private Long customerId = null;
	/** 文件名称 */
	private String name = null;
	/** 素材类型名称(0:新闻; 1:广告素材; 2:报审材料; 3:竞价广告素材) */
	private Short category = null;
	/** 文件类型(1:图片 2:视频) */
	private Short type = null;
	/** 文件链接 */
	private String url = null;
	/** 预览图URL */
	private String previewUrl = null;
	/** 文件检验码MD5 */
	private String filemd5 = null;
	/** 文件长度(字节) */
	private Long fileSize = null;
	/** 文件属性:时长(秒) */
	private Integer duration = null;
	/** 文件属性:宽度 */
	private Integer width = null;
	/** 文件属性:高度 */
	private Integer height = null;
	/** 文件总页数 */
	private Integer pageNum = null;
	/** 是否删除 */
	private Boolean isDeleted = null;
	/** 创建人标识 */
	private Long creatorId = null;
	/** 审核人标识 */
	private Long auditorId = null;
	/** 审核状态(0:未审核; 1:审核通过; 2: 审核未通过) */
	private Short auditStatus = null;
	/** 审核时间 */
	private Timestamp auditTime = null;
	/** 审核备注 */
	private String remark = null;
	/** web内容类型 */
	private String contentType = null;
	/** 创建时间 */
	private Timestamp createdTime = null;
	/** 修改时间 */
	private Timestamp modifiedTime = null;
	/** 素材来源(1:自主; 2:商城购买)  */
	private Short sourceType = null;
	/** 内容商城ID  */
	private Long contentId = null;

}
