package com.wayz.entity.pojo.live;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.security.Timestamp;

/**
 * 海报模板摘要类
 * 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DLiveTemplet implements Serializable {

	/** 模板标识 */
	private Long id = null;
	/** 公司标识 */
	private Long companyId = null;
	/** 分辨率标识 */
	private Long resolutionId = null;
	private String resolutionName = null;
	/** 模板名称 */
	private String name = null;
	/** 预览URL */
	private String previewUrl = null;
	/** 描述 */
	private String description = null;
	/** 模板背景 */
	private String background = null;
	/** 模板背景音乐 */
	private String backgroundAudio = null;
	/** 是否删除 */
	private Boolean isDeleted = null;
	/** 创建时间 */
	private Timestamp createdTime = null;
	private Timestamp modifiedTime = null;

}
