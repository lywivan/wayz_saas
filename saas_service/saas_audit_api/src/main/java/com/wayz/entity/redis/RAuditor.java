package com.wayz.entity.redis;

import java.text.MessageFormat;

/**
 * 
 */
public class RAuditor {

    /** 键值格式 */
    private static final MessageFormat KEY_FORMAT = new MessageFormat("platform:Object:Admin:Auditor:{0}");

    /** 审核方标识 */
    private Long id = null;
    /** 审核方名称 */
    private String name = null;
    /** 联系人 */
    private String contacts = null;
    /** 审核方电话 */
    private String telephone = null;
    /** 审核方类型 1.奥凌;2:CIBN;3.场景方;4.媒体方 */
    private Short type = null;
    /** 审核方状态(1:启用 2:禁用;) */
    private Short status = null;
    /** 创建者标识 */
    private Long creatorId = null;

    /**
     * 获取key值方法
     * @param id
     * @return
     */
    public static String getKey(Long id){
        return KEY_FORMAT.format(new Long[] { id });
    }

    /** 常量相关 */
    /** 审核方标识 */
    public static final String ID = "id";
    /** 审核方名称 */
    public static final String NAME = "name";
    /** 联系人 */
    public static final String CONTACTS = "contacts";
    /** 审核方电话 */
    public static final String TELEPHONE = "telephone";
    /** 审核方类型 1.奥凌;2:CIBN;3.场景方;4.媒体方 */
    public static final String TYPE = "type";
    /** 审核方状态(1:启用 2:禁用;) */
    public static final String STATUS = "status";
    /** 创建者标识 */
    public static final String CREATORID = "creatorId";

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getContacts() {
		return contacts;
	}
	public void setContacts(String contacts) {
		this.contacts = contacts;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public Short getType() {
		return type;
	}
	public void setType(Short type) {
		this.type = type;
	}
	public Short getStatus() {
		return status;
	}
	public void setStatus(Short status) {
		this.status = status;
	}
	public Long getCreatorId() {
		return creatorId;
	}
	public void setCreatorId(Long creatorId) {
		this.creatorId = creatorId;
	}
    
}
