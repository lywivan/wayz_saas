package com.wayz.entity.pojo.auditor;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DAuditor implements Serializable {

    /** 审核方标识 */
    private Long id = null;
    /** 审核方名称 */
    private String name = null;
    /** 联系人 */
    private String contacts = null;
    /** 联系电话 */
    private String telephone = null;
    /** 联系地址 */
    private String address = null;
    /** 审核方类型 1.奥凌;2:CIBN;3.场景方;4.媒体方*/
    private Short type = null;
	/** 审核方类型名称 */
	private String typeName = null;
    /** 审核方状态(1:启用 2:禁用;) */
    private Short status = null;
    /** 审核方描述 */
    private String description = null;
	/** 是否删除 */
	private Boolean isDeleted = null;
	/** 创建人标识 */
	private Long creatorId = null;
	/** 创建时间(YYYY-MM-DD HH:MM:SS) */
	private String createdTime = null;
}
