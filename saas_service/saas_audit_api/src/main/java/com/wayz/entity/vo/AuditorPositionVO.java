package com.wayz.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuditorPositionVO implements Serializable {

	/** 设备标识 */
	private Long positionId = null;
    /** 设备名称 */
    private String positionName = null;
	/** 播控编码 */
	private String playCode = null;
	/** 审核方标识 */
	private Long auditorId = null;
	/** 媒体标识 */
	private Long companyId = null;
    /** 媒体名称 */
    private String companyName = null;
//	/** 区县标识 */
//	private Long districtId = null;
//	/** 城市标识 */
//	private Long cityId = null;
//	/** 省份标识 */
//	private Long provinceId = null;
    /** 设备地址省份 */
	private String provinceName = null;
	/** 设备地址城市 */
	private String cityName = null;
	/** 设备地址区县 */
	private String districtName = null;
	/** 设备地址 */
	private String address = null;
	/** 分辨率标识 */
	private Long resolutionId = null;
	/** 分辨率名称 */
	private String resolutionName = null;
	/** 场景名称 */
	private String sceneName = null;
	/** 场景标识 */
	private Long sceneId = null;
	/** 是否已绑定设备 */
	private Boolean isBound = null;
	/** 是否在线 */
	private Boolean isOnline = null;
	/** 是否启用 */
	private Boolean isEnable = null;
	
}
