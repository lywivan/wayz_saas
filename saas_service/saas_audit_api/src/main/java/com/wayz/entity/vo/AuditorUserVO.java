package com.wayz.entity.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 审核方用户类
 * 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuditorUserVO implements Serializable {

	/** 用户标识 */
	private Long id = null;
	/** 审核方标识 */
	private Long auditorId = null;
	/** 审核方名称 */
	private String auditorName = null;
	/** 审核方类型 */
	private Short auditorType = null;
	/** 审核方类型名称 */
	private String auditorTypeName = null;
	/** 用户名称 */
	private String username = null;
	/** 用户密码 */
	private String password = null;
	/** 用户电话 */
	private String telephone = null;
	/** 用户状态(0:禁用;1:启用) */
	private Short status = null;
	/** 创建人标识 */
	private Long creatorId = null;
	/** 创建时间 */
	private String createdTime = null;
	/** 登录token */
	private String loginToken = null;
}
