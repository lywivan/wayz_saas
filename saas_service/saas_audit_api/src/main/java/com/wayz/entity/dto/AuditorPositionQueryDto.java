package com.wayz.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author mike.ma@wayz.ai
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuditorPositionQueryDto implements Serializable {

	/** 设备名称 */
	private String positionName = null;
	/** 播控编码 */
	private String playCode = null;
	/** 审核方标识 */
	private Long auditorId = null;
	/** 媒体标识 */
	private Long companyId = null;
	/** 媒体名称 */
	private String companyName = null;
	/** 区县标识 */
	private Long districtId = null;
	/** 城市标识 */
	private Long cityId = null;
	/** 省份标识 */
	private Long provinceId = null;
	/** 场景标识 */
	private Long sceneId = null;
	/** 是否在线 */
	private Boolean isOnline = null;
	/** 是否启用 */
	private Boolean isEnable = null;
	/** 分页开始 */
	private Integer startIndex = null;
	/** 分页大小 */
	private Integer pageSize = null;

}
