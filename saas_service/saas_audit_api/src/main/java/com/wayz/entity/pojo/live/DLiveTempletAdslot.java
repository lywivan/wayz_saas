package com.wayz.entity.pojo.live;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 海报模板分块配置类
 * 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DLiveTempletAdslot implements Serializable {

	/** 模板标识 */
	private Long templetId = null;
	/** 编号 */
	private Integer num = null;
	/** 名称 */
	private String name = null;
	/** 素材ID */
	private Long materialId = null;
	/** 文件链接 */
	private String url = null;
	/** 0:AD 1:图片 2:文字区 3:网页区 4:直播流 */
	private Short type = null;
	/** 文本json串 */
	private String jsonTxt = null;
	/** 位置:上 */
	private Integer top = null;
	/** 位置:左 */
	private Integer lft = null;
	/** 宽 */
	private Integer width = null;
	/** 高 */
	private Integer height = null;
	/** 创建时间 */
	private String createdTime = null;
	/** 修改时间 */
	private String modifiedTime = null;
	/** 页面主体内容 */
	private String contentT = null;

}
