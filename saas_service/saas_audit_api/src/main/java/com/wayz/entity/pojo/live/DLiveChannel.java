package com.wayz.entity.pojo.live;

import java.io.Serializable;

/**
 * 直播渠道类
 * 
 */
public class DLiveChannel implements Serializable {

	/** 标识 */
	private Long id = null;
	/** 显示名称 */
	private String name = null;

	/**
	 * 获取标识
	 * 
	 * @return 标识
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 设置标识
	 * 
	 * @param id 标识
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 获取显示名称
	 * 
	 * @return 显示名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置显示名称
	 * 
	 * @param name 显示名称
	 */
	public void setName(String name) {
		this.name = name;
	}

}
