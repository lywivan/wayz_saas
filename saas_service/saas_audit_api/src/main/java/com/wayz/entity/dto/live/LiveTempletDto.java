package com.wayz.entity.dto.live;

import com.wayz.entity.pojo.live.DLiveTempletAdslot;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 海报模板摘要类
 * 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LiveTempletDto implements Serializable {

	/** 模板标识 */
	private Long id = null;
	/** 公司标识 */
	private Long companyId = null;
	/** 分辨率标识 */
	private Long resolutionId = null;
	/** 行业标识 */
	private Long categoryId = null;
	/** 模板名称 */
	private String name = null;
	/** 预览URL */
	private String previewUrl = null;
	/** 模板背景音乐 */
	private String backgroundAudio = null;
	/** 模板背景 */
	private String background = null;
	private List<LiveTempletAdslotDto> adslotList = null;

}
