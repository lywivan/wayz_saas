package com.wayz.entity.redis;

import java.text.MessageFormat;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

/**
 * 登录令牌标识值实现类
 * 
 * @author mahao
 *
 */
@Repository("rLoginTokenMonitorUserIdValue")
public class RLoginTokenMonitorUserIdValueImpl implements RLoginTokenMonitorUserIdValue {

	/** Redis模板 */
	@Resource(name = "redisTemplate")
	private RedisTemplate<String, String> redisTemplate = null;

	/** 键值格式 */
	private static final MessageFormat KEY_FORMAT = new MessageFormat("platform:Value:LoginTokenMonitorUserId:{0}");

	/**
	 * 设置登录令牌标识
	 * 
	 * @param loginToken 登录令牌
	 * @param userId 标识
	 */
	@Override
	public void set(String loginToken, Long userId) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { loginToken });

		// 调用接口
		redisTemplate.opsForValue().set(key, userId.toString());
	}

	/**
	 * 设置令牌用户
	 * 
	 * @param token 登录令牌
	 * @param userId 用户标识
	 * @param timeout 超时时间
	 * @param unit 超时单位
	 */
	@Override
	public void set(String token, Long userId, long timeout, TimeUnit unit) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { token });

		// 调用接口
		redisTemplate.opsForValue().set(key, userId.toString(), timeout, unit);
	}

	/**
	 * 获取登录令牌标识
	 * 
	 * @param loginToken 登录令牌
	 * @return 标识
	 */
	@Override
	public Long get(String loginToken) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { loginToken });

		// 调用接口
		String userId = redisTemplate.opsForValue().get(key);

		// 返回数据
		if (userId != null) {
			return Long.parseLong(userId);
		}
		return null;
	}

	/**
	 * 删除登录令牌标识
	 * 
	 * @param loginToken 登录令牌
	 */
	@Override
	public void remove(String loginToken) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { loginToken });

		// 调用接口
		redisTemplate.delete(key);
	}

	/**
	 * 存在登录令牌标识
	 * 
	 * @param loginToken 登录令牌
	 * @return 是否存在
	 */
	@Override
	public boolean exist(String loginToken) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { loginToken });

		// 调用接口
		return redisTemplate.hasKey(key);
	}

	/**
	 * 设置超时时间
	 * 
	 * @param token 登录令牌
	 * @param timeout 超时时间
	 * @param unit 时间单位
	 */
	@Override
	public void setExpire(String token, long timeout, TimeUnit unit) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { token });

		// 调用接口
		redisTemplate.expire(key, timeout, unit);
	}

	/**
	 * 获取超时时间
	 * 
	 * @param token 登录令牌
	 * @param unit 时间单位
	 * @return 超时时间
	 */
	@Override
	public Long getExpire(String token, TimeUnit unit) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { token });

		// 调用接口
		return redisTemplate.getExpire(key, unit);
	}

}
