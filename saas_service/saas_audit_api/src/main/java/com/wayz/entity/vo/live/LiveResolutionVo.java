package com.wayz.entity.vo.live;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 分辨率类
 * 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LiveResolutionVo implements Serializable {

	/** 分辨率标识 */
	private Long id = null;
	/** 名称 */
	private String name = null;
	/** 宽(pix) */
	private Integer width = null;
	/** 高(pix) */
	private Integer height = null;
	
}
