package com.wayz.entity.dto.live;

import com.wayz.constants.PageDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 海报模板摘要类
 * 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LiveTempletPageDto extends PageDto implements Serializable {

	/** 公司标识 */
	private Long companyId = null;
	/** 分辨率标识 */
	private Long resolutionId = null;
	/** 模板名称 */
	private String name = null;

}
