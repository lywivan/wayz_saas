package com.wayz.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuditorQueryDto implements Serializable {

	/** 审核方名称 */
	private String name = null;
	/** 审核方类型 1.奥凌;2:CIBN;3.场景方;4.媒体方 */
	private Short type = null;
	/** 审核方状态(1:启用 2:禁用;) */
	private Short status = null;
	/** 手机号 */
	private String telephone = null;
	/** 分页开始 */
	private Integer startIndex = null;
	/** 分页大小 */
	private Integer pageSize = null;

}
