package com.wayz.entity.redis;

/**
 * 标识登录令牌值接口
 * 
 * @author mahao
 *
 */
public interface RMonitorUserIdLoginTokenValue {

	/**
	 * 设置标识登录令牌
	 * 
	 * @param userId 标识
	 * @param loginToken 登录令牌
	 */
	public void set(Long userId, String loginToken);

	/**
	 * 获取标识登录令牌
	 * 
	 * @param userId 标识
	 * @return 登录令牌
	 */
	public String get(Long userId);

	/**
	 * 删除标识登录令牌
	 * 
	 * @param userId 标识
	 */
	public void remove(Long userId);

	/**
	 * 存在标识登录令牌
	 * 
	 * @param userId 标识
	 * @return 是否存在
	 */
	public boolean exist(Long userId);

}
