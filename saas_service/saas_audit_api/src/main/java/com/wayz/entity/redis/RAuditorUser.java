package com.wayz.entity.redis;

import java.text.MessageFormat;

/**
 * 
 */
public class RAuditorUser {

    /** 键值格式 */
    private static final MessageFormat KEY_FORMAT = new MessageFormat("platform:Object:Admin:AuditorUser:{0}");
    /** 用户标识 */
    private Long id = null;
	/** 审核方标识 */
	private Long auditorId = null;
	/** 审核方类型 */
	private Short auditorType = null;
    /** 用户名称 */
    private String username = null;
    /** 用户密码 */
    private String password = null;
    /** 用户电话 */
    private String telephone = null;
    /** 创建人标识 */
    private Long creatorId = null;
    
    /**
     * 获取key值方法
     * @param id
     * @return
     */
    public static String getKey(Long id){
        return KEY_FORMAT.format(new Long[] { id });
    }


    /** 常量相关 */
    public static final String ID = "id";
    public static final String AUDITORID = "auditorId";
    public static final String AUDITORTYPE = "auditorType";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String TELEPHONE = "telephone";
    public static final String CREATORID = "creatorId";

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getAuditorId() {
		return auditorId;
	}
	public void setAuditorId(Long auditorId) {
		this.auditorId = auditorId;
	}
	public Short getAuditorType() {
		return auditorType;
	}
	public void setAuditorType(Short auditorType) {
		this.auditorType = auditorType;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public Long getCreatorId() {
		return creatorId;
	}
	public void setCreatorId(Long creatorId) {
		this.creatorId = creatorId;
	}


}
