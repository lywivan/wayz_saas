package com.wayz.entity.redis;

import java.text.MessageFormat;

import javax.annotation.Resource;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

/**
 * 标识登录令牌值实现类
 * 
 * @author mahao
 *
 */
@Repository("rMonitorUserIdLoginTokenValue")
public class RMonitorUserIdLoginTokenValueImpl implements RMonitorUserIdLoginTokenValue {

	/** Redis模板 */
	@Resource(name = "redisTemplate")
	private RedisTemplate<String, String> redisTemplate = null;

	/** 键值格式 */
	private static final MessageFormat KEY_FORMAT = new MessageFormat("platform:Value:MonitorUserIdLoginToken:{0}");

	/**
	 * 设置标识登录令牌
	 * 
	 * @param userId 标识
	 * @param loginToken 登录令牌
	 */
	@Override
	public void set(Long userId, String loginToken) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { userId });

		// 调用接口
		redisTemplate.opsForValue().set(key, loginToken);
	}

	/**
	 * 获取标识登录令牌
	 * 
	 * @param userId 标识
	 * @return 登录令牌
	 */
	@Override
	public String get(Long userId) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { userId });

		// 调用接口
		return redisTemplate.opsForValue().get(key);
	}

	/**
	 * 删除标识登录令牌
	 * 
	 * @param userId 标识
	 */
	@Override
	public void remove(Long userId) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { userId });

		// 调用接口
		redisTemplate.delete(key);
	}

	/**
	 * 存在标识登录令牌
	 * 
	 * @param userId 标识
	 * @return 是否存在
	 */
	@Override
	public boolean exist(Long userId) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { userId });

		// 调用接口
		return redisTemplate.hasKey(key);
	}

}
