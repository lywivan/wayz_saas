package com.wayz.entity.redis;

import java.util.concurrent.TimeUnit;

/**
 * 登录令牌标识值接口
 * 
 * @author mahao
 *
 */
public interface RLoginTokenMonitorUserIdValue {

	/**
	 * 设置登录令牌标识
	 * 
	 * @param loginToken 登录令牌
	 * @param userId 标识
	 */
	void set(String loginToken, Long userId);

	/**
	 * 设置令牌用户
	 * 
	 * @param token 登录令牌
	 * @param userId 用户标识
	 * @param timeout 超时时间
	 * @param unit 超时单位
	 */
	void set(String token, Long userId, long timeout, TimeUnit unit);
	
	/**
	 * 获取登录令牌标识
	 * 
	 * @param loginToken 登录令牌
	 * @return 标识
	 */
	Long get(String loginToken);

	/**
	 * 删除登录令牌标识
	 * 
	 * @param loginToken 登录令牌
	 */
	void remove(String loginToken);

	/**
	 * 存在登录令牌标识
	 * 
	 * @param loginToken 登录令牌
	 * @return 是否存在
	 */
	boolean exist(String loginToken);
	
	/**
	 * 设置超时时间
	 * 
	 * @param token 登录令牌
	 * @param timeout 超时时间
	 * @param unit 时间单位
	 */
	void setExpire(String token, long timeout, TimeUnit unit);

	/**
	 * 获取超时时间
	 * 
	 * @param token 登录令牌
	 * @param unit 时间单位
	 * @return 超时时间
	 */
	Long getExpire(String token, TimeUnit unit);

}
