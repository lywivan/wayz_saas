package com.wayz;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * RabbitMq模块启动类
 *
 * @author:ivan.liu
 */
@EnableDiscoveryClient
@MapperScan("com.wayz.mapper")
@SpringBootApplication(scanBasePackages = {"com.wayz"})
public class RabbitMqApplication {
    public static void main(String[] args) {
        SpringApplication.run(RabbitMqApplication.class,args);
    }
}
