package com.wayz.mapper;

import com.wayz.entity.pojo.play.DPlayHeartbeat;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 播控心跳记录DAO接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("dPlayHeartbeatDAO")
public interface DPlayHeartbeatDAO{

	/**
	 * 批量创建播控器心跳记录
	 * @param createList
	 */
	Integer batchCreate(@Param("createList") List<DPlayHeartbeat> createList,String tableName);
	
}
