package com.wayz.json;

import com.alibaba.fastjson.JSON;
import com.wayz.RabbitMqApplication;
import com.wayz.entity.pojo.play.DPlayHeartbeat;
import com.wayz.rabbitmq.MqDefinition;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 测试FastJson
 *
 * @author:ivan.liu
 */
@SpringBootTest(classes = RabbitMqApplication.class)
@RunWith(SpringRunner.class)
public class JsonTest {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Test
    public void test(){
        DPlayHeartbeat dPlayHeartbeat = new DPlayHeartbeat();
        dPlayHeartbeat.setId(1L);
        dPlayHeartbeat.setPlayCode("aaa");
        dPlayHeartbeat.setMenuid(1L);
        System.out.println(JSON.toJSONString(dPlayHeartbeat));
        rabbitTemplate.convertAndSend(
                MqDefinition.PLAY_EXCHANGE,
                MqDefinition.PLAY_HEART_BEAT_ROUTING_KEY,
                JSON.toJSONString(dPlayHeartbeat));
    }
}
