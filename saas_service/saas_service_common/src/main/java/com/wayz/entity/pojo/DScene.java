package com.wayz.entity.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 场景类
 * 
 *  
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DScene implements Serializable {

	/** 场景标识 */
	private Long id = null;
	/** 场景名称 */
	private String name = null;
	/** 描述 */
	private String description = null;
	/**公司编号*/
	private Long companyId;
	/** 是否删除 */
	private Boolean isDeleted = null;
	/**  */
	private Timestamp createdTime = null;
	/**  */
	private Timestamp modifiedTime = null;


}
