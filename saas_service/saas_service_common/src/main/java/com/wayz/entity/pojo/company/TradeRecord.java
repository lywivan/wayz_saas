package com.wayz.entity.pojo.company;

import java.io.Serializable;

/**
 * 交易记录类
 * 
 * @author wade.liu@wayz.ai
 *
 */
public class TradeRecord implements Serializable {

	/** 标识 */
	private Long id = null;
	/** 金额(元) */
	private Double amount = null;
	/** 实付金额(元) */
	private Double payment = null;
	/** 交易类型(11:现金充值 12:红包充值 13:转账充值 14:退款充值 21:设备续期消费 22:空间扩展消费) */
	private Short tradeType = null;
	/** 交易类型名称 */
	private String tradeTypeName = null;
	/** 操作员名称 */
	private String operatorName = null;
	/** 当前余额(元) */
	private Double balance = null;
	/** 备注 */
	private String remark = null;
	/** 操作员类型(1:平台;2:代理;3:租户) */
	private Short operatorType = null;
	/** 创建时间 */
	private String createdTime = null;

	/**
	 * 获取标识
	 * 
	 * @return 标识
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 设置标识
	 * 
	 * @param id 标识
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 获取金额(元)
	 * 
	 * @return 金额(元)
	 */
	public Double getAmount() {
		return amount;
	}

	/**
	 * 设置金额(元)
	 * 
	 * @param amount 金额(元)
	 */
	public void setAmount(Double amount) {
		this.amount = amount;
	}

	/**
	 * 获取应付金额
	 * 
	 * @return 应付金额
	 */
	public Double getPayment() {
		return payment;
	}

	/**
	 * 设置应付金额
	 * 
	 * @param payment 应付金额
	 */
	public void setPayment(Double payment) {
		this.payment = payment;
	}

	/**
	 * 获取交易类型(11:现金充值 12:红包充值 13:转账充值 14:退款充值 21:设备续期消费 22:空间扩展消费)
	 * 
	 * @return 交易类型(11:现金充值 12:红包充值 13:转账充值 14:退款充值 21:设备续期消费 22:空间扩展消费)
	 */
	public Short getTradeType() {
		return tradeType;
	}

	/**
	 * 设置交易类型(11:现金充值 12:红包充值 13:转账充值 14:退款充值 21:设备续期消费 22:空间扩展消费)
	 * 
	 * @param tradeType 交易类型(11:现金充值 12:红包充值 13:转账充值 14:退款充值 21:设备续期消费
	 *            22:空间扩展消费)
	 */
	public void setTradeType(Short tradeType) {
		this.tradeType = tradeType;
	}

	/**
	 * 获取当前余额(元)
	 * 
	 * @return 当前余额(元)
	 */
	public Double getBalance() {
		return balance;
	}

	/**
	 * 设置当前余额(元)
	 * 
	 * @param balance 当前余额(元)
	 */
	public void setBalance(Double balance) {
		this.balance = balance;
	}

	/**
	 * 获取备注
	 * 
	 * @return 备注
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * 设置备注
	 * 
	 * @param remark 备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * 获取操作员类型(1:平台;2:代理;3:租户)
	 * 
	 * @return 操作员类型(1:平台;2:代理;3:租户)
	 */
	public Short getOperatorType() {
		return operatorType;
	}

	/**
	 * 设置操作员类型(1:平台;2:代理;3:租户)
	 * 
	 * @param operatorType 操作员类型(1:平台;2:代理;3:租户)
	 */
	public void setOperatorType(Short operatorType) {
		this.operatorType = operatorType;
	}

	/**
	 * 获取创建时间
	 * 
	 * @return 创建时间
	 */
	public String getCreatedTime() {
		return createdTime;
	}

	/**
	 * 设置创建时间
	 * 
	 * @param createdTime 创建时间
	 */
	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}

	public String getTradeTypeName() {
		return tradeTypeName;
	}

	public void setTradeTypeName(String tradeTypeName) {
		this.tradeTypeName = tradeTypeName;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

}
