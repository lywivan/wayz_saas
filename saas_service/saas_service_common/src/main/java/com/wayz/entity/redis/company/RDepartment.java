package com.wayz.entity.redis.company;

/**
 * 部门类
 * 
 * @author wade.liu@wayz.ai
 *
 */
public class RDepartment {

	/** 属性相关 */
	/** 名称 */
	private String name = null;
	/** 路径 */
	private String path = null;
	/** 父标识 */
	private Long parentId = null;

	/** 常量相关 */
	/** 名称 */
	public static final String NAME = "name";
	/** 路径 */
	public static final String PATH = "path";
	/** 父标识 */
	public static final String PARENTID = "parentId";

	/**
	 * 获取名称
	 * 
	 * @return 名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置名称
	 * 
	 * @param name 名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 获取路径
	 * 
	 * @return 路径
	 */
	public String getPath() {
		return path;
	}

	/**
	 * 设置路径
	 * 
	 * @param path 路径
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * 获取父标识
	 * 
	 * @return 父标识
	 */
	public Long getParentId() {
		return parentId;
	}

	/**
	 * 设置父标识
	 * 
	 * @param parentId 父标识
	 */
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

}
