package com.wayz.entity.redis.company;

/**
 * 域名logo类
 * 
 * @author wade.liu@wayz.ai
 *
 */
public class RDomainLogo {

	/** 属性相关 */
	/** 域名 */
	private String domainName = null;
	/** logo */
	private String logo = null;
	/** 标题 */
	private String websiteTitle = null;

	/** 常量相关 */
	/** 域名 */
	public static final String DOMAINNAME = "domainName";
	/** logo */
	public static final String LOGO = "logo";
	/** 标题 */
	public static final String WEBSITETITLE = "websiteTitle";

	/**
	 * 获取域名
	 * 
	 * @return 域名
	 */
	public String getDomainName() {
		return domainName;
	}

	/**
	 * 设置域名
	 * 
	 * @param domainName 域名
	 */
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	/**
	 * 获取logo
	 * 
	 * @return logo
	 */
	public String getLogo() {
		return logo;
	}

	/**
	 * 设置logo
	 * 
	 * @param logo logo
	 */
	public void setLogo(String logo) {
		this.logo = logo;
	}

	/**
	 * 获取标题
	 * 
	 * @return 标题
	 */
	public String getWebsiteTitle() {
		return websiteTitle;
	}

	/**
	 * 设置标题
	 * 
	 * @param websiteTitle 标题
	 */
	public void setWebsiteTitle(String websiteTitle) {
		this.websiteTitle = websiteTitle;
	}

}
