package com.wayz.entity.redis.company;

/**
 * 公司类
 * 
 * @author wade.liu@wayz.ai
 *
 */
public class RCompany {

	/** 属性相关 */
	/** 公司标识 */
	private Long companyId = null;
	/** 公司名称 */
	private String name = null;
	/** 公司状态(1:启用 2:禁用 3:试用中 4:已过期) */
	private Short status = null;
	/** 公司主账户id */
	private Long userId;
	/** 手机号 */
	private String telephone = null;
	/** 联系邮箱 */
	private String email = null;
	/** 版本标识 */
	private Long versionId = null;
	/** 是否对接dsp广告 */
	private Boolean accessDsp = null;
	/** 是否需要数据权限功能 */
	private Boolean requireDataAuth = null;
	/** 是否需要广告审核功能 */
	private Boolean requirePlanAudit = null;
	/** 用户数限制 */
	private Integer userLimit = null;
	/** 媒体位数量限制 */
	// private Integer positionLimit = null;
	/** 每年广告投放数量 */
	private Integer planLimit = null;
	/** 素材空间限制(Byte) */
	private Long materialLimit = null;
	/** 有效期至 */
	private String validUntilDate = null;
	/** 试用账户加水印 */
	private Boolean isWatermark = null;
	/** 剩余用户数量 */
	private Integer surplusUserLimit = null;
	/** 剩余媒体位数量限制 */
	// private Integer surplusPositionLimit = null;
	/** 剩余每年广告投放数量 */
	private Integer surplusPlanLimit = null;
	/** 剩余素材空间限制(Byte) */
	private Long surplusMaterialLimit = null;
	/** 联系人 */
	private String contacts = null;
	/** logo(OEM使用) */
	private String logo = null;
	/** 域名(OEM使用) */
	private String domain = null;
	/** 平台名称 */
	private String websiteTitle = null;
	/** 是否有广告 */
	private Boolean isAdPaln = null;
	/** 是否一键登录 */
	private Boolean isLogin = null;
	/** 代理商ID */
	private Long agentId = null;
	/** 默认设备组标识 */
	private Long groupId = null;
	/** 账户余额 */
	private Double balance = null;
	/** 部门当前ID */
	private Long departmentIndex = null;

	/** 常量相关 */
	/** 公司标识 */
	public static final String COMPANYID = "companyId";
	/** 公司名称 */
	public static final String NAME = "name";
	/** 公司状态(1:启用 2:禁用 3:试用中 4:已过期) */
	public static final String STATUS = "status";
	/** 公司主账户id */
	public static final String USERID = "userId";
	/** 手机号 */
	public static final String TELEPHONE = "telephone";
	/** 联系邮箱 */
	public static final String EMAIL = "email";
	/** 版本标识 */
	public static final String VERSIONID = "versionId";
	/** 是否对接dsp广告 */
	public static final String ACCESSDSP = "accessDsp";
	/** 是否需要数据权限功能 */
	public static final String REQUIREDATAAUTH = "requireDataAuth";
	/** 是否需要广告审核功能 */
	public static final String REQUIREPLANAUDIT = "requirePlanAudit";
	/** 用户数限制 */
	public static final String USERLIMIT = "userLimit";
	/** 媒体位数量限制 */
	// public static final String POSITIONLIMIT = "positionLimit";
	/** 每年广告投放数量 */
	public static final String PLANLIMIT = "planLimit";
	/** 素材空间限制(GB) */
	public static final String MATERIALLIMIT = "materialLimit";
	/** 有效期至 */
	public static final String VALIDUNTILDATE = "validUntilDate";
	/** 试用账户加水印 */
	public static final String ISWATERMARK = "isWatermark";
	/** 剩余用户数量 */
	public static final String SURPLUSUSERLIMIT = "surplusUserLimit";
	/** 剩余媒体位数量限制 */
	// public static final String SURPLUSPOSITIONLIMIT = "surplusPositionLimit";
	/** 剩余每年广告投放数量 */
	public static final String SURPLUSPLANLIMIT = "surplusPlanLimit";
	/** 剩余素材空间限制(Byte) */
	public static final String SURPLUSMATERIALLIMIT = "surplusMaterialLimit";
	/** 联系人 */
	public static final String CONTACTS = "contacts";
	/** logo(OEM使用) */
	public static final String LOGO = "logo";
	/** 域名(OEM使用) */
	public static final String DOMAIN = "domain";
	/** 平台名称 */
	public static final String WEBSITETITLE = "websiteTitle";
	/** 是否有广告 */
	public static final String ISADPALN = "isAdPaln";
	/** 代理商ID */
	public static final String AGENTID = "agentId";
	/** 是否一键登录 */
	public static final String ISLOGIN = "isLogin";
	/** 默认设备组标识 */
	public static final String GROUPID = "groupId";
	/** 账户余额 */
	public static final String BALANCE = "balance";
	/** 部门当前ID */
	public static final String DEPARTMENTINDEX = "departmentIndex";

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Short getStatus() {
		return status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getVersionId() {
		return versionId;
	}

	public void setVersionId(Long versionId) {
		this.versionId = versionId;
	}

	public Boolean getAccessDsp() {
		return accessDsp;
	}

	public void setAccessDsp(Boolean accessDsp) {
		this.accessDsp = accessDsp;
	}

	public Boolean getRequireDataAuth() {
		return requireDataAuth;
	}

	public void setRequireDataAuth(Boolean requireDataAuth) {
		this.requireDataAuth = requireDataAuth;
	}

	public Boolean getRequirePlanAudit() {
		return requirePlanAudit;
	}

	public void setRequirePlanAudit(Boolean requirePlanAudit) {
		this.requirePlanAudit = requirePlanAudit;
	}

	public Integer getUserLimit() {
		return userLimit;
	}

	public void setUserLimit(Integer userLimit) {
		this.userLimit = userLimit;
	}

	public Integer getPlanLimit() {
		return planLimit;
	}

	public void setPlanLimit(Integer planLimit) {
		this.planLimit = planLimit;
	}

	public Long getMaterialLimit() {
		return materialLimit;
	}

	public void setMaterialLimit(Long materialLimit) {
		this.materialLimit = materialLimit;
	}

	public Integer getSurplusUserLimit() {
		return surplusUserLimit;
	}

	public void setSurplusUserLimit(Integer surplusUserLimit) {
		this.surplusUserLimit = surplusUserLimit;
	}

	public Integer getSurplusPlanLimit() {
		return surplusPlanLimit;
	}

	public void setSurplusPlanLimit(Integer surplusPlanLimit) {
		this.surplusPlanLimit = surplusPlanLimit;
	}

	public Long getSurplusMaterialLimit() {
		return surplusMaterialLimit;
	}

	public void setSurplusMaterialLimit(Long surplusMaterialLimit) {
		this.surplusMaterialLimit = surplusMaterialLimit;
	}

	public String getContacts() {
		return contacts;
	}

	public void setContacts(String contacts) {
		this.contacts = contacts;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getWebsiteTitle() {
		return websiteTitle;
	}

	public void setWebsiteTitle(String websiteTitle) {
		this.websiteTitle = websiteTitle;
	}

	public Boolean getIsAdPaln() {
		return isAdPaln;
	}

	public void setIsAdPaln(Boolean isAdPaln) {
		this.isAdPaln = isAdPaln;
	}

	public Boolean getIsLogin() {
		return isLogin;
	}

	public void setIsLogin(Boolean isLogin) {
		this.isLogin = isLogin;
	}

	public Long getAgentId() {
		return agentId;
	}

	public void setAgentId(Long agentId) {
		this.agentId = agentId;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public Long getDepartmentIndex() {
		return departmentIndex;
	}

	public void setDepartmentIndex(Long departmentIndex) {
		this.departmentIndex = departmentIndex;
	}

	public String getValidUntilDate() {
		return validUntilDate;
	}

	public void setValidUntilDate(String validUntilDate) {
		this.validUntilDate = validUntilDate;
	}

	public Boolean getIsWatermark() {
		return isWatermark;
	}

	public void setIsWatermark(Boolean isWatermark) {
		this.isWatermark = isWatermark;
	}

	// public Integer getPositionLimit() {
	// return positionLimit;
	// }
	//
	// public void setPositionLimit(Integer positionLimit) {
	// this.positionLimit = positionLimit;
	// }
	//
	// public Integer getSurplusPositionLimit() {
	// return surplusPositionLimit;
	// }
	//
	// public void setSurplusPositionLimit(Integer surplusPositionLimit) {
	// this.surplusPositionLimit = surplusPositionLimit;
	// }

}
