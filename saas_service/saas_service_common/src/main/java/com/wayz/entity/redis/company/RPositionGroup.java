package com.wayz.entity.redis.company;

/**
 * 媒体分组数据类
 * 
 * @author wade.liu@wayz.ai
 *
 */
public class RPositionGroup {

	/** 属性相关 */
	/** 公司标志 */
	private Long companyId = null;
	/** 媒体分组名称 */
	private String name = null;
	/** 手机号 */
	private String phone = null;
	/** 开机时间(HH:MM:SS) */
	private String startTime = null;
	/** 关机时间(HH:MM:SS) */
	private String closeTime = null;
	/** 音量大小(0 - 10) */
	private Short volumn = null;
	/** 屏幕旋转角度(0,90,180,270) */
	private Integer rotation = null;
	/** 是否开机自启 */
	private Boolean isAutoStart = null;
	/** 字幕模板标识 */
	private Long subtitleTempletId = null;
	/** 播控器版本 */
	private String playVersion = null;
	/** 启动画面 */
	private String openReserve = null;
	/** 播控器垫片 */
	private String playReserve = null;
	/** 是否启用自动开关机 */
	private Boolean autoOnOff = null;
	/** 部门路径 */
	private String departPath = null;

	/** 常量相关 */
	/** 公司标志 */
	public static final String COMPANYID = "companyId";
	/** 媒体分组名称 */
	public static final String NAME = "name";
	/** 手机号 */
	public static final String PHONE = "phone";
	/** 开机时间(HH:MM:SS) */
	public static final String STARTTIME = "startTime";
	/** 关机时间(HH:MM:SS) */
	public static final String CLOSETIME = "closeTime";
	/** 音量大小(0 - 10) */
	public static final String VOLUMN = "volumn";
	/** 屏幕旋转角度(0,90,180,270) */
	public static final String ROTATION = "rotation";
	/** 是否开机自启 */
	public static final String ISAUTOSTART = "isAutoStart";
	/** 字幕模板标识 */
	public static final String SUBTITLETEMPLETID = "subtitleTempletId";
	/** 播控器版本 */
	public static final String PLAYVERSION = "playVersion";
	/** 播控器垫片 */
	public static final String PLAYRESERVE = "playReserve";
	/** 启用自动开关机 */
	public static final String AUTOONOFF = "autoOnOff";
	/** 部门路径 */
	public static final String DEPARTPATH = "departPath";
	/** 启动画面 */
	public static final String OPENRESERVE = "openReserve";
	
	/**
	 * 获取公司标志
	 * 
	 * @return 公司标志
	 */
	public Long getCompanyId() {
		return companyId;
	}

	/**
	 * 设置公司标志
	 * 
	 * @param companyId 公司标志
	 */
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	/**
	 * 获取媒体分组名称
	 * 
	 * @return 媒体分组名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置媒体分组名称
	 * 
	 * @param name 媒体分组名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 获取手机号
	 * 
	 * @return 手机号
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * 设置手机号
	 * 
	 * @param phone 手机号
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * 获取开机时间(HH:MM:SS)
	 * 
	 * @return 开机时间(HH:MM:SS)
	 */
	public String getStartTime() {
		return startTime;
	}

	/**
	 * 设置开机时间(HH:MM:SS)
	 * 
	 * @param startTime 开机时间(HH:MM:SS)
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	/**
	 * 获取关机时间(HH:MM:SS)
	 * 
	 * @return 关机时间(HH:MM:SS)
	 */
	public String getCloseTime() {
		return closeTime;
	}

	/**
	 * 设置关机时间(HH:MM:SS)
	 * 
	 * @param closeTime 关机时间(HH:MM:SS)
	 */
	public void setCloseTime(String closeTime) {
		this.closeTime = closeTime;
	}

	/**
	 * 获取音量大小(0 - 10)
	 * 
	 * @return 音量大小(0 - 10)
	 */
	public Short getVolumn() {
		return volumn;
	}

	/**
	 * 设置音量大小(0 - 10)
	 * 
	 * @param volumn 音量大小(0 - 10)
	 */
	public void setVolumn(Short volumn) {
		this.volumn = volumn;
	}

	public Integer getRotation() {
		return rotation;
	}

	public void setRotation(Integer rotation) {
		this.rotation = rotation;
	}

	public Boolean getIsAutoStart() {
		return isAutoStart;
	}

	public void setIsAutoStart(Boolean isAutoStart) {
		this.isAutoStart = isAutoStart;
	}

	/**
	 * 获取模板标识
	 * 
	 * @return
	 */
	public Long getSubtitleTempletId() {
		return subtitleTempletId;
	}

	/**
	 * 设置模板标识
	 * 
	 * @param subtitleTempletId
	 */
	public void setSubtitleTempletId(Long subtitleTempletId) {
		this.subtitleTempletId = subtitleTempletId;
	}

	/**
	 * 获取播控器模板
	 * 
	 * @return
	 */
	public String getPlayVersion() {
		return playVersion;
	}

	/**
	 * 设置播控器模板
	 * 
	 * @param playVersion
	 */
	public void setPlayVersion(String playVersion) {
		this.playVersion = playVersion;
	}

	/**
	 * 获取播控器垫片
	 * 
	 * @return
	 */
	public String getPlayReserve() {
		return playReserve;
	}

	/**
	 * 设置播控器垫片
	 * 
	 * @param playReserve
	 */
	public void setPlayReserve(String playReserve) {
		this.playReserve = playReserve;
	}

	/**
	 * 获取是否启用自动开关机
	 * 
	 * @return 是否启用自动开关机
	 */
	public Boolean getAutoOnOff() {
		return autoOnOff;
	}

	/**
	 * 设置是否启用自动开关机
	 * 
	 * @param autoOnOff
	 */
	public void setAutoOnOff(Boolean autoOnOff) {
		this.autoOnOff = autoOnOff;
	}

	public String getDepartPath() {
		return departPath;
	}

	public void setDepartPath(String departPath) {
		this.departPath = departPath;
	}

	public String getOpenReserve() {
		return openReserve;
	}

	public void setOpenReserve(String openReserve) {
		this.openReserve = openReserve;
	}

}
