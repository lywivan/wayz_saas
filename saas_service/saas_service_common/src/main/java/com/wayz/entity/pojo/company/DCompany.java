package com.wayz.entity.pojo.company;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * 公司类
 * 
 * @author " 须尽欢_____
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DCompany implements Serializable {

	/** 公司标识 */
	private Long id = null;
	/** 名称 */
	private String name = null;
	/** 公司状态(1:启用 2:禁用 3:试用中 4:已过期) */
	private Short status = null;
	/** 管理员编号 */
	private Long userId;
	/** 是否删除 */
	private Boolean isDeleted = null;
	/** 联系人 */
	private String contacts = null;
	/** 电话 */
	private String telephone = null;
	/** email */
	private String email = null;
	/** 详细地址 */
	private String address = null;
	/** logo(OEM使用) */
	private String logo = null;
	/** 域名(OEM使用) */
	private String domain = null;
	/** 公司主账户 */
	private String userName;
	/** 平台名称 */
	private String websiteTitle;
	/** 服务版本 */
	private Long versionId = null;
	/** 服务版本 */
	private String versionName = null;
	/** 是否需要数据权限功能 */
	private Boolean requireDataAuth = null;
	/** 是否需要广告审核功能 */
	private Boolean requirePlanAudit = null;
	/** 是否对接dsp广告 */
	private Boolean accessDsp = null;
	/** 用户数限制 */
	private Integer userLimit = null;
	/** 每年广告投放数量 */
	private Integer planLimit = null;
	/** 素材空间限制(Byte) */
	private Long materialLimit = null;
	/** 试用账户有效期至 */
	private Date validUntilDate = null;
	/** 试用账户加水印 */
	private Boolean isWatermark = null;
	/** 创建者标识 */
	private Long creatorId = null;
	/** 创建时间 */
	private Timestamp createdTime = null;
	/** 修改时间 */
	private Timestamp modifiedTime = null;
	/** 公司来源 1：管理员 2：用户自主 */
	private Short sourceType = null;
	/** 代理商ID */
	private Long agentId;
	/** 代理商名称 */
	private String agentName = null;
	/** 账户余额 */
	private Double balance = null;

}
