package com.wayz.entity.redis.play;

/**
 * 功能版本类
 * 
 * @author wade.liu@wayz.ai
 *
 */
public class RFunctionVersion {
	/** 键值格式 */
	// private static final MessageFormat KEY_FORMAT = new
	// MessageFormat("platform:Id:Admin:Version:{0}");

	/** 属性相关 */
	/** 名称 */
	private String name = null;
	/** 描述 */
	private String description = null;
	/** 分辨率标识 */
	private Long roleId = null;
	/** 用户数限制 */
	private Integer userLimit = null;
	/** 素材空间限制(字节) */
	private Long materialLimit = null;
	/** 单个文件限制(字节) */
	private Long singleFileLimit = null;

	/** 常量相关 */
	/** 名称 */
	public static final String NAME = "name";
	/** 描述 */
	public static final String DESCRIPTION = "description";
	/** 分辨率标识 */
	public static final String ROLEID = "roleId";
	/** 用户数限制 */
	public static final String USERLIMIT = "userLimit";
	/** 素材空间限制(字节) */
	public static final String MATERIALLIMIT = "materialLimit";
	/** 单个文件限制(字节) */
	public static final String SINGLEFILELIMIT = "singleFileLimit";

	/**
	 * 获取名称
	 * 
	 * @return 名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置名称
	 * 
	 * @param name 名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 获取分辨率标识
	 * 
	 * @return 分辨率标识
	 */
	public Long getRoleId() {
		return roleId;
	}

	/**
	 * 设置分辨率标识
	 * 
	 * @param roleId 分辨率标识
	 */
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	/**
	 * 获取用户数限制
	 * 
	 * @return 用户数限制
	 */
	public Integer getUserLimit() {
		return userLimit;
	}

	/**
	 * 设置用户数限制
	 * 
	 * @param userLimit 用户数限制
	 */
	public void setUserLimit(Integer userLimit) {
		this.userLimit = userLimit;
	}

	/**
	 * 获取素材空间限制(字节)
	 * 
	 * @return 素材空间限制(字节)
	 */
	public Long getMaterialLimit() {
		return materialLimit;
	}

	/**
	 * 设置素材空间限制(字节)
	 * 
	 * @param materialLimit 素材空间限制(字节)
	 */
	public void setMaterialLimit(Long materialLimit) {
		this.materialLimit = materialLimit;
	}

	/**
	 * 获取单个文件限制(字节)
	 * 
	 * @return 单个文件限制(字节)
	 */
	public Long getSingleFileLimit() {
		return singleFileLimit;
	}

	/**
	 * 设置单个文件限制(字节)
	 * 
	 * @param singleFileLimit 单个文件限制(字节)
	 */
	public void setSingleFileLimit(Long singleFileLimit) {
		this.singleFileLimit = singleFileLimit;
	}
}
