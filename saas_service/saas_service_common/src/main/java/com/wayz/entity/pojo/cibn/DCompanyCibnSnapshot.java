package com.wayz.entity.pojo.cibn;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * 公司备案快照类
 *
 * @author mike.ma@wayz.ai
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DCompanyCibnSnapshot implements Serializable {

	/** 主键ID */
	private Long id = null;
	/** 公司ID */
	private Long companyId =null;
	/**  */
	private String name = null;
	/** 公司备案号 */
	private String recordNumber = null;
	/** 设备备案数 */
	private Integer equipmentNumber = null;
	/** 联系人 */
	private String contacts = null;
	/** 联系电话 */
	private String telephone = null;
	/** 详细地址 */
	private String address = null;
	/** 渠道 */
	private String channel = null;
	/** 原ID */
	private Long originId = null;
	/** 有效期 */
	private Date validUntilDate = null;
	/** 备案日期 */
	private Timestamp auditTime = null;

}
