package com.wayz.entity.redis.play;

/**
 * 播控器数据类
 * 
 * @author wade.liu@wayz.ai
 *
 */
public class RPlayer {

	/** 属性相关 */
	/** 媒体位标识 */
	private Long positionId = null;
	/** 媒体分组标识 */
	private Long groupId = null;
	/** 无心跳告警标识 */
	private Long hbAlertId = null;
	/** 是否在线 */
	private Boolean isOnline = null;
	/** 最新节目单标识 */
	private Long menuId = null;
	/** 最后心跳时间 */
	private Long lastHeartbeatTime = null;
	/** 公司标识 */
	private Long companyId = null;
	/** 推送令牌 */
	private String pushToken = null;
	/** 是否root */
	private Boolean isRoot = null;
	/** 实时直播标识 */
	private Long liveId = null;
	/** 紧急插播标识 */
	private String emergentId = null;
	/** 换刊screen标识列表 */
	private String screenIds = null;
	/** 设备媒体位状态(0:未开通;1:正常;2:已过期) */
	private Short status = null;
	/** 直播广告标识 */
	private Long livePlanId = null;
	/** 联屏标识 */
	private Long joinId = null;
	/** 联屏屏幕序号 */
	private Integer sort = null;
	/** ip地址 */
	private String ip = null;
	/** 是否直播中 */
	private Boolean isLive = null;
	
	/** 常量相关 */
	/** 媒体位标识 */
	public static final String POSITIONID = "positionId";
	/** 媒体分组标识 */
	public static final String GROUPID = "groupId";
	/** 无心跳告警标识 */
	public static final String HBALERTID = "hbAlertId";
	/** 是否在线 */
	public static final String ISONLINE = "isOnline";
	/** 是否有新的节目单标识 */
	public static final String MENUID = "menuId";
	/** 最后心跳时间 */
	public static final String LASTHEARTBEATTIME = "lastHeartbeatTime";
	/** 公司标识 */
	public static final String COMPANYID = "companyId";
	/** 推送令牌 */
	public static final String PUSHTOKEN = "pushToken";
	/** 是否root */
	public static final String ISROOT = "isRoot";
	/** 实时直播标识 */
	public static final String LIVEID = "liveId";
	/** 紧急插播标识 */
	public static final String EMERGENTID = "emergentId";
	/** 换刊screen标识列表 */
	public static final String SCREENIDS = "screenIds";
	/** 设备媒体位状态 (0:未开通;1:正常;2:已过期) */
	public static final String STATUS = "status";
	/** 直播广告标识 */
	public static final String LIVEPLANID = "livePlanId";
	/** 联屏标识 */
	public static final String JOINID = "joinId";
	/** 联屏屏幕序号 */
	public static final String SORT = "sort";
	/** ip地址 */
	public static final String IP = "ip";
	/** 是否直播中  */
	public static final String ISLIVE = "isLive";
	
	/**
	 * 获取媒体位标识
	 * 
	 * @return 媒体位标识
	 */
	public Long getPositionId() {
		return positionId;
	}

	/**
	 * 设置媒体位标识
	 * 
	 * @param positionId 媒体位标识
	 */
	public void setPositionId(Long positionId) {
		this.positionId = positionId;
	}

	/**
	 * 获取媒体分组标识
	 * 
	 * @return 媒体分组标识
	 */
	public Long getGroupId() {
		return groupId;
	}

	/**
	 * 设置媒体分组标识
	 * 
	 * @param groupId 媒体分组标识
	 */
	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	/**
	 * 获取无心跳告警标识
	 * 
	 * @return 无心跳告警标识
	 */
	public Long getHbAlertId() {
		return hbAlertId;
	}

	/**
	 * 设置无心跳告警标识
	 * 
	 * @param hbAlertId 无心跳告警标识
	 */
	public void setHbAlertId(Long hbAlertId) {
		this.hbAlertId = hbAlertId;
	}

	/**
	 * 获取是否在线
	 * 
	 * @return 是否在线
	 */
	public Boolean getIsOnline() {
		return isOnline;
	}

	/**
	 * 设置是否在线
	 * 
	 * @param isOnline 是否在线
	 */
	public void setIsOnline(Boolean isOnline) {
		this.isOnline = isOnline;
	}

	/**
	 * 获取节目单标识
	 * 
	 * @return 节目单标识
	 */
	public Long getMenuId() {
		return menuId;
	}

	/**
	 * 设置节目单标识
	 * 
	 * @param menuId 节目单标识
	 */
	public void setMenuId(Long menuId) {
		this.menuId = menuId;
	}

	/**
	 * 获取最后心跳时间
	 * 
	 * @return 最后心跳时间
	 */
	public Long getLastHeartbeatTime() {
		return lastHeartbeatTime;
	}

	/**
	 * 设置最后心跳时间
	 * 
	 * @param lastHeartbeatTime 最后心跳时间
	 */
	public void setLastHeartbeatTime(Long lastHeartbeatTime) {
		this.lastHeartbeatTime = lastHeartbeatTime;
	}

	/**
	 * 获取公司标识
	 * 
	 * @return 公司标识
	 */
	public Long getCompanyId() {
		return companyId;
	}

	/**
	 * 设置公司标识
	 * 
	 * @param templetId 公司标识
	 */
	public void setCompanyId(Long templetId) {
		this.companyId = templetId;
	}

	/**
	 * 获取推送令牌
	 * 
	 * @return 推送令牌
	 */
	public String getPushToken() {
		return pushToken;
	}

	/**
	 * 设置推送令牌
	 * 
	 * @param pushToken 推送令牌
	 */
	public void setPushToken(String pushToken) {
		this.pushToken = pushToken;
	}

	/**
	 * 获取是否root
	 * 
	 * @return 是否root
	 */
	public Boolean getIsRoot() {
		return isRoot;
	}

	/**
	 * 设置是否root
	 * 
	 * @param isRoot 是否root
	 */
	public void setIsRoot(Boolean isRoot) {
		this.isRoot = isRoot;
	}

	/**
	 * 获取直播标识
	 * 
	 * @return 直播标识
	 */
	public Long getLiveId() {
		return liveId;
	}

	/**
	 * 设置直播标识
	 * 
	 * @param liveId 直播标识
	 */
	public void setLiveId(Long liveId) {
		this.liveId = liveId;
	}

	/**
	 * 获取紧急插播标识
	 * 
	 * @return 紧急插播标识
	 */
	public String getEmergentId() {
		return emergentId;
	}

	/**
	 * 设置紧急插播标识
	 * 
	 * @param emergentId 紧急插播标识
	 */
	public void setEmergentId(String emergentId) {
		this.emergentId = emergentId;
	}

	/**
	 * 获取换刊screen标识列表
	 * 
	 * @return 换刊screen标识列表
	 */
	public String getScreenIds() {
		return screenIds;
	}

	/**
	 * 设置换刊screen标识列表
	 * 
	 * @param screenIds 换刊screen标识列表
	 */
	public void setScreenIds(String screenIds) {
		this.screenIds = screenIds;
	}

	/**
	 * 获取媒体状态(0:未开通;1:正常;2:已过期)
	 * 
	 * @return 媒体状态(0:未开通;1:正常;2:已过期)
	 */
	public Short getStatus() {
		return status;
	}

	/**
	 * 设置媒体状态(0:未开通;1:正常;2:已过期)
	 * 
	 * @param status 媒体状态(0:未开通;1:正常;2:已过期)
	 */
	public void setStatus(Short status) {
		this.status = status;
	}

	public Long getLivePlanId() {
		return livePlanId;
	}

	public void setLivePlanId(Long livePlanId) {
		this.livePlanId = livePlanId;
	}

	public Long getJoinId() {
		return joinId;
	}

	public void setJoinId(Long joinId) {
		this.joinId = joinId;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Boolean getIsLive() {
		return isLive;
	}

	public void setIsLive(Boolean isLive) {
		this.isLive = isLive;
	}
	
}
