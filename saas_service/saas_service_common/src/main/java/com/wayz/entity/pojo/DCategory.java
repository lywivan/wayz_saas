package com.wayz.entity.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

/**
 * 行业分类类
 * 
 *  
 * 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DCategory {

	/** 场景标识 */
	private Long id = null;
	/** 场景名称 */
	private String name = null;
	/**公司id*/
	private Long companyId = null;
	/** 描述 */
	private String description = null;
	/** 是否删除 */
	private Boolean isDeleted = null;
	/** 创建时间 */
	private Timestamp createdTime = null;
	/** 修改时间 */
	private Timestamp modifiedTime = null;

}
