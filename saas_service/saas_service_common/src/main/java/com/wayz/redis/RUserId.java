package com.wayz.redis;

/**
 * 用户标识接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
public interface RUserId {

	/**
	 * 递增用户标识
	 * 
	 * @return 用户标识
	 */
	public Long increment();

}
