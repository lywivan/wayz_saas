package com.wayz.redis.impl;

import java.text.MessageFormat;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import com.wayz.redis.RPositionIdPlayerCodeValue;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

/**
 * 媒体位标识播控器编码值实现类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("rPositionIdPlayerCodeValue")
public class RPositionIdPlayerCodeValueImpl implements RPositionIdPlayerCodeValue {

	/** Redis模板 */
	@Resource(name = "redisTemplate")
	private RedisTemplate<String, String> redisTemplate = null;

	/** 键值格式 */
	private static final MessageFormat KEY_FORMAT = new MessageFormat("platform:Value:PositionIdPlayerCode:{0}");

	/**
	 * 设置媒体位标识播控器编码
	 * 
	 * @param id 媒体位标识
	 * @param code 播控器编码
	 */
	@Override
	public void set(Long id, String code) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		redisTemplate.opsForValue().set(key, code);
	}

	/**
	 * 设置媒体位标识播控器编码
	 * 
	 * @param id 媒体位标识
	 * @param code 播控器编码
	 * @param timeout 超时时间
	 * @param unit 超时单位
	 */
	@Override
	public void set(Long id, String code, long timeout, TimeUnit unit) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		redisTemplate.opsForValue().set(key, code, timeout, unit);
	}

	/**
	 * 获取媒体位标识播控器编码
	 * 
	 * @param id 媒体位标识
	 * @return 播控器编码
	 */
	@Override
	public String get(Long id) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		return redisTemplate.opsForValue().get(key);
	}

	/**
	 * 删除媒体位标识播控器编码
	 * 
	 * @param id 媒体位标识
	 */
	@Override
	public void remove(Long id) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		redisTemplate.delete(key);
	}

	/**
	 * 存在媒体位标识播控器编码
	 * 
	 * @param id 媒体位标识
	 * @return 是否存在
	 */
	@Override
	public boolean exist(Long id) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		return redisTemplate.hasKey(key);
	}

	/**
	 * 设置超时时间
	 * 
	 * @param id 媒体位标识
	 * @param timeout 超时时间
	 * @param unit 时间单位
	 */
	@Override
	public void setExpire(Long id, long timeout, TimeUnit unit) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		redisTemplate.expire(key, timeout, unit);
	}

	/**
	 * 获取超时时间
	 * 
	 * @param id 媒体位标识
	 * @param unit 时间单位
	 * @return 超时时间
	 */
	@Override
	public Long getExpire(Long id, TimeUnit unit) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		return redisTemplate.getExpire(key, unit);
	}

}
