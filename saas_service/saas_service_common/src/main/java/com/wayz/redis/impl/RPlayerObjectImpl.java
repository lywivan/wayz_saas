package com.wayz.redis.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.wayz.entity.redis.play.RPlayer;
import com.wayz.redis.RPlayerObject;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

/**
 * 播控器数据对象实现类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("rPlayerObject")
public class RPlayerObjectImpl implements RPlayerObject {

	/** Redis模板 */
	@Resource(name = "redisTemplate")
	private RedisTemplate<String, String> redisTemplate = null;

	/** 键值格式 */
	private static final MessageFormat KEY_FORMAT = new MessageFormat("platform:Object:Player:{0}");

	/**
	 * 设置播控器数据对象
	 * 
	 * @param code 播控器编码
	 * @param player 播控器数据对象
	 */
	@Override
	public void set(String code, RPlayer player) {
		// 初始化
		Map<String, String> map = new HashMap<String, String>();
		String key = KEY_FORMAT.format(new String[] { code });

		// 赋值参数
		if (player.getPositionId() != null) {
			map.put(RPlayer.POSITIONID, player.getPositionId().toString());
		}
		if (player.getGroupId() != null) {
			map.put(RPlayer.GROUPID, player.getGroupId().toString());
		}
		if (player.getHbAlertId() != null) {
			map.put(RPlayer.HBALERTID, player.getHbAlertId().toString());
		}
		if (player.getIsOnline() != null) {
			map.put(RPlayer.ISONLINE, player.getIsOnline().toString());
		}
		if (player.getMenuId() != null) {
			map.put(RPlayer.MENUID, player.getMenuId().toString());
		}
		if (player.getLastHeartbeatTime() != null) {
			map.put(RPlayer.LASTHEARTBEATTIME, player.getLastHeartbeatTime().toString());
		}
		if (player.getCompanyId() != null) {
			map.put(RPlayer.COMPANYID, player.getCompanyId().toString());
		}
		if (player.getPushToken() != null) {
			map.put(RPlayer.PUSHTOKEN, player.getPushToken());
		}
		if (player.getIsRoot() != null) {
			map.put(RPlayer.ISROOT, player.getIsRoot().toString());
		}
		if (player.getLiveId() != null) {
			map.put(RPlayer.LIVEID, player.getLiveId().toString());
		}
		if (player.getEmergentId() != null) {
			map.put(RPlayer.EMERGENTID, player.getEmergentId());
		}
		if (player.getScreenIds() != null) {
			map.put(RPlayer.SCREENIDS, player.getScreenIds());
		}
		else {
			map.remove(RPlayer.SCREENIDS);
		}
		if (player.getStatus() != null) {
			map.put(RPlayer.STATUS, player.getStatus().toString());
		}
		if (player.getLivePlanId() != null) {
			map.put(RPlayer.LIVEPLANID, player.getLivePlanId().toString());
		}
		if (player.getJoinId() != null) {
			map.put(RPlayer.JOINID, player.getJoinId().toString());
		}
		if (player.getSort() != null) {
			map.put(RPlayer.SORT, player.getSort().toString());
		}
		if (player.getIp() != null) {
			map.put(RPlayer.IP, player.getIp());
		}
		if (player.getIsLive() != null) {
			map.put(RPlayer.ISLIVE, player.getIsLive().toString());
		}
		
		// 调用接口
		redisTemplate.opsForHash().putAll(key, map);
	}

	/**
	 * 获取播控器数据对象
	 * 
	 * @param code 播控器编码
	 * @return 播控器数据对象
	 */
	@Override
	public RPlayer get(String code) {
		// 初始化
		RPlayer player = null;
		List<String> keyList = new ArrayList<String>();
		String key = KEY_FORMAT.format(new String[] { code });

		// 赋值参数
		keyList.add(RPlayer.POSITIONID);
		keyList.add(RPlayer.GROUPID);
		keyList.add(RPlayer.HBALERTID);
		keyList.add(RPlayer.ISONLINE);
		keyList.add(RPlayer.MENUID);
		keyList.add(RPlayer.LASTHEARTBEATTIME);
		keyList.add(RPlayer.COMPANYID);
		keyList.add(RPlayer.PUSHTOKEN);
		keyList.add(RPlayer.ISROOT);
		keyList.add(RPlayer.LIVEID);
		keyList.add(RPlayer.EMERGENTID);
		keyList.add(RPlayer.SCREENIDS);
		keyList.add(RPlayer.STATUS);
		keyList.add(RPlayer.LIVEPLANID);
		keyList.add(RPlayer.JOINID);
		keyList.add(RPlayer.SORT);
		keyList.add(RPlayer.IP);
		keyList.add(RPlayer.ISLIVE);

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		List<String> valueList = objectOperations.multiGet(key, keyList);

		// 转化数据
		if (valueList != null && !valueList.isEmpty()) {
			player = new RPlayer();
			String[] valueArray = valueList.toArray(new String[0]);
			int length = valueArray.length;
			if (length > 0 && valueArray[0] != null) {
				player.setPositionId(Long.parseLong(valueArray[0]));
			}
			if (length > 1 && valueArray[1] != null) {
				player.setGroupId(Long.parseLong(valueArray[1]));
			}
			if (length > 2 && valueArray[2] != null) {
				player.setHbAlertId(Long.parseLong(valueArray[2]));
			}
			if (length > 3 && valueArray[3] != null) {
				player.setIsOnline(Boolean.parseBoolean(valueArray[3]));
			}
			if (length > 4 && valueArray[4] != null) {
				player.setMenuId(Long.parseLong(valueArray[4]));
			}
			if (length > 5 && valueArray[5] != null) {
				player.setLastHeartbeatTime(Long.parseLong(valueArray[5]));
			}
			if (length > 6 && valueArray[6] != null) {
				player.setCompanyId(Long.parseLong(valueArray[6]));
			}
			if (length > 7 && valueArray[7] != null) {
				player.setPushToken(valueArray[7]);
			}
			if (length > 8 && valueArray[8] != null) {
				player.setIsRoot(Boolean.parseBoolean(valueArray[8]));
			}
			if (length > 9 && valueArray[9] != null) {
				player.setLiveId(Long.parseLong(valueArray[9]));
			}
			if (length > 10 && valueArray[10] != null) {
				player.setEmergentId(valueArray[10]);
			}
			if (length > 11 && valueArray[11] != null) {
				player.setScreenIds(valueArray[11]);
			}
			if (length > 12 && valueArray[12] != null) {
				player.setStatus(Short.parseShort(valueArray[12]));
			}
			if (length > 13 && valueArray[13] != null) {
				player.setLivePlanId(Long.parseLong(valueArray[13]));
			}
			if (length > 14 && valueArray[14] != null) {
				player.setJoinId(Long.parseLong(valueArray[14]));
			}
			if (length > 15 && valueArray[15] != null) {
				player.setSort(Integer.parseInt(valueArray[15]));
			}
			if (length > 16 && valueArray[16] != null) {
				player.setIp(valueArray[16]);
			}
			if (length > 17 && valueArray[17] != null) {
				player.setIsLive(Boolean.parseBoolean(valueArray[17]));
			}
		}

		// 返回数据
		return player;
	}

	/**
	 * 删除播控器数据对象
	 * 
	 * @param code 播控器编码
	 */
	@Override
	public void remove(String code) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { code });

		// 调用接口
		redisTemplate.delete(key);
	}

	/**
	 * 存在播控器数据对象
	 * 
	 * @param code 播控器编码
	 * @return 是否存在
	 */
	@Override
	public boolean exist(String code) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { code });

		// 调用接口
		return redisTemplate.hasKey(key);
	}

	/**
	 * 设置字符串值
	 * 
	 * @param code 播控器编码
	 * @param name 参数名称
	 * @param value 字符串值
	 */
	@Override
	public void setString(String code, String name, String value) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { code });

		// 调用接口
		redisTemplate.opsForHash().put(key, name, value);
	}

	/**
	 * 获取字符串值
	 * 
	 * @param code 播控器编码
	 * @param name 参数名称
	 * @return 字符串值
	 */
	@Override
	public String getString(String code, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { code });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		return objectOperations.get(key, name);
	}

	/**
	 * 设置整型值
	 * 
	 * @param code 播控器编码
	 * @param name 参数名称
	 * @param value 整型值
	 */
	@Override
	public void setInteger(String code, String name, Integer value) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { code });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取整型值
	 * 
	 * @param code 播控器编码
	 * @param name 参数名称
	 * @return 整型值
	 */
	@Override
	public Integer getInteger(String code, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { code });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Integer.parseInt(value);
		}
		return null;
	}

	/**
	 * 设置短整型值
	 * 
	 * @param code 播控器编码
	 * @param name 参数名称
	 * @param value 短整型值
	 */
	@Override
	public void setShort(String code, String name, Short value) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { code });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取短整型值
	 * 
	 * @param code 播控器编码
	 * @param name 参数名称
	 * @return 短整型值
	 */
	@Override
	public Short getShort(String code, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { code });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Short.parseShort(value);
		}
		return null;
	}

	/**
	 * 设置长整型值
	 * 
	 * @param code 播控器编码
	 * @param name 参数名称
	 * @param value 长整型值
	 */
	@Override
	public void setLong(String code, String name, Long value) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { code });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取长整型值
	 * 
	 * @param code 播控器编码
	 * @param name 参数名称
	 * @return 长整型值
	 */
	@Override
	public Long getLong(String code, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { code });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Long.parseLong(value);
		}
		return null;
	}

	/**
	 * 设置浮点型值
	 * 
	 * @param code 播控器编码
	 * @param name 参数名称
	 * @param value 浮点型值
	 */
	@Override
	public void setFloat(String code, String name, Float value) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { code });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取浮点型值
	 * 
	 * @param code 播控器编码
	 * @param name 参数名称
	 * @return 浮点型值
	 */
	@Override
	public Float getFloat(String code, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { code });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Float.parseFloat(value);
		}
		return null;
	}

	/**
	 * 设置双精度浮点型值
	 * 
	 * @param code 播控器编码
	 * @param name 参数名称
	 * @param value 双精度浮点型值
	 */
	@Override
	public void setDouble(String code, String name, Double value) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { code });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取双精度浮点型值
	 * 
	 * @param code 播控器编码
	 * @param name 参数名称
	 * @return 双精度浮点型值
	 */
	@Override
	public Double getDouble(String code, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { code });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Double.parseDouble(value);
		}
		return null;
	}

	/**
	 * 删除参数
	 * 
	 * @param code 播控器编码
	 * @param name 参数名称
	 */
	@Override
	public void remove(String code, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { code });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		objectOperations.delete(key, name);
	}

	/**
	 * 存在参数
	 * 
	 * @param code 播控器编码
	 * @param name 参数名称
	 * @return 持久化异常
	 */
	@Override
	public boolean exist(String code, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { code });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		return objectOperations.hasKey(key, name);
	}

	/**
	 * 获取bool型值
	 * 
	 * @param code 播控器编码
	 * @param name 参数名称
	 * @return 短整型值
	 */
	@Override
	public void setBoolean(String code, String name, Boolean value) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { code });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 设置bool型值
	 * 
	 * @param code 播控器编码
	 * @param name 参数名称
	 * @param value 短整型值
	 */
	@Override
	public Boolean getBoolean(String code, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { code });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Boolean.parseBoolean(value);
		}
		return null;
	}

}
