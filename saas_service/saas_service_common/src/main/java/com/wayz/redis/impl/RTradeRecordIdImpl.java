package com.wayz.redis.impl;

import javax.annotation.Resource;

import com.wayz.redis.RTradeRecordId;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

/**
 * 用户交易记录标识实现类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("rTradeRecordId")
public class RTradeRecordIdImpl implements RTradeRecordId {

	/** Redis模板 */
	@Resource(name = "redisTemplate")
	private RedisTemplate<String, String> redisTemplate = null;

	/** 键值格式 */
	private static final String KEY = "platform:Id:TradeRecord";

	/**
	 * 递增用户交易记录标识
	 * 
	 * @return 用户交易记录标识
	 */
	@Override
	public Long increment() {
		return redisTemplate.opsForValue().increment(KEY, 1L);
	}

}
