package com.wayz.redis.impl;

import com.wayz.entity.redis.user.RUser;
import com.wayz.redis.RUserObject;
import com.wayz.util.CollectionUtils;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户对象实现类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("rUserObject")
public class RUserObjectImpl implements RUserObject {

	/** Redis模板 */
	@Resource(name = "redisTemplate")
	private RedisTemplate<String, String> redisTemplate = null;

	/** 键值格式 */
	private static final MessageFormat KEY_FORMAT = new MessageFormat("yunge:Object:User:{0}");

	/**
	 * 设置用户对象
	 * 
	 * @param id 用户标识
	 * @param user 用户对象
	 */
	@Override
	public void set(Long id, RUser user) {
		// 初始化
		Map<String, String> map = new HashMap<String, String>();
		String key = KEY_FORMAT.format(new Long[] { id });

		// 赋值参数
		if (user.getRoleId() != null) {
			map.put(RUser.ROLEID, user.getRoleId().toString());
		}
		if (user.getType() != null) {
			map.put(RUser.TYPE, user.getType().toString());
		}
		if (user.getCompanyId() != null) {
			map.put(RUser.COMPANYID, user.getCompanyId().toString());
		}
		if (user.getStatus() != null) {
			map.put(RUser.STATUS, user.getStatus().toString());
		}
		if (user.getUsername() != null) {
			map.put(RUser.USERNAME, user.getUsername());
		}
		if (user.getName() != null) {
			map.put(RUser.NAME, user.getName());
		}
		if (user.getPhone() != null) {
			map.put(RUser.PHONE, user.getPhone());
		}
		if (user.getTitle() != null) {
			map.put(RUser.TITLE, user.getTitle());
		}
		if (user.getEmail() != null) {
			map.put(RUser.EMAIL, user.getEmail());
		}
		if (user.getAvatar() != null) {
			map.put(RUser.AVATAR, user.getAvatar());
		}
		if (user.getDepartId() != null) {
			map.put(RUser.DEPARTID, user.getDepartId().toString());
		}
		if (user.getDepartPath() != null) {
			map.put(RUser.DEPARTPATH, user.getDepartPath());
		}
		// 调用接口
		redisTemplate.opsForHash().putAll(key, map);
	}

	/**
	 * 获取用户对象
	 * 
	 * @param id 用户标识
	 * @return 用户对象
	 */
	@Override
	public RUser get(Long id) {
		// 初始化
		RUser user = null;
		List<String> keyList = new ArrayList<String>();
		String key = KEY_FORMAT.format(new Long[] { id });

		// 赋值参数
		keyList.add(RUser.ROLEID);
		keyList.add(RUser.TYPE);
		keyList.add(RUser.COMPANYID);
		keyList.add(RUser.STATUS);
		keyList.add(RUser.USERNAME);
		keyList.add(RUser.NAME);
		keyList.add(RUser.PHONE);
		keyList.add(RUser.TITLE);
		keyList.add(RUser.EMAIL);
		keyList.add(RUser.AVATAR);
		keyList.add(RUser.DEPARTID);
		keyList.add(RUser.DEPARTPATH);

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		List<String> valueList = objectOperations.multiGet(key, keyList);

		// 转化数据
		if (CollectionUtils.isNotEmpty(valueList)) {
			user = new RUser();
			String[] valueArray = valueList.toArray(new String[0]);
			int length = valueArray.length;
			if (length > 0 && valueArray[0] != null) {
				user.setRoleId(Long.parseLong(valueArray[0]));
			}
			if (length > 1 && valueArray[1] != null) {
				user.setType(Short.parseShort(valueArray[1]));
			}
			if (length > 2 && valueArray[2] != null) {
				user.setCompanyId(Long.parseLong(valueArray[2]));
			}
			if (length > 3 && valueArray[3] != null) {
				user.setStatus(Short.parseShort(valueArray[3]));
			}
			if (length > 4 && valueArray[4] != null) {
				user.setUsername(valueArray[4]);
			}
			if (length > 5 && valueArray[5] != null) {
				user.setName(valueArray[5]);
			}
			if (length > 6 && valueArray[6] != null) {
				user.setPhone(valueArray[6]);
			}
			if (length > 7 && valueArray[7] != null) {
				user.setTitle(valueArray[7]);
			}
			if (length > 8 && valueArray[8] != null) {
				user.setEmail(valueArray[8]);
			}
			if (length > 9 && valueArray[9] != null) {
				user.setAvatar(valueArray[9]);
			}
			if (length > 10 && valueArray[10] != null) {
				user.setDepartId(Long.parseLong(valueArray[10]));
			}
			if (length > 11 && valueArray[11] != null) {
				user.setDepartPath(valueArray[11]);
			}

		}

		// 返回数据
		return user;
	}

	/**
	 * 删除用户对象
	 * 
	 * @param id 用户标识
	 */
	@Override
	public void remove(Long id) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		redisTemplate.delete(key);
	}

	/**
	 * 存在用户对象
	 * 
	 * @param id 用户标识
	 * @return 是否存在
	 */
	@Override
	public boolean exist(Long id) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		return redisTemplate.hasKey(key);
	}

	/**
	 * 设置字符串值
	 * 
	 * @param id 用户标识
	 * @param name 参数名称
	 * @param value 字符串值
	 */
	@Override
	public void setString(Long id, String name, String value) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		redisTemplate.opsForHash().put(key, name, value);
	}

	/**
	 * 获取字符串值
	 * 
	 * @param id 用户标识
	 * @param name 参数名称
	 * @return 字符串值
	 */
	@Override
	public String getString(Long id, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		return objectOperations.get(key, name);
	}

	/**
	 * 设置整型值
	 * 
	 * @param id 用户标识
	 * @param name 参数名称
	 * @param value 整型值
	 */
	@Override
	public void setInteger(Long id, String name, Integer value) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取整型值
	 * 
	 * @param id 用户标识
	 * @param name 参数名称
	 * @return 整型值
	 */
	@Override
	public Integer getInteger(Long id, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Integer.parseInt(value);
		}
		return null;
	}

	/**
	 * 设置短整型值
	 * 
	 * @param id 用户标识
	 * @param name 参数名称
	 * @param value 短整型值
	 */
	@Override
	public void setShort(Long id, String name, Short value) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取短整型值
	 * 
	 * @param id 用户标识
	 * @param name 参数名称
	 * @return 短整型值
	 */
	@Override
	public Short getShort(Long id, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Short.parseShort(value);
		}
		return null;
	}

	/**
	 * 设置长整型值
	 * 
	 * @param id 用户标识
	 * @param name 参数名称
	 * @param value 长整型值
	 */
	@Override
	public void setLong(Long id, String name, Long value) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取长整型值
	 * 
	 * @param id 用户标识
	 * @param name 参数名称
	 * @return 长整型值
	 */
	@Override
	public Long getLong(Long id, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Long.parseLong(value);
		}
		return null;
	}

	/**
	 * 设置浮点型值
	 * 
	 * @param id 用户标识
	 * @param name 参数名称
	 * @param value 浮点型值
	 */
	@Override
	public void setFloat(Long id, String name, Float value) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取浮点型值
	 * 
	 * @param id 用户标识
	 * @param name 参数名称
	 * @return 浮点型值
	 */
	@Override
	public Float getFloat(Long id, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Float.parseFloat(value);
		}
		return null;
	}

	/**
	 * 设置双精度浮点型值
	 * 
	 * @param id 用户标识
	 * @param name 参数名称
	 * @param value 双精度浮点型值
	 */
	@Override
	public void setDouble(Long id, String name, Double value) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取双精度浮点型值
	 * 
	 * @param id 用户标识
	 * @param name 参数名称
	 * @return 双精度浮点型值
	 */
	@Override
	public Double getDouble(Long id, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Double.parseDouble(value);
		}
		return null;
	}

	/**
	 * 删除参数
	 * 
	 * @param id 用户标识
	 * @param name 参数名称
	 */
	@Override
	public void remove(Long id, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		objectOperations.delete(key, name);
	}

	/**
	 * 存在参数
	 * 
	 * @param id 用户标识
	 * @param name 参数名称
	 * @return 持久化异常
	 */
	@Override
	public boolean exist(Long id, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		return objectOperations.hasKey(key, name);
	}

}
