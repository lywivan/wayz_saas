package com.wayz.redis;

import com.wayz.entity.redis.play.RPlayer;

/**
 * 播控器数据对象接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
public interface RPlayerObject {

	/**
	 * 设置播控器数据对象
	 * 
	 * @param code 播控器编码
	 * @param player 播控器数据对象
	 */
	public void set(String code, RPlayer player);

	/**
	 * 获取播控器数据对象
	 * 
	 * @param code 播控器编码
	 * @return 播控器数据对象
	 */
	public RPlayer get(String code);

	/**
	 * 删除播控器数据对象
	 * 
	 * @param code 播控器编码
	 */
	public void remove(String code);

	/**
	 * 存在播控器数据对象
	 * 
	 * @param code 播控器编码
	 * @return 是否存在
	 */
	public boolean exist(String code);

	/**
	 * 设置字符串值
	 * 
	 * @param code 播控器编码
	 * @param name 参数名称
	 * @param value 字符串值
	 */
	public void setString(String code, String name, String value);

	/**
	 * 获取字符串值
	 * 
	 * @param code 播控器编码
	 * @param name 参数名称
	 * @return 字符串值
	 */
	public String getString(String code, String name);

	/**
	 * 设置整型值
	 * 
	 * @param code 播控器编码
	 * @param name 参数名称
	 * @param value 整型值
	 */
	public void setInteger(String code, String name, Integer value);

	/**
	 * 获取整型值
	 * 
	 * @param code 播控器编码
	 * @param name 参数名称
	 * @return 整型值
	 */
	public Integer getInteger(String code, String name);

	/**
	 * 设置短整型值
	 * 
	 * @param code 播控器编码
	 * @param name 参数名称
	 * @param value 短整型值
	 */
	public void setShort(String code, String name, Short value);

	/**
	 * 获取短整型值
	 * 
	 * @param code 播控器编码
	 * @param name 参数名称
	 * @return 短整型值
	 */
	public Short getShort(String code, String name);

	/**
	 * 设置bool型值
	 * 
	 * @param code 播控器编码
	 * @param name 参数名称
	 * @param value 短整型值
	 */
	public void setBoolean(String code, String name, Boolean value);

	/**
	 * 获取bool型值
	 * 
	 * @param code 播控器编码
	 * @param name 参数名称
	 * @return 短整型值
	 */
	public Boolean getBoolean(String code, String name);

	/**
	 * 设置长整型值
	 * 
	 * @param code 播控器编码
	 * @param name 参数名称
	 * @param value 长整型值
	 */
	public void setLong(String code, String name, Long value);

	/**
	 * 获取长整型值
	 * 
	 * @param code 播控器编码
	 * @param name 参数名称
	 * @return 长整型值
	 */
	public Long getLong(String code, String name);

	/**
	 * 设置浮点型值
	 * 
	 * @param code 播控器编码
	 * @param name 参数名称
	 * @param value 浮点型值
	 */
	public void setFloat(String code, String name, Float value);

	/**
	 * 获取浮点型值
	 * 
	 * @param code 播控器编码
	 * @param name 参数名称
	 * @return 浮点型值
	 */
	public Float getFloat(String code, String name);

	/**
	 * 设置双精度浮点型值
	 * 
	 * @param code 播控器编码
	 * @param name 参数名称
	 * @param value 双精度浮点型值
	 */
	public void setDouble(String code, String name, Double value);

	/**
	 * 获取双精度浮点型值
	 * 
	 * @param code 播控器编码
	 * @param name 参数名称
	 * @return 双精度浮点型值
	 */
	public Double getDouble(String code, String name);

	/**
	 * 删除参数
	 * 
	 * @param code 播控器编码
	 * @param name 参数名称
	 */
	public void remove(String code, String name);

	/**
	 * 存在参数
	 * 
	 * @param code 播控器编码
	 * @param name 参数名称
	 * @return 持久化异常
	 */
	public boolean exist(String code, String name);

}
