package com.wayz.redis.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.wayz.entity.redis.company.RDomainLogo;
import com.wayz.redis.RDomainLogoObject;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

/**
 * 域名logo对象实现类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("rDomainLogoObject")
public class RDomainLogoObjectImpl implements RDomainLogoObject {

	/** Redis模板 */
	@Resource(name = "redisTemplate")
	private RedisTemplate<String, String> redisTemplate = null;

	/** 键值格式 */
	private static final MessageFormat KEY_FORMAT = new MessageFormat("platform:Object:DomainLogo:{0}");

	/**
	 * 设置域名logo对象
	 * 
	 * @param domainName 域名
	 * @param domainLogo 域名logo对象
	 */
	@Override
	public void set(String domainName, RDomainLogo domainLogo) {
		// 初始化
		Map<String, String> map = new HashMap<String, String>();
		String key = KEY_FORMAT.format(new String[] { domainName });

		// 赋值参数
		if (domainLogo.getDomainName() != null) {
			map.put(RDomainLogo.DOMAINNAME, domainLogo.getDomainName());
		}
		if (domainLogo.getLogo() != null) {
			map.put(RDomainLogo.LOGO, domainLogo.getLogo());
		}
		if (domainLogo.getWebsiteTitle() != null) {
			map.put(RDomainLogo.WEBSITETITLE, domainLogo.getWebsiteTitle());
		}

		// 调用接口
		redisTemplate.opsForHash().putAll(key, map);
	}

	/**
	 * 获取域名logo对象
	 * 
	 * @param domainName 域名
	 * @return 域名logo对象
	 */
	@Override
	public RDomainLogo get(String domainName) {
		// 初始化
		RDomainLogo domainLogo = null;
		List<String> keyList = new ArrayList<String>();
		String key = KEY_FORMAT.format(new String[] { domainName });

		// 赋值参数
		keyList.add(RDomainLogo.DOMAINNAME);
		keyList.add(RDomainLogo.LOGO);
		keyList.add(RDomainLogo.WEBSITETITLE);

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		List<String> valueList = objectOperations.multiGet(key, keyList);

		// 转化数据
		if (valueList != null && !valueList.isEmpty()) {
			domainLogo = new RDomainLogo();
			String[] valueArray = valueList.toArray(new String[0]);
			int length = valueArray.length;
			if (length > 0 && valueArray[0] != null) {
				domainLogo.setDomainName(valueArray[0]);
			}
			if (length > 1 && valueArray[1] != null) {
				domainLogo.setLogo(valueArray[1]);
			}
			if (length > 2 && valueArray[2] != null) {
				domainLogo.setWebsiteTitle(valueArray[2]);
			}
		}

		// 返回数据
		return domainLogo;
	}

	/**
	 * 删除域名logo对象
	 * 
	 * @param domainName 域名
	 */
	@Override
	public void remove(String domainName) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { domainName });

		// 调用接口
		redisTemplate.delete(key);
	}

	/**
	 * 存在域名logo对象
	 * 
	 * @param domainName 域名
	 * @return 是否存在
	 */
	@Override
	public boolean exist(String domainName) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { domainName });

		// 调用接口
		return redisTemplate.hasKey(key);
	}

	/**
	 * 设置字符串值
	 * 
	 * @param domainName 域名
	 * @param name 参数名称
	 * @param value 字符串值
	 */
	@Override
	public void setString(String domainName, String name, String value) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { domainName });

		// 调用接口
		redisTemplate.opsForHash().put(key, name, value);
	}

	/**
	 * 获取字符串值
	 * 
	 * @param domainName 域名
	 * @param name 参数名称
	 * @return 字符串值
	 */
	@Override
	public String getString(String domainName, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { domainName });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		return objectOperations.get(key, name);
	}

	/**
	 * 设置整型值
	 * 
	 * @param domainName 域名
	 * @param name 参数名称
	 * @param value 整型值
	 */
	@Override
	public void setInteger(String domainName, String name, Integer value) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { domainName });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取整型值
	 * 
	 * @param domainName 域名
	 * @param name 参数名称
	 * @return 整型值
	 */
	@Override
	public Integer getInteger(String domainName, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { domainName });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Integer.parseInt(value);
		}
		return null;
	}

	/**
	 * 设置短整型值
	 * 
	 * @param domainName 域名
	 * @param name 参数名称
	 * @param value 短整型值
	 */
	@Override
	public void setShort(String domainName, String name, Short value) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { domainName });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取短整型值
	 * 
	 * @param domainName 域名
	 * @param name 参数名称
	 * @return 短整型值
	 */
	@Override
	public Short getShort(String domainName, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { domainName });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Short.parseShort(value);
		}
		return null;
	}

	/**
	 * 设置长整型值
	 * 
	 * @param domainName 域名
	 * @param name 参数名称
	 * @param value 长整型值
	 */
	@Override
	public void setLong(String domainName, String name, Long value) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { domainName });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取长整型值
	 * 
	 * @param domainName 域名
	 * @param name 参数名称
	 * @return 长整型值
	 */
	@Override
	public Long getLong(String domainName, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { domainName });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Long.parseLong(value);
		}
		return null;
	}

	/**
	 * 设置浮点型值
	 * 
	 * @param domainName 域名
	 * @param name 参数名称
	 * @param value 浮点型值
	 */
	@Override
	public void setFloat(String domainName, String name, Float value) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { domainName });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取浮点型值
	 * 
	 * @param domainName 域名
	 * @param name 参数名称
	 * @return 浮点型值
	 */
	@Override
	public Float getFloat(String domainName, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { domainName });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Float.parseFloat(value);
		}
		return null;
	}

	/**
	 * 设置双精度浮点型值
	 * 
	 * @param domainName 域名
	 * @param name 参数名称
	 * @param value 双精度浮点型值
	 */
	@Override
	public void setDouble(String domainName, String name, Double value) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { domainName });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取双精度浮点型值
	 * 
	 * @param domainName 域名
	 * @param name 参数名称
	 * @return 双精度浮点型值
	 */
	@Override
	public Double getDouble(String domainName, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { domainName });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Double.parseDouble(value);
		}
		return null;
	}

	/**
	 * 删除参数
	 * 
	 * @param domainName 域名
	 * @param name 参数名称
	 */
	@Override
	public void remove(String domainName, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { domainName });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		objectOperations.delete(key, name);
	}

	/**
	 * 存在参数
	 * 
	 * @param domainName 域名
	 * @param name 参数名称
	 * @return 持久化异常
	 */
	@Override
	public boolean exist(String domainName, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new String[] { domainName });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		return objectOperations.hasKey(key, name);
	}

}
