package com.wayz.redis.impl;

import javax.annotation.Resource;

import com.wayz.redis.RUserId;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

/**
 * 用户标识实现类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("rUserId")
public class RUserIdImpl implements RUserId {

	/** Redis模板 */
	@Resource(name = "redisTemplate")
	private RedisTemplate<String, String> redisTemplate = null;
	/** 键值格式 */
	private static final String KEY = "platform:Id:Saas:User";

	/**
	 * 递增用户标识
	 * 
	 * @return 用户标识
	 */
	@Override
	public Long increment() {
		return redisTemplate.opsForValue().increment(KEY, 1L);
	}

}
