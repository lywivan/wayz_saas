package com.wayz.redis;

import com.wayz.entity.redis.company.RDepartment;

/**
 * 部门对象接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
public interface RDepartmentObject {

	/**
	 * 设置部门对象
	 * 
	 * @param companyId 公司标识
	 * @param department 部门对象
	 */
	public void set(Long companyId, Long id, RDepartment department);

	/**
	 * 获取部门对象
	 * 
	 * @param companyId 公司标识
	 * @return 部门对象
	 */
	public RDepartment get(Long companyId, Long id);

	/**
	 * 删除部门对象
	 * 
	 * @param companyId 公司标识
	 */
	public void remove(Long companyId, Long id);

	/**
	 * 存在部门对象
	 * 
	 * @param companyId 公司标识
	 * @return 是否存在
	 */
	public boolean exist(Long companyId, Long id);

	/**
	 * 设置字符串值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @param value 字符串值
	 */
	public void setString(Long companyId, Long id, String name, String value);

	/**
	 * 获取字符串值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @return 字符串值
	 */
	public String getString(Long companyId, Long id, String name);

	/**
	 * 设置整型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @param value 整型值
	 */
	public void setInteger(Long companyId, Long id, String name, Integer value);

	/**
	 * 获取整型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @return 整型值
	 */
	public Integer getInteger(Long companyId, Long id, String name);

	/**
	 * 设置短整型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @param value 短整型值
	 */
	public void setShort(Long companyId, Long id, String name, Short value);

	/**
	 * 获取短整型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @return 短整型值
	 */
	public Short getShort(Long companyId, Long id, String name);

	/**
	 * 设置长整型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @param value 长整型值
	 */
	public void setLong(Long companyId, Long id, String name, Long value);

	/**
	 * 获取长整型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @return 长整型值
	 */
	public Long getLong(Long companyId, Long id, String name);

	/**
	 * 设置浮点型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @param value 浮点型值
	 */
	public void setFloat(Long companyId, Long id, String name, Float value);

	/**
	 * 获取浮点型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @return 浮点型值
	 */
	public Float getFloat(Long companyId, Long id, String name);

	/**
	 * 设置双精度浮点型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @param value 双精度浮点型值
	 */
	public void setDouble(Long companyId, Long id, String name, Double value);

	/**
	 * 获取双精度浮点型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @return 双精度浮点型值
	 */
	public Double getDouble(Long companyId, Long id, String name);

	/**
	 * 删除参数
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 */
	public void remove(Long companyId, Long id, String name);

	/**
	 * 存在参数
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @return 持久化异常
	 */
	public boolean exist(Long companyId, Long id, String name);

}
