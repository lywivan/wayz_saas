package com.wayz.redis;

/**
 * 用户交易记录标识接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
public interface RTradeRecordId {

	/**
	 * 递增用户交易记录标识
	 * 
	 * @return 用户交易记录标识
	 */
	public Long increment();

}
