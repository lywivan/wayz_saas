package com.wayz.redis.impl;

import com.wayz.entity.redis.company.RPositionGroup;
import com.wayz.redis.RPositionGroupObject;
import com.wayz.util.CollectionUtils;
import com.wayz.util.StringUtil;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 媒体分组数据对象实现类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("rPositionGroupObject")
public class RPositionGroupObjectImpl implements RPositionGroupObject {

	/** Redis模板 */
	@Resource(name = "redisTemplate")
	private RedisTemplate<String, String> redisTemplate = null;

	/** 键值格式 */
	private static final MessageFormat KEY_FORMAT = new MessageFormat("platform:Object:PositionGroup:{0}");

	/**
	 * 设置媒体分组数据对象
	 * 
	 * @param id 媒体分组标识
	 * @param rPositionGroup 媒体分组数据对象
	 */
	@Override
	public void set(Long id, RPositionGroup rPositionGroup) {
		// 初始化
		Map<String, String> map = new HashMap<String, String>();
		String key = KEY_FORMAT.format(new Long[] { id });

		// 赋值参数
		if (rPositionGroup.getCompanyId() != null) {
			map.put(RPositionGroup.COMPANYID, rPositionGroup.getCompanyId().toString());
		}
		if (rPositionGroup.getName() != null) {
			map.put(RPositionGroup.NAME, rPositionGroup.getName());
		}
		if (rPositionGroup.getPhone() != null) {
			map.put(RPositionGroup.PHONE, rPositionGroup.getPhone());
		}
		if (rPositionGroup.getStartTime() != null) {
			map.put(RPositionGroup.STARTTIME, rPositionGroup.getStartTime());
		}
		if (rPositionGroup.getCloseTime() != null) {
			map.put(RPositionGroup.CLOSETIME, rPositionGroup.getCloseTime());
		}
		if (rPositionGroup.getVolumn() != null) {
			map.put(RPositionGroup.VOLUMN, rPositionGroup.getVolumn().toString());
		}
		if (rPositionGroup.getRotation() != null) {
			map.put(RPositionGroup.ROTATION, rPositionGroup.getRotation().toString());
		}
		if (rPositionGroup.getIsAutoStart() != null) {
			map.put(RPositionGroup.ISAUTOSTART, rPositionGroup.getIsAutoStart().toString());
		}
		if (rPositionGroup.getSubtitleTempletId() != null) {
			map.put(RPositionGroup.SUBTITLETEMPLETID, String.valueOf(rPositionGroup.getSubtitleTempletId()));
		}else{
			map.put(RPositionGroup.SUBTITLETEMPLETID, "");
		}
		if (!StringUtil.isEmpty(rPositionGroup.getPlayVersion())) {
			map.put(RPositionGroup.PLAYVERSION, rPositionGroup.getPlayVersion());
		}
		else {
			map.put(RPositionGroup.PLAYVERSION, "");
		}
		if (rPositionGroup.getPlayReserve() != null) {
			map.put(RPositionGroup.PLAYRESERVE, rPositionGroup.getPlayReserve());
		}
		else {
			map.put(RPositionGroup.PLAYRESERVE, "");
		}
		if (rPositionGroup.getAutoOnOff() != null) {
			map.put(RPositionGroup.AUTOONOFF, rPositionGroup.getAutoOnOff().toString());
		}
		if (rPositionGroup.getDepartPath() != null) {
			map.put(RPositionGroup.DEPARTPATH, rPositionGroup.getDepartPath());
		}
		if (rPositionGroup.getOpenReserve() != null) {
			map.put(RPositionGroup.OPENRESERVE, rPositionGroup.getOpenReserve());
		}
		else {
			map.put(RPositionGroup.OPENRESERVE, "");
		}
		
		// 调用接口
		redisTemplate.opsForHash().putAll(key, map);
	}

	/**
	 * 获取媒体分组数据对象
	 * 
	 * @param id 媒体分组标识
	 * @return 媒体分组数据对象
	 */
	@Override
	public RPositionGroup get(Long id) {
		// 初始化
		RPositionGroup playerStation = null;
		List<String> keyList = new ArrayList<String>();
		String key = KEY_FORMAT.format(new Long[] { id });

		// 赋值参数
		keyList.add(RPositionGroup.COMPANYID);
		keyList.add(RPositionGroup.NAME);
		keyList.add(RPositionGroup.PHONE);
		keyList.add(RPositionGroup.STARTTIME);
		keyList.add(RPositionGroup.CLOSETIME);
		keyList.add(RPositionGroup.VOLUMN);
		keyList.add(RPositionGroup.ROTATION);
		keyList.add(RPositionGroup.ISAUTOSTART);
		keyList.add(RPositionGroup.SUBTITLETEMPLETID);
		keyList.add(RPositionGroup.PLAYVERSION);
		keyList.add(RPositionGroup.PLAYRESERVE);
		keyList.add(RPositionGroup.AUTOONOFF);
		keyList.add(RPositionGroup.DEPARTPATH);
		keyList.add(RPositionGroup.OPENRESERVE);

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		List<String> valueList = objectOperations.multiGet(key, keyList);

		// 转化数据
		if (CollectionUtils.isNotEmpty(valueList)) {
			playerStation = new RPositionGroup();
			String[] valueArray = valueList.toArray(new String[0]);
			int length = valueArray.length;
			if (length > 0 && valueArray[0] != null) {
				playerStation.setCompanyId(Long.parseLong(valueArray[0]));
			}
			if (length > 1 && valueArray[1] != null) {
				playerStation.setName(valueArray[1]);
			}
			if (length > 2 && valueArray[2] != null) {
				playerStation.setPhone(valueArray[2]);
			}
			if (length > 3 && valueArray[3] != null) {
				playerStation.setStartTime(valueArray[3]);
			}
			if (length > 4 && valueArray[4] != null) {
				playerStation.setCloseTime(valueArray[4]);
			}
			if (length > 5 && valueArray[5] != null) {
				playerStation.setVolumn(Short.parseShort(valueArray[5]));
			}
			if (length > 6 && valueArray[6] != null) {
				playerStation.setRotation(Integer.parseInt(valueArray[6]));
			}
			if (length > 7 && valueArray[7] != null) {
				playerStation.setIsAutoStart(Boolean.parseBoolean(valueArray[7]));
			}
			if (length > 8 && !StringUtil.isEmpty(valueArray[8]) ) {
				playerStation.setSubtitleTempletId(Long.parseLong(valueArray[8]));
			}
			if (length > 9 && valueArray[9] != null) {
				playerStation.setPlayVersion(valueArray[9]);
			}
			if (length > 10 && valueArray[10] != null) {
				playerStation.setPlayReserve(valueArray[10]);
			}
			if (length > 11 && valueArray[11] != null) {
				playerStation.setAutoOnOff(Boolean.parseBoolean(valueArray[11]));
			}
			if (length > 12 && valueArray[12] != null) {
				playerStation.setDepartPath(valueArray[12]);
			}
			if (length > 13 && valueArray[13] != null) {
				playerStation.setOpenReserve(valueArray[13]);
			}
		}

		// 返回数据
		return playerStation;
	}

	/**
	 * 删除媒体分组数据对象
	 * 
	 * @param id 媒体分组标识
	 */
	@Override
	public void remove(Long id) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		redisTemplate.delete(key);
	}

	/**
	 * 存在媒体分组数据对象
	 * 
	 * @param id 媒体分组标识
	 * @return 是否存在
	 */
	@Override
	public boolean exist(Long id) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		return redisTemplate.hasKey(key);
	}

	/**
	 * 设置字符串值
	 * 
	 * @param id 媒体分组标识
	 * @param name 参数名称
	 * @param value 字符串值
	 */
	@Override
	public void setString(Long id, String name, String value) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		redisTemplate.opsForHash().put(key, name, value);
	}

	/**
	 * 获取字符串值
	 * 
	 * @param id 媒体分组标识
	 * @param name 参数名称
	 * @return 字符串值
	 */
	@Override
	public String getString(Long id, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		return objectOperations.get(key, name);
	}

	/**
	 * 设置整型值
	 * 
	 * @param id 媒体分组标识
	 * @param name 参数名称
	 * @param value 整型值
	 */
	@Override
	public void setInteger(Long id, String name, Integer value) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取整型值
	 * 
	 * @param id 媒体分组标识
	 * @param name 参数名称
	 * @return 整型值
	 */
	@Override
	public Integer getInteger(Long id, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Integer.parseInt(value);
		}
		return null;
	}

	/**
	 * 设置短整型值
	 * 
	 * @param id 媒体分组标识
	 * @param name 参数名称
	 * @param value 短整型值
	 */
	@Override
	public void setShort(Long id, String name, Short value) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取短整型值
	 * 
	 * @param id 媒体分组标识
	 * @param name 参数名称
	 * @return 短整型值
	 */
	@Override
	public Short getShort(Long id, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Short.parseShort(value);
		}
		return null;
	}

	/**
	 * 设置长整型值
	 * 
	 * @param id 媒体分组标识
	 * @param name 参数名称
	 * @param value 长整型值
	 */
	@Override
	public void setLong(Long id, String name, Long value) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取长整型值
	 * 
	 * @param id 媒体分组标识
	 * @param name 参数名称
	 * @return 长整型值
	 */
	@Override
	public Long getLong(Long id, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Long.parseLong(value);
		}
		return null;
	}

	/**
	 * 设置浮点型值
	 * 
	 * @param id 媒体分组标识
	 * @param name 参数名称
	 * @param value 浮点型值
	 */
	@Override
	public void setFloat(Long id, String name, Float value) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取浮点型值
	 * 
	 * @param id 媒体分组标识
	 * @param name 参数名称
	 * @return 浮点型值
	 */
	@Override
	public Float getFloat(Long id, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Float.parseFloat(value);
		}
		return null;
	}

	/**
	 * 设置双精度浮点型值
	 * 
	 * @param id 媒体分组标识
	 * @param name 参数名称
	 * @param value 双精度浮点型值
	 */
	@Override
	public void setDouble(Long id, String name, Double value) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取双精度浮点型值
	 * 
	 * @param id 媒体分组标识
	 * @param name 参数名称
	 * @return 双精度浮点型值
	 */
	@Override
	public Double getDouble(Long id, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Double.parseDouble(value);
		}
		return null;
	}

	/**
	 * 删除参数
	 * 
	 * @param id 媒体分组标识
	 * @param name 参数名称
	 */
	@Override
	public void remove(Long id, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		objectOperations.delete(key, name);
	}

	/**
	 * 存在参数
	 * 
	 * @param id 媒体分组标识
	 * @param name 参数名称
	 * @return 持久化异常
	 */
	@Override
	public boolean exist(Long id, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		return objectOperations.hasKey(key, name);
	}

}
