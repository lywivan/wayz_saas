package com.wayz.redis.impl;

import com.wayz.entity.redis.company.RCompany;
import com.wayz.redis.RCompanyObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 公司对象实现类
 * 
 * @author yinjy
 *
 */
@Repository("rCompanyObject")
public class RCompanyObjectImpl implements RCompanyObject {

	/** Redis模板 */
	@Resource(name = "redisTemplate")
	private RedisTemplate<String, String> redisTemplate = null;

	/** 键值格式 */
	private static final MessageFormat KEY_FORMAT = new MessageFormat("platform:Object:Company:{0}");

	/**
	 * 设置公司对象
	 * 
	 * @param companyId 公司标识
	 * @param company 公司对象
	 */
	@Override
	public void set(Long companyId, RCompany company) {
		// 初始化
		Map<String, String> map = new HashMap<String, String>();
		String key = KEY_FORMAT.format(new Long[] { companyId });

		// 赋值参数
		if (company.getCompanyId() != null) {
			map.put(RCompany.COMPANYID, company.getCompanyId().toString());
		}
		if (company.getName() != null) {
			map.put(RCompany.NAME, company.getName());
		}
		if (company.getTelephone() != null) {
			map.put(RCompany.TELEPHONE, company.getTelephone());
		}
		if (company.getEmail() != null) {
			map.put(RCompany.EMAIL, company.getEmail());
		}
		if (company.getVersionId() != null) {
			map.put(RCompany.VERSIONID, company.getVersionId().toString());
		}
		if (company.getAccessDsp() != null) {
			map.put(RCompany.ACCESSDSP, company.getAccessDsp().toString());
		}
		if (company.getRequireDataAuth() != null) {
			map.put(RCompany.REQUIREDATAAUTH, company.getRequireDataAuth().toString());
		}
		if (company.getRequirePlanAudit() != null) {
			map.put(RCompany.REQUIREPLANAUDIT, company.getRequirePlanAudit().toString());
		}
		if (company.getUserLimit() != null) {
			map.put(RCompany.USERLIMIT, company.getUserLimit().toString());
		}
		if (company.getPlanLimit() != null) {
			map.put(RCompany.PLANLIMIT, company.getPlanLimit().toString());
		}
		if (company.getMaterialLimit() != null) {
			map.put(RCompany.MATERIALLIMIT, company.getMaterialLimit().toString());
		}
		if (company.getStatus() != null) {
			map.put(RCompany.STATUS, company.getStatus().toString());
		}
		if (company.getUserId() != null) {
			map.put(RCompany.USERID, company.getUserId().toString());
		}
		if (company.getSurplusUserLimit() != null) {
			map.put(RCompany.SURPLUSUSERLIMIT, company.getSurplusUserLimit().toString());
		}
		if (company.getSurplusPlanLimit() != null) {
			map.put(RCompany.SURPLUSPLANLIMIT, company.getSurplusPlanLimit().toString());
		}
		if (company.getSurplusMaterialLimit() != null) {
			map.put(RCompany.SURPLUSMATERIALLIMIT, company.getSurplusMaterialLimit().toString());
		}
		if (company.getContacts() != null) {
			map.put(RCompany.CONTACTS, company.getContacts());
		}
		if (company.getDomain() != null) {
			map.put(RCompany.DOMAIN, company.getDomain());
		}
		if (company.getLogo() != null) {
			map.put(RCompany.LOGO, company.getLogo());
		}
		if (company.getWebsiteTitle() != null) {
			map.put(RCompany.WEBSITETITLE, company.getWebsiteTitle());
		}
		if (company.getIsAdPaln() != null) {
			map.put(RCompany.ISADPALN, company.getIsAdPaln().toString());
		}
		if (company.getIsLogin() != null) {
			map.put(RCompany.ISLOGIN, company.getIsLogin().toString());
		}
		if (company.getAgentId() != null) {
			map.put(RCompany.AGENTID, company.getAgentId().toString());
		}
		if (company.getGroupId() != null) {
			map.put(RCompany.GROUPID, company.getGroupId().toString());
		}
		if (company.getBalance() != null) {
			map.put(RCompany.BALANCE, company.getBalance().toString());
		}
		if (company.getDepartmentIndex() != null) {
			map.put(RCompany.DEPARTMENTINDEX, company.getDepartmentIndex().toString());
		}
		// if (company.getPositionLimit() != null) {
		// map.put(RCompany.POSITIONLIMIT,
		// company.getPositionLimit().toString());
		// }
		// if (company.getSurplusPositionLimit() != null) {
		// map.put(RCompany.SURPLUSPOSITIONLIMIT,
		// company.getSurplusPositionLimit().toString());
		// }
		if (company.getValidUntilDate() != null) {
			map.put(RCompany.VALIDUNTILDATE, company.getValidUntilDate());
		}
		if (company.getIsWatermark() != null) {
			map.put(RCompany.ISWATERMARK, company.getIsWatermark().toString());
		}
		// 调用接口
		redisTemplate.opsForHash().putAll(key, map);
	}

	/**
	 * 获取公司对象
	 * 
	 * @param companyId 公司标识
	 * @return 公司对象
	 */
	@Override
	public RCompany get(Long companyId) {
		// 初始化
		RCompany company = null;
		List<String> keyList = new ArrayList<String>();
		String key = KEY_FORMAT.format(new Long[] { companyId });

		// 赋值参数
		keyList.add(RCompany.COMPANYID);
		keyList.add(RCompany.NAME);
		keyList.add(RCompany.TELEPHONE);
		keyList.add(RCompany.EMAIL);
		keyList.add(RCompany.VERSIONID);
		keyList.add(RCompany.ACCESSDSP);
		keyList.add(RCompany.REQUIREDATAAUTH);
		keyList.add(RCompany.REQUIREPLANAUDIT);
		keyList.add(RCompany.USERLIMIT);
		keyList.add(RCompany.PLANLIMIT);
		keyList.add(RCompany.MATERIALLIMIT);
		keyList.add(RCompany.USERID);
		keyList.add(RCompany.SURPLUSUSERLIMIT);
		keyList.add(RCompany.SURPLUSPLANLIMIT);
		keyList.add(RCompany.SURPLUSMATERIALLIMIT);
		keyList.add(RCompany.CONTACTS);
		keyList.add(RCompany.DOMAIN);
		keyList.add(RCompany.LOGO);
		keyList.add(RCompany.WEBSITETITLE);
		keyList.add(RCompany.ISADPALN);
		keyList.add(RCompany.STATUS);
		keyList.add(RCompany.AGENTID);
		keyList.add(RCompany.ISLOGIN);
		keyList.add(RCompany.GROUPID);
		keyList.add(RCompany.BALANCE);
		keyList.add(RCompany.DEPARTMENTINDEX);
		// keyList.add(RCompany.POSITIONLIMIT);
		// keyList.add(RCompany.SURPLUSPOSITIONLIMIT);
		keyList.add(RCompany.VALIDUNTILDATE);
		keyList.add(RCompany.ISWATERMARK);

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		List<String> valueList = objectOperations.multiGet(key, keyList);

		// 转化数据
		if (valueList != null && !valueList.isEmpty()) {
			company = new RCompany();
			String[] valueArray = valueList.toArray(new String[0]);
			int length = valueArray.length;
			if (length > 0 && valueArray[0] != null) {
				company.setCompanyId(Long.parseLong(valueArray[0]));
			}
			if (length > 1 && valueArray[1] != null) {
				company.setName(valueArray[1]);
			}
			if (length > 2 && valueArray[2] != null) {
				company.setTelephone(valueArray[2]);
			}
			if (length > 3 && valueArray[3] != null) {
				company.setEmail(valueArray[3]);
			}
			if (length > 4 && valueArray[4] != null) {
				company.setVersionId(Long.valueOf(valueArray[4]));
			}
			if (length > 5 && valueArray[5] != null) {
				company.setAccessDsp(Boolean.parseBoolean(valueArray[5]));
			}
			if (length > 6 && valueArray[6] != null) {
				company.setRequireDataAuth(Boolean.parseBoolean(valueArray[6]));
			}
			if (length > 7 && valueArray[7] != null) {
				company.setRequirePlanAudit(Boolean.parseBoolean(valueArray[7]));
			}
			if (length > 8 && valueArray[8] != null) {
				company.setUserLimit(Integer.parseInt(valueArray[8]));
			}
			if (length > 9 && valueArray[9] != null) {
				company.setPlanLimit(Integer.parseInt(valueArray[9]));
			}
			if (length > 10 && valueArray[10] != null) {
				company.setMaterialLimit(Long.valueOf(valueArray[10]));
			}
			if (length > 11 && valueArray[11] != null) {
				company.setUserId(Long.parseLong(valueArray[11]));
			}
			if (length > 12 && valueArray[12] != null) {
				company.setSurplusUserLimit(Integer.parseInt(valueArray[12]));
			}
			if (length > 13 && valueArray[13] != null) {
				company.setSurplusPlanLimit(Integer.parseInt(valueArray[13]));
			}
			if (length > 14 && valueArray[14] != null) {
				company.setSurplusMaterialLimit(Long.valueOf(valueArray[14]));
			}
			if (length > 15 && valueArray[15] != null) {
				company.setContacts(valueArray[15]);
			}
			if (length > 16 && valueArray[16] != null) {
				company.setDomain(valueArray[16]);
			}
			if (length > 17 && valueArray[17] != null) {
				company.setLogo(valueArray[17]);
			}
			if (length > 18 && valueArray[18] != null) {
				company.setWebsiteTitle(valueArray[18]);
			}
			if (length > 19 && valueArray[19] != null) {
				company.setIsAdPaln(Boolean.parseBoolean(valueArray[19]));
			}
			if (length > 20 && valueArray[20] != null) {
				company.setStatus(Short.valueOf(valueArray[20]));
			}
			if (length > 21 && valueArray[21] != null) {
				company.setAgentId(Long.valueOf(valueArray[21]));
			}
			if (length > 22 && valueArray[22] != null) {
				company.setIsLogin(Boolean.valueOf(valueArray[22]));
			}
			if (length > 23 && valueArray[23] != null) {
				company.setGroupId(Long.valueOf(valueArray[23]));
			}
			if (length > 24 && valueArray[24] != null) {
				company.setBalance(Double.valueOf(valueArray[24]));
			}
			if (length > 25 && valueArray[25] != null) {
				company.setDepartmentIndex(Long.valueOf(valueArray[25]));
			}
			// if (length > 27 && valueArray[27] != null) {
			// company.setPositionLimit(Integer.valueOf(valueArray[27]));
			// }
			// if (length > 28 && valueArray[28] != null) {
			// company.setSurplusPositionLimit(Integer.valueOf(valueArray[28]));
			// }
			if (length > 26 && valueArray[26] != null) {
				company.setValidUntilDate(valueArray[26]);
			}
			if (length > 27 && valueArray[27] != null) {
				company.setIsWatermark(Boolean.valueOf(valueArray[27]));
			}
		}

		// 返回数据
		return company;
	}

	/**
	 * 获取新增部门ID
	 * 
	 * @param companyId 公司标识
	 * @return 字符串值
	 */
	@Override
	public Long getNewDepartId(Long companyId) {
		// 初始化
		Long id = 1L;
		String key = KEY_FORMAT.format(new Long[] { companyId });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String val = objectOperations.get(key, RCompany.DEPARTMENTINDEX);
		if (StringUtils.isEmpty(val)) {
			redisTemplate.opsForHash().put(key, RCompany.DEPARTMENTINDEX, "1");
		}
		else {
			Long index = Long.parseLong(val) + 1;
			redisTemplate.opsForHash().put(key, RCompany.DEPARTMENTINDEX, index.toString());
			id = index;
		}
		// 返回结果
		return id;
	}

	/**
	 * 删除公司对象
	 * 
	 * @param companyId 公司标识
	 */
	@Override
	public void remove(Long companyId) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { companyId });

		// 调用接口
		redisTemplate.delete(key);
	}

	/**
	 * 存在公司对象
	 * 
	 * @param companyId 公司标识
	 * @return 是否存在
	 */
	@Override
	public boolean exist(Long companyId) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { companyId });

		// 调用接口
		return redisTemplate.hasKey(key);
	}

	/**
	 * 设置字符串值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @param value 字符串值
	 */
	@Override
	public void setString(Long companyId, String name, String value) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { companyId });

		// 调用接口
		redisTemplate.opsForHash().put(key, name, value);
	}

	/**
	 * 获取字符串值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @return 字符串值
	 */
	@Override
	public String getString(Long companyId, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { companyId });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		return objectOperations.get(key, name);
	}

	/**
	 * 设置整型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @param value 整型值
	 */
	@Override
	public void setInteger(Long companyId, String name, Integer value) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { companyId });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取整型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @return 整型值
	 */
	@Override
	public Integer getInteger(Long companyId, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { companyId });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Integer.parseInt(value);
		}
		return null;
	}

	/**
	 * 设置短整型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @param value 短整型值
	 */
	@Override
	public void setShort(Long companyId, String name, Short value) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { companyId });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取短整型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @return 短整型值
	 */
	@Override
	public Short getShort(Long companyId, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { companyId });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Short.parseShort(value);
		}
		return null;
	}

	/**
	 * 设置长整型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @param value 长整型值
	 */
	@Override
	public void setLong(Long companyId, String name, Long value) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { companyId });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取长整型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @return 长整型值
	 */
	@Override
	public Long getLong(Long companyId, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { companyId });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Long.parseLong(value);
		}
		return null;
	}

	/**
	 * 设置浮点型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @param value 浮点型值
	 */
	@Override
	public void setFloat(Long companyId, String name, Float value) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { companyId });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取浮点型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @return 浮点型值
	 */
	@Override
	public Float getFloat(Long companyId, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { companyId });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Float.parseFloat(value);
		}
		return null;
	}

	/**
	 * 设置双精度浮点型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @param value 双精度浮点型值
	 */
	@Override
	public void setDouble(Long companyId, String name, Double value) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { companyId });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取双精度浮点型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @return 双精度浮点型值
	 */
	@Override
	public Double getDouble(Long companyId, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { companyId });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Double.parseDouble(value);
		}
		return null;
	}

	/**
	 * 删除参数
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 */
	@Override
	public void remove(Long companyId, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { companyId });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		objectOperations.delete(key, name);
	}

	/**
	 * 存在参数
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @return 持久化异常
	 */
	@Override
	public boolean exist(Long companyId, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { companyId });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		return objectOperations.hasKey(key, name);
	}

}
