package com.wayz.redis.impl;

import com.wayz.entity.redis.company.RDepartment;
import com.wayz.redis.RDepartmentObject;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 部门对象实现类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("rDepartmentObject")
public class RDepartmentObjectImpl implements RDepartmentObject {

	/** Redis模板 */
	@Resource(name = "redisTemplate")
	private RedisTemplate<String, String> redisTemplate = null;

	/** 键值格式 */
	private static final MessageFormat KEY_FORMAT = new MessageFormat("platform:Object:Department:{0}-{1}");

	private String getKey(Long companyId, Long id) {
		String key = KEY_FORMAT.format(new Long[] { companyId, id });
		return key;
	}

	/**
	 * 设置部门对象
	 * 
	 * @param companyId 公司标识
	 * @param department 部门对象
	 */
	@Override
	public void set(Long companyId, Long id, RDepartment department) {
		// 初始化
		Map<String, String> map = new HashMap<String, String>();
		String key = getKey(companyId, id);

		// 赋值参数
		if (department.getName() != null) {
			map.put(RDepartment.NAME, department.getName());
		}
		if (department.getPath() != null) {
			map.put(RDepartment.PATH, department.getPath());
		}
		if (department.getParentId() != null) {
			map.put(RDepartment.PARENTID, department.getParentId().toString());
		}

		// 调用接口
		redisTemplate.opsForHash().putAll(key, map);
	}

	/**
	 * 获取部门对象
	 * 
	 * @param companyId 公司标识
	 * @return 部门对象
	 */
	@Override
	public RDepartment get(Long companyId, Long id) {
		// 初始化
		RDepartment department = null;
		List<String> keyList = new ArrayList<String>();
		String key = getKey(companyId, id);

		// 赋值参数
		keyList.add(RDepartment.NAME);
		keyList.add(RDepartment.PATH);
		keyList.add(RDepartment.PARENTID);

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		List<String> valueList = objectOperations.multiGet(key, keyList);

		// 转化数据
		if (valueList != null && !valueList.isEmpty()) {
			department = new RDepartment();
			String[] valueArray = valueList.toArray(new String[0]);
			int length = valueArray.length;
			if (length > 0 && valueArray[0] != null) {
				department.setName(valueArray[0]);
			}
			if (length > 1 && valueArray[1] != null) {
				department.setPath(valueArray[1]);
			}
			if (length > 2 && valueArray[2] != null) {
				department.setParentId(Long.parseLong(valueArray[2]));
			}
		}

		// 返回数据
		return department;
	}

	/**
	 * 删除部门对象
	 * 
	 * @param companyId 公司标识
	 */
	@Override
	public void remove(Long companyId, Long id) {
		// 初始化
		String key = getKey(companyId, id);

		// 调用接口
		redisTemplate.delete(key);
	}

	/**
	 * 存在部门对象
	 * 
	 * @param companyId 公司标识
	 * @return 是否存在
	 */
	@Override
	public boolean exist(Long companyId, Long id) {
		// 初始化
		String key = getKey(companyId, id);

		// 调用接口
		return redisTemplate.hasKey(key);
	}

	/**
	 * 设置字符串值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @param value 字符串值
	 */
	@Override
	public void setString(Long companyId, Long id, String name, String value) {
		// 初始化
		String key = getKey(companyId, id);

		// 调用接口
		redisTemplate.opsForHash().put(key, name, value);
	}

	/**
	 * 获取字符串值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @return 字符串值
	 */
	@Override
	public String getString(Long companyId, Long id, String name) {
		// 初始化
		String key = getKey(companyId, id);

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		return objectOperations.get(key, name);
	}

	/**
	 * 设置整型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @param value 整型值
	 */
	@Override
	public void setInteger(Long companyId, Long id, String name, Integer value) {
		// 初始化
		String key = getKey(companyId, id);

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取整型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @return 整型值
	 */
	@Override
	public Integer getInteger(Long companyId, Long id, String name) {
		// 初始化
		String key = getKey(companyId, id);

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Integer.parseInt(value);
		}
		return null;
	}

	/**
	 * 设置短整型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @param value 短整型值
	 */
	@Override
	public void setShort(Long companyId, Long id, String name, Short value) {
		// 初始化
		String key = getKey(companyId, id);

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取短整型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @return 短整型值
	 */
	@Override
	public Short getShort(Long companyId, Long id, String name) {
		// 初始化
		String key = getKey(companyId, id);

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Short.parseShort(value);
		}
		return null;
	}

	/**
	 * 设置长整型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @param value 长整型值
	 */
	@Override
	public void setLong(Long companyId, Long id, String name, Long value) {
		// 初始化
		String key = getKey(companyId, id);

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取长整型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @return 长整型值
	 */
	@Override
	public Long getLong(Long companyId, Long id, String name) {
		// 初始化
		String key = getKey(companyId, id);

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Long.parseLong(value);
		}
		return null;
	}

	/**
	 * 设置浮点型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @param value 浮点型值
	 */
	@Override
	public void setFloat(Long companyId, Long id, String name, Float value) {
		// 初始化
		String key = getKey(companyId, id);

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取浮点型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @return 浮点型值
	 */
	@Override
	public Float getFloat(Long companyId, Long id, String name) {
		// 初始化
		String key = getKey(companyId, id);

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Float.parseFloat(value);
		}
		return null;
	}

	/**
	 * 设置双精度浮点型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @param value 双精度浮点型值
	 */
	@Override
	public void setDouble(Long companyId, Long id, String name, Double value) {
		// 初始化
		String key = getKey(companyId, id);

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取双精度浮点型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @return 双精度浮点型值
	 */
	@Override
	public Double getDouble(Long companyId, Long id, String name) {
		// 初始化
		String key = getKey(companyId, id);

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Double.parseDouble(value);
		}
		return null;
	}

	/**
	 * 删除参数
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 */
	@Override
	public void remove(Long companyId, Long id, String name) {
		// 初始化
		String key = getKey(companyId, id);

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		objectOperations.delete(key, name);
	}

	/**
	 * 存在参数
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @return 持久化异常
	 */
	@Override
	public boolean exist(Long companyId, Long id, String name) {
		// 初始化
		String key = getKey(companyId, id);

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		return objectOperations.hasKey(key, name);
	}

}
