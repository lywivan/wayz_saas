package com.wayz.redis;


import com.wayz.entity.redis.play.RFunctionVersion;

/**
 * 功能版本对象接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
public interface RFunctionVersionObject {

	/**
	 * 设置功能版本对象
	 * 
	 * @param versionId 功能版本标识
	 * @param functionVersion 功能版本对象
	 */
	public void set(Long versionId, RFunctionVersion functionVersion);

	/**
	 * 获取功能版本对象
	 * 
	 * @param versionId 功能版本标识
	 * @return 功能版本对象
	 */
	public RFunctionVersion get(Long versionId);

	/**
	 * 删除功能版本对象
	 * 
	 * @param versionId 功能版本标识
	 */
	public void remove(Long versionId);

	/**
	 * 存在功能版本对象
	 * 
	 * @param versionId 功能版本标识
	 * @return 是否存在
	 */
	public boolean exist(Long versionId);

	/**
	 * 设置字符串值
	 * 
	 * @param versionId 功能版本标识
	 * @param name 参数名称
	 * @param value 字符串值
	 */
	public void setString(Long versionId, String name, String value);

	/**
	 * 获取字符串值
	 * 
	 * @param versionId 功能版本标识
	 * @param name 参数名称
	 * @return 字符串值
	 */
	public String getString(Long versionId, String name);

	/**
	 * 设置整型值
	 * 
	 * @param versionId 功能版本标识
	 * @param name 参数名称
	 * @param value 整型值
	 */
	public void setInteger(Long versionId, String name, Integer value);

	/**
	 * 获取整型值
	 * 
	 * @param versionId 功能版本标识
	 * @param name 参数名称
	 * @return 整型值
	 */
	public Integer getInteger(Long versionId, String name);

	/**
	 * 设置短整型值
	 * 
	 * @param versionId 功能版本标识
	 * @param name 参数名称
	 * @param value 短整型值
	 */
	public void setShort(Long versionId, String name, Short value);

	/**
	 * 获取短整型值
	 * 
	 * @param versionId 功能版本标识
	 * @param name 参数名称
	 * @return 短整型值
	 */
	public Short getShort(Long versionId, String name);

	/**
	 * 设置长整型值
	 * 
	 * @param versionId 功能版本标识
	 * @param name 参数名称
	 * @param value 长整型值
	 */
	public void setLong(Long versionId, String name, Long value);

	/**
	 * 获取长整型值
	 * 
	 * @param versionId 功能版本标识
	 * @param name 参数名称
	 * @return 长整型值
	 */
	public Long getLong(Long versionId, String name);

	/**
	 * 设置浮点型值
	 * 
	 * @param versionId 功能版本标识
	 * @param name 参数名称
	 * @param value 浮点型值
	 */
	public void setFloat(Long versionId, String name, Float value);

	/**
	 * 获取浮点型值
	 * 
	 * @param versionId 功能版本标识
	 * @param name 参数名称
	 * @return 浮点型值
	 */
	public Float getFloat(Long versionId, String name);

	/**
	 * 设置双精度浮点型值
	 * 
	 * @param versionId 功能版本标识
	 * @param name 参数名称
	 * @param value 双精度浮点型值
	 */
	public void setDouble(Long versionId, String name, Double value);

	/**
	 * 获取双精度浮点型值
	 * 
	 * @param versionId 功能版本标识
	 * @param name 参数名称
	 * @return 双精度浮点型值
	 */
	public Double getDouble(Long versionId, String name);

	/**
	 * 删除参数
	 * 
	 * @param versionId 功能版本标识
	 * @param name 参数名称
	 */
	public void remove(Long versionId, String name);

	/**
	 * 存在参数
	 * 
	 * @param versionId 功能版本标识
	 * @param name 参数名称
	 * @return 持久化异常
	 */
	public boolean exist(Long versionId, String name);

}
