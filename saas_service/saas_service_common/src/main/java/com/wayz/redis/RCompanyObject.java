package com.wayz.redis;

import com.wayz.entity.redis.company.RCompany;

/**
 * 公司对象接口
 * 
 * @author yinjy
 *
 */
public interface RCompanyObject {

	/**
	 * 设置公司对象
	 * 
	 * @param companyId 公司标识
	 * @param company 公司对象
	 */
	public void set(Long companyId, RCompany company);

	/**
	 * 获取公司对象
	 * 
	 * @param companyId 公司标识
	 * @return 公司对象
	 */
	public RCompany get(Long companyId);

	/**
	 * 获取新增部门ID
	 * 
	 * @param companyId 公司标识
	 * @return 字符串值
	 */
	public Long getNewDepartId(Long companyId);

	/**
	 * 删除公司对象
	 * 
	 * @param companyId 公司标识
	 */
	public void remove(Long companyId);

	/**
	 * 存在公司对象
	 * 
	 * @param companyId 公司标识
	 * @return 是否存在
	 */
	public boolean exist(Long companyId);

	/**
	 * 设置字符串值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @param value 字符串值
	 */
	public void setString(Long companyId, String name, String value);

	/**
	 * 获取字符串值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @return 字符串值
	 */
	public String getString(Long companyId, String name);

	/**
	 * 设置整型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @param value 整型值
	 */
	public void setInteger(Long companyId, String name, Integer value);

	/**
	 * 获取整型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @return 整型值
	 */
	public Integer getInteger(Long companyId, String name);

	/**
	 * 设置短整型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @param value 短整型值
	 */
	public void setShort(Long companyId, String name, Short value);

	/**
	 * 获取短整型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @return 短整型值
	 */
	public Short getShort(Long companyId, String name);

	/**
	 * 设置长整型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @param value 长整型值
	 */
	public void setLong(Long companyId, String name, Long value);

	/**
	 * 获取长整型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @return 长整型值
	 */
	public Long getLong(Long companyId, String name);

	/**
	 * 设置浮点型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @param value 浮点型值
	 */
	public void setFloat(Long companyId, String name, Float value);

	/**
	 * 获取浮点型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @return 浮点型值
	 */
	public Float getFloat(Long companyId, String name);

	/**
	 * 设置双精度浮点型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @param value 双精度浮点型值
	 */
	public void setDouble(Long companyId, String name, Double value);

	/**
	 * 获取双精度浮点型值
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @return 双精度浮点型值
	 */
	public Double getDouble(Long companyId, String name);

	/**
	 * 删除参数
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 */
	public void remove(Long companyId, String name);

	/**
	 * 存在参数
	 * 
	 * @param companyId 公司标识
	 * @param name 参数名称
	 * @return 持久化异常
	 */
	public boolean exist(Long companyId, String name);

}
