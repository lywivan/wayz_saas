package com.wayz.redis.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.wayz.entity.redis.play.RFunctionVersion;
import com.wayz.redis.RFunctionVersionObject;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

/**
 * 功能版本对象实现类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("rFunctionVersionObject")
public class RFunctionVersionObjectImpl implements RFunctionVersionObject {

	/** Redis模板 */
	@Resource(name = "redisTemplate")
	private RedisTemplate<String, String> redisTemplate = null;

	/** 键值格式 */
	private static final MessageFormat KEY_FORMAT = new MessageFormat("platform:Object:FunctionVersion:{0}");

	/**
	 * 设置功能版本对象
	 * 
	 * @param versionId 功能版本标识
	 * @param functionVersion 功能版本对象
	 */
	@Override
	public void set(Long versionId, RFunctionVersion functionVersion) {
		// 初始化
		Map<String, String> map = new HashMap<String, String>();
		String key = KEY_FORMAT.format(new Long[] { versionId });

		// 赋值参数
		if (functionVersion.getName() != null) {
			map.put(RFunctionVersion.NAME, functionVersion.getName());
		}
		if (functionVersion.getDescription() != null) {
			map.put(RFunctionVersion.DESCRIPTION, functionVersion.getDescription());
		}
		if (functionVersion.getRoleId() != null) {
			map.put(RFunctionVersion.ROLEID, functionVersion.getRoleId().toString());
		}
		if (functionVersion.getUserLimit() != null) {
			map.put(RFunctionVersion.USERLIMIT, functionVersion.getUserLimit().toString());
		}
		if (functionVersion.getMaterialLimit() != null) {
			map.put(RFunctionVersion.MATERIALLIMIT, functionVersion.getMaterialLimit().toString());
		}
		if (functionVersion.getSingleFileLimit() != null) {
			map.put(RFunctionVersion.SINGLEFILELIMIT, functionVersion.getSingleFileLimit().toString());
		}

		// 调用接口
		redisTemplate.opsForHash().putAll(key, map);
	}

	/**
	 * 获取功能版本对象
	 * 
	 * @param versionId 功能版本标识
	 * @return 功能版本对象
	 */
	@Override
	public RFunctionVersion get(Long versionId) {
		// 初始化
		RFunctionVersion functionVersion = null;
		List<String> keyList = new ArrayList<String>();
		String key = KEY_FORMAT.format(new Long[] { versionId });

		// 赋值参数
		keyList.add(RFunctionVersion.NAME);
		keyList.add(RFunctionVersion.DESCRIPTION);
		keyList.add(RFunctionVersion.ROLEID);
		keyList.add(RFunctionVersion.USERLIMIT);
		keyList.add(RFunctionVersion.MATERIALLIMIT);
		keyList.add(RFunctionVersion.SINGLEFILELIMIT);

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		List<String> valueList = objectOperations.multiGet(key, keyList);

		// 转化数据
		if (valueList != null && !valueList.isEmpty()) {
			functionVersion = new RFunctionVersion();
			String[] valueArray = valueList.toArray(new String[0]);
			int length = valueArray.length;
			if (length > 0 && valueArray[0] != null) {
				functionVersion.setName(valueArray[0]);
			}
			if (length > 1 && valueArray[1] != null) {
				functionVersion.setDescription(valueArray[1]);
			}
			if (length > 2 && valueArray[2] != null) {
				functionVersion.setRoleId(Long.parseLong(valueArray[2]));
			}
			if (length > 3 && valueArray[3] != null) {
				functionVersion.setUserLimit(Integer.parseInt(valueArray[3]));
			}
			if (length > 4 && valueArray[4] != null) {
				functionVersion.setMaterialLimit(Long.parseLong(valueArray[4]));
			}
			if (length > 5 && valueArray[5] != null) {
				functionVersion.setSingleFileLimit(Long.parseLong(valueArray[5]));
			}
		}

		// 返回数据
		return functionVersion;
	}

	/**
	 * 删除功能版本对象
	 * 
	 * @param versionId 功能版本标识
	 */
	@Override
	public void remove(Long versionId) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { versionId });

		// 调用接口
		redisTemplate.delete(key);
	}

	/**
	 * 存在功能版本对象
	 * 
	 * @param versionId 功能版本标识
	 * @return 是否存在
	 */
	@Override
	public boolean exist(Long versionId) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { versionId });

		// 调用接口
		return redisTemplate.hasKey(key);
	}

	/**
	 * 设置字符串值
	 * 
	 * @param versionId 功能版本标识
	 * @param name 参数名称
	 * @param value 字符串值
	 */
	@Override
	public void setString(Long versionId, String name, String value) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { versionId });

		// 调用接口
		redisTemplate.opsForHash().put(key, name, value);
	}

	/**
	 * 获取字符串值
	 * 
	 * @param versionId 功能版本标识
	 * @param name 参数名称
	 * @return 字符串值
	 */
	@Override
	public String getString(Long versionId, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { versionId });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		return objectOperations.get(key, name);
	}

	/**
	 * 设置整型值
	 * 
	 * @param versionId 功能版本标识
	 * @param name 参数名称
	 * @param value 整型值
	 */
	@Override
	public void setInteger(Long versionId, String name, Integer value) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { versionId });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取整型值
	 * 
	 * @param versionId 功能版本标识
	 * @param name 参数名称
	 * @return 整型值
	 */
	@Override
	public Integer getInteger(Long versionId, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { versionId });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Integer.parseInt(value);
		}
		return null;
	}

	/**
	 * 设置短整型值
	 * 
	 * @param versionId 功能版本标识
	 * @param name 参数名称
	 * @param value 短整型值
	 */
	@Override
	public void setShort(Long versionId, String name, Short value) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { versionId });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取短整型值
	 * 
	 * @param versionId 功能版本标识
	 * @param name 参数名称
	 * @return 短整型值
	 */
	@Override
	public Short getShort(Long versionId, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { versionId });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Short.parseShort(value);
		}
		return null;
	}

	/**
	 * 设置长整型值
	 * 
	 * @param versionId 功能版本标识
	 * @param name 参数名称
	 * @param value 长整型值
	 */
	@Override
	public void setLong(Long versionId, String name, Long value) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { versionId });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取长整型值
	 * 
	 * @param versionId 功能版本标识
	 * @param name 参数名称
	 * @return 长整型值
	 */
	@Override
	public Long getLong(Long versionId, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { versionId });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Long.parseLong(value);
		}
		return null;
	}

	/**
	 * 设置浮点型值
	 * 
	 * @param versionId 功能版本标识
	 * @param name 参数名称
	 * @param value 浮点型值
	 */
	@Override
	public void setFloat(Long versionId, String name, Float value) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { versionId });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取浮点型值
	 * 
	 * @param versionId 功能版本标识
	 * @param name 参数名称
	 * @return 浮点型值
	 */
	@Override
	public Float getFloat(Long versionId, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { versionId });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Float.parseFloat(value);
		}
		return null;
	}

	/**
	 * 设置双精度浮点型值
	 * 
	 * @param versionId 功能版本标识
	 * @param name 参数名称
	 * @param value 双精度浮点型值
	 */
	@Override
	public void setDouble(Long versionId, String name, Double value) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { versionId });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取双精度浮点型值
	 * 
	 * @param versionId 功能版本标识
	 * @param name 参数名称
	 * @return 双精度浮点型值
	 */
	@Override
	public Double getDouble(Long versionId, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { versionId });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Double.parseDouble(value);
		}
		return null;
	}

	/**
	 * 删除参数
	 * 
	 * @param versionId 功能版本标识
	 * @param name 参数名称
	 */
	@Override
	public void remove(Long versionId, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { versionId });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		objectOperations.delete(key, name);
	}

	/**
	 * 存在参数
	 * 
	 * @param versionId 功能版本标识
	 * @param name 参数名称
	 * @return 持久化异常
	 */
	@Override
	public boolean exist(Long versionId, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { versionId });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		return objectOperations.hasKey(key, name);
	}

}
