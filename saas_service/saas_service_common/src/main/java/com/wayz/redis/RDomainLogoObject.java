package com.wayz.redis;

import com.wayz.entity.redis.company.RDomainLogo;

/**
 * 域名logo对象接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
public interface RDomainLogoObject {

	/**
	 * 设置域名logo对象
	 * 
	 * @param domainName 域名
	 * @param domainLogo 域名logo对象
	 */
	public void set(String domainName, RDomainLogo domainLogo);

	/**
	 * 获取域名logo对象
	 * 
	 * @param domainName 域名
	 * @return 域名logo对象
	 */
	public RDomainLogo get(String domainName);

	/**
	 * 删除域名logo对象
	 * 
	 * @param domainName 域名
	 */
	public void remove(String domainName);

	/**
	 * 存在域名logo对象
	 * 
	 * @param domainName 域名
	 * @return 是否存在
	 */
	public boolean exist(String domainName);

	/**
	 * 设置字符串值
	 * 
	 * @param domainName 域名
	 * @param name 参数名称
	 * @param value 字符串值
	 */
	public void setString(String domainName, String name, String value);

	/**
	 * 获取字符串值
	 * 
	 * @param domainName 域名
	 * @param name 参数名称
	 * @return 字符串值
	 */
	public String getString(String domainName, String name);

	/**
	 * 设置整型值
	 * 
	 * @param domainName 域名
	 * @param name 参数名称
	 * @param value 整型值
	 */
	public void setInteger(String domainName, String name, Integer value);

	/**
	 * 获取整型值
	 * 
	 * @param domainName 域名
	 * @param name 参数名称
	 * @return 整型值
	 */
	public Integer getInteger(String domainName, String name);

	/**
	 * 设置短整型值
	 * 
	 * @param domainName 域名
	 * @param name 参数名称
	 * @param value 短整型值
	 */
	public void setShort(String domainName, String name, Short value);

	/**
	 * 获取短整型值
	 * 
	 * @param domainName 域名
	 * @param name 参数名称
	 * @return 短整型值
	 */
	public Short getShort(String domainName, String name);

	/**
	 * 设置长整型值
	 * 
	 * @param domainName 域名
	 * @param name 参数名称
	 * @param value 长整型值
	 */
	public void setLong(String domainName, String name, Long value);

	/**
	 * 获取长整型值
	 * 
	 * @param domainName 域名
	 * @param name 参数名称
	 * @return 长整型值
	 */
	public Long getLong(String domainName, String name);

	/**
	 * 设置浮点型值
	 * 
	 * @param domainName 域名
	 * @param name 参数名称
	 * @param value 浮点型值
	 */
	public void setFloat(String domainName, String name, Float value);

	/**
	 * 获取浮点型值
	 * 
	 * @param domainName 域名
	 * @param name 参数名称
	 * @return 浮点型值
	 */
	public Float getFloat(String domainName, String name);

	/**
	 * 设置双精度浮点型值
	 * 
	 * @param domainName 域名
	 * @param name 参数名称
	 * @param value 双精度浮点型值
	 */
	public void setDouble(String domainName, String name, Double value);

	/**
	 * 获取双精度浮点型值
	 * 
	 * @param domainName 域名
	 * @param name 参数名称
	 * @return 双精度浮点型值
	 */
	public Double getDouble(String domainName, String name);

	/**
	 * 删除参数
	 * 
	 * @param domainName 域名
	 * @param name 参数名称
	 */
	public void remove(String domainName, String name);

	/**
	 * 存在参数
	 * 
	 * @param domainName 域名
	 * @param name 参数名称
	 * @return 持久化异常
	 */
	public boolean exist(String domainName, String name);

}
