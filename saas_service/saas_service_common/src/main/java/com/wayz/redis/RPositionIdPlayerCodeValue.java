package com.wayz.redis;

import java.util.concurrent.TimeUnit;

/**
 * 媒体位标识播控器编码值接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
public interface RPositionIdPlayerCodeValue {

	/**
	 * 设置媒体位标识播控器编码
	 * 
	 * @param id 媒体位标识
	 * @param code 播控器编码
	 */
	public void set(Long id, String code);

	/**
	 * 设置媒体位标识播控器编码
	 * 
	 * @param id 媒体位标识
	 * @param code 播控器编码
	 * @param timeout 超时时间
	 * @param unit 超时单位
	 */
	public void set(Long id, String code, long timeout, TimeUnit unit);

	/**
	 * 获取媒体位标识播控器编码
	 * 
	 * @param id 媒体位标识
	 * @return 播控器编码
	 */
	public String get(Long id);

	/**
	 * 删除媒体位标识播控器编码
	 * 
	 * @param id 媒体位标识
	 */
	public void remove(Long id);

	/**
	 * 存在媒体位标识播控器编码
	 * 
	 * @param id 媒体位标识
	 * @return 是否存在
	 */
	public boolean exist(Long id);

	/**
	 * 设置超时时间
	 * 
	 * @param id 媒体位标识
	 * @param timeout 超时时间
	 * @param unit 时间单位
	 */
	public void setExpire(Long id, long timeout, TimeUnit unit);

	/**
	 * 获取超时时间
	 * 
	 * @param id 媒体位标识
	 * @param unit 时间单位
	 * @return 超时时间
	 */
	public Long getExpire(Long id, TimeUnit unit);

}
