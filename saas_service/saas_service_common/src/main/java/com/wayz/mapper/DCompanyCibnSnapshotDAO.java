package com.wayz.mapper;

import com.wayz.entity.pojo.cibn.DCompanyCibnSnapshot;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 公司备案快照DAO接口
 * 
 * @author mike.ma@wayz.ai
 *
 */
@Repository("dCompanyCibnSnapshotDAO")
public interface DCompanyCibnSnapshotDAO {

	/**
	 * 获取公司备案快照
	 * 
	 * @param id 主键ID
	 * @return 公司备案快照
	 */
	public DCompanyCibnSnapshot get(@Param("id") Long id);

	/**
	 * 创建公司备案快照
	 * 
	 * @param id 主键ID
	 * @param create 公司备案快照创建
	 * @return 创建行数
	 */
	public Integer create(@Param("id") Long id, @Param("create") DCompanyCibnSnapshot create);

	/**
	 * 备案设备数 +1
	 * @param id
	 * @return
	 */
	public Integer resetSnapshotCibnNum(@Param("id") Long id);

}
