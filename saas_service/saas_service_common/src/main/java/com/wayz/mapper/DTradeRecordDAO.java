package com.wayz.mapper;

import com.wayz.entity.pojo.company.DTradeRecord;
import com.wayz.entity.pojo.company.DTradeRecordExt;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * DAO接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("dTradeRecordDAO")
public interface DTradeRecordDAO {
    /**
     * 创建
     *
     * @param id 标识
     * @param create 创建
     * @return 创建行数
     */
    public Integer create(@Param("id") Long id, @Param("create") DTradeRecordExt create);

    /**
     * 统计交易记录条数
     *
     * @param companyId 公司标识
     * @param operatorType 操作员类型类型(1:平台 2:代理 3:租户 )
     * @param tradeType 交易类型(11:现金充值 12:红包充值 13:转账充值 14:退款充值 21:设备续期消费
     *            22:空间扩展消费)
     * @param beginDate 开始日期(YYYY-MM-DD)
     * @param endDate 结束日期(YYYY-MM-DD)
     *
     * @return 交易记录条数
     */
    public Integer count(@Param("companyId") Long companyId, @Param("operatorType") Short operatorType,
                         @Param("tradeType") Short tradeType, @Param("beginDate") String beginDate, @Param("endDate") String endDate);

    /**
     * 查询交易记录
     *
     * @param companyId 公司标识
     * @param operatorType 操作员类型类型(1:平台 2:代理 3:租户 )
     * @param tradeType 交易类型(11:现金充值 12:红包充值 13:转账充值 14:退款充值 21:设备续期消费
     *            22:空间扩展消费)
     * @param beginDate 开始日期(YYYY-MM-DD)
     * @param endDate 结束日期(YYYY-MM-DD)
     * @param startIndex 开始序号
     * @param pageSize 页面大小
     *
     * @return 交易记录条数
     */
    public List<DTradeRecord> query(@Param("companyId") Long companyId, @Param("operatorType") Short operatorType,
                                    @Param("tradeType") Short tradeType, @Param("beginDate") String beginDate,
                                    @Param("endDate") String endDate, @Param("startIndex") Integer startIndex,
                                    @Param("pageSize") Integer pageSize);
}
