package com.wayz.mapper;

import com.wayz.constants.ViewMap;
import com.wayz.entity.pojo.DScene;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 场景DAO接口
 * 
 * @author mike.ma@wayz.ai
 *
 */
@Repository("dSceneDAO")
public interface DSceneDAO {

	/**
	 * 获取场景
	 * 
	 * @param id 场景标识
	 * @return 场景
	 */
	public DScene get(@Param("id") Long id);

	/**
	 * 创建场景
	 * 
	 * @param id 场景标识
	 * @param create 场景创建
	 * @return 创建行数
	 */
	public Integer create(@Param("id") Long id, @Param("create") DScene create);

	/**
	 * 修改场景
	 * 
	 * @param modify 场景修改
	 * @return 修改行数
	 */
	public Integer modify(@Param("modify") DScene modify);

	/**
	 * 
	 * <p>查询场景</p>
	 * 
	 * @param name 场景名称
	 * @return
	 */
	public List<DScene> query(@Param("name") String name,@Param("companyId") Long companyId,
			@Param("startIndex") Integer startIndex,@Param("pageSize") Integer pageSize);

	/**
	 * 
	 * <p>查询广告场景类数量</p>
	 * 
	 * @param name
	 * @return
	 */
	public Integer count(@Param("name") String name,@Param("companyId") Long companyId);

	/**
	 * 
	 * <p>名称唯一</p>
	 * 
	 * @param name
	 * @param id
	 * @return
	 */
	// public Boolean queryNameUnique(@Param("name") String name, @Param("id") Long id,@Param("companyId") Long companyId);

	/**
	 * 根据场景名称获取场景信息
	 * 
	 * @param name 场景名称
	 * @param companyId
	 * @return 场景信息
	 */
	public DScene getByName(@Param("name") String name,@Param("companyId") Long companyId);

	/**
	 * 查询场景视图
	 * 
	 * @return 场景视图
	 */
	public List<ViewMap> querySceneView(@Param("companyId") Long companyId);

}
