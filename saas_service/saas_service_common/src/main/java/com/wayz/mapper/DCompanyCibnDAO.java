package com.wayz.mapper;

import com.wayz.entity.pojo.cibn.DCompanyCibn;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * CIBN DAO接口
 *
 * @author mike.ma@wayz.ai
 */
@Repository("dCompanyCibnDAO")
public interface DCompanyCibnDAO {

    /**
     * 统计申请数量
     *
     * @param companyName 公司名称
     * @param auditStatus 审核状态
     * @param industryId  行业ID
     * @return
     */
    public Integer count(@Param("companyName") String companyName, @Param("auditStatus") Short auditStatus, @Param("industryId") Long industryId);

    /**
     * 申请记录列表
     *
     * @param companyName 公司名称
     * @param auditStatus 审核状态
     * @param industryId  行业ID
     * @return
     */
    public List<DCompanyCibn> query(@Param("companyName") String companyName, @Param("auditStatus") Short auditStatus, @Param("industryId") Long industryId, @Param("startIndex") Integer startIndex, @Param("pageSize") Integer pageSize);

    /**
     * 获取申请记录
     *
     * @param id
     * @return
     */
    public DCompanyCibn get(@Param("id") Long id);

    /**
     * 创建申请记录
     *
     * @param id
     * @param create
     * @return
     */
    public Integer create(@Param("id") Long id, @Param("create") DCompanyCibn create);

    /**
     * 修改申请记录
     *
     * @param modify
     * @return
     */
    Integer modify(@Param("modify") DCompanyCibn modify);

    /**
     * 审核申请记录
     *
     * @param modify
     * @return
     */
    Integer modifyAudit(@Param("modify") DCompanyCibn modify);

    /**
     * 备案设备数 +1
     *
     * @param id
     * @return
     */
    Integer resetCibnNum(@Param("id") Long id);

}
