package com.wayz.mapper;

import com.wayz.entity.pojo.DCategory;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 行业分类DAO接口
 *
 * @author mike.ma@wayz.ai
 */
@Repository("dCategoryDAO")
public interface DCategoryDAO {

    /**
     * 获取行业分类信息
     *
     * @param id 行业标识
     * @return 行业分类信息
     */
    public DCategory get(@Param("id") Long id);

    /**
     * 创建行业分类信息
     *
     * @param id     行业标识
     * @param create 行业分类创建信息
     * @return 创建行数
     */
    public Integer create(@Param("id") Long id, @Param("create") DCategory create);

    /**
     * 修改行业分类信息
     *
     * @param modify 行业分类修改信息
     * @return 修改行数
     */
    public Integer modify(@Param("modify") DCategory modify);

    /**
     * 查询行业分类信息
     *
     * @return 行业分类信息列表
     */
    public List<DCategory> query(@Param("id") Long id, @Param("companyId") Long companyId);

    /**
     * 获取行业分类信息
     *
     * @param name 行业分类名称
     * @return 行业分类信息
     */
    public DCategory getByName(@Param("name") String name, @Param("companyId") Long companyId);

}
