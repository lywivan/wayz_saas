package com.wayz.entity.pojo.company;

import java.io.Serializable;
import java.sql.Time;
import java.sql.Timestamp;

import lombok.Data;

/**
 * 媒体分组创建类
 * 
 *  
 *
 */
@Data
public class DPositionGroupExt implements Serializable {

	/** 公司标志 */
	private Long companyId = null;
	/** 部门路径 */
	private String departPath = null;
	/** 组名称 */
	private String name = null;
	/** 组描述 */
	private String description = null;
	/** 媒体位数量 */
	private Integer positionCount = null;
	/** 场景标识 */
	private Long sceneId = null;
	/** 是否删除 */
	private Boolean isDeleted = null;
	/** 开机时间 */
	private Time startTime = null;
	/** 关机时间 */
	private Time closeTime = null;
	/** 是否启用自动开关机 */
	private Boolean autoOnOff = null;
	/** 音量大小(0 - 10) */
	private Short volumn = null;
	/** 屏幕旋转(屏幕旋转(0,90,180,270) */
	private Integer rotation = null;
	/** 是否开机自启 */
	private Boolean isAutoStart = null;
	/** 管理员 */
	private String admin = null;
	/** 管理员手机号 */
	private String adminPhone = null;
	/** 创建者标识 */
	private Long creatorId = null;
	/** 创建时间 */
	private Timestamp createdTime = null;
	/** 屏幕模板标识 */
	private Long templetId;
	/** 播控器版本 */
	private String playVersion = null;
	/** 启动画面 */
	private String openReserve = null;
	/** 播控器垫片 */
	private String playReserve = null;

}
