package com.wayz.entity.pojo.company;

import java.io.Serializable;
import java.sql.Time;
import java.sql.Timestamp;

import lombok.Data;

/**
 * 媒体分组类
 * 
 *  
 *
 */
@Data
public class DPositionGroup implements Serializable {

	/** 播控器组标识 */
	private Long id = null;
	/** 部门路径 */
	private String departPath = null;
	/** 组名称 */
	private String name = null;
	/** 组描述 */
	private String description = null;
	/** 媒体位数量 */
	private Integer positionCount = null;
	/** 场景标识 */
	private Long sceneId = null;
	/** 是否删除 */
	private Boolean isDeleted = null;
	/** 开机时间 */
	private Time startTime = null;
	/** 关机时间 */
	private Time closeTime = null;
	/** 是否启用自动开关机 */
	private Boolean autoOnOff = null;
	/** 音量大小(0 - 10) */
	private Short volumn = null;
	/** 屏幕旋转(0 - 360) */
	private Integer rotation = null;
	/** 是否开机自启 */
	private Boolean isAutoStart = null;
	/** 广告占用百分比(%) */
	private Integer adverPercent = null;
	/** 管理员 */
	private String admin = null;
	/** 管理员手机号 */
	private String adminPhone = null;
	/** 创建者标识 */
	private Long creatorId = null;
	/** 创建时间 */
	private Timestamp createdTime = null;
	/** 更新时间 */
	private Timestamp modifiedTime = null;
	/** 屏幕模板标识 */
	private Long templetId = null;
	/** 屏幕模板名称 */
	private String templetName = null;
	/** 播控器版本 */
	private String playVersion = null;
	/** 播控器垫片 */
	private String playReserve = null;
	/** 启动画面 */
	private String openReserve = null;
}
