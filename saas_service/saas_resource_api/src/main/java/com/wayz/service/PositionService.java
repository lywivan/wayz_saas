package com.wayz.service;

import com.wayz.exception.WayzException;

/**
 * 广告位服务接口
 *
 * @author wade.liu@wayz.ai
 *
 */
public interface PositionService {
    /**
     * 创建媒体组
     *
     * @param myId 我的标识
     * @param name 名称
     * @param description 描述
     * @return 媒体组标识
     * @throws WayzException 平台异常
     */
    public Long createPositionGroup(Long myId, Long companyId, Long departmentId, String name, String description,
                                    Long sceneId, Long templetId, String playVersion, String openReserve, String playReserve, String startTime,
                                    String closeTime, Boolean autoOnOff, String admin, String adminPhone, Short volumn, Integer rotation,
                                    Boolean isAutoStart, Boolean isDefault) throws WayzException;
}
