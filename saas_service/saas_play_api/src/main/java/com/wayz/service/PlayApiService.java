package com.wayz.service;

import com.wayz.entity.vo.menu.MenuVo;
import com.wayz.entity.vo.task.TaskVo;
import com.wayz.exception.WayzException;

import java.util.List;

/**
 * 播控接口服务接口
 *
 * @author wade.liu@wayz.ai
 */
public interface PlayApiService {
    /**
     * 获取播放任务
     *
     * @param mac 播控设备有线网卡MAC地址
     * @param osType 操作系统类型(1:ios; 2:Android;3:windows)
     * @param imei 播控设备IMEI号
     * @param version 本机播控系统软件版本
     * @param communicationType 通信类型(1:WIFI; 2:GSM; 3:有线)
     * @param isInitial 是否为初始(true为初始; false为正常执行任务)
     * @param ipAddr 远程主机IP
     * @param ts 系统时间戳(精确到秒)
     * @param st 当前状态(0:空闲;1:正常播放;2:正在更新操作系统; 3:正在更新播放器程序)
     * @param planId 当前播放的节目单标识
     * @param signal 信号强度
     * @return 任务列表
     */
    public List<TaskVo> getTask(String mac, Short osType, String imei, String version, Short communicationType,
                                Boolean isInitial, String ipAddr, Long ts, Short st, Long planId, Long menuId, Integer signal)
            throws WayzException;

    /**
     * 上报心跳
     *
     * @param mac 播控设备有线网卡地址
     * @param ts 系统时间戳(精确到秒)
     * @param st 当前状态(0:空闲;1:正常播放;2:正在更新操作系统; 3:正在更新播放器程序)
     * @param menuId 当前播放的节目单标识
     * @param netType 网络类型(1:WiFi;2:3G;3:VPN;4:LAN)
     * @param signal 信号强度
     * @throws WayzException 平台异常
     */
    public void heartbeat(String mac, Long ts, Short st, Long menuId, Short netType, Integer signal)
            throws WayzException;

    /**
     * 获取直播url
     *
     * @param mac 播控设备有线网卡MAC地址
     * @param taskId 播控任务标识
     * @return 直播url
     * @throws WayzException 平台异常
     */
    public String getLiveUrl(String mac, Long taskId) throws WayzException;

    /**
     * 获取节目单任务
     *
     * @param mac 播控设备有线网卡MAC地址
     * @param taskId 播控任务标识
     * @return 节目单列表
     * @throws YungeException 平台异常
     */
    public List<MenuVo> getMenu(String mac, Long taskId) throws WayzException;
}
