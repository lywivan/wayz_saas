package com.wayz.entity.pojo.menu;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Time;
import java.sql.Timestamp;

/**
 * 播控节目单类
 * 
 *  
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DPlayMenu implements Serializable {

	/** 标识 */
	private Long id = null;
	/** 媒体位标识 */
	private Long positionId = null;
	/** 节目单任务标识 */
	private Long menuTaskId = null;
	/** 广告计划标识 */
	private Long planId = null;
	/** 广告画面标识 */
	private Long screenId = null;
	/** 开始播放时间 */
	private Time begintime = null;
	/** 播放时长(秒) */
	private Integer duration = null;
	/** 是否为空闲 */
	private Boolean isIdle = null;
	/** 创建时间 */
	private Timestamp createdTime = null;


}
