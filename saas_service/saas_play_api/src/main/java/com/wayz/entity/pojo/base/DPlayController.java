package com.wayz.entity.pojo.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 终端(播控器)修改类
 * 
 *  
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DPlayController implements Serializable {

	/** 设备编码 */
	private String code = null;
	/** 公司编号 */
	private Long companyId = null;
	/** 媒体位标识 */
	private Long positionId = null;
	/** 播控状态(1:正常 2:故障; 3:告警 ) */
	private Short status = null;
	/** 是否在线 */
	private Boolean isOnline = null;
	/** 创建者标识 */
	private Long creatorId = null;
	/** 修改时间 */
	private Timestamp modifiedTime = null;


}
