package com.wayz.entity.pojo.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 播控告警修改类
 * 
 *  
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DPlayAlert implements Serializable {

	/** 告警记录标识 */
	private Long id = null;
	/** 播控器编码 */
	private String playCode = null;
	/** 告警类型(1:离线;2: 素材下载失败;3:无相应素材;4: 显示器分辨率不符;5: 新服务器连接失败) */
	private Short type = null;
	/** 告警内容 */
	private String content = null;
	/** 是否已恢复 */
	private Boolean isRecover = null;
	/** 修改时间 */
	private Timestamp modifiedTime = null;


}
