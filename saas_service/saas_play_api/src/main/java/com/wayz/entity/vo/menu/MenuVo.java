package com.wayz.entity.vo.menu;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 播控节目单类
 * 
 *  
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MenuVo implements Serializable {

	/** 节目标识 */
	private Long id = null;
	/** 广告计划标识 */
	private Long planId = null;
	/** 屏幕标识 */
	private Long scrId = null;
	/** 开始播放时间（hh:mm:ss） */
	private String start = null;
	/** 播放时长(秒) */
	private Integer duration = null;
	/** 是否为空闲 */
	private Boolean isIdle = null;
	/** 是否需要监播 */
	private Boolean isMonitor = null;

}
