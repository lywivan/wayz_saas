package com.wayz.entity.pojo.play;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 播控心跳记录创建类
 * 
 *  
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DPlayHeartbeat implements Serializable {

	/**编号*/
	private Long id;
	/** 播控器编码 */
	private String playCode = null;
	/** 播控器时间 */
	private Timestamp ts = null;
	/** 当前状态(0:空闲;1:正常播放;2: 正在更新操作系统; 3: 正在更新播放器程序) */
	private Short status = null;
	/** 当前播放的节目单标识 */
	private Long menuid = null;
	/** 网络类型(1:WiFi;2:3G;3:VPN;4:LAN) */
	private Short netType = null;
	/** 信号强度 */
	private Integer rssi = null;
	/** 创建时间 */
	private Timestamp createdTime = null;


}
