package com.wayz.entity.vo.task;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 播控任务类
 * 
 *  
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskVo implements Serializable {

	/** 标识 */
	private Long id = null;
	/** 任务标识 */
	private Long taskId = null;
	/** 任务类型 */
	private Short type = null;


}
