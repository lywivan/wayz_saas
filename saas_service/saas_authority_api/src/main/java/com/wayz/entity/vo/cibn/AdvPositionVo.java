package com.wayz.entity.vo.cibn;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdvPositionVo implements Serializable {
    /** 设备标识 */
    private Long id = null;
    /** 租户标识 */
    private Long companyId = null;
    /** 公司名称 */
    private String companyName = null;
    /** 播控编码 */
    private String playCode = null;
    /** 播控版本 */
    private String packageVersion = null; 
    /** 操作系统 */
    private String osDesc = null;
    /** 设备编码 */
    private String code = null;
    /** 名称 */
    private String name = null;
    /** 媒体分组标识 */
    private Long groupId = null;
    /** 分组名称  */
    private String groupName = null;
    /** 分辨率标识 */
    private Long resolutionId = null;
    /** 分辨率名称 */
    private String resolutionName = null;
    /** 场景标识 */
    private Long sceneId = null;
    /** 是否已绑定设备 */
    private Boolean isBound = null;
    /** 是否启用 */
    private Boolean isEnable = null;
    /** 是否删除 */
    private Boolean isDeleted = null;
    /** 区县标识 */
    private Long districtId = null;
    /** 详细地址 */
    private String address = null;
    /** 经度 */
    private Double longitude = null;
    /** 纬度 */
    private Double latitude = null;
    /** 审核备注 */
    private String remark = null;
    /** 审核时间 */
    private String auditTime = null;
    /** 审核状态(0:未提交审核; 1:待审核; 2: 审核通过; 3:审核未通过) */
    private Short auditStatus = null;
    /** 审核状态(0:未提交审核; 1:待审核; 2: 审核通过; 3:审核未通过) */
    private String statusName = null;
    /** 状态 */
    private Short status = null;
    /** 屏幕尺寸 */
    private String screenSize = null;
    /** 场景图 */
    private String sceneImage = null;
    /** 省份 */
    private String provinceName = null;
    /** 城市 */
    private String cityName = null;
    /** 区县 */
    private String districtName = null;
    /** 开机时间 */
    private String startTime = null;
    /** 关机时间 */
    private String closeTime = null;
    /** 创建时间 */
    private String createdTime = null;
    /** 更新时间 */
    private String modifiedTime = null;
}
