package com.wayz.entity.dto.cibn;

import java.io.Serializable;

import com.wayz.constants.PageDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdvPositionCibnPageDto extends PageDto implements Serializable {
    /** 公司名称 */
    private String name = null;
    /** 媒体分组标识 */
    private Short auditStatus = null;

    private String code=null;
}
