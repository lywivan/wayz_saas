package com.wayz.entity.dto.cibn;

import java.io.Serializable;

import com.wayz.constants.PageDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdvPositionPageDto extends PageDto implements Serializable {
    /** 设备标识 */
    private Long companyId = null;
    /** 租户标识 */
    private Long id = null;
    /** 公司名称 */
    private String name = null;
    /** 播控编码 */
    private String playCode = null;
    /** 播控版本 */
    private Boolean isBound = null;
    /** 操作系统 */
    private Boolean isEnable = null;
    /** 设备编码 */
    private Boolean isOnline = null;
    /** 名称 */
    private Short status = null;
    /** 媒体分组标识 */
    private Short auditStatus = null;
}
