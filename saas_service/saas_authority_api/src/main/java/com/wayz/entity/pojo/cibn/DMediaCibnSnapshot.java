package com.wayz.entity.pojo.cibn;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 媒体点位快照创建类
 * 
 * @author BML
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DMediaCibnSnapshot implements Serializable {

	/** 屏幕尺寸 */
	private String screenSize = null;
	/** 场景图 */
	private String sceneImage = null;
	/** 公司名称 */
	private String name = null;
	/** 详细地址 */
	private String address = null;
	/** 设备编码 */
	private String code = null;
	/** 原ID */
	private Long originId = null;
	/** 有效期 */
	private Date validUntilDate = null;
	/** 备案日期 */
	private Timestamp auditTime = null;

}
