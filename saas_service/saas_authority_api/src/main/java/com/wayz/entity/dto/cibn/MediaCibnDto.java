package com.wayz.entity.dto.cibn;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wade.liu@wayz.ai
 * @Date: 2019/12/6 下午6:54
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MediaCibnDto implements Serializable   {

    /** ID */
    private Long id = null;
    /** 设备编码 */
    private String code = null;
    /** 公司标识 */
    private Long companyId = null;
    /** 点位备案编号 */
    private String recordNumber = null;
    /** 场景图 */
    private String sceneImage = null;
    /** 屏幕尺寸 */
    private String screenSize = null;
    /** 地区标识 */
    private Long districtId = null;
    /** 详细地址 */
    private String address = null;
    /** 有效期至 */
    private String validUntilString = null;
    /** 审核状态(0:未提交审核; 1:待审核; 2: 审核通过; 3:审核未通过) */
    private Short auditStatus = null;
    /** 审核时间 */
    private String auditTime = null;
    /** 审核备注 */
    private String remark = null;
    /** 渠道名称 */
    private String channel = null;
    /** 原标识 */
    private Long originId = null;
    /** 创建时间 */
    private String createdTime = null;
    /** 修改时间 */
    private String modifiedTime = null;
    /** 广告位所在网点地址省份 */
    private String provinceName = null;
    /** 广告位所在网点地址城市 */
    private String cityName = null;
    /** 广告位所在网点地址区县 */
    private String districtName = null;

}
