package com.wayz.service;

import com.wayz.constants.PageVo;
import com.wayz.constants.ViewKeyValue;
import com.wayz.entity.dto.cibn.MediaCibnDto;
import com.wayz.entity.vo.cibn.AdvPositionVo;
import com.wayz.exception.WayzException;

import java.util.List;

/**
 * 媒体点位备案
 * @author wade.liu@wayz.ai
 * @Date: 2019/12/10 下午6:10
 */
public interface AdvPositionCibnService {


    /**
     * 审核状态
     *
     * @param myId
     * @return
     */
    public List<ViewKeyValue> queryAuditType(Long myId) throws WayzException;

    /**
     * 查询媒体点位
     *
     * @param auditStatus
     * @param code
     * @param name
     * @param startIndex
     * @param pageSize
     * @return
     */
    public PageVo<AdvPositionVo> queryPosition(Long companyId, Long id, String playCode, String name, Boolean isBound,
                                               Boolean isEnable, Boolean isOnline, Short status, Short auditStatus, Integer startIndex, Integer pageSize)
            throws WayzException;

    /**
     * 媒体点位详情
     *
     * @param id 媒体点位ID
     * @return
     */
    public AdvPositionVo getPosition(Long id) throws WayzException;

    /**
     * 媒体点位备案列表 --> 管理端
     * @param auditStatus 审核状态
     * @param code 设备编码
     * @param name 公司名称
     * @param startIndex  开始角标
     * @param pageSize    每页大小
     * @return
     * @throws YungeException
     */
    public PageVo<AdvPositionVo> queryAdminPositionCibnList(Short auditStatus,
                                                        String code, String name, Integer startIndex, Integer pageSize)
            throws WayzException;


    /**
     * 审核媒体点位申请
     * @param mediaCibnVO
     * @throws YungeException
     */
    public void auditMediaCibn(MediaCibnDto dto) throws WayzException;
}
