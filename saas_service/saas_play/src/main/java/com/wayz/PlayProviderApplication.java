package com.wayz;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 播控端服务提供方启动类
 *
 * @author:ivan.liu
 */
@EnableDiscoveryClient
@MapperScan("com.wayz.mapper")
@SpringBootApplication(scanBasePackages = {"com.wayz"})
public class PlayProviderApplication {
    public static void main(String[] args) {
        SpringApplication.run(PlayProviderApplication.class,args);
    }
}
