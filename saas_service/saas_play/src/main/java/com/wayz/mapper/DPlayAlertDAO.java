package com.wayz.mapper;

import java.util.List;

import com.wayz.entity.pojo.base.DPlayAlert;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 播控告警DAO接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("dPlayAlertDAO")
public interface DPlayAlertDAO {
	/**
	 * 修改播控告警
	 *
	 * @param modify 播控告警修改
	 * @return 修改行数
	 */
	public Integer modify(@Param("modify") DPlayAlert modify);
}
