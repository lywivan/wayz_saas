package com.wayz.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 直播DAO接口
 *
 * @author wade.liu@wayz.ai
 * @Date: 2019/11/18 下午2:18
 */
@Repository("dRealtimeLiveDAO")
public interface DRealtimeLiveDAO {

	/**
	 * 直播信息
	 *
	 * @param id
	 * @return
	 */
	public String getPullUrl(@Param("id") Long id);

}
