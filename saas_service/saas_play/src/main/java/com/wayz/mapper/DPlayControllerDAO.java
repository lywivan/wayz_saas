package com.wayz.mapper;

import java.util.List;

import com.wayz.entity.pojo.base.DPlayController;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 终端(播控器)DAO接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("dPlayControllerDAO")
public interface DPlayControllerDAO {
    /**
     * 修改终端(播控器)
     *
     * @param modify 终端(播控器)修改
     * @return 修改行数
     */
    public Integer modify(@Param("modify") DPlayController modify);
}
