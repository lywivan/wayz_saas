package com.wayz.config;

import com.wayz.rabbitmq.MqDefinition;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * rabbitMq配置类
 *
 * @author:ivan.liu
 */
@Configuration
public class RabbitConfig {
    @Bean
    public Queue playHeartBeatQueue(){
        return new Queue(MqDefinition.PLAY_HEART_BEAT_QUEUE);
    }

    @Bean
    public DirectExchange playExchange(){
        return new DirectExchange(MqDefinition.PLAY_EXCHANGE);
    }

    @Bean
    public Binding playHeartBeatBinding(Queue playHeartBeatQueue,DirectExchange playExchange){
        return BindingBuilder.bind(playHeartBeatQueue).to(playExchange).with(MqDefinition.PLAY_HEART_BEAT_ROUTING_KEY);
    }
}
