package com.wayz.redis.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.wayz.redis.RPlayerHeartbeatRank;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

/**
 * 播控心跳记录排行实现类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("rPlayerHeartbeatRank")
public class RPlayerHeartbeatRankImpl implements RPlayerHeartbeatRank {

	/** Redis模板 */
	@Resource(name = "redisTemplate")
	private RedisTemplate<String, String> redisTemplate = null;

	/** 键值格式 */
	private static final String KEY = "platform:Rank:PlayerHeartbeat";

	/**
	 * 设置最新心跳时间
	 * 
	 * @param code 播控器编码
	 * @param ts 最新心跳时间
	 */
	@Override
	public void set(String code, Long ts) {
		// double score = 0.0D;
		if (ts != null) {
			// score = ts.doubleValue();
			redisTemplate.opsForZSet().add(KEY, code, ts);
		}
	}

	/**
	 * 获取最新心跳时间
	 * 
	 * @param code 播控器编码
	 * @return 最新心跳时间
	 */
	@Override
	public Long get(String code) {
		Double ts = redisTemplate.opsForZSet().score(KEY, code);
		if (ts != null) {
			return ts.longValue();
		}
		return null;
	}

	/**
	 * 删除最新心跳时间
	 * 
	 * @param code 播控器编码
	 */
	@Override
	public void remove(String code) {
		redisTemplate.opsForZSet().remove(KEY, new Object[] { code });
	}

	/**
	 * 正序播控心跳记录排行
	 * 
	 * @param startIndex 开始序号
	 * @param endIndex 结束序号
	 * @return 播控器编码列表
	 */
	@Override
	public List<String> range(Long startIndex, Long endIndex) {
		long start = 0L;
		long end = Long.MAX_VALUE;
		if (startIndex != null) {
			start = startIndex.longValue();
		}
		if (endIndex != null) {
			end = endIndex.longValue();
		}
		return new ArrayList(redisTemplate.opsForZSet().range(KEY, start, end));
	}

	/**
	 * 根据分数区间获取播控心跳记录排行
	 * 
	 * @param minScore 最小分值
	 * @param maxScore 最大分值
	 * @return 播控器编码列表
	 */
	@Override
	public List<String> rangeByScoreAndRemove(Long minScore, Long maxScore) {
		long min = 0L;
		if (minScore != null) {
			min = minScore;
		}
		// 获取排名列表
		List<String> codeList = new ArrayList(redisTemplate.opsForZSet().rangeByScore(KEY, min, maxScore));
		// 清除列表
		redisTemplate.opsForZSet().removeRangeByScore(KEY, min, maxScore);

		// 返回结果
		return codeList;
	}

	/**
	 * 反序播控心跳记录排行
	 * 
	 * @param startIndex 开始序号
	 * @param endIndex 结束序号
	 * @return 播控器编码列表
	 */
	@Override
	public List<String> reverse(Long startIndex, Long endIndex) {
		long start = 0L;
		long end = Long.MAX_VALUE;
		if (startIndex != null) {
			start = startIndex.longValue();
		}
		if (endIndex != null) {
			end = endIndex.longValue();
		}
		return new ArrayList(redisTemplate.opsForZSet().reverseRange(KEY, start, end));
	}

	/**
	 * 播控心跳记录排行大小
	 * 
	 * @return 播控心跳记录排行大小
	 */
	@Override
	public Long size() {
		return redisTemplate.opsForZSet().size(KEY);
	}

}
