package com.wayz.redis;

/**
 * 播控心跳记录标识接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
public interface RPlayHeartbeatId {

	/**
	 * 递增播控心跳记录标识
	 * 
	 * @return 播控心跳记录标识
	 */
	public Long increment();

}
