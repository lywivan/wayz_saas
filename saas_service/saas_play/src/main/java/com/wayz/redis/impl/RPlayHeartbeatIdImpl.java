package com.wayz.redis.impl;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;

import com.wayz.redis.RPlayHeartbeatId;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

/**
 * 播控心跳记录标识实现类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("rPlayHeartbeatId")
public class RPlayHeartbeatIdImpl implements RPlayHeartbeatId {

	/** Redis模板 */
	@Resource(name = "redisTemplate")
	private RedisTemplate<String, String> redisTemplate = null;

	/** 键值格式 */
	private static final String KEY = "platform:Id:PlayHeartbeat";
	/** 常量相关 */
	/** 放大倍率 */
	private static final long RATIO = 10000000000L;
	/** 日期模式 */
	private final static String DATE_PATTERN = "yyyyMMdd";

	/**
	 * 递增播控心跳记录标识
	 * 
	 * @return 播控心跳记录标识
	 */
	@Override
	public Long increment() {
		// 获取数据
		long date = Long.parseLong(new SimpleDateFormat(DATE_PATTERN).format(new Date()));
		long index = redisTemplate.opsForValue().increment(KEY, 1L);

		// 返回数据
		return date * RATIO + index % RATIO;
	}

}
