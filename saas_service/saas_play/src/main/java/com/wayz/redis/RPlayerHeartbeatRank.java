package com.wayz.redis;

import java.util.List;

/**
 * 播控心跳记录排行接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
public interface RPlayerHeartbeatRank {

	/**
	 * 设置最新心跳时间
	 * 
	 * @param code 播控器编码
	 * @param ts 最新心跳时间
	 */
	public void set(String code, Long ts);

	/**
	 * 获取最新心跳时间
	 * 
	 * @param code 播控器编码
	 * @return 最新心跳时间
	 */
	public Long get(String code);

	/**
	 * 删除最新心跳时间
	 * 
	 * @param code 播控器编码
	 */
	public void remove(String code);

	/**
	 * 正序播控心跳记录排行
	 * 
	 * @param startIndex 开始序号
	 * @param endIndex 结束序号
	 * @return 播控器编码列表
	 */
	public List<String> range(Long startIndex, Long endIndex);

	/**
	 * 根据分数区间获取播控心跳记录排行
	 * 
	 * @param minScore 最小分值
	 * @param maxScore 最大分值
	 * @return 播控器编码列表
	 */
	public List<String> rangeByScoreAndRemove(Long minScore, Long maxScore);

	/**
	 * 反序播控心跳记录排行
	 * 
	 * @param startIndex 开始序号
	 * @param endIndex 结束序号
	 * @return 播控器编码列表
	 */
	public List<String> reverse(Long startIndex, Long endIndex);

	/**
	 * 播控心跳记录排行大小
	 * 
	 * @return 播控心跳记录排行大小
	 */
	public Long size();

}
