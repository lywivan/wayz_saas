package com.wayz.mapper;

import java.util.List;

import com.wayz.entity.pojo.device.DTenancyPrice;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 设备租赁价格DAO接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("dTenancyPriceDAO")
public interface DTenancyPriceDAO {
    /**
     * 获取价格
     *
     * @param days 天数
     * @return 价格
     */
    public DTenancyPrice getByDays(@Param("days") Integer days);

    /**
     * 创建设备租赁价格
     *
     * @param id 标识
     * @param create 设备租赁价格创建
     * @return 创建行数
     */
    public Integer create(@Param("id") Long id, @Param("create") DTenancyPrice create);

    /**
     * 修改设备租赁价格
     *
     * @param modify 设备租赁价格修改
     * @return 修改行数
     */
    public Integer modify(@Param("modify") DTenancyPrice modify);

    /**
     * 删除价格
     *
     * @param id 价格标识
     */
    public void delete(@Param("id") Long id);

    /**
     * 查询价格
     *
     * @return 价格列表
     */
    public List<DTenancyPrice> query();

}
