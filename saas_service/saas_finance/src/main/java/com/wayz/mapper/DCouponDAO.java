package com.wayz.mapper;

import java.util.List;

import com.wayz.entity.pojo.coupon.DCoupon;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 优惠券DAO接口
 * 
 * @author yinjy
 *
 */
@Repository("dCouponDAO")
public interface DCouponDAO {

	/**
	 * 统计数量
	 *
	 * @param productType 产品类型
	 * @param isEnable 是否有效
	 * @return 数量
	 */
	public Integer count(@Param("productType") Short productType, @Param("isEnable") Boolean isEnable);

	/**
	 * 查询
	 *
	 * @param productType 产品类型
	 * @param isEnable 是否有效
	 * @param startIndex 页号
	 * @param pageSize 页大小
	 * @return 列表
	 */
	public List<DCoupon> query(@Param("productType") Short productType, @Param("isEnable") Boolean isEnable,
							   @Param("startIndex") Integer startIndex, @Param("pageSize") Integer pageSize);

	/**
	 * 创建优惠券
	 *
	 * @param id 标识
	 * @param create 优惠券创建
	 * @return 创建行数
	 */
	public Integer create(@Param("id") Long id, @Param("create") DCoupon create);

	/**
	 * 修改优惠券
	 *
	 * @param modify 优惠券修改
	 * @return 修改行数
	 */
	public Integer modify(@Param("modify") DCoupon modify);

	/**
	 * 获取优惠券
	 *
	 * @param id 标识
	 * @return 优惠券
	 */
	public DCoupon get(@Param("id") Long id);
}
