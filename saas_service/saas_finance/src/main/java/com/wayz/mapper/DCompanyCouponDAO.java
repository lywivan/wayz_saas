package com.wayz.mapper;

import java.util.List;

import com.wayz.entity.pojo.coupon.DCompanyCoupon;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 公司优惠券DAO接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("dCompanyCouponDAO")
public interface DCompanyCouponDAO {

	/**
	 * 创建公司优惠券
	 * 
	 * @param id 标识
	 * @param create 公司优惠券创建
	 * @return 创建行数
	 */
	public Integer create(@Param("id") Long id, @Param("create") DCompanyCoupon create);

}
