package com.wayz.mapper;

import java.util.List;

import com.wayz.entity.pojo.space.DSpacePrice;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 空间价格DAO接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("dSpacePriceDAO")
public interface DSpacePriceDAO {
    /**
     * @param space
     * @return
     */
    public DSpacePrice getBySpace(@Param("space") Integer space);

    /**
     * 创建空间价格
     *
     * @param space 空间(GB)
     * @param create 空间价格创建
     * @return 创建行数
     */
    public Integer create(@Param("space") Integer space, @Param("create") DSpacePrice create);

    /**
     * 修改空间价格
     *
     * @param modify 空间价格修改
     * @return 修改行数
     */
    public Integer modify(@Param("modify") DSpacePrice modify);

    /**
     * @param space
     */
    public void delete(@Param("space") Integer space);

    /**
     * @return
     */
    public List<DSpacePrice> query();
}