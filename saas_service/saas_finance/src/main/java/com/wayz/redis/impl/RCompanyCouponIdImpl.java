package com.wayz.redis.impl;

import javax.annotation.*;

import com.wayz.redis.RCompanyCouponId;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.*;

/**
 * 公司代金券标识实现类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("rCompanyCouponId")
public class RCompanyCouponIdImpl implements RCompanyCouponId {

	/** Redis模板 */
	@Resource(name = "redisTemplate")
	private RedisTemplate<String, String> redisTemplate = null;

	/** 键值格式 */
	private static final String KEY = "platform:Id:CompanyCoupon";

	/**
	 * 递增公司代金券标识
	 * 
	 * @return 公司代金券标识
	 */
	@Override
	public Long increment() {
		return redisTemplate.opsForValue().increment(KEY, 1L);
	}

}
