package com.wayz.redis;

/**
 * 公司代金券标识接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
public interface RCompanyCouponId {

	/**
	 * 递增公司代金券标识
	 * 
	 * @return 公司代金券标识
	 */
	public Long increment();

}
