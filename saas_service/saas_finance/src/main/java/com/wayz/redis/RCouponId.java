package com.wayz.redis;

/**
 * 代金券标识接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
public interface RCouponId {

	/**
	 * 递增代金券标识
	 * 
	 * @return 代金券标识
	 */
	public Long increment();

}
