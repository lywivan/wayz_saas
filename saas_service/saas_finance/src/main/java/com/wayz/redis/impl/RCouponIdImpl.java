package com.wayz.redis.impl;

import javax.annotation.*;

import com.wayz.redis.RCouponId;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.*;

/**
 * 代金券标识实现类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("rCouponId")
public class RCouponIdImpl implements RCouponId {

	/** Redis模板 */
	@Resource(name = "redisTemplate")
	private RedisTemplate<String, String> redisTemplate = null;

	/** 键值格式 */
	private static final String KEY = "platform:Id:Coupon";

	/**
	 * 递增代金券标识
	 * 
	 * @return 代金券标识
	 */
	@Override
	public Long increment() {
		return redisTemplate.opsForValue().increment(KEY, 1L);
	}

}
