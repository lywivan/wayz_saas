package com.wayz.redis;

/**
 * 设备续期价格标识接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
public interface RTenancyPriceId {

	/**
	 * 递增设备续期价格标识
	 * 
	 * @return 设备续期价格标识
	 */
	public Long increment();

}
