package com.wayz.redis.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.wayz.entity.redis.device.RTenancyPrice;
import com.wayz.redis.RTenancyPriceObject;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

/**
 * 设备续费价格对象实现类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("rTenancyPriceObject")
public class RTenancyPriceObjectImpl implements RTenancyPriceObject {

	/** Redis模板 */
	@Resource(name = "redisTemplate")
	private RedisTemplate<String, String> redisTemplate = null;

	/** 键值格式 */
	private static final MessageFormat KEY_FORMAT = new MessageFormat("platform:Object:DevicePrice:{0}");

	/**
	 * 设置设备续费价格对象
	 * 
	 * @param id 价格标识
	 * @param devicePrice 设备续费价格对象
	 */
	@Override
	public void set(Long id, RTenancyPrice devicePrice) {
		// 初始化
		Map<String, String> map = new HashMap<String, String>();
		String key = KEY_FORMAT.format(new Long[] { id });

		// 赋值参数
		if (devicePrice.getDays() != null) {
			map.put(RTenancyPrice.DAYS, devicePrice.getDays().toString());
		}
		if (devicePrice.getName() != null) {
			map.put(RTenancyPrice.NAME, devicePrice.getName());
		}
		if (devicePrice.getSalePrice() != null) {
			map.put(RTenancyPrice.SALEPRICE, devicePrice.getSalePrice().toString());
		}
		if (devicePrice.getDiscountPrice() != null) {
			map.put(RTenancyPrice.DISCOUNTPRICE, devicePrice.getDiscountPrice().toString());
		}

		// 调用接口
		redisTemplate.opsForHash().putAll(key, map);
	}

	/**
	 * 获取设备续费价格对象
	 * 
	 * @param id 价格标识
	 * @return 设备续费价格对象
	 */
	@Override
	public RTenancyPrice get(Long id) {
		// 初始化
		RTenancyPrice devicePrice = null;
		List<String> keyList = new ArrayList<String>();
		String key = KEY_FORMAT.format(new Long[] { id });

		// 赋值参数
		keyList.add(RTenancyPrice.DAYS);
		keyList.add(RTenancyPrice.NAME);
		keyList.add(RTenancyPrice.SALEPRICE);
		keyList.add(RTenancyPrice.DISCOUNTPRICE);

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		List<String> valueList = objectOperations.multiGet(key, keyList);

		// 转化数据
		if (valueList != null && !valueList.isEmpty()) {
			devicePrice = new RTenancyPrice();
			String[] valueArray = valueList.toArray(new String[0]);
			int length = valueArray.length;
			if (length > 0 && valueArray[0] != null) {
				devicePrice.setDays(Integer.parseInt(valueArray[0]));
			}
			if (length > 1 && valueArray[1] != null) {
				devicePrice.setName(valueArray[1]);
			}
			if (length > 2 && valueArray[2] != null) {
				devicePrice.setSalePrice(Double.parseDouble(valueArray[2]));
			}
			if (length > 3 && valueArray[3] != null) {
				devicePrice.setDiscountPrice(Double.parseDouble(valueArray[3]));
			}
		}

		// 返回数据
		return devicePrice;
	}

	/**
	 * 删除设备续费价格对象
	 * 
	 * @param id 价格标识
	 */
	@Override
	public void remove(Long id) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		redisTemplate.delete(key);
	}

	/**
	 * 存在设备续费价格对象
	 * 
	 * @param id 价格标识
	 * @return 是否存在
	 */
	@Override
	public boolean exist(Long id) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		return redisTemplate.hasKey(key);
	}

	/**
	 * 设置字符串值
	 * 
	 * @param id 价格标识
	 * @param name 参数名称
	 * @param value 字符串值
	 */
	@Override
	public void setString(Long id, String name, String value) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		redisTemplate.opsForHash().put(key, name, value);
	}

	/**
	 * 获取字符串值
	 * 
	 * @param id 价格标识
	 * @param name 参数名称
	 * @return 字符串值
	 */
	@Override
	public String getString(Long id, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		return objectOperations.get(key, name);
	}

	/**
	 * 设置整型值
	 * 
	 * @param id 价格标识
	 * @param name 参数名称
	 * @param value 整型值
	 */
	@Override
	public void setInteger(Long id, String name, Integer value) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取整型值
	 * 
	 * @param id 价格标识
	 * @param name 参数名称
	 * @return 整型值
	 */
	@Override
	public Integer getInteger(Long id, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Integer.parseInt(value);
		}
		return null;
	}

	/**
	 * 设置短整型值
	 * 
	 * @param id 价格标识
	 * @param name 参数名称
	 * @param value 短整型值
	 */
	@Override
	public void setShort(Long id, String name, Short value) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取短整型值
	 * 
	 * @param id 价格标识
	 * @param name 参数名称
	 * @return 短整型值
	 */
	@Override
	public Short getShort(Long id, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Short.parseShort(value);
		}
		return null;
	}

	/**
	 * 设置长整型值
	 * 
	 * @param id 价格标识
	 * @param name 参数名称
	 * @param value 长整型值
	 */
	@Override
	public void setLong(Long id, String name, Long value) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取长整型值
	 * 
	 * @param id 价格标识
	 * @param name 参数名称
	 * @return 长整型值
	 */
	@Override
	public Long getLong(Long id, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Long.parseLong(value);
		}
		return null;
	}

	/**
	 * 设置浮点型值
	 * 
	 * @param id 价格标识
	 * @param name 参数名称
	 * @param value 浮点型值
	 */
	@Override
	public void setFloat(Long id, String name, Float value) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取浮点型值
	 * 
	 * @param id 价格标识
	 * @param name 参数名称
	 * @return 浮点型值
	 */
	@Override
	public Float getFloat(Long id, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Float.parseFloat(value);
		}
		return null;
	}

	/**
	 * 设置双精度浮点型值
	 * 
	 * @param id 价格标识
	 * @param name 参数名称
	 * @param value 双精度浮点型值
	 */
	@Override
	public void setDouble(Long id, String name, Double value) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 转换数据
		String $value = null;
		if (value != null) {
			$value = value.toString();
		}

		// 调用接口
		redisTemplate.opsForHash().put(key, name, $value);
	}

	/**
	 * 获取双精度浮点型值
	 * 
	 * @param id 价格标识
	 * @param name 参数名称
	 * @return 双精度浮点型值
	 */
	@Override
	public Double getDouble(Long id, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		String value = objectOperations.get(key, name);
		if (value != null) {
			return Double.parseDouble(value);
		}
		return null;
	}

	/**
	 * 删除参数
	 * 
	 * @param id 价格标识
	 * @param name 参数名称
	 */
	@Override
	public void remove(Long id, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		objectOperations.delete(key, name);
	}

	/**
	 * 存在参数
	 * 
	 * @param id 价格标识
	 * @param name 参数名称
	 * @return 持久化异常
	 */
	@Override
	public boolean exist(Long id, String name) {
		// 初始化
		String key = KEY_FORMAT.format(new Long[] { id });

		// 调用接口
		HashOperations<String, String, String> objectOperations = redisTemplate.opsForHash();
		return objectOperations.hasKey(key, name);
	}

}
