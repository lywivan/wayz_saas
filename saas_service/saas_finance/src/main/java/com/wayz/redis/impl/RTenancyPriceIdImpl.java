package com.wayz.redis.impl;

import javax.annotation.*;

import com.wayz.redis.RTenancyPriceId;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.*;

/**
 * 设备续期价格标识实现类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("rTenancyPriceId")
public class RTenancyPriceIdImpl implements RTenancyPriceId {

	/** Redis模板 */
	@Resource(name = "redisTemplate")
	private RedisTemplate<String, String> redisTemplate = null;

	/** 键值格式 */
	private static final String KEY = "platform:Id:TenancyPrice";

	/**
	 * 递增设备续期价格标识
	 * 
	 * @return 设备续期价格标识
	 */
	@Override
	public Long increment() {
		return redisTemplate.opsForValue().increment(KEY, 1L);
	}

}
