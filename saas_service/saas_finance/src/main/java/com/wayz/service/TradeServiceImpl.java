package com.wayz.service;

import com.wayz.constants.CTradeType;
import com.wayz.constants.ViewKeyValue;
import com.wayz.entity.pojo.device.DTenancyPrice;
import com.wayz.entity.pojo.space.DSpacePrice;
import com.wayz.entity.redis.device.RTenancyPrice;
import com.wayz.exception.ExceptionCode;
import com.wayz.exception.WayzException;
import com.wayz.mapper.DSpacePriceDAO;
import com.wayz.mapper.DTenancyPriceDAO;
import com.wayz.redis.RTenancyPriceId;
import com.wayz.redis.RTenancyPriceObject;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 交易服务实现类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Service("tradeService")
@org.apache.dubbo.config.annotation.Service(version = "1.0.0", protocol = "dubbo")
@RefreshScope
public class TradeServiceImpl implements TradeService {
	/** DAO相关 */
	/** 设备续期价格DAO */
	@Resource(name = "dTenancyPriceDAO")
	private DTenancyPriceDAO dTenancyPriceDAO;
	/** 空间价格DAO */
	@Resource(name = "dSpacePriceDAO")
	private DSpacePriceDAO dSpacePriceDAO;

	/** 缓存相关 */
	/** 设备续期价格标识 */
	@Resource(name = "rTenancyPriceId")
	private RTenancyPriceId rTenancyPriceId;
	/** 设备续期价格对象 */
	@Resource(name = "rTenancyPriceObject")
	private RTenancyPriceObject rTenancyPriceObject;

	/**
	 * 查询交易类型
	 *
	 * @param myId 我的客户标识
	 * @param type 交易分类(0:所有;1:充值;2:消费)
	 * @return 交易类型列表
	 * @throws WayzException 平台异常
	 */
	@Override
	public List<ViewKeyValue> queryTradeType(Long myId, Short type) throws WayzException {
		// 初始化
		List<ViewKeyValue> typeList = new ArrayList<ViewKeyValue>();

		// 依次赋值
		for (CTradeType cTradeType : CTradeType.values()) {
			if (type == 0 || cTradeType.getValue() / 10 == type) {
				ViewKeyValue viewKeyValue = new ViewKeyValue();
				viewKeyValue.setId(cTradeType.getValue());
				viewKeyValue.setName(cTradeType.getDescription());
				typeList.add(viewKeyValue);
			}
		}

		// 返回数据
		return typeList;
	}

	/**
	 * 创建媒体续期价格
	 *
	 * @param myId 我的客户标识
	 * @param days 天数
	 * @param name 显示名称
	 * @param salePrice 原价
	 * @param discountPrice 优惠价
	 * @return 价格标识
	 */
	@Override
	public Long createDevicePrice(Long myId, Integer days, String name, Double salePrice, Double discountPrice)
			throws WayzException {
		DTenancyPrice dTenancyPrice = dTenancyPriceDAO.getByDays(days);
		if (dTenancyPrice != null) {
			throw new WayzException(ExceptionCode.PARAMETER_ERROR, "授权天数已存在!");
		}
		// 初始化
		Long id = rTenancyPriceId.increment();

		// 新增数据
		DTenancyPrice create = new DTenancyPrice();
		create.setDays(days);
		create.setName(name);
		create.setSalePrice(salePrice);
		create.setDiscountPrice(discountPrice);
		dTenancyPriceDAO.create(id, create);

		// 维护缓存
		RTenancyPrice rTenancyPrice = new RTenancyPrice();
		rTenancyPrice.setDays(days);
		rTenancyPrice.setName(name);
		rTenancyPrice.setSalePrice(salePrice);
		rTenancyPrice.setDiscountPrice(discountPrice);
		rTenancyPriceObject.set(id, rTenancyPrice);

		// 返回数据
		return id;
	}

	/**
	 * 修改媒体续期价格
	 *
	 * @param myId 我的客户标识
	 * @param id 标识
	 * @param days 天数
	 * @param name 显示名称
	 * @param salePrice 原价
	 * @param discountPrice 优惠价
	 * @throws YungeException 平台异常
	 */
	@Override
	public void modifyDevicePrice(Long myId, Long id, Integer days, String name, Double salePrice, Double discountPrice)
			throws WayzException {
		DTenancyPrice dTenancyPrice = dTenancyPriceDAO.getByDays(days);
		if (dTenancyPrice != null && !dTenancyPrice.getId().equals(id)) {
			throw new WayzException(ExceptionCode.PARAMETER_ERROR, "授权天数已存在!");
		}
		// 修改数据
		DTenancyPrice modify = new DTenancyPrice();
		modify.setId(id);
		modify.setDays(days);
		modify.setName(name);
		modify.setSalePrice(salePrice);
		modify.setDiscountPrice(discountPrice);
		dTenancyPriceDAO.modify(modify);

		// 维护缓存
		RTenancyPrice rTenancyPrice = new RTenancyPrice();
		rTenancyPrice.setDays(days);
		rTenancyPrice.setName(name);
		rTenancyPrice.setSalePrice(salePrice);
		rTenancyPrice.setDiscountPrice(discountPrice);
		rTenancyPriceObject.set(id, rTenancyPrice);
	}

	/**
	 * 删除媒体续期价格
	 *
	 * @param myId 我的客户标识
	 * @param id 标识
	 * @throws YungeException 平台异常
	 */
	@Override
	public void deleteDevicePrice(Long myId, Long id) throws WayzException {
		// 删除数据
		dTenancyPriceDAO.delete(id);

		// 删除缓存
		rTenancyPriceObject.remove(id);
	}

	/**
	 * 查询媒体续期价格
	 *
	 * @param myId 我的客户标识
	 * @return 价格列表
	 * @throws YungeException 平台异常
	 */
	@Override
	public List<DTenancyPrice> queryDevicePrice(Long myId) throws WayzException {

		// 查询数据
		List<DTenancyPrice> dPriceList = dTenancyPriceDAO.query();

		// 返回数据
		return dPriceList;
	}

	/**
	 * 创建空间价格
	 *
	 * @param myId 我的客户标识
	 * @param space 空间(Gb)
	 * @param name 显示名称
	 * @param salePrice 原价
	 * @param discountPrice 优惠价
	 * @return 空间
	 * @throws YungeException 平台异常
	 */
	@Override
	public Integer createSpacePrice(Long myId, Integer space, String name, Double salePrice, Double discountPrice)
			throws WayzException {
		DSpacePrice dSpacePrice = dSpacePriceDAO.getBySpace(space);
		if (dSpacePrice != null) {
			throw new WayzException(ExceptionCode.PARAMETER_ERROR, "空间数已存在!");
		}

		// 新增数据
		DSpacePrice create = new DSpacePrice();
		create.setName(name);
		create.setSalePrice(salePrice);
		create.setDiscountPrice(discountPrice);
		dSpacePriceDAO.create(space, create);

		// 返回数据
		return space;
	}

	/**
	 * 修改空间价格
	 *
	 * @param myId 我的客户标识
	 * @param space 空间
	 * @param name 显示名称
	 * @param salePrice 原价
	 * @param discountPrice 优惠价
	 * @throws YungeException 平台异常
	 */
	@Override
	public void modifySpacePrice(Long myId, Integer space, String name, Double salePrice, Double discountPrice)
			throws WayzException {
		DSpacePrice dSpacePrice = dSpacePriceDAO.getBySpace(space);
		if (dSpacePrice != null && !dSpacePrice.getSpace().equals(space)) {
			throw new WayzException(ExceptionCode.PARAMETER_ERROR, "空间数已存在!");
		}
		// 修改数据
		DSpacePrice modify = new DSpacePrice();
		modify.setSpace(space);
		modify.setName(name);
		modify.setSalePrice(salePrice);
		modify.setDiscountPrice(discountPrice);
		dSpacePriceDAO.modify(modify);

	}

	/**
	 * 删除空间价格
	 *
	 * @param myId 我的客户标识
	 * @param space 空间
	 * @throws YungeException 平台异常
	 */
	@Override
	public void deleteSpacePrice(Long myId, Integer space) throws WayzException {
		// 删除数据
		dSpacePriceDAO.delete(space);

	}

	/**
	 * 查询空间价格
	 *
	 * @param myId 我的客户标识
	 * @return 价格列表
	 * @throws YungeException 平台异常
	 */
	@Override
	public List<DSpacePrice> querySpacePrice(Long myId) throws WayzException {

		// 查询数据
		List<DSpacePrice> dPriceList = dSpacePriceDAO.query();

		// 返回数据
		return dPriceList;
	}
}
