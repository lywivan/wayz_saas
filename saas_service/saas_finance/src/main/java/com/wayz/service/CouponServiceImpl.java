package com.wayz.service;

import com.wayz.constants.CCouponStatus;
import com.wayz.constants.CProductType;
import com.wayz.entity.pojo.coupon.CouponPage;
import com.wayz.entity.pojo.coupon.DCompanyCoupon;
import com.wayz.entity.pojo.coupon.DCoupon;
import com.wayz.exception.WayzException;
import com.wayz.mapper.DCompanyCouponDAO;
import com.wayz.mapper.DCouponDAO;
import com.wayz.redis.RCompanyCouponId;
import com.wayz.redis.RCouponId;
import com.wayz.util.CollectionUtils;
import com.wayz.util.TimeHelper;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 代金券服务实现类
 * 
 * @author yinjy
 *
 */
@Service(version = "1.0.0", protocol = "dubbo")
@RefreshScope
public class CouponServiceImpl implements CouponService {
	/** DAO 相关 */
	/** 代金券DAO */
	@Resource(name = "dCouponDAO")
	private DCouponDAO dCouponDAO = null;

	/** 对象相关 */
	/** 代金券标识接口 */
	@Resource(name = "rCouponId")
	private RCouponId rCouponId = null;

	/** 公司代金券标识接口 */
	@Resource(name = "rCompanyCouponId")
	private RCompanyCouponId rCompanyCouponId = null;

	/** 公司代金券DAO */
	@Resource(name = "dCompanyCouponDAO")
	private DCompanyCouponDAO dCompanyCouponDAO = null;

	/**
	 * 查询代金券
	 *
	 * @param myId 我的客户标识
	 * @param productType 产品类型(0:充值; 1:点位新购; 2:点位续期; 3:空间; 4:直播服务)
	 * @return 代金券分页
	 * @throws WayzException 平台异常
	 */
	@Override
	public CouponPage queryCoupon(Long myId, Short productType, Boolean isEnable, Integer startIndex, Integer pageSize)
			throws WayzException {
		// 初始化
		CouponPage couponPage = new CouponPage();
		List<DCoupon> couponList = new ArrayList<DCoupon>();

		// 查询统计数据
		Integer totalCount = dCouponDAO.count(productType, isEnable);
		List<DCoupon> dCouponList = dCouponDAO.query(productType, isEnable, startIndex, pageSize);
		if (CollectionUtils.isNotEmpty(dCouponList)) {
			for (DCoupon dCoupon : dCouponList) {
				// 初始化
				DCoupon coupon = new DCoupon();

				// 设置参数
				coupon.setId(dCoupon.getId());
				coupon.setName(dCoupon.getName());
				coupon.setTitle(dCoupon.getTitle());
				coupon.setProductType(dCoupon.getProductType());
				coupon.setProductTypeName(CProductType.getDescription(dCoupon.getProductType()));
				coupon.setFullAmount(dCoupon.getFullAmount());
				coupon.setReduceAmount(dCoupon.getReduceAmount());
				coupon.setProvideNumber(dCoupon.getProvideNumber());
				coupon.setBalanceNumber(dCoupon.getBalanceNumber());
				coupon.setValidDays(dCoupon.getValidDays());
				coupon.setIsEnable(dCoupon.getIsEnable());
				coupon.setIsPublic(dCoupon.getIsPublic());
				// coupon.setCreatorName(dCoupon.getCreatorId());
				coupon.setCreatedTime(dCoupon.getCreatedTime());
				couponList.add(coupon);
			}
		}
		// 设置参数
		couponPage.setTotalCount(totalCount);
		couponPage.setCouponList(couponList);

		// 返回数据
		return couponPage;
	}

	/**
	 * 创建代金券
	 *
	 * @param myId 我的客户标识
	 * @param name 名称
	 * @param title 标题描述
	 * @param productType 产品类型(0:充值; 1:点位新购; 2:点位续期; 3:空间; 4:直播服务)
	 * @param fullAmount 最低消费限制
	 * @param reduceAmount 可减免金额
	 * @param provideNumber 发放数量
	 * @param isPublic 是否公开
	 * @return 代金券标识
	 */
	@Override
	public Long createCoupon(Long myId, String name, String title, Short productType, Double fullAmount,
							 Double reduceAmount, Integer provideNumber, Integer validDays,

							 Boolean isPublic) throws WayzException {
		// 初始化
		Long id = rCouponId.increment();

		// 初始化
		DCoupon dCoupon = new DCoupon();

		// 设置参数
		dCoupon.setName(name);
		dCoupon.setTitle(title);
		dCoupon.setProductType(productType);
		dCoupon.setFullAmount(fullAmount);
		dCoupon.setReduceAmount(reduceAmount);
		dCoupon.setProvideNumber(provideNumber);
		dCoupon.setBalanceNumber(provideNumber);
		dCoupon.setValidDays(validDays);
		dCoupon.setIsEnable(true);
		dCoupon.setIsPublic(isPublic);
		dCoupon.setCreatorId(myId);
		dCouponDAO.create(id, dCoupon);

		// 返回数据
		return id;
	}

	/**
	 * 修改代金券
	 *
	 * @param myId 我的客户标识
	 * @param id 标识
	 * @param name 名称
	 * @param title 标题描述
	 * @param productType 产品类型(0:充值; 1:点位新购; 2:点位续期; 3:空间; 4:直播服务)
	 * @param fullAmount 最低消费限制
	 * @param reduceAmount 可减免金额
	 * @param provideNumber 发放数量
	 * @param isPublic 是否公开
	 */
	@Override
	public void modifyCoupon(Long myId, Long id, String name, String title, Short productType, Double fullAmount,
							 Double reduceAmount, Integer provideNumber, Integer validDays, Boolean isPublic) throws WayzException {
		// 初始化
		DCoupon dCoupon = new DCoupon();

		// 设置参数
		dCoupon.setId(id);
		dCoupon.setName(name);
		dCoupon.setTitle(title);
		dCoupon.setProductType(productType);
		dCoupon.setFullAmount(fullAmount);
		dCoupon.setReduceAmount(reduceAmount);
		dCoupon.setProvideNumber(provideNumber);
		// dCouponModify.setBalanceNumber(balanceNumber);
		dCoupon.setValidDays(validDays);
		// dCouponModify.setIsEnable(true);
		dCoupon.setIsPublic(isPublic);
		// dCouponModify.setCreatorId(myId);
		dCouponDAO.modify(dCoupon);
	}

	/**
	 * 失效代金券
	 *
	 * @param myId 我的客户标识
	 * @param id 标识
	 */
	@Override
	public void enableCoupon(Long myId, Long id, Boolean isEnable) throws WayzException {
		// 初始化
		DCoupon dCoupon = new DCoupon();

		// 设置参数
		dCoupon.setId(id);

		dCoupon.setIsEnable(isEnable);
		dCouponDAO.modify(dCoupon);
	}

	/**
	 * 分配代金券
	 *
	 * @param myId 我的客户标识
	 * @param couponId 代金券标识
	 * @param companyId 公司标识
	 * @param days 有效天数
	 * @throws YungeException 平台异常
	 */
	@Override
	public void distributeCoupon(Long myId, Long couponId, Long companyId, Integer days) throws WayzException {
		// 获取代金券数据
		DCoupon dCoupon = dCouponDAO.get(couponId);
		if (dCoupon == null) {
			throw new WayzException("代金券不存在!");
		}
		// 创建公司代金券
		DCompanyCoupon dCompanyCoupon = new DCompanyCoupon();
		// 设置参数
		dCompanyCoupon.setCompanyId(companyId);
		dCompanyCoupon.setCouponId(couponId);
		dCompanyCoupon.setName(dCoupon.getName());
		dCompanyCoupon.setTitle(dCoupon.getTitle());
		dCompanyCoupon.setProductType(dCoupon.getProductType());
		dCompanyCoupon.setFullAmount(dCoupon.getFullAmount());
		dCompanyCoupon.setReduceAmount(dCoupon.getReduceAmount());
		dCompanyCoupon.setStatus(CCouponStatus.USABLE.getValue());
		dCompanyCoupon.setBeginDate(TimeHelper.getDate(TimeHelper.getDate()));
		dCompanyCoupon.setExpireDate(TimeHelper.getDate(dCompanyCoupon.getBeginDate(), days));
		dCompanyCoupon.setProviderId(myId);
		// 创建数据
		dCompanyCouponDAO.create(rCompanyCouponId.increment(), dCompanyCoupon);
		// dCompanyCoupon.setCreatedTime(createdTime);
	}
}
