package com.wayz;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@MapperScan("com.wayz.mapper")
@SpringBootApplication(scanBasePackages = {"com.wayz"})
public class FinanceProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(FinanceProviderApplication.class, args);
    }
}
