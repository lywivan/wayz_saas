package com.wayz.mapper;

import com.wayz.entity.pojo.company.DPositionGroupExt;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 媒体分组DAO接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("dPositionGroupDAO")
public interface DPositionGroupDAO {

	/**
	 * 是否存在名称
	 *
	 * @param name 名称
	 * @return 存在
	 */
	public Boolean exists(@Param("companyId") Long companyId, @Param("name") String name);

	/**
	 * 创建媒体分组
	 *
	 * @param id 播控器组标识
	 * @param create 媒体分组创建
	 * @return 创建行数
	 */
	public Integer create(@Param("id") Long id, @Param("create") DPositionGroupExt dPositionGroupExt);
}
