package com.wayz.redis.impl;

import javax.annotation.*;

import com.wayz.redis.RPositionGroupId;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.*;

/**
 * 媒体分组标识实现类
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("rPositionGroupId")
public class RPositionGroupIdImpl implements RPositionGroupId {

	/** Redis模板 */
	@Resource(name = "redisTemplate")
	private RedisTemplate<String, String> redisTemplate = null;

	/** 键值格式 */
	private static final String KEY = "platform:Id:PositionGroup";

	/**
	 * 递增媒体分组标识
	 * 
	 * @return 媒体分组标识
	 */
	@Override
	public Long increment() {
		return redisTemplate.opsForValue().increment(KEY, 1L);
	}

}
