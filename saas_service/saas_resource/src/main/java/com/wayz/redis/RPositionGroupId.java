package com.wayz.redis;

/**
 * 媒体分组标识接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
public interface RPositionGroupId {

	/**
	 * 递增媒体分组标识
	 * 
	 * @return 媒体分组标识
	 */
	public Long increment();

}
