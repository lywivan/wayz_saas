package com.wayz.service;

import com.wayz.entity.pojo.company.DPositionGroupExt;
import com.wayz.entity.redis.company.RCompany;
import com.wayz.entity.redis.company.RDepartment;
import com.wayz.entity.redis.company.RPositionGroup;
import com.wayz.exception.ExceptionCode;
import com.wayz.exception.WayzException;
import com.wayz.mapper.DPositionGroupDAO;
import com.wayz.redis.RCompanyObject;
import com.wayz.redis.RDepartmentObject;
import com.wayz.redis.RPositionGroupId;
import com.wayz.redis.RPositionGroupObject;
import com.wayz.service.PositionService;
import com.wayz.util.TimeHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.MessageFormat;

/**
 * 设备服务实现类
 *
 * @author wade.liu@wayz.ai
 */
@EnableAsync
@Service("positionService")
@org.apache.dubbo.config.annotation.Service(version = "1.0.0", protocol = "dubbo")
@RefreshScope
public class PositionServiceImpl implements PositionService {

	/** 媒体分组DAO */
	@Resource(name = "dPositionGroupDAO")
	private DPositionGroupDAO dPositionGroupDAO = null;
	/** 标识相关 */
	/** 设备分组标识接口 */
	@Resource(name = "rPositionGroupId")
	private RPositionGroupId rPositionGroupId;
	/** 部门对象 */
	@Resource(name = "rDepartmentObject")
	private RDepartmentObject rDepartmentObject;
	/** 媒体分组对象 */
	@Resource(name = "rPositionGroupObject")
	private RPositionGroupObject rPositionGroupObject = null;
	/** 公司对象 */
	@Resource(name = "rCompanyObject")
	private RCompanyObject rCompanyObject;

	/**
	 * 创建媒体组
	 *
	 * @param myId 我的客户标识
	 * @param name 名称
	 * @param description 描述
	 * @return 媒体组标识
	 * @throws WayzException 平台异常
	 */
	@Override
	public Long createPositionGroup(Long myId, Long companyId, Long departmentId, String name, String description,
									Long sceneId, Long templetId, String playVersion, String openReserve, String playReserve, String startTime,
									String closeTime, Boolean autoOnOff, String admin, String adminPhone, Short volumn, Integer rotation,
									Boolean isAutoStart, Boolean isDefault) throws WayzException {

		//int i =1/0;
		// 校验名称是否存在
		if (dPositionGroupDAO.exists(companyId, name)) {
			throw new WayzException(ExceptionCode.PARAMETER_ERROR, MessageFormat.format("媒体组的名称{0}已经存在!", name));
		}

		// 检查部门
		String departPath = null;
		RDepartment rDepartment = rDepartmentObject.get(companyId, departmentId);
		if (rDepartment == null) {
			throw new WayzException(ExceptionCode.PARAMETER_ERROR, MessageFormat.format("部门{0}不存在!", departmentId));
		}
		if (StringUtils.isEmpty(rDepartment.getPath())) {
			departPath = "0/";
		}
		else {
			departPath = rDepartment.getPath();
		}

		// 创建数据
		DPositionGroupExt dPositionGroupExt = new DPositionGroupExt();
		Long id = rPositionGroupId.increment();
		dPositionGroupExt.setCompanyId(companyId);
		dPositionGroupExt.setDepartPath(departPath);
		dPositionGroupExt.setName(name);
		dPositionGroupExt.setDescription(description);
		dPositionGroupExt.setSceneId(sceneId);
		dPositionGroupExt.setStartTime(TimeHelper.getTime(startTime));
		dPositionGroupExt.setCloseTime(TimeHelper.getTime(closeTime));
		dPositionGroupExt.setAdmin(admin);
		dPositionGroupExt.setAdminPhone(adminPhone);
		dPositionGroupExt.setCreatorId(myId);
		dPositionGroupExt.setVolumn(volumn);
		dPositionGroupExt.setRotation(rotation);
		dPositionGroupExt.setIsAutoStart(isAutoStart);
		dPositionGroupExt.setAutoOnOff(autoOnOff);
		dPositionGroupExt.setTempletId(templetId);
		dPositionGroupExt.setPlayVersion(playVersion);
		dPositionGroupExt.setPlayReserve(playReserve);
		dPositionGroupExt.setOpenReserve(openReserve);
		dPositionGroupDAO.create(id, dPositionGroupExt);

		// 维护缓存
		RPositionGroup rPositionGroup = new RPositionGroup();
		rPositionGroup.setCompanyId(companyId);
		rPositionGroup.setDepartPath(departPath);
		rPositionGroup.setName(name);
		rPositionGroup.setStartTime(startTime);
		rPositionGroup.setCloseTime(closeTime);
		rPositionGroup.setPhone(adminPhone);
		rPositionGroup.setVolumn(volumn);
		rPositionGroup.setRotation(rotation);
		rPositionGroup.setIsAutoStart(isAutoStart);
		rPositionGroup.setSubtitleTempletId(templetId);
		rPositionGroup.setPlayVersion(playVersion);
		rPositionGroup.setPlayReserve(playReserve);
		rPositionGroup.setOpenReserve(openReserve);
		rPositionGroupObject.set(id, rPositionGroup);

		// 维护默认组
		if (isDefault == true) {
			rCompanyObject.setLong(companyId, RCompany.GROUPID, id);
		}

		// 返回
		return id;
	}
}
