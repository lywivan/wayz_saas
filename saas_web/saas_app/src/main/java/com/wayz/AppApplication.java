package com.wayz;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 播控端启动类
 *
 * @author:ivan.liu
 */
@SpringBootApplication(exclude={DataSourceAutoConfiguration.class},scanBasePackages = {"com.wayz"})
@EnableDiscoveryClient
@MapperScan("com.wayz.mapper")
public class AppApplication {
    public static void main(String[] args) {
        SpringApplication.run(AppApplication.class,args);
    }
}
