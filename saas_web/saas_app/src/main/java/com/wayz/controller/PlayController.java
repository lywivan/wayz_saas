package com.wayz.controller;

import com.wayz.exception.WayzException;
import com.wayz.response.WayzResponse;
import com.wayz.service.PlayApiService;
import com.wayz.validator.ParameterValidator;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * <p>播控控制器类</p>
 * 
 *  
 * @author mahao
 */
@RestController
@RequestMapping("/play")
public class PlayController {

	/** 播控代理 */
	@Reference(version = "1.0.0", check = false)
	private PlayApiService playApiService = null;

	/**
	 * 获取播放任务
	 * 
	 * @param mac 播控设备MAC地址
	 * @param osType 操作系统类型(1:ios; 2:Android;3:windows)
	 * @param imei 播控设备IMEI号
	 * @param version 本机播控系统软件版本
	 * @param commType 通信类型(1:WIFI; 2:GSM; 3:有线)
	 * @param isInitial 是否为初始(true为初始; false为正常执行任务)
	 * @param ipAddr 远程主机IP
	 * @param ts 系统时间戳(精确到秒)
	 * @param st 当前状态(0:空闲;1:正常播放;2:正在更新操作系统; 3:正在更新播放器程序)
	 * @param planId 当前播放的节目单标识
	 * @param signal 信号强度
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@RequestMapping(value = "/getTask", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse getTask(@RequestParam(value = "mac", required = false) String mac,
								@RequestParam(value = "osType", required = false) Short osType,
								@RequestParam(value = "imei", required = false) String imei,
								@RequestParam(value = "version", required = false) String version,
								@RequestParam(value = "commType", required = false) Short commType,
								@RequestParam(value = "isInitial", required = false) Boolean isInitial,
								@RequestParam(value = "ipAddr", required = false) String ipAddr,
								@RequestParam(value = "ts", required = false) Long ts,
								@RequestParam(value = "st", required = false) Short st,
								@RequestParam(value = "planId", required = false) Long planId,
								@RequestParam(value = "menuId", required = false) Long menuId,
								@RequestParam(value = "signal", required = false) Integer signal) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数:
		ParameterValidator.notNull(mac, "播控设备网卡MAC地址");

		// 调用接口
		$response.setData(playApiService.getTask(mac, osType, imei, version, commType, isInitial, ipAddr, ts, st, planId,
				menuId, signal));

		// 返回应答
		return $response;
	}

	/**
	 * 获取直播url
	 * 
	 * @param mac 播控设备MAC地址
	 * @param taskId 播控任务标识
	 * @return 直播url
	 * @throws WayzException 平台异常
	 */
	@RequestMapping(value = "/getLiveUrl", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse getLiveUrl(@RequestParam(value = "mac", required = false) String mac,
			@RequestParam(value = "taskId", required = false) Long taskId) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		ParameterValidator.notNull(mac, "播控设备网卡MAC地址");
		ParameterValidator.notNull(taskId, "直播任务ID");
		// 调用接口
		$response.setData(playApiService.getLiveUrl(mac, taskId));
		// 返回应答
		return $response;
	}

	/**
	 * 获取节目单任务
	 * 
	 * @param mac 播控设备MAC地址
	 * @param taskId 播控任务标识
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@RequestMapping(value = "/getMenu", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse getMenu(@RequestParam(value = "mac", required = false) String mac,
			@RequestParam(value = "taskId", required = false) Long taskId) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数:
		ParameterValidator.notNull(mac, "播控设备网卡MAC地址");
		// 验证参数:
		ParameterValidator.notNull(taskId, "播控任务标识");

		// 调用接口
		$response.setData(playApiService.getMenu(mac, taskId));

		// 返回应答
		return $response;
	}

	/**
	 * 获取屏幕素材任务
	 * 
	 * @param mac 播控设备MAC地址
	 * @param taskId 播控任务标识
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	/*@RequestMapping(value = "/getScreenMaterial", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse getScreenMaterial(@RequestParam(value = "mac", required = false) String mac,
			@RequestParam(value = "taskId", required = false) Long taskId) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数:
		ParameterValidator.notNull(mac, "播控设备网卡MAC地址");
		// 验证参数:
		ParameterValidator.notNull(taskId, "播控任务标识");

		// 调用接口
		$response.setData(playProxy.getScreenMaterial(mac, taskId));

		// 返回应答
		return $response;
	}*/

	/**
	 * 获取屏幕素材任务V2
	 * 
	 * @param mac 播控设备MAC地址
	 * @param taskId 播控任务标识
	 * @return platform应答
	 * @throws WayzException 平台异常
	 *//*
	@RequestMapping(value = "/getScreenMaterialV2", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse getScreenMaterialV2(@RequestParam(value = "mac", required = false) String mac,
			@RequestParam(value = "taskId", required = false) Long taskId) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数:
		ParameterValidator.notNull(mac, "播控设备网卡MAC地址");
		// 验证参数:
		ParameterValidator.notNull(taskId, "播控任务标识");

		// 调用接口
		$response.setData(playProxy.getScreenMaterialV2(mac, taskId));

		// 返回应答
		return $response;
	}

	*//**
	 * 获取联屏屏幕素材任务
	 * 
	 * @param mac 播控设备MAC地址
	 * @param taskId 播控任务标识
	 * @return platform应答
	 * @throws WayzException 平台异常
	 *//*
	@RequestMapping(value = "/getJoinScreenMaterial", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse getJoinScreenMaterial(@RequestParam(value = "mac", required = false) String mac,
			@RequestParam(value = "taskId", required = false) Long taskId) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数:
		ParameterValidator.notNull(mac, "播控设备网卡MAC地址");
		// 验证参数:
		ParameterValidator.notNull(taskId, "播控任务标识");

		// 调用接口
		$response.setData(playProxy.getJoinScreenMaterial(mac, taskId));

		// 返回应答
		return $response;
	}

	*//**
	 * 获取直播屏幕素材任务
	 * 
	 * @param mac 播控设备MAC地址
	 * @param taskId 播控任务标识
	 * @return platform应答
	 * @throws WayzException 平台异常
	 *//*
	@RequestMapping(value = "/getLiveScreenMaterial", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse getLiveScreenMaterial(@RequestParam(value = "mac", required = false) String mac,
			@RequestParam(value = "taskId", required = false) Long taskId,
			@RequestParam(value = "width", required = false) Integer width,
			@RequestParam(value = "height", required = false) Integer height) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		ParameterValidator.notNull(mac, "播控设备网卡MAC地址");
		ParameterValidator.notNull(taskId, "直播任务标识");
		ParameterValidator.notNull(width, "设备宽");
		ParameterValidator.notNull(height, "设备高");

		// 调用接口
		$response.setData(playProxy.getLiveScreenMaterial(mac, taskId, width, height));

		// 返回应答
		return $response;
	}

	*//**
	 * 获取紧急插播图层定义
	 * 
	 * @param mac 播控设备MAC地址
	 * @param taskId 播控任务标识
	 * @return platform应答
	 * @throws WayzException 平台异常
	 *//*
	@RequestMapping(value = "/getEmergentLayer", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse getEmergentLayer(@RequestParam(value = "mac", required = false) String mac,
			@RequestParam(value = "taskId", required = false) Long taskId) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数: 播控设备MAC地址
		ParameterValidator.notNull(mac, "播控设备MAC地址");
		// 验证参数: 播控任务标识
		ParameterValidator.notNull(taskId, "播控任务标识");

		// 调用接口
		$response.setData(playProxy.getEmergentLayer(mac, taskId));

		// 返回应答
		return $response;
	}

	*//**
	 * 任务应答
	 * 
	 * @param mac 播控设备MAC地址
	 * @param taskId 任务标识
	 * @return platform应答
	 * @throws WayzException 平台异常
	 *//*
	@RequestMapping(value = "/answerTask", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse answerTask(@RequestParam(value = "mac", required = false) String mac,
			@RequestParam(value = "taskId", required = false) Long taskId,
			@RequestParam(value = "type", required = false) Short type) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数: 播控设备MAC地址
		ParameterValidator.notNull(mac, "播控设备MAC地址");
		// 验证参数: 任务标识
		ParameterValidator.notNull(taskId, "任务标识");
		// 验证参数: 任务类型
		ParameterValidator.notNull(type, "任务类型");

		// 调用接口
		playProxy.answerTask(mac, taskId, type);

		// 返回应答
		return $response;
	}

	*//**
	 * 获取播控器配置信息
	 * 
	 * @param mac 播控器mac地址
	 * @return platform应答
	 * @throws WayzException 平台异常
	 *//*
	@RequestMapping(value = "/getMyInfo", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse getMyInfo(@RequestParam(value = "mac", required = false) String mac) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数:
		ParameterValidator.notNull(mac, "播控设备网卡MAC地址");

		// 调用接口
		$response.setData(playProxy.getMyInfo(mac));

		// 返回应答
		return $response;
	}

	*//**
	 * 获取联屏配置信息
	 * 
	 * @param mac 播控器mac地址
	 * @return platform应答
	 * @throws WayzException 平台异常
	 *//*
	@RequestMapping(value = "/getJoinInfo", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse getJoinInfo(@RequestParam(value = "mac", required = false) String mac) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数:
		ParameterValidator.notNull(mac, "播控设备网卡MAC地址");

		// 调用接口
		$response.setData(playProxy.getJoinInfo(mac));

		// 返回应答
		return $response;
	}

	*//**
	 * 获取播控器最新版本
	 * 
	 * @param mac 播控器mac地址
	 * @param version 播控器版本
	 * @return platform应答
	 * @throws WayzException 平台异常
	 *//*
	@RequestMapping(value = "/getPackageVersion", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse getPackageVersion(@RequestParam(value = "mac", required = false) String mac,
			@RequestParam(value = "version", required = false) String version) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数:
		ParameterValidator.notNull(mac, "播控设备网卡MAC地址");
		ParameterValidator.notNull(version, "播控软件版本");

		// 调用接口
		$response.setData(playProxy.getPackageVersion(mac, version));

		// 返回应答
		return $response;
	}

	*//**
	 * 获取设备上投放中的广告计划
	 * 
	 * @param mac 播控器mac地址
	 * @return platform应答
	 * @throws WayzException 平台异常
	 *//*
	@RequestMapping(value = "/getPositionPuttingPlan", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse getPositionPuttingPlan(@RequestParam(value = "mac", required = false) String mac,
			@RequestParam(value = "width", required = false) Integer width,
			@RequestParam(value = "height", required = false) Integer height) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		ParameterValidator.notNull(mac, "播控设备网卡MAC地址");
		ParameterValidator.notNull(width, "设备宽");
		ParameterValidator.notNull(height, "设备高");

		// 调用接口
		$response.setData(playProxy.getPositionPuttingPlan(mac, width, height));

		// 返回应答
		return $response;
	}

	*//**
	 * 获取垫片版本
	 * 
	 * @param mac 播控器mac地址
	 * @param version 垫片版本
	 * @return platform应答
	 * @throws WayzException 平台异常
	 *//*
	@RequestMapping(value = "/getReserveImageVersion", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse getReserveImageVersion(@RequestParam(value = "mac", required = false) String mac,
			@RequestParam(value = "version", required = false) String version) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数:
		ParameterValidator.notNull(mac, "播控设备网卡MAC地址");
		ParameterValidator.notNull(version, "播控垫片版本");

		// 调用接口
		$response.setData(playProxy.getReserveImageVersion(mac, version));

		// 返回应答
		return $response;
	}

	*//**
	 * 上报心跳
	 * 
	 * @param mac 播控设备地址
	 * @param ts 系统时间戳(精确到秒)
	 * @param st 当前状态(0:空闲;1:正常播放;2:正在更新操作系统; 3:正在更新播放器程序)
	 * @param menuId 当前播放的节目单标识
	 * @param netType 网络类型(1:WiFi;2:3G;3:VPN;4:LAN)
	 * @param signal 信号强度
	 * @return platform应答
	 * @throws WayzException 平台异常
	 *//*
	@RequestMapping(value = "/heartbeat", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse heartbeat(@RequestParam(value = "mac", required = false) String mac,
			@RequestParam(value = "ts", required = false) Long ts,
			@RequestParam(value = "st", required = false) Short st,
			@RequestParam(value = "menuId", required = false) Long menuId,
			@RequestParam(value = "netType", required = false) Short netType,
			@RequestParam(value = "signal", required = false) Integer signal) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数:播控设备网卡MAC地址
		ParameterValidator.notNull(mac, "播控设备网卡MAC地址");
		// 验证参数:网络类型
		ParameterValidator.notNull(netType, "网络类型");

		// 调用接口
		playProxy.heartbeat(mac, ts, st, menuId, netType, signal);

		// 返回应答
		return $response;
	}

	*//**
	 * 上报播控系统信息
	 * 
	 * @param mac 播控mac地址
	 * @param sysInfo 播控系统信息
	 * @return platform应答
	 * @throws WayzException 平台异常
	 *//*
	@RequestMapping(value = "/reportSystemInfo", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse reportSystemInfo(@RequestParam(value = "mac", required = false) String mac,
			@RequestJsonParam(value = "sysInfo") PlaySystemInfo sysInfo) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数:
		ParameterValidator.notNull(mac, "播控设备网卡MAC地址");
		// 验证参数:播控系统信息
		ParameterValidator.notNull(sysInfo, "播控系统信息");

		// 调用接口
		playProxy.reportSystemInfo(mac, sysInfo);

		// 返回应答
		return $response;
	}

	*//**
	 * 上报播控告警信息
	 * 
	 * @param mac 播控设备地址
	 * @param type 告警类型(1:素材下载失败;2:无相应素材;3:显示器分辨率不符;4:新服务器连接失败;5:
	 *            配置文件解析异常;6:播放Menu为空;7:Crash log;8:内存使用率过高;9:磁盘已满)
	 * @param content 播控告警内容
	 * @return platform应答
	 * @throws WayzException 平台异常
	 *//*
	@RequestMapping(value = "/reportAlertInfo", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse reportAlertInfo(@RequestParam(value = "mac", required = false) String mac,
			@RequestParam(value = "type", required = false) Short type,
			@RequestParam(value = "content", required = false) String content) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数:播控设备网卡MAC地址
		ParameterValidator.notNull(mac, "播控设备网卡MAC地址");
		// 验证参数:告警类型
		ParameterValidator.notNull(type, "告警类型");

		// 调用接口
		playProxy.reportAlertInfo(mac, type, content);

		// 返回应答
		return $response;
	}

	*//**
	 * 上报播控截屏日志
	 * 
	 * @param mac 播控设备MAC地址
	 * @param logList 日志列表
	 * @param zipFile 图片压缩包文件
	 * @return platform应答
	 * @throws WayzException 平台异常
	 *//*
	@RequestMapping(value = "/reportSnapshotLog", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse reportSnapshotLog(@RequestParam(value = "mac", required = false) String mac,
			@RequestJsonParam(value = "logList") List<SnapshotLog> logList,
			@RequestParam(value = "zipFile", required = false) MultipartFile zipFile) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数:播控设备网卡MAC地址
		ParameterValidator.notNull(mac, "播控设备网卡MAC地址");
		// 验证参数:播控设备网卡MAC地址
		ParameterValidator.notNull(logList, "日志列表");

		// 调用接口
		playProxy.reportSnapshotLog(mac, logList, zipFile);

		// 返回应答
		return $response;
	}

	*//**
	 * 上报直播状态
	 * 
	 * @param mac 播控设备MAC地址
	 * @param liveId 直播标识
	 * @param liveStatus 直播状态：0 开始播放; 1正常播放； 2 结束播放
	 * @param duration 持续播放时长(秒)
	 * @param commType 通信类型(1:WIFI; 2:GSM; 3:有线)
	 * @param ts 系统时间戳(精确到秒)
	 * @param signal 信号强度
	 * @return platform应答
	 * @throws WayzException 平台异常
	 *//*
	@RequestMapping(value = "/reportLiveEvent", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse reportLiveEvent(@RequestParam(value = "mac", required = false) String mac,
			@RequestParam(value = "liveId", required = false) Long liveId,
			@RequestParam(value = "liveStatus", required = false) Short liveStatus,
			@RequestParam(value = "duration", required = false) Integer duration,
			@RequestParam(value = "commType", required = false) Short commType,
			@RequestParam(value = "ts", required = false) Long ts,
			@RequestParam(value = "signal", required = false) Integer signal) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数:播控设备网卡MAC地址
		ParameterValidator.notNull(mac, "播控设备网卡MAC地址");
		// 验证参数:直播标识
		ParameterValidator.notNull(liveId, "直播标识");
		// 验证参数:直播状态
		ParameterValidator.notNull(liveStatus, "直播状态");
		// 验证参数:持续播放时长
		ParameterValidator.notNull(duration, "持续播放时长");
		// 验证参数:时间戳
		ParameterValidator.notNull(ts, "时间戳");

		// 调用接口
		playProxy.reportLiveEvent(mac, liveId, liveStatus, duration, commType, ts, signal);

		// 返回应答
		return $response;
	}

	*//**
	 * 上报直播截屏日志
	 * 
	 * @param mac 播控设备MAC地址
	 * @param logList 日志列表
	 * @param zipFile 图片压缩包文件
	 * @return platform应答
	 * @throws WayzException 平台异常
	 *//*
	@RequestMapping(value = "/reportLiveSnapshotLog", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse reportLiveSnapshotLog(@RequestParam(value = "mac", required = false) String mac,
			@RequestJsonParam(value = "logList") List<LiveSnapshotLog> logList,
			@RequestParam(value = "zipFile", required = false) MultipartFile zipFile) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数:播控设备网卡MAC地址
		ParameterValidator.notNull(mac, "播控设备网卡MAC地址");
		// 验证参数:播控设备网卡MAC地址
		ParameterValidator.notNull(logList, "日志列表");

		// 调用接口
		playProxy.reportLiveSnapshotLog(mac, logList, zipFile);

		// 返回应答
		return $response;
	}

	*//**
	 * 上报直播截屏文件
	 * 
	 * @param mac 播控设备MAC地址
	 * @param liveId 直播标识
	 * @param imgFile 图片文件
	 * @return platform应答
	 * @throws WayzException 平台异常
	 *//*
	@ResponseBody
	@RequestMapping(value = "/reportLiveSnapshot", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse reportLiveSnapshot(@RequestParam(value = "mac", required = false) String mac,
			@RequestParam(value = "liveId", required = false) Long liveId,
			@RequestParam(value = "imgFile", required = false) MultipartFile imgFile) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数: 播控设备MAC地址
		ParameterValidator.notNull(mac, "播控设备MAC地址");
		// 验证参数:直播标识
		ParameterValidator.notNull(liveId, "直播标识");
		// 验证参数: 图片文件
		ParameterValidator.notNull(imgFile, "图片文件");

		// 调用接口
		playProxy.reportLiveSnapshot(mac, liveId, imgFile);

		// 返回应答
		return $response;
	}

	*//**
	 * 上报播控日志
	 * 
	 * @param mac 播控设备MAC地址
	 * @param logList 日志列表
	 * @return platform应答
	 * @throws WayzException 平台异常
	 *//*
	@RequestMapping(value = "/reportPlayLog", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse reportPlayLog(@RequestParam(value = "mac", required = false) String mac,
			@RequestJsonParam(value = "logList") List<PlayLog> logList) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();
		
		// 验证参数
		// 验证参数: 播控设备MAC地址
		ParameterValidator.notNull(mac, "播控设备MAC地址");
		
		// 验证参数: 日志列表
		ParameterValidator.notNull(logList, "日志列表");
		if (logList != null && logList.size() > 50) {
			System.out.println(mac + "收到播放日志过大，size：" + logList.size());
		}

		// 调用接口
		playProxy.reportPlayLog(mac, logList);

		// 返回应答
		return $response;
	}

	*//**
	 * 上报dsp广告播放日志
	 * 
	 * @param mac 播控设备MAC地址
	 * @param playCode 播控器编码
	 * @param planId 广告计划标识
	 * @param requestId 请求标识
	 * @param channelId 渠道标识(1:百度众盟)
	 * @param fileUrl 文件下载地址
	 * @param fileName 文件名
	 * @param type 文件类型(1:图片;2:视频)
	 * @param duration 播放时长(秒)
	 * @param beginTime 开始播放时间(yyyy-mm-dd hh:MM:ss)
	 * @param endTime 结束播放时间(yyyy-mm-dd hh:MM:ss)
	 * @return platform应答
	 * @throws WayzException 平台异常
	 *//*
	@RequestMapping(value = "/reportDspPlayLog", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse reportDspPlayLog(@RequestParam(value = "mac", required = false) String mac,
			@RequestParam(value = "playCode", required = false) String playCode,
			@RequestParam(value = "planId", required = false) Long planId,
			@RequestParam(value = "requestId", required = false) String requestId,
			@RequestParam(value = "channelId", required = false) Short channelId,
			@RequestParam(value = "fileUrl", required = false) String fileUrl,
			@RequestParam(value = "fileName", required = false) String fileName,
			@RequestParam(value = "type", required = false) Short type,
			@RequestParam(value = "duration", required = false) Integer duration,
			@RequestParam(value = "beginTime", required = false) String beginTime,
			@RequestParam(value = "endTime", required = false) String endTime) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数:
		if ((mac == null || mac.isEmpty()) && (playCode == null || playCode.isEmpty())) {
			throw new WayzException("播控设备MAC或编码至少应填写一项");
		}
		// 验证参数:请求标识
		ParameterValidator.notNull(requestId, "请求标识");
		// 验证参数:渠道标识
		ParameterValidator.notNull(channelId, "渠道标识");
		// 验证参数:渠道标识
		ParameterValidator.notNull(fileUrl, "文件url");
		// 验证参数:开始播放时间
		ParameterValidator.notNull(beginTime, "开始播放时间");
		// 验证参数:结束播放时间
		ParameterValidator.notNull(endTime, "结束播放时间");

		// 调用接口
		playProxy.reportDspPlayLog(mac, playCode, planId, requestId, channelId, fileUrl, fileName, type, duration,
				beginTime, endTime);

		// 返回应答
		return $response;
	}

	*//**
	 * 上报dsp广告播放日志列表
	 * 
	 * @param mac 播控设备MAC地址
	 * @param playCode 播控器编码
	 * @param logList 日志列表
	 * @return platform应答
	 * @throws WayzException 平台异常
	 *//*
	@ResponseBody
	@RequestMapping(value = "/reportDspPlayLogList", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse reportDspPlayLogList(@RequestParam(value = "mac", required = false) String mac,
			@RequestParam(value = "playCode", required = false) String playCode,
			@RequestJsonParam(value = "logList") List<DspLog> logList) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数: 播控设备MAC地址
		if ((mac == null || mac.isEmpty()) && (playCode == null || playCode.isEmpty())) {
			throw new WayzException("播控设备MAC或编码至少应填写一项");
		}
		// 验证参数: 日志列表
		ParameterValidator.notNull(logList, "日志列表");

		// 调用接口
		playProxy.reportDspPlayLogList(mac, playCode, logList);

		// 返回应答
		return $response;
	}

	*//**
	 * 上报消息推送令牌
	 * 
	 * @param mac 播控设备MAC地址
	 * @param pushToken 消息推送令牌
	 * @return platform应答
	 * @throws WayzException 平台异常
	 *//*
	@ResponseBody
	@RequestMapping(value = "/reportPushToken", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse reportPushToken(@RequestParam(value = "mac", required = false) String mac,
			@RequestParam(value = "pushToken", required = false) String pushToken) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数: 播控设备MAC地址
		ParameterValidator.notNull(mac, "播控设备MAC地址");

		// 调用接口
		playProxy.reportPushToken(mac, pushToken);

		// 返回应答
		return $response;
	}

	*//**
	 * 上报素材下载数量
	 * 
	 * @param playCode 播控编码
	 * @param planId 广告计划ID
	 * @param totalNum 素材总数量
	 * @param successNum 成功总数量
	 * @return
	 * @throws WayzException
	 *//*
	@ResponseBody
	@RequestMapping(value = "/reportMaterialStatus", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse reportMaterialStatus(@RequestParam(value = "playCode", required = false) String playCode,
			@RequestParam(value = "planId", required = false) Long planId,
			@RequestParam(value = "totalNum", required = false) Integer totalNum,
			@RequestParam(value = "successNum", required = false) Integer successNum) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();

		// 验证参数
		ParameterValidator.notNull(playCode, "截屏命令标识");
		ParameterValidator.notNull(planId, "广告计划标识");
		// 调用接口
		playProxy.reportMaterialStatus(playCode, planId, totalNum, successNum);
		// 返回应答
		return response;
	}

	*//**
	 * 上报截屏文件
	 * 
	 * @param mac 播控设备MAC地址
	 * @param cmdId 截屏命令标识
	 * @param imgFile 图片文件
	 * @return platform应答
	 * @throws WayzException 平台异常
	 *//*
	@ResponseBody
	@RequestMapping(value = "/reportSnapshot", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse reportSnapshot(@RequestParam(value = "mac", required = false) String mac,
			@RequestParam(value = "cmdId", required = false) String cmdId,
			@RequestParam(value = "imgFile", required = false) MultipartFile imgFile) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数: 播控设备MAC地址
		ParameterValidator.notNull(mac, "播控设备MAC地址");
		// 验证参数: 截屏命令标识
		ParameterValidator.notNull(cmdId, "截屏命令标识");
		// 验证参数: 图片文件
		ParameterValidator.notNull(imgFile, "图片文件");

		// 调用接口
		playProxy.reportSnapshot(mac, cmdId, imgFile);

		// 返回应答
		return $response;
	}

	*//**
	 * 上报播控本地IP
	 * 
	 * @param mac 播控mac地址
	 * @param ip ip地址
	 * @return platform应答
	 * @throws WayzException 平台异常
	 *//*
	@RequestMapping(value = "/reportIp", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse reportIp(@RequestParam(value = "mac", required = false) String mac,
			@RequestParam(value = "ip") String ip) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数:
		ParameterValidator.notNull(mac, "播控设备网卡MAC地址");
		// 验证参数:播控系统信息
		ParameterValidator.notNull(ip, "播控ip地址");

		// 调用接口
		playProxy.reportIp(mac, ip);

		// 返回应答
		return $response;
	}

	*//**
	 * 获取我的编码
	 * 
	 * @param macAddr 播控设备MAC地址
	 * @param androidId
	 * @param osVersion OS版本
	 * @param appVersion 本机播控系统软件版本
	 * @param communicationType 通信类型(1:WIFI; 2:GSM; 3:有线)
	 * @param isRoot 是否root
	 * @param manufacturer 制造商
	 * @param model 型号
	 * @param ts 系统时间戳(精确到秒)
	 * @return
	 * @return 播控编码
	 * @throws WayzException 平台异常
	 *//*
	@ResponseBody
	@RequestMapping(value = "/getMyCode", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse getMyCode(@RequestParam(value = "macAddr", required = false) String macAddr,
			@RequestParam(value = "androidId", required = false) String androidId,
			@RequestParam(value = "osVersion", required = false) String osVersion,
			@RequestParam(value = "appVersion", required = false) String appVersion,
			@RequestParam(value = "communicationType", required = false) Short communicationType,
			@RequestParam(value = "isRoot", required = false) Boolean isRoot,
			@RequestParam(value = "manufacturer", required = false) String manufacturer,
			@RequestParam(value = "model", required = false) String model,
			@RequestParam(value = "ts", required = false) Long ts, HttpServletRequest request) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数: 播控设备MAC地址
		// ParameterValidator.notNull(macAddr, "播控设备MAC地址");
		// // 验证参数: 截屏命令标识
		// ParameterValidator.notNull(androidId, "截屏命令标识");
		// // 验证参数: 图片文件
		// ParameterValidator.notNull(imgFile, "图片文件");

		// 调用接口
		$response.setData(playProxy.getMyCode(macAddr, androidId, osVersion, appVersion, communicationType, isRoot,
				manufacturer, model, ts, request));

		// 返回应答
		return $response;
	}*/

}