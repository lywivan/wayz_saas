<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
%>
<script src="${path}/website/assets/js/jquery.js"></script>
<script src="${path}/website/js/common/utils.js"></script>
<script type="text/javascript">
	$(function() {
		$('#img_404').click(function() {
			Utils.toRedirect('index');
		});
	});
</script>
<div>
	<br />
	<div align="center">
		<img id="img_404" alt="找不到页面啦！" src="${path}/website/image/404.png">
	</div>
</div>

