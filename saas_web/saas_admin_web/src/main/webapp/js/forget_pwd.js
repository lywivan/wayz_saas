/**
 * 准备函数
 */
$(document).ready(function() {
	
	// 添加点击事件
	$("#sendSMSVerifyCodeButton").bind("click", sendSMSVerifyCode);
	$("#confirmResetPwdButton").bind("click", resetPwd);
});

/**
 * 重置表单
 */
function reset() {
	document.getElementById('resetPwdForm').reset();
	$(window.parent.document).find('.layui-layer-iframe').remove();
}

/**
 * 获取验证码
 * 1注册用户;2找回密码;3绑定;4解绑
 */
function sendSMSVerifyCode() {
	// 获取参数
	var phone = $.trim($('#mPhone').val());
	if(Utils.isNotNull(phone)){
		if (phone.length != 11) {
			Utils.showDefTips('mPhone','手机号格式不正确，请检查!');
			return false;
		}
	}else{
		Utils.showDefTips('mPhone','手机号不能为空!');
		return false;
	}
	var data = {
		"phone" : phone,
		"authCodeType": 2
	}
	// 调用接口
	var response = callInterface("post", "general/sendSMSAuthCode", data , false);
	if (response.code == 0) {
		alert('验证码已发送,请查看!');
	} else {
		alert(response.message);
	}
}

/**
 * 重置密码
 */
function resetPwd() {
	// 获取参数
	var data = {};
	var phone = $.trim($('#mPhone').val());
	if(Utils.isNotNull(phone)){
		if (phone.length != 11) {
			Utils.showDefTips('mPhone','手机号格式不正确，请检查!');
			return false;
		}
	}else{
		Utils.showDefTips('mPhone','手机号不能为空!');
		return false;
	}
	var verifyCode = $.trim($('#mVerifycode').val());
	if (verifyCode == '') {
		Utils.showDefTips('mVerifycode','验证码不能为空!');
		return false;
	}
	var password = $.trim($('#newPassword').val());
	if (password == '') {
		Utils.showDefTips('newPassword','新密码不能为空!');
		return false;
	}
	var chkpwd = $.trim($('#confirmPassword').val());
	if (chkpwd == '') {
		Utils.showDefTips('confirmPassword','确认密码不能为空!');
		return false;
	}
	if (password != chkpwd) {
		Utils.showDefTips('confirmPassword','两次输入密码不一致!');
		return false;
	}
	data['phone'] = phone;
	data['verifyCode'] = verifyCode;
	data['newPassword'] = $.md5(password);

	// 调用接口
	var response = callInterface("post", "authority/resetPassword", data, false);
	if (response.code == 0) {
		alert('密码重置成功,请登录!');
		window.location.href = "login";
	} else {
		alert(response.message);
	}
}