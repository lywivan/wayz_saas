/**
 * 设备详情
 * 
 * @param id
 */
function toPositionView(id){
	// 显示对话框
	$("#positionViewDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});
	$("#vPositionId").val(id);
	
	// 调用接口
	var response = callInterface("post", "saas/media/getPositionDetail", {'id' : id }, false);
	if (response.code == 0) {
		var data = response.data;
		$("#vPName").text(data.name);
		$("#vPArea").text(Utils.convertNull(data.provinceName+data.cityName+data.districtName));
		$("#vPAddress").text(Utils.convertNull(data.address));
		$("#vPIsEnable").text(data.isEnable?"启用":"禁用");
		$("#vPIsBound").text(data.isBound?"已绑定":"未绑定");
		$("#vGroupName").text(Utils.convertNull(data.groupName));
		$("#vResolutionName").text(Utils.convertNull(data.resolutionName));
		$("#vPStartTime").text(Utils.convertNull(data.startTime));
		$("#vPCloseTime").text(Utils.convertNull(data.closeTime));
		$("#vPPlayCode").text(Utils.convertNull(data.playCode));
	} else {
		showErrorMsg(response.code, response.message);
	}
}