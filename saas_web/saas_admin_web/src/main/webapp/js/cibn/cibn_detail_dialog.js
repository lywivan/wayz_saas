/**
 * 素材详情
 */
function toCibnView(companyId){
	
	// 组装参数
	var data = {
		'companyId' : companyId	
	};
	
	// 显示对话框
	$("#cibnDetailDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});
	
	// 调用接口
	var response = callInterface("post", "saas/cibn/getCibnetail",
			data, false);
	if (response.code == 0) {
		var data = response.data;
		$("#vCompanyName").html(handleEmptyStr(data.companyName));
		$("#vIndustryName").html(data.industryName);
		$("#vIdentityBverse").html("<img src='"+data.identityBverse+"' style='width:171px;height:111px'/>");
		$("#vIdentityReverse").html("<img src='"+data.identityReverse+"' style='width:171px;height:111px'/>");
		$("#vBusinessLicense").html("<img src='"+data.businessLicense+"' style='width:171px;height:111px'/>");
		$("#vAuditStatus").html(getMatAuditStatusName(data.auditStatus));
		if(Utils.isNotNull(data.remark) && data.auditStatus == MaterialAuditStatus.NOPASS_AUDIT_V){
			$("#vRemark").html(data.remark);
			$("#vRemark").closest('tr').show();
		}else{
			$("#vRemark").closest('tr').hide();
		}
		$("#vCreatedTime").html(handleEmptyStr(data.createdTime));
		
	} else {
		showErrorMsg(response.code, response.message);
	}
}