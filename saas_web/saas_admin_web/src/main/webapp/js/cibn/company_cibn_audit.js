/**
 * 准备函数
 */
$(document).ready(function() {
	// 加载数据列表
	queryCibn();
    $("#auditRejectButton").bind("click" , cibnAuditReject);
	// 查询事件
	$("#queryButton").bind("click", function(){
		queryCibn(null, null);
	});
});

/**
 * 查询
 */
function queryCibn(pageIndex, pageSize) {
	// 组装参数
	var data = {};
	
	var companyName = $("#companyName").val();
	if (Utils.isNotNull(companyName)) {
		data['companyName'] = companyName;
	}
	
	var industryId = $("#industryId").val();
	if (Utils.isNotNull(industryId)) {
		data['industryId'] = industryId;
	}
	
	var auditStatus = $("#auditStatus").val();
	if (Utils.isNotNull(auditStatus)) {
		data['auditStatus'] = auditStatus;
	}
	
	//设置分页参数
	setPageIndex(data,pageIndex,pageSize);
	
	// 调用接口
	var response = callInterface("post", "saas/cibn/queryCibnList",data, false);
	if (response.code == 0) {
		// 清空表格及分页
		$("#dataTable tr:not(:first)").remove();
		$("#pageDiv").html("");
		
		var count = response.data.totalCount;
		if (count == null || count == 0) {
			var cols = $("#dataTable").find("tr:first th");
			$('#dataTable').append("<tr><td colspan='" + cols.length + "'>无数据!</td></tr>");
			return false;
		}
		
		var list = response.data.list;
		if (list != null && list.length > 0) {
			for ( var i in list) {
				var trHtml = "<tr>";
				trHtml += "<td align='center'><a class='edit' href='javascript:toCibnView(" + list[i].companyId + ");'>" + list[i].companyName + "</a></td>";
				trHtml += "<td align='center'>" + list[i].industryName + "</td>";
				trHtml += "<td align='center'>" + getMatAuditStatusName(list[i].auditStatus) + "</td>";
				trHtml += "<td align='center'>" + Utils.convertNull(list[i].createdTime) + "</td>";
				
				// 操作
				trHtml += "<td align='center' class='operationColumn'>";
				if(list[i].auditStatus == MaterialAuditStatus.PENDING_AUDIT_V){
					trHtml += '<a href="javascript:void(0);"class="zengzhi edit" onclick="cibnAuditPass(\''
						+ list[i].companyId + '\');">通过</a>&nbsp;&nbsp;';
					trHtml += '<a href="javascript:void(0);" class="zengzhi copy" onclick="showCibnAuditRejectDialog(\''
							+ list[i].companyId + '\');">驳回</a>';
				}
				trHtml += "</td>";
				trHtml += "</tr>";
				
				// 动态添加一行
				$('#dataTable').append(trHtml);
			}
		}
			
		// 调用分页
		layui.use('laypage', function() {
			layui.laypage.render({
				elem : 'pageDiv',
				// pages : pages,
				curr : pageIndex || 1, // 当前页
				count : count,
				limit : pageSize,
				skip : true, // 是否开启跳页
				layout : ['count', 'prev', 'page', 'next', 'limit', 'skip'],
				groups : 5, // 连续显示分页数
				jump : function(obj, first) { // 触发分页后的回调
					if (!first) {
						queryCibn(obj.curr,obj.limit);
					}
				}
			});
		});
	}
}
/**
 * 审核通过
 */
function cibnAuditPass(id) {
	// 设置参数
	var data = {};
	data['companyId'] = id;
	data['auditStatus'] = MaterialAuditStatus.PASS_AUDIT_V;
	
	// 调用接口
	var response = callInterface("post", "saas/cibn/auditCibn", data, false);
	if (response.code == 0) {
		showMsg('操作成功!');
		
		// 刷新
		queryCibn(currPage);
	} else {
		showErrorMsg(response.code, response.message);
	}
	return false;
}

/**
 * 显示审核驳回对话框
 */
function showCibnAuditRejectDialog(id) {
	// 显示对话框
	$("#cibnAuditRejectDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});
	$('#cibnAuditRejectForm')[0].reset();
	$("#mId").val(id);
}

/**
 * 审核驳回
 */
function cibnAuditReject() {
	// 设置参数
	var data = {};
	var remark = $.trim($('#remark').val());
	if (Utils.isNull(remark)) {
		showAlert('驳回原因不能为空!');
		return false;
	}
	data['companyId'] = $.trim($("#mId").val());
	data['auditStatus'] = MaterialAuditStatus.NOPASS_AUDIT_V;
	data['remark'] = remark;

	// 询问框
	layer.confirm('确定驳回审核吗？', {
	  offset :'20px',btn: ['确定','取消'] //按钮
	}, function(){
		// 调用接口
		var response = callInterface("post", "saas/cibn/auditCibn", data, false);
		if (response.code == 0) {
			showMsg('操作成功!');
			
			// 关闭对话框
			$('#cibnAuditRejectDialog').modal("hide");

			// 刷新
			queryCibn(currPage);
		} else {
			showErrorMsg(response.code, response.message);
		}
	}, function(){
		return;
	});
	return false;
}