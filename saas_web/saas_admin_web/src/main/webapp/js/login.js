$(document).ready(function(){
	$(".nav li").hover(function(){
		$(this).find(".nav-main").stop(true).fadeIn(100);
	},function(){
		$(this).find(".nav-main").stop(true).fadeOut(100);
	});
	$(".right-xf>span").hover(function(){
		$(this).find(".xf-main").stop(true).animate({left:'-168px'},800);
		$(this).find("a").css("background","#0062c8");
	},function(){
		$(this).find(".xf-main").stop(true).animate({left:'72px'},300);
		$(this).find("a").css("background","#0d1f41");
	});
	var explorer =navigator.userAgent ;
	if (explorer.indexOf("MSIE") >= 0) {
		$("#canvas").remove();
	}
	// 从cookie读取
	var username = $.cookie('username');
	var password = $.cookie('password');
	var remember = $.cookie('remember');
	if (typeof (username) != "undefined") {
		$("#username").val(username);
		if (password == null || password == "") {
			$("#password").val("");
		}else{
			$("#password").val(base64decode(password));
		}
		if (remember) {
			$(".rember-on").show();
		}
		else {
			$(".rember-on").hide();
		}
		$("#authcode").focus();
	}
	
	// 获取验证码
	getAuthcode();
	
	// 添加点击事件
	$("#authimage").bind("click", getAuthcode);
	$("#loginButton").bind("click", tologin);
	
	$("#authcode").keydown(function(event){
	    event=document.all?window.event:event;
	    if((event.keyCode || event.which)==13){
	    	tologin();
	    }
	}); 
	/* 记住密码 */
	$(".remember-click").click(function() {
		if ($(".rember-on").is(":hidden")) {
			$(".rember-on").show();
		}
		else {
			$(".rember-on").hide();
		}
	});
});
/**
 * 调整窗口大小
 */
//window.onresize = function() {
//	$(".login").css("height", $(window).height());
//};

/**
 * 获取验证码
 */
function getAuthcode() {
	// 清空验证码
	$("#authcode").val("");
	
	// 获取验证码 --
	$("#authimage").attr("src","authority/getAuthcode?t=" + new Date().getTime());
}

/**
 * 用户登录
 */
function tologin() {
	// 获取参数
	var username = $.trim($("#username").val());
	var password = $.trim($("#password").val());
	var authcode = $("#authcode").val();

	// 检查参数
	// 用户名称
	if (typeof (username) == "undefined" || username == "") {
		$("#username").tips({
			side : 2, //side 提示窗显示位置  1，2，3，4 分别代表 上右下左 默认为1（上） 可选
			color :'#000',//提示文字色 默认为白色 可选
			msg : '用户名不能为空',//提示消息  必填
			bg : '#ffe74c',//提示窗背景色 默认为红色 可选
			time : 1//自动关闭时间 默认2秒 设置0则不自动关闭 可选
			//x:0,//横向偏移  正数向右偏移 负数向左偏移 默认为0 可选
			//y:0,//纵向偏移  正数向下偏移 负数向上偏移 默认为0 可选
		});
		$("#username").focus();
		return;
	}
	// 检查参数: 用户密码
	if (typeof (password) == "undefined" || password == "") {
		$("#password").tips({
			side : 2,
			msg : '密码不能为空',
			color :'#000',
			bg : '#ffe74c',
			time : 1
		});
		$("#password").focus();
		return;
	}

	// 检查参数: 验证编码
	if (typeof (authcode) == "undefined" || authcode == "") {
		$("#authcode").tips({
			side : 2,
			msg : '验证码不能为空',
			color :'#000',
			bg : '#ffe74c',
			time : 1
		});
		$("#authcode").focus();
		return;
	}

	// 调用接口
	var $data = {
		"authCode" : authcode,
		"username" : username,
		"password" : $.md5(password)
	};
	var response = callInterface("post", "authority/login", $data, false);
	if (response.code == 0) {
		// 保存
		saveToken(response.data);
		
		// 跳转
		window.location.href = "index";
	} else {
		alert(response.message);
	}
}

/**
 * 保存
 */
function saveToken(token) {
	$.cookie('token', token, {
		expires : 7
	})
	if ($(".rember-on").is(":hidden")) {
		$.cookie("username", "", {
			expires : -1
		});
		$.cookie("password", "", {
			expires : -1
		});
		$.cookie("remember", false, {
			expires : -1
		});
	}
	else {
		$.cookie('username', $.trim($("#username").val()), {
			expires : 7
		});
		$.cookie('password', base64encode($.trim($("#password").val())), {
			expires : 7
		});
		$.cookie('remember', true, {
			expires : 7
		});
	}
}

/**
 * 整数类型的改变事件
 * @param t
 */
function integerChange(t){
	var $this = $(t);
	$this.val( $this.val().replace(/[^\d]/g,'') );
}