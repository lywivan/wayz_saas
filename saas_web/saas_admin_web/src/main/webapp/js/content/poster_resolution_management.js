
/**
 * 准备函数
 */
var operateRight = new Object();
$(document).ready(function() {
	// 加载权限
	loadRight();
	
	// 加载表格
	queryDataList();
	
	// 绑定函数
	$("#queryButton").bind("click", confirmQuery);
	$("#createButton").bind("click", showCreateDialog);
	$("#confirmCreateButton").bind("click", confirmCreate);
});

/**
 * 加载权限
 */
function loadRight() {
	operateRight.deleteRight=$("#delete").val();
}

/**
 * 查询
 */
function queryDataList(pageIndex, pageSize) {
	// 组装参数
	var data = {};
	var width = $.trim($("#width").val());
	if (width != "") {
		data['width'] = width;
	}
	var height = $.trim($("#height").val());
	if (height != "") {
		data['height'] = height;
	}

	// 设置分页参数
	setPageIndex(data, pageIndex, pageSize);
	
	// 调用接口 
	var response = callInterface("post", "saas/poster/queryPosterResolution", data, false);
	if (response.code == 0) {
		var count = response.data.totalCount;
		if (count == null) {
			count = 0;
		}
		// 清空表格及分页
		$("#dataTable tr:not(:first)").remove();
		$("#pageDiv").html("");

		// 动态添加一行
		if (count == 0) {
			var cols = $("#dataTable").find("tr:first th");
			$('#dataTable').append(
					"<tr><td colspan='" + cols.length + "'>无数据!</td></tr>");
			return false;
		}

		var list = response.data.list;
		if (list != null && list.length > 0) {
			for ( var i in list) {
				var trHtml = "<tr>";
				trHtml += "<td align='center'>" + list[i].name + "</td>";
				trHtml += "<td align='center'>" + list[i].width + "</td>";
				trHtml += "<td align='center'>" + list[i].height + "</td>";
				trHtml += "<td align='center'>" + list[i].createdTime + "</td>";
				trHtml += "<td align='center' class='operationColumn'>";
				if(operateRight.deleteRight == 1){
					trHtml += '<a href="javascript:void(0);" class="delete" onclick="deleteResolution('
						+ list[i].id + ');">删除</a>';
				}
				trHtml += "</td>";
				trHtml += "</tr>";

				// 动态添加一行
				$('#dataTable').append(trHtml);
			}
		}
		
		// 调用分页
		layui.use(['laypage'], function() {
			var laypage = layui.laypage;
			laypage.render({
				elem : 'pageDiv',
				// pages : pages,
				curr : pageIndex || 1, // 当前页
				count : count,
				limit : pageSize,
				skip : true, // 是否开启跳页
				layout : ['count', 'prev', 'page', 'next', 'limit', 'skip'],
				groups : 5, // 连续显示分页数
				jump : function(obj, first) { // 触发分页后的回调
					// 一定要加此判断，否则初始时会无限刷新
					if (!first) {
						// 点击跳页触发函数自身，并传递当前页：obj.curr
						queryDataList(obj.curr, obj.limit);
					}
				}
			});
		});
	}
}

/**
 * 确认查询
 */
function confirmQuery() {
	queryDataList();
}

/**
 * 显示创建对话框
 */
function showCreateDialog() {
	$("#createDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});
	$('#addForm')[0].reset();
}

/**
 * 确认创建
 */
function confirmCreate() {
	// 获取参数
	var data = {};
	var width = $.trim($('#cWidth').val());
	if (width == '') {
		showAlert('宽不能为空!');
		return false;
	}else{
		if( isNaN(width) ){
			showAlert('宽只能是数字!');
			return false;
		}
	}
	data['width'] = width;
	
	var height = $.trim($('#cHeight').val());
	if (height == '') {
		showAlert('高不能为空!');
		return false;
	}else{
		if( isNaN(height) ){
			showAlert('高只能是数字!');
			return false;
		}
	}
	data['height'] = height;

	// 调用接口
	var response = callInterface("post", "saas/poster/createPosterResolution", data, false);
	if (response.code == 0) {
		showMsg('操作成功!');

		// 关闭对话框
		$('#createDialog').modal("hide");

		// 刷新
		queryDataList();
		
	} else {
		showErrorMsg(response.code, response.message);
	}
}

/**
 * 删除分辨率
 * 
 * @param liveId
 */
function deleteResolution(id){
	// 设置参数
	var data = {
		"id" : id
	};
	
	// 询问框
	var index = layer.confirm('确定删除该分辨率吗？', {
	  offset :'20px',btn: ['确定','取消'] //按钮
	}, function(index){
		// 调用接口
		var response = callInterface("post", "saas/poster/deletePosterResolution",
				data, false);
		if (response.code == 0) {
			showMsg('操作成功!');
			layer.close(index);
			// 刷新
			queryDataList();
		} else {
			showErrorMsg(response.code, response.message);
		}
	}, function(){
		return;
	});
}