/**
 * 准备函数
 */
$(document).ready(function() {
	$('#materialDetailDialog').on('hide.bs.modal', function () {
		  $("#materialDetailCanvasZone video").attr("src","");
	});
});

/**
 * 素材详情
 */
function showMaterialDetailDialog(materialId,materialName){
	
	// 组装参数
	var data = {
		'id' : materialId	
	};
	
	$("#materialName").text(materialName);
	
	// 显示对话框
	$("#materialDetailDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});
	
	// 调用接口
	var response = callInterface("post", "saas/shop/getContent",
			data, false);
	if (response.code == 0) {
		
		var data = response.data;
		
		if(data.type == FileType.IMAGE_V){
			$("#vDuration").parent().parent().hide();
		}else{
			$("#vDuration").parent().parent().show();
		}
		
		$("#vChannelName").text(handleEmptyStr(data.channelName));
		$("#vTypeName").text(handleEmptyStr(getFileTypeName(data.type)));
		$("#vFileSize").text(handleEmptyStr(getFileSizeAuto(data.fileSize)));
		$("#vResolutionName").text(data.width+"*"+data.height);
		$("#vDuration").text(handleEmptyStr(data.duration));
		
		$("#vTitle").text(handleEmptyStr(data.title));
		$("#vPrice").text(data.price == 0 ? "免费" : "￥"+data.price);
		$("#vCategoryName").text(handleEmptyStr(data.categoryName));
		$("#vDescription").text(handleEmptyStr(data.description));
		$("#vAuditStatus").text(getMatAuditStatusName(data.auditStatus));
		if(data.auditStatus == 2){
			$("#vRemark").text(handleEmptyStr(data.remark));
			$(".vRemark").show();
		}else{
			$(".vRemark").hide();
		}
		$("#vCreateTime").text(handleEmptyStr(data.createdTime));
		
		// 设置预览素材信息
		setFilePreviewHtml(data.type, data.name, data.url);
	} else {
		showErrorMsg(response.code, response.message);
	}
}