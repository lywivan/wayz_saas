/**
 * 准备函数
 */
$(document).ready(function() {
	// 加载表格
	queryMaterial();
	
	$("#auditRejectButton").bind("click" , contentAuditReject);
	
	$(".search-main-top a").click(function(){
		var $this = $(this);
		if($this.hasClass("on")){
			$this.removeClass("on");
		}else{
			$this.siblings().removeClass("on");
			$this.addClass("on");
		}
		queryMaterial();
	});
});

/**
 * 查询
 */
function queryMaterial(pageIndex, pageSize) {
	// 组装参数
	var data = {};
	$(".channelId").each(function(){
		var channelId = $(this).attr('data-v');
		if($(this).hasClass("on") && Utils.isNotNull(channelId)){
			data.channelId = channelId;
		}
	});
	
	$(".type").each(function(){
		var type = $(this).attr('data-v');
		if($(this).hasClass("on") && Utils.isNotNull(type)){
			data.type = type;
		}
	});
	data.status = 0;
	
	// 设置分页参数
	setPageIndex(data, pageIndex, pageSize);

	// 调用接口
	var response = callInterface("post", "saas/shop/queryContent", data, false);
	if (response.code == 0) {
		// 清空分页
		$("#pageDiv").html("");

		var materialSpan = "";
		var count = response.data.totalCount;
		var list = response.data.list;
		if (list != null && list.length > 0) {
			materialSpan += '<section>';
			materialSpan += '<div class="wf-container">';
			//循环item
			for ( var i in list) {
				if(Utils.isNull(list[i].previewUrl)){
					continue;
				}
				materialSpan += '<div class="wf-box">';
				
				materialSpan += "<img style='width:200px;height:200px' src=\"" + list[i].previewUrl + Global.IMG_SiZE_TWO_H + "\"></a>";
				materialSpan += '<div class="content"><p>';
				materialSpan += '<font style="font-weight:bold">';
				materialSpan += '<font style="font-weight:bold" title="'+list[i].title+'">' + cutString(list[i].title,16) + '</font><br>';
				materialSpan += '<font style="font-weight:bold"> 渠道：</font>'+list[i].channelName+'<br>';
				materialSpan += '<font style="font-weight:bold"> 更新时间：</font>' + list[i].createdTime + '<br>';
				materialSpan += '<div class="clear">';
				var price = Utils.toFormatDecimal(list[i].price,2);
				if(price == 0 || price == 0.00){
					price = "免费";
				}else{
					price = "￥" + price;
				}
				materialSpan += '<div><font style="display:inline-block;width:50%;text-align:left;color:red;">' + price + '</font>';
				// 通过
				materialSpan += '<span style="display:inline-block;width:50%;text-align:right;"><a href="javascript:void(0);"class="zengzhi delete" onclick="contentAuditPass(\''
					+ list[i].id + '\');">通过</a>';
				// 驳回
				materialSpan += '&nbsp;&nbsp;<a href="javascript:void(0);" class="zengzhi delete" onclick="showContentAuditRejectDialog(\''
					+ list[i].id + '\');">驳回</a></span></div>';
				materialSpan += '</div>';
				materialSpan += '</p></div></div>';
			}
			materialSpan += '</div></section>';
			$('#materialDiv').html(materialSpan);
			var waterfall = new Waterfall({ minBoxWidth: 200 });
			
			// 调用分页
			layui.use(['laypage'], function() {
				var laypage = layui.laypage;
				laypage.render({
					elem : 'pageDiv',
					// pages : pages,
					curr : pageIndex || 1, // 当前页
					count : count,
					limit : pageSize,
					skip : true, // 是否开启跳页
					layout : ['count', 'prev', 'page', 'next', 'limit', 'skip'],
					groups : 5, // 连续显示分页数
					jump : function(obj, first) { // 触发分页后的回调
						if (!first) {
							queryMaterial(obj.curr, obj.limit);
						}
					}
				});
			});
		}else{
			$('#materialDiv').html("无素材!");
		}
	}
}
/**
 * 审核通过
 */
function contentAuditPass(id) {
	// 设置参数
	var data = {};
	data['id'] = id;
	data['status'] = 1;
	
	// 调用接口
	var response = callInterface("post", "saas/shop/auditorContent", data, false);
	if (response.code == 0) {
		showMsg('操作成功!');
		
		// 刷新
		queryMaterial(currPage);
	} else {
		showErrorMsg(response.code, response.message);
	}
	return false;
}

/**
 * 显示审核驳回对话框
 */
function showContentAuditRejectDialog(id) {
	// 显示对话框
	$("#contentAuditRejectDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});
	$('#contentAuditRejectForm')[0].reset();
	$("#mId").val(id);
}

/**
 * 审核驳回
 */
function contentAuditReject() {
	// 设置参数
	var data = {};
	var remark = $.trim($('#remark').val());
	if (Utils.isNull(remark)) {
		showAlert('驳回原因不能为空!');
		return false;
	}
	data['id'] = $.trim($("#mId").val());
	data['status'] = 2;
	data['remark'] = remark;

	// 询问框
	layer.confirm('确定驳回审核吗？', {
	  offset :'20px',btn: ['确定','取消'] //按钮
	}, function(){
		// 调用接口
		var response = callInterface("post", "saas/shop/auditorContent", data, false);
		if (response.code == 0) {
			showMsg('操作成功!');
			
			// 关闭对话框
			$('#contentAuditRejectDialog').modal("hide");

			// 刷新
			queryMaterial(currPage);
		} else {
			showErrorMsg(response.code, response.message);
		}
	}, function(){
		return;
	});
	return false;
}