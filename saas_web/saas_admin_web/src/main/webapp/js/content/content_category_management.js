
/**
 * 准备函数
 */
var operateRight = new Object();
$(document).ready(function() {
	// 加载权限
	loadRight();
	
	// 加载表格
	queryCategory();

	// 绑定函数
	$("#queryButton").bind("click", confirmQueryCategory);
	$("#createButton").bind("click", showCreateCategoryDialog);
	$("#confirmCreateButton").bind("click", confirmCreateCategory);
	$("#confirmModifyButton").bind("click", confirmModifyCategory);
});

/**
 * 加载权限
 */
function loadRight() {
	operateRight.modifyRight=$("#modify").val();
	operateRight.deleteRight=$("#delete").val();
}

/**
 * 查询
 */
function queryCategory(pageIndex, pageSize) {
	// 组装参数
	var data = {};
	var name = $.trim($("#name").val());
	if (name != "") {
		data['name'] = name;
	}
	

	// 设置分页参数
	setPageIndex(data, pageIndex, pageSize);
	
	// 调用接口
	var response = callInterface("post", "saas/category/queryCategory", data, false);
	if (response.code == 0) {
		// 清空表格及分页
		$("#dataTable tr:not(:first)").remove();
		$("#pageDiv").html("");

		var count = response.data.totalCount;
		if (count == null || count == 0) {
			var cols = $("#dataTable").find("tr:first th");
			$('#dataTable').append("<tr><td colspan='" + cols.length + "'>无数据!</td></tr>");
			return false;
		}
		var list = response.data.list;
		if (list != null && list.length > 0) {
			for ( var i in list) {
				var trHtml = "<tr>";
				trHtml += "<td align='center'>" + list[i].name + "</td>";
				trHtml += "<td>" + handleEmptyStr(list[i].description) + "</td>";
				trHtml += "<td align='center'>";
				if(operateRight.modifyRight == 1){
					trHtml +="<a href='javascript:void(0);' class='edit' onclick='showModifyCategoryDialog("
						+ JSON.stringify(list[i])
						+ ");'>编辑</a>&nbsp;&nbsp;";
				}
				/*if(operateRight.deleteRight == 1){
					trHtml += '<a href="javascript:void(0);" class="delete" onclick="deleteCategory('
						+ list[i].id + ');">删除</a>';
				}*/
				trHtml += "</td>";
				trHtml += "</tr>";
	
				// 动态添加一行
				$('#dataTable').append(trHtml);
			}
		}
		
		// 调用分页
		layui.use(['laypage'], function() {
			var laypage = layui.laypage;
			laypage.render({
				elem : 'pageDiv',
				// pages : pages,
				curr : pageIndex || 1, // 当前页
				count : count,
				limit : pageSize,
				skip : true, // 是否开启跳页
				layout : ['count', 'prev', 'page', 'next', 'limit', 'skip'],
				groups : 5, // 连续显示分页数
				jump : function(obj, first) {
					if (!first) {
						queryCategory(obj.curr, obj.limit);
					}
				}
			});
		});
	}
}

/**
 * 确认查询
 */
function confirmQueryCategory() {
	queryCategory();
}

/**
 * 显示创建对话框
 */
function showCreateCategoryDialog() {
	$("#createCategoryDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});
	$('#addCategoryForm')[0].reset();
}

/**
 * 确认创建
 */
function confirmCreateCategory() {
	// 获取参数
	var data = {};
	var name = $.trim($('#cName').val());
	if (name == '') {
		showAlert('内容分类不能为空!');
		return false;
	}
	data['name'] = name;
	
	var description = $.trim($('#cDescription').val());
	if (description != "") {
		data['description'] = description;
	}

	// 调用接口
	var response = callInterface("post", "saas/category/createCategory", data, false);
	if (response.code == 0) {
		showMsg('操作成功!');

		// 关闭对话框
		$('#createCategoryDialog').modal("hide");

		// 刷新
		queryCategory();
	} else {
		showErrorMsg(response.code, response.message);
	}
}

/**
 * 显示修改对话框
 */
function showModifyCategoryDialog(data) {
	// 显示对话框
	$("#modifyCategoryDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});
	$('#modifyCategoryForm')[0].reset();

	$("#mId").val(data.id);
	$("#mName").val(data.name);
	$("#mDescription").val(data.description);
}

/**
 * 确认修改
 */
function confirmModifyCategory() {
	// 获取参数
	var data = {};
	var id = $.trim($("#mId").val());
	data['id'] = id;
	var name = $.trim($('#mName').val());
	if (name == '') {
		showAlert('内容分类不能为空!');
		return false;
	}
	data['name'] = name;
	
	var description = $.trim($('#mDescription').val());
	data['description'] = description;
	
	// 调用接口
	var response = callInterface("post", "saas/category/modifyCategory", data,
			false);
	if (response.code == 0) {
		showMsg('操作成功!');

		// 关闭对话框
		$('#modifyCategoryDialog').modal("hide");

		// 刷新
		queryCategory(currPage);
	} else {
		showErrorMsg(response.code, response.message);
	}
}

/**
 * 显示删除对话框
 */
function deleteCategory($id) {
	// 设置参数
	var $data = {
		"id" : $id
	};
	
	// 询问框
	layer.confirm('确定删除该内容分类吗？', {
	  offset :'20px',btn: ['确定','取消'] //按钮
	}, function(){
		// 调用接口
		var response = callInterface("post", "saas/category/deleteCategory", $data, false);
		if (response.code == 0) {
			showMsg('操作成功!');

			// 刷新
			queryCategory();
		} else {
			showErrorMsg(response.code, response.message);
		}
	}, function(){
		return;
	});
}