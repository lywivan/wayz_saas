/**
 * 准备函数
 */
var operateRight = new Object();
$(document).ready(function() {
	
	// 加载权限
	loadRole();

	// 加载表格
	queryDevicePrice();

	// 绑定函数
	$("#queryButton").bind("click", confirmQueryDevicePrice);
	$("#createButton").bind("click", showCreateDevicePriceDialog);
	$("#confirmCreateButton").bind("click", confirmCreateDevicePrice);
	$("#confirmModifyButton").bind("click", confirmModifyDevicePrice);
});

/**
 * 加载权限
 */
function loadRole() {
	operateRight.modifyRight = $("#modify").val();
	operateRight.deleteRight = $("#delete").val();
}

/**
 * 查询用户
 */
function queryDevicePrice(pageIndex, pageSize) {
	// 组装参数
	var data = {};

	// 设置分页参数
	setPageIndex(data, pageIndex, pageSize);
	
	// 调用接口
	var response = callInterface("post", "admin/queryDevicePrice", data, false);
	if (response.code == 0) {
		// 清空表格及分页
		$("#DevicePriceTable tr:not(:first)").remove();
		var list = response.data;
		if (list != null && list.length > 0) {
			for ( var i in list) {
				var trHtml = "<tr>";
				trHtml += "<td align='center'>" + list[i].name + "</td>";
				trHtml += "<td align='center'>" + list[i].days + "</td>";
				trHtml += "<td align='center'>" + list[i].salePrice + "</td>";
				trHtml += "<td align='center'>" + list[i].discountPrice + "</td>";
				// 操作
				trHtml += "<td align='center' class='operationColumn'>";
				if (operateRight.modifyRight == 1) {
					trHtml += "<a href='javascript:void(0);' class='edit' onclick='showModifyDevicePriceDialog(" + JSON.stringify(list[i]) + ");'>编辑</a>&nbsp;&nbsp;";
				}
				if (operateRight.deleteRight == 1) {
					trHtml += "<a href='javascript:void(0);' class='delete' onclick='deleteDevicePrice(" + list[i].id
							+ ",\"" + list[i].name + "\");'>删除</a>";
				}
				trHtml += "</td>";
				trHtml += "</tr>";

				// 动态添加一行
				$('#DevicePriceTable').append(trHtml);
			}
		}else{
			// 获取表格行数、列数
			var cols = $("#DevicePriceTable").find("tr:first th");
			$('#DevicePriceTable').append("<tr><td colspan='" + cols.length + "'>无数据!</td></tr>");
		}
	}else {
		showErrorMsg(response.code, response.message);
	}
}

/**
 * 确认查询
 */
function confirmQueryDevicePrice() {
	queryDevicePrice();
}


/**
 * 显示创建对话框
 */
function showCreateDevicePriceDialog() {
	// 弹出框
	$("#createDevicePriceDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});
	$('#addDevicePriceForm')[0].reset();
}

/**
 * 确认创建
 */
function confirmCreateDevicePrice() {
	// 获取参数
	var data = {};
	var name = $.trim($('#cName').val());
	if (Utils.isNull(name)) {
		showAlert('名称不能为空!');
		return;
	}
	data['name'] = name;
	var days = $("#cDays").val();
	if (Utils.isNull(days)) {
		showAlert("天数不能为空!");
		return;
	}
	data['days'] = days;
	var salePrice = $.trim($('#cSalePrice').val());
	if (Utils.isNull(salePrice)) {
		showAlert("原价不能为空!");
		return false;
	}
	data['salePrice'] = salePrice;
	var discountPrice = $.trim($('#cDiscountPrice').val());
	if (Utils.isNull(discountPrice)) {
		showAlert("优惠价不能为空!");
		return false;
	}
	data['discountPrice'] = discountPrice;

	// 调用接口
	var response = callInterface("post", "admin/createDevicePrice", data, false);
	if (response.code == 0) {
		showMsg('操作成功!');
		$('#createDevicePriceDialog').modal("hide");

		// 刷新
		queryDevicePrice();
	}
	else {
		showErrorMsg(response.code, response.message);
	}
}

/**
 * 显示修改用户对话框
 */
function showModifyDevicePriceDialog(data) {
	// 显示对话框
	$("#modifyDevicePriceDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});
	$('#modifyDevicePriceForm')[0].reset();
	
	// 设置参数
	$("#mId").val(data.id);
	$("#mName").val(data.name);
	$("#mDays").val(data.days);
	$("#mSalePrice").val(data.salePrice);
	$("#mDiscountPrice").val(data.discountPrice);

}

/**
 * 确认修改用户
 */
function confirmModifyDevicePrice() {
	
	// 获取参数
	var data = {};
	data['id'] = $.trim($("#mId").val());
	var name = $.trim($('#mName').val());
	if (Utils.isNull(name)) {
		showAlert('名称不能为空!');
		return;
	}
	data['name'] = name;
	var days = $("#mDays").val();
	if (Utils.isNull(days)) {
		showAlert("天数不能为空!");
		return;
	}
	data['days'] = days;
	var salePrice = $.trim($('#mSalePrice').val());
	if (Utils.isNull(salePrice)) {
		showAlert("原价不能为空!");
		return false;
	}
	data['salePrice'] = salePrice;
	var discountPrice = $.trim($('#mDiscountPrice').val());
	if (Utils.isNull(discountPrice)) {
		showAlert("优惠价不能为空!");
		return false;
	}
	data['discountPrice'] = discountPrice;
	
	// 调用接口
	var response = callInterface("post", "admin/modifyDevicePrice", data, false);
	if (response.code == 0) {
		showMsg('操作成功!');
		$('#modifyDevicePriceDialog').modal("hide");

		// 刷新
		queryDevicePrice(currPage);
	}
	else {
		showErrorMsg(response.code, response.message);
	}
}

/**
 * 删除
 */
function deleteDevicePrice($id, $name) {
	// 设置参数
	var $data = {
		"id" : $id
	};

	// 询问框
	layer.confirm('确定删除[' + $name + ']吗？', {
		offset :'20px',btn: ['确定','取消']
	// 按钮
	}, function() {
		// 调用接口
		var response = callInterface("post", "admin/deleteDevicePrice", $data, false);
		if (response.code == 0) {
			showMsg('操作成功!');

			// 刷新
			queryDevicePrice();
		}
		else {
			showErrorMsg(response.code, response.message);
		}
	}, function() {
		return;
	});
}