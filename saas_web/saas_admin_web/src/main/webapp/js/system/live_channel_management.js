/**
 * 准备函数
 */
var operateRight = new Object();
$(document).ready(function() {
	
	// 加载权限
	loadRole();

	// 加载表格
	queryLiveChannel();

	// 绑定函数
	$("#queryButton").bind("click", confirmQueryLiveChannel);
	$("#createButton").bind("click", showCreateLiveChannelDialog);
	$("#confirmCreateButton").bind("click", confirmCreateLiveChannel);
	$("#confirmModifyButton").bind("click", confirmModifyLiveChannel);
});

/**
 * 加载权限
 */
function loadRole() {
	operateRight.modifyRight = $("#modify").val();
	operateRight.deleteRight = $("#delete").val();
}

/**
 * 查询
 */
function queryLiveChannel(pageIndex, pageSize) {
	// 组装参数
	var data = {};

	// 设置分页参数
	setPageIndex(data, pageIndex, pageSize);
	
	// 调用接口
	var response = callInterface("post", "admin/queryLiveChannel", data, false);
	if (response.code == 0) {
		// 清空表格及分页
		$("#LiveChannelTable tr:not(:first)").remove();
		var list = response.data;
		if (list != null && list.length > 0) {
			var num = 1;
			for ( var i in list) {
				var trHtml = "<tr>";
				trHtml += "<td align='center'>" + (num++) + "</td>";
				trHtml += "<td align='center'>" + list[i].name + "</td>";
				// 操作
				trHtml += "<td align='center' class='operationColumn'>";
				if (operateRight.modifyRight == 1) {
					trHtml += "<a href='javascript:void(0);' class='edit' onclick='showModifyLiveChannelDialog(" + JSON.stringify(list[i]) + ");'>编辑</a>&nbsp;&nbsp;";
				}
				if (operateRight.deleteRight == 1) {
					trHtml += "<a href='javascript:void(0);' class='delete' onclick='deleteLiveChannel(" + list[i].id
							+ ",\"" + list[i].name + "\");'>删除</a>";
				}
				trHtml += "</td>";
				trHtml += "</tr>";

				// 动态添加一行
				$('#LiveChannelTable').append(trHtml);
			}
		}else{
			// 获取表格行数、列数
			var cols = $("#LiveChannelTable").find("tr:first th");
			$('#LiveChannelTable').append("<tr><td colspan='" + cols.length + "'>无数据!</td></tr>");
		}
	}else {
		showErrorMsg(response.code, response.message);
	}
}

/**
 * 确认查询
 */
function confirmQueryLiveChannel() {
	queryLiveChannel();
}

/**
 * 显示创建对话框
 */
function showCreateLiveChannelDialog() {
	// 弹出框
	$("#createLiveChannelDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});
	$('#addLiveChannelForm')[0].reset();
}

/**
 * 确认创建
 */
function confirmCreateLiveChannel() {
	// 获取参数
	var data = {};
	var name = $.trim($('#cName').val());
	if (Utils.isNull(name)) {
		showAlert('名称不能为空!');
		return;
	}
	data['name'] = name;

	// 调用接口
	var response = callInterface("post", "admin/createLiveChannel", data, false);
	if (response.code == 0) {
		showMsg('操作成功!');
		$('#createLiveChannelDialog').modal("hide");

		// 刷新
		queryLiveChannel();
	}
	else {
		showErrorMsg(response.code, response.message);
	}
}

/**
 * 显示修改对话框
 */
function showModifyLiveChannelDialog(data) {
	// 显示对话框
	$("#modifyLiveChannelDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});
	$('#modifyLiveChannelForm')[0].reset();
	
	// 设置参数
	$("#mId").val(data.id);
	$("#mName").val(data.name);

}

/**
 * 确认修改
 */
function confirmModifyLiveChannel() {
	// 获取参数
	var data = {};
	data['id'] = $.trim($("#mId").val());
	var name = $.trim($('#mName').val());
	if (Utils.isNull(name)) {
		showAlert('名称不能为空!');
		return;
	}
	data['name'] = name;
	
	// 调用接口
	var response = callInterface("post", "admin/modifyLiveChannel", data, false);
	if (response.code == 0) {
		showMsg('操作成功!');
		$('#modifyLiveChannelDialog').modal("hide");

		// 刷新
		queryLiveChannel(currPage);
	}
	else {
		showErrorMsg(response.code, response.message);
	}
}

/**
 * 删除
 */
function deleteLiveChannel($id, $name) {
	// 设置参数
	var $data = {
		"id" : $id
	};
	// 询问框
	layer.confirm('确定删除[' + $name + ']吗？', {
		offset :'20px',btn: ['确定','取消']
	// 按钮
	}, function() {
		// 调用接口
		var response = callInterface("post", "admin/deleteLiveChannel", $data, false);
		if (response.code == 0) {
			showMsg('操作成功!');

			// 刷新
			queryLiveChannel();
		}
		else {
			showErrorMsg(response.code, response.message);
		}
	}, function() {
		return;
	});
}