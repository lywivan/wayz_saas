/**
 * 准备函数
 */
$(document).ready(function() {
	// 加载表格
	queryDataList(1, 10);
	
});

var data = [
    {
        "id":1,
        "name":"拆车网-第三场",
        "bDate":"2021/1/9 15:00:00",
        "eDate":"2021/1/9 16:00:00",
        "liveQd":"抖音直播",
        "liveType":"卖货直播",
        "customerName":"北京车讯互联网股份有限公司",
        "mediaName":"上海水瓶鱼电子商务中心",
        "adMoney":70000,
        "mediaMoney":60000,
        "posCount":1000,
        "status":"直播结束",
        "url":"http://res.oohlink.com/live/1.jpg"
    },
    {
        "id":2,
        "name":"拆车网-第二场",
        "bDate":"2021/1/10 15:00:00",
        "eDate":"2021/1/10 16:00:00",
        "liveQd":"抖音直播",
        "liveType":"卖货直播",
        "customerName":"北京车讯互联网股份有限公司",
        "mediaName":"上海水瓶鱼电子商务中心",
        "adMoney":70000,
        "mediaMoney":60000,
        "posCount":1000,
        "status":"直播结束",
        "url":"http://res.oohlink.com/live/2.jpg"
    },
    {
        "id":3,
        "name":"拆车网-第三场",
        "bDate":"2021/1/16 15:00:00",
        "eDate":"2021/1/16 16:00:00",
        "liveQd":"抖音直播",
        "liveType":"卖货直播",
        "customerName":"北京车讯互联网股份有限公司",
        "mediaName":"上海水瓶鱼电子商务中心",
        "adMoney":70000,
        "mediaMoney":60000,
        "posCount":1000,
        "status":"直播结束",
        "url":"http://res.oohlink.com/live/3.jpg"
    },
    {
        "id":4,
        "name":"拆车网-第四场",
        "bDate":"2021/1/17 15:00:00",
        "eDate":"2021/1/17 16:00:00",
        "liveQd":"抖音直播",
        "liveType":"卖货直播",
        "customerName":"北京车讯互联网股份有限公司",
        "mediaName":"上海水瓶鱼电子商务中心",
        "adMoney":70000,
        "mediaMoney":60000,
        "posCount":1000,
        "status":"直播结束",
        "url":"http://res.oohlink.com/live/4.jpg"
    },
    {
        "id":5,
        "name":"拆车网-第五场",
        "bDate":"2021/1/23 16:00:00",
        "eDate":"2021/1/23 17:00:00",
        "liveQd":"抖音直播",
        "liveType":"卖货直播",
        "customerName":"北京车讯互联网股份有限公司",
        "mediaName":"上海水瓶鱼电子商务中心",
        "adMoney":70000,
        "mediaMoney":60000,
        "posCount":1000,
        "status":"直播结束",
        "url":"http://res.oohlink.com/live/5.jpg"
    }
];

/**
 * 查询
 */
function queryDataList(pageIndex, pageSize) {
	var count = data.length;
	
	// 清空表格及分页
	$("#dataTable tr:not(:first)").remove();
	$("#pageDiv").html("");

	// 动态添加一行
	if (count == 0) {
		var cols = $("#dataTable").find("tr:first th");
		$('#dataTable').append("<tr><td colspan='" + cols.length + "'>无数据!</td></tr>");
		return false;
	}

	var list = data;
	if (list != null && list.length > 0) {
		for ( var i in list) {
			var trHtml = "<tr>";
			trHtml += "<td align='center'>" + list[i].name + "</td>";
			trHtml += "<td align='center'>" + list[i].bDate + "</td>";
			trHtml += "<td align='center'>" + list[i].eDate + "</td>";
			trHtml += "<td align='center'>" + list[i].liveQd + "</td>";
			trHtml += "<td align='center'>" + list[i].liveType + "</td>";
			trHtml += "<td align='center'>" + list[i].customerName + "</td>";
			trHtml += "<td align='center'>" + list[i].mediaName + "</td>";
			trHtml += "<td align='center'>" + list[i].adMoney + "</td>";
			trHtml += "<td align='center'>" + list[i].mediaMoney + "</td>";
			trHtml += "<td align='center'>" + list[i].posCount + "</td>";
			trHtml += "<td align='center'>" + list[i].status + "</td>";
			
			trHtml += "<td align='center'>";
			// 操作
			trHtml += '<a href="javascript:void(0);" class="edit" onclick="getDetail(\''
				+ list[i].name
				+ '\',\''
				+ list[i].url
				+ '\');">查看详情</a> ';
			trHtml += "</td>";
			trHtml += "</tr>";

			// 动态添加一行
			$('#dataTable').append(trHtml);
		}
	}
	
	// 调用分页
	layui.use(['laypage'], function() {
		var laypage = layui.laypage;
		laypage.render({
			elem : 'pageDiv',
			// pages : pages,
			curr : pageIndex || 1, // 当前页
			limit : pageSize,
			count : count,
			limit : 10, 
			skip : true, // 是否开启跳页
			layout : ['count', 'prev', 'page', 'next', 'limit', 'skip'],
			groups : 5, // 连续显示分页数
			jump : function(obj, first) {  
				if (!first) {
					queryDataList(obj.curr, obj.limit);
				}
			}
		});
	});
}


/**
 * 获取详情
 * @param id
 */
function getDetail(name, url){
	
	$("#materialName").text(name);
	
	// 显示对话框
	$("#materialDetailDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});
	
	// 设置预览素材信息
	setFilePreviewHtml(FileType.IMAGE_V, name, url);
}
