/**
 * 用户,设备添加
 */
var ckList1 = new Array();
var ckList2 = new Array();
$(document).ready(function() {
	
	// 绑定函数
	$("#querySelectablePositionButton").bind("click", confirmQuerySelectablePosition);
	
	// 添加设备
	$("#addPositionButton").bind("click", function (){
		confirmAddPosition(false);
	});
	$("#addAllPositionButton").bind("click", function (){
		confirmAddPosition(true);
	});
	
	// 删除设备
	$("#deletePositionButton").bind("click", function (){
		confirmDeletePosition(false);
	});
	$("#deleteAllPositionButton").bind("click", function (){
		confirmDeletePosition(true);
	});
	
	// 列表 全选、反选
	var selectListAll1 = "selectListAll1", selectListAll2 = "selectListAll2";
	var rowCheckbox1 = "rowCheckbox1", rowCheckbox2 = "rowCheckbox2";
	$("#"+selectListAll1).click(function() {
		selectCbAll(this,rowCheckbox1,ckList1);
	});
	$("#"+selectListAll2).click(function() {
		selectCbAll(this,rowCheckbox2,ckList2);
	});
	// 列表单个 全选、反选
	$(document).on("click","input[name='"+rowCheckbox1+"']",function(){
		selectCbOne(this,selectListAll1,rowCheckbox1,ckList1);
	});
	$(document).on("click","input[name='"+rowCheckbox2+"']",function(){
		selectCbOne(this,selectListAll2,rowCheckbox2,ckList2);
	});
	
	// 模态框关闭事件，当钩子使用
//	$('#setPositionDialog').on('hide.bs.modal', function() {
//		queryAuditor();
//	});
});

/**
 * 显示设备对话框
 * 
 * @param auditorId
 */
function showSetPositionDialog(auditorId){
	$("#setPositionDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});
	$('#setPositionForm')[0].reset();
	$("#mId").val(auditorId);
	$("#liveCompanyId").val("");
	$form.render();
	
	//查询待选
	querySelectablePosition();
	
	//查询已选
	querySelectedPosition();
}

/**
 * 查询待选择的设备
 * 
 * @param pageIndex
 * @param pageSize
 * @returns {Boolean}
 */
function querySelectablePosition(pageIndex,pageSize) {
	// 组装参数
	var data = {};
	data['auditorId'] = $("#mId").val();
	var positionName = $("#positionName").val();
	if (Utils.isNotNull(positionName)) {
		data['positionName'] = positionName;
	}
	var companyId = $("#liveCompanyId").val();
	if (Utils.isNotNull(companyId)) {
		data['companyId'] = companyId;
	}
	
	//设置分页参数
	setPageIndex(data, pageIndex, pageSize);
	
	// 调用接口
	var response = callInterface("post", "auditor/querySelectablePosition", data, false);
	if (response.code == 0) {
		// 清空表格及分页
		$("#selectPositionTable tr:not(:first)").remove();
		$("#selectPageDiv").html("");

		var count = response.data.totalCount;
		if (count == null || count == 0) {
			var cols = $("#selectPositionTable").find("tr:first th");
			$('#selectPositionTable').append("<tr><td colspan='" + cols.length + "'>无数据!</td></tr>");
			return false;
		}
		
		var list = response.data.list;
		if (list != null && list.length > 0) {
			for ( var i in list) {
				var trHtml = "";
				// 3.是否选中
				var checked = '';
				if (strInArray_Pos(ckList1, list[i].positionId) > -1) {
					checked = 'checked';
				}
				trHtml += "<tr>";
				trHtml += "<td align='center' width='30px'><label><input name='rowCheckbox1' class='select_checkbox' " +
						""+checked+" type='checkbox'" + " value="+ list[i].positionId  + " /><span class='lbl'></span></label></td>";
				trHtml += "<td align='center'>" + list[i].positionName + "</td>";
				trHtml += "<td align='center'>" + list[i].playCode + "</td>";
				trHtml += "<td align='center'>" + list[i].provinceName + "-" + list[i].cityName + "-" + list[i].districtName + "</td>";
				trHtml += "<td align='center'>" + Utils.convertNull(list[i].address) + "</td>";
				trHtml += "<td align='center'>" + list[i].companyName + "</td>";
				trHtml += "<td align='center'>" + Utils.convertNull(list[i].sceneName) +"</td>";
				trHtml += "<td align='center'>" + (list[i].isOnline?"在线":"离线") +"</td>";
				trHtml += "</tr>";
				
				// 动态添加一行
				$('#selectPositionTable').append(trHtml);
			}
		}
			
		// 调用分页
		layui.use('laypage', function() {
			layui.laypage.render({
				elem : 'selectPageDiv',
				curr : pageIndex || 1, // 当前页
				count : count,
				limit : pageSize,
				layout : ['count', 'prev', 'page', 'next', 'limit', 'skip'],
				groups : 5, // 连续显示分页数
				jump : function(obj, first) { 
					if (!first) {
						querySelectablePosition(obj.curr,obj.limit);
					}
				}
			});
		});
		
		//4.处理分页后的复选框选中逻辑
		handlePageCb('selectListAll1','rowCheckbox1');
	}
}

/**
 * 查询已选择的设备
 */
function querySelectedPosition(pageIndex,pageSize) {
	// 组装参数
	var data = {};
	data['auditorId'] = $("#mId").val();
	
	//设置分页参数
	setPageIndex(data,pageIndex,pageSize);

	// 调用接口
	var response = callInterface("post", "auditor/querySelectedPosition", data, false);
	if (response.code == 0) {
		// 清空表格及分页
		$("#selectedPositionTable tr:not(:first)").remove();
		$("#selectedPageDiv").html("");

		var count = response.data.totalCount;
		if (count == null || count == 0) {
			var cols = $("#selectedPositionTable").find("tr:first th");
			$('#selectedPositionTable').append("<tr><td colspan='" + cols.length + "'>无数据!</td></tr>");
			return false;
		}
		
		var list = response.data.list;
		if (list != null && list.length > 0) {
			for ( var i in list) {
				var trHtml = "";
				// 3.是否选中
				var checked = '';
				if (strInArray_Pos(ckList2, list[i].positionId) > -1) {
					checked = 'checked';
				}
				trHtml += "<tr>";
				trHtml += "<td align='center' width='30px'><label><input name='rowCheckbox2' class='select_checkbox' " +
						""+checked+" type='checkbox'" + " value="+ list[i].positionId  + " /><span class='lbl'></span></label></td>";
				trHtml += "<td align='center'>" + list[i].positionName + "</td>";
				trHtml += "<td align='center'>" + list[i].playCode + "</td>";
				trHtml += "<td align='center'>" + list[i].provinceName + "-" + list[i].cityName + "-" + list[i].districtName + "</td>";
				trHtml += "<td align='center'>" + Utils.convertNull(list[i].address) + "</td>";
				trHtml += "<td align='center'>" + list[i].companyName + "</td>";
				trHtml += "<td align='center'>" + Utils.convertNull(list[i].sceneName) +"</td>";
				trHtml += "<td align='center'>" + (list[i].isOnline?"在线":"离线") +"</td>";
				trHtml += "</tr>";
				
				// 动态添加一行
				$('#selectedPositionTable').append(trHtml);
			}
		}
		
		// 调用分页
		layui.use('laypage', function() {
			layui.laypage.render({
				elem : 'selectedPageDiv',
				curr : pageIndex || 1, // 当前页
				count : count,
				limit : pageSize,
				layout : ['count', 'prev', 'page', 'next', 'limit', 'skip'],
				groups : 5, // 连续显示分页数
				jump : function(obj, first) { // 触发分页后的回调
					if (!first) {
						querySelectedPosition(obj.curr,obj.limit);
					}
				}
			});
		});
		
		//4.处理分页后的复选框选中逻辑
		handlePageCb('selectListAll2','rowCheckbox2');
	}
}

/**
 * 确认查询
 */
function confirmQuerySelectablePosition() {
	ckList1 = new Array();
	// ckList2 = new Array();
	querySelectablePosition();
	// querySelectedPosition();
}

/**
 * 确认添加设备
 */
function confirmAddPosition(isAllAdd){
	// 设置参数
	var data = {};
	data['auditorId'] = $("#mId").val();
	if(!isAllAdd){
		if (ckList1.length > 0) {
			data['positionIdList'] = JSON.stringify(ckList1);
		} else {
			showAlert("请选择设备!");
			return;
		}
	}
	
	// 调用接口
	var response = callInterface("post", "auditor/addPosition", data, false);
	if (response.code == 0) {
		// 刷新
		ckList1 = new Array();
		ckList2 = new Array();
		querySelectablePosition();
		querySelectedPosition();
	} else {
		showErrorMsg(response.code, response.message);
	}
}

/**
 * 确认删除设备
 */
function confirmDeletePosition(isDelAll){
	// 设置参数
	var data = {};
	data['auditorId'] = $("#mId").val();
	var delInfo = "确认删除设备吗？";
	if(!isDelAll){
		if (ckList2.length > 0) {
			data['positionIdList'] = JSON.stringify(ckList2);
		} else {
			showAlert("请选择设备!");
			return;
		}
	}else{
		delInfo = "确认删除全部设备吗？";
	}
	
	// 询问框
	layer.confirm(delInfo, {
	  offset :'20px',
	  btn: ['确定','取消'] //按钮
	}, function(index){
		// 调用接口
		var response = callInterface("post", "auditor/deletePosition", data, false);
		if (response.code == 0) {
			// 刷新
			ckList1 = new Array();
			ckList2 = new Array();
			querySelectablePosition();
			querySelectedPosition();
		} else {
			showErrorMsg(response.code, response.message);
		}
		layer.close(index);
	}, function(){
		return;
	});
}
