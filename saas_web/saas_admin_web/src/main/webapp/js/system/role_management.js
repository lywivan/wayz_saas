// 角色对应功能权限
var rolerightdata = {};
// 所有功能权限树数组
var menuRightArray;

// 设置树属性
var setting = {
	check : {
		enable : true
	},
	view : {
		showIcon : false
	},
	data : {
		simpleData : {
			enable : true
		}
	}
};

/**
 * 准备函数
 */
var operateRight = new Object();
$(document).ready(function() {
	// 加载权限
	loadRight();

	// 加载表格
	queryRole();

	// 绑定函数
	$("#queryButton").bind("click", confirmQueryRole);
	$("#createButton").bind("click", showCreateRoleDialog);
	$("#modifyButton").bind("click", showModifyRoleDialog);
	$("#confirmCreateButton").bind("click", confirmCreateRole);
	$("#confirmModifyButton").bind("click", confirmModifyRole);
	$("#confirmSetRoleRightButton").bind("click", confirmSetRoleRight);
});

/**
 * 加载权限
 */
function loadRight() {
	operateRight.modifyRight = $("#modify").val();
	operateRight.setRoleRight = $("#setRoleRight").val();
	operateRight.deleteRight = $("#delete").val();
}

/**
 * 查询角色
 */
function queryRole(pageIndex, pageSize) {
	// 组装参数
	var data = {};
	var name = $.trim($("#name").val());
	if (name != "") {
		data['name'] = name;
	}
	
	// 设置分页参数
	setPageIndex(data, pageIndex, pageSize);

	// 调用接口
	var response = callInterface("post", "role/queryRole", data, false);
	if (response.code == 0) {
		// 清空表格及分页
		$("#roleTable tr:not(:first)").remove();
		$("#pageDiv").html("");
		var dataTable = $("#roleTable");
		
		var count = response.data.totalCount;
		if (count == null || count == 0) {
			var cols = dataTable.find("tr:first th");
			dataTable.append("<tr><td colspan='" + cols.length + "'>无数据!</td></tr>");
			return false;
		}

		// 得到总页数
		var list = response.data.list;
		if (list != null && list.length > 0) {
			for ( var i in list) {
				var trHtml = "<tr>";
				trHtml += "<td align='center'>" + list[i].name + "</td>";
				trHtml += "<td>" + handleEmptyStr(list[i].description) + "</td>";
				// 操作
				trHtml += "<td align='center' width='150px'>";
				if (operateRight.modifyRight == 1) {
					trHtml += "<a href='javascript:void(0);' class='edit' onclick='showModifyRoleDialog("
							+ JSON.stringify(list[i]) + ");'>编辑</a>&nbsp;&nbsp;";
				}
				if (operateRight.deleteRight == 1) {
					trHtml += "<a href='javascript:void(0);' class='delete' onclick='deleteRole(" + list[i].id
							+ ",\"" + list[i].name + "\");'>删除</a>&nbsp;&nbsp;";
				}
				if (operateRight.setRoleRight == 1) {
					trHtml += "<a href='javascript:void(0);' class='copy' onclick='showRoleRightDialog(" + list[i].id
							+ ",\"" + list[i].name + "\");'>设置权限</a>";
				}
				trHtml += "</td>";
				trHtml += "</tr>";

				// 动态添加一行
				dataTable.append(trHtml);
			}
		}

		// 调用分页
		layui.use(['laypage'], function() {
			var laypage = layui.laypage;
			laypage.render({
				elem : 'pageDiv',
				// pages : pages,
				curr : pageIndex || 1, // 当前页
				count : count,
				limit : pageSize,
				skip : true, // 是否开启跳页
				layout : ['count', 'prev', 'page', 'next', 'limit', 'skip'],
				groups : 5, // 连续显示分页数
				jump : function(obj, first) {
					if (!first) {
						queryRole(obj.curr, obj.limit);
					}
				}
			});
		});
	}else {
		showErrorMsg(response.code, response.message);
	}
}

/**
 * 确认查询
 */
function confirmQueryRole() {
	queryRole();
}

/**
 * 显示创建对话框
 */
function showCreateRoleDialog() {
	$("#createRoleDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});
	$('#addRoleForm')[0].reset();
}

/**
 * 确认创建
 */
function confirmCreateRole() {
	// 获取参数
	var data = {};
	var name = $.trim($('#cName').val());
	if (name == "") {
		showAlert('角色名称不能为空!');
		return false;
	}
	data['name'] = name;

	var description = $.trim($('#cDescription').val());
	if (description != "") {
		data['description'] = description;
	}

	// 调用接口
	var response = callInterface("post", "role/createRole", data, false);
	if (response.code == 0) {
		showMsg('操作成功!');
		$('#createRoleDialog').modal("hide");

		// 刷新
		queryRole();
	}
	else {
		showErrorMsg(response.code, response.message);
	}
	return false;
}

/**
 * 显示修改角色对话框
 */
function showModifyRoleDialog(data) {
	// 显示对话框
	$("#modifyRoleDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});
	$('#modifyRoleForm')[0].reset();
	
	// 设置参数
	$("#mId").val(data.id);
	$("#mName").val(data.name);
	$("#mDescription").val(data.description);
}

/**
 * 确认修改角色
 */
function confirmModifyRole() {
	// 获取参数
	var data = {};
	data['id'] = $.trim($("#mId").val());
	var name = $.trim($('#mName').val());
	if (name == "") {
		showAlert('角色名称不能为空!');
		return false;
	}
	data['name'] = name;
	var description = $.trim($('#mDescription').val());
	data['description'] = description;

	// 调用接口
	var response = callInterface("post", "role/modifyRole", data, false);
	if (response.code == 0) {
		showMsg('操作成功!');

		// 关闭
		$('#modifyRoleDialog').modal("hide");

		// 刷新
		queryRole(currPage);
	}
	else {
		showErrorMsg(response.code, response.message);
	}
	return false;
}

/**
 * 显示设置角色功能权限对话框
 */
function showRoleRightDialog(roleId, roleName) {
	$("#roleInfo").html("(" + roleName + ")");
	$("#roleId").val(roleId);
	$("#roleName").val(roleName);

	// 查询角色对应功能权限
	queryRoleRight(roleId);

	// 初始化树
	$.fn.zTree.init($("#roleRightTree"), setting, menuRightArray);

	// 显示对话框
	$("#setRoleRightDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});
}

/**
 * 显示删除对话框
 */
function deleteRole($id, $name) {
	var $data = {
		"id" : $id
	};

	// 询问框
	layer.confirm('确定删除[' + $name + ']吗？', {
		offset :'20px',btn: ['确定','取消']
	// 按钮
	}, function() {
		// 调用接口
		var response = callInterface("post", "role/deleteRole", $data, false);
		if (response.code == 0) {
			showMsg('操作成功!');

			// 刷新
			queryRole();
		}
		else {
			showErrorMsg(response.code, response.message);
		}
	}, function() {
		return;
	});
}

/**
 * 确认设置角色权限
 */
function confirmSetRoleRight() {
	var $roleId = $("#roleId").val();
	var rightIdList = new Array();
	var zTree = $.fn.zTree.getZTreeObj("roleRightTree");
	var nodes = zTree.getCheckedNodes(true);
	for (var i = 0; i < nodes.length; i++) {
		// 获取叶子节点
		//if (!nodes[i].isParent && nodes[i].checked) {
			rightIdList.push(parseInt(nodes[i].id));
		//}
	}

	// 设置参数
	var $data = {
		'roleId' : $roleId,
		'menuIdList' : rightIdList//JSON.stringify(rightIdList)
	};

	// 调用接口
	var response = callInterface2("post", "role/authorityRole", JSON.stringify($data), false);
	if (response.code == 0) {
		showMsg('操作成功!');

		$('#setRoleRightDialog').modal("hide");
	}
	else {
		showErrorMsg(response.code, response.message);
	}

}

/**
 * 查询角色对应功能权限
 */
function queryRoleRight($roleId) {
	// 设置参数
	var $data = {
		'roleId' : $roleId
	};
	// 调用接口
	var response = callInterface("post", "menu/queryRoleMenu", $data, false);
	if (response.code == 0) {
		rolerightdata = {}
		var rightList = response.data;
		if (rightList.length > 0) {
			for ( var i in rightList) {
				rolerightdata[rightList[i].id] = true;
			}
		}
		// 查询所有菜单功能权限树
		queryMenuRightTree();

	}
	else {
		showErrorMsg(response.code, response.message);
	}
}

/**
 * 查询所有菜单功能权限树
 */
function queryMenuRightTree() {
	// 初始化功能权限数组
	menuRightArray = new Array();

	// 调用接口
	var response = callInterface("post", "menu/queryAllMenu", null, false);
	if (response.code == 0) {
		// 功能权限对象
		var list = response.data;
		if (list != null && list.length > 0) {
			getMenuRightArray(list);
		}
	}
	else {
		showErrorMsg(response.code, response.message);
	}
}

/**
 * 父功能模块树
 */
function getMenuRightArray(list) {
	for ( var i in list) {
		var childlist = list[i].childList;
		if (childlist != null && childlist.length > 0) {
			getMenuRightChildArray(list[i].id, childlist);
		}
	}
}

/**
 * 子功能模块树
 */
function getMenuRightChildArray(pId, list) {
	for ( var i in list) {
		var menu = new Object();
		if (list[i].parentId != null) {
			pId = list[i].parentId;
		}
		menu.pId = pId;
		menu.id = list[i].id;
		var desc = list[i].description;
		if (typeof (desc) != "undefined" && desc != null && desc != "") {
			menu.name = list[i].name + "(" + desc + ")";
		}
		else {
			menu.name = list[i].name;
		}
		menu.open = false;
		if (rolerightdata[list[i].id]) {
			menu.checked = true;
		}
		var childlist = list[i].childList;
		if (childlist != null && childlist.length > 0) {
			getMenuRightChildArray(pId, childlist);
		}
		else {
			var right = list[i].rightList;
			if (right != null && right.length > 0) {
				getMenuRightChildArray(list[i].id, right);
			}
		}
		menuRightArray.push(menu);
	}
}
