/**
 * 显示公司详情
 */
function showCompanyDetail(id) {
	$("#companyDetailDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});

	// 调用接口
	var response = callInterface("post", "saas/company/getCompanyDetail", {
		'id' : id
	}, false);
	if (response.code == 0) {
		var data = response.data;
		if (data != null) {
			$("#vName").html(data.name);
			$("#vUserName").html(data.userName);
			$("#vContacts").html(data.contacts);
			$("#vPhone").html(Utils.convertNull(data.telephone));
			$("#vEmail").html(Utils.convertNull(data.email));
			$("#vAddress").html(Utils.convertNull(data.address));
			$("#vSourceType").html(Utils.convertNull(ComSourceType[data.sourceType]));
			
			$("#vVersionName").html(Utils.convertNull(data.versionName));
			$("#vBalance").html(Utils.toFormatDecimal(data.balance,2));
			// 总
			var materialLimit = Utils.isNull(data.materialLimit)?'--':Utils.toFormatDecimal(getFileSizeMByB(data.materialLimit),0)+"M";
			$("#vMaterialTotalLimit").html(materialLimit);
			// 剩余
			var surplusMaterialLimit = Utils.isNull(data.surplusMaterialLimit)?'--':Utils.toFormatDecimal(getFileSizeMByB(data.surplusMaterialLimit),2)+"M";
			$("#vSurplusMaterialLimit").html(surplusMaterialLimit);
			// 已使用
			var alreadyMaterialLimit = Utils.isNull(data.alreadyMaterialLimit)?'--':Utils.toFormatDecimal(getFileSizeMByB(data.alreadyMaterialLimit),2)+"M";
			$("#vAlreadyMaterialLimit ").html(alreadyMaterialLimit);
			
			$("#vUserLimit ").html(data.userLimit);
			$("#vStatusName ").html(data.statusName);
			// 试用账号
			if(data.status == 2){
				$(".vValidUntilDateTd").show();
				$("#vValidUntilDate").html(data.validUntilDate);
				$("#vIsWatermark").html(data.isWatermark ? '是' : '否');
			}else{
				$(".vValidUntilDateTd").hide();
			}
			$("#vAgentName").html(data.agentName);
			if (Utils.isNotNull(data.agentName)) {
				$("#vIsLogin").closest('td').show();
				$("#vIsLogin").html(data.isLogin ? '是' : '否');
			}
			else {
				$("#vIsLogin").closest('td').hide();
			}
			$("#vRequirePlan").html(data.isAdPlan ? '是' : '否');
		}
	}
	else {
		showErrorMsg(response.code, response.message);
	}
	
	// 调用接口获取OEM
	var response = callInterface("post", "saas/company/getCompanyOEM", {
		"id" : id
	}, false);
	if (response.code == 0) {
		var data = response.data;
		if (data != null) {
			$("#vPlatFormDomain").html(data.domain);
			$("#vPlatFormName").html(data.websiteTitle);
			if( Utils.isNotNull(data.logo) ){
				$("#vPlatFormLogo").html(fillFileDiv(data.logo, FileType.IMAGE_V));
			}else{
				$("#vPlatFormLogo").html("");
			}
		}
	}
}
