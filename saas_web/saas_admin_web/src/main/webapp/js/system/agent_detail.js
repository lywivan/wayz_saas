/**
 * 显示公司详情
 */
function showAgentDetail(id) {
	$("#agentDetailDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});

	// 调用接口
	var response = callInterface("post", "agent/getAgentCompanyDetail", {
		'id' : id
	}, false);
	if (response.code == 0) {
		var data = response.data;
		if (data != null) {
			$("#vDAgentName").html(data.name);
			$("#vAgentContacts").html(data.contacts);
			$("#vAgentPhone").html(Utils.convertNull(data.telephone));
			$("#vAgentProxyType").html(data.proxyType=="1"?"公司":"个人");
			$("#vAgentStatus").html(data.status=="1"?"启用":"禁用");
			$("#vAgentPositionLimit").html(Utils.convertNull(data.positionLimit));
			$("#vAgentConsumePositionLimit").html(Utils.convertNull(data.consumePositionLimit));
			$("#vAgentDescription").html(Utils.convertNull(data.description));
			var areaText = "";
			if(Utils.isNotNull(data.provinceName) && Utils.isNotNull(data.cityName)){
				areaText = data.provinceName + "-" + data.cityName;
			}
			$("#vAgentArea").html(areaText);
			$("#vAgentCategoryName").html(Utils.convertNull(data.categoryName));
			
			if(data.proxyType == "1"){
				if( Utils.isNotNull(data.businessLicense) ){
					$("#vAgentBusinessLicense").html(fillFileDiv(data.businessLicense, FileType.IMAGE_V));
				}else{
					$("#vAgentBusinessLicense").html("");
				}
				$(".agent-company-data").show();
				$(".agent-personal-data").hide();
			}else{
				if( Utils.isNotNull(data.identityBverse) ){
					$("#vAgentIdentityBverse").html(fillFileDiv(data.identityBverse, FileType.IMAGE_V));
				}else{
					$("#vAgentIdentityBverse").html("");
				}
				if( Utils.isNotNull(data.identityReverse) ){
					$("#vAgentIdentityReverse").html(fillFileDiv(data.identityReverse, FileType.IMAGE_V));
				}else{
					$("#vAgentIdentityReverse").html("");
				}
				$(".agent-company-data").hide();
				$(".agent-personal-data").show();
			}
		}
	}
	else {
		showErrorMsg(response.code, response.message);
	}
}
