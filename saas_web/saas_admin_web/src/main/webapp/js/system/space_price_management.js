/**
 * 准备函数
 */
var operateRight = new Object();
$(document).ready(function() {
	
	// 加载权限
	loadRole();

	// 加载表格
	querySpacePrice();

	// 绑定函数
	$("#queryButton").bind("click", confirmQuerySpacePrice);
	$("#createButton").bind("click", showCreateSpacePriceDialog);
	$("#confirmCreateButton").bind("click", confirmCreateSpacePrice);
	$("#confirmModifyButton").bind("click", confirmModifySpacePrice);
});

/**
 * 加载权限
 */
function loadRole() {
	operateRight.modifyRight = $("#modify").val();
	operateRight.deleteRight = $("#delete").val();
}

/**
 * 查询用户
 */
function querySpacePrice(pageIndex, pageSize) {
	// 组装参数
	var data = {};

	// 设置分页参数
	setPageIndex(data, pageIndex, pageSize);
	
	// 调用接口
	var response = callInterface("post", "admin/querySpacePrice", data, false);
	if (response.code == 0) {
		// 清空表格及分页
		$("#SpacePriceTable tr:not(:first)").remove();
		var list = response.data;
		if (list != null && list.length > 0) {
			for ( var i in list) {
				var trHtml = "<tr>";
				trHtml += "<td align='center'>" + list[i].name + "</td>";
				trHtml += "<td align='center'>" + list[i].space + "</td>";
				trHtml += "<td align='center'>" + list[i].salePrice + "</td>";
				trHtml += "<td align='center'>" + list[i].discountPrice + "</td>";
				// 操作
				trHtml += "<td align='center' class='operationColumn'>";
				if (operateRight.modifyRight == 1) {
					trHtml += "<a href='javascript:void(0);' class='edit' onclick='showModifySpacePriceDialog(" + JSON.stringify(list[i]) + ");'>编辑</a>&nbsp;&nbsp;";
				}
				if (operateRight.deleteRight == 1) {
					trHtml += "<a href='javascript:void(0);' class='delete' onclick='deleteSpacePrice(" + list[i].id
							+ ",\"" + list[i].name + "\");'>删除</a>";
				}
				trHtml += "</td>";
				trHtml += "</tr>";

				// 动态添加一行
				$('#SpacePriceTable').append(trHtml);
			}
		}else{
			// 获取表格行数、列数
			var cols = $("#SpacePriceTable").find("tr:first th");
			$('#SpacePriceTable').append("<tr><td colspan='" + cols.length + "'>无数据!</td></tr>");
		}
	}else {
		showErrorMsg(response.code, response.message);
	}
}

/**
 * 确认查询
 */
function confirmQuerySpacePrice() {
	querySpacePrice();
}

/**
 * 显示创建对话框
 */
function showCreateSpacePriceDialog() {
	// 弹出框
	$("#createSpacePriceDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});
	$('#addSpacePriceForm')[0].reset();
}

/**
 * 确认创建
 */
function confirmCreateSpacePrice() {
	// 获取参数
	var data = {};
	var name = $.trim($('#cName').val());
	if (Utils.isNull(name)) {
		showAlert('名称不能为空!');
		return;
	}
	data['name'] = name;
	var space = $("#cSpace").val();
	if (Utils.isNull(space)) {
		showAlert("空间数量(GB)不能为空!");
		return;
	}
	data['space'] = space;
	var salePrice = $.trim($('#cSalePrice').val());
	if (Utils.isNull(salePrice)) {
		showAlert("原价不能为空!");
		return false;
	}
	data['salePrice'] = salePrice;
	var discountPrice = $.trim($('#cDiscountPrice').val());
	if (Utils.isNull(discountPrice)) {
		showAlert("优惠价不能为空!");
		return false;
	}
	data['discountPrice'] = discountPrice;

	// 调用接口
	var response = callInterface("post", "admin/createSpacePrice", data, false);
	if (response.code == 0) {
		showMsg('操作成功!');
		$('#createSpacePriceDialog').modal("hide");

		// 刷新
		querySpacePrice();
	}
	else {
		showErrorMsg(response.code, response.message);
	}
}

/**
 * 显示修改用户对话框
 */
function showModifySpacePriceDialog(data) {
	// 显示对话框
	$("#modifySpacePriceDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});
	$('#modifySpacePriceForm')[0].reset();
	
	// 设置参数
	$("#mId").val(data.id);
	$("#mName").val(data.name);
	$("#mSpace").val(data.space);
	$("#mSalePrice").val(data.salePrice);
	$("#mDiscountPrice").val(data.discountPrice);

}

/**
 * 确认修改用户
 */
function confirmModifySpacePrice() {
	
	// 获取参数
	var data = {};
	data['id'] = $.trim($("#mId").val());
	var name = $.trim($('#mName').val());
	if (Utils.isNull(name)) {
		showAlert('名称不能为空!');
		return;
	}
	data['name'] = name;
	var space = $("#mSpace").val();
	if (Utils.isNull(space)) {
		showAlert("空间数量(GB)不能为空!");
		return;
	}
	data['space'] = space;
	var salePrice = $.trim($('#mSalePrice').val());
	if (Utils.isNull(salePrice)) {
		showAlert("原价不能为空!");
		return false;
	}
	data['salePrice'] = salePrice;
	var discountPrice = $.trim($('#mDiscountPrice').val());
	if (Utils.isNull(discountPrice)) {
		showAlert("优惠价不能为空!");
		return false;
	}
	data['discountPrice'] = discountPrice;
	
	// 调用接口
	var response = callInterface("post", "admin/modifySpacePrice", data, false);
	if (response.code == 0) {
		showMsg('操作成功!');
		$('#modifySpacePriceDialog').modal("hide");

		// 刷新
		querySpacePrice(currPage);
	}
	else {
		showErrorMsg(response.code, response.message);
	}
}

/**
 * 删除
 */
function deleteSpacePrice($id, $name) {
	// 设置参数
	var $data = {
		"id" : $id
	};

	// 询问框
	layer.confirm('确定删除[' + $name + ']吗？', {
		offset :'20px',btn: ['确定','取消']
	// 按钮
	}, function() {
		// 调用接口
		var response = callInterface("post", "admin/deleteSpacePrice", $data, false);
		if (response.code == 0) {
			showMsg('操作成功!');

			// 刷新
			querySpacePrice();
		}
		else {
			showErrorMsg(response.code, response.message);
		}
	}, function() {
		return;
	});
}