/**
 * 准备函数
 */
var operateRight = new Object();
var cContentEditor;
var mContentEditor;
$(document).ready(function() {
	// 加载权限
	loadRight();

	// 加载表格
	querySysNotice();

	// 绑定函数
	$("#queryButton").bind("click", confirmQuerySysNotice);
	$("#createButton").bind("click", showCreateSysNoticeDialog);
	$(".confirmCreateButton").bind("click", function(){
		confirmCreateSysNotice(this);
	});
	$(".confirmModifyButton").bind("click", function(){
		confirmModifySysNotice(this);
	});
});

/**
 * 加载权限
 */
function loadRight() {
	operateRight.modifyRight = $("#modify").val();
	operateRight.deleteRight = $("#delete").val();
}

/**
 * 查询角色
 */
function querySysNotice(pageIndex, pageSize) {
	// 组装参数
	var data = {};
	var title = $.trim($("#title").val());
	if (Utils.isNotNull(title)) {
		data['title'] = title;
	}
	
	// 设置分页参数
	setPageIndex(data, pageIndex, pageSize);
	
	// 调用接口
	var response = callInterface("post", "saas/notice/queryNotice", data, false);
	if (response.code == 0) {
		// 清空表格及分页
		$("#SysNoticeTable tr:not(:first)").remove();
		$("#pageDiv").html("");

		var count = response.data.totalCount;
		if (count == null || count == 0) {
			var cols = $("#SysNoticeTable").find("tr:first th");
			$('#SysNoticeTable').append("<tr><td colspan='" + cols.length + "'>无数据!</td></tr>");
			return false;
		}

		// 得到总页数
		var list = response.data.list;
		if (list != null && list.length > 0) {
			for ( var i in list) {
				var trHtml = "<tr>";
				trHtml += "<td align='center'>" + list[i].title + "</td>";
				/*trHtml += "<td>" + handleEmptyStr(list[i].content) + "</td>";*/
				trHtml += "<td align='center'>" + handleEmptyStr(list[i].statusName) + "</td>";
				trHtml += "<td align='center'>" + (Utils.isNull(list[i].pushTime)?'--':list[i].pushTime) + "</td>";
				
				// 操作
				trHtml += "<td align='center' width='120px'>";
				if (operateRight.modifyRight == 1 && list[i].status == 1) {
					trHtml += "<a href='javascript:void(0);' class='edit' onclick='showModifySysNoticeDialog(" + list[i].id
							+ ",\);'>编辑</a>&nbsp;&nbsp;";
				}
				if (operateRight.deleteRight == 1) {
					trHtml += "<a href='javascript:void(0);' class='delete' onclick='deleteSysNotice(" + list[i].id
							+ ",\"" + list[i].title + "\");'>删除</a>&nbsp;&nbsp;";
				}
				trHtml += "</td>";
				trHtml += "</tr>";

				// 动态添加一行
				$('#SysNoticeTable').append(trHtml);
			}
		}
		
		// 调用分页
		layui.use(['laypage'], function() {
			var laypage = layui.laypage;
			laypage.render({
				elem : 'pageDiv',
				// pages : pages,
				curr : pageIndex || 1, // 当前页
				count : count,
				limit : pageSize,
				skip : true, // 是否开启跳页
				layout : ['count', 'prev', 'page', 'next', 'limit', 'skip'],
				groups : 5, // 连续显示分页数
				jump : function(obj, first) {
					if (!first) {
						querySysNotice(obj.curr, obj.limit);
					}
				}
			});
		});
	}else {
		showErrorMsg(response.code, response.message);
	}
}

/**
 * 确认查询
 */
function confirmQuerySysNotice() {
	querySysNotice();
}

/**
 * 显示创建对话框
 */
function showCreateSysNoticeDialog() {
	$("#createSysNoticeDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});
	$('#addSysNoticeForm')[0].reset();
	
	//创建editor
	cContentEditor = undefined;
	var E = window.wangEditor;
    cContentEditor = new E('#cContent');
    cContentEditor.customConfig.customUploadImg = function (files, insert) {
    	//自定义的上传图片到服务器方法 
        uploadEditorImage(files,insert);
    }
    cContentEditor.create();
    cContentEditor.txt.clear();
}

/**
 * 确认创建
 */
function confirmCreateSysNotice(t) {
	// 获取参数
	var data = {};
	
	var title = $.trim($('#cTitle').val());
	if (Utils.isNull(title)) {
		showAlert('通知标题不能为空!');
		return false;
	}
	data['title'] = title;
	
	var ehtml = $.trim(cContentEditor.txt.html());
	if( ehtml == "<p><br></p>" ){
		showAlert('通知内容不能为空!');
		return false;
	}
	data['content'] = ehtml;
	
	var isPush = $(t).attr('data-ispush');
	data['status'] = isPush

	// 调用接口
	var response = callInterface("post", "saas/notice/createNotice", data, false);
	if (response.code == 0) {
		showMsg('操作成功!');
		$('#createSysNoticeDialog').modal("hide");
		// 刷新
		querySysNotice();
	}
	else {
		showErrorMsg(response.code, response.message);
	}
}

/**
 * 显示修改角色对话框
 */
function showModifySysNoticeDialog(id) {
	// 显示对话框
	$("#modifySysNoticeDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});
	$('#modifySysNoticeForm')[0].reset();
	
	// 设置参数
	$("#mId").val(id);
	getNoticeDetail(id);
}

/**
 * 获取通知详情
 * @param id
 */
function getNoticeDetail(id){
	// 调用接口
	var response = callInterface("post", "saas/notice/getNoticeDetail", {
		'id' : id
	}, false);
	if (response.code == 0) {
		var data = response.data;
		$("#mId").val(data.id);
		$("#mTitle").val(data.title);
		
		mContentEditor = undefined;
		var E = window.wangEditor;
	    mContentEditor = new E('#mContent');
	    mContentEditor.customConfig.customUploadImg = function (files, insert) {
	    	uploadEditorImage(files,insert);
	    }
	    mContentEditor.create();
	    mContentEditor.txt.html(data.content);
		
	} else {
		showErrorMsg(response.code, response.message);
	}
}

/**
 * 确认修改角色
 */
function confirmModifySysNotice(t) {
	// 获取参数
	var data = {};
	data['id'] = $.trim($("#mId").val());
	
	var title = $.trim($('#mTitle').val());
	if (Utils.isNull(title)) {
		showAlert('通知标题不能为空!');
		return false;
	}
	data['title'] = title;
	
	var ehtml = $.trim(mContentEditor.txt.html());
	if( ehtml == "<p><br></p>" ){
		showAlert('通知内容不能为空!');
		return false;
	}
	data['content'] = ehtml;
	
	var isPush = $(t).attr('data-ispush');
	data['status'] = isPush

	// 调用接口
	var response = callInterface("post", "saas/notice/modifyNotice", data, false);
	if (response.code == 0) {
		showMsg('操作成功!');
		// 关闭
		$('#modifySysNoticeDialog').modal("hide");
		// 刷新
		querySysNotice();
	}
	else {
		showErrorMsg(response.code, response.message);
	}
}

/**
 * 显示删除对话框
 */
function deleteSysNotice($id, $name) {
	var $data = {
		"id" : $id
	};

	// 询问框
	layer.confirm('确定删除[' + $name + ']吗？', {
		offset :'20px',btn: ['确定','取消']
	// 按钮
	}, function() {
		// 调用接口
		var response = callInterface("post", "saas/notice/deleteNotice", $data, false);
		if (response.code == 0) {
			showMsg('操作成功!');
			// 刷新
			querySysNotice();
		}
		else {
			showErrorMsg(response.code, response.message);
		}
	}, function() {
		return;
	});
}