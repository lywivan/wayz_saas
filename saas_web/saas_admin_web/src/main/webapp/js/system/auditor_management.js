/**
 * 准备函数
 */
var operateRight = new Object();
$(document).ready(function() {
	// 加载权限
	loadRight();

	// 加载表格
	queryAuditor();
	
	// 绑定函数
	$("#queryButton").bind("click", confirmQueryAuditor);
	$("#createButton").bind("click", showCreateAuditorDialog);
	$("#confirmCreateButton").bind("click", confirmCreateAuditor);
	$("#confirmModifyButton").bind("click", confirmModifyAuditor);
	$("#confirmResetPassword").bind("click", resetPassword);
	
	// 加载form模块
	layui.use('form', function(){
		$form = layui.form;
	});
});

/**
 * 加载权限
 */
function loadRight() {
	operateRight.modifyRight = $("#modify").val();
	operateRight.deleteRight = $("#delete").val();
	operateRight.modifyUserPwd = $("#modifyUserPwd").val();
	operateRight.setPositionRight = $("#setSenceDevice").val();
}

/**
 * 查询
 */
function queryAuditor(pageIndex, pageSize) {
	// 组装参数
	var data = {};
	var name = $.trim($("#name").val());
	if (name != "") {
		data['name'] = name;
	}
	var telephone = $.trim($("#telephone").val());
	if (telephone != "") {
		data['telephone'] = telephone;
	}
	var status = $.trim($("#status").val());
	if (status != "") {
		data['status'] = status;
	}
	var type = $.trim($("#type").val());
	if (type != "") {
		data['type'] = type;
	}

	// 设置分页参数
	setPageIndex(data, pageIndex, pageSize);

	// 调用接口
	var response = callInterface("post", "auditor/queryAuditor", data, false);
	if (response.code == 0) {
		// 清空表格及分页
		$("#AuditorTable tr:not(:first)").remove();
		$("#pageDiv").html("");

		var count = response.data.totalCount;
		if (count == null || count == 0) {
			var cols = $("#AuditorTable").find("tr:first th");
			$('#AuditorTable').append("<tr><td colspan='" + cols.length + "'>无数据!</td></tr>");
			return false;
		}
		var list = response.data.list;
		if (list != null && list.length > 0) {
			for ( var i in list) {
				var trHtml = "<tr>";
				trHtml += "<td align='center'>" + list[i].name + "</td>";
				trHtml += "<td align='center'>" + list[i].typeName + "</td>";
				trHtml += "<td align='center'>" + list[i].contacts + "</td>";
				trHtml += "<td align='center'>" + list[i].telephone + "</td>";
				trHtml += "<td align='center'><input id='status_"+list[i].id+"' class='ace ace-switch' type='checkbox' "+ 
						(list[i].status == 1? 'checked': '')+" onclick='modifyAuditorStatus(" + list[i].id + ");' /> <span class='lbl'></span></td>";
				trHtml += "<td align='center'>" + list[i].createdTime + "</td>";
				trHtml += "<td align='center' style='width:260px;'>";
				if(operateRight.modifyRight == 1) {
					trHtml += "<a href='javascript:void(0);' class='edit' onclick='showModifyAuditorDialog(" + JSON.stringify(list[i]) + ");'>编辑</a>&nbsp;&nbsp;";
				}
				if(operateRight.deleteRight == 1) {
					trHtml += "<a href='javascript:void(0);' class='delete' onclick='deleteAuditor(" + list[i].id + ",\"" + list[i].name + "\");'>删除</a>&nbsp;&nbsp;";
				}
				if (operateRight.modifyUserPwd == 1) {
					trHtml += '<a href="javascript:void(0);" class="add" onclick="showResetPasswordDialog(\'' + list[i].telephone + '\');">重置密码</a>&nbsp;&nbsp;';
				}
				if(operateRight.setPositionRight == 1 && list[i].type == 3) {
					trHtml += '<a class="copy" href="javascript:showSetPositionDialog('+list[i].id +',\''+list[i].name+'\');">添加设备</a>&nbsp;&nbsp;';
				}
				trHtml += "</td>";
				trHtml += "</tr>";

				// 动态添加一行
				$('#AuditorTable').append(trHtml);
			}
		}

		// 调用分页
		layui.use(['laypage'], function() {
			var laypage = layui.laypage;
			laypage.render({
				elem : 'pageDiv',
				// pages : pages,
				curr : pageIndex || 1, // 当前页
				count : count,
				limit : pageSize,
				skip : true, // 是否开启跳页
				layout : ['count', 'prev', 'page', 'next', 'limit', 'skip'],
				groups : 5, // 连续显示分页数
				jump : function(obj, first) {
					if (!first) {
						queryAuditor(obj.curr, obj.limit);
					}
				}
			});
		});
	}else {
		showErrorMsg(response.code, response.message);
	}
}

/**
 * 确认查询
 */
function confirmQueryAuditor() {
	queryAuditor();
}


/**
 * 显示创建对话框
 */
function showCreateAuditorDialog() {
	$("#createAuditorDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});
	$('#addForm')[0].reset();
	$("#cType").val("");
	// $form.render();
	updateLayuiForm();
}

/**
 * 确认创建
 */
function confirmCreateAuditor() {
	// 获取参数
	var data = {};
	var type = $.trim($('#cType').val());
	if (Utils.isNull(type)) {
		showAlert("请选择审核方类型!");
		return false;
	}
	data['type'] = type;
	var name = $.trim($('#cName').val());
	if (Utils.isNull(name)) {
		showAlert('审核方名称不能为空!');
		return false;
	}
	data['name'] = name;
	var contacts = $.trim($('#cContacts').val());
	if (Utils.isNull(contacts)) {
		showAlert('联系人不能为空!');
		return false;
	}
	data['contacts'] = contacts;
	var telephone = $.trim($('#cTelephone').val());
	if (Utils.isNull(telephone)) {
		showAlert("手机号不能为空!");
		return false;
	}else if (telephone.length != 11) {
		showAlert("手机号格式不正确，请检查!");
		return false;
	}
	data['telephone'] = telephone;
		
	// 调用接口
	var response = callInterface("post", "auditor/createAuditor", data, false);
	if (response.code == 0) {
		showMsg('操作成功!');

		// 关闭对话框
		$('#createAuditorDialog').modal("hide");

		// 刷新
		queryAuditor();
	}
	else {
		showErrorMsg(response.code, response.message);
	}

	return false;
}

/**
 * 显示修改对话框
 */
function showModifyAuditorDialog(vo) {
	// 显示对话框
	$("#modifyAuditorDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});
	$('#modifyForm')[0].reset();
	$("#mId").val(vo.id);
	$("#mName").val(vo.name);
	$("#mContacts").val(vo.contacts);
	$("#mTypeSpan").html(vo.typeName);
	$("#mTelephoneSpan").html(vo.telephone);
	// updateLayuiForm();
}

/**
 * 确认修改
 */
function confirmModifyAuditor() {
	// 获取参数
	var data = {};
	data['id'] = $.trim($("#mId").val());
	var name = $.trim($('#mName').val());
	if (Utils.isNull(name)) {
		showAlert('审核方名称不能为空!');
		return false;
	}
	data['name'] = name;
//	var telephone = $.trim($('#mTelephone').val());
//	if (Utils.isNull(telephone)) {
//		showAlert("手机号不能为空!");
//		return false;
//	}else if (telephone.length != 11) {
//		showAlert("手机号格式不正确，请检查!");
//		return false;
//	}
//	data['telephone'] = telephone;
	var contacts = $.trim($('#mContacts').val());
	if (Utils.isNull(contacts)) {
		showAlert('联系人不能为空!');
		return false;
	}
	data['contacts'] = contacts;
	
	// 调用接口
	var response = callInterface("post", "auditor/modifyAuditor", data, false);
	if (response.code == 0) {
		showMsg('操作成功!');

		// 关闭对话框
		$('#modifyAuditorDialog').modal("hide");

		// 刷新
		queryAuditor(currPage);
	}
	else {
		showErrorMsg(response.code, response.message);
	}
}

/**
 * 设置启用禁用
 */
function modifyAuditorStatus(id) {
	var data = {};
	var status = 0;
	if($('#status_'+id).is(":checked")){
		status = 1;
	}
	data.id = id;
	data.status = status;
	
	// 调用接口
	var response = callInterface("post", "auditor/modifyAuditorStatus", data, false);
	if (response.code == 0) {
		showMsg('操作成功!');
	} else {
		showErrorMsg(response.code, response.message);
	}
	return false;
}

function showResetPasswordDialog($telephone){
	// 显示对话框
	$("#resetPasswordDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});

	// 重置表单
	$('#resetPasswordForm')[0].reset();
	$("#rUsername").html($telephone);
}

/**
 * 删除
 */
function deleteAuditor($id, $name) {
	// 设置参数
	var $data = {
		"id" : $id
	};
	// 询问框
	layer.confirm('确定删除[' + $name + ']吗？', {
		offset :'20px',btn: ['确定','取消']
	// 按钮
	}, function() {
		// 调用接口
		var response = callInterface("post", "auditor/deleteAuditor", $data, false);
		if (response.code == 0) {
			showMsg('操作成功!');

			// 刷新
			queryAuditor();
		}
		else {
			showErrorMsg(response.code, response.message);
		}
	}, function() {
		return;
	});
}

/**
 * 重置密码
 */
function resetPassword() {
	var resetPassword = $.trim($('#resetPassword').val());
	if (Utils.isNull(resetPassword)) {
		showAlert('新密码不能为空!');
		return false;
	}
	var resetChkpwd = $.trim($('#resetChkpwd').val());
	if (Utils.isNull(resetChkpwd)) {
		showAlert('确认新密码不能为空!');
		return;
	}
	if (resetPassword != resetChkpwd) {
		showAlert('两次输入密码不一致!');
		return;
	}
	
	// 设置参数
	var $data = {
		"username" : $.trim($("#rUsername").html()),
		"newPassword" : $.md5(resetPassword)
	}

	// 调用接口
	var response = callInterface("post", "auditor/resetAuditorPassword", $data, false);
	if (response.code == 0) {
		showMsg('操作成功!');
		$('#resetPasswordDialog').modal("hide");
	}
	else {
		showErrorMsg(response.code, response.message);
	}
}

