/**
 * 准备函数
 */
$(document).ready(function() {
	
	// 是否播放竞价广告:否
	$("#isGetDsp_0").attr("checked",true);
	$('#timeZone').val(8);
	updateLayuiForm();
	
	// 获取数据
	initPlayConfig();
	
	// 绑定函数
	$("#saveButton_playConfig").bind("click", setPlayerGlobalOption);
	$("#resetRedisData").bind("click", resetRedisData);
});

/**
 * 初始化设备配置
 */
function initPlayConfig(){
	// 调用接口
	var response = callInterface("post", "playManagement/getPlayerGlobalOption", {}, false);
	if (response.code == 0) {
		var data = response.data;
		$("#taskPeriod").val(data.taskPeriod);
		$("#heartPeriod").val(data.heartPeriod);
		$("#reporPeriod").val(data.reporPeriod);
		$("#timeZone").val(data.timeZone);
		
		// 是否播放竞价广告
		if(Utils.isNotNull(data.isGetDsp)){
			$("input[name=isGetDsp]").prop('checked',false);
			if(data.isGetDsp){
				$("#isGetDsp_1").prop('checked',true);
			}else{
				$("#isGetDsp_0").prop('checked',true);
			}
		}
		if (Utils.isNotNull(data.serverAddress)) {
			$("#serverAddress").val(data.serverAddress);
		}
		updateLayuiForm();
	}
}

/**
 * 保存系统全局配置
 */
function setPlayerGlobalOption(){
	
	var data = {};
	var taskPeriod = $('#taskPeriod').val();
	if( Utils.isNotNull(taskPeriod) ){
		if(isNaN(taskPeriod) ){
			showAlert('获取任务周期必须是数字类型!');
			return false;
		}
	}else{
		showAlert('获取任务周期不能为空!');
		return false;
	}
	data['taskPeriod'] = taskPeriod;
	
	var heartPeriod = $('#heartPeriod').val();
	if( Utils.isNotNull(heartPeriod) ){
		if( isNaN(heartPeriod) ){
			showAlert('发送心跳周期必须是数字类型!');
			return false;
		}
	}else{
		showAlert('发送心跳周期不能为空!');
		return false;
	}
	data['heartPeriod'] = heartPeriod;
	
	var reporPeriod = $('#reporPeriod').val();
	if( Utils.isNotNull(reporPeriod) ){
		if( isNaN(reporPeriod) ){
			showAlert('系统参数上报周期必须是数字类型!');
			return false;
		}
	}else{
		showAlert('系统参数上报周期不能为空!');
		return false;
	}
	data['reporPeriod'] = reporPeriod;
	
	var isGetDsp = $("input[name=isGetDsp]:checked").val();
	data['isGetDsp'] = isGetDsp;
	
	var timeZone = $('#timeZone').val();	
	if(Utils.isNull(timeZone)){
		showAlert('设备所在时区不能为空!');
		return false;
	}
	data['timeZone'] = timeZone;
	
	var serverAddress = $('#serverAddress').val();
	data['serverAddress'] = serverAddress;
	
	// 调用接口
	var response = callInterface("post", "playManagement/setPlayerGlobalOption", data, false);
	if (response.code == 0) {
		showAlert('操作成功!');
	}else{
		showAlert('操作失败!');
	}
}

/**
 * 重置REDIS缓存
 */
function resetRedisData(){
	var $element = $("input[name='redisItem']:checked");
	var info = $element.val();
	if( Utils.isNull(info) || Utils.isUndefined(info)){
		showAlert('请先选择要重置的数据!');
		return false;
	}else{
		var url = $element.attr('itemUrl');
		
		// 询问框
		layer.confirm('确定重置' + info + '缓存吗？', {
			offset :'20px',btn: ['确定','取消']
		// 按钮
		}, function() {
			// 调用接口
			var response = callInterface("post", url, {}, false);
			if (response.code == 0) {
				showAlert('操作成功!');
			}
			else {
				showAlert('操作失败!');
			}
		}, function() {
			return;
		});
	}
}