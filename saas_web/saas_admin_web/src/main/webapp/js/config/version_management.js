// 角色对应功能权限
var rolerightdata = {};
// 所有功能权限树数组
var menuRightArray;

// 设置树属性
var setting = {
	check : {
		enable : true
	},
	view : {
		showIcon : false
	},
	data : {
		simpleData : {
			enable : true
		}
	}
};

/**
 * 准备函数
 */
var operateRight = new Object();
$(document).ready(function() {
	// 加载权限
	loadRight();

	// 加载表格
	queryVersion();

	// 绑定函数
	$("#queryButton").bind("click", confirmQueryVersion);
	$("#createButton").bind("click", showCreateVersionDialog);
	$("#modifyButton").bind("click", showModifyVersionDialog);
	$("#confirmCreateButton").bind("click", confirmCreateVersion);
	$("#confirmModifyButton").bind("click", confirmModifyVersion);
	$("#confirmSetVersionRightButton").bind("click", confirmSetVersionRight);
});

/**
 * 加载权限
 */
function loadRight() {
	operateRight.modifyRight = $("#modify").val();
	operateRight.setVersionRight = $("#setVersionRight").val();
	operateRight.deleteRight = $("#delete").val();
}

/**
 * 查询角色
 */
function queryVersion() {
	// 组装参数
	var data = {};
	var name = $.trim($("#name").val());
	if (name != "") {
		data['name'] = name;
	}

	// 调用接口
	var response = callInterface("post", "saas/version/queryVersion", data, false);
	if (response.code == 0) {
		// 清空表格及分页
		$("#versionTable tr:not(:first)").remove();
		$("#pageDiv").html("");
		var dataTable = $("#versionTable");

		// 得到总页数
		var list = response.data;
		if (list != null && list.length > 0) {
			for ( var i in list) {
				var trHtml = "<tr>";
				trHtml += "<td align='center'><a href='javascript:showVersionDetail(" + list[i].id + ");'>" + list[i].name + "</a></td>";
				trHtml += "<td align='center'>" + handleEmptyStr(list[i].userLimit) + "</td>";
				trHtml += "<td align='center'>" + Utils.toFormatDecimal(getFileSizeMByB(list[i].materialLimit),0) + "MB</td>";
				trHtml += "<td align='center'>" + Utils.toFormatDecimal(getFileSizeMByB(list[i].singleFileLimit),0) + "MB</td>";
				trHtml += "<td>" + handleEmptyStr(list[i].description) + "</td>";
				trHtml += "<td align='center'>" + handleEmptyStr(list[i].createdTime) + "</td>";
				// 操作
				trHtml += "<td align='center' width='150px'>";
				if (operateRight.modifyRight == 1) {
					trHtml += "<a href='javascript:void(0);' class='edit' onclick='showModifyVersionDialog("
							+ JSON.stringify(list[i]) + ");'>编辑</a>&nbsp;&nbsp;";
				}
				if (operateRight.deleteRight == 1) {
					trHtml += "<a href='javascript:void(0);' class='delete' onclick='deleteVersion(" + list[i].id
							+ ",\"" + list[i].name + "\");'>删除</a>&nbsp;&nbsp;";
				}
				if (operateRight.setVersionRight == 1) {
					trHtml += "<a href='javascript:void(0);' class='copy' onclick='showVersionRightDialog(" + list[i].id
							+ ",\"" + list[i].name + "\");'>设置权限</a>";
				}
				trHtml += "</td>";
				trHtml += "</tr>";

				// 动态添加一行
				dataTable.append(trHtml);
			}
		}else{
			var cols = dataTable.find("tr:first th");
			dataTable.append("<tr><td colspan='" + cols.length + "'>无数据!</td></tr>");
		}
	}else {
		showErrorMsg(response.code, response.message);
	}
}

/**
 * 确认查询
 */
function confirmQueryVersion() {
	queryVersion();
}

/**
 * 显示创建对话框
 */
function showCreateVersionDialog() {
	$("#createVersionDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});
	$('#addVersionForm')[0].reset();
}

/**
 * 确认创建
 */
function confirmCreateVersion() {
	// 获取参数
	var data = {};
	var name = $.trim($('#cName').val());
	if (name == "") {
		showAlert('版本名称不能为空!');
		return false;
	}
	data['name'] = name;
	
	var userLimit = $.trim($('#cUserLimit').val());
	if (userLimit == "") {
		showAlert('用户数上限不能为空!');
		return false;
	}else{
		if (userLimit <= 0) {
			showAlert("用户数上限必须大于0个!");
			return false;
		}
	}
	data['userLimit'] = userLimit;
	
	var materialLimit = $.trim($('#cMaterialLimit').val());
	if (materialLimit == "") {
		showAlert('素材空间上限不能为空!');
		return false;
	}else{
		if (materialLimit <= 0) {
			showAlert("素材空间上限必须大于0MB!");
			return false;
		}
	}
	data['materialLimit'] = materialLimit;
	
	var singleFileLimit = $.trim($('#cSingleFileLimit').val());
	if (singleFileLimit == "") {
		showAlert('单个文件上限不能为空!');
		return false;
	}else{
		if (singleFileLimit <= 0) {
			showAlert("单个文件上限必须大于0MB!");
			return false;
		}
	}
	data['singleFileLimit'] = singleFileLimit;

	var description = $.trim($('#cDescription').val());
	if (description != "") {
		data['description'] = description;
	}

	// 调用接口
	var response = callInterface("post", "saas/version/createVersion", data, false);
	if (response.code == 0) {
		showMsg('操作成功!');
		$('#createVersionDialog').modal("hide");

		// 刷新
		queryVersion();
	}
	else {
		showErrorMsg(response.code, response.message);
	}
	return false;
}

/**
 * 显示修改角色对话框
 */
function showModifyVersionDialog(data) {
	// 显示对话框
	$("#modifyVersionDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});
	$('#modifyVersionForm')[0].reset();
	
	// 设置参数
	$("#mId").val(data.id);
	$("#mName").val(data.name);
	$("#mDescription").val(data.description);
	$("#mMaterialLimit").val(Utils.toFormatDecimal(getFileSizeMByB(data.materialLimit),0));
	$("#mSingleFileLimit").val(Utils.toFormatDecimal(getFileSizeMByB(data.singleFileLimit),0));
	$("#mUserLimit").val(Utils.convertNull(data.userLimit));
	updateLayuiForm();
}

/**
 * 确认修改角色
 */
function confirmModifyVersion() {
	// 获取参数
	var data = {};
	data['id'] = $.trim($("#mId").val());
	var name = $.trim($('#mName').val());
	if (name == "") {
		showAlert('角色名称不能为空!');
		return false;
	}
	data['name'] = name;
	
	var userLimit = $.trim($('#mUserLimit').val());
	if (userLimit == "") {
		showAlert('用户数上限不能为空!');
		return false;
	}else{
		if (userLimit <= 0) {
			showAlert("用户数上限必须大于0个!");
			return false;
		}
	}
	data['userLimit'] = userLimit;
	
	var materialLimit = $.trim($('#mMaterialLimit').val());
	if (materialLimit == "") {
		showAlert('素材空间上限不能为空!');
		return false;
	}else{
		if (materialLimit <= 0) {
			showAlert("素材空间上限必须大于0MB!");
			return false;
		}
	}
	data['materialLimit'] = materialLimit;
	
	var singleFileLimit = $.trim($('#mSingleFileLimit').val());
	if (singleFileLimit == "") {
		showAlert('单个文件上限不能为空!');
		return false;
	}else{
		if (singleFileLimit <= 0) {
			showAlert("单个文件上限必须大于0MB!");
			return false;
		}
	}
	data['singleFileLimit'] = singleFileLimit;
	
	var description = $.trim($('#mDescription').val());
	data['description'] = description;

	// 调用接口
	var response = callInterface("post", "saas/version/modifyVersion", data, false);
	if (response.code == 0) {
		showMsg('操作成功!');

		// 关闭
		$('#modifyVersionDialog').modal("hide");

		// 刷新
		queryVersion();
	}
	else {
		showErrorMsg(response.code, response.message);
	}
	return false;
}

/**
 * 显示设置角色功能权限对话框
 */
function showVersionRightDialog(versionId, versionName) {
	$("#versionInfo").html("(" + versionName + ")");
	$("#versionId").val(versionId);
	$("#versionName").val(versionName);

	// 查询角色对应功能权限
	queryVersionRight(versionId);

	// 初始化树
	$.fn.zTree.init($("#versionRightTree"), setting, menuRightArray);

	// 显示对话框
	$("#setVersionRightDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});
}

/**
 * 显示删除对话框
 */
function deleteVersion($id, $name) {
	var $data = {
		"id" : $id
	};

	// 询问框
	layer.confirm('确定删除[' + $name + ']吗？', {
		offset :'20px',btn: ['确定','取消']
	// 按钮
	}, function() {
		// 调用接口
		var response = callInterface("post", "saas/version/deleteVersion", $data, false);
		if (response.code == 0) {
			showMsg('操作成功!');

			// 刷新
			queryVersion();
		}
		else {
			showErrorMsg(response.code, response.message);
		}
	}, function() {
		return;
	});
}

/**
 * 确认设置角色权限
 */
function confirmSetVersionRight() {
	var versionId = $("#versionId").val();
	var rightIdList = new Array();
	var zTree = $.fn.zTree.getZTreeObj("versionRightTree");
	var nodes = zTree.getCheckedNodes(true);
	for (var i = 0; i < nodes.length; i++) {
		// 获取叶子节点
		//if (!nodes[i].isParent && nodes[i].checked) {
			rightIdList.push(parseInt(nodes[i].id));
		//}
	}

	// 设置参数
	var $data = {
		'versionId' : versionId,
		'menuIdList' : JSON.stringify(rightIdList)
	};
	// 调用接口
	var response = callInterface("post", "saas/version/setVersionMenuList", $data, false);
	if (response.code == 0) {
		showMsg('操作成功!');

		$('#setVersionRightDialog').modal("hide");
	}
	else {
		showErrorMsg(response.code, response.message);
	}
}

/**
 * 查询角色对应功能权限
 */
function queryVersionRight(versionId) {
	// 设置参数
	var $data = {
		'versionId' : versionId
	};
	// 调用接口
	var response = callInterface("post", "saas/menu/queryMenuByVersionId", $data, false);
	if (response.code == 0) {
		rolerightdata = {}
		var rightList = response.data;
		if (rightList.length > 0) {
			for ( var i in rightList) {
				rolerightdata[rightList[i].id] = true;
			}
		}
		// 查询所有菜单功能权限树
		queryMenuRightTree();

	}
	else {
		showErrorMsg(response.code, response.message);
	}
}

/**
 * 查询所有菜单功能权限树
 */
function queryMenuRightTree() {
	// 初始化功能权限数组
	menuRightArray = new Array();

	// 调用接口
	var response = callInterface("post", "saas/menu/queryAllMenu", null, false);
	if (response.code == 0) {
		// 功能权限对象
		var list = response.data;
		if (list != null && list.length > 0) {
			getMenuRightArray(list);
		}
	}
	else {
		showErrorMsg(response.code, response.message);
	}
}

/**
 * 父功能模块树
 */
function getMenuRightArray(list) {
	for ( var i in list) {
		var childlist = list[i].childList;
		if (childlist != null && childlist.length > 0) {
			getMenuRightChildArray(list[i].id, childlist);
		}
	}
}

/**
 * 子功能模块树
 */
function getMenuRightChildArray(pId, list) {
	for ( var i in list) {
		var menu = new Object();
		if (list[i].parentId != null) {
			pId = list[i].parentId;
		}
		menu.pId = pId;
		menu.id = list[i].id;
		var desc = list[i].description;
		if (typeof (desc) != "undefined" && desc != null && desc != "") {
			menu.name = list[i].name + "(" + desc + ")";
		}
		else {
			menu.name = list[i].name;
		}
		menu.open = false;
		if (rolerightdata[list[i].id]) {
			menu.checked = true;
		}
		var childlist = list[i].childList;
		if (childlist != null && childlist.length > 0) {
			getMenuRightChildArray(pId, childlist);
		}
		else {
			var right = list[i].rightList;
			if (right != null && right.length > 0) {
				getMenuRightChildArray(list[i].id, right);
			}
		}
		menuRightArray.push(menu);
	}
}

function showVersionDetail(id){
	$("#versionDetailDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});

	// 调用接口
	var response = callInterface("post", "saas/version/getVersionById", {
		'id' : id
	}, false);
	if (response.code == 0) {
		var data = response.data;
		if (data != null) {
			$("#vName").html(data.name);
			$("#vCreatedTime").html(data.createdTime);
			$("#vMaterialLimit").html(Utils.toFormatDecimal(getFileSizeMByB(data.materialLimit),0)+"MB");
			$("#vSingleFileLimit").html(Utils.toFormatDecimal(getFileSizeMByB(data.singleFileLimit),0)+"MB");
			$("#vUserLimit").html(Utils.convertNull(data.userLimit));
			$("#vDescription").html(Utils.convertNull(data.description));
		}
	}
	else {
		showErrorMsg(response.code, response.message);
	}
}
