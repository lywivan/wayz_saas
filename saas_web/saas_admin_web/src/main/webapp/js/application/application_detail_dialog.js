/**
 * 素材详情
 */
function showApplicationDetailDialog(id,applicationName){
	
	// 组装参数
	var data = {
		'id' : id	
	};
	$("#applicationName").text(applicationName);
	// 显示对话框
	$("#applicationDetailDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});
	
	// 调用接口
	var response = callInterface("post", "saas/application/getApplication",
			data, false);
	if (response.code == 0) {
		
		var data = response.data;
		$("#vName").text(data.name);
		$("#vIcon").text(data.icon);
		$("#vHref").text(data.href);
		var starHtml = "";
		for(var k=0;k<data.starNum;k++){
			starHtml += '<i class="icon-star"></i>'
		}
		$("#vStarNum").html(starHtml);
		$("#vBuyCount").text(data.buyCount);
		$("#vIsEnable").text(data.isEnable?'启用':'停用');
		$("#vDescription").text(data.description);
		
		// 设置预览素材信息
		setFilePreviewHtml(FileType.IMAGE_V, data.name, data.images);
	} else {
		showErrorMsg(response.code, response.message);
	}
}