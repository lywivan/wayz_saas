/**
 * 准备函数
 */
$(document).ready(function() {

    // 加载数据列表
    queryPosition();

    // 查询事件
    $("#queryButton").bind("click", function() {
        queryPosition(null, null);
    });

});

/**
 * 选中余额支付
 * 
 * @param amount
 */
function selectBalancePay(amount) {
    if (amount == 0 || amount == 0.00) {
        $(".balanceLable").siblings().css({
            "border-color": ""
        });
        $(".balanceLable").css({
            "border-color": "#3d9bd2"
        });
        $(".balanceLable").siblings().removeClass("on");
        $(".balanceLable").addClass("on");

        // 初始化代金券数据
        clearCouponData("amount", "positionSfjeSpan", true);
        $("#positionCouponTr").hide();
    }
}

/**
 * 查询
 * @param pageIndex 起始页
 * @param pageSize  每页条数
 */
function queryPosition(pageIndex, pageSize) {
    // 组装参数
    var data = {};

    // 设置筛选参数
    setParamData(data);

    //设置分页参数
    setPageIndex(data, pageIndex, pageSize);

    // 调用接口
    var response = callInterface("post", "saas/media/queryPosition", data, false);
    if (response.code == 0) {
        // 清空表格及分页
        $("#dataTable tr:not(:first)").remove();
        $("#pageDiv").html("");

        var count = response.data.totalCount;
        if (count == null || count == 0) {
            var cols = $("#dataTable").find("tr:first th");
            $('#dataTable').append("<tr><td colspan='" + cols.length + "'>无数据!</td></tr>");
            return false;
        }

        var list = response.data.list;
        if (list != null && list.length > 0) {
            for (var i in list) {
                var trHtml = "<tr>";
                trHtml += "<td align='center'>" + list[i].id + "</td>";
                trHtml += "<td align='center'><a class='edit' href='javascript:toPositionView(" + list[i].id + ");'>" + list[i].name + "</a></td>";
                trHtml += "<td align='center'>" + list[i].companyName + "</a>";
                trHtml += '<td align="center">' + list[i].groupName + '</td>';
                trHtml += "<td align='center'>" + Utils.convertNull(list[i].resolutionName) + "</td>";
                trHtml += "<td align='center'>" + Utils.convertNull(list[i].playCode) + "</td>";
                trHtml += "<td align='center'>" + Utils.convertNull(list[i].packageVersion) + "</td>";
                trHtml += "<td align='center'>" + Utils.convertNull(list[i].osDesc) + "</td>";
                trHtml += "<td align='center'>" + Utils.convertNull(list[i].isOnline) + "</td>";
                trHtml += "<td align='center'>" + (list[i].isEnable ? "启用" : "禁用") + "</td>";
                // trHtml += "<td align='center'>" + list[i].statusName + "</td>";
                trHtml += "<td align='center'>" + PositionStatus[list[i].status] + "</td>";
                trHtml += "</tr>";

                // 动态添加一行
                $('#dataTable').append(trHtml);
            }
        }

        // 调用分页
        layui.use('laypage', function() {
            layui.laypage.render({
                elem: 'pageDiv',
                // pages : pages,
                curr: pageIndex || 1, // 当前页
                count: count,
                limit: pageSize,
                skip: true, // 是否开启跳页
                layout: ['count', 'prev', 'page', 'next', 'limit', 'skip'],
                groups: 5, // 连续显示分页数
                jump: function(obj, first) { // 触发分页后的回调
                    if (!first) {
                        queryPosition(obj.curr, obj.limit);
                    }
                }
            });
        });
    }
}


/**
 * 设置筛选参数
 * 
 * @param data
 */
function setParamData(data) {
	var companyId = $.trim($("#companyId").val());
    if (Utils.isNotNull(companyId)) {
        data['companyId'] = companyId;
    }
    var id = $("#id").val();
    if (Utils.isNotNull(id)) {
        data['id'] = id;
    }
    var name = $.trim($("#name").val());
    if (Utils.isNotNull(name)) {
        data['name'] = name;
    }
    var code = $.trim($("#code").val());
    if (Utils.isNotNull(code)) {
        data['playCode'] = code;
    }
    var isBound = $("#isBound").val();
    if (Utils.isNotNull(isBound)) {
        data['isBound'] = isBound;
    }
    var isEnable = $("#isEnable").val();
    if (Utils.isNotNull(isEnable)) {
        data['isEnable'] = isEnable;
    }
    var isOnline = $("#isOnline").val();
    if (Utils.isNotNull(isOnline)) {
        data['isOnline'] = isOnline;
    }
    var status = $("#status").val();
    if (Utils.isNotNull(status)) {
        data['status'] = status;
    }
}