/**
 * 设备详情
 * @param id
 */
var PositionStatus = ["未开通", "正常", "已过期"];
function toPositionView(id){
	// 清空表格
	$("#positionPlanTable tr:not(:first)").remove();
	
	// 显示对话框
	$("#positionViewDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});
	$("#vPositionId").val(id);
	
	// 调用接口
	var response = callInterface("post", "saas/media/getPositionDetail", {'id' : id }, false);
	if (response.code == 0) {
		var data = response.data;
		$("#vPName").html(data.name);
		$("#vPCompanyName").html(data.companyName);
		$("#vPStatus").html(PositionStatus[data.status]);
		$("#vPAuditStatus").html(data.statusName);
		$("#vPArea").html(data.provinceName + '-' + data.cityName + '-' + data.districtName);
		$("#vPAddress").html(Utils.isNull(data.address) ? "无" : data.address);
		$("#vPSceneName").html(Utils.isNull(data.sceneName) ? "无" : data.sceneName);
		$("#vPPlayCode").html(Utils.isNull(data.playCode) ? "无" : data.playCode);
		$("#vPPackageVersion").html(Utils.isNull(data.packageVersion) ? "无" : data.packageVersion);
		$("#vPOSDesc").html(Utils.isNull(data.osDesc) ? "无" : data.osDesc);
		$("#vPIsEnable").html(data.isEnable ? "启用" : "禁用");
		$("#vPIsBound").html(data.isBound ? "已绑定" : "未绑定");
		$("#vPIsOnLine").html(data.isOnline ? "在线" : "离线");
		$("#vGroupName").html(Utils.convertNull(data.groupName));
		$("#vResolutionName").html(Utils.convertNull(data.resolutionName));
		$("#vPStartTime").html(Utils.convertNull(data.startTime));
		$("#vPCloseTime").html(Utils.convertNull(data.closeTime));
		$("#vPCreatedTime").html(Utils.convertNull(data.createdTime));
		
		// 广告列表
		var planList = data.planList;
		if (Utils.isNotNull(planList) && planList.length > 0) {
			for ( var i in planList) {
				var trHtml = "<tr>";
				trHtml += "<td><a class='edit' href='javascript:showPlanDetail(" + planList[i].planId + ");'>" + planList[i].planName + "</a></td>";
				trHtml += "<td align='center'>" + planList[i].adType + "</td>";
				trHtml += "<td align='center'>" + planList[i].beginDate + "至" + planList[i].endDate + "</td>";
				trHtml += "<td align='center'>" + (planList[i].adType == PlanType.AD_V ? planList[i].playTime:'--') + "</td>";
				if (planList[i].adType != PlanType.AD_V) {
					trHtml += "<td align='center'>--</td>";
					trHtml += "<td align='center'>--</td>";
				}else{
					trHtml += "<td align='center'>" + handleEmptyStr(planList[i].dayPlayTimes) + "</td>";
					// 广告最小间隔
					if(list[i].timeInterval == -1){
						trHtml += "<td align='center'>均匀播放</td>";
					}else{
						trHtml += "<td align='center'>"+ planList[i].timeInterval +"</td>";
					}
				}
				trHtml += "<td align='center'>"+ planList[i].putStatus +"</td>";
				$('#positionPlanTable').append(trHtml);
			}
		}else{
			var cols = $("#positionPlanTable").find("tr:first th");
			$('#positionPlanTable').append("<tr><td colspan='" + cols.length + "'>无数据!</td></tr>");
		}
	} else {
		showErrorMsg(response.code, response.message);
	}
}