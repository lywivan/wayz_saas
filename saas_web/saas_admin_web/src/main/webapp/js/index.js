/**
 * 准备函数
 */
$(document).ready(function() {
	
	// 首页第一次加载，获取窗口尺寸
	getDimensions();

	// 加载菜单信息
	loadMenu();

	// 绑定函数
	$("#confirmModifyPwdButton").bind("click", confirmModifyPwd);

	// 头部菜單
	$(".header-nav li").on('click', function() {
		var $aDom = $(this).find('a');
		var headerId = $aDom.attr('headerId');
		$(this).siblings().find('a').removeClass('on');
		if (!$aDom.hasClass('on')) {
			$aDom.addClass('on');
		}
		
		if (Utils.isNotUndefined(headerId) && Utils.isNotNull(headerId)) {
			$(".menuItem").hide();
			$('#menuItem_' + headerId).show();
		}
	});
});

/**
 * 加载菜单
 */
function loadMenu() {
	// 调用接口
	var response = callInterface("post", "menu/queryMyMenu", null, false);
	if (response.code == 0) {
		var list = response.data;
		if (list != null && list.length > 0) {
			
			showLeftMenu("导航菜单", list[0].childList);
			// 导航菜单
			//injectMenu(list);
		}
	}
	else {
		showErrorMsg(response.code, response.message);
	}
}

/**
 * 导航菜单
 */
function injectMenu(list) {
	var topHtml = "";
	var topList = list[0].childList;
	if (topList != null && topList.length > 0) {
		for ( var i in topList) {
			if (topList[i].parentId == 0) {
				// 头部菜单
				topHtml += "<li headerId='"+ topList[i].id + "'><a href='javaScript:void(0);' onclick='showLeftMenu(\"" + topList[i].name + "\","
						+ JSON.stringify(topList[i].childList) + ");' class='"+ (i == 0 ? 'on' : '') +"' headerId='"
						+ topList[i].id + "'>" + topList[i].name + "</a></li>";
			}
		}
		// 左侧菜单
		showLeftMenu(topList[0].name, topList[0].childList);
		$('.header-nav').html(topHtml);
	}
}

/**
 * 左侧菜单
 */
function showLeftMenu(name, firstList) {
	var html = "";
	if (firstList != null && firstList.length > 0) {
		for ( var j in firstList) {
			var secondList = firstList[j].childList;
			var ishasSecond = (secondList != null && secondList.length);
			var menu_icon = firstList[j].icon;
			if (!ishasSecond) {
				html += "<li id=\"" + firstList[j].href + "\" class=" + (j == 0 ? 'open' : '')
						+ "><a href='javascript:loadHtml(\"" + firstList[j].href + "\",\"" + name + "\",\""
						+ firstList[j].name + "\");'><i class='" + menu_icon + "'></i><span class='menu-text'>"
						+ firstList[j].name + "</span></a>";
				// 默认显示页
				if (j == 0) {
					loadHtml(firstList[j].href, name, firstList[j].name);
				}
			}
			else {
				html += "<li class=" + (j == 0 ? 'open' : '') + "><a href='#' class='dropdown-toggle'><i class='"
						+ menu_icon + "'></i><b class='arrow icon-angle-down'></b><span class='menu-text'>"
						+ firstList[j].name + "</span></a>";
				html += "<ul class='submenu' style='display:" + (j == 0 ? 'block' : 'none') + "'>";
				for ( var k in secondList) {
					html += "<li id=\"" + secondList[k].href + "\" class=" + (k == 0 ? 'active' : '')
							+ "><a href='javascript:loadHtml(\"" + secondList[k].href + "\",\"" + name + "\",\""
							+ firstList[j].name + "\",\"" + secondList[k].name
							+ "\");'> <i class='icon-double-angle-right'></i>" + secondList[k].name + "</a>";
					// 默认显示页
					if (j == 0 && k == 0) {
						loadHtml(secondList[k].href, name, firstList[j].name, secondList[k].name);
					}
					html += "</li>";
				}
				html += "</ul>";
			}
			html += "</li>";
		}
	}
	$("#leftMenu").html(html);
	$(".nav-list li").on("click", function() {
		$(".nav-list li").removeClass("open");
		$(this).addClass("open").siblings("li").removeClass("open");
		$(this).siblings("li").find(".submenu").hide();
	});
	$(".submenu li").on("click", function() {
		$(this).addClass("active").siblings("li").removeClass("active");
	});
}

/**
 * 获取未读消息数量
 */
function getUnreadMessageCount() {
	// 调用接口
	var response = callInterface("post", "general/countUnreadMessage", null, false);
	if (response.code == 0) {
		var count = 0;
		if (Utils.isNotNull(response.data)) {
			count = response.data;
		}
		$('#msgInfo').html(
				"<a href='javascript:void(0);' onclick='showMsgPage();'><font color=#FFFFFF id='noReadMsg'>" + count
						+ "</font></a>");
	}
	else {
		showErrorMsg(response.code, response.message);
	}
}

/**
 * 显示修改密码对话框
 */
function showModifyPwdDialog() {
	$("#modifyPwdDialog").modal({
		backdrop : 'static',
		keyboard : false,
		show : true
	});
	$('#modifyPwdForm')[0].reset();
}

/**
 * 确认修改
 */
function confirmModifyPwd() {
	// 获取参数
	var modifydata = {};
	var $oldPassword = $.trim($('#oldPassword').val());
	if ($oldPassword == '') {
		showAlert('旧密码不能为空!');
		return false;
	}
	var $newPassword = $.trim($('#newPassword').val());
	if ($newPassword == '') {
		showAlert('新密码不能为空!');
		return false;
	}
	var $chkpwd = $.trim($('#chkNewPwd').val());
	if ($chkpwd == '') {
		showAlert('确认密码不能为空!');
		return false;
	}
	if ($chkpwd != $newPassword) {
		showAlert('两次输入密码不一致!');
		return false;
	}
	modifydata['oldPassword'] = $.md5($oldPassword);
	modifydata['newPassword'] = $.md5($newPassword);

	// 调用接口
	var response = callInterface("post", "user/modifyPassword", modifydata, false);
	if (response.code == 0) {
		$('#modifyPwdDialog').modal("hide");

		// 询问框
		layer.confirm('修改密码成功,请重新登录？', {
			offset :'20px',btn: ['确定','取消']
		// 按钮
		}, function() {
			Utils.toRedirect('login');
		}, function() {
			return;
		});
	}
	else {
		showErrorMsg(response.code, response.message);
	}
	return false;
}

/**
 * 用户退出
 */
function logout() {
	// 询问框
	var index = layer.confirm('确定退出系统吗？', {
		offset :'20px',btn: ['确定','取消']
	// 按钮
	}, function() {
		// 清除
		layer.close(index);

		// 调用接口
		$.ajax({
			type : "post",
			url : "logout",
			data : null,
			dataType : 'json',
			complete : function() {
				window.location.replace("login");
			}
		});
	}, function() {
		return;
	});
}