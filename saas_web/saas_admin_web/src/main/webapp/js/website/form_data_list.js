/**
 * 准备函数
 */

$(document).ready(function() {

    // 加载表格
    querFrom();

    // 绑定函数
    $("#queryFromButton").bind("click", confirmQueryFrom);

});

/**
 * 查询
 */
function querFrom(pageIndex, pageSize) {
    // 组装参数
    var data = {};
    var phone = $.trim($("#phone").val());
    if (Utils.isNotNull(phone)) {
		data['phone'] = phone;
	}
    var type = $('#type').val();
    if (Utils.isNotNull(type)) {
        data['type'] = type;
    }

    // 设置分页参数
    setPageIndex(data, pageIndex, pageSize);

    // 调用接口
    var response = callInterface("post", "home/queryFormData", data, false);
    if (response.code == 0) {
        var count = 0;
        if( Utils.isNotNull(response.data) && Utils.isNotNull(response.data.totalCount) ){
            count = response.data.totalCount;
        }

        // 清空表格及分页
        $("#dataTable tr:not(:first)").remove();
        $("#pageDiv").html("");

        // 动态添加一行
        if (count == 0) {
            var cols = $("#dataTable").find("tr:first th");
            $('#dataTable').append("<tr><td colspan='" + cols.length + "'>无数据!</td></tr>");
            return false;
        }

        var list = response.data.list;
        if (list != null && list.length > 0) {
            var num =0;
            for ( var i in list) {
                var trHtml = "<tr>";
                trHtml += "<td align='center'>" + list[i].contact + "</td>";
                trHtml += "<td>" + list[i].telephone + "</td>";
                trHtml += "<td>" + list[i].companyName + "</td>";
                trHtml += "<td align='center'>" + list[i].provinceName + "-" +list[i].cityName + "</td>";
                trHtml += "<td align='center'>" + list[i].typeName+ "</td>";
                trHtml += "<td align='center'>" + list[i].createdTime + "</td>";
                trHtml += "</tr>";

                // 动态添加一行
                $('#dataTable').append(trHtml);
            }
        }

        // 调用分页
        layui.use(['laypage'], function() {
            var laypage = layui.laypage;
            laypage.render({
                elem : 'pageDiv',
                // pages : pages,
                curr : pageIndex || 1, // 当前页
                count : count,
                limit : pageSize,
                skip : true, // 是否开启跳页
                layout : ['count', 'prev', 'page', 'next', 'limit', 'skip'],
                groups : 5, // 连续显示分页数
                jump : function(obj, first) {
                    if (!first) {
                        querFrom(obj.curr, obj.limit);
                    }
                }
            });
        });
    }
}

/**
 * 确认查询
 */
function confirmQueryFrom() {
    querFrom();
}

