
/**
 * 公共方法
 * 
 * @param $type:参数传递方式
 * @param $url:请求路径
 * @param $data:请求参数
 * @param $async:数据加载方式
 * @param $response:返回数据
 */
function callInterface($type, $url, $data, $async) {
	// 调用接口
	var $response = {};
	$.ajax({
		type : $type,
		url : $url,
		data : $data,
		dataType : 'json',
		cache : false,// 从页面缓存获取加载信息
		async : $async, // false 同步加载,true异步加载
		beforeSend : function() {
		},
		success : function(response) {
			$response = response;
		},
		error : function(err) {
			return null;
		},
		complete : function() {
			$data = {};
		}
	});
	return $response;
}

/**
 * 公共方法
 *
 * @param $type:参数传递方式
 * @param $url:请求路径
 * @param $data:请求参数
 * @param $async:数据加载方式
 * @param $response:返回数据
 */
function callInterface2($type, $url, $data, $async) {
	// 调用接口
	var $response = {};
	$.ajax({
		type : $type,
		url : $url,
		data : $data,
		dataType : 'json',
		contentType:"application/json;charset=utf-8",
		cache : false,// 从页面缓存获取加载信息
		async : $async, // false 同步加载,true异步加载
		beforeSend : function() {
		},
		success : function(response) {
			$response = response;
		},
		error : function(err) {
			return null;
		},
		complete : function() {
			$data = {};
		}
	});
	return $response;
}

/**
 * 提示异常
 */
function showErrorMsg(code, message) {
	var noLogin = false;
	if (message == "令牌不存在" || message.indexOf('myId') > 0) {
		noLogin = true;
		message = "登录已超时!";
	}
	if (noLogin || code == 2) {
//		var index = layer.confirm(message + ',请重新登录!', {
//			offset :'20px',btn: ['确定','取消']
//		}, function() {
//			layer.close(index);
//			Utils.toRedirect('login');
//		}, function() {
//			return;
//		});
		showAlert(message + ',请重新登录!');
		Utils.toRedirect('login');
	}
	else {
		if (code == 9999) {
			if (message.length > 100) {
				message = "系统错误!";
			}
		}
		showAlert(message);
	}
	return false;
}

