/**
 * 准备函数
 */
var $layer = null, $form = null;
$(document).ready(function() {
	// 如果不加载form模块，select、checkbox、radio等将无法显示，并且无法使用form相关功能
	layui.use(['layer', 'form'], function() {
		$layer = layui.layer, $form = layui.form;
	});
	
	// 可拖拽
	$(document).on("show.bs.modal", ".modal", function(){
	    $(this).draggable({
	    	handle: ".modal-header"   // 只能点击头部拖动
	    });
	});
});

/**
 * 我的消息页面
 */
function showMsgPage() {
	
	var html = "";
	html += "<li id=\"my_message\" class=\"open\"><a href=\"javascript:jumpPage(my_message,我的消息);\"><i class=\"icon-bell\"><\/i><span class=\"menu-text\">我的消息<\/span><\/a><\/li>";
	var $leftMenu = getWinParent("#leftMenu");
	$leftMenu.html(html);
	
	var $headerNav = getWinParent(".header-nav");
	$headerNav.find("li a").removeClass('on');
	
	layer.closeAll();
	jumpPage('my_message','我的消息');
}

/**
 * 获取父级对象
 * 
 * @param domId
 * @returns
 */
function getWinParent(domId){
	var $winParent = $(window.parent.document).find(domId);
	var $element = $(domId);
	if(Utils.isNotUndefined($winParent)){
		$element = $winParent;
	}
	return $element;
}

/**
 * 更新layuiForm
 */
function updateLayuiForm() {
	layui.use('form', function() {
		// 表单
		_form = layui.form;
		_form.render('select');
		_form.render('checkbox');
		_form.render('radio');
	});
}

/**
 * 提示信息
 */
function showMsg(message) {
	layer.msg(message,{offset :'20px',});
}

/**
 * 弹框信息
 */
function showAlert(message) {
	layer.alert(message,{offset :'20px',});
}

/**
 * 加载链接
 */
function loadHtml(href, name, firstName, secondName) {
	// 勿删：修复页面弹框里链接跳转页面遮罩不关闭问题
	$(".modal-backdrop").hide();
	$(".nav-list li").on("click", function() {
		$(".nav-list li").removeClass("open");
		$(this).addClass("open").siblings("li").removeClass("open");
		$(this).siblings("li").find(".submenu").hide();
	});
	$(".submenu li").on("click", function() {
		$(this).addClass("active").siblings("li").removeClass("active");
	});
	if (Utils.isNotNull(href)) {
		Utils.loadPageContent(href, '');
	}
	else {
		Utils.loadPageContent("blank", '');
	}
	
	var $navigationInfo = getWinParent("#navigationInfo");
	// 面包屑导航
	if (Utils.isNotNull(secondName)) {
		$navigationInfo.html(
				"<li><i class='icon-home home-icon'></i>" + name + "</li><li>" + firstName + "</li><li class='active'>"
						+ secondName + "</li>");
	}
	else {
		if (Utils.isNotNull(firstName)) {
			$navigationInfo.html(
					"<li><i class='icon-home home-icon'></i>" + name + "</li><li class='active'>" + firstName + "</li>");
		}else{
			$navigationInfo.html(
					"<li><i class='icon-home home-icon'></i>" + name + "</li>");
		}
	}
}

/**
 * 查看PDF文件
 */
function viewLayerFile(url) {
	var index = layer.open({
		type : 2,
		title : false,
		anim : -1,
		shift: -1,
		area : ['96%', '96%'],
		offset : '20px',
		time : 0,
		content : url
	});
}

/**
 * 跳转页面
 * 
 * @param url
 * @param tit1
 * @param tit2
 * @param tit3
 */
function jumpPage(url, tit1, tit2, tit3){
	menuActive(url);
	loadHtml(url, tit1, tit2, tit3);
}

/**
 * 菜单选中
 * 
 * @param id
 */
function menuActive(id){
	var index = url.indexOf("?");
	if(index != -1){
		url = url.substring(0, index);
	}
	var $parent = $(window.parent.document);
	var $iframeDom = $parent.find("#"+url);
	if(Utils.isNotUndefined($iframeDom)){
		$iframeDom.siblings().attr('class','');
		$iframeDom.attr('class','active');
	}else{
		$("#"+url).siblings().attr('class','');
		$("#"+url).attr('class','active');
	}
}

/**
 * 关闭LoadingDiv
 */
function closeLoadingDiv() {
	var loadingDiv = document.getElementById("loadingDiv");
	if (loadingDiv != null) {
		document.body.removeChild(loadingDiv);
	}
}

/**
 * 创建LoadingDiv
 */
function createLoadingDiv() {
	var loadingDiv = document.getElementById("loadingDiv");
	if (loadingDiv != null) {
		document.body.removeChild(loadingDiv);
	}
	loadingDiv = document.createElement("div");
	loadingDiv.id = "loadingDiv";
	loadingDiv.style.position = "fixed";
	loadingDiv.style.background = "rgba(0,0,0,.6)";
	loadingDiv.style.zIndex = "999999999";
	// loadingDiv.style.backgroundColor = "#ffffff";
	// loadingDiv.style.Font="10pt";
	loadingDiv.style.width = "100%";
	loadingDiv.style.height = "100%";
	loadingDiv.style.left = "0";
	loadingDiv.style.top = "0";
	loadingDiv.innerHTML = "<img src='image/loading.gif' style='display:block;position:absolute;top:50%;left:50%;width:66px;height:66px;margin:-33px 0 0 -33px;' />";
	document.body.appendChild(loadingDiv);
}