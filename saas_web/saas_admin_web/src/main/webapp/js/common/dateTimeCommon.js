
/**
 * 校验开关机时间
 * @param mStartTimeHh
 * @param mCloseTimeHh
 * @param mStartTimeMm
 * @param mCloseTimeMm
 * @returns
 */
function checkSCTime(startTimeHh, closeTimeHh, startTimeMm, closeTimeMm) {
	var t = true;
	if (parseInt(closeTimeHh) < parseInt(startTimeHh)) {
		t = false;
	}
	else if (parseInt(closeTimeHh) == parseInt(startTimeHh)) {
		if (parseInt(closeTimeMm) <= parseInt(startTimeMm)) {
			t = false;
		}
	}
	return t;
}

/**
 * 校验开关机时间
 * @param beginTimeStr
 * @param endTimeStr
 * @returns
 */
function checkBETime(beginTimeStr,endTimeStr) {
	var d1 = getCurrentDate() + " " + beginTimeStr;
	var d2 = getCurrentDate() + " " + endTimeStr;
	return compareDate(d1,d2);
}

/**
 * 比较俩个日期大小
 * @param d1
 * @param d2
 * @returns {Boolean}
 */
function compareDate(d1,d2){
  return ((new Date(d1.replace(/-/g,"\/"))) < (new Date(d2.replace(/-/g,"\/"))));
}

/**
 * 获取当前日期加时间
 * 
 * @returns {String}
 */
function getPresetTime($day, $hour) {
	// 获取
	var now = new Date();
	now.setDate(now.getDate() + $day);// 获取$day天后的日期
	if ($day > 0) {
		now.setHours($hour); // 时
		now.setMinutes(0); // 分
		now.setSeconds(0); // 秒
	}
	else if($day == 0){
		now.setHours(now.getHours()+ $hour); // 时
	}
	var year = now.getFullYear(); // 年
	var month = now.getMonth() + 1; // 月
	var day = now.getDate(); // 日
	var hh = now.getHours(); // 时
	var mm = now.getMinutes(); // 分
	var ss = now.getSeconds(); // 秒
	
	// 组装
	var datetime = year + "-";
	if (month < 10) {
		datetime += "0";
	}
	datetime += month + "-";
	if (day < 10) {
		datetime += "0";
	}
	datetime += day + " ";
	if (hh < 10) {
		datetime += "0";
	}
	datetime += hh + ":";
	if (mm < 10) {
		datetime += '0';
	}
	datetime += mm + ":";
	if (ss < 10) {
		datetime += '0';
	}
	datetime += ss;

	// 返回
	return datetime;
}

/**
 * 获取当前年份(如:2015)
 * 
 * @returns {String}
 */
function getCurrentYear() {
	var now = new Date();
	var year = now.getFullYear(); // 年
	return year;
}

/**
 * 获取当前日期(如:2015-11-12)
 * 
 * @returns {String}
 */
function getCurrentDate() {
	// 获取
	var now = new Date();
	var year = now.getFullYear(); // 年
	var month = now.getMonth() + 1; // 月
	var day = now.getDate(); // 日

	// 组装
	var datetime = year + "-";
	if (month < 10) {
		datetime += "0";
	}
	datetime += month + "-";
	if (day < 10) {
		datetime += "0";
	}
	datetime += day;

	// 返回
	return datetime;
}

/**
 * 获取当前日期加时间(如:2015-11-12 12:00)
 * 
 * @returns {String}
 */
function getCurrentTime() {
	// 获取
	var now = new Date();
	var year = now.getFullYear(); // 年
	var month = now.getMonth() + 1; // 月
	var day = now.getDate(); // 日
	var hh = now.getHours(); // 时
	var mm = now.getMinutes(); // 分
	var ss = now.getSeconds(); // 秒

	// 组装
	var datetime = year + "-";
	if (month < 10) {
		datetime += "0";
	}
	datetime += month + "-";
	if (day < 10) {
		datetime += "0";
	}
	datetime += day + " ";
	if (hh < 10) {
		datetime += "0";
	}
	datetime += hh + ":";
	if (mm < 10) {
		datetime += '0';
	}
	datetime += mm + ":";
	if (ss < 10) {
		datetime += '0';
	}
	datetime += ss;

	// 返回
	return datetime;
}

/**
 * 获取当前日期加时间(如:2015-11-12 12:00)
 * 
 * @returns {String}
 */
function getTime(){
	// 获取
	var now = new Date();
	var year = now.getFullYear(); // 年
	var month = now.getMonth() + 1; // 月
	var day = now.getDate(); // 日
	var hh = now.getHours(); // 时
	var mm = now.getMinutes(); // 分
	var ss = now.getSeconds(); // 秒

	// 组装
	var datetime = year;
	if (month < 10) {
		datetime += "0";
	}
	datetime += month;
	if (day < 10) {
		datetime += "0";
	}
	datetime += day + "_";
	if (hh < 10) {
		datetime += "0";
	}
	datetime += hh;
	if (mm < 10) {
		datetime += '0';
	}
	datetime += mm;
	return datetime; 
}

/**
 * 获取指定日期day天后的日期
 * @param date
 * @param day
 */
function getAfterDaysDate(date, day) {
	var now = new Date(date);
	now.setDate(now.getDate() + day);
	var date = now.getFullYear() + "-" + ((now.getMonth() + 1) < 10 ? "0" : "") + (now.getMonth() + 1) + "-"
			+ (now.getDate() < 10 ? "0" : "") + now.getDate();
	return date;
}


/**
 * 设置当前日期
 * day天后的日期
 */
function getAfterDate(day) {
	// 日期
	var now = new Date();
	now.setDate(now.getDate() + day);// 获取day天前的日期
	var date = now.getFullYear() + "-" + ((now.getMonth() + 1) < 10 ? "0" : "") + (now.getMonth() + 1) + "-"
			+ (now.getDate() < 10 ? "0" : "") + now.getDate();
	return date;
}

/**
 * 设置当前日期
 * day天前的日期
 */
function getCurrentDay(day) {
	// 起止日期数组
	var startStop = new Array();
	var now = new Date();
	var lastDay = now.getFullYear() + "-" + ((now.getMonth() + 1) < 10 ? "0" : "") + (now.getMonth() + 1) + "-"
			+ (now.getDate() < 10 ? "0" : "") + now.getDate()/*
																 * + " " +
																 * (now.getHours() <
																 * 10 ? "0" :
																 * "") +
																 * now.getHours() +
																 * ":" +
																 * (now.getMinutes() <
																 * 10 ? "0" :
																 * "") +
																 * now.getMinutes()
																 */;

	now.setDate(now.getDate() - day);// 获取6天前的日期
	var firstDay = now.getFullYear() + "-" + ((now.getMonth() + 1) < 10 ? "0" : "") + (now.getMonth() + 1) + "-"
			+ (now.getDate() < 10 ? "0" : "") + now.getDate()/*
																 * + " " +
																 * (now.getHours() <
																 * 10 ? "0" :
																 * "") +
																 * now.getHours() +
																 * ":" +
																 * (now.getMinutes() <
																 * 10 ? "0" :
																 * "") +
																 * now.getMinutes()
																 */;

	// 起始时间
	var beginTime = firstDay + " 00:00:00";
	// 终止时间
	var endTime = lastDay + " 23:59:59";
	// 添加至数组中
	startStop.push(beginTime);
	startStop.push(endTime);

	// 返回
	return startStop;
}

/**
 * 获得本周起止时间
 */
function getCurrentWeek() {
	// 起止日期数组
	var startStop = new Array();
	// 获取当前时间
	var currentDate = new Date();
	// 返回date是一周中的某一天
	var week = currentDate.getDay();
	// 返回date是一个月中的某一天
	var month = currentDate.getDate();
	// 一天的毫秒数
	var millisecond = 1000 * 60 * 60 * 24;
	// 减去的天数
	var minusDay = week != 0 ? week - 1 : 6;
	// 本周 周一
	var monday = new Date(currentDate.getTime() - (minusDay * millisecond));
	// 本周 周日
	var sunday = new Date(monday.getTime() + (6 * millisecond));

	// 本周起始时间
	var beginTime = monday.getFullYear() + "-" + ((monday.getMonth() + 1) < 10 ? "0" : "") + (monday.getMonth() + 1)
			+ "-" + (monday.getDate() < 10 ? "0" : "") + monday.getDate() + " 00:00:00";
	// 本周终止时间
	var endTime = sunday.getFullYear() + "-" + ((sunday.getMonth() + 1) < 10 ? "0" : "") + (sunday.getMonth() + 1)
			+ "-" + (sunday.getDate() < 10 ? "0" : "") + sunday.getDate() + " 23:59:59";
	// 添加至数组中
	startStop.push(beginTime);
	startStop.push(endTime);

	// 返回
	return startStop;
}

/*
 * 获得本月的起止时间
 */
function getCurrentMonth() {
	// 起止日期数组
	var startStop = new Array();
	// 获取当前时间
	var currentDate = new Date();
	// 获得当前月份0-11
	var currentMonth = currentDate.getMonth();
	// 获得当前年份4位年
	var currentYear = currentDate.getFullYear();
	// 求出本月第一天
	var firstDay = new Date(currentYear, currentMonth, 1);
	// 当为12月的时候年份需要加1
	// 月份需要更新为0 也就是下一年的第一个月
	if (currentMonth == 11) {
		currentYear++;
		currentMonth = 0;// 就为
	}
	else {
		// 否则只是月份增加,以便求的下一月的第一天
		currentMonth++;
	}
	// 一天的毫秒数
	var millisecond = 1000 * 60 * 60 * 24;
	// 下月的第一天
	var nextMonthDayOne = new Date(currentYear, currentMonth, 1);
	// 求出上月的最后一天
	var lastDay = new Date(nextMonthDayOne.getTime() - millisecond);

	// 本周起始时间
	var beginTime = firstDay.getFullYear() + "-" + ((firstDay.getMonth() + 1) < 10 ? "0" : "")
			+ (firstDay.getMonth() + 1) + "-" + (firstDay.getDate() < 10 ? "0" : "") + firstDay.getDate() + " 00:00:00";
	// 本周终止时间
	var endTime = lastDay.getFullYear() + "-" + ((lastDay.getMonth() + 1) < 10 ? "0" : "") + (lastDay.getMonth() + 1)
			+ "-" + (lastDay.getDate() < 10 ? "0" : "") + lastDay.getDate() + " 23:59:59";

	// 添加至数组中
	startStop.push(beginTime);
	startStop.push(endTime);

	// 返回
	return startStop;
}

/**
 * 时间比较(yyyy-mm-dd hh:mi:ss)
 * 
 * @returns
 */
function datetimeCompare() {
	var beginTime = "2015-09-21 00:00:00";
	var endTime = "2015-09-21 00:00:01";
	var beginTimes = beginTime.substring(0, 10).split('-');
	var endTimes = endTime.substring(0, 10).split('-');

	beginTime = beginTimes[1] + '-' + beginTimes[2] + '-' + beginTimes[0] + ' ' + beginTime.substring(10, 19);
	endTime = endTimes[1] + '-' + endTimes[2] + '-' + endTimes[0] + ' ' + endTime.substring(10, 19);

	var a = (Date.parse(endTime) - Date.parse(beginTime)) / 3600 / 1000;
	if (a < 0) {
		alert("endTime小!");
	}
	else if (a > 0) {
		alert("endTime大!");
	}
	else if (a == 0) {
		alert("时间相等!");
	}
	else {
		return 'exception'
	}
}

/**
 * 将秒数换成时分秒格式
 */
function formatSeconds(value) {
	var theTime = parseInt(value);// 秒
	var theTime1 = 0;// 分
	var theTime2 = 0;// 小时
	if (theTime > 60) {
		theTime1 = parseInt(theTime / 60);
		theTime = parseInt(theTime % 60);
		if (theTime1 > 60) {
			theTime2 = parseInt(theTime1 / 60);
			theTime1 = parseInt(theTime1 % 60);
		}
	}
	var result = "" + parseInt(theTime) + "秒";
	if (theTime1 > 0) {
		result = "" + parseInt(theTime1) + "分" + result;
	}
	if (theTime2 > 0) {
		result = "" + parseInt(theTime2) + "小时" + result;
	}
	return result;
}

/**
 * 计算天数差的函数，通用
 * @param startDate 格式:yyyy-MM-dd  
 * @param endDate 格式:yyyy-MM-dd 
 * @returns
 */  
function dateDiff(endDate, startDate) {
	var iDays = parseInt(1 + Math.abs(new Date(endDate) - new Date(startDate)) / 1000 / 60 / 60 / 24);//把相差的毫秒数转换为天数  
	return iDays;
}  

/**
 * 时间单位转换(s,m,h)
 * 
 * @param bytes
 * @returns
 */
function formatSeconds(value) {
	if (value === 0)
		return '0秒';
	var theTime = parseInt(value);// 秒
	var theTime1 = 0;// 分
	var theTime2 = 0;// 小时
	if (theTime > 60) {
		theTime1 = parseInt(theTime / 60);
		theTime = parseInt(theTime % 60);
		if (theTime1 > 60) {
			theTime2 = parseInt(theTime1 / 60);
			theTime1 = parseInt(theTime1 % 60);
		}
	}
	var result = "" + parseInt(theTime) + "秒";
	if (theTime1 > 0) {
		result = "" + parseInt(theTime1) + "分" + result;
	}
	if (theTime2 > 0) {
		result = "" + parseInt(theTime2) + "小时" + result;
	}
	return result;
}

/**
 * 格式化日期(yyyy-MM-dd)
 * 
 * @returns {String}
 */
function getFormatDate($date) {
	var Year = 0;
	var Month = 0;
	var Day = 0;
	var datetime = "";
	Year = $date.getFullYear();// 支持IE和火狐浏览器.
	Month = $date.getMonth() + 1;
	Day = $date.getDate();
	datetime += Year + "-";
	if (Month >= 10) {
		datetime += Month + "-";
	}
	else {
		datetime += "0" + Month + "-";
	}
	if (Day >= 10) {
		datetime += Day;
	}
	else {
		datetime += "0" + Day;
	}
	return datetime;
}

/**
 * 判断日期格式 2018-03-21至2018-12-31
 * **/
function dateForm($date){
	var date = /^(\d{4})-(0\d{1}|1[0-2])-(0\d{1}|[12]\d{1}|3[01])$/;
	return date.test($date);
}

function checkDate(d){
   var ds=d.match(/\d+/g),ts=['getFullYear','getMonth','getDate'];
     var d=new Date(d.replace(/-/g,'/')),i=3;
     ds[1]--;
     while(i--)if( ds[i]*1!=d[ts[i]]()) return false;
     return true;
}

/**
 * 获取指定日期的后一天
 */
function getNextDay($date, i) {
	var date = new Date($date);
	date.setDate(date.getDate() + i);

	// 格式化
	return getFormatDate(date);
}

/**
 * 求两个日期相差的天数
 */
function getDays(beginDate, endDate) {
	var s1 = new Date(beginDate.replace(/-/g, "/"));
	var s2 = new Date(endDate.replace(/-/g, "/"));
	var days = s2.getTime() - s1.getTime();
	return parseInt(days / (1000 * 60 * 60 * 24));
}