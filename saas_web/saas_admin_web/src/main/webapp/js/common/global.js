// 公共信息
var Global = {
		
	//分页：默认每页显示条数
	PAGE_SISE : 10
	
	//分页条数选择
	,LIMITS : [10, 20, 30, 40, 50, 100]

	//是否启用投放模板 
	,IS_USE_TEMPLET : true
	
	// 投放模板缩放比例
	,TEMPLET_MULT : 4
	
	//默认皮肤颜色
	,DEFAULT_COLOR : '#54a9ff'
	
	//日期段选中分割符号
	,DATE_RANGE_STR : '~'
	
	,DATE_SPLIT_STR:'至'
	
	//时间选择格式
	,TIME_FORMAT_STR : 'HH:mm:ss'
	
	//日期选择格式
	,DATE_FORMAT_STR : 'yyyy-MM-dd'
	
	//日期+时间选择格式
	,DATETIME_FORMAT_STR : 'yyyy-MM-dd HH:mm:ss'
	
	//CDN图片大小为：100px
	,IMG_SiZE_ONE_H : '@!100'
		
	//阿里CDN图片大小为：200px
	,IMG_SiZE_TWO_H : '@!200f'	
		
	//CDN图片大小为：300px
	,IMG_SiZE_THREE_H : '@!300'
		
	,DEFAULT_PIC : 'image/filePreview/list-def-pic.jpg'
		
	,DEFAULT_VIDEO_PIC : 'image/filePreview/video.png'	
		
	,DEFAULT_AUDIO_PIC : 'image/filePreview/audio.png'
	
	,DEFAULT_WEB_PAGE_PIC : 'image/filePreview/webpage.png'
		
	,DEFAULT_OFFICE_PIC : 'image/filePreview/office.png'
			
	,DEFAULT_OTHER_PIC : 'image/filePreview/other.png'
	
	,LOADING_ERR_PIC : 'image/filePreview/loaderr.jpg'
	
	,OFFICE_PRE_STR : 'https://view.officeapps.live.com/op/view.aspx?src='
		
	//layer 遮罩透明度和颜色
	,LAYER_SHADE : [0.3,'#333']
}

//投放类型
var PlanType = {
	AD : '广告'
	,AD_V : 1
	,CONTENT : '内容'		
	,CONTENT_V : 2
}

//素材类型
var MaterialCategroyType = {
	AD : '广告素材'
	,AD_V : 1
	,AUTH : '报审材料'		
	,AUTH_V : 2
	,RTB_AD : '竞价广告素材'		
	,RTB_AD_V : 3
}

//素材类型
var FileType = {
	OTHER : '其它'		
	,OTHER_V : 0
	,OTHER_SUFFIX : 'rar,zip'
	,OTHER_MAX_SIZE : 50
	,OTHER_CON_TYPE : ''
	,IMAGE : '图片'
	,IMAGE_V : 1
	,IMAGE_SUFFIX : 'gif,jpg,jpeg,png,bmp'
	,IMAGE_MAX_SIZE : 10	
	,IMAGE_CON_TYPE : 'image'
	,IMAGE_PRE : '预览图片'
	,IMAGE_PRE_V : 11
	,IMAGE_PRE_SUFFIX : 'jpg,jpeg,png,bmp'
	,IMAGE_PRE_MAX_SIZE : 0.5	
	,IMAGE_PRE_CON_TYPE : 'image'
	,VIDEO : '视频'		
	,VIDEO_V : 2
	,VIDEO_SUFFIX : 'mp4'
	,VIDEO_MAX_SIZE : 1024
	,VIDEO_CON_TYPE : 'video/mp4'
	,AUDIO : '音频'		
	,AUDIO_V : 3
	,AUDIO_SUFFIX : 'mp3'
	,AUDIO_MAX_SIZE : 10
	,AUDIO_CON_TYPE : 'audio/mpeg'
	,WEB_PAGE : '网页'		
	,WEB_PAGE_V : 4
	,WEB_PAGE_SUFFIX : 'html'
	,OFFICE : 'office文档'		
	,OFFICE_V : 5
	,OFFICE_SUFFIX : 'pdf,doc,docx,xls,xlsx,pptx'	
}

var OfficeFileType = {
	EXCEL_SUFFIX : 'xls,xlsx'	
	,EXCEL_PREVIEW : 'excel'
	,EXCEL_CON_TYPE : 'application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
	,WORD_SUFFIX : 'doc,docx'
	,WORD_PREVIEW : 'word'	
	,WORD_CON_TYPE : 'application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document'
	,PPT_SUFFIX : 'pptx'
	,PPT_PREVIEW : 'ppt'	
	,PPT_CON_TYPE : 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
	,PDF_SUFFIX : 'pdf'
	,PDF_PREVIEW : 'pdf'
	,PDF_CON_TYPE : 'application/pdf'		
}

//素材类型
var CompanyId = {
	PLATFORM_V : 0	
}

// 公司来源
var ComSourceType = ["", "后台创建", "前端注册"];

// 素材审核状态
var MaterialAuditStatus = {
	PENDING_AUDIT: "待审核",
	PENDING_AUDIT_V : 0, 
	PASS_AUDIT : "审核通过",
	PASS_AUDIT_V : 1,
	NOPASS_AUDIT : "审核未通过",
	NOPASS_AUDIT_V : 2
}

var AuthCodeType = {
	REGISTER : '注册用户'
	,REGISTER_V : 1
	,RETRIEVE : '找回密码'		
	,RETRIEVE_V : 2
	,BIND_PHONE : '绑定手机'		
	,BIND_PHONE_V : 3
	,UNBIND_PHONE : '解绑手机'		
	,UNBIND_PHONE_V : 4	
}