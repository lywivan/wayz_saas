/**
 * 触发选择文件事件
 * 
 * @param selectFileId
 */
function doTriggerClick(selectFileId){
	$("#"+selectFileId).trigger("click");
}

/**
 * 校验文件大小是否超限
 * 
 * @param type 文件类型
 * @param totalSize 文件总大小
 * @returns {Boolean}
 */
function checkUploadFileSize(type, totalSize){
	var flag = true;
	if (type == FileType.IMAGE_V){
		if(totalSize > FileType.IMAGE_MAX_SIZE){
			flag = false;
		}
	}else if (type == FileType.IMAGE_PRE_V){
		if(totalSize > FileType.IMAGE_PRE_MAX_SIZE){
			flag = false;
		}
	}else if (type == FileType.VIDEO_V){
		if(totalSize > FileType.VIDEO_MAX_SIZE){
			flag = false;
		}
	}else if (type == FileType.AUDIO_V){
		if(totalSize > FileType.AUDIO_MAX_SIZE){
			flag = false;
		}
	}else if (type == FileType.OTHER_V){
		if(totalSize > FileType.OTHER_MAX_SIZE){
			flag = false;
		}
	}
	return flag;
}

function getMaxFileSizeDes(){
	var maxInfo = FileType.IMAGE +"<=" + FileType.IMAGE_MAX_SIZE +"M、";
 		maxInfo += FileType.VIDEO +"<=" + FileType.VIDEO_MAX_SIZE +"M";
 	return maxInfo;
}

/**
 * 获取文件大小 返回单位M
 * 
 * @param size 单位B
 * @returns {String} 
 */
function getFileSizeMByB(size) {
    if (Utils.isNull(size)){
    	return "";
    }
    return (size / Math.pow(1024.00, 2)).toFixed(2); //M
}

/**
 * 自动获取文件大小
 * 
 * @param size 单位B
 * @returns {String}
 */
function getFileSizeAuto(size) {
	if (Utils.isNull(size)){
    	return "";
    }
    
    var num = 1024.00; //byte
    if (size < num)
        return size + "B";
    if (size < Math.pow(num, 2))
        return (size / num).toFixed(2) + "K"; //kb
    if (size < Math.pow(num, 3))
        return (size / Math.pow(num, 2)).toFixed(2) + "M"; //M
    if (size < Math.pow(num, 4))
        return (size / Math.pow(num, 3)).toFixed(2) + "G"; //G
    return (size / Math.pow(num, 4)).toFixed(2) + "T"; //T
}

function getUuid(){
	  var len=32;//32长度
	  var radix=16;//16进制
	  var chars='0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
	  var uuid=[],i;radix=radix||chars.length;
	  if(len){
		  for(i=0;i<len;i++)
			  uuid[i]=chars[0|Math.random()*radix];
	  }else{
		var r;uuid[8]=uuid[13]=uuid[18]=uuid[23]='-';uuid[14]='4';
		for(i=0;i<36;i++){
			if(!uuid[i]){
				r=0|Math.random()*16;uuid[i]=chars[(i==19)?(r&0x3)|0x8:r];
			}
		}
	 }
	 return uuid.join('');
}

/**
 * 获取默认底图
 * 
 * @param type (0:其他 1:图片 2:视频 3:音频 4:网页 5:OFFICE文档) 
 * @param url
 * @returns
 */
function getDefPic(type, url){
	if (type == FileType.IMAGE_V){
		url = url + Global.IMG_SiZE_TWO_H
	}else if (type == FileType.VIDEO_V){
		url = Global.DEFAULT_VIDEO_PIC;
	}else if (type == FileType.AUDIO_V){
		url = Global.DEFAULT_AUDIO_PIC;
	}else if (type == FileType.WEB_PAGE_V){
		url = Global.DEFAULT_WEB_PAGE_PIC;
	}else if (type == FileType.OFFICE_V){
		url = Global.DEFAULT_OFFICE_PIC;
	}else {
		url = Global.DEFAULT_OTHER_PIC;
	}
	return url;
}

/**
 * 获取文件类型数值
 *
 * 说明：(0:其他 1:图片 2:视频 3:音频 4:网页 5:OFFICE文档)
 * @param fileSuffix 文件后缀
 * 注：avi,wmv 默认属于视频
 * @returns {Number}
 */
function getFileTypeV(fileSuffix, contentType){
	if(Utils.isNull(contentType)){
		return "";
	}
	if(FileType.OTHER_SUFFIX.indexOf(fileSuffix)>-1){
		return FileType.OTHER_V;
	}else if(FileType.IMAGE_SUFFIX.indexOf(fileSuffix)>-1 && contentType.indexOf(FileType.IMAGE_CON_TYPE)>-1){
		return FileType.IMAGE_V;
	}else if(FileType.VIDEO_SUFFIX.indexOf(fileSuffix)>-1 && FileType.VIDEO_CON_TYPE.indexOf(contentType)>-1){
		return FileType.VIDEO_V;
	}else if(FileType.AUDIO_SUFFIX.indexOf(fileSuffix)>-1 && FileType.AUDIO_CON_TYPE.indexOf(contentType)>-1){
		return FileType.AUDIO_V;
	}else if(isOfficeFile(fileSuffix,contentType)){
		return FileType.OFFICE_V;
	}else{
		return "";
	}
}

/**
 * 获取审核状态名
 * 
 * @param auditStatus
 * @returns
 */
function getMatAuditStatusName(auditStatus){
	if(MaterialAuditStatus.PENDING_AUDIT_V == auditStatus){
		return MaterialAuditStatus.PENDING_AUDIT;
	}else if(MaterialAuditStatus.PASS_AUDIT_V == auditStatus){
		return MaterialAuditStatus.PASS_AUDIT;
	}else if(MaterialAuditStatus.NOPASS_AUDIT_V == auditStatus){
		return MaterialAuditStatus.NOPASS_AUDIT;
	}else{
		return "";
	}
}

function isOfficeFile(fileSuffix,contentType){
	// 不支持.ppt文件
	if(fileSuffix != OfficeFileType.PPT_PREVIEW && FileType.OFFICE_SUFFIX.indexOf(fileSuffix)>-1){
		if(OfficeFileType.EXCEL_CON_TYPE.indexOf(contentType)>-1 
			|| OfficeFileType.WORD_CON_TYPE.indexOf(contentType)>-1
				|| OfficeFileType.PPT_CON_TYPE.indexOf(contentType)>-1
					|| OfficeFileType.PDF_CON_TYPE.indexOf(contentType)>-1){
			return true;
		}
	}
	return false;
}

/**
 * 根据后缀获取文档类型预览图名称
 * 
 * @param suffix
 * @returns
 */
function getOfficePreviewName(suffix){
	if(OfficeFileType.EXCEL_SUFFIX.indexOf(suffix) > -1){
		return OfficeFileType.EXCEL_PREVIEW;
	}else if(OfficeFileType.WORD_SUFFIX.indexOf(suffix) > -1){
		return OfficeFileType.WORD_PREVIEW;
	}else if(OfficeFileType.PPT_SUFFIX.indexOf(suffix) > -1){
		return OfficeFileType.PPT_PREVIEW;
	}else{
		return suffix;
	}
}

/**
 * 获取文件类型后缀 
 * 
 * @param type 文件类型 (0:其他 1:图片 2:视频 3:音频 4:网页 5:OFFICE文档) 
 * 注：avi,wmv 默认属于视频
 * @returns {String}
 */
function getFileSuffix(type){
	if(type == FileType.IMAGE_V){
		return FileType.IMAGE_SUFFIX;
	}else if(type == FileType.VIDEO_V){
		return FileType.VIDEO_SUFFIX;
	}else if(type == FileType.AUDIO_V){
		return FileType.AUDIO_SUFFIX;
	}else if(type == FileType.WEB_PAGE_V){
		return FileType.WEB_PAGE_SUFFIX;
	}else if(type == FileType.OFFICE_V){
		return FileType.OFFICE_SUFFIX;
	}else if(type == FileType.OTHER_V){
		return FileType.OTHER_SUFFIX;
	}else{
		return "";
	}
}

/**
 * 获取文件类型名称 
 * 
 * @param type 文件类型 (0:其他 1:图片 2:视频 3:音频 4:网页 5:OFFICE文档) 
 * @returns {String}
 */
function getFileTypeName(type){
	if(type == FileType.IMAGE_V){
		return FileType.IMAGE;
	}else if(type == FileType.VIDEO_V){
		return FileType.VIDEO;
	}else if(type == FileType.AUDIO_V){
		return FileType.AUDIO;
	}else if(type == FileType.WEB_PAGE_V){
		return FileType.WEB_PAGE;
	}else if(type == FileType.OFFICE_V){
		return FileType.OFFICE;
	}else if(type == FileType.OTHER_V){
		return FileType.OTHER;
	}else{
		return "";
	}
}

/**
 * 根据后缀获取文件格式
 * 
 * @param url	实际地址
 * @param preUrl 预览地址
 * @returns
 */
function getTempFormat(url, preUrl){
	var tempUrl = Utils.isNull(preUrl) ? url : preUrl;
	var ftArray = tempUrl.split(".");
	return ftArray[ftArray.length - 1];
}

/**
 * 设置文件上传进度
 * 
 * @param percent
 * @param fileIndex
 */
function setFileProgress(percent,fileIndex){
	$(".fileProgress_"+fileIndex).css({
    	'width' : percent+"%",
    	'background' : '#7FFFD4'
    });
	var percentInfo = "正在上传：" + percent + "%";
	if(percent == 100 || percent == "100"){
		percentInfo = "上传完成：" + percent + "%";
	}
    $(".fileProgressTit_"+fileIndex).html(percentInfo);
}

function getOssWrapper(){
	// 调用接口
	var response = callInterface("post", "file/getUploadAdminToken",{}, false);
	if (response.code == 0) {
		var data = response.data;
		if (data != null) {
			return new OSS.Wrapper({
				region: "oss-" + data.endPoint,
				accessKeyId: data.accessKeyId,/*这两者到阿里云控制台获得*/
				accessKeySecret: data.accessKeySecret,
				bucket: data.bucketName,/*装图片的桶名*/
				stsToken: data.securityToken,
				secure: true
			});
		}
	}
}

function getStoreAs(file){
	var suffx = file.name.substr(file.name.indexOf("."));
	return "material/" + getUuid() + suffx;
}

function getImgInfo (url) {
    return new Promise((resolve, reject) => {
        var img = new Image();
        img.src = url;
        var timer = setInterval(function () {
            if (img.width > 0 || img.height > 0) {
                resolve({
                    width: img.width,
                    height: img.height,
                    size: img
                })
                clearInterval(timer);
            }
        }, 50);
    });
}

/**
 * 资源加载失败处理
 * 
 * @param t
 */
function loadError(t){
	var sty = $(t).attr('style');
	$(t).parent().html('<img title="资源已加载失败" src="' + Global.LOADING_ERR_PIC + '" style="'+sty+'" />')
}

/**
 * 设置预览画布素材
 * 
 * @param type
 * @param matName
 * @param url
 */
function setFilePreviewHtml(type, matName, url){
	
	if(Utils.isNotNull(type)){
		var lHtml = "";
		var sty = "width:100%;height:100%;top:0px;left:0px";
		if(type == FileType.IMAGE_V){
			//图片
			lHtml += '<img title="' + matName + '" src="' + url + '" onerror="loadError(this);" style="' + sty + '" />';
		}else if(type == FileType.VIDEO_V){
			//视频
			lHtml += '<video title="' + matName + '" src="' + url + '" onerror="loadError(this);" preload loop autoplay style="' + sty + '"></video>';
		}
		$("#materialDetailCanvasZone").html(lHtml);
	}else{
		$("#materialDetailCanvasZone").children().attr("title","无素材");
		$("#materialDetailCanvasZone").html("");
	}
}

/**
 * 获取文件md5
 */
function getFileMd5($dom, file){
	var blobSlice = File.prototype.slice || File.prototype.mozSlice || File.prototype.webkitSlice,
    chunkSize = 2097152, // read in chunks of 2MB
    chunks = Math.ceil(file.size / chunkSize),
    currentChunk = 0,
    spark = new SparkMD5.ArrayBuffer(),
    frOnload = function(e){
        spark.append(e.target.result); // append array buffer
        currentChunk++;
        if (currentChunk < chunks){
        	 loadNext();
        }else{
        	$dom.attr('data-md5', spark.end().toUpperCase());
        }
    },
    frOnerror = function () {
        console.log("spark md5 err");
    };
    function loadNext() {
        var fileReader = new FileReader();
        fileReader.onload = frOnload;
        fileReader.onerror = frOnerror;
        var start = currentChunk * chunkSize,
                end = ((start + chunkSize) >= file.size) ? file.size : start + chunkSize;
        fileReader.readAsArrayBuffer(blobSlice.call(file, start, end));
    };
    loadNext();
}

/**
 * 处理阿里oss文件地址
 */
function handleOssUrl(url){
	var length = url.lastIndexOf('?');
	var fileUrl = "";
	if(length>0){
		fileUrl = url.substr(0,length);//文件最终路径
	}else{
		fileUrl = url; 
	}
	return fileUrl;
}