// 上传成功的文件集合
var succUploadArr = [];
// 上传失败的文件集合
var failUploadArr = [];
// 选择的全部文件集合
var multipartFileArr = [];
// 文件下标
var fileIndex = 0;
// 视频域
var $Vedio;
$(document).ready(function() {
	$Vedio = $("#myVideo");
	$(document).on("click",".delFileItem", function(){
		delFileItem(this);
	});
});

/**
 * 初始化
 */
function initMultArr(){
	succUploadArr = [];
	failUploadArr = [];
	multipartFileArr = [];
	fileIndex = 0;
}

/**
 * 触发选择文件事件
 * 
 * @param selectFileId
 */
function doTriggerClick(selectFileId){
	$("#"+selectFileId).trigger("click");
}

/**
 * 多素材文件上传 
 * 
 * @param selectFileId 文件选择框ID
 */
function selectMultipartFile(selectFileId) {
	
	// 当前选择的文件
	var fileObjs = document.getElementById(selectFileId);
	var fileList = fileObjs.files;
	// 已选择的文件
	var index = multipartFileArr.length;
	for (var i = 0; i < fileList.length; i++) {
		
		// 文件名称
		var fileName = fileList[i].name;
		var suffix = (fileName.substring(fileName.lastIndexOf(".")+1,fileName.length)).toLowerCase(); 
		
		// 获取文件类型和缩略图
		var fileType = getFileTypeV(suffix, fileList[i].type);
		if(fileType == FileType.OFFICE_V){
			suffix = getOfficePreviewName(suffix);
		}
		
		if (fileName.length > 50) {
			fileName = fileName.substring(0, 50);
		}
		// 索引追加处理
		if(index>0 && i==0){
			index = multipartFileArr.length;
		}else if(index==0 && i==0){
			index = 0;
		}else{
			index++;
		}
		// 存放图片内容的数组，提交数据到后台时用
		multipartFileArr.push(fileList[i]);
		
		// 初始化、进度条(默认隐藏)、文件名
		var fileDiv = "";
	    fileDiv += "	<div class='fileItem file-item fl materialType_"+index+"' data-index='"+index+"' data-type='" + fileType + "'>";
	    fileDiv += "		<div class='item-img'>";
	    fileDiv += "			<img class='materialUrl_"+index+"' src=\"image/fileLogo/"+suffix+".jpg\" />";
	    fileDiv += "		</div>";
	    fileDiv += "		<div class='item-name'>";
	    fileDiv += "			<input type='text' placeholder='素材名称' class='materialName_"+index+"' value='"+fileName+"'/>";
	    fileDiv += "		</div>";
	    fileDiv += "		<div class='progress'>";
	    fileDiv += "			<div class='solidBar fileProgress_"+index+"' style='width:100%' ></div>";
	    fileDiv += "			<div class='progressTit fileProgressTit_"+index+"'>点击确定后开始上传...</div>";
	    fileDiv += "		</div>";
	    fileDiv += "		<i class=\"delFileItem close-img icon-remove\" title=\"删除\"></i>";
	    fileDiv += "	</div>";
	    $("#fileDivList").append(fileDiv);
	}
	return false;
}

/**
 * 删除文件
 * 
 * @param t
 */
function delFileItem(t){
	var $fileItem = $(t).closest(".fileItem");
	var index = $fileItem.attr("data-index");
	multipartFileArr.splice(index,1); 
	$fileItem.remove();
	
	// 删除后重新组织索引
	var fileDiv = "";
	$(multipartFileArr).each(function(index,v){
		var fileName = v.name;
		var suffix = (fileName.substring(fileName.lastIndexOf(".")+1,fileName.length)).toLowerCase(); 
		
		// 获取文件类型和缩略图
		var fileType = getFileTypeV(suffix, multipartFileArr[index].type);
		if(fileType == FileType.OFFICE_V){
			suffix = getOfficePreviewName(suffix);
		}
		
		if (fileName.length > 50) {
			fileName = fileName.substring(0, 50);
		}
	    fileDiv += "	<div class='fileItem file-item fl materialType_"+index+"' data-index='"+index+"' data-type='" + fileType + "'>";
	    fileDiv += "		<div class='item-img'>";
	    fileDiv += "			<img class='materialUrl_"+index+"' src=\"image/fileLogo/"+suffix+".jpg\" />";
	    fileDiv += "		</div>";
	    fileDiv += "		<div class='item-name'>";
	    fileDiv += "			<input type='text' placeholder='素材名称' class='materialName_"+index+"' value='"+fileName+"'/>";
	    fileDiv += "		</div>";
	    fileDiv += "		<div class='progress'>";
	    fileDiv += "			<div class='solidBar fileProgress_"+index+"' style='width:100%' ></div>";
	    fileDiv += "			<div class='progressTit fileProgressTit_"+index+"'>点击确定后开始上传...</div>";
	    fileDiv += "		</div>";
	    fileDiv += "		<i class=\"delFileItem close-img icon-remove\" title=\"删除\"></i>";
	    fileDiv += "	</div>";
	});
	$("#fileDivList").html(fileDiv);
}