/**
 * 获取窗口尺寸
 */
var winWidth = 0;
var winHeight = 0;
function getDimensions() {
	var sizeObj = new Object();
	// 获取窗口宽度
	if (window.innerWidth) {
		winWidth = window.innerWidth;
	}
	else if ((document.body) && (document.body.clientWidth)) {
		winWidth = document.body.clientWidth;
	}
	// 获取窗口高度
	if (window.innerHeight) {
		winHeight = window.innerHeight;
	}
	else if ((document.body) && (document.body.clientHeight)) {
		winHeight = document.body.clientHeight;
	}

	// 通过深入Document内部对body进行检测，获取窗口大小
	if (document.documentElement && document.documentElement.clientHeight && document.documentElement.clientWidth) {
		winHeight = document.documentElement.clientHeight;
		winWidth = document.documentElement.clientWidth;
	}
	// console.log(winWidth + " " + winHeight);
	sizeObj.width = winWidth;
	sizeObj.height = winHeight;
	return sizeObj;
}

window.onload = function() {
	$("#mainContent").css("height",$(window).height()-110 + "px");
}

/**
 * 调整窗口大小
 */
window.onresize = function() {
	// 除第一次加载外，之后的获取会影响实际高度，慎用
	getDimensions();

	$("#mainContent").css("height",$(window).height()-110 + "px");
};
