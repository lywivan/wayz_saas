
/**
*
	"^\d+$"　　//非负整数（正整数 + 0） 
	"^[0-9]*[1-9][0-9]*$"　　//正整数 
	"^((-\d+)|(0+))$"　　//非正整数（负整数 + 0） 
	"^-[0-9]*[1-9][0-9]*$"　　//负整数 
	"^-?\d+$"　　　　//整数 
	"^\d+(\.\d+)?$"　　//非负浮点数（正浮点数 + 0） 
	"^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$"　//正浮点数 
	"^((-\d+(\.\d+)?)|(0+(\.0+)?))$"　　//非正浮点数（负浮点数 + 0） 
	"^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$"　//负浮点数 
	"^(-?\d+)(\.\d+)?$"　　//浮点数 
	"^[A-Za-z]+$"　　//由26个英文字母组成的字符串 
	"^[A-Z]+$"　　//由26个英文字母的大写组成的字符串 
	"^[a-z]+$"　　//由26个英文字母的小写组成的字符串 
	"^[A-Za-z0-9]+$"　　//由数字和26个英文字母组成的字符串 
	"^\w+$"　　//由数字、26个英文字母或者下划线组成的字符串 
	"^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$"　　　　//email地址 
	"^[a-zA-z]+://(\w+(-\w+)*)(\.(\w+(-\w+)*))*(\?\S*)?$"　　//url 
	/^13\d{9}$/gi手机号正则表达式
	
	
	public static bool IsValidMobileNo(string MobileNo)
	{
	   const string regPattern = @"^(130|131|132|133|134|135|136|137|138|139)\d{8}$";
	   return Regex.IsMatch(MobileNo, regPattern);
	} 
 
	正则表达式--验证手机号码:13[0-9]{9}
	实现手机号前带86或是+86的情况:^((\+86)|(86))?(13)\d{9}$
	电话号码与手机号码同时验证:(^(\d{3,4}-)?\d{7,8})$|(13[0-9]{9}) 
	提取信息中的网络链接:(h|H)(r|R)(e|E)(f|F)  *=  *('|")?(\w|\\|\/|\.)+('|"|  *|>)?  
	提取信息中的邮件地址:\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*  
	提取信息中的图片链接:(s|S)(r|R)(c|C)  *=  *('|")?(\w|\\|\/|\.)+('|"|  *|>)?
	提取信息中的IP地址:(\d+)\.(\d+)\.(\d+)\.(\d+)    
	提取信息中的中国手机号码:(86)*0*13\d{9}    
	提取信息中的中国固定电话号码:(\(\d{3,4}\)|\d{3,4}-|\s)?\d{8}    
	提取信息中的中国电话号码（包括移动和固定电话）:(\(\d{3,4}\)|\d{3,4}-|\s)?\d{7,14}    
	提取信息中的中国邮政编码:[1-9]{1}(\d+){5}    
	提取信息中的中国身份证号码:\d{18}|\d{15}    
	提取信息中的整数：\d+    
	提取信息中的浮点数（即小数）：(-?\d*)\.?\d+    
	提取信息中的任何数字  ：(-?\d*)(\.\d+)?  
	提取信息中的中文字符串：[\u4e00-\u9fa5]*    
	提取信息中的双字节字符串  (汉字)：[^\x00-\xff]* 
*/

//验证邮箱
var emailRegex = /^(?:\w+\.?)*\w+@(?:\w+\.)*\w+$/;

//验证手机
var phoneRegex = /^(13[0-9]|14[0-9]|15[0-9]|17[0-9]|18[0-9])\d{8}$/;

//验证电话:区号+座机号码+分机号码
var telRegex = /^(0[0-9]{2,3}\-)?([2-9][0-9]{6,7})+(\-[0-9]{1,4})?$/;

//验证日期
var dateRegex=/^(\d{4})-(\d{2})-(\d{2})$/;

/**
 * 验证邮箱
 */
function checkEmail($email) {
	if ($email == "") {
		showAlert("邮箱不能为空!");
		return false;
	}
	if (!emailRegex.test($email)) {
		showAlert("邮箱不正确,请重新输入!");
		return false;
	}
	return true;
}
function isEmail($email) {
	if (!emailRegex.test($email)) {
		showAlert("邮箱不正确,请重新输入!");
		return false;
	}
	return true;
}

/**
 * 验证手机
 */
function checkPhone($phone) {
	if ($phone == "") {
		showAlert("手机号不能为空!");
		return false;
	}
	if (!phoneRegex.test($phone)) {
		showAlert("手机号不正确,请重新输入!");
		return false;
	}
	return true;
}

//验证手机号
function isMobile(value) {
	if (!phoneRegex.test(value)) {
		showAlert('手机号不正确,请重新输入!');
		return false;
	}
	return true;
}

/**
 * 获取手机验证码
 * 1注册用户;2找回密码;3绑定;4解绑
 */
function getPhoneVerifyCode($phone,$authCodeType) {
	// 获取手机号
	if (checkPhone($phone)) {
		// 调用接口
		var data = {
			"phone" : $phone,
			"authCodeType": $authCodeType
		}
		var response = callInterface("post", "general/sendSMSAuthCode", data, false);
		if(response.code==0){
			//获取成功
			//$("#btn-next").attr("disabled", false);
			showAlert('验证码已发送到手机,请查看!');
		}else{
			showErrorMsg(response.code, response.message);
		}
	}
	return false;
}

/**
 * 检查手机验证码
 */
function checkPhoneVerifyCode($phone,$authCode) {
	var isPass=checkPhone($phone);
	if (isPass) {
		if ($authCode == "") {
			showAlert("请输入手机验证码!");
			return false;
		}
		// 调用接口
		var data = {
			"phone" : $phone,
			"authCode": $authCode
		}
		var response = callInterface("post", "general/checkSMSVerifyCode", data, false);
		if(response.code==0){
			//验证成功
			isPass=true;
		}else{
			showErrorMsg(response.code, response.message);
			isPass=false;
		}
	}
	return isPass;
}

/**
 * 获取邮箱验证码
 * 1注册用户;2找回密码;3绑定;4解绑
 */
function getEmailVerifyCode($email,$authCodeType) {
	// 获取邮箱
	if (checkEmail($email)) {
		// 调用接口
		var data = {
			"email" : $email,
			"authCodeType": $authCodeType
		}
		var response = callInterface("post", "general/sendMailVerifyCode", data , false);
		if(response.code==0){
			//获取成功
			//$("#btn-next").attr("disabled", false);
			showAlert('验证码已发送到邮箱,请查看!');
		}else{
			showErrorMsg(response.code, response.message);
		}
	}
	return false;
}

/**
 * 检查邮箱验证码
 */
function checkEmailVerifyCode($email,$authCode) {
	var isPass=checkEmail($email);
	if (isPass) {
		if ($authCode == "") {
			showAlert("请输入邮箱验证码!");
			return false;
		}
		// 调用接口
		var data = {
			"email" : $email,
			"authCode": $authCode
		}
		var response = callInterface("post", "general/checkMailVerifyCode", data, false);
		if(response.code==0){
			//验证成功
			isPass=true;
		}else{
			showErrorMsg(response.code, response.message);
			isPass=false;
		}
	}
	return isPass;
}

/**
 * 变更手机号
 */
function telephoneChange($phone,$authCode) {
	var isPass=checkPhone($phone);
	if (isPass) {
		if ($authCode == "") {
			showAlert("请输入手机验证码!");
			return false;
		}
		// 调用接口
		var data = {
			"id" : $.trim($("#mId").val()),
			"telephone" : $phone,
			"smsAuthCode": $authCode
		}
		var response = callInterface("post", "customer/telephoneChange", data, false);
		if(response.code==0){
			//验证成功
			isPass=true;
		}else{
			showErrorMsg(response.code, response.message);
			isPass=false;
		}
	}
	return isPass;
}

/**
 * 变更邮箱
 */
function emailChange($email,$authCode) {
	var isPass=checkEmail($email);
	if (isPass) {
		if ($authCode == "") {
			showAlert("请输入邮箱验证码!");
			return false;
		}
		// 调用接口
		var data = {
			"id" : $.trim($("#mId").val()),
			"email" : $email,
			"emailAuthCode": $authCode
		}
		var response = callInterface("post", "customer/emailChange", data, false);
		if(response.code==0){
			//验证成功
			isPass=true;
		}else{
			showErrorMsg(response.code, response.message);
			isPass=false;
		}
	}
	return isPass;
}

/**
* 检查上传的Excel文件后缀名
*/
function checkExcelFileFormat(filename) {
	// 对文件名进行截取，以取得后缀名
	var temp = filename.split(".");
	// 获取截取的最后一个字符串，即为后缀名
	var last = temp[temp.length - 1];
	// 添加需要判断的后缀名类型
	var tp = "xls,xlsx,doc,docx,pdf";// "jpg,gif,bmp,JPG,GIF,BMP";
	// 返回符合条件的后缀名在字符串中的位置
	// JS大写转小写: toLowerCase()
	// JS小写转大写: toUpperCase()
	var rs = tp.indexOf(last.toLowerCase());
	// 如果返回的结果大于或等于0，说明包含允许上传的文件类型
	if (rs >= 0) {
		return true;
	} else {
		return false;
	}
}

/**
* 检查上传的压缩文件后缀名
*/
function checkRarZipFileFormat(filename) {
	// 对文件名进行截取，以取得后缀名
	var temp = filename.split(".");
	// 获取截取的最后一个字符串，即为后缀名
	var last = temp[temp.length - 1];
	// 添加需要判断的后缀名类型
	var tp = "rar,zip";
	// 返回符合条件的后缀名在字符串中的位置
	// JS大写转小写: toLowerCase()
	// JS小写转大写: toUpperCase()
	var rs = tp.indexOf(last.toLowerCase());
	// 如果返回的结果大于或等于0，说明包含允许上传的文件类型
	if (rs >= 0) {
		return true;
	} else {
		return false;
	}
}