/**
 * 选择角色
 * 
 * @param data:参数
 * @param domId:DOMId
 */
function queryRoleView(roleType, domId) {
	// 初始化
	var opts = '<option value="">请选择</option>';
	if (roleType != "") {
		// 调用接口
		var response = callInterface("post", "authority/queryRoleView", {'type':roleType}, false);
		if (response.code == 0) {
			if (response.data != null) {
				$(response.data).each(function(i, temp) {
					opts += '<option value="' + temp.id + '">' + temp.name + '</option>';
				});
			}
		}
		else {
			showErrorMsg(response.code, response.message);
		}
	}
	$("#" + domId).html(opts);
	updateLayuiForm();
}

/**
 * 选择公司获取客户
 * 
 * @param companyId:选择公司Id
 * @param customerId:客户Id
 */
function queryCustomerByCompanyIdView(companyId, customerId) {
	// 初始化
	var opts = '<option value="">请选择</option>';
	var $companyId = $.trim($("#" + companyId).val());
	if ($companyId != "") {
		// 组装参数
		var data = {};
		data['companyId'] = $companyId;
		// 调用接口
		var response = callInterface("post", "customer/queryCustomerView", data, false);
		if (response.code == 0) {
			if (response.data != null) {
				$(response.data).each(function(i, temp) {
					if(temp.id > 0){
						opts += '<option value="' + temp.id + '">' + temp.name + '</option>';
					}
				});
			}
		}
		else {
			showErrorMsg(response.code, response.message);
		}
	}
	$("#" + customerId).html(opts);
	updateLayuiForm();
}