/**	
 * 回车函数
 */
$(document).keyup(function(event) {
	if (event.keyCode == 13) {
		var v_id = $(event.target).attr('id');
		var v_txt = $(event.target).attr('placeholder');
		if (v_id == "positionName" && v_txt.indexOf("回车") >= 0) {
		}
		else {
			$("#queryButton").trigger("click");
		}
	}
});

/**
 * 获取项目根路径
 */
function getRootPath() {
	var roorPath = "";// "http://localhost:8080/project/";
	// 获取当前网址，如： http://localhost:8080/project/test/index.jsp
	var curWwwPath = window.document.location.href;
	// 获取主机地址之后的目录，如：project/test/index.jsp
	var pathName = window.document.location.pathname;
	var pos = curWwwPath.indexOf(pathName);
	// 获取主机地址，如： http://localhost:8080
	var localhostPath = curWwwPath.substring(0, pos);
	// 获取带"/"的项目名，如：/project
	var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);

	// 返回路径 http://localhost:8080/project
	roorPath = localhostPath + projectName + "/";
	return roorPath;
}

/**
 * 获取浏览器类型
 * 
 * @returns {String}
 */
function getBrowserType() {
	var userAgent = navigator.userAgent; // 取得浏览器的userAgent字符串
	// 判断是否Opera浏览器
	var isOpera = userAgent.indexOf("Opera") > -1;
	if (isOpera) {
		return "Opera"
	}
	// 判断是否Firefox浏览器
	if (userAgent.indexOf("Firefox") > -1) {
		return "FF";
	} 
	// 判断是否Chrome浏览器
	if (userAgent.indexOf("Chrome") > -1) {
		return "Chrome";
	}
	// 判断是否Safari浏览器
	if (userAgent.indexOf("Safari") > -1) {
		return "Safari";
	} 
	// 判断是否IE浏览器
	if (userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1
			&& !isOpera) {
		return "IE";
	}

}

/**
 * 随机数
 * 
 * @param Min
 * @param Max
 * @returns
 */
function GetRandomNum(Min, Max) {
	var Range = Max - Min;
	var Rand = Math.random();
	return (Min + Math.round(Rand * Range));
}

/**
 * 处理字符串为空
 */
function handleEmptyStr($value) {
	if (typeof ($value) == "undefined" || $value == null) {
		$value = "";
	}
	return $value;
}

function convertNull(str) {
	if (Utils.isNull(str)) {
		str = "";
	}
	return str;
}

/**
 * 参数说明： 根据长度截取先使用字符串，超长部分追加… str 对象字符串 len 目标字节长度 返回值： 处理结果字符串
 */
function cutString(str, len) {
	if (Utils.isUndefined(str) || Utils.isNull(str)) {
		return "";
	}
	// var len = 40;
	// length属性读出来的汉字长度为1
	if (str.length * 2 <= len) {
		return str;
	}
	var strlen = 0;
	var s = "";
	for (var i = 0; i < str.length; i++) {
		s = s + str.charAt(i);
		if (str.charCodeAt(i) > 128) {
			strlen = strlen + 2;
			if (strlen >= len) {
				return s.substring(0, s.length - 1) + "...";
			}
		}
		else {
			strlen = strlen + 1;
			if (strlen >= len) {
				return s.substring(0, s.length - 2) + "...";
			}
		}
	}
	return s;
}

// 转码
function encode(url) {
	return encodeURI(encodeURI(url));
}

// 验证方法
function verifyFunction(call) {
	if (typeof (call) == 'function') {
		return true;
	}
	return false;
}

/**
 * 数字格式化：数字、金额用逗号隔开 调用：formatNumber("12345.675910", 3) 返回12,345.676
 */
function formatNumber(num) {
	if (Utils.isNull(num)) {
		return num;
	}
	var parts = num.toString().split(".");
	parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	return parts.join(".");
}

/**
 * 还原函数
 */
function restoreNumber(s) {
	return parseFloat(s.replace(/[^\d\.-]/g, ""));
}

/**
 * 正整数类型的改变事件
 * 
 * @param t
 */
function integerChange(t) {
	var $this = $(t);
	$this.val($this.val().replace(/[^\d]/g, ''));
}

/**
 * 只输入数字和小数点
 */
function doubleChange(t) {
	var $this = $(t);
	$this.val($this.val().replace(/[^\d.]/g, ""));
	;// 清除“数字”和“.”以外的字符
	$this.val($this.val().replace(/^\./g, ""));// 验证第一个字符是数字而不是.
	$this.val($this.val().replace(/\.{2,}/g, "."));// 只保留第一个. 清除多余的.
	$this.val($this.val().replace(".", "$#$").replace(/\./g, "").replace("$#$", "."));
}

/**
 * 小数类型的改变事件
 * 
 * @param t
 */
function decimalChange(t) {
	var $this = $(t);
	$this.val($this.val().replace(/[^\d.]/g, ''));
	var thisV = $this.val();
	if (Utils.isNotNull(thisV)) {
		var dianIndex = thisV.indexOf('.');
		var lastDianIndex = thisV.lastIndexOf('.');

		if (dianIndex != -1 && (lastDianIndex != dianIndex)) {
			$this.val($this.val().replace(/[\d.]/g, ''));
		}
	}
}

/**
 * 判断字符串在数组中位置
 * 
 * @param array
 * @param str
 */
function strInArray_Pos(array, str) {
	if (array.length > 0) {
		for (var i = 0; i < array.length; i++) {
			if (array[i] == str) {
				return i
			}
		}
	}
	return -1;
}

/**
 * 检测数组中是否有重复的值
 * 
 * @param arr
 * @returns {Boolean}
 */
function isRepeat(arr) {
	var hash = {};
	for ( var i in arr) {
		if (hash[arr[i]]) {
			return true;
		}
		hash[arr[i]] = true;
	}
	return false;
}

/**
 * 选中全选复选框的逻辑
 * 
 * @param t 当前
 * @param name 单个复选框的名称
 */
function selectCbAll(t, name, ckList) {
	if (t.checked) {
		// 多个 加入
		setIdList(true, null, true, name, ckList);
		$("input[name='" + name + "']").prop("checked", true);
	}
	else {
		// 多个 移除
		setIdList(true, null, false, name, ckList);
		$("input[name='" + name + "']").prop("checked", false);
	}
}

/**
 * 选中单个复选框的逻辑
 * 
 * @param t 当前
 * @param allName 全选复选框的ID
 * @param name 单个复选框的名称
 */
function selectCbOne(t, allName, name, ckList) {
	// 全选
	var $selectAll = $("#" + allName);
	// 全部数量
	var allLen = $("input[name='" + name + "']").length;
	// 已选中数量
	var checkedLen = $("input[name='" + name + "']:checked").length;
	if (t.checked) {
		// 单个 加入
		setIdList(false, $(t).val(), true, name, ckList);
		if (allLen == checkedLen) {
			$selectAll.prop("checked", true);
		}
		else {
			$selectAll.prop("checked", false);
		}
	}
	else {
		// 单个 移除
		setIdList(false, $(t).val(), false, name, ckList);
		$selectAll.prop("checked", false);
	}
}

/**
 * 设置ID列表
 * 
 * @param isMore 是否多个 true:是
 * @param id 单个id
 * @param isAdd 是否加入列表 true:是
 * @param name 单个复选框的名称
 */
function setIdList(isMore, id, isAdd, name, ckList) {
	if (isMore) {
		$("input[name='" + name + "']").each(function() {
			var id = $(this).val();
			var index = strInArray_Pos(ckList, id);
			if (Utils.isNotNull(id)) {
				if (isAdd) {
					// 添加
					if (index == -1) {
						ckList.push(id);
					}
				}
				else {
					// 删除
					if (index > -1) {
						ckList.splice(index, 1);
					}
				}
			}
		});
	}
	else {
		var index = strInArray_Pos(ckList, id);
		if (Utils.isNotNull(id)) {
			if (isAdd) {
				// 添加
				if (index == -1) {
					ckList.push(id);
				}
			}
			else {
				// 删除
				if (index > -1) {
					ckList.splice(index, 1);
				}
			}
		}
	}
}

/**
 * 处理分页后的复选框选中逻辑
 * 
 * @param allName 全选复选框的ID
 * @param name 单个复选框的名称
 */
function handlePageCb(allName, name) {
	var $selectAll = $("#" + allName);
	// 全部数量
	var allLen = $("input[name='" + name + "']").length;
	// 已选中数量
	var checkedLen = $("input[name='" + name + "']:checked").length;
	if (allLen == checkedLen && checkedLen > 0) {
		$selectAll.prop("checked", true);
	}
	else {
		$selectAll.prop("checked", false);
	}
}

/**
 * 清空
 * 
 * @param allName 全选复选框的ID
 * @param name 单个复选框的名称 clearPageCb('selectListAll','rowCheckbox');
 */
function clearPageCb(allName, name) {
	var $selectAll = $("#" + allName);
	$selectAll.prop("checked", false);
	// 已选中数量
	var checkedLen = $("input[name='" + name + "']:checked").length;
	if (checkedLen > 0) {
		$("input[name='" + name + "']:checked").each(function(i) {
			// if ($(this).is(':checked') == true) {
			$(this).prop("checked", false);
			// }
		});
	}
}

/**
 * 当前页码
 */
var currPage = 1;

/**
 * 设置分页参数
 * 
 * @param data 参数集合
 * @param pageIndex 当前页
 * @param pageSize 每页显示数量
 */
function setPageIndex(data, pageIndex, pageSize) {
	// 当前页
	if (Utils.isUndefined(pageIndex) || Utils.isNull(pageIndex)) {
		pageIndex = 1;
	}

	// 每页显示数量
	if (Utils.isUndefined(pageSize) || Utils.isNull(pageSize)) {
		pageSize = Global.PAGE_SISE;
	}

	data['startIndex'] = (pageIndex - 1) * pageSize;
	data['pageSize'] = pageSize;
	currPage = pageIndex;
}

/**
 * 获取当前登陆用户公司标示
 * 
 * @returns
 */
function getCompanyId(){
	return $.session.get("company_id");
}

/**
 * 获取当前登陆用户角色标示
 * 
 * @returns
 */
function getRoleId(){
	return $.session.get("role_id");
}

/**
 * 获取合适的宽高
 */
var mult = 1;
function getAddFitWidthHeight(width, height, m) {
	//console.log(m+"     "+width +"     "+ height  +  "width / m = " + width / m +  " height / m = " + height / m)
	if (width >= height) {
		if (width / m < 192) {
			m = m - 1;
			getAddFitWidthHeight(width, height, m);
		}else{
			//console.log("宽屏："+m);
			mult = m;
			return;
		}
	}else{
		if (height / m < 192) {
			m = m - 1;
			getAddFitWidthHeight(width, height, m);
		}else{
			//console.log("竖屏："+m);
			mult = m;
			return;
		}
	}
}

function getViewFitWidthHeight(width, height, m) {
	if (width >= height) {
		if (width / m < 96) {
			m = m - 1;
			getViewFitWidthHeight(width, height, m);
		}else{
			mult = m;
			return;
		}
	}else{
		if (height / m < 96) {
			m = m - 1;
			getViewFitWidthHeight(width, height, m);
		}else{
			mult = m;
			return;
		}
	}
}