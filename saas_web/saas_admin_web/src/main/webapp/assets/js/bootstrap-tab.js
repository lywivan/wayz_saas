/**
 * 添加激活Tab页
 * 
 * @param id:tab标识
 * @param title:tab标题
 * @param url:tab链接
 * @param isClosed:tab是否关闭
 * @param parentAddtabs:是否由主页面触发
 */
var addTabs = function(id, title, url, isClosed, parentAddtabs) {
	if (url == null || url == "null") {
		url = "";
	}
	id = "tab_" + id;
	var content = false;
	var isTabExist = false;
	// 默认由主页面触发
	if (typeof (parentAddtabs) == "undefined" || parentAddtabs == null) {
		parentAddtabs = true;
	}
	if (parentAddtabs) {
		$(".active").removeClass("active");
		isTabExist = $("#" + id)[0];
	} else {
		parent.$(".active").removeClass("active");
		isTabExist = parent.$("#" + id)[0];
	}
	
	// 如果TAB不存在，创建一个新的TAB
	if (isTabExist) {
		//关闭已打开的TAB,重新创建
		closeTab(id);
	}
	
	// 获取窗口尺寸
	//var winHeight = getDimensions().height;
	
	// 固定TAB中IFRAME高度
	var height = $(window.parent.document).find(".mainIframe").eq(0).attr('height');
	if (typeof (height) == "undefined" || height == null || height == 0) {
		// 获取父级iframe窗体固定高度
		height = winHeight - 125;
	} else {
	}
	//console.log("winHeight=" + winHeight + "  height=" + $(window.parent.document).find(".mainIframe").eq(0).attr('height'));
	
	// 创建新TAB的title
	title = '<li role="presentation" id="tab_' + id + '"><a href="#' + id
			+ '" aria-controls="' + id + '" role="tab" data-toggle="tab">'
			+ title;
	
	// 是否允许关闭
	if (isClosed) {
		title += ' <i class="icon-remove" tabclose="' + id + '"></i>';
	}
	title += '</a></li>';
	
	// 是否指定TAB内容
	if (content) {
		content = '<div role="tabpanel" class="tab-pane" id="' + id + '">'
				+ content + '</div>';
	} else {// 没有内容，使用IFRAME打开链接
		content = '<div role="tabpanel" class="tab-pane" id="'
			+ id + '"><iframe style="background-color:#ffffff;" class="mainIframe" src="' + url + '" width="100%" height="'
			+ height + '" frameborder="no" border="0" marginwidth="0" marginheight="0" scrolling="yes"></iframe></div>';
	}

	// 加入TABS
	if(parentAddtabs){
		$(".nav-tabs").append(title);
		$(".tab-content").append(content);
	}else{
		parent.$(".nav-tabs").append(title);
		parent.$(".tab-content").append(content);
	}
	
	// 激活TAB
	if(parentAddtabs){
		$("#tab_" + id).addClass('active');
		$("#" + id).addClass("active");
	}else{
		parent.$("#tab_" + id).addClass('active');
		parent.$("#" + id).addClass("active");
	}
	//console.log("isTabExist = " + isTabExist + " parentAddtabs = " + parentAddtabs)
};

/**
 * 关闭Tab
 */
var closeTab = function(id) {
	//console.log(typeof ($("li.active").attr('id')));
	// 如果关闭的是当前激活的TAB，激活他的前一个TAB
	if (typeof ($("li.active").attr('id')) != "undefined"){
		if ($("li.active").attr('id') == "tab_" + id) {
			$("#tab_" + id).prev().addClass('active');
			$("#" + id).prev().addClass('active');
		}
		// 关闭TAB
		$("#tab_" + id).remove();
		$("#" + id).remove();
	}else{
		var $wpd = $(window.parent.document);
		if ($wpd.find("li.active").attr('id') == "tab_" + id) {
			$wpd.find("#tab_" + id).prev().addClass('active');
			$wpd.find("#" + id).prev().addClass('active');
		}
		$wpd.find("#tab_" + id).remove();
		$wpd.find("#" + id).remove();
	}
};

$(function() {
	$(".nav-tabs").on("click", "[tabclose]", function(e) {
		id = $(this).attr("tabclose");
		closeTab(id);
	});
});