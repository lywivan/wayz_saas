<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../common_taglibs.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>分辨率管理</title>
<script type="text/javascript" src="js/content/poster_resolution_management.js?v=<%=System.currentTimeMillis()%>"></script>
</head>
<body>
	<div class="page-content">
		<div class="row">
			<div class="col-xs-12">
				<div class="step-main">
					<ul class="search-top layui-form">
						<li class="left-td"> 
							<span class="search-tit">宽：</span>
							<div class="search-main">
								<input id="width" type="text" class="uniform input-small" onkeyup="integerChange(this);" placeholder="宽" />
							</div></li>
						<li class="left-td"> 
							<span class="search-tit">高：</span>
							<div class="search-main">
								<input id="height" type="text" class="uniform input-small" onkeyup="integerChange(this);" placeholder="高" />
							</div></li>
					</ul>
					<div class="clear"></div>
					<div class="search-button">
						<c:if test="${fn:contains($rightList,'saas/poster/queryPosterResolution')}">
							<button id="queryButton" type="button" class="search-fn-btn">
								<i class="icon-search"></i> 查询
							</button>
						</c:if>
					</div>
				</div>
				<div class="space-4"></div>
				<div id="toolbarDiv">
					<c:if test="${fn:contains($rightList,'saas/poster/createPosterResolution')}">
						<button id="createButton" type="button" class="btn btn-step">
							<i class="icon-plus-sign-alt"></i> 创建
						</button>
					</c:if>
					<c:if test="${fn:contains($rightList,'saas/poster/deletePosterResolution')}">
						<input type="hidden" id="delete" value="1" />
					</c:if>
				</div>
				<div class="step-main">
					<div class="table-responsive">
						<table id="dataTable" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th class="center">分辨率</th>
									<th class="center">宽(pix)</th>
									<th class="center">高(pix)</th>
									<th width="160px" class="center">创建时间</th>
									<th width="120px" class="center">操作</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
					<div class="page-new clear">
						<div id="pageDiv" align="right" class="pull-right"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="createDialog" class="modal fade">
		<div class="window-box modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						创建分辨率
					</div>
				</div>

				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="addForm" class="form-horizontal" role="form">
									<table class="table detail-tab layui-form">
										<tr>
											<td width="100px" align="right"><span class="required_style">*</span>宽(pix)：</td>
											<td><input id="cWidth" type="text" class="col-sm-6" onkeyup="integerChange(this);" placeholder="宽" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>高(pix)：</td>
											<td><input id="cHeight" type="text" class="col-sm-6" onkeyup="integerChange(this);" placeholder="高" /></td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmCreateButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn btn-fn btn-delect" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
