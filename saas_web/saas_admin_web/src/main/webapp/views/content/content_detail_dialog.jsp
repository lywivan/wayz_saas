<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<meta name="referrer" content="never">
<input id="contentId" type="hidden" />
<script type="text/javascript" src="js/content/content_detail_dialog.js?v=<%=System.currentTimeMillis()%>"></script>
<div id="materialDetailDialog" class="modal fade">
	<div class="window-box modal-dialog">
		<div class="modal-content">
			<div class="modal-header no-padding">
				<div class="table-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
						<span class="box-close">×</span>
					</button>
					【<span id="materialName"></span>】内容详情
				</div>
			</div>

			<div class="modal-body-max no-padding">
				<div class="page-content">
					<div class="row">
						<div class="col-xs-12">
							<div class="widget-box">
								<div class="box-main">
									<h4 class="step-tit">
										<i></i>内容预览
									</h4>
									<div class="widget-main">
										<div style="width: 100%; height: 300px; position: relative;">
											<div id="materialDetailCanvasZone" style="position: absolute; width:100%;height:100%;top:0px;left:0px"></div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="widget-box">
								<div class="box-main">
									<h4 class="step-tit">
										<i></i>内容信息
									</h4>
									<div class="widget-main">
										<table class="table detail-tab">
											<tr>
												<td width="100px" align="right">标题：</td>
												<td><span id="vTitle"></span></td>
												<td width="100px" align="right">内容分类：</td>
												<td><span id="vCategoryName"></span></td>
											</tr>
											<tr>
												<td align="right">价格：</td>
												<td><span id="vPrice"></span></td>
												<td align="right">渠道：</td>
												<td><span id="vChannelName"></span></td>
											</tr>
											<tr>
												<td align="right">文件类型：</td>
												<td><span id="vTypeName"></span></td>
												<td align="right">分辨率：</td>
												<td><span id="vResolutionName"></span></td>
											</tr>
											<tr>
												<td align="right">文件大小：</td>
												<td><span id="vFileSize"></span></td>
												<td align="right">时长(秒)：</td>
												<td><span id="vDuration"></span></td>
											</tr>
											<tr>
												<td align="right">内容简介：</td>
												<td colspan="3"><span id="vDescription"></span></td>
											</tr>
											<tr>
												<td align="right">审核状态：</td>
												<td><span id="vAuditStatus"></span></td>
												<td align="right">更新时间：</td>
												<td ><span id="vCreateTime"></span></td>
											</tr>
											<tr>
												<td class="vRemark" align="right">驳回原因：</td>
												<td class="vRemark" colspan="3"><span id="vRemark"></span></td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="modal-footer no-margin-top">
				<button type="button" class="btn btn-fn btn-delect" data-dismiss="modal">取消</button>
			</div>
		</div>
	</div>
</div>

