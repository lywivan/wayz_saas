<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../common_taglibs.jsp"%>
<%@ include file="content_detail_dialog.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>内容审核管理</title>
<script type="text/javascript" src="js/content/content_audit_management.js?v=<%=System.currentTimeMillis()%>"></script>
</head>
<body>
	<div class="page-content">
		<div class="row">
			<div class="col-xs-12">
				<div class="step-main">
					<ul class="search-main-top">
						<li>
							<span class="search-tit">渠道：</span>
							<a href="javascript:void(0);" class="on channelId" data-v="">不限</a>
							<c:forEach var="entry" items="${contentChannelList}">
								<a href="javascript:void(0);" class="channelId" data-v="${entry.id}">${entry.name}</a>
							</c:forEach>
						</li>
						<li>
							<span class="search-tit">格式：</span>
							<a href="javascript:void(0);" class="on type" data-v="">不限</a>
							<c:forEach var="entry" items="${fileTypeList}">
								<c:if test="${entry.id == 1 || entry.id == 2 }">
									<a href="javascript:void(0);" class="type" data-v="${entry.id}">${entry.name}</a>
								</c:if>
							</c:forEach>
						</li>
					</ul>
					<div class="clear"></div>
				</div>
				<div id="toolbarDiv"> </div>
				<div class="step-main">
					<div class="col-xs-12" id="materialDiv"></div>
					<div class="page-new clear">
						<div id="pageDiv" align="right" class="pull-right"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="contentAuditRejectDialog" class="modal fade">
		<div class="window-box modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						内容驳回
					</div>
				</div>

				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="contentAuditRejectForm" class="form-horizontal" role="form">
									<input type="hidden" id="mId" name="mId" />
									<table class="table details-tab">
										<tr>
											<td style="width: 100px;" align="right"><span class="required_style">*</span>驳回原因：</td>
											<td><textarea id="remark" rows="3" class="col-sm-10" placeholder="此处录入驳回原因"></textarea></td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="auditRejectButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn btn-fn btn-delect" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>