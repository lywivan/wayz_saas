<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../common_taglibs.jsp"%>
<%@ include file="content_detail_dialog.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>内容管理</title>
<script src="plugin/alioss/aliyun-oss-sdk-4.4.4.min.js"></script>
<script type="text/javascript" src="js/content/content_management.js?v=<%=System.currentTimeMillis()%>"></script>
</head>
<body>
	<div class="page-content">
		<div class="row">
			<div class="col-xs-12">
				<div class="step-main">
					<ul class="search-main-top">
						<li>
							<span class="search-tit">内容渠道：</span>
							<a href="javascript:void(0);" class="on channelId" data-v="">不限</a>
							<c:forEach var="entry" items="${contentChannelList}">
								<a href="javascript:void(0);" class="channelId" data-v="${entry.id}">${entry.name}</a>
							</c:forEach>
						</li>
						<li>
							<span class="search-tit">内容分类：</span>
							<a href="javascript:void(0);" class="on categoryId" data-v="">不限</a>
							<c:forEach var="entry" items="${contentCategoryList}">
								<a href="javascript:void(0);" class="categoryId" data-v="${entry.id}">${entry.name}</a>
							</c:forEach>
						</li>
						<li>
							<span class="search-tit">文件类型：</span>
							<a href="javascript:void(0);" class="on type" data-v="">不限</a>
							<c:forEach var="entry" items="${fileTypeList}">
								<c:if test="${entry.id == 1 || entry.id == 2 }">
									<a href="javascript:void(0);" class="type" data-v="${entry.id}">${entry.name}</a>
								</c:if>
							</c:forEach>
						</li>
						<li>
							<span class="search-tit">价格：</span>
							<a href="javascript:void(0);" class="on price" data-v="">不限</a>
							<a href="javascript:void(0);" class="price" data-v="0-0">免费</a>
							<a href="javascript:void(0);" class="price" data-v="0-10">0-10</a>
							<a href="javascript:void(0);" class="price" data-v="10-20">10-20</a>
							<a href="javascript:void(0);" class="price" data-v="20-50">20-50</a>
							<a href="javascript:void(0);" class="price" data-v="50-100">50-100</a>
							<a href="javascript:void(0);" class="price" data-v="100-9999999">100以上</a>
						</li>
						<li>
							<span class="search-tit">状态：</span>
							<a href="javascript:void(0);" class="on status" data-v="">不限</a>
							<c:forEach var="entry" items="${auditStatusList}">
								<a href="javascript:void(0);" class="status" data-v="${entry.id}">${entry.name}</a>
							</c:forEach>
						</li>
					</ul>
					<div class="clear"></div>
				</div>
				<div id="toolbarDiv">
					<c:if test="${fn:contains($rightList,'saas/shop/deleteContent')}">
						<input type="hidden" id="delete" value="1" />
					</c:if>
					<c:if test="${fn:contains($rightList,'saas/shop/modifyContent')}">
						<input type="hidden" id="modify" value="1" />
					</c:if>
					<c:if test="${fn:contains($rightList,'saas/shop/createContent')}">
						<button id="createButton" type="button" class="btn btn-step">
							<i class="icon-reply"></i> 上传
						</button>
					</c:if>
				</div>
				<div class="step-main">
					<div class="col-xs-12" id="materialDiv"></div>
					<div class="page-new clear">
						<div id="pageDiv" align="right" class="pull-right"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="createMaterialDialog" class="modal fade">
		<div class="window-box modal-dialog-max">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						上传内容
					</div>
				</div>

				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="createMaterialForm" method="post" enctype="multipart/form-data"
									class="form-horizontal" role="form">
									<input type="hidden" id="name">
									<input type="hidden" id="matUrl">
									<input type="hidden" id="matPreUrl">
                                    <input type="hidden" id="filemd5" />
                                    <input type="hidden" id="fileSize" />
                                    <input type="hidden" id="duration" />
                                    <input type="hidden" id="width" />
                                    <input type="hidden" id="height"/>
									<table class="table detail-tab layui-form">
										<tr>
											<td width="55%">
												<table>
													<tr>
														<td width="100px" align="right"><span class="required_style">*</span>素材类型：</td>
														<td>
															<div class="layui-form-item">
															    <div class="layui-input-block" style="width: 264px">
																   <select id="cType" lay-filter="cType" lay-verify="required" lay-search>
																		<c:forEach var="entry" items="${fileTypeList }">
																			<c:if test="${entry.id == 1 || entry.id == 2}">
																				<option value="${entry.id}">${entry.name}</option>
																			</c:if>
																		</c:forEach>
																	</select>
																</div> 	
															</div>
														</td>
													</tr>
													<tr class="imagePreItem" style="display:none;">
														<td align="right">
															<span class="required_style">*</span>
															素材预览图：
														</td>
														<td align="left" class="requirements" style="vertical-align: top; position: relative;">
															<input type="file" id="cMaterialPreviewFile" onchange="selectFileToOss(this.id,'fileDivListPre','cType',1);"
																style="position: absolute; clip: rect(0, 0, 0, 0);" />
															<button type="button" class="btn btn-step btn-step-c1"
																onclick="doContTriggerClick('cMaterialPreviewFile');">选择预览图</button>
															<span>用于封面图展示，建议尺寸：320*180  大小 ≤ 0.5M</span>
														</td>
													</tr>
													<tr class="imagePreItem" style="display:none;">
														<td align="right" style='width: 82px'></td>
														<td align="left" style="vertical-align: top;">
															<div id="fileDivListPre"></div>
														</td>
													</tr>
													<tr>
														<td align="right">
															<span class="required_style">*</span>
															素材文件：
														</td>
														<td align="left" class="requirements" style="vertical-align: top; position: relative;">
															<input type="file" id="cMaterialFile" onchange="selectFileToOss(this.id,'fileDivList','cType',0);"
																style="position: absolute; clip: rect(0, 0, 0, 0);" />
															<button type="button" class="btn btn-step btn-step-c1"
																onclick="doContTriggerClick('cMaterialFile');">选择文件</button>
															<span></span>
														</td>
													</tr>
													<tr>
														<td align="right" style='width: 82px'></td>
														<td align="left" style="vertical-align: top;">
															<div id="fileDivList"></div>
														</td>
													</tr>
													<tr>
														<td align="right">素材要求：</td>
														<td align="left">
															<!-- <span class="viewRequ"><a href="javascript:void(0);">查看</a></span> -->
															<table class="table table-striped table-bordered" style="border: 1px solid #ccc !important;">
																<thead>
																	<tr>
																		<th class="center" width="60px">类型</th>
																		<th class="center">文件格式</th>
																		<th class="center">大小</th>
																		<th class="center">备注</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td align='center'>图片</td>
																		<td>jpg,jpeg,png,bmp</td>
																		<td align='center'>≤10M</td>
																		<td>颜色模式：RGB模式</td>
																	</tr>
																	<tr>
																		<td align='center'>视频</td>
																		<td>mp4</td>
																		<td align='center'>≤1024M</td>
																		<td>编码格式：h264</td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
												</table>
											</td>
											<td width="45%" style="vertical-align: top;">
												<table>
													<tr>
														<td width="100px" align="right"><span class="required_style">*</span>标题：</td>
														<td><input id="cTitle" type="text" style="width: 264px" placeholder="标题" /></td>
													</tr>
													<tr>
														<td width="100px" align="right"><span class="required_style">*</span>渠道：</td>
														<td>
															<div class="layui-form-item">
															    <div class="layui-input-block" style="width: 264px">
																   <select id="cChannelId" lay-verify="required" lay-search>
																		<c:forEach var="entry" items="${contentChannelList }">
																			<option value="${entry.id}">${entry.name}</option>
																		</c:forEach>
																	</select>
																</div> 	
															</div>
														</td>
													</tr>
													<tr>
														<td width="100px" align="right"><span class="required_style">*</span>分类：</td>
														<td>
															<div class="layui-form-item">
															    <div class="layui-input-block" style="width: 264px">
																   <select id="cCategoryId" lay-verify="required" lay-search>
																		<c:forEach var="entry" items="${contentCategoryList }">
																			<option value="${entry.id}">${entry.name}</option>
																		</c:forEach>
																	</select>
																</div> 	
															</div>
														</td>
													</tr>
													<tr>
														<td width="100px" align="right"><span class="required_style">*</span>价格：</td>
														<td><input id="cPrice" type="text" style="width: 264px" placeholder="价格(免费请输入 0)" /></td>
													</tr>
													<tr>
														<td width="100px" align="right"><span class="required_style">*</span>内容简介：</td>
														<td align="left"><textarea id="cDescription" style="width: 264px;height:50%" placeholder="内容简介"></textarea></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmCreateMaterialButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn btn-fn btn-delect" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>
	
	<div id="modifyMaterialDialog" class="modal fade">
		<div class="window-box modal-dialog-max">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						编辑内容
					</div>
				</div>

				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="modifyMaterialForm" method="post" enctype="multipart/form-data"
									class="form-horizontal" role="form">
									<input type="hidden" id="mId">
									<input type="hidden" id="mAuditStatus">
									<table class="table detail-tab layui-form">
										<tr>
											<td width="50%">
												<table>
													<tr>
														<td align="center" colspan="4">
															<div>
																<img id="mMaterialFile" src="" style="width:60%;height:60%">
															</div>
														</td>
													</tr>
													<tr>
														<td align="right">渠道：</td>
														<td align="left"><span id="mChannelName"></span></td>
													</tr>
													<tr>
														<td align="right">文件类型：</td>
														<td align="left"><span id="mTypeName"></span></td>
													</tr>
													<tr>
														<td align="right">文件大小：</td>
														<td align="left"><span id="mFileSize"></span></td>
													</tr>
													<tr>
														<td align="right">分辨率：</td>
														<td align="left"><span id="mResolutionName"></span></td>
													</tr>
													<tr>
														<td align="right">时长(秒)：</td>
														<td align="left" colspan="3"><span id="mDuration"></span></td>
													</tr>
												</table>
											</td>
											<td width="50%" style="vertical-align: top;">
												<table>
													<tr>
														<td align="right"><span class="required_style">*</span>标题：</td>
														<td align="left"><input id="mTitle" type="text" style="width: 264px" placeholder="标题" /></td>
													</tr>
													<tr>
														<td align="right"><span class="required_style">*</span>分类：</td>
														<td align="left">
															<div class="layui-form-item">
															    <div class="layui-input-block" style="width: 264px">
																   <select id="mCategoryId" lay-verify="required" lay-search>
																		<c:forEach var="entry" items="${contentCategoryList }">
																			<option value="${entry.id}">${entry.name}</option>
																		</c:forEach>
																	</select>
																</div> 	
															</div>
														</td>
													</tr>
													<tr>
														<td align="right"><span class="required_style">*</span>价格：</td>
														<td align="left"><input id="mPrice" type="text" style="width: 264px" onkeyup="doubleChange(this);" placeholder="价格(免费请输入 0)" /></td>
													</tr>
													<tr>
														<td align="right"><span class="required_style">*</span>内容简介：</td>
														<td align="left"><textarea id="mDescription" style="width: 264px;height:50%" placeholder="内容简介"></textarea></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmModifyMaterialButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn btn-fn btn-delect" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>
	<video id="myVideo" muted style="display:none;" oncanplaythrough="getVideoDes()"></video>
</body>
</html>