<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../common_taglibs.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>行业类别管理</title>
<script type="text/javascript" src="js/content/poster_category_management.js?v=<%=System.currentTimeMillis()%>"></script>
</head>
<body>
	<div class="page-content">
		<div class="row">
			<div class="col-xs-12">
				<div class="space-4"></div>
				<div id="toolbarDiv">
					<%-- <c:if test="${fn:contains($rightList,'saas/poster/queryPosterCategory')}">
							<button id="queryButton" type="button" class="search-fn-btn">
								<i class="icon-search"></i> 查询
							</button>
					</c:if> --%>
					<c:if test="${fn:contains($rightList,'saas/poster/createPosterCategory')}">
						<button id="createButton" type="button" class="btn btn-step">
							<i class="icon-plus-sign-alt"></i> 创建
						</button>
					</c:if>
					<c:if test="${fn:contains($rightList,'saas/poster/modifyPosterCategory')}">
						<input type="hidden" id="modify" value="1" />
					</c:if>
					<c:if test="${fn:contains($rightList,'saas/poster/deletePosterCategory')}">
						<input type="hidden" id="delete" value="1" />
					</c:if>
				</div>
				<div class="step-main">
					<div class="table-responsive">
						<table id="dataTable" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th width="100px" class="center">序号</th>
									<th class="center">行业类别</th>
									<th class="center">描述</th>
									<th width="120px" class="center">操作</th>
								</tr>
							</thead>
							<tbody>

							</tbody>
						</table>
					</div>
					<!-- <div id="pageDiv" align="right" class="pull-right"></div> -->
					<div class="space-8"></div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="createCategoryDialog" class="modal fade">
		<div class="window-box modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						创建行业类别
					</div>
				</div>

				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="addCategoryForm" class="form-horizontal" role="form">
									<table class="table detail-tab">
										<thead>
											<tr>
												<td width="100px" nowrap="nowrap" align="right"><span class="required_style">*</span>行业类别：</td>
												<td align="left"><input id="cName" type="text" class="col-sm-6" placeholder="行业类别" /></td>
											</tr>
										</thead>
										<tr>
											<td align="right">描述：</td>
											<td align="left"><textarea id="cDescription" style="width:50%;height:50%;padding: 5px 4px;" placeholder="描述"></textarea></td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmCreateButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn btn-fn btn-delect" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>

	<div id="modifyCategoryDialog" class="modal fade">
		<div class="window-box modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						编辑行业类别
					</div>
				</div>

				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="modifyCategoryForm" class="form-horizontal" role="form">
									<input type="hidden" id="mId" name="mId" />
									<table class="table detail-tab">
										<thead>
											<tr>
												<td width="100px" nowrap="nowrap" align="right"><span class="required_style">*</span>行业类别：</td>
												<td align="left"><input id="mName" type="text" class="col-sm-6" placeholder="行业类别" /></td>
											</tr>
										</thead>
										<tr>
											<td align="right">描述：</td>
											<td align="left"><textarea id="mDescription" style="width:50%;height:50%;padding:5px 4px;" placeholder="描述"></textarea></td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmModifyButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn btn-fn btn-delect" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
