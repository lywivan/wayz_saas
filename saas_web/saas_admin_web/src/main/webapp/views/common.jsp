<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!-- basic styles -->
<link href="assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="assets/css/font-awesome.min.css" rel="stylesheet" />
 <!--[if IE]>
	<link rel="stylesheet" href="assets/css/font-awesome-ie7.min.css" />
<![endif]-->

<!-- ace styles -->
<link rel="stylesheet" href="assets/css/ace.min.css" />
<!-- <link rel="stylesheet" href="assets/css/ace-rtl.min.css" /> -->
<link rel="stylesheet" href="assets/css/ace-skins.min.css" />

<!--[if lte IE 8]>
	<link rel="stylesheet" href="assets/css/ace-ie.min.css" />
<![endif]-->


<!--[if lt IE 9]>
	<script src="assets/js/html5shiv.js"></script>
<![endif]-->

<script src="assets/js/ace-extra.min.js"></script>

<!-- basic scripts -->
<script type="text/javascript" src="js/common/jquery-1.10.2.js"></script>

<!--[if lte IE 8]>
  <script src="assets/js/excanvas.min.js"></script>
<![endif]-->
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/typeahead-bs2.min.js"></script>
<script src="assets/js/jquery-ui-1.10.3.custom.min.js"></script>
<link rel="stylesheet" href="assets/css/jquery-ui-1.10.3.full.min.css" />
<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
<script src="assets/js/jquery.slimscroll.min.js"></script>

<script src="assets/js/jquery.tips.js"></script>
<script src="assets/js/jquery.cookie.js"></script>
<script src="assets/js/jquerysession.js"></script>
<script src="assets/js/jquery.md5.js"></script>
<!-- <script src="assets/js/jquery.colorbox-min.js"></script> -->

<!-- ace settings handler -->
<script src="assets/js/ace-elements.min.js"></script>
<script src="assets/js/ace.min.js"></script>

<!-- layui组件 -->
<script src="plugin/laydate/laydate.js"></script>
<link rel="stylesheet" type="text/css" href="plugin/layui/css/layui.css" media="all"/>
<script type="text/javascript" src="plugin/layui/layui.js"></script>

<!-- 自定义 -->
<script type="text/javascript" src="js/common/showLayer.js?v=<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="js/common/jquery.call.interface.js"></script>
<script type="text/javascript" src="js/common/global.js"></script>
<script type="text/javascript" src="js/common/common.js"></script>
<script type="text/javascript" src="js/common/utils.js"></script>
<script type="text/javascript" src="js/common/fileCommon.js"></script>
<script type="text/javascript" src="js/common/dateTimeCommon.js"></script>
<script type="text/javascript" src="js/common/getDimensions.js"></script>
<script type="text/javascript" src="js/common/province.city.district.area.js"></script>
<script type="text/javascript" src="js/common/fileUpload.js"></script>
<script type="text/javascript" src="js/common/verifydata.js"></script>
<script type="text/javascript" src="js/common/verifydata.js"></script>
<script type="text/javascript" src="plugin/jsMd5/spark-md5.min.js"></script>

<!-- 瀑布流照片墙 -->
<link href="plugin/waterfall/waterfall.css" rel="stylesheet" >
<script src="plugin/waterfall/responsive_waterfall.js"></script>

<!--[if lt IE 9]>
	<script src="assets/js/html5shiv.js"></script>
	<script src="assets/js/respond.min.js"></script>
<![endif]-->

<link rel="stylesheet" href="css/frame-user.css" />
<style type="text/css">
.error {
	color: red;
}

.fontstyle_14 {
	font-size: 14px;
	font-weight: bold;
	color: #ffffff
}

.statusfontstyle_14 {
	font-size: 14px;
	font-weight: bold;
	color: red
}

.required_style {
	color: red;
}

.upload_table {
	border-collapse: collapse;
}

.upload_table td {
	border: solid 1px #ddd;
}

.show_table {
	border: 0px solid #fff;
}

.show_table td {
	border: 0px solid #fff;
}
</style>