<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../common_taglibs.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>版本管理</title>
<link rel="stylesheet" href="css/zTreeStyle/zTreeStyle.css" type="text/css">
<script type="text/javascript" src="plugin/ztree/jquery.ztree.core.js"></script>
<script type="text/javascript" src="plugin/ztree/jquery.ztree.excheck.js"></script>
<script type="text/javascript" src="js/config/version_management.js?v=<%=System.currentTimeMillis()%>"></script>
</head>
<body>
	<div class="page-content">
		<div class="row">
			<div class="col-xs-12">
				<div class="step-main">
					<ul class="search-top layui-form">
						<li class="left-td"><span class="search-tit">版本名称：</span>
							<div class="search-main">
								<input autocomplete="off" id="name" type="text" class="uniform input-large" placeholder="版本名称" />
							</div>
						</li>
					</ul>
					<div class="clear"></div>
					<div class="search-button">
						<c:if test="${fn:contains($rightList,'saas/version/queryVersion')}">
							<button id="queryButton" type="button" class="search-fn-btn">
								<i class="icon-search"></i> 查询
							</button>
						</c:if>
					</div>
				</div>
				<div id="toolbarDiv">
					<c:if test="${fn:contains($rightList,'saas/version/createVersion')}">
						<button id="createButton" type="button" class="btn btn-step">
							<i class="icon-credit-card"></i> 创建
						</button>
					</c:if>
					<c:if test="${fn:contains($rightList,'saas/version/modifyVersion')}">
						<input id="modify" type="hidden" value="1" />
					</c:if>
					<c:if test="${fn:contains($rightList,'saas/version/authorityVersion')}">
						<input id="setVersionRight" type="hidden" value="1" />
					</c:if>
					<c:if test="${fn:contains($rightList,'saas/version/deleteVersion')}">
						<input id="delete" type="hidden" value="1" />
					</c:if>
				</div>
				<div class="step-main">
					<div class="table-responsive">
						<table id="versionTable" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th class="center">版本名称</th>
									<th class="center">最大用户数</th>
									<th class="center">素材空间上限</th>
									<th class="center">单个文件上限</th>
									<th class="center">版本描述</th>
									<th class="center">创建时间</th>
									<th class="center">操作</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
					<div class="page-new clear">
						<div id="pageDiv" align="right" class="pull-right"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="createVersionDialog" class="modal fade">
		<div class="window-box modal-dialog-middle">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						添加版本
					</div>
				</div>

				<div class="modal-body-middle no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="addVersionForm" class="form-horizontal" role="form">
									<table class="table detail-tab layui-form">
										<tr>
											<td width="150px" align="right"><span class="required_style">*</span>版本名称：</td>
											<td><input id="cName" name="cName" type="text" class="col-sm-3" placeholder="版本名称" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>用户数上限：</td>
											<td><input id="cUserLimit" name="cUserLimit" type="text" class="col-sm-3" onkeyup="integerChange(this);" placeholder="用户数上限(个)" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>素材空间上限(MB)：</td>
											<td><input id="cMaterialLimit" name="cMaterialLimit" type="text" class="col-sm-3" onkeyup="integerChange(this);" placeholder="素材空间上限(MB)" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>单个文件上限(MB)：</td>
											<td><input id="cSingleFileLimit" name="cSingleFileLimit" type="text" class="col-sm-3" onkeyup="integerChange(this);" placeholder="单个文件上限(MB)" /></td>
										</tr>
										<tr>
											<td align="right">版本描述：</td>
											<td><textarea id="cDescription" style="height: 50%; width: 50%" placeholder="版本描述"></textarea></td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmCreateButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>

	<div id="modifyVersionDialog" class="modal fade">
		<div class="window-box modal-dialog-middle">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						编辑版本
					</div>
				</div>

				<div class="modal-body-middle no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="modifyVersionForm" method="post" class="form-horizontal" role="form">
									<input id="mId" type="hidden" />
									<table class="table detail-tab layui-form">
										<tr>
											<td width="150px" align="right"><span class="required_style">*</span>版本名称：</td>
											<td><input id="mName" name="mName" type="text" class="col-sm-3" placeholder="版本名称" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>用户数上限：</td>
											<td><input id="mUserLimit" name="mUserLimit" type="text" class="col-sm-3" onkeyup="integerChange(this);" placeholder="用户数上限(个)" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>素材空间上限(MB)：</td>
											<td><input id="mMaterialLimit" name="mMaterialLimit" type="text" class="col-sm-3" onkeyup="integerChange(this);" placeholder="素材空间上限(MB)" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>单个文件上限(MB)：</td>
											<td><input id="mSingleFileLimit" name="mSingleFileLimit" type="text" class="col-sm-3" onkeyup="integerChange(this);" placeholder="单个文件上限(MB)" /></td>
										</tr>
										<tr>
											<td align="right">版本描述：</td>
											<td><textarea id="mDescription" style="height: 50%; width: 50%" placeholder="版本描述"></textarea></td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmModifyButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>
	
	<div id="versionDetailDialog" class="modal fade">
		<div class="window-box modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						版本详情
					</div>
				</div>
	
				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<table class="table detail-tab">
									<tr>
										<td>名称：<span id="vName"></span></td>
										<td>用户数上限：<span id="vUserLimit"></span></td>
									</tr>
									<tr>
										<td>素材空间上限：<span id="vMaterialLimit"></span></td>
										<td>单个文件上限：<span id="vSingleFileLimit"></span></td>
									</tr>
									<tr>
										<td>创建时间：<span id="vCreatedTime"></span></td>
										<td>描述：<span id="vDescription"></span></td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
	
				<div class="modal-footer no-margin-top"></div>
			</div>
		</div>
	</div>

	<div id="setVersionRightDialog" class="modal fade">
		<div class="window-box modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						<i class="icon-credit-card"></i> 设置版本 <span id="versionInfo"></span> 功能权限
					</div>
				</div>

				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="modifyVersionRightForm" method="post" class="form-horizontal" role="form">
									<input id="versionId" name="versionId" type="hidden" /> 
									<input id="versionIdName" name="versionName" type="hidden" />
									<div style="height: 520px; text-align: left; overflow: auto;">
										<ul id="versionRightTree" class="ztree"></ul>
									</div>
									<div class="space-4"></div>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmSetVersionRightButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
