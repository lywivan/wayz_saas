<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../common_taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>系统全局配置</title>
<script type="text/javascript" src="js/config/system_config.js?v=<%=System.currentTimeMillis()%>"></script>
<style>
.left-td1 .search-tit {
	width: 128px;
}

.left-td1 .search-main {
	width: 61%;
}

.search-main input[type="checkbox"] {
	height: 16px;
	width: 16px;
	margin-top: 6px;
}

.bootstrap-duallistbox-container .btn-box {
	margin-top: 18px;
}

.ue-container {
	background: none repeat scroll 0 0;
	border: 0;
	margin: -18px 0 0 2px;
	padding: 0;
	width: 100%;
}
</style>
<script type="text/javascript">
<!--
	// 广告渠道
	var $RTBChannelList = new Array();
	-->
</script>
</head>
<body>
	<div class="page-content">
		<div class="row">
			<div class="col-xs-12">
				<div class="step-main">
					<h4 class="step-tit">
						<i></i>参数配置
					</h4>
					<div class="search-content">
						<ul class="search-top layui-form">
							<li class="left-td left-td1"><span class="search-tit search-tit1"><span class="required_style">*</span>获取任务周期(秒)：</span>
								<div class="search-main">
									<input type="text" id="taskPeriod" onkeyup="integerChange(this);" placeholder="获取任务周期(秒)" />
								</div>
							</li>
							<li class="left-td left-td1"><span class="search-tit search-tit1"><span class="required_style">*</span>发送心跳周期(秒)：</span>
								<div class="search-main">
									<input type="text" id="heartPeriod" onkeyup="integerChange(this);" placeholder="发送心跳周期(秒)" />
								</div>
							</li>
							<li class="left-td left-td1"><span class="search-tit search-tit1"><span class="required_style">*</span>参数上报周期(秒)：</span>
								<div class="search-main">
									<input type="text" id="reporPeriod" onkeyup="integerChange(this);" placeholder="系统参数上报周期(秒)" />
								</div>
							</li>
							<li class="left-td left-td1"><span class="search-tit search-tit1"><span class="required_style">*</span>设备所在时区：</span>
								<div class="search-main">
									<div class="layui-form-item">
										<div class="layui-input-block">
											<select id="timeZone">
												<option value="-12">-12:00</option>
												<option value="-11">-11:00</option>
												<option value="-10">-10:00</option>
												<option value="-9">-9:00</option>
												<option value="-8">-8:00</option>
												<option value="-7">-7:00</option>
												<option value="-6">-6:00</option>
												<option value="-5">-5:00</option>
												<option value="-4">-4:00</option>
												<option value="-3">-3:00</option>
												<option value="-2">-2:00</option>
												<option value="-1">-1:00</option>
												<option value="0">0:00</option>
												<option value="1">+1:00</option>
												<option value="2">+2:00</option>
												<option value="3">+3:00</option>
												<option value="4">+4:00</option>
												<option value="5">+5:00</option>
												<option value="6">+6:00</option>
												<option value="7">+7:00</option>
												<option value="8" selected>+8:00</option>
												<option value="9">+9:00</option>
												<option value="10">+10:00</option>
												<option value="11">+11:00</option>
												<option value="12">+12:00</option>
												<option value="13">+13:00</option>
											</select>
										</div>
									</div>
								</div>
							</li>
							<li class="left-td left-td1"><span class="search-tit search-tit1">服务器地址：</span>
								<div class="search-main">
									<input type="text" id="serverAddress" placeholder="服务器地址" />
								</div>
							</li>
							<li class="left-td left-td1"><span class="search-tit search-tit1">播放竞价广告：</span>
								<div class="search-main">
									<input type="radio" name="isGetDsp" id="isGetDsp_1" value="1" title="是" lay-skin="primary">
									&nbsp;&nbsp;
									<input type="radio" name="isGetDsp" id="isGetDsp_0" value="0" title="否" lay-skin="primary">
								</div>
							</li>
						</ul>
						<div class="clear"></div>
						<div class="search-button">
							<button id="saveButton_playConfig" type="button" class="btn btn-step">保存</button>
						</div>
					</div>
				</div>

				<div class="step-main">
					<h4 class="step-tit">
						<i></i>重置Redis缓存
					</h4>
					<div class="search-content">
						<table class="my-query-table table-margin">
							<tr>
								<td><label class="xtqj"> <input type="radio" name="redisItem" value="分辨率"
										itemUrl="resource/resetResolutionToRedis" /> 分辨率&nbsp;&nbsp;
								</label> <label class="xtqj"> <input type="radio" name="redisItem" value="设备"
										itemUrl="playManagement/restPlayerControllerToRedis" /> 设备&nbsp;&nbsp;
								</label> <label class="xtqj"> <input type="radio" name="redisItem" value="设备版本"
										itemUrl="playManagement/restPlayerVersionToRedis" /> 设备版本&nbsp;&nbsp;
								</label> <label class="xtqj"> <input type="radio" name="redisItem" value="设备垫片"
										itemUrl="playManagement/restReserveImageToRedis" /> 设备垫片&nbsp;&nbsp;
								</label></td>
							</tr>
						</table>
					</div>
					<div class="clear"></div>
					<div class="search-button">
						<button id="resetRedisData" type="button" class="btn btn-step">重置</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
