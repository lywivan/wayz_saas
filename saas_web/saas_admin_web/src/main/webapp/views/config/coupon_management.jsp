<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../common_taglibs.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>代金券管理</title>
<script type="text/javascript" src="js/config/coupon_management.js?v=<%=System.currentTimeMillis()%>"></script>
</head>
<body>
	<div class="page-content">
		<div class="row">
			<div class="col-xs-12">
				<div class="step-main">
					<ul class="search-top layui-form">
						<li class="left-td"> 
							<span class="search-tit">代金券类型：</span>
							<div class="search-main">
								<div class="layui-form-item">
									<div class="layui-input-block">
										<select id="productType">
											<option value="">请选择</option>
											<option value="0">账户充值</option>
											<option value="1">点位新购</option>
											<option value="2">点位续期</option>
											<option value="3">空间</option>
											<option value="4">直播服务</option>
										</select>
									</div>
								</div>
							</div>
						</li>
						<li class="left-td" style="margin-right: 100px;">
							<span class="search-tit">代金券状态：</span>
							<div class="search-main">
								<div class="layui-form-item">
									<div class="layui-input-block">
										<select id="isEnable">
											<option value="">请选择</option>
											<option value="1">启用</option>
											<option value="0">禁用</option>
										</select>
									</div>
								</div>
							</div>
						</li>
					</ul>
					<div class="clear"></div>
					<div class="search-button">
						<c:if test="${fn:contains($rightList,'admin/queryCoupon')}">
							<button id="queryButton" type="button" class="search-fn-btn">
								<i class="icon-search"></i> 查询
							</button>
						</c:if>
					</div>
				</div>
				<div id="toolbarDiv">
					<c:if test="${fn:contains($rightList,'admin/createCoupon')}">
						<button id="createButton" type="button" class="btn btn-step">
							<i class="icon-credit-card"></i> 创建
						</button>
					</c:if>
					<c:if test="${fn:contains($rightList,'admin/modifyCoupon')}">
						<input type="hidden" id="modifyCoupon" value="1" />
					</c:if>
					<c:if test="${fn:contains($rightList,'admin/distributeCoupon')}">
						<input type="hidden" id="distributeCoupon" value="1" />
					</c:if>
					<c:if test="${fn:contains($rightList,'admin/enableCoupon')}">
						<input type="hidden" id="enableCoupon" value="1" />
					</c:if>
				</div>
				<div class="step-main">
					<div class="table-responsive">
						<table id="couponTable" class="table table-striped table-bordered">
							<thead>
								<tr>	
									<th class="center">名称</th>
									<th class="center">类型</th>
									<th class="center">最低消费(元)</th>
									<th class="center">减免金额(元)</th>
									<th class="center">发放数量</th>
									<th class="center">剩余数量</th>
									<th class="center">有效天数</th>
									<th class="center">当前状态</th>
									<th class="center">是否公开</th>
									<th class="center">创建时间</th>
									<th class="center">使用说明</th>
									<th class="center operationColumn">操作</th>
								</tr>
							</thead>
							<tbody>

							</tbody>
						</table>
					</div>
					<div class="page-new clear">
						<div id="pageDiv" align="right" class="pull-right"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="createCouponDialog" class="modal fade">
		<div class="window-box modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						创建代金券
					</div>
				</div>

				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="addCouponForm" class="form-horizontal" role="form">
									<table class="table detail-tab layui-form">
										<tr>
											<td width="100px" align="right"><span class="required_style">*</span>名称：</td>
											<td><input id="cName" type="text" class="col-sm-6" placeholder="名称" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>代金券类型：</td>
											<td>
												<div class="layui-form-item">
													<div class="layui-input-block" style="width: 50%">
														<select id="cProductType" lay-verify="required" lay-search>
															<option value="">请选择</option>
															<option value="0">账户充值</option>
															<option value="1">点位新购</option>
															<option value="2">点位续期</option>
															<option value="3">空间</option>
															<option value="4">直播服务</option>
														</select>
													</div>
												</div>
											</td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>最低消费(元)：</td>
											<td><input id="cFullAmount" onkeyup="decimalChange(this);" type="text" class="col-sm-6" placeholder="最低消费" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>减免金额(元)：</td>
											<td><input id="cReduceAmount" onkeyup="decimalChange(this);" type="text" class="col-sm-6" placeholder="减免金额(元) " /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>发放数量：</td>
											<td><input id="cProvideNumber" onkeyup="integerChange(this);" type="text" class="col-sm-6" placeholder="发放数量 " /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>有效天数：</td>
											<td><input id="cValidDays" onkeyup="integerChange(this);" type="text" class="col-sm-6" placeholder="有效天数" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>使用说明：</td>
											<td><input id="cTitle" type="text" class="col-sm-6" placeholder="使用说明" /></td>
										</tr>
										<tr>
											<td align="right">是否公开：</td>
											<td><input type="checkbox" name="cIsPublic" id="cIsPublic" lay-skin="primary"></td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmCreateButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>

	<div id="modifyCouponDialog" class="modal fade">
		<div class="window-box modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						编辑代金券
					</div>
				</div>

				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="modifyCouponForm" class="form-horizontal" role="form">
									<input id="mId" type="hidden" />
									<table class="table detail-tab layui-form">
										<tr>
											<td width="100px" align="right"><span class="required_style">*</span>名称：</td>
											<td><input id="mName" type="text" class="col-sm-6" placeholder="名称" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>代金券类型：</td>
											<td>
												<div class="layui-form-item">
													<div class="layui-input-block" style="width: 50%">
														<select id="mProductType" lay-verify="required" lay-search>
															<option value="">请选择</option>
															<option value="0">账户充值</option>
															<option value="1">点位新购</option>
															<option value="2">点位续期</option>
															<option value="3">空间</option>
															<option value="4">直播服务</option>
														</select>
													</div>
												</div>
											</td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>最低消费：</td>
											<td><input id="mFullAmount" onkeyup="decimalChange(this);" type="text" class="col-sm-6" placeholder="最低消费" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>减免金额：</td>
											<td><input id="mReduceAmount" onkeyup="decimalChange(this);" type="text" class="col-sm-6" placeholder="减免金额 " /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>发放数量：</td>
											<td><input id="mProvideNumber" onkeyup="integerChange(this);" type="text" class="col-sm-6" placeholder="发放数量 " /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>有效天数：</td>
											<td><input id="mValidDays" onkeyup="integerChange(this);" type="text" class="col-sm-6" placeholder="有效天数" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>使用说明：</td>
											<td><input id="mTitle" type="text" class="col-sm-6" placeholder="使用说明" /></td>
										</tr>
										<tr>
											<td align="right">是否公开：</td>
											<td><input type="checkbox" name="cIsPublic" id="mIsPublic" lay-skin="primary"></td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmModifyButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>
	
	<div id="distributeCouponDialog" class="modal fade">
		<div class="window-box modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						分配代金券
					</div>
				</div>

				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="distributeCouponForm" class="form-horizontal" role="form">
									<input id="couponId" type="hidden" />
									<table class="table detail-tab layui-form">
										<tr>
											<td align="right"><span class="required_style">*</span>所属公司：</td>
											<td>
												<div class="layui-form-item">
													<div class="layui-input-block" style="width: 50%">
														<select id="companyId" lay-verify="required" lay-search>
															<option value="">请选择</option>
															<c:if test="${not empty companyList}">
																<c:forEach var="vo" items="${companyList}">
																	<option value="${vo.id}">${vo.name}</option>
																</c:forEach>
															</c:if>
														</select>
													</div>
												</div>
											</td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>有效天数：</td>
											<td><input id="days" onkeyup="integerChange(this);" type="text" class="col-sm-6" placeholder="有效天数" /></td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmDistributeButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
