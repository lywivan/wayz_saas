<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../common_taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>播控版本</title>
<script type="text/javascript" src="js/config/play_version.js?v=<%=System.currentTimeMillis()%>"></script>
</head>
<body>
	<div class="page-content">
		<div class="row">
			<div class="col-xs-12">

				<div id="toolbarDiv">
					<button id="createButton" type="button" class="btn btn-step">
						<i class="icon-credit-card"></i> 创建
					</button>
				</div>

				<div class="step-main">
					<table id="playVersionTable" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th class="center">版本号</th>
								<th class="center">MD5验证码</th>
								<th class="center">是否通用</th>
								<th class="center">版本描述</th>
								<th class="center">创建时间</th>
								<th class="center">操作</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
					<div class="page-new clear">
						<div id="pageDiv" align="right" class="pull-right"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="createPlayVersionDialog" class="modal fade">
		<div class="window-box modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						创建播控版本
					</div>
				</div>

				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="addPlayForm" class="form-horizontal layui-form" role="form">
									<div class="space-4"></div>
									<input type="hidden" id="mId" name="mId" />
									<table class="table detail-tab">
										<thead>
											<tr>
												<td style="width: 100px;" align="right"><span class="required_style">*</span>版本号：</td>
												<td><input id="cVersion" type="text" class="col-sm-6" placeholder="版本号" /></td>
											</tr>
										</thead>
										<tr>
											<td align="right"><span class="required_style">*</span>版本文件：</td>
											<td><input type="hidden" id="cPackageUrl" />
												<div id="cPackageFileDiv"></div> <input type="file" id="cPackageFile"
												onchange="showUploadPlayVersionFile(this.id,'cPackageFileDiv','cPackageUrl','0');" /></td>
										</tr>
										<tr>
											<td align="right">版本描述：</td>
											<td><input id="cDescription" type="text" class="col-sm-6" placeholder="版本描述" /></td>
										</tr>
										<tr>
											<td align="right">是否通用：</td>
											<td>
												<input type="checkbox" lay-filter="cIsGeneral" name="cIsGeneral" id="cIsGeneral" lay-skin="primary">
											</td>
										</tr>
										<tr class="companyTr">
											<td align="right"><span class="required_style">*</span>指定公司：</td>
											<td>
												<div class="layui-form-item">
													<div class="layui-input-block" style="width: 50%">
														<select id="cCompanyId" lay-verify="required" lay-search>
															<option value="">请选择</option>
															<c:if test="${not empty companyList}">
																<c:forEach var="vo" items="${companyList}">
																	<option value="${vo.id}">${vo.name}</option>
																</c:forEach>
															</c:if>
														</select>
													</div>
												</div>
											</td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmCreateButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn btn-fn btn-delect" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>
	
	<div id="setPlayVersionCompanyDialog" class="modal fade">
		<div class="window-box modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						设置播控版本公司
					</div>
				</div>

				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="setPlayVersionCompanyForm" class="form-horizontal" role="form">
									<div class="space-4"></div>
									<input type="hidden" id="version" name="version" />
									<table class="table detail-tab">
										<tr>
											<td width="100px" align="right"><span class="required_style">*</span>选择公司：</td>
											<td>
												<c:if test="${not empty companyList }">
													<input type="checkbox" id="selectListAll">全选 <br/>
													<c:forEach var="vo" items="${companyList }">
														<input type="checkbox" name="companyItem" id="companyItem_${vo.id}" value="${vo.id}">${vo.name} &nbsp;
													</c:forEach>
												</c:if>
											</td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmSetPVCButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn btn-fn btn-delect" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
