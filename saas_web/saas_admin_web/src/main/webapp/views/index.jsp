﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ include file="common.jsp"%>
<!DOCTYPE html>
<head>
<title>奥凌媒体服务运营管理平台</title>
<meta name="description" content="我们以先进的人工智能和大数据为核心,打造一站式户外广告平台" />
<meta name="keywords" content="奥凌,屏多多,广告计划,广告交易平台,户外媒体,户外广告,智云联众" />
<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
<c:set var="company_id" value="${user.companyId}" scope="session" />
<c:set var="company_type" value="${user.roleType}" scope="session" />
<c:set var="role_id" value="${user.roleId}" scope="session" />
<script type="text/javascript">
	$.session.set('user_id', ${user.id});
	$.session.set('user_name','${user.username}');
	$.session.set('company_id', ${user.companyId});
	$.session.set('company_name',"${user.companyName}");
	$.session.set('company_type', ${user.roleType});
	$.session.set('role_id', ${user.roleId});
</script>
<script type="text/javascript" src="js/index.js?v=<%=System.currentTimeMillis()%>"></script>
</head>
<body>
	<div class="navbar navbar-default" id="navbar" style="height:auto;">
		<div class="navbar-container" id="navbar-container">
			<div class="navbar-header header-logo pull-left">
				<a href="#" target="_blank" class="navbar-brand">
				<img id="logoFile" src='images/logo-content.png' /></a>
			</div>
			<div class="navbar-header pull-right" role="navigation">
				<ul class="nav ace-nav">
					<li class="light-blue">
						<a data-toggle="dropdown" href="#" class="dropdown-toggle" title="${user.username}">
							<span class="user-info">${user.username}</span> 
							<i class="user-ico icon-angle-down"></i>
						</a>
						<ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
							<li>
								<a href="javascript:void(0);" onclick="showModifyPwdDialog();"> 
									<i class="icon-cog"></i>修改密码
								</a>
							</li>
							<li class="divider"></li>
							<li><a href="javascript:logout();"> <i class="icon-off"></i>退出
							</a></li>
						</ul></li>
					<!-- <li id="msgLi" class=""><a data-toggle="dropdown" class="dropdown-toggle" href="javascript:void(0);" onclick="showMsgPage();"> <i
							class="icon-bell-alt icon-animated-bell" style="margin-top:13px;"></i> <span class="badge" id="msgInfo"></span>
					</a></li> -->
				</ul>
			</div>
			<ul class="header-nav pull-left">
			</ul>
		</div>
	</div>
	<div class="null-main"></div>
	<div class="main-container" id="main-container">
		<div class="main-container-inner">
			<a class="menu-toggler" id="menu-toggler" href="#"> <span class="menu-text"></span></a>
			<div class="sidebar" id="sidebar">
				<div class="sidebar-shortcuts" id="sidebar-shortcuts">
				</div>
				<ul class="nav nav-list" id="leftMenu"></ul>
			</div>
			<div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb" id="navigationInfo">
					</ul>
				</div>
				<iframe id="mainContent" src="" width="100%" frameborder="no" border="0" srolling="auto"></iframe>
			</div>
		</div>
	</div>

	<div id="modifyPwdDialog" class="modal fade">
		<div class="window-box modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						修改密码
					</div>
				</div>

				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="modifyPwdForm" class="form-horizontal" role="form">
									<table class="table">
										<thead>
											<tr>
												<td width="100px" align="right"><span class="required_style">*</span>旧密码：</td>
												<td align="left"><input type="password" id="oldPassword" name="oldPassword" placeholder="旧密码"
													style="width: 180px;" /></td>
											</tr>
										</thead>
										<tr>
											<td nowrap="nowrap" align="right"><span class="required_style">*</span>新密码：</td>
											<td align="left"><input type="password" id="newPassword" name="newPassword" placeholder="新密码"
												style="width: 180px;" /></td>
										</tr>
										<tr>
											<td nowrap="nowrap" align="right"><span class="required_style">*</span>确认新密码：</td>
											<td align="left"><input type="password" id="chkNewPwd" name="chkNewPwd" placeholder="确认新密码"
												style="width: 180px;" /></td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmModifyPwdButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn btn-fn btn-delect" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>

	<!-- 消息页面 -->
	<div class="page-content" style="display: none; background: #fff none repeat scroll 0 0;" id="msgDivDialog">
		<div class="row">
			<div class="col-xs-12">
				<div class="space-4"></div>
				<span id="msgDiv"></span>
			</div>
		</div>
	</div>
</body>
</html>