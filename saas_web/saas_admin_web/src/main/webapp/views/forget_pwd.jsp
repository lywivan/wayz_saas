<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>忘记密码</title>
<meta name="description" content="我们以先进的人工智能和大数据为核心，打造一站式户外广告设备管理平台，帮助户外媒体主降低销售、管理、维护成本，增加广告收入，提高户外媒体的核心竞争力。" />
<meta name="keywords" content="咪咕视讯慧屏广告,奥凌,广告计划,广告交易平台,户外媒体,户外广告,智云联众,媒体主管理平台,户外媒体管理平台" />
<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
<link href="assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="assets/css/font-awesome.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="css/platform.css" />
<link rel="stylesheet" href="assets/css/ace.min.css" />
<script src="js/common/jquery-1.10.2.js"></script>
<script src="js/common/jquery.call.interface.js"></script>
<script src="assets/js/jquery.tips.js"></script>
<script src="js/common/utils.js"></script>
<script src="assets/js/jquery.md5.js"></script>
<script type="text/javascript" src="js/forget_pwd.js?v=<%=System.currentTimeMillis()%>"></script>
<style type="text/css">
a{text-decoration: none;}
.form-tit{color:#fff!important;}
</style>
</head>
<body style="background: #00172f;">
	<div class="login">
		<div class="login-box">
			<div class="login-main">
				<a href="javascript:;" class="logo-ico">
					<img src="images/login-logo.png" />
				</a>
				<div class="login-content register-content">
					<ul class="login-form">
						<li><span class="form-tit">手机号</span> 
							<div class="resdiv">
								<input id="mPhone" type="text" class="form-text" placeholder="请输入手机号"
							autocomplete="off" required /></div>
						</li>
						<li><span class="form-tit">短信验证码</span> 
							<div class="resdiv">
								<input id="mVerifycode" type="text" placeholder="请输入短信验证码"
							autocomplete="off" required /> <a href="javascript:void(0);" title="点击获取验证码" id="sendSMSVerifyCodeButton"
							class="yzdx">获取验证码</a></div>
						</li>
						<li><span class="form-tit">新密码</span> 
							<div class="resdiv">
								<input type="password" id="newPassword" class="form-text"
							placeholder="请输入新密码" autocomplete="off" onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" required minlength="6"
							maxlength="30" /></div>
						</li>
						<li><span class="form-tit">确认密码</span> 
							<div class="resdiv">
								<input type="password" id="confirmPassword" class="form-text"
							placeholder="请输入确认密码" autocomplete="off" onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" required
							equalTo="#newPassword" minlength="6" maxlength="30" /></div>
						</li>
						<li class="login-sub"><a href="javascript:void(0);" id="confirmResetPwdButton">确认修改</a></li>
						<li class="login-loading"><a href="login" class="forget">立即登录</a></li>
					</ul>
				</div>
				<div class="clearfix"></div>
				<div class="support">技术支持单位：奥凌OOHLink</div>
			</div>
		</div>
	</div>
</body>
</html>