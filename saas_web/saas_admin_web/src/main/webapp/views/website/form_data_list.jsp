<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../common_taglibs.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta charset="utf-8" />
	<title>表单数据列表</title>
	<script type="text/javascript" src="js/website/form_data_list.js?v=<%=System.currentTimeMillis()%>"></script>
</head>
<body>
	<div class="page-content">
		<div class="row">
			<div class="col-xs-12">
				<div class="step-main">
					<ul class="search-top layui-form">
						<li class="left-td">
							<span class="search-tit">表单类型：</span>
							<div class="search-main">
								<select id="type" lay-verify="required" lay-search>
									<option value="">全部</option>
									<c:if test="${typeList != null}">
										<c:forEach var="temp" items="${typeList}">
											<option value="${temp.id}">${temp.name}</option>
										</c:forEach>
									</c:if>
								</select>
							</div>
						</li>
						<li class="left-td"><span class="search-tit">手机号：</span>
							<div class="search-main">
								<input autocomplete="off" id="phone" type="text" class="col-sm-6" placeholder="手机号" />
							</div>
						</li>
					</ul>
					<div class="clear"></div>
					<div class="search-button">
						<button id="queryFromButton" type="button" class="search-fn-btn">
							<i class="icon-search"></i> 查询
						</button>
					</div>
				</div>
				<div class="step-main">
					<div class="table-responsive">
						<table id="dataTable" class="table table-striped table-bordered">
							<thead>
							<tr>
								<th class="center">联系人</th>
								<th class="center">联系电话</th>
								<th class="center">公司名称</th>
								<th class="center">地址</th>
								<th class="center">类型</th>
								<th class="center">创建时间</th>
							</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
					<div class="page-new clear">
						<div id="pageDiv" align="right" class="pull-right"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>