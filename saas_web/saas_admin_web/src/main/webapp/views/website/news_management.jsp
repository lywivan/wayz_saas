<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../common_taglibs.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta charset="utf-8" />
	<title>新闻管理</title>
	<script type="text/javascript" src="plugin/wangEditor/release/wangEditor.min.js"></script>
	<script type="text/javascript" src="js/website/news_management.js?v=<%=System.currentTimeMillis()%>"></script>
</head>
<body>
<div class="page-content">
	<div class="row">
		<div class="col-xs-12">
			<div class="step-main">
				<ul class="search-top layui-form">
					<li class="left-td">
						<span class="search-tit">新闻标题：</span>
						<div class="search-main">
							<input id="title" type="text" placeholder="新闻标题" autocomplete="off" />
						</div>
					</li>
					<li class="left-td">
						<span class="search-tit">新闻类型：</span>
						<div class="search-main">
							<select id="categoryId" lay-verify="required" lay-search>
								<option value="">全部</option>
								<c:if test="${newsTypeList != null}">
									<c:forEach var="temp" items="${newsTypeList}">
										<option value="${temp.id}">${temp.name}</option>
									</c:forEach>
								</c:if>
							</select>
						</div>
					</li>
					<li class="left-td">
						<span class="search-tit">发布状态：</span>
						<div class="search-main">
							<select id="isPush" lay-verify="required" lay-search>
								<option value="">全部</option>
								<option value="1">已发布</option>
								<option value="0">未发布</option>
							</select>
						</div>
					</li>
				</ul>
				<div class="clear"></div>
				<div class="search-button">
					<c:if test="${fn:contains($rightList,'home/queryArticle')}">
						<button id="queryNewsButton" type="button" class="search-fn-btn">
							<i class="icon-search"></i> 查询
						</button>
					</c:if>
				</div>
			</div>
			<div id="toolbarDiv">
				<c:if test="${fn:contains($rightList,'home/createArticle')}">
					<button id="createNewsButton" type="button" class="btn btn-step">
						<i class="icon-credit-card"></i> 创建
					</button>
				</c:if>
				<c:if test="${fn:contains($rightList,'home/modifyArticle')}">
					<input type="hidden" id="modify" value="1" />
				</c:if>
				<c:if test="${fn:contains($rightList,'home/delArticle')}">
					<input type="hidden" id="delete" value="1" />
				</c:if>
				<c:if test="${fn:contains($rightList,'home/modifyArticlePush')}">
					<input type="hidden" id="publish" value="1" />
				</c:if>
			</div>
			<div class="step-main">
				<div class="table-responsive">
					<table id="dataTable" class="table table-striped table-bordered">
						<thead>
						<tr>
							<th class="center">排序码</th>
							<th class="center">标题</th>
							<th class="center">类型</th>
							<th class="center">摘要</th>
							<th class="center">是否发布</th>
							<th class="center">发布时间</th>
							<th class="center">创建时间</th>
							<th width="150px" class="center">操作</th>
						</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				<div class="page-new clear">
					<div id="pageDiv" align="right" class="pull-right"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="createNewsDialog" class="modal fade">
	<div class="modal-dialog-max">
		<div class="modal-content">
			<div class="modal-header no-padding">
				<div class="table-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
						<span class="box-close">×</span>
					</button>
					创建新闻
				</div>
			</div>

			<div class="modal-body-max no-padding">
				<div class="page-content" style="background:#fff;">
					<div class="row">
						<div class="col-xs-12">
							<form id="addNewsForm" class="form-horizontal" role="form">
								<div class="space-4"></div>
								<table class="table site-box">
									<tr>
										<td width="100px" align="right"><span class="required_style">*</span>排序：</td>
										<td><input id="cNewsSort" type="text" onkeyup="integerChange(this)" class="uniform input-large" placeholder="排序为阿拉伯数字" autocomplete="off" /></td>
									</tr>
									<tr>
										<td align="right"><span class="required_style">*</span>新闻标题：</td>
										<td><input id="cNewsTitle" type="text" class="uniform input-large"  placeholder="新闻标题" autocomplete="off" /></td>
									</tr>
									<tr>
										<td align="right"><span class="required_style">*</span>发布时间：</td>
										<td>
											<input type="text" class="layui-input" id="cPushTime" autocomplete="off" lay-verify="date" placeholder="发布时间" style="width: 121px; height: 28px;"  />
										</td>
									</tr>
									<tr class='layui-form'>
										<td align="right"><span class="required_style">*</span>新闻类型：</td>
										<td>
											<div class='input-group'>
												<select id="cNewsType" lay-verify="required" lay-search>
													<option value="">请选择</option>
													<c:if test="${newsTypeList != null}">
														<c:forEach var="temp" items="${newsTypeList}">
															<option value="${temp.id}">${temp.name}</option>
														</c:forEach>
													</c:if>
												</select>
											</div>
										</td>
									</tr>
									<tr>
										<td align="right"><span class="required_style">*</span>新闻摘要：</td>
										<td><textarea id="cNewsContent" style="width: 600px; height: 100px;" placeholder="新闻摘要"></textarea></td>
									</tr>
									<tr>
										<td align="right"><span class="required_style">*</span>新闻图片：</td>
										<td><input type="hidden" id="cNewsImgUrl" />
											<div id="cNewsImgFileDiv"></div> <input type="file" id="cNewsImgFile"
																					onchange="showUploadFile(this.id,'cNewsImgFileDiv','cNewsImgUrl','0',true);" /> 建议上传大小为214*156的图片，展示效果更佳</td>
									</tr>
									<tr>
										<td align="right">关键词：</td>
										<td><input id="cKeyWords" type="text" class="uniform input-large" placeholder="关键词之间用英文逗号隔开" autocomplete="off" /></td>
									</tr>
									<tr>
										<td align="right">新闻描述：</td>
										<td><textarea id="cNewsDesc" style="width: 600px; height: 50px;" placeholder="新闻描述"></textarea></td>
									</tr>
									<tr>
										<td align="right"><span class="required_style">*</span>新闻详情：</td>
										<td>
											<div id="cNewsDetail"></div>
										</td>
									</tr>
								</table>
								<div class="space-4"></div>
							</form>
						</div>
					</div>
				</div>
			</div>

			<div class="modal-footer no-margin-top">
				<button id="confirmCreateNewsButton" type="button" class="btn btn-primary">确定</button>
				<button class="btn btn-fn btn-delect" data-dismiss="modal">取消</button>
			</div>
		</div>
	</div>
</div>

<div id="modifyNewsDialog" class="modal fade">
	<div class="modal-dialog-max">
		<div class="modal-content">
			<div class="modal-header no-padding">
				<div class="table-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
						<span class="box-close">×</span>
					</button>
					编辑新闻
				</div>
			</div>

			<div class="modal-body-max no-padding">
				<div class="page-content" style="background:#fff;">
					<div class="row">
						<div class="col-xs-12">
							<form id="modifyNewsForm" class="form-horizontal" role="form">
								<input id="mNewsId"type="hidden" />
								<div class="space-4"></div>
								<table class="table site-box">
									<tr>
										<td width="100px" align="right"><span class="required_style">*</span>排序：</td>
										<td><input id="mNewsSort" type="text" onkeyup="integerChange(this)" class="uniform input-large" placeholder="排序为阿拉伯数字" autocomplete="off" /></td>
									</tr>
									<tr>
										<td align="right"><span class="required_style">*</span>新闻标题：</td>
										<td><input id="mNewsTitle" type="text" class="uniform input-large"  placeholder="新闻标题" autocomplete="off" /></td>
									</tr>
									<tr>
										<td align="right"><span class="required_style">*</span>发布时间：</td>
										<td>
											<input type="text" class="layui-input" id="mPushTime" autocomplete="off" lay-verify="date" placeholder="发布时间" style="width: 121px; height: 28px;"  />
										</td>
									</tr>
									<tr class='layui-form'>
										<td align="right"><span class="required_style">*</span>新闻类型：</td>
										<td>
											<div class='input-group'>
												<select id="mNewsType" lay-verify="required" lay-search>
													<option value="">请选择</option>
													<c:if test="${newsTypeList != null}">
														<c:forEach var="temp" items="${newsTypeList}">
															<option value="${temp.id}">${temp.name}</option>
														</c:forEach>
													</c:if>
												</select>
											</div>
										</td>
									</tr>
									<tr>
										<td align="right"><span class="required_style">*</span>新闻摘要：</td>
										<td><textarea id="mNewsContent" style="width: 600px; height: 100px;" placeholder="新闻摘要"></textarea></td>
									</tr>
									<tr>
										<td align="right"><span class="required_style">*</span>新闻图片：</td>
										<td><input type="hidden" id="mNewsImgUrl" />
											<div id="mNewsImgFileDiv"></div> <input type="file" id="mNewsImgFile"
																					onchange="showUploadFile(this.id,'mNewsImgFileDiv','mNewsImgUrl','0',true);" /> 建议上传大小为214*156的图片，展示效果更佳</td>
									</tr>
									<tr>
										<td align="right">关键词：</td>
										<td><input id="mKeyWords" type="text" class="uniform input-large" placeholder="关键词之间用英文逗号隔开" autocomplete="off" /></td>
									</tr>
									<tr>
										<td align="right">新闻描述：</td>
										<td><textarea id="mNewsDesc" style="width: 600px; height: 50px;" placeholder="新闻描述"></textarea></td>
									</tr>
									<tr>
										<td align="right"><span class="required_style">*</span>新闻详情：</td>
										<td>
											<div id="mNewsDetail" style="width:900px;"></div>
										</td>
									</tr>
								</table>
								<div class="space-4"></div>
							</form>
						</div>
					</div>
				</div>
			</div>

			<div class="modal-footer no-margin-top">
				<button id="confirmModifyNewsButton" type="button" class="btn btn-primary">确定</button>
				<button class="btn btn-fn btn-delect" data-dismiss="modal">取消</button>
			</div>
		</div>
	</div>
</div>
</body>
</html>
