<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../common_taglibs.jsp"%>
<%@ include file="position_detail_dialog.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>设备管理</title>
<script type="text/javascript" src="js/position/position_management.js?v=<%=System.currentTimeMillis()%>"></script>
</head>
<body>
	<div class="page-content">
		<div class="row">
			<div class="col-xs-12">
				<div class="step-main">
					<ul class="search-top layui-form">
						<li class="left-td"><span class="search-tit">公司：</span>
							<div class="search-main">
								<div class="layui-form-item">
									<div class="layui-input-block">
										<select id="companyId">
											<option value="">请选择</option>
											<c:if test="${not empty mediaCompanyList}">
												<c:forEach var="vo" items="${mediaCompanyList }">
													<option value="${vo.id}">${vo.name}</option>
												</c:forEach>
											</c:if>
										</select>
									</div>
								</div>
							</div>
						</li>
						<li class="left-td"><span class="search-tit">设备序号：</span> 
							<div class="search-main">
								<input type="text" id="id" placeholder="设备序号" />
							</div>
						</li>
						<li class="left-td"><span class="search-tit">设备名称：</span> 
							<div class="search-main">
								<input type="text" id="name" placeholder="设备名称" />
							</div>
						</li>
						<li class="left-td"><span class="search-tit">播控编码：</span> 
							<div class="search-main">
								<input type="text" id="code" placeholder="播控编码" />
							</div>
						</li>
						<li class="left-td"><span class="search-tit">绑定播控：</span> 
							<div class="search-main">
								<div class="layui-form-item">
									<div class="layui-input-block">
										<select id="isBound" lay-verify="required">
											<option value="">请选择</option>
											<option value="0">未绑定</option>
											<option value="1">已绑定</option>
										</select>
									</div>
								</div>
							</div>
						</li>
						<li class="left-td"><span class="search-tit">是否在线：</span> 
							<div class="search-main">
								<div class="layui-form-item">
									<div class="layui-input-block">
										<select id="isOnline" name="isOnline">
											<option value="">请选择</option>
											<option value="1">在线</option>
											<option value="0">离线</option>
										</select>
									</div>
								</div>
							</div>
						</li>
						<li class="left-td"><span class="search-tit">是否启用：</span> 
							<div class="search-main">
								<div class="layui-form-item">
									<div class="layui-input-block">
										<select id="isEnable" lay-verify="required">
											<option value="">请选择</option>
											<option value="1">启用</option>
											<option value="0">禁用</option>
										</select>
									</div>
								</div>
							</div>
						</li>
						<li class="left-td" style="margin-right: 100px;"><span class="search-tit">状态：</span> 
							<div class="search-main">
								<div class="layui-form-item">
									<div class="layui-input-block">
										<select id="status" lay-verify="required">
											<option value="">请选择</option>
											<option value="0">未开通</option>
											<option value="1">正常</option>
											<option value="2">已过期</option>
										</select>
									</div>
								</div>
							</div>
						</li>
					</ul>
					<div class="clear"></div>
					<div class="search-button">
						<button id="queryButton" type="button" class="search-fn-btn">
							<i class="icon-search"></i> 查询
						</button>
					</div>
				</div>
				<div class="step-main">
					<div class="table-responsive">
						<table id="dataTable" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th class="center">设备序号</th>
									<th class="center">设备名称</th>
									<th class="center">公司名称</th>
									<th class="center">设备组</th>
									<th class="center">分辨率</th>
									<th class="center">播控编码</th>
									<th class="center">播控版本</th>
									<th class="center">操作系统</th>
									<th class="center">是否在线</th>
									<th class="center">是否启用</th>
									<!-- <th class="center">审核状态</th> -->
									<th class="center">状态</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
					<div class="page-new clear">
						<div id="pageDiv" align="right" class="pull-right"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>