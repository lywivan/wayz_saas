<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<script type="text/javascript" src="js/position/position_detail_dialog.js?v=<%=System.currentTimeMillis()%>"></script>
<!-- 设备详情 -->
<input id="vPositionId" type="hidden">
<div id="positionViewDialog" class="modal fade">
	<div class="window-box modal-dialog-middle">
		<div class="modal-content">
			<div class="modal-header no-padding">
				<div class="table-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
						<span class="box-close">×</span>
					</button>
					设备详情
				</div>
			</div>

			<div class="modal-body no-padding">
				<div class="page-content">
					<div class="row">
						<div class="col-xs-12">
							<div class="box-main">
								<h4 class="step-tit">
									<i></i>基本信息
								</h4>
								<table class="table detail-tab">
									<tr>
										<td>设备名称：<span id="vPName"></span></td>
										<td>公司名称：<span id="vPCompanyName"></span></td>
										<td>状态：<span id="vPStatus"></span></td>
									</tr>
									<tr>
										<td>设备组：<span id="vGroupName"></span></td>
										<td>分辨率：<span id="vResolutionName"></span></td>
										<td>审核状态：<span id="vPAuditStatus"></span></td>
									</tr>
									<tr>
										<td>区域：<span id="vPArea"></span></td>
										<td>详细地址：<span id="vPAddress"></span></td>
										<td>场景：<span id="vPSceneName"></span></td>
									</tr>
									<tr>
										<td>开机时间：<span id="vPStartTime"></span></td>
										<td>关机时间：<span id="vPCloseTime"></span></td>
										<td>绑定播控：<span id="vPIsBound"></span></td>
									</tr>
									<tr>
										<td>播控编码：<span id="vPPlayCode"></span></td>
										<td>播控版本：<span id="vPPackageVersion"></span></td>
										<td>操作系统：<span id="vPOSDesc"></span></td>
									</tr>
									<tr>
										<td>是否在线：<span id="vPIsOnLine"></span></td>
										<td>是否可用：<span id="vPIsEnable"></span></td>
										<td>创建时间：<span id="vPCreatedTime"></span></td>
									</tr>
								</table>
							</div>

							<!-- <div class="box-main">
								<h4 class="step-tit">
									<i></i>在刊内容或广告
								</h4>
								<table id="positionPlanTable" class="table table-striped table-bordered">
									<thead>
										<tr>
											<th class="center">名称</th>
											<th class="center">类型</th>
											<th class="center">投放周期</th>
											<th class="center">播放时长(s)</th>
											<th class="center">播放次数</th>
											<th class="center">轮播周期(s)</th>
											<th class="center">投放状态</th>
										</tr>
									</thead>
								</table>
							</div> -->
						</div>
					</div>
				</div>
			</div>

			<div class="modal-footer no-margin-top"></div>
		</div>
	</div>
</div>