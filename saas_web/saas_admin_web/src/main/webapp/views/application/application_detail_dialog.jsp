<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<script type="text/javascript" src="js/application/application_detail_dialog.js?v=<%=System.currentTimeMillis()%>"></script>
<div id="applicationDetailDialog" class="modal fade">
	<div class="window-box modal-dialog">
		<div class="modal-content">
			<div class="modal-header no-padding">
				<div class="table-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
						<span class="box-close">×</span>
					</button>
					【<span id="applicationName"></span>】应用详情
				</div>
			</div>

			<div class="modal-body-max no-padding">
				<div class="page-content">
					<div class="row">
						<div class="col-xs-12">
							<div class="widget-box">
								<div class="box-main">
									<h4 class="step-tit">
										<i></i>应用预览图
									</h4>
									<div class="widget-main">
										<div style="width: 100%; height: 300px; position: relative;">
											<div id="materialDetailCanvasZone" style="position: absolute; width:100%;height:100%;top:0px;left:0px"></div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="widget-box">
								<div class="box-main">
									<h4 class="step-tit">
										<i></i>应用信息
									</h4>
									<div class="widget-main">
										<table class="table detail-tab">
											<tr>
												<td width="100px" align="right">应用名称：</td>
												<td><span id="vName"></span></td>
												<td width="100px" align="right">应用图标：</td>
												<td><span id="vIcon"></span></td>
											</tr>
											<tr>
												<td align="right">应用链接：</td>
												<td><span id="vHref"></span></td>
												<td align="right">应用星级：</td>
												<td><span id="vStarNum" class="star"></span></td>
											</tr>
											<tr>
												<td align="right">使用量：</td>
												<td><span id="vBuyCount"></span></td>
												<td align="right">状态：</td>
												<td><span id="vIsEnable"></span></td>
											</tr>
											<tr>
												<td align="right">应用简介：</td>
												<td colspan="3"><span id="vDescription"></span></td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="modal-footer no-margin-top">
			</div>
		</div>
	</div>
</div>

