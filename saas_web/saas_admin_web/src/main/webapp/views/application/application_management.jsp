<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../common_taglibs.jsp"%>
<%@ include file="application_detail_dialog.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>应用管理</title>
<script src="plugin/alioss/aliyun-oss-sdk-4.4.4.min.js"></script>
<script type="text/javascript" src="js/application/application_management.js?v=<%=System.currentTimeMillis()%>"></script>
</head>
<body>
	<div class="page-content">
		<div class="row">
			<div class="col-xs-12">
				<div class="step-main">
					<ul class="search-top layui-form">
						<li class="left-td"><span class="search-tit">应用名称：</span>
							<div class="search-main">
								<input type="text" class="layui-input" id="name" autocomplete="off" placeholder="应用名称" />
							</div>
						</li>
						<li class="left-td"><span class="search-tit">星级：</span>
							<div class="search-main">
								<select id="starNum" data-placeholder="请选择">
									<option value="">请选择</option>
									<option value="1">一星</option>
									<option value="2">二星</option>
									<option value="3">三星</option>
									<option value="4">四星</option>
									<option value="5">五星</option>
								</select>
							</div>
						</li>
					</ul>
					<div class="clear"></div>
					<div class="search-button">
						<button id="queryButton" type="button" class="search-fn-btn">
							<i class="icon-search"></i> 查询
						</button>
					</div>
				</div>
				<div id="toolbarDiv">
					<c:if test="${fn:contains($rightList,'saas/application/deleteApplication')}">
						<input type="hidden" id="delete" value="1" />
					</c:if>
					<c:if test="${fn:contains($rightList,'saas/application/createApplication')}">
						<button id="createButton" type="button" class="btn btn-step">
							<i class="icon-credit-card"></i> 创建应用
						</button>
					</c:if>
				</div>
				<div class="step-main">
					<div class="col-xs-12" id="applicationDiv"></div>
					<div class="page-new clear">
						<div id="pageDiv" align="right" class="pull-right"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="createApplicationDialog" class="modal fade">
		<div class="window-box modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						创建应用
					</div>
				</div>

				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="createApplicationForm" method="post" enctype="multipart/form-data"
									class="form-horizontal" role="form">
									<input type="hidden" id="cAppPreUrl">
									<table class="table detail-tab layui-form">
										<tr>
											<td width="100px" align="right"><span class="required_style">*</span>应用名称：</td>
											<td><input id="cName" type="text" style="width: 264px" placeholder="应用名称" /></td>
										</tr>
										<tr>
											<td width="100px" align="right"><span class="required_style">*</span>应用图标：</td>
											<td><input id="cIcon" type="text" style="width: 264px" placeholder="应用图标" /></td>
										</tr>
										<tr>
											<td width="100px" align="right"><span class="required_style">*</span>应用链接：</td>
											<td><input id="cHref" type="text" style="width: 264px" placeholder="应用链接" /></td>
										</tr>
										<tr>
											<td width="100px" align="right"><span class="required_style">*</span>星级：</td>
											<td>
												<div class="layui-form-item">
												    <div class="layui-input-block" style="width: 264px">
													   <select id="cStarNum" data-placeholder="请选择">
															<option value="">请选择</option>
															<option value="1">一星</option>
															<option value="2">二星</option>
															<option value="3">三星</option>
															<option value="4">四星</option>
															<option value="5">五星</option>
														</select>
													</div> 	
												</div>
											</td>
										</tr>
										<tr>
											<td align="right">
												<span class="required_style">*</span>
												应用预览图：
											</td>
											<td align="left" class="requirements" style="vertical-align: top; position: relative;">
												<input type="file" id="cApplicationPreviewFile" onchange="selectFileToOss(this.id,'cFileDivListPre',0);"
													style="position: absolute; clip: rect(0, 0, 0, 0);" />
												<button type="button" class="btn btn-step btn-step-c1"
													onclick="doContTriggerClick('cApplicationPreviewFile');">选择预览图</button>
												<span>用于封面图展示，建议尺寸：320*180  大小 ≤ 0.5M</span>
											</td>
										</tr>
										<tr>
											<td align="right" style='width: 82px'></td>
											<td align="left" style="vertical-align: top;">
												<div id="cFileDivListPre"></div>
											</td>
										</tr>
										<tr>
											<td width="100px" align="right"><span class="required_style">*</span>应用简介：</td>
											<td align="left"><textarea id="cDescription" style="width: 264px;height:50%" placeholder="应用简介"></textarea></td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmCreateApplicationButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn btn-fn btn-delect" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>
	
	<div id="modifyApplicationDialog" class="modal fade">
		<div class="window-box modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						编辑应用
					</div>
				</div>

				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="modifyApplicationForm" method="post" enctype="multipart/form-data"
									class="form-horizontal" role="form">
									<input type="hidden" id="mId">
									<input type="hidden" id="mAppPreUrl">
									<table class="table detail-tab layui-form">
										<tr>
											<td width="100px" align="right"><span class="required_style">*</span>应用名称：</td>
											<td><input id="mName" type="text" style="width: 264px" placeholder="应用名称" /></td>
										</tr>
										<tr>
											<td width="100px" align="right"><span class="required_style">*</span>应用图标：</td>
											<td><input id="mIcon" type="text" readonly style="width: 264px" placeholder="应用图标" /></td>
										</tr>
										<tr>
											<td width="100px" align="right"><span class="required_style">*</span>应用链接：</td>
											<td><input id="mHref" type="text" readonly style="width: 264px" placeholder="应用链接" /></td>
										</tr>
										<tr>
											<td width="100px" align="right"><span class="required_style">*</span>星级：</td>
											<td>
												<div class="layui-form-item">
												    <div class="layui-input-block" style="width: 264px">
													   <select id="mStarNum" data-placeholder="请选择">
															<option value="">请选择</option>
															<option value="1">一星</option>
															<option value="2">二星</option>
															<option value="3">三星</option>
															<option value="4">四星</option>
															<option value="5">五星</option>
														</select>
													</div> 	
												</div>
											</td>
										</tr>
										<tr>
											<td align="right">
												<span class="required_style">*</span>
												应用预览图：
											</td>
											<td align="left" class="requirements" style="vertical-align: top; position: relative;">
												<input type="file" id="mApplicationPreviewFile" onchange="selectFileToOss(this.id,'mFileDivListPre',1);"
													style="position: absolute; clip: rect(0, 0, 0, 0);" />
												<button type="button" class="btn btn-step btn-step-c1"
													onclick="doContTriggerClick('mApplicationPreviewFile');">选择预览图</button>
												<span>用于封面图展示，建议尺寸：320*180  大小 ≤ 0.5M</span>
											</td>
										</tr>
										<tr>
											<td align="right" style='width: 82px'></td>
											<td align="left" style="vertical-align: top;">
												<div id="mFileDivListPre"></div>
											</td>
										</tr>
										<tr>
											<td width="100px" align="right"><span class="required_style">*</span>应用简介：</td>
											<td align="left"><textarea id="mDescription" style="width: 264px;height:50%" placeholder="应用简介"></textarea></td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmModifyApplicationButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn btn-fn btn-delect" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>