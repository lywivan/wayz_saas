<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<script type="text/javascript" src="js/cibn/position_detail_dialog.js?v=<%=System.currentTimeMillis()%>"></script>
<!-- 设备详情 -->
<input id="vPositionId" type="hidden">
<div id="positionViewDialog" class="modal fade">
	<div class="window-box modal-dialog">
		<div class="modal-content">
			<div class="modal-header no-padding">
				<div class="table-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
						<span class="box-close">×</span>
					</button>
					设备详情
				</div>
			</div>

			<div class="modal-body-max no-padding">
				<div class="page-content">
					<div class="row">
						<div class="col-xs-12">
							<div class="widget-box">
								<div class="box-main">
									<h4 class="step-tit">
										<i></i>基础信息
									</h4>
									<div class="widget-main">
										<table class="table detail-tab">
											<tr>
												<td>设备名称：<span id="vPName"></span></td>
												<td>媒体组：<span id="vGroupName"></span></td>
											</tr>
											<tr>
												<td>区域：<span id="vPArea"></span></td>
												<td>详细地址：<span id="vPAddress"></span></td>
											</tr>
											<tr>
												<td>开机时间：<span id="vPStartTime"></span></td>
												<td>关机时间：<span id="vPCloseTime"></span></td>
											</tr>
											<tr>
												<td>是否绑定播控：<span id="vPIsBound"></span></td>
												<td>播控编码：<span id="vPPlayCode"></span></td>
											</tr>
											<tr>
												<td>分辨率：<span id="vResolutionName"></span></td>
												<td>是否可用：<span id="vPIsEnable"></span></td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="modal-footer no-margin-top"></div>
		</div>
	</div>
</div>