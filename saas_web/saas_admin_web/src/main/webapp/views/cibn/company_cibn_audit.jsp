<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../common_taglibs.jsp"%>
<%@ include file="cibn_detail_dialog.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>CIBN审核管理</title>
<script type="text/javascript" src="js/cibn/company_cibn_audit.js?v=<%=System.currentTimeMillis()%>"></script>
</head>
<body>
	<div class="page-content">
		<div class="row">
			<div class="col-xs-12">
				<div class="step-main">
					<ul class="search-top layui-form">
						<li class="left-td"><span class="search-tit">公司名称：</span>
							<div class="search-main">
								<input type="text" id="companyName" placeholder="公司名称" />
							</div>
						</li>
						<li class="left-td">
							<span class="search-tit">行业：</span>
							<div class="search-main">
								<div class="layui-form-item">
									<div class="layui-input-block">
										<select id="industryId" lay-verify="required" lay-search>
											<option value="">请选择</option>
											<c:if test="${not empty industryList}">
												<c:forEach var="vo" items="${industryList}">
													<option value="${vo.id}">${vo.name}</option>
												</c:forEach>
											</c:if>
										</select>
									</div>
								</div>
							</div>
						</li>
						<li class="left-td">
							<span class="search-tit">审核状态：</span>
							<div class="search-main">
								<div class="layui-form-item">
									<div class="layui-input-block">
										<select id="auditStatus" lay-verify="required" lay-search>
											<option value="">请选择</option>
											<c:if test="${not empty auditStatusList}">
												<c:forEach var="vo" items="${auditStatusList}">
													<option value="${vo.id}">${vo.name}</option>
												</c:forEach>
											</c:if>
										</select>
									</div>
								</div>
							</div>
						</li>
					</ul>
					<div class="clear"></div>
					<div class="search-button">
						<button id="queryButton" type="button" class="search-fn-btn">
							<i class="icon-search"></i> 查询
						</button>
					</div>
					<div class="clear"></div>
				</div>
				<div id="toolbarDiv"> </div>
				<div class="step-main">
					<div class="tab-main">
						<table id="dataTable" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th class="center">CIBN公司名称</th>
									<th class="center">行业</th>
									<th class="center">审核状态</th>
									<th class="center">申请时间</th>
									<th class="center operationColumn">操作</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
					<div class="page-new clear">
						<div id="pageDiv" align="right" class="pull-right"></div>
					</div>			
				</div>
			</div>
		</div>
	</div>
	
	<div id="cibnAuditRejectDialog" class="modal fade">
		<div class="window-box modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						CIBN请驳回申
					</div>
				</div>

				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="cibnAuditRejectForm" class="form-horizontal" role="form">
									<input type="hidden" id="mId" name="mId" />
									<table class="table details-tab">
										<tr>
											<td style="width: 100px;" align="right"><span class="required_style">*</span>驳回原因：</td>
											<td><textarea id="remark" rows="3" class="col-sm-10" placeholder="此处录入驳回原因"></textarea></td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="auditRejectButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn btn-fn btn-delect" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>