<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../common_taglibs.jsp"%>
<%@ include file="position_detail_dialog.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>设备CIBN审核管理</title>
<script type="text/javascript" src="js/cibn/position_cibn_audit.js?v=<%=System.currentTimeMillis()%>"></script>
</head>
<body>
	<div class="page-content">
		<div class="row">
			<div class="col-xs-12">
				<div class="step-main">
					<ul class="search-top layui-form">
						<li class="left-td"><span class="search-tit">设备：</span>
							<div class="search-main">
								<input type="text" id="name" placeholder="设备名称" />
							</div>
						</li>
						<li class="left-td">
							<span class="search-tit">审核状态：</span>
							<div class="search-main">
								<div class="layui-form-item">
									<div class="layui-input-block">
										<select id="auditStatus" lay-verify="required" lay-search>
											<option value="">请选择</option>
											<c:if test="${not empty auditStatusList}">
												<c:forEach var="vo" items="${auditStatusList}">
													<c:if test="${vo.id != 0}">
														<option value="${vo.id}">${vo.name}</option>
													</c:if>
												</c:forEach>
											</c:if>
										</select>
									</div>
								</div>
							</div>
						</li>
					</ul>
					<div class="clear"></div>
					<div class="search-button">
						<button id="queryButton" type="button" class="search-fn-btn">
							<i class="icon-search"></i> 查询
						</button>
					</div>
				</div>

				<div class="space-4"></div>
				<div class="step-main">
					<div class="table-responsive">
						<table id="dataTable" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th class="center">设备名称</th>
									<th class="center">屏幕尺寸</th>
									<th class="center">场景图</th>
									<th class="center">详细地址</th>
									<th class="center">备案状态</th>
									<th class="center operationColumn">操作</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
					<div class="page-new clear">
						<div id="pageDiv" align="right" class="pull-right"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="cibnAuditRejectDialog" class="modal fade">
		<div class="window-box modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						驳回审核
					</div>
				</div>

				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="cibnAuditRejectForm" class="form-horizontal" role="form">
									<input type="hidden" id="hidPositionId" name="hidPositionId" />
									<input type="hidden" id="hidCompanyId" name="hidCompanyId" />
									<table class="table details-tab">
										<tr>
											<td style="width: 100px;" align="right"><span class="required_style">*</span>驳回原因：</td>
											<td><textarea id="remark" rows="3" class="col-sm-10" placeholder="此处录入驳回原因"></textarea></td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="auditRejectButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn btn-fn btn-delect" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>