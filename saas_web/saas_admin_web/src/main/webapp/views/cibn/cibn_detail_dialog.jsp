<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<meta name="referrer" content="never">
<script type="text/javascript" src="js/cibn/cibn_detail_dialog.js?v=<%=System.currentTimeMillis()%>"></script>
<div id="cibnDetailDialog" class="modal fade">
	<div class="window-box modal-dialog">
		<div class="modal-content">
			<div class="modal-header no-padding">
				<div class="table-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
						<span class="box-close">×</span>
					</button>
					CIBN申请详情
				</div>
			</div>

			<div class="modal-body-max no-padding">
				<div class="page-content">
					<div class="row">
						<div class="col-xs-12">
							<table class="table detail-tab">
								<tr>
									<td style="width:100px" align="right">公司名称：</td>
									<td><span id="vCompanyName"></span></td>
								</tr>
								<tr>
									<td style="width:100px" align="right">所属行业：</td>
									<td><span id="vIndustryName"></span></td>
								</tr>
								<tr>
									<td align="right">身份证正面图：</td>
									<td><span id="vIdentityBverse"></span></td>
								</tr>
								<tr>
									<td align="right">身份证反面图：</td>
									<td><span id="vIdentityReverse"></span></td>
								</tr>
								<tr>
									<td align="right">营业执照图：</td>
									<td><span id="vBusinessLicense"></span></td>
								</tr>
								<tr>
									<td align="right">审核状态：</td>
									<td><span id="vAuditStatus"></span></td>
								</tr>
								<tr>
									<td align="right">驳回原因：</td>
									<td><span id="vRemark"></span></td>
								</tr>
								<tr>
									<td align="right">申请时间：</td>
									<td><span id="vCreatedTime"></span></td>
								</tr>
							</table>	
						</div>
					</div>
				</div>
			</div>

			<div class="modal-footer no-margin-top">
			</div>
		</div>
	</div>
</div>

