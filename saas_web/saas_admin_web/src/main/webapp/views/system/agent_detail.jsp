<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<script type="text/javascript" src="js/system/agent_detail.js?v=<%=System.currentTimeMillis()%>"></script>
<div id="agentDetailDialog" class="modal fade">
	<div class="window-box modal-dialog">
		<div class="modal-content">
			<div class="modal-header no-padding">
				<div class="table-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
						<span class="box-close">×</span>
					</button>
					详情
				</div>
			</div>

			<div class="modal-body no-padding">
				<div class="page-content">
					<div class="row">
						<div class="col-xs-12">
							<h4 class="step-tit">
								<i></i>
								基本信息
							</h4>
							<table class="table detail-tab">
								<tr>
									<td>代理商名称：<span id="vDAgentName"></span></td>
									<td>代理商类型：<span id="vAgentProxyType"></span></td>
								</tr>
								<tr>
									<td class="agent-company-data">联系人：<span id="vAgentContacts"></span></td>
									<td>联系电话：<span id="vAgentPhone"></span></td>
								</tr>
								<tr>
									<td>屏幕购买量：<span id="vAgentPositionLimit"></span></td>
									<td>屏幕成交量：<span id="vAgentConsumePositionLimit"></span></td>
								</tr>
								<tr>
									<td>行业：<span id="vAgentCategoryName"></span></td>
									<td>区域：<span id="vAgentArea"></span></td>
								</tr>
								<tr>
									<td>状态：<span id="vAgentStatus"></span></td>
									<td>备注：<span id="vAgentDescription"></span></td>
								</tr>
								<tr class="agent-personal-data">
									<td>身份证正面图：<div id="vAgentIdentityBverse"></div></td>
									<td>身份证反面图：<div id="vAgentIdentityReverse"></div></td>
								</tr>
								<tr class="agent-company-data">
									<td>营业执照图：<div id="vAgentBusinessLicense"></div></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>

			<div class="modal-footer no-margin-top"></div>
		</div>
	</div>
</div>