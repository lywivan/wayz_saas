<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../common_taglibs.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta charset="utf-8" />
	<title>直播流广告列表</title>
	<script type="text/javascript" src="js/system/live_plan_list.js?v=<%=System.currentTimeMillis()%>"></script>
</head>
<body>
<div class="page-content">
	<div class="row">
		<div class="col-xs-12">
			
			<div id="toolbarDiv">
			</div>
			<div class="step-main">
				<div class="table-responsive">
					<table id="dataTable" class="table table-striped table-bordered">
						<thead>
						<tr>
							<th class="center">直播名称</th>
							<th class="center">开始时间</th>
							<th class="center">结束时间</th>
							<th class="center">直播渠道</th>
							<th class="center">直播类型</th>
							<th class="center">客户名称</th>
							<th class="center">媒体名称</th>
							<th class="center">广告金额(元)</th>
							<th class="center">媒体结算(元)</th>
							<th class="center">设备数量</th>
							<th class="center">状态</th>
							<th width="100px" class="center">操作</th>
						</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				<div class="page-new clear">
					<div id="pageDiv" align="right" class="pull-right"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="materialDetailDialog" class="modal fade">
	<div class="window-box modal-dialog">
		<div class="modal-content">
			<div class="modal-header no-padding">
				<div class="table-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
						<span class="box-close">×</span>
					</button>
					【<span id="materialName"></span>】直播预览
				</div>
			</div>

			<div class="modal-body-max no-padding">
				<div class="page-content">
					<div class="row">
						<div class="col-xs-12">
							<div class="box-main">
								<div class="widget-main">
									<div style="width: 100%; height: 100%; position: relative;">
										<div id="materialDetailCanvasZone" style="position: absolute; width:100%;height:100%;top:0px;left:0px"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="modal-footer no-margin-top">
				<button type="button" class="btn btn-fn btn-delect" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</div>
</div>
</body>
</html>
