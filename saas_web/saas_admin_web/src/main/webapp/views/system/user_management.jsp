<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../common_taglibs.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>用户管理</title>
<script type="text/javascript" src="js/system/user_management.js?v=<%=System.currentTimeMillis()%>"></script>
</head>
<body>
	<div class="page-content">
		<div class="row">
			<div class="col-xs-12">
				<div class="step-main">
					<ul class="search-top layui-form">
						<li class="left-td">
							<span class="search-tit">用户姓名：</span>
							<div class="search-main">
								<input type="text" id="name" placeholder="用户姓名" />
							</div>
						</li>
						<li class="left-td" style="margin-right: 100px;"><span class="search-tit">状态：</span>
							<div class="search-main">
								<div class="layui-form-item">
									<div class="layui-input-block">
										<select id="status">
											<option value="">请选择</option>
											<option value="1">启用</option>
											<option value="0">禁用</option>
										</select>
									</div>
								</div>
							</div>
						</li>
					</ul>
					<div class="clear"></div>
					<div class="search-button">
						<c:if test="${fn:contains($rightList,'user/queryUser')}">
							<button id="queryButton" type="button" class="search-fn-btn">
								<i class="icon-search"></i> 查询
							</button>
						</c:if>
					</div>
				</div>
				<div id="toolbarDiv">
					<c:if test="${fn:contains($rightList,'user/createUser')}">
						<button id="createButton" type="button" class="btn btn-step">
							<i class="icon-credit-card"></i> 创建
						</button>
					</c:if>
					<c:if test="${fn:contains($rightList,'user/modifyUser')}">
						<input type="hidden" id="modify" value="1" />
					</c:if>
					<c:if test="${fn:contains($rightList,'user/deleteUser')}">
						<input type="hidden" id="delete" value="1" />
					</c:if>
				</div>
				<div class="step-main">
					<div class="table-responsive">
						<table id="userTable" class="table table-striped table-bordered">
							<thead>
								<tr>	
									<th class="center">用户名</th>
									<th class="center">用户角色</th>
									<th class="center">用户姓名</th>
									<th class="center">手机号码</th>
									<th class="center">邮箱</th>
									<th class="center">用户状态</th>
									<th class="center">创建时间</th>
									<th class="center operationColumn">操作</th>
								</tr>
							</thead>
							<tbody>

							</tbody>
						</table>
					</div>
					<div class="page-new clear">
						<div id="pageDiv" align="right" class="pull-right"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="createUserDialog" class="modal fade">
		<div class="window-box modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						添加用户
					</div>
				</div>

				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="addUserForm" class="form-horizontal" role="form">
									<input id="cStatus" type="hidden" value="1" /> 
									<table class="table detail-tab layui-form">
										<tr>
											<td align="right"><span class="required_style">*</span>用户角色：</td>
											<td>
												<div class="layui-form-item">
													<div class="layui-input-block" style="width: 50%">
														<select id="cRoleId" lay-verify="required" lay-search>
															<option value="">请选择</option>
															<c:forEach var="entry" items="${roleList}">
																<option value="${entry.id}">${entry.name}</option>
															</c:forEach>
														</select>
													</div>
												</div>
											</td>
										</tr>
										<tr>
											<td width="100px" align="right"><span class="required_style">*</span>用户名：</td>
											<td><input id="cUsername" type="text" class="col-sm-6" placeholder="用户名" /></td>
										</tr>
											<tr>
											<td align="right"><span class="required_style">*</span>用户密码：</td>
											<td><input id="cPassword" type="password" class="col-sm-6" placeholder="用户密码"
												onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>确认密码：</td>
											<td><input id="cChkpwd" type="password" class="col-sm-6" placeholder="确认密码"
												onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" onblur="verificationPassword()" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>用户姓名：</td>
											<td><input id="cName" type="text" class="col-sm-6" placeholder="用户姓名" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>手机号码：</td>
											<td><input id="cPhone" type="text" class="col-sm-6" placeholder="手机号码 " /></td>
										</tr>
										<tr>
											<td align="right">联系邮箱：</td>
											<td><input id="cEmail" type="text" class="col-sm-6" placeholder="联系邮箱" /></td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmCreateButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>

	<div id="modifyUserDialog" class="modal fade">
		<div class="window-box modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						编辑用户
					</div>
				</div>

				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="modifyUserForm" class="form-horizontal" role="form">
									<input id="mId" type="hidden" />
									<input id="mStatus" type="hidden" />
									<table class="table detail-tab layui-form">
										<tr>
											<td width="100px" align="right"><span class="required_style">*</span>用户名：</td>
											<td align="left" height="35px"><span id="mUsername"></span></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>用户角色：</td>
											<td align="left" height="35px"><span id="mRoleName"></span> <input id="mRoleId" type="hidden" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>用户姓名：</td>
											<td><input id="mName" type="text" class="col-sm-6" placeholder="用户姓名" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>手机号码：</td>
											<td height="35px"><input id="mPhone" type="text" class="col-sm-6" placeholder="手机号码" /></td>
										</tr>
										<tr>
											<td align="right">联系邮箱：</td>
											<td><input id="mEmail" type="text" class="col-sm-6" placeholder="联系邮箱" /></td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmModifyButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
