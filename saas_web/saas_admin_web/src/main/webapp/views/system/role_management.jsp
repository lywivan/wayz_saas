<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../common_taglibs.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>角色管理</title>
<link rel="stylesheet" href="css/zTreeStyle/zTreeStyle.css" type="text/css">
<script type="text/javascript" src="plugin/ztree/jquery.ztree.core.js"></script>
<script type="text/javascript" src="plugin/ztree/jquery.ztree.excheck.js"></script>
<!-- <script type="text/javascript" src="plugin/ztree/jquery.ztree.exedit.js"></script> -->
<script type="text/javascript" src="js/system/role_management.js?v=<%=System.currentTimeMillis()%>"></script>
</head>
<body>
	<div class="page-content">
		<div class="row">
			<div class="col-xs-12">
				<div class="step-main">
					<ul class="search-top layui-form">
						<li class="left-td"><span class="search-tit">角色名称：</span>
							<div class="search-main">
								<input autocomplete="off" id="name" type="text" class="uniform input-large" placeholder="角色名称" />
							</div>
						</li>
						<%-- <li class="left-td"><span class="search-tit">版本：</span>
							<div class="search-main">
								<div class="layui-form-item">
									<div class="layui-input-block">
										<select id="version" lay-verify="required" lay-search>
											<option value="">请选择</option>
											<c:forEach var="entry" items="${versionList}">
												<option value="${entry.id}">${entry.name}</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>
						</li>
						<li class="left-td" style="margin-right: 100px;"><span class="search-tit">应用：</span>
							<div class="search-main">
								<div class="layui-form-item">
									<div class="layui-input-block">
										<select id="application" lay-verify="required" lay-search>
											<option value="">请选择</option>
											<c:forEach var="entry" items="${applicationList}">
												<option value="${entry.id}">${entry.name}</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>
						</li> --%>
					</ul>
					<div class="clear"></div>
					<div class="search-button">
						<c:if test="${fn:contains($rightList,'role/queryRole')}">
							<button id="queryButton" type="button" class="search-fn-btn">
								<i class="icon-search"></i> 查询
							</button>
						</c:if>
					</div>
				</div>
				<div id="toolbarDiv">
					<c:if test="${fn:contains($rightList,'role/createRole')}">
						<button id="createButton" type="button" class="btn btn-step">
							<i class="icon-credit-card"></i> 创建
						</button>
					</c:if>
					<c:if test="${fn:contains($rightList,'role/modifyRole')}">
						<input id="modify" type="hidden" value="1" />
					</c:if>
					<c:if test="${fn:contains($rightList,'role/authorityRole')}">
						<input id="setRoleRight" type="hidden" value="1" />
					</c:if>
					<c:if test="${fn:contains($rightList,'role/deleteRole')}">
						<input id="delete" type="hidden" value="1" />
					</c:if>
				</div>
				<div class="step-main">
					<div class="table-responsive">
						<table id="roleTable" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th class="center">角色名称</th>
									<th class="center">角色描述</th>
									<th class="center">操作</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
					<div class="page-new clear">
						<div id="pageDiv" align="right" class="pull-right"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="createRoleDialog" class="modal fade">
		<div class="window-box modal-dialog-middle">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						添加角色
					</div>
				</div>

				<div class="modal-body-middle no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="addRoleForm" class="form-horizontal" role="form">
									<table class="table detail-tab layui-form">
										<thead>
											<tr>
												<td width="100px" align="right"><span class="required_style">*</span>角色名称：</td>
												<td><input id="cName" name="cName" type="text" class="col-sm-3" placeholder="角色名称" /></td>
											</tr>
										</thead>
										<tr>
											<td align="right">角色描述：</td>
											<td><textarea id="cDescription" style="height: 50%; width: 50%" placeholder="角色描述"></textarea></td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmCreateButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>

	<div id="modifyRoleDialog" class="modal fade">
		<div class="window-box modal-dialog-middle">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						编辑角色
					</div>
				</div>

				<div class="modal-body-middle no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="modifyRoleForm" method="post" action="authority/modifyRole" class="form-horizontal" role="form">
									<input id="mId" type="hidden" /> <input id="mType" type="hidden" /> <input id="mParentId" type="hidden" />
									<table class="table detail-tab layui-form">
										<thead>
											<tr>
												<td width="100px" align="right"><span class="required_style">*</span>角色名称：</td>
												<td><input id="mName" name="mName" type="text" class="col-sm-3" placeholder="角色名称" /></td>
											</tr>
										</thead>
										<tr>
											<td align="right">角色描述：</td>
											<td><textarea id="mDescription" style="height: 50%; width: 50%" placeholder="角色描述"></textarea></td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmModifyButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>

	<div id="setRoleRightDialog" class="modal fade">
		<div class="window-box modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						<i class="icon-credit-card"></i> 设置角色 <span id="roleInfo"></span> 功能权限
					</div>
				</div>

				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="modifyRoleRightForm" method="post" action="manager/setRoleMenu" class="form-horizontal" role="form">
									<input id="roleId" name="roleId" type="hidden" /> 
									<input id="roleName" name="roleName" type="hidden" />
									<div style="height: 520px; text-align: left; overflow: auto;">
										<ul id="roleRightTree" class="ztree"></ul>
									</div>
									<div class="space-4"></div>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmSetRoleRightButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
