<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../common_taglibs.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>系统通知管理</title>
<script type="text/javascript" src="plugin/wangEditor/release/wangEditor.min.js"></script>
<script type="text/javascript" src="js/system/system_notice_management.js?v=<%=System.currentTimeMillis()%>"></script>
</head>
<body>
	<div class="page-content">
		<div class="row">
			<div class="col-xs-12">
				<div class="step-main">
					<ul class="search-top layui-form">
						<li class="left-td"><span class="search-tit">标题：</span>
							<div class="search-main">
								<input id="title" type="text" class="uniform input-large" placeholder="标题" />
							</div>
						</li>
					</ul>
					<div class="clear"></div>
					<div class="search-button">
						<c:if test="${fn:contains($rightList,'saas/notice/queryNotice')}">
							<button id="queryButton" type="button" class="search-fn-btn">
								<i class="icon-search"></i> 查询
							</button>
						</c:if>
					</div>
				</div>
				<div id="toolbarDiv">
					<c:if test="${fn:contains($rightList,'saas/notice/createNotice')}">
						<button id="createButton" type="button" class="btn btn-step">
							<i class="icon-credit-card"></i> 创建
						</button>
					</c:if>
					<c:if test="${fn:contains($rightList,'saas/notice/modifyNotice')}">
						<input id="modify" type="hidden" value="1" />
					</c:if>
					<c:if test="${fn:contains($rightList,'saas/notice/deleteNotice')}">
						<input id="delete" type="hidden" value="1" />
					</c:if>
				</div>
				<div class="step-main">
					<div class="table-responsive">
						<table id="SysNoticeTable" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th class="center">标题</th>
									<!-- <th class="center">消息内容</th> -->
									<th class="center">状态</th>
									<th class="center">推送时间</th>
									<th class="center">操作</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
					<div class="page-new clear">
						<div id="pageDiv" align="right" class="pull-right"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="createSysNoticeDialog" class="modal fade">
		<div class="window-box modal-dialog-middle">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						创建通知
					</div>
				</div>

				<div class="modal-body-middle no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="addSysNoticeForm" class="form-horizontal" role="form">
									<table class="table detail-tab layui-form">
										<tr>
											<td width="100px" align="right"><span class="required_style">*</span>标题：</td>
											<td><input id="cTitle" type="text" class="col-sm-3" placeholder="标题" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>通知内容：</td>
											<td>
												<div id="cContent"></div>
											</td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button type="button" data-ispush="1" class="confirmCreateButton btn btn-primary">保存</button>
					<button type="button" data-ispush="2" class="confirmCreateButton btn btn-primary">推送</button>
					<button class="btn" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>

	<div id="modifySysNoticeDialog" class="modal fade">
		<div class="window-box modal-dialog-middle">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						编辑通知
					</div>
				</div>

				<div class="modal-body-middle no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="modifySysNoticeForm" method="post" class="form-horizontal" role="form">
									<input id="mId" type="hidden" />
									<table class="table detail-tab layui-form">
										<tr>
											<td width="100px" align="right"><span class="required_style">*</span>标题：</td>
											<td><input id="mTitle" type="text" class="col-sm-3" placeholder="标题" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>通知内容：</td>
											<td>
												<div id="mContent" style="width:800px;"></div>
											</td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button type="button" data-ispush="1" class="confirmModifyButton btn btn-primary">保存</button>
					<button type="button" data-ispush="2" class="confirmModifyButton btn btn-primary">推送</button>
					<button class="btn" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
