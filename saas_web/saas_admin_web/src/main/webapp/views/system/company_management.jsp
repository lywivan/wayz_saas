<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../common_taglibs.jsp"%>
<%@ include file="company_detail.jsp"%>
<%@ include file="agent_detail.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>公司管理</title>
<style type="text/css">
	.comInpWidth{
		width : 225px;
	}
</style>
<script src="plugin/alioss/aliyun-oss-sdk-4.4.4.min.js"></script>
<script type="text/javascript" src="js/system/company_management.js?v=<%=System.currentTimeMillis()%>"></script>
</head>
<body>
	<input id="companyId" type="hidden" />
	<div class="page-content">
		<div class="row">
			<div class="col-xs-12">
				<div class="step-main">
					<ul class="search-top layui-form">
						<li class="left-td"><span class="search-tit">公司名称：</span>
							<div class="search-main">
								<input autocomplete="off" id="name" type="text" class="col-sm-6" placeholder="公司名称" />
							</div>
						</li>
						<li class="left-td"><span class="search-tit">手机号：</span>
							<div class="search-main">
								<input autocomplete="off" id="phone" type="text" class="col-sm-6" placeholder="手机号" />
							</div>
						</li>
						<li class="left-td"><span class="search-tit">版本：</span>
							<div class="search-main">
								<div class="layui-form-item">
									<div class="layui-input-block">
										<select id="versionId">
											<option value="">请选择</option>
											<c:if test="${not empty versionList}">
												<option value="">请选择</option>
												<c:forEach var="vo" items="${versionList }">
													<option value="${vo.id}">${vo.name}</option>
												</c:forEach>
											</c:if>
										</select>
									</div>
								</div>
							</div>
						</li>
						<li class="left-td"><span class="search-tit">来源：</span>
							<div class="search-main">
								<div class="layui-form-item">
									<div class="layui-input-block">
										<select id="sourceType">
											<option value="">请选择</option>
											<option value="1">后台创建</option>
											<option value="2">前端注册</option>
										</select>
									</div>
								</div>
							</div>
						</li>
						<li class="left-td"><span class="search-tit">代理商：</span>
							<div class="search-main">
								<div class="layui-form-item">
									<div class="layui-input-block">
										<select id="agentId">
											<option value="">请选择</option>
											<c:if test="${not empty agentList}">
												<option value="">请选择</option>
												<c:forEach var="vo" items="${agentList }">
													<option value="${vo.id}">${vo.name}</option>
												</c:forEach>
											</c:if>
										</select>
									</div>
								</div>
							</div>
						</li>
						<!-- <li class="left-td"><span class="search-tit">是否成单：</span>
							<div class="search-main">
								<div class="layui-form-item">
									<div class="layui-input-block">
										<select id="isOrderSuccess">
											<option value="">请选择</option>
											<option value="1">是</option>
											<option value="0">否</option>
										</select>
									</div>
								</div>
							</div>
						</li> -->
						<li class="left-td" style="margin-right: 100px;"><span class="search-tit">账户状态：</span>
							<div class="search-main">
								<div class="layui-form-item">
									<div class="layui-input-block">
										<select id="status">
											<option value="">请选择</option>
											<option value="2">试用中</option>
											<option value="1">正常</option>
											<option value="3">已过期</option>
											<option value="0">禁用</option>
										</select>
									</div>
								</div>
							</div>
						</li>
					</ul>
					<div class="clear"></div>
					<div class="search-button">
						<c:if test="${fn:contains($rightList,'company/queryCompany')}">
							<button id="queryButton" type="button" class="search-fn-btn" style='cursor: pointer;'>
								<i class="icon-search"></i> 查询
							</button>
						</c:if>
					</div>
				</div>
				<div id="toolbarDiv">
					<c:if test="${fn:contains($rightList,'company/createCompany')}">
						<button id="createButton" type="button" class="btn btn-step">
							<i class="icon-credit-card"></i> 创建
						</button>
					</c:if>
					<c:if test="${fn:contains($rightList,'company/modifyCompany')}">
						<input type="hidden" id="modify" value="1" />
					</c:if>
					<c:if test="${fn:contains($rightList,'company/deleteCompany')}">
						<input type="hidden" id="delete" value="1" />
					</c:if>
					<c:if test="${fn:contains($rightList,'company/oemCofing')}">
						<input type="hidden" id="oemCofing" value="1" />
					</c:if>
					<c:if test="${fn:contains($rightList,'saas/company/getCompanyMainAccount')}">
						<input type="hidden" id="thirdLogin" value="1" />
					</c:if>
					<c:if test="${fn:contains($rightList,'saas/company/resetUserPassword')}">
						<input type="hidden" id="modifyUserPwd" value="1" />
					</c:if>
					<c:if test="${fn:contains($rightList,'saas/playManagement/setPlayerGlobalOption')}">
						<input type="hidden" id="playCofingRight" value="1" />
					</c:if>
				</div>
				<div class="step-main">
					<div class="table-responsive">
						<table id="companyTable" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th class="center">名称</th>
									<th class="center">代理商</th>
									<th class="center">来源</th>
									<th class="center">版本</th>
									<th class="center">联系人</th>
									<th class="center">联系电话</th>
									<!-- <th class="center">账户余额(元)</th> -->
									<!-- <th class="center">用户数量</th> -->
									<th class="center">账户状态</th>
									<th class="center">有效期</th>
									<!-- <th class="center">账户名</th> -->
									<th class="center">创建时间</th>
									<th class="center">交易记录</th>
									<th width="350px" class="center operationColumn">操作</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
					<div class="page-new clear">
						<div id="pageDiv" align="right" class="pull-right"></div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>

	<div id="createCompanyDialog" class="modal fade">
		<div class="window-box modal-dialog-middle">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						创建公司
					</div>
				</div>
				
				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form class="form-horizontal" id="addCompanyForm" role="form">
									<h4 class="step-tit">
										<i></i>
										基本信息
									</h4>
									<table class="table detail-tab layui-form">
										<tr>
											<td width="100px" align="right"><span class="required_style">*</span>公司名称：</td>
											<td align="left"><input id="cName" type="text" class="comInpWidth" placeholder="公司名称" /></td>
											<td width="100px" align="right"><span class="required_style">*</span>联系人：</td>
											<td align="left"><input id="cContacts" type="text" class="comInpWidth" placeholder="联系人" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>手机号：</td>
											<td align="left"><input id="cTelephone" type="text" class="comInpWidth" onkeyup="integerChange(this);" placeholder="手机号" /></td>
											<td align="right">联系邮箱：</td>
											<td align="left"><input id="cEmail" type="text" class="comInpWidth" placeholder="联系邮箱" /></td>
										</tr>
										<tr>
											<td align="right">联系地址：</td>
											<td align="left"><input id="cAddress" type="text" class="comInpWidth" placeholder="公司地址" /></td>
										</tr>
									</table>
								
									<h4 class="step-tit">
										<i></i>
										管理账户
									</h4>
									<table class="table detail-tab layui-form">
										<tr>
											<td width="100px"  align="right"><span class="required_style">*</span>账户类型：</td>
											<td align="left">
												 <input name="cStatus" id="cStatus1" lay-filter="cStatusFilter" type="radio" info="正式" value="1" title="正式" />&nbsp;&nbsp; 
                                                 <input name="cStatus" id="cStatus2" lay-filter="cStatusFilter" type="radio" info="试用" value="2" title="试用 " />
											</td>
											<td></td>
											<td></td>
										</tr>
										<tr>
											<td width="100px" nowrap="nowrap" align="right"><span class="required_style">*</span>用户名：</td>
											<td align="left"><input id="cUserName" type="text" class="comInpWidth" placeholder="用户名"/></td>
											<td width="100px" align="right">所属代理商：</td>
											<td align="left">
												<div class="layui-form-item">
													<div class="layui-input-block comInpWidth">
														<select id="cAgentId" lay-filter="cAgentId">
															<option value="">请选择</option>
															<c:if test="${not empty agentList}">
																<option value="">请选择</option>
																<c:forEach var="vo" items="${agentList }">
																	<option value="${vo.id}">${vo.name}</option>
																</c:forEach>
															</c:if>
														</select>
													</div>
												</div>
											</td>
										</tr>
										<tr class="cValidUntilDateTd" >
											<td width="100px" align="right"><span class="required_style">*</span>有效期至：</td>
											<td align="left">
												<input type="text" name="cValidUntilDate" id="cValidUntilDate" placeholder="试用账户有效期">
											</td>
                                        	<td width="100px" align="right">播控加水印：</td>
                                            <td align="left">
                                            	<input type="checkbox" id="cIsWatermark" lay-skin="primary">
                                            <td>
                                        </tr>
										<tr class="isLoginTd">	
											<td width="100px" align="right">一键登录：</td>
											<td align="left">
												<input type="checkbox" name="cIsLogin" id="cIsLogin" lay-skin="primary">
											</td>
										</tr>
									</table>
							
									<h4 class="step-tit">
										<i></i>
										服务配置
									</h4>
									<table class="table detail-tab layui-form">
										<tr>
											<td width="100px" align="right"><span class="required_style">*</span>版本：</td>
											<td align="left">
												<div class="layui-form-item">
												    <div class="layui-input-block comInpWidth">
													   <select id="cVersionId" lay-filter="cVersionId" lay-verify="required" lay-search>
													   		<option value="">请选择</option>
															<c:forEach var="entry" items="${versionList }">
																<option value="${entry.id}" userLimit="${entry.userLimit}" materialLimit="${entry.materialLimit}">${entry.name}</option>
															</c:forEach>
														</select>
													</div> 	
												</div>
											</td>
											<td align="right"><span class="required_style">*</span>授权用户数：</td>
											<td align="left"><input id="cUserLimit" type="text" onkeyup="integerChange(this);" class="comInpWidth" placeholder="创建用户数量的最大上限(单位/个)" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>素材总空间(MB)：</td>
											<td align="left"><input id="cMaterialTotalLimit" type="text" onkeyup="integerChange(this);" class="comInpWidth" placeholder="素材空间的最大上限(单位/MB)" /></td>
											<td align="right">广告投放：</td>
											<td align="left">
												<input type="checkbox" name="cRequirePlan" id="cRequirePlan" lay-skin="primary">
											</td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmCreateButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>

	<div id="modifyCompanyDialog" class="modal fade">
		<div class="window-box modal-dialog-middle">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						编辑公司
					</div>
				</div>
				
				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form class="form-horizontal" id="modifyCompanyForm" role="form">
									<input type="hidden" id="mId" />
									<h4 class="step-tit">
										<i></i>
										基本信息
									</h4>
									<table class="table detail-tab layui-form">
										<tr>
											<td width="100px" align="right"><span class="required_style">*</span>公司名称：</td>
											<td align="left"><input id="mName" type="text" class="comInpWidth" placeholder="公司名称" /></td>
											<td width="100px" align="right"><span class="required_style">*</span>联系人：</td>
											<td align="left"><input id="mContacts" type="text" class="comInpWidth" placeholder="联系人" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>手机号：</td>
											<td align="left"><input id="mTelephone" type="text" class="comInpWidth" placeholder="手机号" /></td>
											<td align="right">联系邮箱：</td>
											<td align="left"><input id="mEmail" type="text" class="comInpWidth" placeholder="联系邮箱" /></td>
										</tr>
										<tr>
											<td align="right">联系地址：</td>
											<td align="left"><input id="mAddress" type="text" class="comInpWidth" placeholder="公司地址" /></td>
										</tr>
									</table>
									
									<h4 class="step-tit">
										<i></i>
										管理账户
									</h4>
									<table class="table detail-tab layui-form">
										<tr>
											<td width="100px" nowrap="nowrap" align="right"><span class="required_style">*</span>用户名：</td>
											<td align="left"><input id="mUserName" type="text" class="comInpWidth" readonly placeholder="用户名"/></td>
											<td width="100px" align="right">所属代理商：</td>
											<td align="left">
												<div class="layui-form-item">
													<div class="layui-input-block comInpWidth">
														<select id="mAgentId" lay-filter="mAgentId">
															<option value="">请选择</option>
															<c:if test="${not empty agentList}">
																<option value="">请选择</option>
																<c:forEach var="vo" items="${agentList }">
																	<option value="${vo.id}">${vo.name}</option>
																</c:forEach>
															</c:if>
														</select>
													</div>
												</div>
											</td>
										</tr>
										<tr>
											<td class="isLoginTd" width="100px" align="right">一键登录：</td>
											<td class="isLoginTd" align="left">
												<input type="checkbox" name="mIsLogin" id="mIsLogin" lay-skin="primary">
											</td>
										</tr>
									</table>
							
									<h4 class="step-tit">
										<i></i>
										服务配置
									</h4>
									<table class="table detail-tab layui-form">
										<tr>
											<td width="100px" align="right"><span class="required_style">*</span>版本：</td>
											<td align="left">
												<div class="layui-form-item">
												    <div class="layui-input-block comInpWidth">
													   <select id="mVersionId" lay-filter="mVersionId">
													   		<option value="">请选择</option>
															<c:forEach var="entry" items="${versionList }">
																<option value="${entry.id}" userLimit="${entry.userLimit}" materialLimit="${entry.materialLimit}">${entry.name}</option>
															</c:forEach>
														</select>
													</div> 	
												</div>
											</td>
											<td width="100px" align="right"><span class="required_style">*</span>授权用户数：</td>
											<td align="left"><input id="mUserLimit" type="text"onkeyup="integerChange(this);" class="comInpWidth" placeholder="创建用户数量的最大上限(单位/个)" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>素材总空间(MB)：</td>
											<td align="left"><input id="mMaterialTotalLimit" type="text" onkeyup="integerChange(this);" class="comInpWidth" placeholder="素材空间的最大上限(单位/MB)" /></td>
											<td align="right">广告投放：</td>
											<td align="left">
												<input type="checkbox" name="mRequirePlan" id="mRequirePlan" lay-skin="primary">
											</td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmModifyButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>
	
	<div id="setCompanyLogoDialog" class="modal fade" >
		<div class="window-box modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						OEM配置(<span id="sCompanyName"></span>)
					</div>
				</div>

				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="setCompanyLogoForm" class="form-horizontal" role="form">
									<table class="table detail-tab layui-form">
										<tr>
											<td style="width: 100px; text-align: right;"><span class="required_style">*</span>平台域名：</td>
											<td><input id="mDomainName" type="text" class="col-sm-6" placeholder="平台域名" /></td>
										</tr>
										<tr>
											<td style="text-align: right;"><span class="required_style">*</span>平台名称：</td>
											<td><input id="mWebsiteTitle" type="text" class="col-sm-6" placeholder="平台名称" /></td>
										</tr>
										<tr>
											<td align="right">
												<span class="required_style">*</span>
												平台LOGO：
											</td>
											<td align="left" class="requirements" style="vertical-align: top; position: relative;">
												<input type="file" id="mLogo" onchange="selectImageFileToOss(this.id,'mLogoDiv');"
													style="position: absolute; clip: rect(0, 0, 0, 0);" />
												<button type="button" class="btn btn-step btn-step-c1"
													onclick="doTriggerClick('mLogo');">选择LOGO图</button>
											</td>
										</tr>
										<tr class="personal-data">
											<td align="right" style='width: 82px'></td>
											<td align="left" style="vertical-align: top;">
												<div id="mLogoDiv"></div>
											</td>
										</tr>
										<tr>
											<td align="right" style='width: 82px'></td>
											<td>建议logo尺寸为: 98*41</td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmSetCompanyLogoButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>
	
	<div id="resetPasswordDialog" class="modal fade">
		<div class="window-box modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						重置密码
					</div>
				</div>

				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="resetPasswordForm" class="form-horizontal" role="form">
									<input id="rUserId" type="hidden" />
									<table class="table detail-tab  layui-form">
										<thead>
											<tr>
												<td width="100px" align="right"><span class="required_style">*</span>用户名：</td>
												<td align="left" height="35px"><span id="rUserName"></span></td>
											</tr>
										</thead>
										<tr>
											<td align="right"><span class="required_style">*</span>新密码：</td>
											<td><input id="resetPassword" type="password" class="col-sm-6" placeholder="新密码"
												onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>确认新密码：</td>
											<td><input id="resetChkpwd" type="password" class="col-sm-6" placeholder="确认新密码"
												onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" /></td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmResetPassword" type="button" class="btn btn-primary">确定</button>
					<button class="btn" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>
	
	<div id="setPlayConfigDialog" class="modal fade" >
		<div class="window-box modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						设备配置
					</div>
				</div>

				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="setPlayConfigForm" class="form-horizontal" role="form">
									<table class="table detail-tab layui-form">
										<tr>
											<td style="width: 150px; text-align: right;"><span class="required_style">*</span>获取任务周期(秒)：</td>
											<td><input type="text" id="taskPeriod" class="col-sm-6" onkeyup="integerChange(this);" placeholder="获取任务周期(秒)" /></td>
										</tr>
										<!-- <tr>
											<td style="text-align: right;"><span class="required_style">*</span>发送心跳周期(秒)：</td>
											<td><input type="text" id="heartPeriod" class="col-sm-6" onkeyup="integerChange(this);" placeholder="发送心跳周期(秒)" /></td>
										</tr> -->
										<tr>
											<td style="text-align: right;"><span class="required_style">*</span>系统参数上报周期(秒):</td>
											<td><input type="text" id="reporPeriod" class="col-sm-6" onkeyup="integerChange(this);" placeholder="系统参数上报周期(秒)" /></td>
										</tr>
										<tr>
											<td style="text-align: right;"><span class="required_style">*</span>设备所在时区：</td>
											<td>
												<div class="layui-form-item">
													<div class="layui-input-block" style="width:50%">
														<select id="timeZone">
															<option value="-12">-12:00</option>
															<option value="-11">-11:00</option>
															<option value="-10">-10:00</option>
															<option value="-9">-9:00</option>
															<option value="-8">-8:00</option>
															<option value="-7">-7:00</option>
															<option value="-6">-6:00</option>
															<option value="-5">-5:00</option>
															<option value="-4">-4:00</option>
															<option value="-3">-3:00</option>
															<option value="-2">-2:00</option>
															<option value="-1">-1:00</option>
															<option value="0">0:00</option>
															<option value="1">+1:00</option>
															<option value="2">+2:00</option>
															<option value="3">+3:00</option>
															<option value="4">+4:00</option>
															<option value="5">+5:00</option>
															<option value="6">+6:00</option>
															<option value="7">+7:00</option>
															<option value="8" selected>+8:00</option>
															<option value="9">+9:00</option>
															<option value="10">+10:00</option>
															<option value="11">+11:00</option>
															<option value="12">+12:00</option>
															<option value="13">+13:00</option>
														</select>
													</div>
												</div>
											</td>
										</tr>
										<tr>
											<td style="text-align: right;"><span class="required_style">*</span>是否对接平台广告：</td>
											<td>
												<input type="radio" name="isGetDsp" id="isGetDsp_1" value="1" title="是" lay-skin="primary">
												&nbsp;&nbsp;
												<input type="radio" name="isGetDsp" id="isGetDsp_0" value="0" title="否" lay-skin="primary">
											</td>
										</tr>
										<tr>
											<td style="text-align: right;"><span class="required_style">*</span>是否接入直播上屏：</td>
											<td>
												<input type="radio" name="isLive" id="isLive_1" value="1" title="是" lay-skin="primary">
												&nbsp;&nbsp;
												<input type="radio" name="isLive" id="isLive_0" value="0" title="否" lay-skin="primary">
											</td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="saveButtonPlayConfig" type="button" class="btn btn-primary">确定</button>
					<button class="btn" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>
	
	<div id="rechargeCompanyDialog" class="modal fade" >
		<div class="window-box modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						公司充值【<span id="RechargeCompanyName"></span>】
					</div>
				</div>

				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="RechargeCompanyForm" class="form-horizontal" role="form">
									<input id="RechargeCompanyId" type="hidden" />
									<table class="table detail-tab layui-form">
										<tr>
											<td width="100px" align="right"><span class="required_style">*</span>充值类型：</td>
											<td>
												<div class="layui-form-item">
													<div class="layui-input-block" style="width: 50%">
														<select id="cTradeType" lay-verify="required" lay-search>
															<option value="">请选择</option>
															<c:forEach var="entry" items="${chargeTypeList}">
																<option value="${entry.id}">${entry.name}</option>
															</c:forEach>
														</select>
													</div>
												</div>
											</td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>充值金额(元)：</td>
											<td><input type="text" name="cAmount" id="cAmount" onkeyup="decimalChange(this)"
												class="col-sm-6" placeholder="充值金额" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>实付金额(元)：</td>
											<td><input type="text" name="cPayment" id="cPayment" onkeyup="decimalChange(this)"
												class="col-sm-6" placeholder="实付金额" value="0"/></td>
										</tr>
										<tr>
											<td align="right">备注：</td>
											<td><textarea id="cRemark" style="width:50%;height:30%" cols= class="col-sm-10" placeholder="备注"></textarea></td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmRechargeButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn btn-fn btn-delect" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>
	
	<div id="modifyCompanyStatusDialog" class="modal fade">
        <div class="window-box modal-dialog">
            <div class="modal-content">
                <div class="modal-header no-padding">
                    <div class="table-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            <span class="box-close">×</span>
                        </button>
                       	修改账户类型
                    </div>
                </div>
                <div class="modal-body no-padding">
                    <div class="page-content">
                        <div class="row">
                            <div class="col-xs-12">
                                <form id="modifyCompanyStatusForm" class="form-horizontal layui-form" role="form">
                                    <table class="table detail-tab layui-form">
                                        <tr>
                                        	<td  width="100px" align="right"><span class="required_style">*</span>账户类型：</td>
                                            <td align="left">
                                                <input name="mStatus" id="mStatus1" lay-filter="mStatusFilter" type="radio" info="正式" value="1" title="正式" />&nbsp;&nbsp; 
                                                <input name="mStatus" id="mStatus2" lay-filter="mStatusFilter" type="radio" info="试用" value="2" title="试用" />&nbsp;&nbsp; 
                                                <input name="mStatus" id="mStatus0" lay-filter="mStatusFilter" type="radio" info="禁用" value="0" title="禁用" />
                                            <td>
                                        </tr>
                                        <tr style="display:none" class="mValidUntilDateTd" >
                                        	<td align="right"><span class="required_style">*</span>有效期至：</td>
                                            <td align="left">
                                                 <input type="text"  name="mValidUntilDate" id="mValidUntilDate" placeholder="试用账户有效期">
                                            <td>
                                        </tr>
                                        <tr style="display:none" class="mValidUntilDateTd" >
                                        	<td align="right">播控加水印：</td>
                                            <td align="left">
                                            	<input type="checkbox" id="mIsWatermark" lay-skin="primary">
                                            <td>
                                        </tr>
                                    </table>
                                    <div class="space-4"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer no-margin-top">
                    <button id=confirmModifyCompanyStatusButton type="button" class="btn btn-primary">确定</button>
                    <button class="btn btn-fn btn-delect" data-dismiss="modal">取消</button>
                </div>
            </div>
        </div>
    </div>
    
    <div id="tradeRecordDialog" class="modal fade">
		<div class="window-box modal-dialog-max">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button> 交易记录【<span id="tradeRecordCompanyName"></span>】
					</div>
				</div>

				<div class="modal-body-max no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<div class="box-main">
									<div class="table-responsive">
										<table id="tradeRecordTable" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th class="center">序号</th>
													<th class="center">交易时间</th>
													<th class="center">交易类型</th>
													<th class="center">交易金额(元)</th>
													<th class="center">实付金额(元)</th>
													<th class="center">当前余额(元)</th>
													<th class="center">操作员</th>
												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
									</div>
									<div class="page-new clear">
										<div id="tradeRecord_pageDiv" align="right" class="pull-right"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer no-margin-top"></div>
			</div>
		</div>
	</div>
</body>
</html>
