<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../common_taglibs.jsp"%>
<%@ include file="auditor_position_dialog.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>审核方管理</title>
</style>
<script type="text/javascript" src="js/system/auditor_management.js?v=<%=System.currentTimeMillis()%>"></script>
</head>
<body>
	<input type="hidden" id="mId" />
	<div class="page-content">
		<div class="row">
			<div class="col-xs-12">
				<div class="step-main">
					<ul class="search-top layui-form">
						<li class="left-td"><span class="search-tit">审核方类型：</span>
							<div class="search-main">
								<div class="layui-form-item">
									<div class="layui-input-block">
										<select id="type" lay-verify="required" lay-search>
											<option value="">请选择</option>
											<option value="1">奥凌</option>
											<option value="2">CIBN</option>
											<option value="3">场景方</option>
										</select>
									</div>
								</div>
							</div>
						</li>
						<li class="left-td"><span class="search-tit">审核方名称：</span>
							<div class="search-main">
								<input autocomplete="off" id="name" type="text" class="col-sm-6" placeholder="审核方名称" />
							</div>
						</li>
						<li class="left-td"><span class="search-tit">手机号：</span>
							<div class="search-main">
								<input autocomplete="off" id="telephone" type="text" class="col-sm-6" placeholder="手机号" />
							</div>
						</li>
						<li class="left-td" style="margin-right: 100px;"><span class="search-tit">状态：</span>
							<div class="search-main">
								<div class="layui-form-item">
									<div class="layui-input-block">
										<select id="status">
											<option value="">请选择</option>
											<option value="1">启用</option>
											<option value="0">禁用</option>
										</select>
									</div>
								</div>
							</div>
						</li>
					</ul>
					<div class="clear"></div>
					<div class="search-button">
						<c:if test="${fn:contains($rightList,'auditor/queryAuditor')}">
							<button id="queryButton" type="button" class="search-fn-btn" style='cursor: pointer;'>
								<i class="icon-search"></i> 查询
							</button>
						</c:if>
					</div>
				</div>
				<div id="toolbarDiv">
					<c:if test="${fn:contains($rightList,'auditor/createAuditor')}">
						<button id="createButton" type="button" class="btn btn-step">
							<i class="icon-credit-card"></i> 创建
						</button>
					</c:if>
					<c:if test="${fn:contains($rightList,'auditor/modifyAuditor')}">
						<input type="hidden" id="modify" value="1" />
					</c:if>
					<c:if test="${fn:contains($rightList,'auditor/deleteAuditor')}">
						<input type="hidden" id="delete" value="1" />
					</c:if>
					<c:if test="${fn:contains($rightList,'auditor/resetAuditorPassword')}">
						<input type="hidden" id="modifyUserPwd" value="1" />
					</c:if>
					<c:if test="${fn:contains($rightList,'auditor/setSenceDevice')}">
						<input type="hidden" id="setSenceDevice" value="1" />
					</c:if>
				</div>
				<div class="step-main">
					<div class="table-responsive">
						<table id="AuditorTable" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th class="center">审核方名称</th>
									<th class="center">审核方类型</th>
									<th class="center">联系人</th>
									<th class="center">手机号(用户名)</th>
									<th class="center">状态</th>
									<th class="center">创建时间</th>
									<th width="180px" class="center operationColumn">操作</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
					<div class="page-new clear">
						<div id="pageDiv" align="right" class="pull-right"></div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>

	<div id="createAuditorDialog" class="modal fade">
		<div class="window-box modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						创建审核方
					</div>
				</div>
				
				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form class="form-horizontal" id="addForm" role="form">
									<table class="table detail-tab layui-form">
										<tr>
											<td width="120px" align="right"><span class="required_style">*</span>审核方类型：</td>
											<td align="left">
												<div class="layui-form-item">
													<div class="layui-input-block" style="width: 50%">
														<select id="cType" lay-verify="required" lay-search>
															<option value="">请选择</option>
															<option value="1">奥凌</option>
															<option value="2">CIBN</option>
															<option value="3">场景方</option>
														</select>
													</div>
												</div>
											</td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>审核方名称：</td>
											<td align="left"><input id="cName" type="text" class="col-sm-6"  placeholder="审核方名称" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>审核方联系人：</td>
											<td align="left"><input id="cContacts" type="text" class="col-sm-6"  placeholder="审核方联系人" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>手机号(用户名)：</td>
											<td align="left"><input id="cTelephone" type="text" class="col-sm-6"  onkeyup="integerChange(this);" placeholder="手机号(用户名)" /></td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmCreateButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>

	<div id="modifyAuditorDialog" class="modal fade">
		<div class="window-box modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						编辑审核方
					</div>
				</div>
				
				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form class="form-horizontal" id="modifyForm" role="form">
									<table class="table detail-tab layui-form">
										<tr>
											<td width="120px" align="right"><span class="required_style">*</span>审核方类型：</td>
											<td align="left"><span id="mTypeSpan"></span></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>审核方名称：</td>
											<td align="left"><input id="mName" type="text" class="col-sm-6"  placeholder="审核方名称" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>审核方联系人：</td>
											<td align="left"><input id="mContacts" type="text" class="col-sm-6"   placeholder="审核方联系人" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>手机号(用户名)：</td>
											<td align="left"><span id="mTelephoneSpan"></span></td>
											<!-- <td align="left"><input id="mTelephone" type="text" class="col-sm-6"  onkeyup="integerChange(this);" placeholder="手机号(用户名)" /></td> -->
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmModifyButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>
	
	<div id="resetPasswordDialog" class="modal fade">
		<div class="window-box modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						重置密码
					</div>
				</div>

				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="resetPasswordForm" class="form-horizontal" role="form">
									<table class="table detail-tab layui-form">
										<tr>
											<td width="120px" align="right"><span class="required_style">*</span>用户名：</td>
											<td align="left" height="35px"><span id="rUsername"></span></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>新密码：</td>
											<td><input id="resetPassword" type="password" class="col-sm-6" placeholder="新密码"
												onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>确认新密码：</td>
											<td><input id="resetChkpwd" type="password" class="col-sm-6" placeholder="确认新密码"
												onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" /></td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmResetPassword" type="button" class="btn btn-primary">确定</button>
					<button class="btn" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
