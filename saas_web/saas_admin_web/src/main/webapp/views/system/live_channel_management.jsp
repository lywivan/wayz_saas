<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../common_taglibs.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>直播渠道管理</title>
<script type="text/javascript" src="js/system/live_channel_management.js?v=<%=System.currentTimeMillis()%>"></script>
</head>
<body>
	<div class="page-content">
		<div class="row">
			<div class="col-xs-12">
				<div id="toolbarDiv">
					<c:if test="${fn:contains($rightList,'admin/createLiveChannel')}">
						<button id="createButton" type="button" class="btn btn-step">
							<i class="icon-credit-card"></i> 创建
						</button>
					</c:if>
					<c:if test="${fn:contains($rightList,'admin/modifyLiveChannel')}">
						<input type="hidden" id="modify" value="1" />
					</c:if>
					<c:if test="${fn:contains($rightList,'admin/deleteLiveChannel')}">
						<input type="hidden" id="delete" value="1" />
					</c:if>
				</div>
				<div class="step-main">
					<div class="table-responsive">
						<table id="LiveChannelTable" class="table table-striped table-bordered">
							<thead>
								<tr>	
									<th class="center">序号</th>
									<th class="center">直播渠道名称</th>
									<th class="center">操作</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="createLiveChannelDialog" class="modal fade">
		<div class="window-box modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						添加直播渠道
					</div>
				</div>

				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="addLiveChannelForm" class="form-horizontal" role="form">
									<table class="table detail-tab layui-form">
										<tr>
											<td width="100px" align="right"><span class="required_style">*</span>名称：</td>
											<td><input id="cName" type="text" class="col-sm-6" placeholder="名称" /></td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmCreateButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>

	<div id="modifyLiveChannelDialog" class="modal fade">
		<div class="window-box modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						编辑直播渠道
					</div>
				</div>

				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="modifyLiveChannelForm" class="form-horizontal" role="form">
									<input id="mId" type="hidden" />
									<table class="table detail-tab layui-form">
										<tr>
											<td width="100px" align="right"><span class="required_style">*</span>名称：</td>
											<td><input id="mName" type="text" class="col-sm-6" placeholder="名称" /></td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmModifyButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
