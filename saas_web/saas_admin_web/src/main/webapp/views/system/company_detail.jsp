<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<script type="text/javascript" src="js/system/company_detail.js?v=<%=System.currentTimeMillis()%>"></script>
<div id="companyDetailDialog" class="modal fade">
	<div class="window-box modal-dialog">
		<div class="modal-content">
			<div class="modal-header no-padding">
				<div class="table-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
						<span class="box-close">×</span>
					</button>
					公司详情
				</div>
			</div>

			<div class="modal-body no-padding">
				<div class="page-content">
					<div class="row">
						<div class="col-xs-12">
							<h4 class="step-tit">
								<i></i>
								基本信息
							</h4>
							<table class="table detail-tab">
								<tr>
									<td>公司名称：<span id="vName"></span></td>
									<td>公司来源：<span id="vSourceType"></span></td>
								</tr>
								<tr>
									<td>联系人：<span id="vContacts"></span></td>
									<td>联系电话：<span id="vPhone"></span></td>
								</tr>
								<tr>
									<td>联系邮箱：<span id="vEmail"></span></td>
									<td>联系地址：<span id="vAddress"></span></td>
								</tr>
							</table>
							
							<h4 class="step-tit">
								<i></i>
								管理账户
							</h4>
							<table class="table detail-tab">
								<tr>
									<td>用户名：<span id="vUserName"></span></td>
									<td>账户状态：<span id="vStatusName"></span></td>
								</tr>
								<tr style="display:none" class="vValidUntilDateTd" >
									<td>有效期：<span id="vValidUntilDate"></span></td>
									<td>加水印：<span id="vIsWatermark"></span></td>
								</tr>
								<tr>
									<td>代理商：<span id="vAgentName"></span></td>
									<td>一键登录：<span id="vIsLogin"></span></td>
								</tr>
							</table>
							
							<h4 class="step-tit">
								<i></i>
								服务配置
							</h4>
							<table class="table detail-tab">
								<tr>
									<td>版本：<span id="vVersionName"></span></td>
									<td>账户余额(元)：<span id="vBalance"></span></td>
								</tr>
								<tr>	
									<td>广告投放：<span id="vRequirePlan"></span></td>
									<td>素材总空间：<span id="vMaterialTotalLimit"></span></td>
								</tr>
								<tr>	
									<td>已用素材空间：<span id="vAlreadyMaterialLimit"></span></td>
									<td>剩余素材空间：<span id="vSurplusMaterialLimit"></span></td>
								</tr>
								<tr>
									<td>用户数量：<span id="vUserLimit"></span></td>
								</tr>
							</table>
							
							<h4 class="step-tit">
								<i></i>
								OEM配置
							</h4>
							<table class="table detail-tab">
								<tr>
									<td>平台域名：<span id="vPlatFormDomain"></span></td>
									<td>平台名称：<span id="vPlatFormName"></span></td>
								</tr>
								<tr>
									<td>平台LOGO：<div id="vPlatFormLogo"></div></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>

			<div class="modal-footer no-margin-top"></div>
		</div>
	</div>
</div>