<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<script type="text/javascript" src="js/system/auditor_position_dialog.js?v=<%=System.currentTimeMillis()%>"></script>
	<div id="setPositionDialog" class="modal fade">
		<div class="window-box modal-dialog-max">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						添加设备
					</div>
				</div>
	
				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<div class="step-main">
									<form id="setPositionForm" class="form-horizontal" role="form">
									<ul class="search-top layui-form">
										<li class="left-td"><span class="search-tit">所属公司：</span>
											<div class="search-main">
												<div class="layui-form-item">
													<div class="layui-input-block">
														<select id="liveCompanyId" lay-verify="required" lay-search>
															<option value="">请选择</option>
															<c:forEach items="${liveCompanyList}" var="temp">
																<option value="${temp.id }">${temp.name}</option>
															</c:forEach>
														</select>
													</div>
												</div>
											</div></li>
										<li class="left-td"><span class="search-tit">设备名称：</span>
											<div class="search-main">
												<input type="text" id="positionName" placeholder="设备名称" />
											</div></li>
										<%-- <li class="left-td" style="margin-right: 100px;"><span
											class="search-tit">场景：</span>
											<div class="search-main">
												<div class="layui-form-item">
													<div class="layui-input-block">
														<select id="tag" lay-verify="required" lay-search>
															<option value="">请选择</option>
															<c:forEach items="${sceneList}" var="temp">
																<option value="${temp.id }">${temp.name}</option>
															</c:forEach>
														</select>
													</div>
												</div>
											</div></li> --%>
									</ul>
								</form>
									<div class="clear"></div>
									<div class="search-button media-search-btn">
										<button id="querySelectablePositionButton" type="button" class="search-fn-btn">
											<i class="icon-search"></i> 查询
										</button>
									</div>
								</div>
								<div class="step-main">
									<h4 class="step-tit">
										<i></i> 可选设备
									</h4>
									<table id="selectPositionTable" class="table table-striped table-bordered">
										<thead>
											<tr>
												<th class="center"><label> <input type="checkbox" id="selectListAll1" class="ace" /> <span
														class="lbl"></span>
												</label></th>
												<th class="center">设备名称</th>
												<th class="center">播控编码</th>
												<th class="center">区域</th>
												<th class="center">详细地址</th>
												<th class="center">所属公司</th>
												<th class="center">设备场景</th>
												<th class="center">在线状态</th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
									<div class="page-new clear">
										<div id="btn_div" class="pull-left">
											<button id="addPositionButton" type="button" class="btn btn-step btn-step-c1">添加设备</button>
											<!-- <button id="addAllPositionButton" type="button" class="btn btn-step btn-step-c1">添加全部</button> -->
										</div>
										<div id="selectPageDiv" align="right" class="pull-right"></div>
									</div>
								</div>
								<div class="step-main">
									<h4 class="step-tit">
										<i></i> 已选设备
									</h4>
									<table id="selectedPositionTable" class="table table-striped table-bordered">
										<thead>
											<tr>
												<th class="center"><label> <input type="checkbox" id="selectListAll2" class="ace" /> <span
														class="lbl"></span>
												</label></th>
												<th class="center">设备名称</th>
												<th class="center">播控编码</th>
												<th class="center">区域</th>
												<th class="center">详细地址</th>
												<th class="center">所属公司</th>
												<th class="center">设备场景</th>
												<th class="center">在线状态</th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
									<div class="page-new clear">
										<div id="btn_div" class="pull-left">
											<button id="deletePositionButton" type="button" class="btn btn-step btn-step-c4">删除设备</button>
											<!-- <button id="deleteAllPositionButton" type="button" class="btn btn-step btn-step-c4">删除全部</button> -->
										</div>
										<div id="selectedPageDiv" align="right" class="pull-right"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
	
				<div class="modal-footer no-margin-top"></div>
			</div>
		</div>
	</div>
	
	<div id="selectedPositionDialog" class="modal fade">
		<div class="window-box modal-dialog-max">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						已添加的设备
					</div>
				</div>
	
				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<div class="box-main">
									<table id="selectedPositionTable2" class="table table-striped table-bordered">
										<thead>
											<tr>
												<th class="center">设备名称</th>
												<th class="center">播控编码</th>
												<th class="center">区域</th>
												<th class="center">详细地址</th>
												<th class="center">所属公司</th>
												<th class="center">设备场景</th>
												<th class="center">在线状态</th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
									<div class="page-new clear">
										<div id="selectedPageDiv2" align="right" class="pull-right"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
	
				<div class="modal-footer no-margin-top"></div>
			</div>
		</div>
	</div>