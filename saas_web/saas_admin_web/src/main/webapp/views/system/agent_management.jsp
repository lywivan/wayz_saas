<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../common_taglibs.jsp"%>
<%@ include file="agent_detail.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>代理商管理</title>
<style type="text/css">
	.comInpWidth{
		width : 225px;
	}
</style>
<script src="plugin/alioss/aliyun-oss-sdk-4.4.4.min.js"></script>
<script type="text/javascript" src="js/system/agent_management.js?v=<%=System.currentTimeMillis()%>"></script>
</head>
<body>
	<input id="agentId" type="hidden" />
	<div class="page-content">
		<div class="row">
			<div class="col-xs-12">
				<div class="step-main">
					<ul class="search-top layui-form">
						<li class="left-td"><span class="search-tit">代理商名称：</span>
							<div class="search-main">
								<input autocomplete="off" id="name" type="text" class="col-sm-6" placeholder="代理商名称" />
							</div>
						</li>
						<li class="left-td"><span class="search-tit">手机号：</span>
							<div class="search-main">
								<input autocomplete="off" id="phone" type="text" class="col-sm-6" placeholder="手机号" />
							</div>
						</li>
						<li class="left-td"><span class="search-tit">区域：</span>
							<div class="search-main">
								<div class="ssq" style="width: 105px">
									<div class="layui-form-item">
										<div class="layui-input-block">
											<select id="provinceId" lay-filter="provinceId" lay-verify="required" lay-search>
												<option value="">省份</option>
												<c:if test="${not empty provinceList}">
													<c:forEach var="province" items="${provinceList}">
														<option value="${province.id}">${province.name}</option>
													</c:forEach>
												</c:if>
											</select>
										</div>
									</div>
								</div>
								<div class="ssq" style="width: 105px">
									<div class="layui-form-item">
										<div class="layui-input-block">
											<select id="cityId" lay-filter="cityId" lay-verify="required" lay-search>
												<option value="">城市</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</li>
						<li class="left-td"><span class="search-tit">行业：</span>
							<div class="search-main">
								<div class="layui-form-item">
									<div class="layui-input-block">
										<select id="categoryId" lay-verify="required" lay-search>
											<option value="">请选择</option>
											<c:if test="${not empty industryList}">
												<c:forEach var="vo" items="${industryList}">
													<option value="${vo.id}">${vo.name}</option>
												</c:forEach>
											</c:if>
										</select>
									</div>
								</div>
							</div>
						</li>
						<li class="left-td" style="margin-right: 100px;"><span class="search-tit">状态：</span>
							<div class="search-main">
								<div class="layui-form-item">
									<div class="layui-input-block">
										<select id="status">
											<option value="">请选择</option>
											<option value="1">启用</option>
											<option value="0">禁用</option>
										</select>
									</div>
								</div>
							</div>
						</li>
					</ul>
					<div class="clear"></div>
					<div class="search-button">
						<c:if test="${fn:contains($rightList,'agent/queryAgentCompany')}">
							<button id="queryButton" type="button" class="search-fn-btn" style='cursor: pointer;'>
								<i class="icon-search"></i> 查询
							</button>
						</c:if>
					</div>
				</div>
				<div id="toolbarDiv">
					<c:if test="${fn:contains($rightList,'agent/createAgentCompany')}">
						<button id="createButton" type="button" class="btn btn-step">
							<i class="icon-credit-card"></i> 创建
						</button>
					</c:if>
					<c:if test="${fn:contains($rightList,'agent/modifyAgentCompany')}">
						<input type="hidden" id="modify" value="1" />
					</c:if>
					<c:if test="${fn:contains($rightList,'agent/createAgentOEM')}">
						<input type="hidden" id="oemCofing" value="1" />
					</c:if>
					<c:if test="${fn:contains($rightList,'agent/resetAgentPassword')}">
						<input type="hidden" id="modifyUserPwd" value="1" />
					</c:if>
				</div>
				<div class="step-main">
					<div class="table-responsive">
						<table id="agentTable" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th class="center">代理商名称</th>
									<th class="center">代理商类型</th>
									<th class="center">联系人</th>
									<th class="center">手机号(用户名)</th>
									<th class="center">屏幕购买数量</th>
									<th class="center">行业</th>
									<th class="center">区域</th>
									<th class="center">状态</th>
									<th class="center">创建时间</th>
									<th width="180px" class="center operationColumn">操作</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
					<div class="page-new clear">
						<div id="pageDiv" align="right" class="pull-right"></div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>

	<div id="createAgentDialog" class="modal fade">
		<div class="window-box modal-dialog-middle">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						创建代理商
					</div>
				</div>
				
				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form class="form-horizontal" id="addAgentForm" role="form">
									<table class="table detail-tab layui-form">
										<tr>
											<td width="100px" align="right"><span class="required_style">*</span>代理商类型：</td>
											<td align="left">
												<input type="radio" name="cProxyType" lay-filter="cProxyType" id="cProxyType_1" value="1" title="公司" lay-skin="primary">
												&nbsp;&nbsp;
												<input type="radio" name="cProxyType" lay-filter="cProxyType" id="cProxyType_2" value="2" title="个人" lay-skin="primary">
											</td>
											<td width="100px" align="right"><span class="required_style">*</span>代理商名称：</td>
											<td align="left"><input id="cName" type="text" class="comInpWidth" placeholder="代理商名称" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>手机号(用户名)：</td>
											<td align="left"><input id="cTelephone" type="text" class="comInpWidth" onkeyup="integerChange(this);" placeholder="手机号(用户名)" /></td>
											<td align="right"><span class="required_style">*</span>屏幕购买量：</td>
											<td align="left"><input id="cPositionLimit" type="text" class="comInpWidth" onkeyup="integerChange(this);" placeholder="屏幕购买量" /></td>
										</tr>
										<tr>
											<td width="100px" align="right"><span class="required_style">*</span>所属行业：</td>
											<td align="left">
												<div class="layui-form-item">
													<div class="layui-input-block comInpWidth">
														<select id="cCategoryId" lay-verify="required" lay-search>
															<option value="">请选择</option>
															<c:if test="${not empty industryList}">
																<c:forEach var="vo" items="${industryList}">
																	<option value="${vo.id}">${vo.name}</option>
																</c:forEach>
															</c:if>
														</select>
													</div>
												</div>
											</td>
											<td align="right"><span class="required_style">*</span>所属区域：</td>
											<td>
												<div class="ssq" style="width: 112px">
													<div class="layui-form-item">
														<div class="layui-input-block" >
															<select id="cProvinceId" lay-filter="cProvinceId" lay-verify="required" lay-search>
																<option value="">省份</option>
																<c:if test="${not empty provinceList}">
																	<c:forEach var="province" items="${provinceList}">
																		<option value="${province.id}">${province.name}</option>
																	</c:forEach>
																</c:if>
															</select>
														</div>
													</div>
												</div>
												<div class="ssq" style="width: 112px">
													<div class="layui-form-item">
														<div class="layui-input-block">
															<select id="cCityId" lay-filter="cCityId" lay-verify="required" lay-search>
																<option value="">城市</option>
															</select>
														</div>
													</div>
												</div>
											</td>
										</tr>
										<tr class="company-data">
											<td align="right">
												<span class="required_style">*</span>
												营业执照：
											</td>
											<td align="left" class="requirements" style="vertical-align: top; position: relative;">
												<input type="file" id="cBusinessLicenseFile" onchange="selectImageFileToOss(this.id,'cFileDivBL');"
													style="position: absolute; clip: rect(0, 0, 0, 0);" />
												<button type="button" class="btn btn-step btn-step-c1"
													onclick="doTriggerClick('cBusinessLicenseFile');">选择营业执照图</button>
											</td>
											<td align="right"><span class="required_style">*</span>联系人：</td>
											<td align="left"><input id="cContacts" type="text" class="comInpWidth"  placeholder="联系人" /></td>
										</tr>
										<tr class="company-data">
											<td align="right" style='width: 82px'></td>
											<td align="left" style="vertical-align: top;">
												<div id="cFileDivBL"></div>
											</td>
										</tr>
										<tr class="personal-data">
											<td align="right">
												<span class="required_style">*</span>
												身份证正面：
											</td>
											<td align="left" class="requirements" style="vertical-align: top; position: relative;">
												<input type="file" id="cIdentityBverseFile" onchange="selectImageFileToOss(this.id,'cFileDivIB');"
													style="position: absolute; clip: rect(0, 0, 0, 0);" />
												<button type="button" class="btn btn-step btn-step-c1"
													onclick="doTriggerClick('cIdentityBverseFile');">选择身份证正面图</button>
											</td>
											
											<td align="right">
												<span class="required_style">*</span>
												身份证反面：
											</td>
											<td align="left" class="requirements" style="vertical-align: top; position: relative;">
												<input type="file" id="cIdentityReverseFile" onchange="selectImageFileToOss(this.id,'cFileDivIR');"
													style="position: absolute; clip: rect(0, 0, 0, 0);" />
												<button type="button" class="btn btn-step btn-step-c1"
													onclick="doTriggerClick('cIdentityReverseFile');">选择身份证反面图</button>
											</td>
										</tr>
										<tr class="personal-data">
											<td align="right" style='width: 82px'></td>
											<td align="left" style="vertical-align: top;">
												<div id="cFileDivIB"></div>
											</td>
											
											<td align="right" style='width: 82px'></td>
											<td align="left" style="vertical-align: top;">
												<div id="cFileDivIR"></div>
											</td>
										</tr>
										<tr>
											<td align="right">备注：</td>
											<td align="left" colspan="3">
												<textarea id="cDescription" style="height: 30%; width: 87%" placeholder="备注"></textarea>
											</td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmCreateButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>

	<div id="modifyAgentDialog" class="modal fade">
		<div class="window-box modal-dialog-middle">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						编辑代理商
					</div>
				</div>
				
				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form class="form-horizontal" id="modifyAgentForm" role="form">
									<input type="hidden" id="mId" />
									<table class="table detail-tab layui-form">
										<tr>
											<td width="100px" align="right"><span class="required_style">*</span>代理商类型：</td>
											<td align="left">
												<input type="radio" name="mProxyType" lay-filter="mProxyType" id="mProxyType_1" value="1" title="公司" lay-skin="primary">
												&nbsp;&nbsp;
												<input type="radio" name="mProxyType" lay-filter="mProxyType" id="mProxyType_2" value="2" title="个人" lay-skin="primary">
											</td>
											<td width="100px" align="right"><span class="required_style">*</span>代理商名称：</td>
											<td align="left"><input id="mName" type="text" class="comInpWidth" placeholder="代理商名称" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>手机号(用户名)：</td>
											<td align="left"><input id="mTelephone" type="text" class="comInpWidth" onkeyup="integerChange(this);" placeholder="手机号(用户名)" /></td>
											<td align="right"><span class="required_style">*</span>屏幕购买量：</td>
											<td align="left"><input id="mPositionLimit" type="text" class="comInpWidth" onkeyup="integerChange(this);" placeholder="屏幕购买量" /></td>
										</tr>
										<tr>
											<td width="100px" align="right"><span class="required_style">*</span>所属行业：</td>
											<td align="left">
												<div class="layui-form-item">
													<div class="layui-input-block comInpWidth">
														<select id="mCategoryId" lay-verify="required" lay-search>
															<option value="">请选择</option>
															<c:if test="${not empty industryList}">
																<c:forEach var="vo" items="${industryList}">
																	<option value="${vo.id}">${vo.name}</option>
																</c:forEach>
															</c:if>
														</select>
													</div>
												</div>
											</td>
											<td align="right"><span class="required_style">*</span>所属区域：</td>
											<td>
												<div class="ssq" style="width: 112px">
													<div class="layui-form-item">
														<div class="layui-input-block" >
															<select id="mProvinceId" lay-filter="mProvinceId" lay-verify="required" lay-search>
																<option value="">省份</option>
																<c:if test="${not empty provinceList}">
																	<c:forEach var="province" items="${provinceList}">
																		<option value="${province.id}">${province.name}</option>
																	</c:forEach>
																</c:if>
															</select>
														</div>
													</div>
												</div>
												<div class="ssq" style="width: 112px">
													<div class="layui-form-item">
														<div class="layui-input-block">
															<select id="mCityId" lay-filter="mCityId" lay-verify="required" lay-search>
																<option value="">城市</option>
															</select>
														</div>
													</div>
												</div>
											</td>
										</tr>
										<tr class="company-data">
											<td align="right"><span class="required_style">*</span>联系人：</td>
											<td align="left"><input id="mContacts" type="text" class="comInpWidth"  placeholder="联系人" /></td>
											<td align="right">
												<span class="required_style">*</span>
												营业执照：
											</td>
											<td align="left" class="requirements" style="vertical-align: top; position: relative;">
												<input type="file" id="mBusinessLicenseFile" onchange="selectImageFileToOss(this.id,'mFileDivBL');"
													style="position: absolute; clip: rect(0, 0, 0, 0);" />
												<button type="button" class="btn btn-step btn-step-c1"
													onclick="doTriggerClick('mBusinessLicenseFile');">选择营业执照图</button>
											</td>
										</tr>
										<tr class="company-data">
											<td align="right" style='width: 82px'></td>
											<td align="left" style="vertical-align: top;">
												<div id="mFileDivBL"></div>
											</td>
										</tr>
										<tr class="personal-data">
											<td align="right">
												<span class="required_style">*</span>
												身份证正面：
											</td>
											<td align="left" class="requirements" style="vertical-align: top; position: relative;">
												<input type="file" id="mIdentityBverseFile" onchange="selectImageFileToOss(this.id,'mFileDivIB');"
													style="position: absolute; clip: rect(0, 0, 0, 0);" />
												<button type="button" class="btn btn-step btn-step-c1"
													onclick="doTriggerClick('mIdentityBverseFile');">选择身份证正面图</button>
											</td>
											
											<td align="right">
												<span class="required_style">*</span>
												身份证反面：
											</td>
											<td align="left" class="requirements" style="vertical-align: top; position: relative;">
												<input type="file" id="mIdentityReverseFile" onchange="selectImageFileToOss(this.id,'mFileDivIR');"
													style="position: absolute; clip: rect(0, 0, 0, 0);" />
												<button type="button" class="btn btn-step btn-step-c1"
													onclick="doTriggerClick('mIdentityReverseFile');">选择身份证反面图</button>
											</td>
										</tr>
										<tr class="personal-data">
											<td align="right" style='width: 82px'></td>
											<td align="left" style="vertical-align: top;">
												<div id="mFileDivIB"></div>
											</td>
										
											<td align="right" style='width: 82px'></td>
											<td align="left" style="vertical-align: top;">
												<div id="mFileDivIR"></div>
											</td>
										</tr>
										<tr>
											<td align="right">备注：</td>
											<td align="left" colspan="3">
												<textarea id="mDescription" style="height: 30%; width: 87%" placeholder="备注"></textarea>
											</td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmModifyButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>
	
	<div id="setAgentLogoDialog" class="modal fade" >
		<div class="window-box modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						OEM配置(<span id="sAgentName"></span>)
					</div>
				</div>

				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="setAgentLogoForm" class="form-horizontal" role="form">
									<table class="table detail-tab layui-form">
										<tr>
											<td style="width: 100px; text-align: right;"><span class="required_style">*</span>平台域名：</td>
											<td><input id="mDomainName" type="text" class="col-sm-6" placeholder="平台域名" /></td>
										</tr>
										<tr>
											<td style="text-align: right;"><span class="required_style">*</span>平台名称：</td>
											<td><input id="mWebsiteTitle" type="text" class="col-sm-6" placeholder="平台名称" /></td>
										</tr>
										<tr>
											<td align="right">
												<span class="required_style">*</span>
												平台LOGO：
											</td>
											<td align="left" class="requirements" style="vertical-align: top; position: relative;">
												<input type="file" id="mLogo" onchange="selectImageFileToOss(this.id,'mLogoDiv');"
													style="position: absolute; clip: rect(0, 0, 0, 0);" />
												<button type="button" class="btn btn-step btn-step-c1"
													onclick="doTriggerClick('mLogo');">选择LOGO图</button>
											</td>
										</tr>
										<tr class="personal-data">
											<td align="right" style='width: 82px'></td>
											<td align="left" style="vertical-align: top;">
												<div id="mLogoDiv"></div>
											</td>
										</tr>
										<tr>
											<td align="right" style='width: 82px'></td>
											<td>建议logo尺寸为: 98*41</td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmSetAgentLogoButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>
	
	<div id="resetPasswordDialog" class="modal fade">
		<div class="window-box modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						重置密码
					</div>
				</div>

				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="resetPasswordForm" class="form-horizontal" role="form">
									<input id="rUserId" type="hidden" />
									<table class="table detail-tab  layui-form">
										<thead>
											<tr>
												<td width="100px" align="right"><span class="required_style">*</span>用户名：</td>
												<td align="left" height="35px"><span id="rUserName"></span></td>
											</tr>
										</thead>
										<tr>
											<td align="right"><span class="required_style">*</span>新密码：</td>
											<td><input id="resetPassword" type="password" class="col-sm-6" placeholder="新密码"
												onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>确认新密码：</td>
											<td><input id="resetChkpwd" type="password" class="col-sm-6" placeholder="确认新密码"
												onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" /></td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmResetPassword" type="button" class="btn btn-primary">确定</button>
					<button class="btn" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
