<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../common_taglibs.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>设备续期价格管理</title>
<script type="text/javascript" src="js/system/device_price_management.js?v=<%=System.currentTimeMillis()%>"></script>
</head>
<body>
	<div class="page-content">
		<div class="row">
			<div class="col-xs-12">
				<div id="toolbarDiv">
					<c:if test="${fn:contains($rightList,'admin/createDevicePrice')}">
						<button id="createButton" type="button" class="btn btn-step">
							<i class="icon-credit-card"></i> 创建
						</button>
					</c:if>
					<c:if test="${fn:contains($rightList,'admin/modifyDevicePrice')}">
						<input type="hidden" id="modify" value="1" />
					</c:if>
					<c:if test="${fn:contains($rightList,'admin/deleteDevicePrice')}">
						<input type="hidden" id="delete" value="1" />
					</c:if>
				</div>
				<div class="step-main">
					<div class="table-responsive">
						<table id="DevicePriceTable" class="table table-striped table-bordered">
							<thead>
								<tr>	
									<th class="center">名称</th>
									<th class="center">天数</th>
									<th class="center">原价(元)</th>
									<th class="center">优惠价(元)</th>
									<th class="center">操作</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="createDevicePriceDialog" class="modal fade">
		<div class="window-box modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						添加设备续期价格
					</div>
				</div>

				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="addDevicePriceForm" class="form-horizontal" role="form">
									<table class="table detail-tab layui-form">
										<tr>
											<td width="100px" align="right"><span class="required_style">*</span>名称：</td>
											<td><input id="cName" type="text" class="col-sm-6" placeholder="名称" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>天数：</td>
											<td><input id="cDays" type="text" onkeyup="integerChange(this)" class="col-sm-6" placeholder="天数" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>原价：</td>
											<td><input id="cSalePrice" type="text"  onkeyup="doubleChange(this)" class="col-sm-6" placeholder="原价 " /> 元</td>
										</tr>
										<tr>
											<td align="right">优惠价：</td>
											<td><input id="cDiscountPrice" type="text"  onkeyup="doubleChange(this)" class="col-sm-6" placeholder="优惠价" /> 元</td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmCreateButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>

	<div id="modifyDevicePriceDialog" class="modal fade">
		<div class="window-box modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-padding">
					<div class="table-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<span class="box-close">×</span>
						</button>
						编辑设备续期价格
					</div>
				</div>

				<div class="modal-body no-padding">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<form id="modifyDevicePriceForm" class="form-horizontal" role="form">
									<input id="mId" type="hidden" />
									<table class="table detail-tab layui-form">
										<tr>
											<td width="100px" align="right"><span class="required_style">*</span>名称：</td>
											<td><input id="mName" type="text" class="col-sm-6" placeholder="名称" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>天数：</td>
											<td><input id="mDays" type="text" onkeyup="integerChange(this)" class="col-sm-6" placeholder="天数" /></td>
										</tr>
										<tr>
											<td align="right"><span class="required_style">*</span>原价：</td>
											<td><input id="mSalePrice" type="text" onkeyup="doubleChange(this)" class="col-sm-6" placeholder="原价 " /> 元</td>
										</tr>
										<tr>
											<td align="right">优惠价：</td>
											<td><input id="mDiscountPrice" type="text" onkeyup="doubleChange(this)" class="col-sm-6" placeholder="优惠价" /> 元</td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer no-margin-top">
					<button id="confirmModifyButton" type="button" class="btn btn-primary">确定</button>
					<button class="btn" data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
