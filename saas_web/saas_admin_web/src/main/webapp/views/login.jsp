<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="description" content="我们以先进的人工智能和大数据为核心,打造一站式户外广告设备管理平台,帮助户外媒体主降低销售、管理、维护成本,增加广告收入,提高户外媒体的核心竞争力。" />
<meta name="keywords" content="奥凌,屏多多,广告计划,广告交易平台,户外媒体,户外广告,智云联众,媒体主管理平台,户外媒体管理平台" />
<title>奥凌媒体服务运营管理平台</title>
<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
<link rel="stylesheet" type="text/css" href="css/platform.css" />
<script type="text/javascript" src="js/common/jquery-1.10.2.js"></script>
<script type="text/javascript" src="js/common/jquery.call.interface.js"></script>
<script type="text/javascript" src="js/common/utils.js"></script>
<script type="text/javascript" src="assets/js/jquery.tips.js"></script>
<script type="text/javascript" src="assets/js/jquery.cookie.js"></script>
<script type="text/javascript" src="assets/js/jquerysession.js"></script>
<script type="text/javascript" src="assets/js/jquery.md5.js"></script>
<!--[if lte IE 8]>
	  <script type="text/javascript" src="assets/js/excanvas.min.js"></script>
	<![endif]-->
<script type="text/javascript" src="js/login.js?v=<%=System.currentTimeMillis()%>"></script>
</head>
<body style="background: #00172f;">
	<div class="login">
		<div class="login-box">
			<div class="login-main">
				<a href="javascript:;" class="logo-ico">
					<img src="images/login-logo.png" />
				</a>
				<div class="login-content">
					<ul class="login-form">
						<li><span class="form-ico"><i class="icons-admin"></i></span> <input type="text" id="username"
							class="form-text" placeholder="请输入用户名" autocomplete="off" style="left:60px;width:84%;" /></li>
						<li><span class="form-ico"><i class="icons-password"></i></span> <input type="password" id="password"
							class="form-text" placeholder="请输入密码" autocomplete="off" style="left:60px;width:84%;" /></li>
						<li><span class="form-ico"><i class="icons-verification"></i></span> <input type="text" id="authcode"
							onkeyup="integerChange(this)" placeholder="请输入验证码" style="left:60px;width:84%;" />&nbsp; <a href="#" class="yz"><img id="authimage"
								width="88" height="30" alt="验证码" title="点击更换" /></a></li>
						<li class="login-loading"><label class="remember-click"> <span class="remember"><i
									class="rember-on icons-rember"></i></span> 记住密码
									<a href="forget_pwd" class="forget">忘记密码？</a>
						</label></li>
						<li class="login-sub"><a href="#" id="loginButton">登录</a></li>
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</body>
</html>