package com.wayz.interceptor;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class InterceptorConfig implements WebMvcConfigurer {
    @Bean
    public JsonAuthorityInterceptor jsonAuthorityInterceptor(){
        return new JsonAuthorityInterceptor();
    }
    @Bean
    public ViewAuthorityInterceptor viewAuthorityInterceptor(){
        return new ViewAuthorityInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        InterceptorRegistration jsonAuthorityInterceptor = registry
                .addInterceptor(jsonAuthorityInterceptor());
        InterceptorRegistration viewAuthorityInterceptor = registry
                .addInterceptor(viewAuthorityInterceptor());
        // 拦截所有、排除
        viewAuthorityInterceptor
                .addPathPatterns("/logout", "/index", "/play_version", "/version_management",
                        "/role_management", "/user_management", "/company_management", "/company_detail",
                        "/news_management", "/news_detail", "/case_management", "/case_detail", "/form_data_list", "/system_notice_management",
                        "/content_management", "/content_category_management", "/content_audit_management",
                        "/company_cibn_audit", "/position_cibn_audit", "/application_management",
                        "/auditor_management", "/live_channel_management", "/coupon_management","/position_management","/device_price_management",
                        "/space_price_management")
        ;
        jsonAuthorityInterceptor
                .addPathPatterns("/authority/queryMyMenu", "/authority/queryUser", "/authority/createUser", "/authority/modifyUser", "/authority/deleteUser", "/authority/modifyPassword", "/authority/queryMenuRight", "/authority/queryRoleRight", "/authority/modifyRoleRight", "/authority/queryRole", "/authority/createRole", "/authority/modifyRole", "/authority/deleteRole",
                        "/saas/cibn/queryCibnList", "/saas/cibn/auditCibn", "/saas/cibn/getCibnetail",
                        "/auditor/queryAuditor", "/auditor/getAuditorDetail", "/auditor/createAuditor", "/auditor/modifyAuditor", "/auditor/modifyAuditorStatus", "/auditor/deleteAuditor", "/auditor/resetAuditorPassword", "/auditor/querySelectablePosition", "/auditor/querySelectedPosition", "/auditor/addPosition", "/auditor/deletePosition",
                        "/admin/createLiveChannel", "/admin/modifyLiveChannel", "/admin/deleteLiveChannel", "/admin/queryLiveChannel",
                        "/admin/queryLiveResolutionView", "/admin/getLiveResolution",
                        "/admin/queryLivePlanTemplet", "/admin/queryLivePlanTempletView", "/admin/createLivePlanTemplet", "/admin/modifyLivePlanTemplet", "/admin/deleteLivePlanTemplet", "/admin/getLivePlanTempletDetail",
                        "/file/uploadFile"
                )
                .excludePathPatterns("/authority/getAuthcode","/ok")
        ;
    }
}
