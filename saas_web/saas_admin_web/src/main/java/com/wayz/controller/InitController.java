package com.wayz.controller;

import com.wayz.exception.WayzException;
import com.wayz.response.WayzResponse;
import com.wayz.service.InitService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @className: InitController
 * @description: 初始化系统
 * <功能详细描述>
 * @author: 须尽欢_____
 * @date: 2019/7/1
 */
@Controller
@RequestMapping("/init")
public class InitController {

    @Reference(version = "1.0.0", check = false)
    private InitService initService;
    /**
     * 查询全部菜单
     *
     * @return yunge应答
     * @throws WayzException 云歌广告平台异常
     */
    @ResponseBody
    @RequestMapping(value = "/initAdmin", method = { RequestMethod.GET, RequestMethod.POST })
    public WayzResponse queryAllMenu() throws WayzException {
        // 初始化
        WayzResponse $response = new WayzResponse();

        // 调用接口
        $response.setData(initService.init());

        // 返回应答
        return $response;
    }

}
