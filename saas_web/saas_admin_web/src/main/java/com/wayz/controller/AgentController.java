package com.wayz.controller;

import com.wayz.entity.dto.agent.AgentDto;
import com.wayz.exception.WayzException;
import com.wayz.response.WayzResponse;
import com.wayz.service.AgentService;
import com.wayz.service.AgentUserService;
import com.wayz.validator.ParameterValidator;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


/**
 * 代理商管理类
 *
 * @author mike.ma@wayz.ai
 */
@Controller
@RequestMapping("/agent")
@SessionAttributes("myId")
public class AgentController {

	/** 代理商服务 */
	@Reference(version = "1.0.0", check = false)
	private AgentService AgentService;
	/** 代理商用户服务 */
	@Reference(version = "1.0.0", check = false)
	private AgentUserService agentUserService;

	/**
	 * 代理商列表
	 * 
	 * @param myId 我的ID
	 * @param name 代理商名称
	 * @param phone 手机号
	 * @param status 状态
	 * @param startIndex 开始角标
	 * @param pageSize 每页大小
	 * @return
	 * @throws WayzException
	 */
	@ResponseBody
	@RequestMapping(value = "/queryAgentCompany", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse queryAgentCompany(@ModelAttribute("myId") Long myId,
										  @RequestParam(value = "name", required = false) String name,
										  @RequestParam(value = "phone", required = false) String phone,
										  @RequestParam(value = "status", required = false) Short status,
										  @RequestParam(value = "provinceId", required = false) Long provinceId,
										  @RequestParam(value = "cityId", required = false) Long cityId,
										  @RequestParam(value = "categoryId", required = false) Long categoryId,
										  @RequestParam(value = "startIndex") Integer startIndex, @RequestParam(value = "pageSize") Integer pageSize)
			throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		response.setData(AgentService.queryAgentCompany(myId, name, phone, status, provinceId, cityId, categoryId,
				startIndex, pageSize));
		return response;
	}

	/**
	 * 获取代理商详情
	 *
	 * @param myId 我的标识
	 * @param id 代理商标识
	 * @return 云歌应答
	 * @throws WayzException 云歌广告平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/getAgentCompanyDetail", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse getAgentCompanyDetail(@ModelAttribute("myId") Long myId, @RequestParam("id") Long id)
			throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		// 调用接口
		response.setData(AgentService.getAgentCompany(myId, id));
		// 返回应答
		return response;
	}

	/**
	 *
	 * 创建代理商
	 * @param myId 我的ID
	 * @param dto 实体类
	 * @return
	 * @throws WayzException
	 */
	@ResponseBody
	@RequestMapping(value = "/createAgentCompany", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse createAgentCompany(@ModelAttribute("myId") Long myId, AgentDto dto) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		// 调用接口
		AgentService.createAgentCompany(myId, dto);
		// 返回应答
		return response;
	}

	/**
	 * 修改代理商
	 * 
	 * @param myId 我的ID
	 * @param dto 实体类
	 * @return
	 * @throws WayzException
	 */
	@ResponseBody
	@RequestMapping(value = "/modifyAgentCompany", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse modifyAgentCompany(@ModelAttribute("myId") Long myId, AgentDto dto) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		// 验证参数: 代理商标识
		ParameterValidator.notNull(dto.getId(), "代理商标识");
		// 调用接口
		AgentService.modifyAgentCompany(myId, dto);
		// 返回应答
		return response;
	}

	/**
	 * 修改代理商状态
	 * 
	 * @param myId 我的ID
	 * @param id 代理商ID
	 * @param status 代理商状态(1:启用 2:禁用;)
	 * @return
	 * @throws WayzException
	 */
	@ResponseBody
	@RequestMapping(value = "/modifyAgentStatus", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse modifyAgentStatus(@ModelAttribute("myId") Long myId, @RequestParam("id") Long id,
			@RequestParam("status") Short status) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		// 验证参数: 代理商标识
		ParameterValidator.notNull(id, "代理商标识");
		// 调用接口
		response.setData(AgentService.modifyAgentStatus(id, status));
		// 返回应答
		return response;
	}

	/**
	 * 获取代理商OEM
	 * 
	 * @param myId
	 * @param id 代理商ID
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getAgentOEM", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse getAgentOEM(@ModelAttribute("myId") Long myId, @RequestParam("id") Long id)
			throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		// 调用接口
		response.setData(AgentService.getAgentOEM(myId, id));
		// 返回应答
		return response;
	}

	/**
	 * OEM配置
	 *
	 * @param myId 我的标识
	 * @return 云歌应答
	 * @throws WayzException 云歌广告平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/createAgentOEM", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse createAgentOEM(@ModelAttribute("myId") Long myId, @RequestParam("id") Long id,
			@RequestParam("logo") String logo, @RequestParam("domain") String domain,
			@RequestParam("websiteTitle") String websiteTitle) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		// 验证参数
		AgentService.createAgentOEM(myId, id, logo, domain, websiteTitle);
		// 返回应答
		return response;
	}

	/**
	 * 代理商重置密码
	 * 
	 * @param myId 我的ID
	 * @param id 代理商ID
	 * @param newPassword 新的密码
	 * @return
	 * @throws WayzException
	 */
	@ResponseBody
	@RequestMapping(value = "/resetAgentPassword", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse resetAgentPassword(@ModelAttribute("myId") Long myId, @RequestParam("id") Long id,
			@RequestParam(value = "newPassword", required = false) String newPassword) throws WayzException {
		WayzResponse response = new WayzResponse();
		agentUserService.resetAgentUserPassword(myId, id, newPassword);
		return response;
	}

}
