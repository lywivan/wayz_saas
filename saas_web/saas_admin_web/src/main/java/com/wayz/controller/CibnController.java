package com.wayz.controller;

import com.wayz.entity.dto.cibn.CompanyCibnDto;
import com.wayz.exception.WayzException;
import com.wayz.response.WayzResponse;
import com.wayz.service.CategoryService;
import com.wayz.service.CompanyCibnService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

/**
 * CIBN控制类
 */
@Controller
@RequestMapping("/saas/cibn")
@SessionAttributes("myId")
public class CibnController {

    /** CIBN服务 */
    @Reference(version = "1.0.0", check = false)
    private CompanyCibnService companyCibnService;

    /**
     * 申请记录列表
     * @param myId
     * @param companyName
     * @param auditStatus
     * @param industryId
     * @param startIndex
     * @param pageSize
     * @return
     * @throws WayzException
     */
    @ResponseBody
    @RequestMapping(value = "/queryCibnList", method = {RequestMethod.GET, RequestMethod.POST})
    public WayzResponse queryCibnList(@ModelAttribute("myId") Long myId,
                                      @RequestParam(value = "companyName", required = false) String companyName,
                                      @RequestParam(value = "auditStatus", required = false) Short auditStatus,
                                      @RequestParam(value = "industryId", required = false) Long industryId,
                                      @RequestParam(value = "startIndex", required = false) Integer startIndex,
                                      @RequestParam(value = "pageSize", required = false) Integer pageSize) throws WayzException {
        WayzResponse response = new WayzResponse();
        response.setData(companyCibnService.query(companyName, auditStatus, industryId, startIndex, pageSize));
        return response;
    }


    /**
     * 审核申请记录
     * @param myId
     * @param companyCibnDto
     * @return
     * @throws WayzException
     */
    @ResponseBody
    @RequestMapping(value = "/auditCibn", method = {RequestMethod.GET, RequestMethod.POST})
    public WayzResponse auditCibn(@ModelAttribute("myId") Long myId, CompanyCibnDto companyCibnDto
                                       ) throws WayzException {
        WayzResponse response = new WayzResponse();
        companyCibnService.auditCibn(companyCibnDto);
        return response;
    }

    /**
     * 申请记录详情
     * @param companyId
     * @return
     * @throws WayzException
     */
    @ResponseBody
    @RequestMapping(value = "/getCibnetail", method = {RequestMethod.GET, RequestMethod.POST})
    public WayzResponse getCibnetail(@ModelAttribute("companyId") Long companyId) throws WayzException {
        WayzResponse response = new WayzResponse();
        response.setData(companyCibnService.getAdminCibn(companyId));
        return response;
    }

}
