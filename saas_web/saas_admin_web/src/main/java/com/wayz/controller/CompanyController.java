package com.wayz.controller;

import com.wayz.annotation.DYGTransactional;
import com.wayz.entity.dto.company.CompanyDto;
import com.wayz.entity.dto.company.CompanyPageDto;
import com.wayz.exception.WayzException;
import com.wayz.response.WayzResponse;
import com.wayz.service.CompanySaasService;
import com.wayz.service.PositionService;
import com.wayz.validator.ParameterValidator;
//import io.seata.spring.annotation.GlobalTransactional;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

/**
 * 公司控制器类
 */
@Controller
@RequestMapping("/saas/company")
@SessionAttributes("myId")
public class CompanyController {

	/** 公司代理 */
	@Reference(version = "1.0.0", check=false)
	private CompanySaasService companyService = null;

	/** 媒体服务 */
	@Reference(version = "1.0.0", check=false)
	private PositionService positionService = null;

	/**
	 * 获取公司详情
	 * 
	 * @param myId 我的标识
	 * @param id 公司标识
	 * @return 云歌应答
	 * @throws WayzException 云歌广告平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/getCompanyDetail", method = {RequestMethod.POST })
	public WayzResponse getCompanyDetail(@ModelAttribute("myId") Long myId, @RequestParam("id") Long id)
			throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		// 调用接口
		response.setData(companyService.getCompanyDetail(myId, id));

		// 返回应答
		return response;
	}

	/**
	 * 创建公司
	 * 
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/createCompany", method = { RequestMethod.POST })
	@DYGTransactional
//	@GlobalTransactional
	public WayzResponse createCompany(@ModelAttribute("myId") Long myId, CompanyDto companyDto)
			throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		// 调用接口
		Long companyId = companyService.createCompany(myId, companyDto);
		// 创建默认设备组
		positionService.createPositionGroup(myId, companyId, 0L, "默认组", "默认组", null, null, null, null, null, "08:00:00",
				"22:00:00", true, null, null, (short) 5, 0, true, true);
		// 返回应答
		return response;
	}

	/**
	 * 修改公司
	 * 
	 * @param myId 我的标识
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/modifyCompany", method = { RequestMethod.POST })
	public WayzResponse modifyCompany(@ModelAttribute("myId") Long myId, CompanyDto companyDto)
			throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();

		// 验证参数
		// 验证参数: 公司标识
		ParameterValidator.notNull(companyDto.getId(), "公司标识");

		// 调用接口
		companyService.modifyCompany(myId, companyDto);

		// 返回应答
		return response;
	}

	/**
	 * 修改公司状态
	 *
	 * @param myId 我的标识
	 * @param status 公司状态
	 * @return platform应答
	 */
	@ResponseBody
	@RequestMapping(value = "/modifyCompanyStatus", method = { RequestMethod.POST })
	public WayzResponse modifyCompanyStatus(@ModelAttribute("myId") Long myId, @RequestParam("id") Long id,
			@RequestParam(value = "status", required = false) Short status,
			@RequestParam(value = "validUntilDate", required = false) String validUntilDate,
			@RequestParam(value = "isWatermark", required = false) Boolean isWatermark) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();

		// 验证参数
		ParameterValidator.notNull(status, "公司状态");

		// 调用接口
		response.setData(companyService.modifyCompanyStatus(id, status, validUntilDate, isWatermark));
		// 返回应答
		return response;
	}

	/**
	 * 获取公司列表
	 *
	 * @param myId 我的标识
	 * @param companyPageDto 公司创建来源 1：管理员 2：用户自主
	 * @return
	 * @throws WayzException 云歌广告平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/queryCompany", method = { RequestMethod.POST })
	public WayzResponse queryCompany(@ModelAttribute("myId") Long myId,
									 CompanyPageDto companyPageDto)
			throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		response.setData(companyService.queryCompany(myId, companyPageDto));
		return response;
	}

	/**
	 * OEM配置
	 *
	 * @param myId 我的标识
	 * @return 云歌应答
	 * @throws WayzException 云歌广告平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/createOEM", method = { RequestMethod.POST })
	public WayzResponse createOEM(@ModelAttribute("myId") Long myId, @RequestParam("companyId") Long id,
			@RequestParam("logo") String logo, @RequestParam("domain") String domain,
			@RequestParam("websiteTitle") String websiteTitle) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		// 验证参数
		companyService.updateCompanyOEM(myId, id, logo, domain, websiteTitle);
		// 返回应答
		return response;
	}

	/**
	 * 获取公司OEM详情
	 *
	 * @param myId 我的标识
	 * @param id 公司标识
	 * @return 云歌应答
	 * @throws WayzException 云歌广告平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/getCompanyOEM", method = { RequestMethod.POST })
	public WayzResponse getCompanyOEM(@ModelAttribute("myId") Long myId, @RequestParam("id") Long id)
			throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		// 调用接口
		response.setData(companyService.getCompanyOEM(myId, id));

		// 返回应答
		return response;
	}

	/**
	 * 删除公司
	 *
	 * @param myId 我的标识
	 * @param id 公司标识
	 * @return 云歌应答
	 * @throws WayzException 云歌广告平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteCompany", method = { RequestMethod.POST })
	public WayzResponse deleteCompany(@ModelAttribute("myId") Long myId, @RequestParam("id") Long id)
			throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		// 调用接口
		companyService.deleteCompany(myId, id);
		// 返回应答
		return response;
	}

	/**
	 * 公司充值
	 *
	 * @param myId 用户标识
	 * @param companyId 公司标识
	 * @param amount 充值金额(元)
	 * @param payment 实付金额(元)
	 * @param tradeType 充值类型(11:现金充值 12:红包充值 13:转账充值 14:退款充值)
	 * @param remark 备注
	 * 
	 * @return 账户余额
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/rechargeCompany", method = { RequestMethod.POST })
	public WayzResponse rechargeCompany(@ModelAttribute("myId") Long myId, @RequestParam("companyId") Long companyId,
			@RequestParam("amount") Double amount, @RequestParam("payment") Double payment,
			@RequestParam("tradeType") Short tradeType, @RequestParam("remark") String remark) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		// 调用接口
		response.setData(companyService.rechargeCompany(myId, companyId, amount, payment, tradeType, remark));
		// 返回应答
		return response;
	}

	/**
	 * 查询公司视图
	 * 
	 * @param myId 我的标识
	 * @return 云歌应答
	 * @throws WayzException 云歌广告平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/queryCompanyViewMap", method = { RequestMethod.POST })
	public WayzResponse queryCompanyViewMap(@ModelAttribute("myId") Long myId) {
		// 初始化
		WayzResponse response = new WayzResponse();
		// 调用接口
		response.setData(companyService.queryViewMap());
		// 返回应答
		return response;
	}

	/**
	 * 查询交易概要
	 * 
	 * @param myId 我的标识
	 * @param companyId 公司标识
	 * @param operatorType 操作员类型类型(1:平台 2:代理 3:租户 )
	 * @param tradeType 交易类型(11:现金充值 12:红包充值 13:转账充值 14:退款充值 21:设备续期消费
	 *            22:空间扩展消费)
	 * @param beginDate 开始日期(YYYY-MM-DD)
	 * @param endDate 结束日期(YYYY-MM-DD)
	 * @param startIndex 开始序号
	 * @param pageSize 页面大小
	 * @return 交易概要分页
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/queryTradeRecord", method = { RequestMethod.POST })
	public WayzResponse queryTradeRecord(@ModelAttribute("myId") Long myId, @RequestParam("companyId") Long companyId,
			@RequestParam(value = "operatorType", required = false) Short operatorType,
			@RequestParam(value = "tradeType", required = false) Short tradeType,
			@RequestParam(value = "beginDate", required = false) String beginDate,
			@RequestParam(value = "endDate", required = false) String endDate,
			@RequestParam(value = "startIndex", required = false) Integer startIndex,
			@RequestParam(value = "pageSize", required = false) Integer pageSize) throws WayzException {

		// 初始化
		WayzResponse $response = new WayzResponse();

		// 调用接口
		$response.setData(companyService.queryTradeRecord(myId, companyId, operatorType, tradeType, beginDate, endDate,
				startIndex, pageSize));

		// 返回应答
		return $response;
	}

	/**
	 * 获取公司主账户
	 * 
	 * @param myId 我的标识
	 * @param id 公司标识
	 * @return 云歌应答
	 * @throws WayzException 云歌广告平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/getCompanyMainAccount", method = { RequestMethod.POST })
	public WayzResponse getCompanyMainAccount(@ModelAttribute("myId") Long myId, @RequestParam("id") Long id)
			throws WayzException {
		WayzResponse response = new WayzResponse();
		response.setData(companyService.getCompanyMainAccount(myId, id));
		return response;
	}

	/**
	 * 重置主账号密码
	 * 
	 * @param myId
	 * @param id
	 * @return
	 * @throws WayzException
	 */
	@ResponseBody
	@RequestMapping(value = "/resetUserPassword", method = { RequestMethod.POST })
	public WayzResponse resetUserPassword(@ModelAttribute("myId") Long myId, @RequestParam("id") Long id,
			@RequestParam(value = "newPassword", required = false) String newPassword) throws WayzException {
		WayzResponse response = new WayzResponse();
		companyService.resetUserPassword(myId, id, newPassword);
		return response;
	}

}
