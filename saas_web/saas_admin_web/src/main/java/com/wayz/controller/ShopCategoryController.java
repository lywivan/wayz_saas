package com.wayz.controller;

import com.wayz.exception.WayzException;
import com.wayz.response.WayzResponse;
import com.wayz.service.CategoryService;
import com.wayz.service.ContentCategoryService;
import com.wayz.validator.ParameterValidator;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/saas/category")
@SessionAttributes("myId")
public class ShopCategoryController {

	/**
	 * 分类服务
	 */
	@Reference(version = "1.0.0", check = false)
	private ContentCategoryService contentCategoryService;

	/**
	 * 分类列表
	 *
	 * @param myId 我的标识
	 * @param name 分类名称
	 * @param startIndex 开始角标
	 * @param pageSize 页面大小
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/queryCategory", method = { RequestMethod.POST })
	public WayzResponse queryCategory(@ModelAttribute("myId") Long myId,
									  @RequestParam(value = "name", required = false) String name,
									  @RequestParam(value = "startIndex", required = false) Integer startIndex,
									  @RequestParam(value = "pageSize", required = false) Integer pageSize) throws WayzException {
		WayzResponse response = new WayzResponse();
		response.setData(contentCategoryService.queryContentCategory(myId, name, startIndex, pageSize));
		return response;
	}

	/**
	 * 分类视图列表
	 * 
	 * @param myId
	 * @param name
	 */
	@ResponseBody
	@RequestMapping(value = "/queryCategoryView", method = { RequestMethod.POST })
	public WayzResponse queryCategoryView(@ModelAttribute("myId") Long myId,
			@RequestParam(value = "name", required = false) String name) throws WayzException {
		WayzResponse response = new WayzResponse();
		response.setData(contentCategoryService.queryContentCategoryView(name));
		return response;
	}

	/**
	 * 创建分类
	 *
	 * @param myId 我的标识
	 * @param name 分类名称
	 * @param description 描述
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/createCategory", method = { RequestMethod.POST })
	public WayzResponse createCategory(@ModelAttribute("myId") Long myId,
			@RequestParam(value = "name", required = false) String name,
			@RequestParam(value = "description", required = false) String description) throws WayzException {
		WayzResponse response = new WayzResponse();
		ParameterValidator.notNull(name, "分类名称有误");
		contentCategoryService.createContentCategory(myId, name, description);
		return response;
	}

	/**
	 * 删除分类
	 *
	 * @param myId 我的标识
	 * @param id 主键ID
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteCategory", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse deleteCategory(@ModelAttribute("myId") Long myId,
			@RequestParam(value = "id", required = false) Long id) throws WayzException {
		WayzResponse response = new WayzResponse();
		ParameterValidator.notNull(id, "分类信息有误");
		contentCategoryService.deleteCategory(myId, id);
		return response;
	}

	/**
	 * 修改分类
	 *
	 * @param myId 我的标识
	 * @param id 主键ID
	 * @param name 分类名称
	 * @param description 描述
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/modifyCategory", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse modifyCategory(@ModelAttribute("myId") Long myId,
			@RequestParam(value = "id", required = false) Long id,
			@RequestParam(value = "name", required = false) String name,
			@RequestParam(value = "description", required = false) String description) throws WayzException {
		WayzResponse response = new WayzResponse();
		ParameterValidator.notNull(id, "分类信息有误");
		contentCategoryService.modifyCategory(myId, id, name, description);
		return response;
	}

	/**
	 * 分类详情
	 *
	 * @param myId 我的标识
	 * @param id 主键ID
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getCategory", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse getCategory(@ModelAttribute("myId") Long myId,
			@RequestParam(value = "id", required = false) Long id) throws WayzException {
		WayzResponse response = new WayzResponse();
		ParameterValidator.notNull(id, "分类信息有误");
		response.setData(contentCategoryService.getContentCategory(myId, id));
		return response;
	}

}
