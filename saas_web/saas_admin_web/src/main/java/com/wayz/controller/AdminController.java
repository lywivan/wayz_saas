package com.wayz.controller;

import com.wayz.entity.dto.coupon.CouponPageDto;
import com.wayz.entity.dto.device.DTenancyPriceDto;
import com.wayz.entity.dto.live.LiveTempletDto;
import com.wayz.entity.dto.live.LiveTempletPageDto;
import com.wayz.entity.pojo.coupon.DCoupon;
import com.wayz.exception.WayzException;
import com.wayz.response.WayzResponse;
import com.wayz.service.CouponService;
import com.wayz.service.LiveService;
import com.wayz.service.TradeService;
import com.wayz.validator.ParameterValidator;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * 平台管理控制器类
 * 
 * @author yinjy
 *
 */
@Controller
@RequestMapping("/admin")
@SessionAttributes("myId")
public class AdminController {
	/** 代金卷服务 */
	@Reference(version = "1.0.0", check = false)
	private CouponService couponService = null;
	/** 交易服务 */
	@Reference(version = "1.0.0", check = false)
	private TradeService tradeService = null;
	/** 直播服务 */
	@Reference(version = "1.0.0", check = false)
	private LiveService liveService = null;

	/**
	 * 查询代金券
	 *
	 * @param myId 我的标识
	 * @param couponPageDto 分页查询参数
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/queryCoupon", method = { RequestMethod.POST })
	public WayzResponse queryCoupon(@ModelAttribute("myId") Long myId,
									CouponPageDto couponPageDto) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数

		// 调用接口
		$response.setData(couponService.queryCoupon(myId, couponPageDto.getProductType(), couponPageDto.getIsEnable()
				, couponPageDto.getStartIndex(), couponPageDto.getPageSize()));

		// 返回应答
		return $response;
	}

	/**
	 * 创建代金券
	 *
	 * @param myId 我的标识
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/createCoupon", method = { RequestMethod.POST })
	public WayzResponse createCoupon(@ModelAttribute("myId") Long myId,
									 DCoupon dCoupon) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		ParameterValidator.notNull(dCoupon.getName(), "名称");
		ParameterValidator.notNull(dCoupon.getProductType(), "优惠券类型");
		ParameterValidator.notNull(dCoupon.getFullAmount(), "最低消费");
		ParameterValidator.notNull(dCoupon.getReduceAmount(), "减免金额");
		ParameterValidator.notNull(dCoupon.getProvideNumber(), "发放数量");
		ParameterValidator.notNull(dCoupon.getValidDays(), "有效天数");

		// 调用接口
		$response.setData(couponService.createCoupon(myId, dCoupon.getName(), dCoupon.getTitle(),
				dCoupon.getProductType(), dCoupon.getFullAmount(), dCoupon.getReduceAmount(),
				dCoupon.getProvideNumber(), dCoupon.getValidDays(), dCoupon.getIsPublic()));

		// 返回应答
		return $response;
	}

	/**
	 * 修改代金券
	 *
	 * @param myId 我的标识
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/modifyCoupon", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse modifyCoupon(@ModelAttribute("myId") Long myId,
									  DCoupon dCoupon) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		ParameterValidator.notNull(dCoupon.getId(), "优惠券标示");
		ParameterValidator.notNull(dCoupon.getName(), "名称");
		ParameterValidator.notNull(dCoupon.getProductType(), "优惠券类型");
		ParameterValidator.notNull(dCoupon.getFullAmount(), "最低消费");
		ParameterValidator.notNull(dCoupon.getReduceAmount(), "减免金额");
		ParameterValidator.notNull(dCoupon.getProvideNumber(), "发放数量");
		ParameterValidator.notNull(dCoupon.getValidDays(), "有效天数");

		// 调用接口
		couponService.modifyCoupon(myId, dCoupon.getId(), dCoupon.getName(), dCoupon.getTitle(),
				dCoupon.getProductType(), dCoupon.getFullAmount(), dCoupon.getReduceAmount(),
				dCoupon.getProvideNumber(), dCoupon.getValidDays(), dCoupon.getIsPublic());

		// 返回应答
		return $response;
	}

	/**
	 * 生效代金券
	 *
	 * @param myId 我的标识
	 * @param id 标识
	 * @param isEnable 是否启用
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/enableCoupon", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse enableCoupon(@ModelAttribute("myId") Long myId,
									  @RequestParam(value = "id", required = false) Long id,
									  @RequestParam(value = "isEnable", required = false) Boolean isEnable) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		ParameterValidator.notNull(id, "优惠券标示");

		// 调用接口
		couponService.enableCoupon(myId, id, isEnable);

		// 返回应答
		return $response;
	}

	/**
	 * 分配代金券
	 *
	 * @param myId 我的标识
	 * @param couponId 代金券标识
	 * @param companyId 公司标识
	 * @param days 有效天数
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/distributeCoupon", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse distributeCoupon(@ModelAttribute("myId") Long myId,
										  @RequestParam(value = "couponId", required = false) Long couponId,
										  @RequestParam(value = "companyId", required = false) Long companyId,
										  @RequestParam(value = "days", required = false) Integer days) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		ParameterValidator.notNull(couponId, "优惠券标示");
		ParameterValidator.notNull(days, "有效天数");

		// 调用接口
		couponService.distributeCoupon(myId, couponId, companyId, days);

		// 返回应答
		return $response;
	}

	/**
	 * 创建媒体续期价格
	 *
	 * @param myId 我的标识
	 * @param days 天数
	 * @param name 显示名称
	 * @param salePrice 原价
	 * @param discountPrice 优惠价
	 * @return platform应答
	 */
	@ResponseBody
	@RequestMapping(value = "/createDevicePrice", method = { RequestMethod.POST })
	public WayzResponse createDevicePrice(@ModelAttribute("myId") Long myId,
										   @RequestParam(value = "days", required = false) Integer days,
										   @RequestParam(value = "name", required = false) String name,
										   @RequestParam(value = "salePrice", required = false) Double salePrice,
										   @RequestParam(value = "discountPrice", required = false) Double discountPrice) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数

		// 调用接口
		$response.setData(tradeService.createDevicePrice(myId, days, name, salePrice, discountPrice));

		// 返回应答
		return $response;
	}

	/**
	 * 修改媒体续期价格
	 *
	 * @param myId 我的标识
	 * @return platform应答
	 */
	@ResponseBody
	@RequestMapping(value = "/modifyDevicePrice", method = { RequestMethod.POST })
	public WayzResponse modifyDevicePrice(@ModelAttribute("myId") Long myId,
										  DTenancyPriceDto dto) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数

		// 调用接口
		tradeService.modifyDevicePrice(myId, dto.getId(), dto.getDays(),
				dto.getName(), dto.getSalePrice(), dto.getDiscountPrice());

		// 返回应答
		return $response;
	}

	/**
	 * 删除媒体续期价格
	 *
	 * @param myId 我的标识
	 * @param id 标识
	 * @return platform应答
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteDevicePrice", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse deleteDevicePrice(@ModelAttribute("myId") Long myId,
										   @RequestParam(value = "id", required = false) Long id) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数

		// 调用接口
		tradeService.deleteDevicePrice(myId, id);

		// 返回应答
		return $response;
	}

	/**
	 * 查询媒体续期价格
	 *
	 * @param myId 我的标识
	 * @return platform应答
	 */
	@ResponseBody
	@RequestMapping(value = "/queryDevicePrice", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse queryDevicePrice(@ModelAttribute("myId") Long myId) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 调用接口
		$response.setData(tradeService.queryDevicePrice(myId));

		// 返回应答
		return $response;
	}


	/**
	 * 创建空间价格
	 *
	 * @param myId 我的标识
	 * @param name 显示名称
	 * @param salePrice 原价
	 * @param discountPrice 优惠价
	 * @return platform应答
	 */
	@ResponseBody
	@RequestMapping(value = "/createSpacePrice", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse createSpacePrice(@ModelAttribute("myId") Long myId,
										  @RequestParam(value = "space", required = false) Integer space,
										  @RequestParam(value = "name", required = false) String name,
										  @RequestParam(value = "salePrice", required = false) Double salePrice,
										  @RequestParam(value = "discountPrice", required = false) Double discountPrice) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数

		// 调用接口
		$response.setData(tradeService.createSpacePrice(myId, space, name, salePrice, discountPrice));

		// 返回应答
		return $response;
	}

	/**
	 * 修改空间价格
	 *
	 * @param myId 我的标识
	 * @param space 天数
	 * @param name 显示名称
	 * @param salePrice 原价
	 * @param discountPrice 优惠价
	 * @return platform应答
	 */
	@ResponseBody
	@RequestMapping(value = "/modifySpacePrice", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse modifySpacePrice(@ModelAttribute("myId") Long myId,
										  @RequestParam(value = "space", required = false) Integer space,
										  @RequestParam(value = "name", required = false) String name,
										  @RequestParam(value = "salePrice", required = false) Double salePrice,
										  @RequestParam(value = "discountPrice", required = false) Double discountPrice) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数

		// 调用接口
		tradeService.modifySpacePrice(myId, space, name, salePrice, discountPrice);

		// 返回应答
		return $response;
	}

	/**
	 * 删除空间价格
	 *
	 * @param myId 我的标识
	 * @param space 标识
	 * @return platform应答
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteSpacePrice", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse deleteSpacePrice(@ModelAttribute("myId") Long myId,
										  @RequestParam(value = "space", required = false) Integer space) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数

		// 调用接口
		tradeService.deleteSpacePrice(myId, space);

		// 返回应答
		return $response;
	}

	/**
	 * 查询空间价格
	 *
	 * @param myId 我的标识
	 * @return platform应答
	 */
	@ResponseBody
	@RequestMapping(value = "/querySpacePrice", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse querySpacePrice(@ModelAttribute("myId") Long myId) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 调用接口
		$response.setData(tradeService.querySpacePrice(myId));

		// 返回应答
		return $response;
	}

	/**
	 * 创建直播渠道
	 *
	 * @param myId 我的标识
	 * @param name 显示名称
	 * @return platform应答
	 */
	@ResponseBody
	@RequestMapping(value = "/createLiveChannel", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse createLiveChannel(@ModelAttribute("myId") Long myId,
										   @RequestParam(value = "name", required = false) String name) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		ParameterValidator.notNull(name, "直播渠道名称");

		// 调用接口
		$response.setData(liveService.createLiveChannel(myId, name));

		// 返回应答
		return $response;
	}

	/**
	 * 修改直播渠道
	 *
	 * @param myId 我的标识
	 * @param id 标识
	 * @param name 显示名称
	 * @return platform应答
	 */
	@ResponseBody
	@RequestMapping(value = "/modifyLiveChannel", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse modifyLiveChannel(@ModelAttribute("myId") Long myId,
										   @RequestParam(value = "id", required = false) Long id,
										   @RequestParam(value = "name", required = false) String name) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		ParameterValidator.notNull(id, "直播渠道标识");
		ParameterValidator.notNull(name, "直播渠道名称");

		// 调用接口
		liveService.modifyLiveChannel(myId, id, name);

		// 返回应答
		return $response;
	}

	/**
	 * 删除直播渠道
	 *
	 * @param myId 我的标识
	 * @param id 标识
	 * @return platform应答
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteLiveChannel", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse deleteLiveChannel(@ModelAttribute("myId") Long myId,
										   @RequestParam(value = "id", required = false) Long id) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		ParameterValidator.notNull(id, "直播渠道标识");

		// 调用接口
		liveService.deleteLiveChannel(myId, id);

		// 返回应答
		return $response;
	}

	/**
	 * 查询直播渠道
	 *
	 * @param myId 我的标识
	 * @return platform应答
	 */
	@ResponseBody
	@RequestMapping(value = "/queryLiveChannel", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse queryLiveChannel(@ModelAttribute("myId") Long myId) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 调用接口
		$response.setData(liveService.queryLiveChannel(myId));

		// 返回应答
		return $response;
	}

	/**
	 * 获取直播模板分辨率
	 *
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getLiveResolution", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse getLiveResolution(@ModelAttribute("myId") Long myId,
										   @RequestParam(value = "id", required = false) Long id) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 调用接口
		$response.setData(liveService.getLiveResolution(myId, id));

		// 返回应答
		return $response;
	}

	/**
	 * 查询分屏模板
	 *
	 * @param myId 我的标识
	 * @return platform应答
	 */
	@ResponseBody
	@RequestMapping(value = "/queryLivePlanTemplet", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse queryLivePlanTemplet(@ModelAttribute("myId") Long myId, LiveTempletPageDto liveTempletPageDto) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		response.setData(liveService.queryLivePlanTemplet(myId, liveTempletPageDto));
		return response;
	}

	/**
	 * 查询分屏模板视图列表
	 *
	 * @param myId 我的标识
	 * @return platform应答
	 */
	@ResponseBody
	@RequestMapping(value = "/queryLivePlanTempletView", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse queryLivePlanTempletView(@ModelAttribute("myId") Long myId) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 调用接口
		$response.setData(liveService.queryLivePlanTempletView(myId, null));

		// 返回应答
		return $response;
	}

	/**
	 * 创建分屏模板
	 *
	 * @param myId 我的标识
	 * @param resolutionId 分辨率标识
	 * @param name 模板名称
	 * @param previewUrl 预览URL
	 * @param background 背景颜色
	 * @param adslotList 分屏列表
	 * @return platform应答
	 */
	@ResponseBody
	@RequestMapping(value = "/createLivePlanTemplet", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse createLivePlanTemplet(@ModelAttribute("myId") Long myId,
											  LiveTempletDto liveTempletDto) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数

		// 调用接口
		liveService.createLivePlanTemplet(myId, liveTempletDto);

		// 返回应答
		return $response;
	}

	/**
	 * 修改分屏模板
	 *
	 * @param myId 我的标识
	 * @return platform应答
	 */
	@ResponseBody
	@RequestMapping(value = "/modifyLivePlanTemplet", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse modifyLivePlanTemplet(@ModelAttribute("myId") Long myId,
											  LiveTempletDto liveTempletDto) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数: 模板标识
		ParameterValidator.notNull(liveTempletDto.getId(), "模板标识");

		// 调用接口
		liveService.modifyLivePlanTemplet(myId, liveTempletDto);

		// 返回应答
		return $response;
	}

	/**
	 * 删除分屏模板
	 *
	 * @param myId 我的标识
	 * @param id 分屏模板标识
	 * @return platform应答
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteLivePlanTemplet", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse deleteLivePlanTemplet(@ModelAttribute("myId") Long myId,
											   @RequestParam(value = "id", required = false) Long id) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数: 模板标识
		ParameterValidator.notNull(id, "模板标识");

		// 调用接口
		liveService.deleteLivePlanTemplet(myId, id);

		// 返回应答
		return $response;
	}

	/**
	 * 获取分屏模板详情
	 *
	 * @param myId 我的标识
	 * @param templetId 模板标识
	 * @return platform应答
	 */
	@ResponseBody
	@RequestMapping(value = "/getLivePlanTempletDetail", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse getLivePlanTempletDetail(@ModelAttribute("myId") Long myId,
												  @RequestParam(value = "templetId", required = false) Long templetId) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数: 模板标识
		ParameterValidator.notNull(templetId, "模板标识");

		// 调用接口
		$response.setData(liveService.getLivePlanTempletDetail(myId, templetId));

		// 返回应答
		return $response;
	}
}
