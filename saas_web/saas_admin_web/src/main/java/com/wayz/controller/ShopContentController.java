package com.wayz.controller;

import com.wayz.entity.dto.content.ShopContentDto;
import com.wayz.entity.dto.content.ShopContentPageDto;
import com.wayz.exception.WayzException;
import com.wayz.response.WayzResponse;
import com.wayz.service.ContentService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/saas/shop")
@SessionAttributes("myId")
public class ShopContentController {

    /**
     * 内容服务
     */
    @Reference(version = "1.0.0", check = false)
    private ContentService contentService;


    /**
     * 商城素材列表
     *
     * @param myId       我的标识
     * @return
     * @throws WayzException
     */
    @ResponseBody
    @RequestMapping(value = "/queryContent", method = {RequestMethod.GET, RequestMethod.POST})
    public WayzResponse queryContent(@ModelAttribute("myId") Long myId,
                                     ShopContentPageDto dto) throws WayzException {
        WayzResponse response = new WayzResponse();
        response.setData(contentService.queryShopContent(myId, dto.getChannelId(),
                dto.getCategoryId(), dto.getBeginPrice(), dto.getEndPrice(),
                dto.getType(), dto.getStartIndex(), dto.getPageSize(), dto.getStatus()));
        return response;
    }

    /**
     * 创建内容
     *
     * @param myId       我的标识
     * @return
     * @throws WayzException
     */
    @ResponseBody
    @RequestMapping(value = "/createContent", method = {RequestMethod.GET, RequestMethod.POST})
    public WayzResponse createContent(@ModelAttribute("myId") Long myId,
                                      ShopContentDto dto) throws WayzException {
        WayzResponse response = new WayzResponse();
        if (dto.getFilemd5() == null || dto.getFilemd5().length() != 32){
            throw new WayzException("图片上传错误");
        }
        String fileMd5 = dto.getFilemd5().toUpperCase();

        contentService.createShop(myId, dto.getChannelId(), dto.getCategoryId(),
                dto.getName(),dto.getTitle(),dto.getDescription(), dto.getPrice(),
                dto.getType(), dto.getUrl(), dto.getPreviewUrl(), dto.getFilemd5(),
                dto.getFileSize(), dto.getDuration(), dto.getWidth(), dto.getHeight());
        return response;
    }


    /**
     * 删除内容
     *
     * @param myId 我的标识
     * @param id   主键ID
     * @return
     * @throws WayzException
     */
    @ResponseBody
    @RequestMapping(value = "/deleteContent", method = {RequestMethod.GET, RequestMethod.POST})
    public WayzResponse deleteContent(@ModelAttribute("myId") Long myId,
                                       @RequestParam(value = "id", required = false) Long id) throws WayzException {
        WayzResponse response = new WayzResponse();
        contentService.deleteShop(myId, id);
        return response;
    }


    /**
     * 修改内容
     *
     * @param myId       我的标识
     * @return
     * @throws WayzException
     */
    @ResponseBody
    @RequestMapping(value = "/modifyContent", method = {RequestMethod.GET, RequestMethod.POST})
    public WayzResponse modifyContent(@ModelAttribute("myId") Long myId,
                                       ShopContentDto dto) throws WayzException {
        WayzResponse response = new WayzResponse();
        contentService.modifyShop(myId, dto.getId(), dto.getChannelId(), dto.getCategoryId(),
                dto.getName(),dto.getTitle(),dto.getDescription(), dto.getPrice(),
                dto.getType(),dto.getAuditStatus(), dto.getUrl(), dto.getPreviewUrl(), dto.getFilemd5(),
                dto.getFileSize(), dto.getDuration(), dto.getWidth(), dto.getHeight());
        return response;
    }


    /**
     * 内容详情
     *
     * @param myId 我的标识
     * @param id   主键ID
     * @return
     * @throws WayzException
     */
    @ResponseBody
    @RequestMapping(value = "/getContent", method = {RequestMethod.GET, RequestMethod.POST})
    public WayzResponse getContent(@ModelAttribute("myId") Long myId,
                                    @RequestParam(value = "id", required = false) Long id) throws WayzException {
        WayzResponse response = new WayzResponse();
        response.setData(contentService.getShopContent(myId, id));
        return response;
    }


    /**
     * 内容审核
     * @param myId    审核人
     * @param id      主键ID
     * @param status  0:未审核; 1:审核通过; 2: 审核未通过
     * @return
     * @throws WayzException
     */
    @ResponseBody
    @RequestMapping(value = "/auditorContent", method = {RequestMethod.GET, RequestMethod.POST})
    public WayzResponse auditorContent(@ModelAttribute("myId") Long myId,
                                        @RequestParam(value = "id", required = false) Long id,
                                        @RequestParam(value = "remark", required = false) String remark,
                                        @RequestParam(value = "status", required = false) Short status) throws WayzException {
        WayzResponse response = new WayzResponse();
        contentService.auditorContent(myId,id,remark,status);
        return response;
    }


}
