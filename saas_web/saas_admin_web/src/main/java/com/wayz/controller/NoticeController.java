package com.wayz.controller;

import com.wayz.entity.dto.notice.NoticeDto;
import com.wayz.exception.WayzException;
import com.wayz.response.WayzResponse;
import com.wayz.service.AliossService;
import com.wayz.service.NoticeService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 系统消息通知Controller
 *
 * @author mike.ma@wayz.ai
 */
@Controller
@RequestMapping("/saas/notice")
@SessionAttributes("myId")
public class NoticeController {

    /** 消息通知服务 */
    @Reference(version = "1.0.0", check = false)
    private NoticeService noticeService = null;
    /**
     * 系统消息列表
     *
     * @param title
     * @param type
     * @param startIndex
     * @param pageSize
     * @return
     * @throws WayzException
     */
    @ResponseBody
    @RequestMapping(value = "/queryNotice", method = {RequestMethod.GET, RequestMethod.POST})
    public WayzResponse queryAdminNotice(@RequestParam(value = "title", required = false) String title,
                                         @RequestParam(value = "type", required = false) String type,
                                         @RequestParam(value = "startIndex", required = false) Integer startIndex,
                                         @RequestParam(value = "pageSize", required = false) Integer pageSize) throws WayzException {
        WayzResponse response = new WayzResponse();
        response.setData(noticeService.queryNotice(title, startIndex, pageSize));
        return response;
    }

    /**
     * 创建系统消息通知
     *
     * @param noticeDto
     * @return
     * @throws WayzException
     */
    @ResponseBody
    @RequestMapping(value = "/createNotice", method = {RequestMethod.GET, RequestMethod.POST})
    public WayzResponse createNotice(@ModelAttribute("myId") Long myId, NoticeDto noticeDto) throws WayzException {
        WayzResponse response = new WayzResponse();
        noticeService.createNotice(myId, noticeDto);
        return response;
    }

    /**
     * 修改系统消息通知
     *
     * @param noticeDto
     * @return
     * @throws WayzException
     */
    @ResponseBody
    @RequestMapping(value = "/modifyNotice", method = {RequestMethod.GET, RequestMethod.POST})
    public WayzResponse modifyNotice(@ModelAttribute("myId") Long myId, NoticeDto noticeDto) throws WayzException {
        WayzResponse response = new WayzResponse();
        noticeService.modifyNotice(myId, noticeDto);
        return response;
    }

    /**
     * 消息通知详情
     *
     * @param id
     * @return
     * @throws WayzException
     */
    @ResponseBody
    @RequestMapping(value = "/getNoticeDetail", method = {RequestMethod.GET, RequestMethod.POST})
    public WayzResponse getNoticeDetail(@RequestParam("id") Long id) throws WayzException {
        WayzResponse response = new WayzResponse();
        response.setData(noticeService.getNotice(id));
        return response;
    }

    /**
     * 删除系统消息通知
     *
     * @param noticeId
     * @return
     * @throws WayzException
     */
    @ResponseBody
    @RequestMapping(value = "/deleteNotice", method = {RequestMethod.GET, RequestMethod.POST})
    public WayzResponse deleteNotice(@RequestParam("id") Long noticeId) {
        WayzResponse response = new WayzResponse();
        noticeService.deleteNotice(noticeId);
        return response;
    }
}
