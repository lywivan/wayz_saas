package com.wayz.controller;

import com.wayz.exception.WayzException;
import com.wayz.response.WayzResponse;
import com.wayz.service.HomeService;
import com.wayz.validator.ParameterValidator;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * 官网控制器类
 * 
 * @author mike.ma@wayz.ai
 *
 */
@Controller
@RequestMapping("/home")
@SessionAttributes("myId")
public class HomeController {

	/** 官网代理 */
	@Reference(version = "1.0.0", check = false)
	private HomeService homeService = null;

	/**
	 * 查询城市
	 * 
	 * @param myId 我的标识
	 * @param provinceId 省份标识
	 * @param id 城市标识
	 * @return platform应答
	 * @throws
	 */
	@ResponseBody
	@RequestMapping(value = "/queryCity", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse queryCity(@ModelAttribute("myId") Long myId,
								  @RequestParam(value = "provinceId", required = false) Long provinceId,
								  @RequestParam(value = "id", required = false) Long id) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数

		// 调用接口
		$response.setData(homeService.queryCity(myId, provinceId, id));

		// 返回应答
		return $response;
	}

	/**
	 * 创建新闻
	 * 
	 * @param myId 我的标识
	 * @param categoryId 分类报道
	 * @param title 标题
	 * @param titlePic 标题图片
	 * @param description 描述
	 * @param keyWords 关键词
	 * @param abstractContent 摘要
	 * @param content 新闻详情
	 * @param pushTime 发布时间
	 * @param isPush 是否发布(0=否,1=是)
	 * @param sort 排序
	 * @return platform应答
	 * @throws WayzException
	 */
	@ResponseBody
	@RequestMapping(value = "/createArticle", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse createArticle(@ModelAttribute("myId") Long myId,
			@RequestParam(value = "categoryId", required = false) Short categoryId,
			@RequestParam(value = "title", required = false) String title,
			@RequestParam(value = "titlePic", required = false) String titlePic,
			@RequestParam(value = "description", required = false) String description,
			@RequestParam(value = "keyWords", required = false) String keyWords,
			@RequestParam(value = "abstractContent", required = false) String abstractContent,
			@RequestParam(value = "content", required = false) String content,
			@RequestParam(value = "pushTime", required = false) String pushTime,
			@RequestParam(value = "isPush", required = false) Boolean isPush,
			@RequestParam(value = "sort", required = false) Integer sort) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数: 分类报道
		ParameterValidator.notNull(categoryId, "分类报道");
		// 验证参数: 标题
		ParameterValidator.notNull(title, "标题");
		// 验证参数: 标题图片
		ParameterValidator.notNull(titlePic, "标题图片");
		// 验证参数: 描述
		ParameterValidator.notNull(description, "描述");
		// 验证参数: 摘要
		ParameterValidator.notNull(abstractContent, "摘要 ");
		// 验证参数: 新闻详情
		ParameterValidator.notNull(content, "新闻详情");
		// 验证参数: 发布时间
		ParameterValidator.notNull(pushTime, "发布时间");
		// 验证参数: 是否发布(0=否,1=是)
		ParameterValidator.notNull(isPush, "是否发布(0=否,1=是)");
		// 验证参数: 排序
		ParameterValidator.notNull(sort, "排序");

		// 调用接口
		homeService.createArticle(myId, categoryId, title, titlePic, description, keyWords, abstractContent, content,
				pushTime, isPush, sort);

		// 返回应答
		return $response;
	}

	/**
	 * 修改新闻
	 * 
	 * @param myId 我的标识
	 * @param id 标识
	 * @param categoryId 分类报道
	 * @param title 标题
	 * @param titlePic 标题图片
	 * @param description 描述
	 * @param keyWords 关键词
	 * @param abstractContent 摘要
	 * @param content 新闻详情
	 * @param pushTime 发布时间
	 * @param isPush 是否发布(0=否,1=是)
	 * @param sort 排序
	 * @return platform应答
	 * @throws WayzException
	 */
	@ResponseBody
	@RequestMapping(value = "/modifyArticle", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse modifyArticle(@ModelAttribute("myId") Long myId,
			@RequestParam(value = "id", required = false) Long id,
			@RequestParam(value = "categoryId", required = false) Short categoryId,
			@RequestParam(value = "title", required = false) String title,
			@RequestParam(value = "titlePic", required = false) String titlePic,
			@RequestParam(value = "description", required = false) String description,
			@RequestParam(value = "keyWords", required = false) String keyWords,
			@RequestParam(value = "abstractContent", required = false) String abstractContent,
			@RequestParam(value = "content", required = false) String content,
			@RequestParam(value = "pushTime", required = false) String pushTime,
			@RequestParam(value = "isPush", required = false) Boolean isPush,
			@RequestParam(value = "sort", required = false) Integer sort) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数: 标识
		ParameterValidator.notNull(id, "标识");
		// 验证参数: 分类报道
		ParameterValidator.notNull(categoryId, "分类报道");
		// 验证参数: 标题
		ParameterValidator.notNull(title, "标题");
		// 验证参数: 标题图片
		ParameterValidator.notNull(titlePic, "标题图片");
		// 验证参数: 描述
		ParameterValidator.notNull(description, "描述");
		// 验证参数: 摘要
		ParameterValidator.notNull(abstractContent, "摘要");
		// 验证参数: 新闻详情
		ParameterValidator.notNull(content, "新闻详情");
		// 验证参数: 发布时间
		ParameterValidator.notNull(pushTime, "发布时间");
		// 验证参数: 是否发布(0=否,1=是)
		ParameterValidator.notNull(isPush, "是否发布(0=否,1=是)");
		// 验证参数: 排序
		ParameterValidator.notNull(sort, "排序");

		// 调用接口
		homeService.modifyArticle(myId, id, categoryId, title, titlePic, description, keyWords, abstractContent, content,
				pushTime, isPush, sort);

		// 返回应答
		return $response;
	}

	/**
	 * 删除新闻
	 * 
	 * @param myId 我的标识
	 * @param id 标识
	 * @return platform应答
	 * @throws WayzException
	 */
	@ResponseBody
	@RequestMapping(value = "/delArticle", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse delArticle(@ModelAttribute("myId") Long myId,
			@RequestParam(value = "id", required = false) Long id) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数: 标识
		ParameterValidator.notNull(id, "标识");

		// 调用接口
		homeService.delArticle(myId, id);

		// 返回应答
		return $response;
	}

	/**
	 * 修改发布
	 * 
	 * @param myId 我的标识
	 * @param id 标识
	 * @return platform应答
	 * @throws WayzException
	 */
	@ResponseBody
	@RequestMapping(value = "/modifyArticlePush", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse modifyArticlePush(@ModelAttribute("myId") Long myId,
			@RequestParam(value = "id", required = false) Long id) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数: 标识
		ParameterValidator.notNull(id, "标识");

		// 调用接口
		homeService.modifyArticlePush(myId, id);

		// 返回应答
		return $response;
	}

	/**
	 * 查询发布
	 * 
	 * @param myId 我的标识
	 * @param categoryId 分类报道
	 * @param title 标题
	 * @param isPush 是否发布(0=否,1=是)
	 * @param startIndex 开始序号
	 * @param pageSize 页面大小
	 * @return platform应答
	 * @throws WayzException
	 */
	@ResponseBody
	@RequestMapping(value = "/queryArticle", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse queryArticle(@ModelAttribute("myId") Long myId,
			@RequestParam(value = "categoryId", required = false) Short categoryId,
			@RequestParam(value = "title", required = false) String title,
			@RequestParam(value = "isPush", required = false) Boolean isPush,
			@RequestParam(value = "startIndex", required = false) Integer startIndex,
			@RequestParam(value = "pageSize", required = false) Integer pageSize) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数

		// 调用接口
		$response.setData(homeService.queryArticle(myId, categoryId, title, isPush, startIndex, pageSize));

		// 返回应答
		return $response;
	}

	/**
	 * 新闻详情
	 * 
	 * @param myId 我的标识
	 * @param id 标识
	 * @return platform应答
	 * @throws WayzException
	 */
	@ResponseBody
	@RequestMapping(value = "/getArticleDetail", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse getArticleDetail(@ModelAttribute("myId") Long myId,
			@RequestParam(value = "id", required = false) Long id) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数: 标识
		ParameterValidator.notNull(id, "标识");

		// 调用接口
		$response.setData(homeService.getArticle(myId, id));

		// 返回应答
		return $response;
	}

	/**
	 * 修改合作案例
	 * 
	 * @param myId 我的标识
	 * @param id 案例标识
	 * @param title 标题
	 * @param subTitle 副标题
	 * @param titlePic 标题图片url
	 * @param imgUrl 列表图片url
	 * @param seoTitle seo标题
	 * @param keyWords 关键字
	 * @param description 描述
	 * @param content 新闻详情
	 * @param pushTime 发布时间
	 * @param isPush 是否发布(0=否,1=是)
	 * @param sort 排序
	 * @return platform应答
	 * @throws WayzException
	 */
	@ResponseBody
	@RequestMapping(value = "/modifyCase", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse modifyCase(@ModelAttribute("myId") Long myId,
			@RequestParam(value = "id", required = false) Long id,
			@RequestParam(value = "title", required = false) String title,
			@RequestParam(value = "subTitle", required = false) String subTitle,
			@RequestParam(value = "titlePic", required = false) String titlePic,
			@RequestParam(value = "imgUrl", required = false) String imgUrl,
			@RequestParam(value = "seoTitle", required = false) String seoTitle,
			@RequestParam(value = "keyWords", required = false) String keyWords,
			@RequestParam(value = "description", required = false) String description,
			@RequestParam(value = "content", required = false) String content,
			@RequestParam(value = "pushTime", required = false) String pushTime,
			@RequestParam(value = "isPush", required = false) Boolean isPush,
			@RequestParam(value = "sort", required = false) Integer sort) throws WayzException {

		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数: 案例标识
		ParameterValidator.notNull(id, "案例标识");
		// 验证参数: 标题
		ParameterValidator.notNull(title, "标题");
		// 验证参数: 副标题
		ParameterValidator.notNull(subTitle, "副标题 ");
		// 验证参数: 标题图片
		ParameterValidator.notNull(titlePic, "标题图片");
		// 验证参数: 列表图片
		ParameterValidator.notNull(imgUrl, "列表图片");
		// 验证参数: 新闻详情
		ParameterValidator.notNull(content, "新闻详情");
		// 验证参数: 发布时间
		ParameterValidator.notNull(pushTime, "发布时间");
		// 验证参数: 是否发布(0=否,1=是)
		ParameterValidator.notNull(isPush, "是否发布(0=否,1=是)");
		// 验证参数: 排序
		ParameterValidator.notNull(sort, "排序");

		// 调用接口
		homeService.modifyCase(myId, id, title, subTitle, titlePic, imgUrl, seoTitle, keyWords, description, content,
				pushTime, isPush, sort);

		// 返回应答
		return $response;
	}

	/**
	 * 创建合作案例
	 * 
	 * @param myId 我的标识
	 * @param title 标题
	 * @param subTitle 副标题
	 * @param titlePic 标题图片url
	 * @param imgUrl 列表图片url
	 * @param seoTitle seo标题
	 * @param keyWords 关键字
	 * @param description 描述
	 * @param content 新闻详情
	 * @param pushTime 发布时间
	 * @param isPush 是否发布(0=否,1=是)
	 * @param sort 排序
	 * @return platform应答
	 * @throws WayzException
	 */
	@ResponseBody
	@RequestMapping(value = "/createCase", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse createCase(@ModelAttribute("myId") Long myId,
			@RequestParam(value = "title", required = false) String title,
			@RequestParam(value = "subTitle", required = false) String subTitle,
			@RequestParam(value = "titlePic", required = false) String titlePic,
			@RequestParam(value = "imgUrl", required = false) String imgUrl,
			@RequestParam(value = "seoTitle", required = false) String seoTitle,
			@RequestParam(value = "keyWords", required = false) String keyWords,
			@RequestParam(value = "description", required = false) String description,
			@RequestParam(value = "content", required = false) String content,
			@RequestParam(value = "pushTime", required = false) String pushTime,
			@RequestParam(value = "isPush", required = false) Boolean isPush,
			@RequestParam(value = "sort", required = false) Integer sort) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数: 标题
		ParameterValidator.notNull(title, "标题");
		// 验证参数: 副标题
		ParameterValidator.notNull(subTitle, "副标题 ");
		// 验证参数: 标题图片
		ParameterValidator.notNull(titlePic, "标题图片");
		// 验证参数: 列表图片
		ParameterValidator.notNull(imgUrl, "列表图片");
		// 验证参数: 新闻详情
		ParameterValidator.notNull(content, "新闻详情");
		// 验证参数: 发布时间
		ParameterValidator.notNull(pushTime, "发布时间");
		// 验证参数: 是否发布(0=否,1=是)
		ParameterValidator.notNull(isPush, "是否发布(0=否,1=是)");
		// 验证参数: 排序
		ParameterValidator.notNull(sort, "排序");

		// 调用接口
		homeService.createCase(myId, title, subTitle, titlePic, imgUrl, seoTitle, keyWords, description, content,
				pushTime, isPush, sort);

		// 返回应答
		return $response;
	}

	/**
	 * 删除合作案例
	 * 
	 * @param myId 我的标识
	 * @param id 案例标识
	 * @return platform应答
	 * @throws WayzException
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteCase", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse deleteCase(@ModelAttribute("myId") Long myId,
			@RequestParam(value = "id", required = false) Long id) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数

		// 调用接口
		homeService.deleteCase(myId, id);

		// 返回应答
		return $response;
	}

	/**
	 * 发布
	 * 
	 * @param myId 我的标识
	 * @param id 案例标识
	 * @return platform应答
	 * @throws WayzException
	 */
	@ResponseBody
	@RequestMapping(value = "/modifyCasePush", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse modifyCasePush(@ModelAttribute("myId") Long myId,
			@RequestParam(value = "id", required = false) Long id) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数

		// 调用接口
		homeService.modifyCasePush(myId, id);

		// 返回应答
		return $response;
	}

	/**
	 * 查询合作案例
	 * 
	 * @param title 案例标题(模糊查询)
	 * @param isPush 是否发布(0=否,1=是)
	 * @return platform应答
	 * @throws WayzException
	 */
	@ResponseBody
	@RequestMapping(value = "/queryCase", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse queryCase(@RequestParam(value = "title", required = false) String title,
			@RequestParam(value = "isPush", required = false) Boolean isPush,
			@RequestParam(value = "startIndex", required = false) Integer startIndex,
			@RequestParam(value = "pageSize", required = false) Integer pageSize) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数

		// 调用接口
		$response.setData(homeService.queryCase(title, isPush, startIndex, pageSize));

		// 返回应答
		return $response;
	}
	
	/**
	 * 获取合作案例详情
	 * 
	 * @param myId 我的标识
	 * @param id 合作案例标识
	 * @return platform应答
	 * @throws WayzException
	 */
	@ResponseBody
	@RequestMapping(value = "/getCaseDetail", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse getCaseDetail(@ModelAttribute("myId") Long myId,
			@RequestParam(value = "id", required = false) Long id) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		// 验证参数: 标识
		ParameterValidator.notNull(id, "标识");

		// 调用接口
		$response.setData(homeService.getCase(id));

		// 返回应答
		return $response;
	}
	
	/**
	 * 查询表单数据
	 * 
	 * @param myId
	 * @param type 类型
	 * @param companyName 公司名称
	 * @param phone 手机号
	 * @param startIndex 开始序号
	 * @param pageSize 页面大小
	 * @return platform应答
	 * @throws WayzException
	 */
	@ResponseBody
	@RequestMapping(value = "/queryFormData", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse queryFormData(@ModelAttribute("myId") Long myId,
			@RequestParam(value = "type", required = false) Short type,
			@RequestParam(value = "provinceId", required = false) Long provinceId,
			@RequestParam(value = "cityId", required = false) Long cityId,
			@RequestParam(value = "companyName", required = false) String companyName,
			@RequestParam(value = "phone", required = false) String phone,
			@RequestParam(value = "startIndex", required = false) Integer startIndex,
			@RequestParam(value = "pageSize", required = false) Integer pageSize) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数

		// 调用接口
		$response.setData(homeService.queryFormData(myId, type, provinceId, cityId, companyName, phone, startIndex,
				pageSize));

		// 返回应答
		return $response;
	}
}
