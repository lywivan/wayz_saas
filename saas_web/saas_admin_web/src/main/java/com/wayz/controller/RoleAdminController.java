package com.wayz.controller;

import com.wayz.entity.dto.role.RoleDto;
import com.wayz.entity.dto.role.RoleMenuIdsDto;
import com.wayz.entity.dto.role.RolePageDto;
import com.wayz.exception.WayzException;
import com.wayz.response.WayzResponse;
import com.wayz.service.RoleAdminService;
import com.wayz.validator.ParameterValidator;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @author wade.liu@wayz.ai
 * @className: RoleController
 * @description: 角色控制器
 * <功能详细描述>
 */
@Controller
@RequestMapping("/role")
@SessionAttributes("myId")
public class RoleAdminController {

    /**
     * 角色服务
     */
    @Reference(version = "1.0.0", check = false)
    private RoleAdminService roleAdminService = null;

    /**
     * 创建角色
     *
     * @param myId 我的标识
     * @throws WayzException 云歌广告平台异常
     */
    @ResponseBody
    @RequestMapping(value = "/createRole", method = {RequestMethod.GET, RequestMethod.POST})
    public WayzResponse createRole(@ModelAttribute("myId") Long myId, RoleDto roleDto) throws WayzException {
        // 初始化
        WayzResponse response = new WayzResponse();
        // 验证参数
        // 验证参数: 角色名称
        ParameterValidator.notNull(roleDto.getName(), "角色名称");
        ParameterValidator.stringLengthLimit(roleDto.getName(), "角色名称", 50);
        // 验证参数: 角色状态(0:禁用;1:启用)
        // 调用接口
        response.setData(roleAdminService.createRole(myId, roleDto));
        // 返回应答
        return response;
    }

    /**
     * 修改角色
     *
     * @param myId 我的标识
     * @return yunge应答
     * @throws WayzException 云歌广告平台异常
     */
    @ResponseBody
    @RequestMapping(value = "/modifyRole", method = {RequestMethod.GET, RequestMethod.POST})
    public WayzResponse modifyRole(@ModelAttribute("myId") Long myId, RoleDto roleDto) throws WayzException {
        // 初始化
        WayzResponse response = new WayzResponse();
        // 验证参数
        // 验证参数: 角色标识
        ParameterValidator.notNull(roleDto.getId(), "角色标识");
        // 验证参数: 角色名称
        ParameterValidator.notNull(roleDto.getName(), "角色名称");
        ParameterValidator.stringLengthLimit(roleDto.getName(), "角色名称", 50);
        // 调用接口
        roleAdminService.modifyRole(myId, roleDto);
        // 返回应答
        return response;
    }

    /**
     * 删除角色
     *
     * @param myId 我的标识
     * @param id   角色标识
     * @return yunge应答
     * @throws WayzException 云歌广告平台异常
     */
    @ResponseBody
    @RequestMapping(value = "/deleteRole", method = {RequestMethod.GET, RequestMethod.POST})
    public WayzResponse deleteRole(@ModelAttribute("myId") Long myId,
                                   @RequestParam(value = "id") Long id) throws WayzException {
        // 初始化
        WayzResponse response = new WayzResponse();
        // 验证参数
        // 验证参数: 角色标识
        ParameterValidator.notNull(id, "角色标识");
        // 调用接口
        roleAdminService.deleteRole(myId, id);
        // 返回应答
        return response;
    }

    /**
     * 查询角色视图
     *
     * @param myId 我的标识
     * @return yunge应答
     * @throws WayzException 云歌广告平台异常
     */
    @ResponseBody
    @RequestMapping(value = "/queryRoleView", method = {RequestMethod.GET, RequestMethod.POST})
    public WayzResponse queryRoleView(@ModelAttribute("myId") Long myId) throws WayzException {
        // 初始化
        WayzResponse response = new WayzResponse();
        // 调用接口
        response.setData(roleAdminService.queryRoleView(myId));

        // 返回应答
        return response;
    }

    /**
     * 角色授权接口
     *
     * @param myId 我的标识
     * @param dto  角色权限标识列表
     * @return yunge应答
     * @throws WayzException 云歌广告平台异常
     */
    @ResponseBody
    @RequestMapping(value = "/authorityRole", method = {RequestMethod.GET, RequestMethod.POST})
    public WayzResponse authorityRole(@ModelAttribute("myId") Long myId, @RequestBody RoleMenuIdsDto roleMenuIdsDto) throws WayzException {
        // 初始化
        WayzResponse response = new WayzResponse();

        // 验证参数
        // 验证参数: 角色标识
        ParameterValidator.notNull(roleMenuIdsDto.getRoleId(), "角色标识");
        // 验证参数: 权限标识列表
        ParameterValidator.notNull(roleMenuIdsDto.getMenuIdList(), "权限标识列表");

        // 调用接口
        roleAdminService.authorityRole(myId, roleMenuIdsDto);

        // 返回应答
        return response;
    }

    /**
     * 查询角色
     *
     * @param myId 我的标识
     * @return yunge应答
     * @throws WayzException 云歌广告平台异常
     */
    @ResponseBody
    @RequestMapping(value = "/queryRole", method = {RequestMethod.GET, RequestMethod.POST})
    public WayzResponse queryRole(@ModelAttribute("myId") Long myId, RolePageDto rolePageDto) throws WayzException {
        // 初始化
        WayzResponse response = new WayzResponse();
        response.setData(roleAdminService.queryRole(myId, rolePageDto));
        return response;
    }
}
