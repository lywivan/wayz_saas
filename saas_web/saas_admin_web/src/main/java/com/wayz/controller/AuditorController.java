package com.wayz.controller;

import com.wayz.annotation.RequestJsonParam;
import com.wayz.entity.dto.AuditorDto;
import com.wayz.entity.dto.AuditorPositionQueryDto;
import com.wayz.entity.dto.AuditorQueryDto;
import com.wayz.exception.WayzException;
import com.wayz.response.WayzResponse;
import com.wayz.service.AuditorPositionService;
import com.wayz.service.AuditorService;
import com.wayz.validator.ParameterValidator;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 审核方管理类
 *
 * @author mike.ma@wayz.ai
 */
@Controller
@RequestMapping("/auditor")
@SessionAttributes("myId")
public class AuditorController {

	/** 审核方服务 */
	@Reference(version = "1.0.0", check = false)
	private AuditorService auditorService;
	/** 审核广告位服务 */
	@Reference(version = "1.0.0", check = false)
	private AuditorPositionService auditorPositionService;
	/**
	 * 审核方列表
	 * 
	 * @param myId 我的ID
	 * @param dto 审核方名称
	 * @return
	 * @throws WayzException
	 */
	@ResponseBody
	@RequestMapping(value = "/queryAuditor", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse queryAuditor(@ModelAttribute("myId") Long myId, AuditorQueryDto dto) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		response.setData(auditorService.queryAuditor(myId, dto));
		return response;
	}

	/**
	 * 获取审核方详情
	 *
	 * @param myId 我的标识
	 * @param id 审核方标识
	 * @return 云歌应答
	 * @throws WayzException 云歌广告平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/getAuditorDetail", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse getAuditorDetail(@ModelAttribute("myId") Long myId, @RequestParam("id") Long id)
			throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		// 验证参数: 审核方标识
		ParameterValidator.notNull(id, "审核方标识");
		// 调用接口
		response.setData(auditorService.getAuditor(myId, id));
		// 返回应答
		return response;
	}

	/**
	 *
	 * 创建审核方
	 * 
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/createAuditor", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse createAuditor(@ModelAttribute("myId") Long myId, AuditorDto dto) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		// 验证参数: 审核方标识
		ParameterValidator.notNull(dto.getType(), "审核方类型");
		ParameterValidator.notNull(dto.getName(), "审核方名称");
		ParameterValidator.notNull(dto.getContacts(), "审核方联系人");
		ParameterValidator.notNull(dto.getTelephone(), "审核方手机号");
		// 调用接口
		auditorService.createAuditor(myId, dto);
		// 返回应答
		return response;
	}

	/**
	 * 修改审核方
	 * 
	 * @param myId 我的ID
	 * @param dto 实体类
	 * @return
	 * @throws WayzException
	 */
	@ResponseBody
	@RequestMapping(value = "/modifyAuditor", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse modifyAuditor(@ModelAttribute("myId") Long myId, AuditorDto dto) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		// 验证参数: 审核方标识
		ParameterValidator.notNull(dto.getId(), "审核方标识");
		ParameterValidator.notNull(dto.getName(), "审核方名称");
		ParameterValidator.notNull(dto.getContacts(), "审核方联系人");
		// ParameterValidator.notNull(auditorVO.getTelephone(), "审核方手机号");
		// 调用接口
		auditorService.modifyAuditor(myId, dto);
		// 返回应答
		return response;
	}

	/**
	 * 修改审核方状态
	 * 
	 * @param myId 我的ID
	 * @param id 审核方ID
	 * @param status 审核方状态(1:启用 0:禁用;)
	 * @return
	 * @throws WayzException
	 */
	@ResponseBody
	@RequestMapping(value = "/modifyAuditorStatus", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse modifyAuditorStatus(@ModelAttribute("myId") Long myId, @RequestParam("id") Long id,
			@RequestParam("status") Short status) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		// 验证参数: 审核方标识
		ParameterValidator.notNull(id, "审核方标识");
		ParameterValidator.notNull(status, "审核方状态");
		// 调用接口
		response.setData(auditorService.modifyAuditorStatus(myId, id, status));
		// 返回应答
		return response;
	}

	/**
	 * 删除审核方
	 * 
	 * @param myId 我的标识
	 * @param id 公司标识
	 * @return 云歌应答
	 * @throws WayzException 云歌广告平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteAuditor", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse deleteAuditor(@ModelAttribute("myId") Long myId,
			@RequestParam(value = "id", required = false) Long id) throws WayzException {
		WayzResponse response = new WayzResponse();
		ParameterValidator.notNull(id, "审核方标识");
		auditorService.deleteAuditor(myId, id);
		return response;
	}

	/**
	 * 审核方重置密码
	 * 
	 * @param myId 我的ID
	 * @param username 审核方名称
	 * @param newPassword 新的密码
	 * @return
	 * @throws WayzException
	 */
	@ResponseBody
	@RequestMapping(value = "/resetAuditorPassword", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse resetAuditorPassword(@ModelAttribute("myId") Long myId,
			@RequestParam(value = "username", required = false) String username,
			@RequestParam(value = "newPassword", required = false) String newPassword) throws WayzException {
		WayzResponse response = new WayzResponse();
		ParameterValidator.notNull(username, "审核方用户名");
		auditorService.resetAuditorPassword(myId, username, newPassword);
		return response;
	}

	/**
	 * 查询场景方可选择审核设备
	 * 
	 * @param myId 我的标识
	 * @param dto
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/querySelectablePosition", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse querySelectablePosition(@ModelAttribute("myId") Long myId, AuditorPositionQueryDto dto)
			throws WayzException {
		WayzResponse $response = new WayzResponse();
		ParameterValidator.notNull(dto.getAuditorId(), "审核方标识");
		$response.setData(auditorPositionService.querySelectablePosition(myId, dto));
		return $response;
	}

	/**
	 * 查询场景方已选择审核设备
	 * 
	 * @param myId 我的标识
	 * @param dto
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/querySelectedPosition", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse querySelectedPosition(@ModelAttribute("myId") Long myId, AuditorPositionQueryDto dto)
			throws WayzException {
		WayzResponse $response = new WayzResponse();
		ParameterValidator.notNull(dto.getAuditorId(), "审核方标识");
		$response.setData(auditorPositionService.querySelectedPosition(myId, dto));
		return $response;
	}

	/**
	 * 添加场景方审核设备
	 * 
	 * @param myId 我的标识
	 * @param auditorId 审核方标识
	 * @param positionIdList 设备标识列表
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/addPosition", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse addPosition(@ModelAttribute("myId") Long myId,
			@RequestParam(value = "auditorId", required = false) Long auditorId,
			@RequestJsonParam(value = "positionIdList") List<Long> positionIdList) throws WayzException {
		WayzResponse $response = new WayzResponse();
		ParameterValidator.notNull(auditorId, "审核方标识");
		ParameterValidator.notNull(positionIdList, "设备标识列表");
		auditorPositionService.addPosition(myId, auditorId, positionIdList);
		return $response;
	}

	/**
	 * 删除场景方审核设备
	 * 
	 * @param myId 我的标识
	 * @param auditorId 审核方标识
	 * @param positionIdList 设备标识列表
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/deletePosition", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse deletePosition(@ModelAttribute("myId") Long myId,
			@RequestParam(value = "auditorId", required = false) Long auditorId,
			@RequestJsonParam(value = "positionIdList") List<Long> positionIdList) throws WayzException {
		WayzResponse $response = new WayzResponse();
		ParameterValidator.notNull(auditorId, "审核方标识");
		ParameterValidator.notNull(positionIdList, "设备标识列表");
		auditorPositionService.deletePosition(myId, auditorId, positionIdList);
		return $response;
	}

}
