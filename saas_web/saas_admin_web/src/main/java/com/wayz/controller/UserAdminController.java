package com.wayz.controller;

import com.wayz.entity.dto.user.PageUserAdminDto;
import com.wayz.entity.dto.user.UserAdminDto;
import com.wayz.exception.WayzException;
import com.wayz.response.WayzResponse;
import com.wayz.service.UserAdminService;
import com.wayz.validator.ParameterValidator;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @className: UserController
 * @description: 用户
 */
@Controller
@RequestMapping("/user")
@SessionAttributes("myId")
public class UserAdminController {

    @Reference(version = "1.0.0", check=false)
    private UserAdminService userAdminService;
    /**
     * 创建用户
     *
     * @param myId 我的标识
     * @return
     * @throws WayzException
     */
    @ResponseBody
    @RequestMapping(value = "/createUser", method = { RequestMethod.GET, RequestMethod.POST })
    public WayzResponse createUser(@ModelAttribute("myId") Long myId, UserAdminDto userAdminDto) throws WayzException {
        // 初始化
        WayzResponse response = new WayzResponse();

        // 验证参数
        // 验证参数: 角色标识
        ParameterValidator.notNull(userAdminDto.getRoleId(), "角色标识");
        // 验证参数: 用户名称
        ParameterValidator.notNull(userAdminDto.getUsername(), "用户名");
        ParameterValidator.stringLengthLimit(userAdminDto.getUsername(), "用户姓名", 50);
        // 验证参数: 用户密码
        // 验证参数: 用户姓名
        ParameterValidator.stringLengthLimit(userAdminDto.getName(), "用户姓名", 20);
        // 验证参数: 手机号
        ParameterValidator.notNull(userAdminDto.getPhone(), "手机号");
        ParameterValidator.phoneFormat(userAdminDto.getPhone(), "手机号");
        // 验证参数: 用户邮箱
        ParameterValidator.stringLengthLimit(userAdminDto.getEmail(), "联系邮箱", 50);

        // 调用接口
        response.setData(userAdminService.createUser(myId,userAdminDto));

        // 返回应答
        return response;
    }

    /**
     * 查询用户
     *
     * @param myId 我的标识
     * @return
     * @throws WayzException
     */
    @ResponseBody
    @RequestMapping(value = "/queryUser", method = { RequestMethod.GET, RequestMethod.POST })
    public WayzResponse queryUser(@ModelAttribute("myId") Long myId, PageUserAdminDto pageUserAdminDto) {

        WayzResponse response = new WayzResponse();
        response.setData(userAdminService.queryUser(myId, pageUserAdminDto));
        return response;
    }

    /**
     * 修改用户
     *
     * @param myId 我的标识
     * @throws WayzException 云歌广告平台异常
     */
    @ResponseBody
    @RequestMapping(value = "/modifyUser", method = { RequestMethod.GET, RequestMethod.POST })
    public WayzResponse modifyUser(@ModelAttribute("myId") Long myId, UserAdminDto userAdminDto) throws WayzException {
        // 初始化
        WayzResponse response = new WayzResponse();

        // 验证参数
        // 验证参数: 用户标识
        ParameterValidator.notNull(userAdminDto.getId(), "用户标识");
        // 验证参数: 用户姓名
        ParameterValidator.stringLengthLimit(userAdminDto.getName(), "用户姓名", 20);
        // 验证参数: 用户邮箱
        ParameterValidator.stringLengthLimit(userAdminDto.getEmail(), "联系邮箱", 50);
        // 调用接口
        userAdminService.modifyUser(myId, userAdminDto);

        // 返回应答
        return response;
    }

    /**
     * 修改用户的 启用/禁用 状态
     *
     * @param myId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/updateUserStatus", method = { RequestMethod.GET, RequestMethod.POST })
    public WayzResponse updateUserStatus(@ModelAttribute("myId") Long myId, UserAdminDto userAdminDto) throws WayzException {
        // 初始化
        WayzResponse response = new WayzResponse();

        // 验证参数

        // 调用接口
        userAdminService.updateUserStatus(myId, userAdminDto);

        // 返回应答
        return response;
    }

    /**
     * 修改密码
     *
     * @param myId 我的标识
     * @throws WayzException 云歌广告平台异常
     */
    @ResponseBody
    @RequestMapping(value = "/modifyPassword", method = RequestMethod.POST)
    public WayzResponse modifyPassword(@ModelAttribute("myId") Long myId, UserAdminDto userAdminDto) throws WayzException {
        // 初始化
        WayzResponse response = new WayzResponse();

        // 验证参数
        // 验证参数: 原始密码
        ParameterValidator.notNull(userAdminDto.getOldPassword(), "原始密码");
        ParameterValidator.stringLengthLimit(userAdminDto.getOldPassword(), "原始密码", 32);
        // 验证参数: 新的密码
        ParameterValidator.notNull(userAdminDto.getNewPassword(), "新的密码");
        ParameterValidator.stringLengthLimit(userAdminDto.getNewPassword(), "新的密码", 32);

        // 调用接口
        userAdminService.modifyPassword(myId, userAdminDto);

        // 返回应答
        return response;
    }

    /**
     * 删除用户
     *
     * @param myId 我的标识
     * @param id 用户标识
     * @throws WayzException 云歌广告平台异常
     */
    @ResponseBody
    @RequestMapping(value = "/deleteUser", method = { RequestMethod.GET, RequestMethod.POST })
    public WayzResponse deleteUser(@ModelAttribute("myId") Long myId, @RequestParam("id") Long id) throws WayzException {
        // 初始化
        WayzResponse response = new WayzResponse();

        // 验证参数
        // 验证参数: 用户标识
        ParameterValidator.notNull(id, "用户标识");

        // 调用接口
        userAdminService.deleteUser(myId, id);

        // 返回应答
        return response;
    }
}
