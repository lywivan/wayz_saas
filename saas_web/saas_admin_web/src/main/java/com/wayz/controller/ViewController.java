package com.wayz.controller;

import com.wayz.exception.WayzException;
import com.wayz.response.WayzResponse;
import com.wayz.service.*;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * 视图控制器类
 *
 * @author yinjy
 */
@Controller
@RequestMapping("")
@SessionAttributes("myId")
public class ViewController {
    /** 代理相关 */
    /**
     * 用户服务
     */
    @Reference(version = "1.0.0", check = false)
    private UserAdminService userAdminService;

    /**
     * 菜单服务
     */
    @Reference(version = "1.0.0", check = false)
    private MenuAdminService menuAdminService;

    /**
     * 角色服务
     */
    @Reference(version = "1.0.0", check = false)
    private RoleAdminService roleAdminService;

    /**
     * 公司服务
     */
    @Reference(version = "1.0.0", check = false)
    private CompanySaasService companyService;

    @Reference(version = "1.0.0", check = false)
    private VersionService versionService;

    /**
     * 代理商代理服务
     */
    @Reference(version = "1.0.0", check = false)
    private AgentService agentService;

    /**
     * 平台管理代理服务
     */
    @Reference(version = "1.0.0", check = false)
    private TradeService tradeService;
    /**
     * 点位审核服务
     */
    @Reference(version = "1.0.0", check = false)
    private AdvPositionCibnService advPositionCibnService;
    /**
     * 渠道服务
     */
    @Reference(version = "1.0.0", check = false)
    private ChannelService channelService;
    /**
     * 行业服务
     */
    @Reference(version = "1.0.0", check = false)
    private CategoryService categoryService;
    /**
     * 内容分类服务
     */
    @Reference(version = "1.0.0", check = false)
    private ContentCategoryService contentCategoryService;
    /**
     * 内容服务
     */
    @Reference(version = "1.0.0", check = false)
    private ContentService contentService;
    /**
     * 官网首页服务
     */
    @Reference(version = "1.0.0", check = false)
    private HomeService homeService;
    /**
     * CIBN服务
     */
	@Reference(version = "1.0.0", check = false)
    private CompanyCibnService companyCibnService;

    /** 视图相关 */
    /**
     * 登录视图
     */
    private static final String LOGIN = "login";
    /**
     * 错误视图
     */
    private static final String ERROR = "error";
    /**
     * 索引视图
     */
    private static final String INDEX = "index";
    /**
     * 跳转视图
     */
    private static final String REDIRECT = "redirect";
    /**
     * 忘记密码视图
     */
    private static final String FORGET_PWD = "forget_pwd";
    /**
     * 用户管理视图
     */
    private static final String USER_MANAGEMENT = "system/user_management";
    /**
     * 角色管理视图
     */
    private static final String ROLE_MANAGEMENT = "system/role_management";
    /**
     * 优惠券管理视图
     */
    private static final String COUPON_MANAGEMENT = "config/coupon_management";
    /**
     * 播控系统版本视图
     */
    private static final String PLAY_VERSION = "config/play_version";
    /**
     * 公司管理视图
     */
    private static final String COMPANY_MANAGEMENT = "system/company_management";
    /**
     * 代理商管理视图
     */
    private static final String AGENT_MANAGEMENT = "system/agent_management";
    /**
     * 审核方管理视图
     */
    private static final String AUDITOR_MANAGEMENT = "system/auditor_management";
    /**
     * 版本管理视图
     */
    private static final String VERSION_MANAGEMENT = "config/version_management";
    /**
     * 设备管理视图
     */
    private static final String POSITION_MANAGEMENT = "position/position_management";
    /**
     * 系统通知管理视图
     */
    private static final String SYSTEM_NOTICE_MANAGEMENT = "system/system_notice_management";
    /**
     * 设备续期价格管理视图
     */
    private static final String DEVICE_PRICE_MANAGEMENT = "system/device_price_management";
    /**
     * 空间扩展价格管理视图
     */
    private static final String SPACE_PRICE_MANAGEMENT = "system/space_price_management";
    /**
     * 直播渠道管理视图
     */
    private static final String LIVE_CHANNEL_MANAGEMENT = "system/live_channel_management";
    /** 直播模板制作视图 */
    private static final String LIVE_TEMPLATE_INDEX = "live_template_index";
    /**
     * 直播列表视图
     */
    private static final String LIST_PLAN_LIST = "system/live_plan_list";
    /**
     * 内容管理视图
     */
    private static final String CONTENT_MANAGEMENT = "content/content_management";
    /**
     * 内容审核管理视图
     */
    private static final String CONTENT_AUDIT_MANAGEMENT = "content/content_audit_management";
    /**
     * 内容分类管理视图
     */
    private static final String CONTENT_CATEGORY_MANAGEMENT = "content/content_category_management";
    /**
     * 海报行业类别管理视图
     */
    private static final String POSTER_CATEGORY_MANAGEMENT = "content/poster_category_management";
    /**
     * 海报分辨率管理视图
     */
    private static final String POSTER_RESOLUTION_MANAGEMENT = "content/poster_resolution_management";
    /**
     * 海报模板制作视图
     */
    private static final String POSTER_TEMPLATE_INDEX = "poster_template_index";
    /**
     * 公司CIBN审核视图
     */
    private static final String COMPANY_CIBN_AUDIT = "cibn/company_cibn_audit";
    /**
     * 媒体位CIBN审核视图
     */
    private static final String POSITION_CIBN_AUDIT = "cibn/position_cibn_audit";
    /**
     * 官网：新闻管理视图
     */
    private static final String NEWS_MANAGEMENT = "website/news_management";
    /**
     * 官网：新闻详情视图
     */
    private static final String NEWS_DETAIL = "website/news_detail";
    /**
     * 官网：合作案例管理视图
     */
    private static final String CASE_MANAGEMENT = "website/case_management";
    /**
     * 官网：合作案例详情视图
     */
    private static final String CASE_DETAIL = "website/case_detail";
    /**
     * 官网：表单数据列表视图
     */
    private static final String FORM_DATA_LIST = "website/form_data_list";
    /**
     * 应用管理视图
     */
    private static final String APPLICATION_MANAGEMENT = "application/application_management";

    /**
     * 进入登录视图
     *
     * @return 模型视图
     * @throws WayzException
     */
    @RequestMapping(value = "/", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView enterLoginViews(HttpServletRequest request) throws WayzException {
        // 初始化
        ModelAndView mv = new ModelAndView();

        // 处理逻辑
        try {
            // 设置视图
            mv.setViewName(LOGIN);
        } catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }

    /**
     * 进入登录视图
     *
     * @return 模型视图
     */
    @RequestMapping(value = "/login", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView enterLoginView(HttpServletRequest request) throws WayzException {
        // 初始化
        ModelAndView mv = new ModelAndView();

        // 处理逻辑
        try {
            // 设置视图
            mv.setViewName(LOGIN);
        } catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }

    /**
     * 进入索引视图
     *
     * @param myId 我的标识
     * @return 模型视图
     */
    @RequestMapping(value = "/index", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView enterIndexView(@ModelAttribute("myId") Long myId) {
        // 初始化
        ModelAndView mv = new ModelAndView();

        // 处理逻辑
        try {
            // 设置模型
            // 获取用户信息
            mv.addObject("user", userAdminService.getMyUser(myId));

            // 设置视图
            mv.setViewName(INDEX);
        } catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }

    /**
     * 进入跳转视图
     *
     * @return 模型视图
     */
    @RequestMapping(value = "/redirect", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView enterRedirectView() {
        // 初始化
        ModelAndView mv = new ModelAndView();

        // 设置视图
        mv.setViewName(REDIRECT);

        // 返回应答
        return mv;
    }

    /**
     * 进入忘记密码视图
     *
     * @return 模型视图
     */
    @RequestMapping(value = "/forget_pwd", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView enterForgetPwdView(HttpServletRequest request) {
        // 初始化
        ModelAndView mv = new ModelAndView();

        // 处理逻辑
        try {
            // 设置视图
            mv.setViewName(FORGET_PWD);
        } catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }

    /**
     * 进入用户管理视图
     *
     * @param myId 我的标识
     * @return 模型视图
     */
    @RequestMapping(value = "/user_management", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView enterUserManagementView(@ModelAttribute("myId") Long myId) {
        // 初始化
        ModelAndView mv = new ModelAndView();

        // 处理逻辑
        try {
            // 设置模型
            // 获取所有角色
            mv.addObject("roleList", roleAdminService.queryRoleView(myId));

            // 设置权限
            mv.addObject("$rightList", menuAdminService.queryViewAuthority(myId, "user_management"));

            // 设置视图
            mv.setViewName(USER_MANAGEMENT);
        } catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }

    /**
     * 进入角色管理视图
     *
     * @param myId 我的标识
     * @return 模型视图
     */
    @RequestMapping(value = "/role_management", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView enterRoleManagementView(@ModelAttribute("myId") Long myId) {
        // 初始化
        ModelAndView mv = new ModelAndView();

        // 处理逻辑
        try {
            // 设置模型

            // 设置权限
            mv.addObject("$rightList", menuAdminService.queryViewAuthority(myId, "role_management"));

            // 设置视图
            mv.setViewName(ROLE_MANAGEMENT);
        } catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }

    /**
     * 进入优惠券管理视图
     *
     * @param myId 我的标识
     * @return 模型视图
     */
    @RequestMapping(value = "/coupon_management", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView enterCouponManagementView(@ModelAttribute("myId") Long myId) {
        // 初始化
        ModelAndView mv = new ModelAndView();

        // 处理逻辑
        try {
            // 获取所有公司
            mv.addObject("companyList", companyService.queryViewMap());

            // 设置权限
            mv.addObject("$rightList", menuAdminService.queryViewAuthority(myId, "coupon_management"));

            // 设置视图
            mv.setViewName(COUPON_MANAGEMENT);
        } catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }

    /**
     * 播控器系统版本
     *
     * @param myId 我的标识
     * @return 模型视图
     */
    @RequestMapping(value = "/play_version", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView enterPlayVersionView(@ModelAttribute("myId") Long myId) {
        // 初始化
        ModelAndView mv = new ModelAndView();

        // 处理逻辑
        try {
            // 获取所有公司
            mv.addObject("companyList", companyService.queryViewMap());

            // 设置权限
            mv.addObject("$rightList", menuAdminService.queryViewAuthority(myId, "play_version"));

            // 设置视图
            mv.setViewName(PLAY_VERSION);
        } catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }

    @RequestMapping(value = "/company_management", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView enterCompanyManagerView(@ModelAttribute("myId") Long myId) {
        // 初始化
        ModelAndView mv = new ModelAndView();

        // 处理逻辑
        try {
            // 设置模型
            // 所有版本
            mv.addObject("versionList", versionService.queryVersionView(myId));
            // 所有代理商
            mv.addObject("agentList", agentService.queryAgentCompanyView());
            // 充值类型
            mv.addObject("chargeTypeList", tradeService.queryTradeType(myId, (short) 1));

            // 设置权限
            mv.addObject("$rightList", menuAdminService.queryViewAuthority(myId, "company_management"));

            // 设置视图
            mv.setViewName(COMPANY_MANAGEMENT);
        } catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }

    /**
     * 进入代理商管理视图
     *
     * @param myId 我的标识
     * @return 模型视图
     */
    @RequestMapping(value = "/agent_management", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView enterAgentManagerView(@ModelAttribute("myId") Long myId) {
        // 初始化
        ModelAndView mv = new ModelAndView();

        // 处理逻辑
        try {
            // 设置模型
            // 获取行业
             mv.addObject("industryList", categoryService.queryCategory(null, null, null));

            // 查询省份
            mv.addObject("provinceList", homeService.queryProvince(myId, null));

            // 设置权限
            mv.addObject("$rightList", menuAdminService.queryViewAuthority(myId, "agent_management"));

            // 设置视图
            mv.setViewName(AGENT_MANAGEMENT);
        } catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }

    /**
     * 进入审核方管理视图
     *
     * @param myId 我的标识
     * @return 模型视图
     */
    @RequestMapping(value = "/auditor_management", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView enterAuditorManagementView(@ModelAttribute("myId") Long myId) {
        // 初始化
        ModelAndView mv = new ModelAndView();

        // 处理逻辑
        try {
            // 设置模型

            // 设置权限
            mv.addObject("$rightList", menuAdminService.queryViewAuthority(myId, "auditor_management"));

            // 设置视图
            mv.setViewName(AUDITOR_MANAGEMENT);
        } catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }

    /**
     * 进入版本管理视图
     *
     * @param myId 我的标识
     * @return 模型视图
     */
    @RequestMapping(value = "/version_management", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView enterVersionManagementView(@ModelAttribute("myId") Long myId) {
        // 初始化
        ModelAndView mv = new ModelAndView();

        // 处理逻辑
        try {
            // 设置权限
            mv.addObject("$rightList", menuAdminService.queryViewAuthority(myId, "version_management"));

            // 设置视图
            mv.setViewName(VERSION_MANAGEMENT);
        } catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }

    /**
     * 进入设备管理视图
     *
     * @param myId 我的标识
     * @return 模型视图
     */
    @RequestMapping(value = "/position_management", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView enterPositionManagementView(@ModelAttribute("myId") Long myId) {
        // 初始化
        ModelAndView mv = new ModelAndView();

        // 处理逻辑
        try {
            // 媒体主公司
            mv.addObject("mediaCompanyList", companyService.queryViewMap());
            // 审核状态
            mv.addObject("auditStatusList", advPositionCibnService.queryAuditType(myId));

            // 设置权限
            mv.addObject("$rightList", menuAdminService.queryViewAuthority(myId, "position_management"));

            // 设置视图
            mv.setViewName(POSITION_MANAGEMENT);
        } catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }

    /**
     * 进入系统通知视图
     *
     * @param myId 我的标识
     * @return 模型视图
     */
    @RequestMapping(value = "/system_notice_management", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView enterSystemNoticeManagementView(@ModelAttribute("myId") Long myId) {
        // 初始化
        ModelAndView mv = new ModelAndView();

        // 处理逻辑
        try {
            // 设置权限
            mv.addObject("$rightList", menuAdminService.queryViewAuthority(myId, "system_notice_management"));

            // 设置视图
            mv.setViewName(SYSTEM_NOTICE_MANAGEMENT);
        } catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }

    /**
     * 进入设备续期价格管理视图
     *
     * @param myId 我的标识
     * @return 模型视图
     */
    @RequestMapping(value = "/device_price_management", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView enterDevicePriceManagementView(@ModelAttribute("myId") Long myId) {
        // 初始化
        ModelAndView mv = new ModelAndView();

        // 处理逻辑
        try {
            // 设置模型
            // 设置权限
            mv.addObject("$rightList", menuAdminService.queryViewAuthority(myId, "device_price_management"));

            // 设置视图
            mv.setViewName(DEVICE_PRICE_MANAGEMENT);
        } catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }

    /**
     * 进入空间扩展价格管理视图
     *
     * @param myId 我的标识
     * @return 模型视图
     */
    @RequestMapping(value = "/space_price_management", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView enterSpacePriceManagementView(@ModelAttribute("myId") Long myId) {
        // 初始化
        ModelAndView mv = new ModelAndView();

        // 处理逻辑
        try {
            // 设置模型
            // 设置权限
            mv.addObject("$rightList", menuAdminService.queryViewAuthority(myId, "space_price_management"));

            // 设置视图
            mv.setViewName(SPACE_PRICE_MANAGEMENT);
        } catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }

    /**
     * 进入直播渠道管理视图
     *
     * @param myId 我的标识
     * @return 模型视图
     */
    @RequestMapping(value = "/live_channel_management", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView enterLiveChannelManagementView(@ModelAttribute("myId") Long myId) {
        // 初始化
        ModelAndView mv = new ModelAndView();

        // 处理逻辑
        try {
            // 设置模型
            // 设置权限
            mv.addObject("$rightList", menuAdminService.queryViewAuthority(myId, "live_channel_management"));

            // 设置视图
            mv.setViewName(LIVE_CHANNEL_MANAGEMENT);
        } catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }

    /**
     * 进入直播列表视图
     *
     * @return 模型视图
     */
    @RequestMapping(value = "/live_plan_list", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView enterLivePlanListView(@ModelAttribute("myId") Long myId) {
        // 初始化
        ModelAndView mv = new ModelAndView();

        // 处理逻辑
        try {
            // 设置视图
            mv.setViewName(LIST_PLAN_LIST);
        } catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }

    /**
     * 进入内容管理视图
     *
     * @param myId 我的标识
     * @return 模型视图
     */
    @RequestMapping(value = "/content_management", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView enterContentManagementView(@ModelAttribute("myId") Long myId) {
        // 初始化
        ModelAndView mv = new ModelAndView();

        // 处理逻辑
        try {

            // 获取内容渠道
            mv.addObject("contentChannelList", channelService.queryChannelViewList(null));

            // 获取内容分类
            mv.addObject("contentCategoryList", contentCategoryService.queryContentCategoryView(null));

            // 所有文件类型
            mv.addObject("fileTypeList", contentService.queryMaterialType(myId));

            // 审核状态
            mv.addObject("auditStatusList", contentService.queryAuditType(myId));

            // 设置权限
            mv.addObject("$rightList", menuAdminService.queryViewAuthority(myId, "content_management"));

            // 设置视图
            mv.setViewName(CONTENT_MANAGEMENT);
        } catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }

    /**
     * 进入内容审核视图
     *
     * @param myId 我的标识
     * @return 模型视图
     */
    @RequestMapping(value = "/content_audit_management", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView enterContentAuditManagementView(@ModelAttribute("myId") Long myId) {
        // 初始化
        ModelAndView mv = new ModelAndView();

        // 处理逻辑
        try {

            // 获取内容渠道
            mv.addObject("contentChannelList", channelService.queryChannelViewList(null));

            // 所有文件类型
            mv.addObject("fileTypeList", contentService.queryMaterialType(myId));

            // 设置权限
            mv.addObject("$rightList", menuAdminService.queryViewAuthority(myId, "content_audit_management"));

            // 设置视图
            mv.setViewName(CONTENT_AUDIT_MANAGEMENT);
        } catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }

    /**
     * 进入内容分类管理视图
     *
     * @param myId 我的标识
     * @return 模型视图
     */
    @RequestMapping(value = "/content_category_management", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView enterContentCategoryManagementView(@ModelAttribute("myId") Long myId) {
        // 初始化
        ModelAndView mv = new ModelAndView();

        // 处理逻辑
        try {
            // 设置权限
            mv.addObject("$rightList", menuAdminService.queryViewAuthority(myId, "content_category_management"));

            // 设置视图
            mv.setViewName(CONTENT_CATEGORY_MANAGEMENT);
        } catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }

    /**
     * 进入海报行业类别管理视图
     *
     * @param myId 我的标识
     * @return 模型视图
     */
    @RequestMapping(value = "/poster_category_management", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView entePosterCategoryManagementView(@ModelAttribute("myId") Long myId) {
        // 初始化
        ModelAndView mv = new ModelAndView();

        // 处理逻辑
        try {
            // 设置权限
            mv.addObject("$rightList", menuAdminService.queryViewAuthority(myId, "poster_category_management"));

            // 设置视图
            mv.setViewName(POSTER_CATEGORY_MANAGEMENT);
        } catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }

    /**
     * 进入海报分辨率管理视图
     *
     * @param myId 我的标识
     * @return 模型视图
     */
    @RequestMapping(value = "/poster_resolution_management", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView enterPosterResolutionManagementView(@ModelAttribute("myId") Long myId) {
        // 初始化
        ModelAndView mv = new ModelAndView();

        // 处理逻辑
        try {
            // 设置权限
            mv.addObject("$rightList", menuAdminService.queryViewAuthority(myId, "poster_resolution_management"));

            // 设置视图
            mv.setViewName(POSTER_RESOLUTION_MANAGEMENT);
        } catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }

    /**
     * 进入海报模板制作视图
     *
     * @return 模型视图
     */
    @RequestMapping(value = "/poster_template_index", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView enterPosterTemplateIndexView(HttpServletRequest request) {
        // 初始化
        ModelAndView mv = new ModelAndView();
        // 处理逻辑
        try {
            // 设置视图
            mv.setViewName(POSTER_TEMPLATE_INDEX);
        } catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }

    /**
     * 新闻管理视图
     *
     * @param myId 我的标识
     * @return 模型视图
     */
    @RequestMapping(value = "/news_management", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView enterNewsManagementView(@ModelAttribute("myId") Long myId) {
        // 初始化
        ModelAndView mv = new ModelAndView();

        // 处理逻辑
        try {
            // 设置模型
            // 查询新闻类型
            mv.addObject("newsTypeList", homeService.queryArticleCategory(myId, null));

            // 设置权限
            mv.addObject("$rightList", menuAdminService.queryViewAuthority(myId, "news_management"));

            // 设置视图
            mv.setViewName(NEWS_MANAGEMENT);
        } catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }

    /**
     * 新闻详情视图
     *
     * @param myId 我的标识
     * @return 模型视图
     */
    @RequestMapping(value = "/news_detail", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView enterNewsDetailView(@ModelAttribute("myId") Long myId,
                                            @RequestParam(value = "id", required = false) Long id) {
        // 初始化
        ModelAndView mv = new ModelAndView();

        // 处理逻辑
        try {
            // 设置模型
            // 设置权限
            mv.addObject("$rightList", menuAdminService.queryViewAuthority(myId, "news_detail"));

            // 设置视图
            mv.setViewName(NEWS_DETAIL);
        } catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }

    /**
     * 合作案例管理视图
     *
     * @param myId 我的标识
     * @return 模型视图
     */
    @RequestMapping(value = "/case_management", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView enterCaseManagementView(@ModelAttribute("myId") Long myId) {
        // 初始化
        ModelAndView mv = new ModelAndView();

        // 处理逻辑
        try {
            // 设置模型
            // 设置权限
            mv.addObject("$rightList", menuAdminService.queryViewAuthority(myId, "case_management"));

            // 设置视图
            mv.setViewName(CASE_MANAGEMENT);
        } catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }

    /**
     * 合作案例详情视图
     *
     * @param myId 我的标识
     * @return 模型视图
     */
    @RequestMapping(value = "/case_detail", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView enterCaseDetailView(@ModelAttribute("myId") Long myId,
                                            @RequestParam(value = "id", required = false) Long id) {
        // 初始化
        ModelAndView mv = new ModelAndView();

        // 处理逻辑
        try {
            // 设置模型
            // 设置权限
            mv.addObject("$rightList", menuAdminService.queryViewAuthority(myId, "case_detail"));

            // 设置视图
            mv.setViewName(CASE_DETAIL);
        } catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }

    /**
     * 表单数据列表视图
     *
     * @param myId 我的标识
     * @return 模型视图
     */
    @RequestMapping(value = "/form_data_list", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView enterFormDataListView(@ModelAttribute("myId") Long myId,
                                              @RequestParam(value = "id", required = false) Long id) {
        // 初始化
        ModelAndView mv = new ModelAndView();

        // 处理逻辑
        try {
            // 设置模型
            // 查询新闻类型
            mv.addObject("typeList", homeService.queryFormDataCategory(myId, null));
            //// 查询省份
            mv.addObject("provinceList", homeService.queryProvince(myId, null));

            // 设置权限
            mv.addObject("$rightList", menuAdminService.queryViewAuthority(myId, "form_data_list"));

            // 设置视图
            mv.setViewName(FORM_DATA_LIST);
        } catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }

    /**
     * 进入模板制作视图
     *
     * @return 模型视图
     */
    @RequestMapping(value = "/live_template_index", method = { RequestMethod.GET, RequestMethod.POST })
    public ModelAndView enterLiveTemplateIndexView(HttpServletRequest request) throws WayzException {
        // 初始化
        ModelAndView mv = new ModelAndView();
        // 处理逻辑
        try {
            // 设置视图
            mv.setViewName(LIVE_TEMPLATE_INDEX);
        }
        catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }

    /**
     * 进入公司CIBN审核视图
     *
     * @param myId 我的标识
     * @return 模型视图
     */
    @RequestMapping(value = "/company_cibn_audit", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView enterCompanyCibnAuditView(@ModelAttribute("myId") Long myId) {
        // 初始化
        ModelAndView mv = new ModelAndView();

        // 处理逻辑
        try {
            // 获取行业
            mv.addObject("industryList", categoryService.queryCategory(null, null, null));

            // 审核状态
            mv.addObject("auditStatusList", companyCibnService.queryAuditType(myId));

            // 设置权限
            mv.addObject("$rightList", menuAdminService.queryViewAuthority(myId, "company_cibn_audit"));

            // 设置视图
            mv.setViewName(COMPANY_CIBN_AUDIT);
        } catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }

    /**
     * 进入媒体位CIBN审核视图
     *
     * @param myId 我的标识
     * @return 模型视图
     */
    @RequestMapping(value = "/position_cibn_audit", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView enterPositionCibnAuditView(@ModelAttribute("myId") Long myId) {
        // 初始化
        ModelAndView mv = new ModelAndView();

        // 处理逻辑
        try {
            // 审核状态
            //mv.addObject("auditStatusList", positionCibnService.queryAuditType(myId));

            // 设置权限
            mv.addObject("$rightList", menuAdminService.queryViewAuthority(myId, "position_cibn_audit"));

            // 设置视图
            mv.setViewName(POSITION_CIBN_AUDIT);
        } catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }

    /**
     * 进入应用管理视图
     *
     * @param myId 我的标识
     * @return 模型视图
     */
    @RequestMapping(value = "/application_management", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView enterApplicationManagementView(@ModelAttribute("myId") Long myId) {
        // 初始化
        ModelAndView mv = new ModelAndView();

        // 处理逻辑
        try {
            // 设置权限
            mv.addObject("$rightList", menuAdminService.queryViewAuthority(myId, "application_management"));

            // 设置视图
            mv.setViewName(APPLICATION_MANAGEMENT);
        } catch (Exception e) {
            // 错误视图
            mv.addObject("message", e.getMessage());
            mv.setViewName(ERROR);
        }

        // 返回应答
        return mv;
    }
}
