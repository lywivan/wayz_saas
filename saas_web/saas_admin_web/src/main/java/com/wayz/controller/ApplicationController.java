package com.wayz.controller;

import com.wayz.entity.dto.application.ApplicationDto;
import com.wayz.entity.dto.application.ShopContentPageDto;
import com.wayz.exception.WayzException;
import com.wayz.response.WayzResponse;
import com.wayz.service.ApplicationService;
import com.wayz.validator.ParameterValidator;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * 应用控制类
 */
@Controller
@RequestMapping("/saas/application")
@SessionAttributes("myId")
public class ApplicationController {
	/**
	 * 应用服务类
	 */
	@Reference(version = "1.0.0", check = false)
	private ApplicationService applicationService;

	/**
	 * 查询应用列表
	 * 
	 * @param myId 我的标识
	 * @return
	 * @throws WayzException
	 */
	@ResponseBody
	@RequestMapping(value = "/queryApplication", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse queryApplication(@ModelAttribute("myId") Long myId,
										 ShopContentPageDto dto) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		response.setData(applicationService.queryApplication(myId, dto.getName(),
				dto.getStarNum(), dto.getIsEnable(), dto.getStartIndex(), dto.getPageSize()));
		return response;
	}

	/**
	 * 应用详情
	 * 
	 * @param myId 我的标识
	 * @param id 应用标识
	 * @return
	 * @throws WayzException
	 */
	@ResponseBody
	@RequestMapping(value = "/getApplication", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse getApplication(@ModelAttribute("myId") Long myId,
			@RequestParam(value = "id", required = false) Long id) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		ParameterValidator.notNull(id, "应用ID");
		response.setData(applicationService.getApplication(myId, id));
		return response;
	}

	/**
	 * 创建应用
	 * 
	 * @param myId 我的标识
	 * @return
	 * @throws WayzException
	 */
	@ResponseBody
	@RequestMapping(value = "/createApplication", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse createApplication(@ModelAttribute("myId") Long myId,
										  ApplicationDto dto) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		ParameterValidator.notNull(dto.getName(), "应用名称有误");
		applicationService.createApplication(myId, dto.getName(),
				dto.getIcon(), dto.getHref(), dto.getStarNum(),
				dto.getDescription(), dto.getImages(), true);
		return response;
	}

	/**
	 * 修改应用
	 * 
	 * @param myId 我的标识
	 * @return
	 * @throws WayzException
	 */
	@ResponseBody
	@RequestMapping(value = "/modifyApplication", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse modifyApplication(@ModelAttribute("myId") Long myId,
			ApplicationDto dto) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		ParameterValidator.notNull(dto.getId(), "应用标识有误");
		applicationService.modifyApplication(myId, dto.getId(),
				dto.getName(), dto.getIcon(), dto.getHref(),
				dto.getStarNum(), dto.getDescription(), dto.getImages(), true);
		return response;
	}

	/**
	 * 删除应用
	 * 
	 * @param myId 我的标识
	 * @param id 应用标识
	 * @return
	 * @throws WayzException
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteApplication", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse deleteApplication(@ModelAttribute("myId") Long myId,
			@RequestParam(value = "id", required = false) Long id) throws WayzException {

		// 初始化
		WayzResponse response = new WayzResponse();
		ParameterValidator.notNull(id, "应用标识有误");
		applicationService.deleteApplication(myId, id);
		return response;
	}

}
