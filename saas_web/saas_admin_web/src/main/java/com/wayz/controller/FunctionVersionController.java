package com.wayz.controller;

import com.wayz.entity.dto.function.VersionMenuIdsDto;
import com.wayz.exception.WayzException;
import com.wayz.response.WayzResponse;
import com.wayz.service.FunctionVersionService;
import com.wayz.validator.ParameterValidator;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


/**
 * 版本管理控制器类
 */
@Controller
@RequestMapping("/saas/version")
@SessionAttributes("myId")
public class FunctionVersionController {

	/**
	 * 功能版本服务
	 */
	@Reference(version = "1.0.0", check = false)
	private FunctionVersionService functionVersionService = null;

	/**
	 * 创建版本
	 *
	 * @param myId 我的标识
	 * @param name 版本名称
	 * @param description 描述
	 * @param userLimit 用户数限制
	 * @param materialLimit 素材空间限制
	 * @param singleFileLimit 单个文件大小限制
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/createVersion", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse createFunctionVersion(@ModelAttribute("myId") Long myId,
											  @RequestParam(value = "name") String name,
											  @RequestParam(value = "description", required = false) String description,
											  @RequestParam(value = "userLimit", required = false) Integer userLimit,
											  @RequestParam(value = "materialLimit", required = false) Long materialLimit,
											  @RequestParam(value = "singleFileLimit", required = false) Long singleFileLimit) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		// 调用接口
		response.setData(functionVersionService.createFunctionVersion(myId, name, description, userLimit, materialLimit,
				singleFileLimit));
		// 返回应答
		return response;
	}

	/**
	 * 查询功能版本
	 *
	 * @param myId 我的标识
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/queryVersion", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse queryFunctionVersion(@ModelAttribute("myId") Long myId,
											 @RequestParam(value = "name", required = false) String name) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		// 调用接口
		response.setData(functionVersionService.queryFunctionVersion(myId, name));
		// 返回应答
		return response;
	}

	/**
	 * 查询功能版本视图
	 *
	 * @param myId 我的标识
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/queryVersionViewMap", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse queryVersionViewMap(@ModelAttribute("myId") Long myId) {
		// 初始化
		WayzResponse response = new WayzResponse();
		// 调用接口
		response.setData(functionVersionService.queryVersionView(myId));
		// 返回应答
		return response;
	}

	/**
	 * 删除功能版本
	 *
	 * @param myId 我的标识
	 * @param id 版本标识
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteVersion", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse deleteFunctionVersion(@ModelAttribute("myId") Long myId, @RequestParam("id") Long id)
			throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		// 调用接口
		functionVersionService.deleteFunctionVersion(myId, id);
		// 返回应答
		return response;
	}


	/**
	 * 根据ID获取版本详情
	 * @param myId 我的ID
	 * @param id 版本ID
	 * @return
	 * @throws WayzException
	 */
	@ResponseBody
	@RequestMapping(value = "/getVersionById", method = {RequestMethod.GET, RequestMethod.POST})
	public WayzResponse getVersionById(@ModelAttribute("myId") Long myId,
									   @RequestParam("id") Long id) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		// 调用接口
		response.setData(functionVersionService.getVersion(myId, id));
		// 返回应答
		return response;
	}

	/**
	 * 修改版本
	 *
	 * @param myId 我的标识
	 * @param name 版本名称
	 * @param description 描述
	 * @param userLimit 用户数限制
	 * @param materialLimit 素材空间限制
	 * @param singleFileLimit 单个文件大小限制
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/modifyVersion", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse modifyVersion(@ModelAttribute("myId") Long myId, @RequestParam(value = "id") Long id,
									  @RequestParam(value = "name") String name, @RequestParam(value = "description") String description,
									  @RequestParam(value = "userLimit", required = false) Integer userLimit,
									  @RequestParam(value = "materialLimit", required = false) Long materialLimit,
									  @RequestParam(value = "singleFileLimit", required = false) Long singleFileLimit) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		// 调用接口
		functionVersionService.modifyFunctionVersion(myId, id, name, description, userLimit, materialLimit, singleFileLimit);
		// 返回应答
		return response;
	}

	/**
	 * 给管理员角色授权
	 *
	 * @param myId 我的标识
	 * @param dto 角色权限标识列表
	 */
	@ResponseBody
	@RequestMapping(value = "/setVersionMenuList", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse setVersionMenuList(@ModelAttribute("myId") Long myId,
										   @RequestBody VersionMenuIdsDto dto) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();

		// 验证参数
		// 验证参数: 版本标识
		ParameterValidator.notNull(dto.getVersionId(), "版本标识");
		// 验证参数: 权限标识列表
		ParameterValidator.notNull(dto.getMenuIdList(), "权限标识列表");

		// 调用接口
		functionVersionService.setVersionMenuList(myId, dto.getVersionId(), dto.getMenuIdList());
		// 返回应答
		return response;

	}

}
