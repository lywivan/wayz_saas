package com.wayz.controller;

import com.wayz.exception.WayzException;
import com.wayz.response.WayzResponse;
import com.wayz.service.MenuAdminService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @className: MenuController
 * @description: 菜单 <功能详细描述>
 * @author wade.liu@wayz.ai
 * @date: 2019/6/20
 */
@Controller
@RequestMapping("/menu")
@SessionAttributes("myId")
public class MenuAdminController {

	/** 菜单服务 */
	@Reference(version = "1.0.0", check=false)
	private MenuAdminService menuAdminService = null;

	/**
	 * 查询全部菜单
	 *
	 * @param myId 我的标识
	 * @return yunge应答
	 * @throws WayzException 云歌广告平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/queryAllMenu", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse queryAllMenu(@ModelAttribute("myId") Long myId) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();

		// 调用接口
		response.setData(menuAdminService.queryAllMenu(myId));

		// 返回应答
		return response;
	}

	/**
	 * 查询我的菜单
	 *
	 * @param myId 我的标识
	 * @return yunge应答
	 * @throws WayzException 云歌广告平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/queryMyMenu", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse queryMyMenu(@ModelAttribute("myId") Long myId) {
		// 初始化
		WayzResponse response = new WayzResponse();

		// 调用接口
		response.setData(menuAdminService.queryMyMenu(myId));

		// 返回应答
		return response;
	}

	/**
	 * 查询角色菜单
	 *
	 * @param myId 我的标识
	 * @param roleId 角色标识
	 * @return yunge应答
	 * @throws WayzException 云歌广告平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/queryRoleMenu", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse queryRoleMenu(@ModelAttribute("myId") Long myId,
									  @RequestParam("roleId") Long roleId) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		// 调用接口
		response.setData(menuAdminService.queryRoleMenu(myId, roleId));
		// 返回应答
		return response;
	}
}
