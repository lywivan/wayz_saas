package com.wayz.controller;

import com.wayz.entity.dto.cibn.AdvPositionCibnPageDto;
import com.wayz.entity.dto.cibn.AdvPositionPageDto;
import com.wayz.entity.dto.cibn.MediaCibnDto;
import com.wayz.exception.WayzException;
import com.wayz.response.WayzResponse;
import com.wayz.service.AdvPositionCibnService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * 媒体点位申请记录控制类
 */
@Controller
@RequestMapping("/saas/media")
@SessionAttributes("myId")
public class PositionAdminCibnController {

	/**
	 * 代理服务
	 */
	@Reference(version = "1.0.0", check = false)
	private AdvPositionCibnService advPositionCibnService;

	/**
	 * 查询媒体点位
	 *
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/queryPosition", method = { RequestMethod.POST })
	public WayzResponse queryPosition(@ModelAttribute("myId") Long myId,
									  AdvPositionPageDto dto) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		response.setData(advPositionCibnService.queryPosition(
				dto.getCompanyId(), dto.getId(), dto.getPlayCode(),
				dto.getName(), dto.getIsBound(), dto.getIsEnable(), dto.getIsOnline(),
				dto.getStatus(), dto.getAuditStatus(), dto.getStartIndex(), dto.getPageSize()));
		return response;
	}

	/**
	 * 媒体位详情
	 *
	 * @param myId
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getPositionDetail", method = { RequestMethod.POST })
	public WayzResponse getDetails(@ModelAttribute("myId") Long myId,
			@RequestParam(value = "id", required = false) Long id) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		response.setData(advPositionCibnService.getPosition(id));
		return response;
	}

	/**
	 * 媒体点位申请记录列表
	 *
	 * @param myId
	 */
	@ResponseBody
	@RequestMapping(value = "/queryPositionCibnList", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse queryPositionCibnList(@ModelAttribute("myId") Long myId,
											  AdvPositionCibnPageDto dto) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		response.setData(advPositionCibnService.queryAdminPositionCibnList(
				dto.getAuditStatus(), dto.getCode(),
				dto.getName(), dto.getStartIndex(), dto.getPageSize()));
		return response;
	}

	/**
	 * 媒体点位审核
	 *
	 * @param myId
	 * @param mediaCibnVO
	 * @return
	 * @throws YungeException
	 */
	@ResponseBody
	@RequestMapping(value = "/auditMediaCibn", method = { RequestMethod.POST })
	public WayzResponse auditMediaCibn(@ModelAttribute("myId") Long myId, MediaCibnDto dto)
			throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		advPositionCibnService.auditMediaCibn(dto);
		return response;
	}

}
