package com.wayz.controller;

import com.wayz.entity.dto.poster.*;
import com.wayz.exception.WayzException;
import com.wayz.response.WayzResponse;
import com.wayz.service.PosterService;
import com.wayz.validator.ParameterValidator;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * 海报管理控制器类
 */
@Controller
@RequestMapping("/saas/poster")
@SessionAttributes("myId")
public class PosterController {

	/** 海报代理 */
	@Reference(version = "1.0.0", check = false)
	private PosterService posterService;

	/**
	 * 查询分辨率
	 * 
	 * @param myId 我的标识
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/queryPosterResolutionView", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse queryPosterResolutionView(@ModelAttribute("myId") Long myId) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();

		// 验证参数

		// 调用接口
		response.setData(posterService.queryPosterResolutionView(myId, null));

		// 返回应答
		return response;
	}

	/**
	 * 查询分辨率
	 * 
	 * @param myId 我的标识
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/queryPosterResolution", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse queryPosterResolution(@ModelAttribute("myId") Long myId, PosterResolutionPageDto posterResolutionPageDto )throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();

		// 验证参数

		// 调用接口
		response.setData(posterService.queryPosterResolution(myId, posterResolutionPageDto));

		// 返回应答
		return response;
	}

	/**
	 * 创建分辨率
	 * 
	 * @param myId 我的标识
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/createPosterResolution", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse createPosterResolution(@ModelAttribute("myId") Long myId, PosterResolutionDto posterResolution) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();

		// 验证参数
		// 验证参数: 宽(pix)
		ParameterValidator.notNull(posterResolution.getWidth(), "宽(pix)");
		// 验证参数: 高(pix)
		ParameterValidator.notNull(posterResolution.getHeight(), "高(pix)");
		// 验证参数: 排序值(升序排列)
		// ParameterValidator.notNull(sort, "排序值");

		// 调用接口
		response.setData(posterService.createPosterResolution(myId, posterResolution));

		// 返回应答
		return response;
	}

	/**
	 * 删除分辨率
	 * 
	 * @param myId 我的标识
	 * @param id 分辨率标识
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/deletePosterResolution", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse deletePosterResolution(@ModelAttribute("myId") Long myId,
			@RequestParam(value = "id", required = false) Long id) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();

		// 验证参数
		// 验证参数: 分辨率标识
		ParameterValidator.notNull(id, "分辨率标识");

		// 调用接口
		posterService.deletePosterResolution(myId, id, null);

		// 返回应答
		return response;
	}

	/**
	 * 查询海报类别
	 * 
	 * @param myId 我的标识
	 * @param id 海报类别标识
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/queryPosterCategory", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse queryPosterCategory(@ModelAttribute("myId") Long myId,
			@RequestParam(value = "id", required = false) Long id) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();

		// 验证参数

		// 调用接口
		response.setData(posterService.queryPosterCategory(myId, null, id));

		// 返回应答
		return response;
	}

	/**
	 * 创建海报类别
	 * 
	 * @param myId 我的标识
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/createPosterCategory", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse createPosterCategory(@ModelAttribute("myId") Long myId, PosterCategoryDto posterCategoryDto) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();

		// 验证参数
		// 验证参数: 海报类别名称
		ParameterValidator.notNull(posterCategoryDto.getName(), "海报类别名称");

		// 调用接口
		response.setData(posterService.createPosterCategory(myId, posterCategoryDto));

		// 返回应答
		return response;
	}

	/**
	 * 修改海报类别
	 * 
	 * @param myId 我的标识
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/modifyPosterCategory", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse modifyPosterCategory(@ModelAttribute("myId") Long myId, PosterCategoryDto posterCategoryDto) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();

		// 验证参数
		// 验证参数: 海报类别标识
		ParameterValidator.notNull(posterCategoryDto.getId(), "海报类别标识");
		// 验证参数: 海报类别名称
		ParameterValidator.notNull(posterCategoryDto.getName(), "海报类别名称");

		// 调用接口
		posterService.modifyPosterCategory(myId, posterCategoryDto);

		// 返回应答
		return response;
	}

	/**
	 * 删除海报类别
	 * 
	 * @param myId 我的标识
	 * @param id 海报类别标识
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/deletePosterCategory", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse deletePosterCategory(@ModelAttribute("myId") Long myId,
			@RequestParam(value = "id", required = false) Long id) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();

		// 验证参数
		// 验证参数: 海报类别标识
		ParameterValidator.notNull(id, "海报类别标识");

		// 调用接口
		posterService.deletePosterCategory(myId, id);

		// 返回应答
		return response;
	}

	/**
	 * 查询海报模板
	 * 
	 * @param myId 我的标识
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/queryPosterTemplet", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse queryPosterTemplet(@ModelAttribute("myId") Long myId, PosterTempletPageDto posterTempletPageDto) throws WayzException {
		// 初始化
		WayzResponse response = new WayzResponse();
		response.setData(posterService.queryPosterTemplet(myId, posterTempletPageDto));
		return response;
	}

	/**
	 * 查询海报模板视图列表
	 * 
	 * @param myId 我的标识
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/queryPosterTempletView", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse queryPosterTempletView(@ModelAttribute("myId") Long myId) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 调用接口
		$response.setData(posterService.queryPosterTempletView(myId, null));

		// 返回应答
		return $response;
	}

	/**
	 * 创建海报模板
	 * 
	 * @param myId 我的标识
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/createPosterTemplet", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse createPosterTemplet(@ModelAttribute("myId") Long myId, PosterTempletDto posterTempletDto) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		ParameterValidator.notNull(posterTempletDto.getResolutionId(), "分辨率");
		ParameterValidator.notNull(posterTempletDto.getName(), "海报名称");
		ParameterValidator.notNull(posterTempletDto.getPreviewUrl(), "预览图");

		// 调用接口
		posterService.createPosterTemplet(myId, posterTempletDto);

		// 返回应答
		return $response;
	}

	/**
	 * 修改海报模板
	 * 
	 * @param myId 我的标识
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/modifyPosterTemplet", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse modifyPosterTemplet(@ModelAttribute("myId") Long myId, PosterTempletDto posterTempletDto) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		ParameterValidator.notNull(posterTempletDto.getId(), "海报标示");
		ParameterValidator.notNull(posterTempletDto.getResolutionId(), "分辨率");
		ParameterValidator.notNull(posterTempletDto.getName(), "海报名称");
		ParameterValidator.notNull(posterTempletDto.getPreviewUrl(), "预览图");

		// 调用接口
		posterService.modifyPosterTemplet(myId, posterTempletDto);

		// 返回应答
		return $response;
	}

	/**
	 * 更新海报模板发布状态
	 * 
	 * @param myId 我的标识
	 * @param id 模板标识
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/updatePosterTempletStatus", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse updatePosterTempletStatus(@ModelAttribute("myId") Long myId,
			@RequestParam(value = "id", required = false) Long id,
			@RequestParam(value = "isPublish", required = false) Boolean isPublish) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		ParameterValidator.notNull(id, "海报标示");

		// 调用接口
		posterService.updatePosterTempletStatus(myId, id, isPublish);

		// 返回应答
		return $response;
	}

	/**
	 * 更新海报模板所属公司
	 * 
	 * @param myId 我的标识
	 * @param id 模板标识
	 * @param companyId 所属公司
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/updatePosterCompanyIdByTempletId", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse updatePosterCompanyIdByTempletId(@ModelAttribute("myId") Long myId,
			@RequestParam(value = "id", required = false) Long id,
			@RequestParam(value = "companyId", required = false) Long companyId) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		ParameterValidator.notNull(id, "海报标示");
		ParameterValidator.notNull(companyId, "公司标示");

		// 调用接口
		posterService.updatePosterCompanyIdByTempletId(myId, id, companyId);

		// 返回应答
		return $response;
	}

	/**
	 * 删除海报模板
	 * 
	 * @param myId 我的标识
	 * @param id 海报模板标识
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/deletePosterTemplet", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse deletePosterTemplet(@ModelAttribute("myId") Long myId,
			@RequestParam(value = "id", required = false) Long id) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		ParameterValidator.notNull(id, "海报标示");

		// 调用接口
		posterService.deletePosterTemplet(myId, id);

		// 返回应答
		return $response;
	}

	/**
	 * 获取海报模板详情
	 * 
	 * @param myId 我的标识
	 * @param templetId 模板标识
	 * @return platform应答
	 * @throws WayzException 平台异常
	 */
	@ResponseBody
	@RequestMapping(value = "/getPosterTempletDetail", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse getPosterTempletDetail(@ModelAttribute("myId") Long myId,
			@RequestParam(value = "templetId", required = false) Long templetId) throws WayzException {
		// 初始化
		WayzResponse $response = new WayzResponse();

		// 验证参数
		ParameterValidator.notNull(templetId, "海报标示");

		// 调用接口
		$response.setData(posterService.getPosterTempletDetail(myId, templetId));

		// 返回应答
		return $response;
	}
}
