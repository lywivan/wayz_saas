package com.wayz.controller;

import com.wayz.entity.dto.playVersion.PlayerVersionCompanyDto;
import com.wayz.entity.dto.playVersion.PlayVersionDto;
import com.wayz.exception.WayzException;
import com.wayz.response.WayzResponse;
import com.wayz.service.AliossService;
import com.wayz.service.PlayVersionService;
import com.wayz.validator.ParameterValidator;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @className: PlayVersionController
 * @description: 播控版本 <功能详细描述>
 * @author: 须尽欢_____
 * @date: 2019/7/9
 */
@Controller
@RequestMapping("/playVersion")
@SessionAttributes("myId")
public class PlayVersionController {

	/** 服务相关 */
	@Reference(version = "1.0.0", check = false)
	private PlayVersionService playVersionService;

	/** 文件服务 */
	@Reference(version = "1.0.0", check = false)
	private AliossService aliossService = null;

	@ResponseBody
	@RequestMapping(value = "/createPlayVersion", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse createPlayVersion(@ModelAttribute("myId") Long myId, PlayVersionDto PlayVersionDto)
			throws WayzException {
		if (PlayVersionDto.getIsGeneral() == true) {
			aliossService.setSymlink(PlayVersionDto.getPackageUrl(), "play.apk");
		}
		// 初始化
		WayzResponse response = new WayzResponse();
		// 验证参数
		// 验证参数: 角色名称
		ParameterValidator.notNull(PlayVersionDto.getVersion(), "版本编号");
		ParameterValidator.notNull(PlayVersionDto.getPackageUrl(), "下载地址");
		ParameterValidator.notNull(PlayVersionDto.getFileMd5(), "md5");
		// 调用接口
		response.setData(playVersionService.create(PlayVersionDto));
		// 返回应答
		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/queryPlayVersion", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse queryPlayVersion(@ModelAttribute("myId") Long myId,
										 @RequestParam("startIndex") Integer startIndex, @RequestParam("pageSize") Integer pageSize) {
		// 初始化
		WayzResponse response = new WayzResponse();
		// 调用接口
		response.setData(playVersionService.queryAll(startIndex, pageSize));
		// 返回应答
		return response;
	}

	/**
	 * 版本分配公司
	 * 
	 * @param myId 我得标志
	 * @param dto 版本分配公司
	 * @return
	 * @throws WayzException
	 */
	@ResponseBody
	@RequestMapping(value = "/grantCompany", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse grantCompany(@ModelAttribute("myId") Long myId,
									 @RequestBody PlayerVersionCompanyDto dto) throws WayzException {
		WayzResponse response = new WayzResponse();
		ParameterValidator.notNull(dto.getVersion(), "版本名称");
		ParameterValidator.notNull(dto.getCompanyIdList(), "公司列表");
		response.setData(playVersionService.grantCompany(dto.getVersion(), dto.getCompanyIdList()));
		return response;
	}

	/**
	 * 查询当前版本有那些公司在使用
	 * 
	 * @param myId
	 * @param version
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/queryCompany", method = { RequestMethod.GET, RequestMethod.POST })
	public WayzResponse queryPlayVersion(@ModelAttribute("myId") Long myId,
										 @RequestParam(value = "version") String version) {
		// 初始化
		WayzResponse response = new WayzResponse();
		// 调用接口
		response.setData(playVersionService.queryCompanyIdListByVersion(version));
		// 返回应答
		return response;
	}

}
