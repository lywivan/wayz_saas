package com.wayz.response.exception;

import com.wayz.exception.WayzException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 拦截异常并转换成 自定义ExceptionResponse
 */
@RestControllerAdvice
@Slf4j
public class SaasExceptionHandler {

    @ExceptionHandler(Exception.class)
    public Object Exception(Exception ex) {
        log.error("http response error",ex);
        ExceptionResponse response =new ExceptionResponse(ex.getMessage());
        return response;
    }

    @ExceptionHandler(WayzException.class)
    public Object Exception(WayzException ex) {
        log.error("http response error",ex);
        ExceptionResponse response =new ExceptionResponse(ex.getMessage());
        return response;
    }
}
