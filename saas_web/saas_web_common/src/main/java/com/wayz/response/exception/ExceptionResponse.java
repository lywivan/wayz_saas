package com.wayz.response.exception;

import com.wayz.exception.ExceptionCode;
import lombok.Data;

@Data
public class ExceptionResponse {
    private String message;
    private int code;
    public ExceptionResponse(String message){
        this.message = message;
        this.code= ExceptionCode.UNKNOWN_ERROR.getValue();
    }
}
