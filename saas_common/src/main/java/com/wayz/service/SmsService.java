package com.wayz.service;

import com.wayz.exception.WayzException;

/**
 * 短信服务接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
public interface SmsService {

	/**
	 * 发送短信
	 * 
	 * @param phone 电话号码
	 * @param type 短信类型
	 * @param content 短信内容
	 * @throws YungeException 云歌异常
	 */
	public void sendMessage(String phone, Short type, String content) throws WayzException;

}
