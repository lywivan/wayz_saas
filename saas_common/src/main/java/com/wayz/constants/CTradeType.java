package com.wayz.constants;

/**
 * 交易类型枚举
 * 
 * @author wade.liu@wayz.ai
 *
 */
public enum CTradeType {

	/** 字段相关 */
	/** 充值 */
	CASH_RECHARGE((short) 11, "现金充值"),
	/** 赠送充值 */
	FREE_RECHARGE((short) 12, "红包充值"),
	/** 转账充值 */
	TRANSFER_RECHARGE((short) 13, "转账充值"),
	/** 退款充值 */
	REFUND_RECHARGE((short) 14, "退款充值"),
	/** 提现 */
	// WITHDRAW((short) 15, "提现"),
	/** 设备消费 */
	DEVICE_CONSUME((short) 21, "设备续期消费"),
	/** 空间消费 */
	SPACE_CONSUME((short) 22, "空间扩展消费");

	/** 属性相关 */
	/** 交易类型值 */
	private short value = 0;
	/** 交易类型描述 */
	private String description = null;

	/**
	 * 构造函数
	 * 
	 * @param value 交易类型值
	 * @param description 交易类型描述
	 */
	private CTradeType(short value, String description) {
		this.value = value;
		this.description = description;
	}

	/**
	 * 获取交易类型值
	 * 
	 * @return 交易类型值
	 */
	public short getValue() {
		return value;
	}

	/**
	 * 获取交易类型描述
	 * 
	 * @return 交易类型描述
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 获取交易类型描述
	 * 
	 * @param value 交易类型值
	 * @return 交易类型描述
	 */
	public static String getDescription(Short value) {
		if (value != null) {
			for (CTradeType field : values()) {
				if (value.equals(field.value)) {
					return field.description;
				}
			}
		}
		return null;
	}

	/**
	 * 根据值获取交易类型
	 * 
	 * @param value 交易类型值
	 * @return 交易类型
	 */
	public static CTradeType fromValue(Short value) {
		if (value != null) {
			for (CTradeType field : values()) {
				if (value.equals(field.value)) {
					return field;
				}
			}
		}
		return null;
	}

	/**
	 * 包含交易类型值
	 * 
	 * @param value 交易类型值
	 * @return 是否包含
	 */
	public static boolean contains(Short value) {
		if (value != null) {
			for (CTradeType field : values()) {
				if (value.equals(field.value)) {
					return true;
				}
			}
		}
		return false;
	}

}
