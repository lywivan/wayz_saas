package com.wayz.constants;

/**
 * 菜单状态枚举
 * 
 * @author wade.liu@wayz.ai
 *
 */
public enum CMenuStatus {

	/** 字段相关 */
	/** 隐藏 */
	HIDDEN((short) 0, "隐藏"),
	/** 显示 */
	SHOWED((short) 1, "显示");

	/** 属性相关 */
	/** 菜单状态值 */
	private short value = 0;
	/** 菜单状态描述 */
	private String description = null;

	/**
	 * 构造函数
	 * 
	 * @param value 菜单状态值
	 * @param description 菜单状态描述
	 */
	private CMenuStatus(short value, String description) {
		this.value = value;
		this.description = description;
	}

	/**
	 * 获取菜单状态值
	 * 
	 * @return 菜单状态值
	 */
	public short getValue() {
		return value;
	}

	/**
	 * 获取菜单状态描述
	 * 
	 * @return 菜单状态描述
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 获取菜单状态描述
	 * 
	 * @param value 菜单状态值
	 * @return 菜单状态描述
	 */
	public static String getDescription(Short value) {
		if (value != null) {
			for (CMenuStatus field : values()) {
				if (value.equals(field.value)) {
					return field.description;
				}
			}
		}
		return null;
	}

	/**
	 * 根据值获取菜单状态
	 * 
	 * @param value 菜单状态值
	 * @return 菜单状态
	 */
	public static CMenuStatus fromValue(Short value) {
		if (value != null) {
			for (CMenuStatus field : values()) {
				if (value.equals(field.value)) {
					return field;
				}
			}
		}
		return null;
	}

	/**
	 * 包含菜单状态值
	 * 
	 * @param value 菜单状态值
	 * @return 是否包含
	 */
	public static boolean contains(Short value) {
		if (value != null) {
			for (CMenuStatus field : values()) {
				if (value.equals(field.value)) {
					return true;
				}
			}
		}
		return false;
	}

}
