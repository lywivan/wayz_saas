package com.wayz.constants;

import java.io.Serializable;

/**
 * 视图通用键值类
 * 
 * @author wade.liu@wayz.ai
 *
 */
public class ViewKeyValue implements Serializable {
	/** 标识 */
	private Short id = null;
	/** 名称 */
	private String name = null;

	/**
	 * 获取标识
	 * 
	 * @return 标识
	 */
	public Short getId() {
		return id;
	}

	/**
	 * 设置标识
	 * 
	 * @param id 标识
	 */
	public void setId(Short id) {
		this.id = id;
	}

	/**
	 * 获取名称
	 * 
	 * @return 名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置名称
	 * 
	 * @param name 名称
	 */
	public void setName(String name) {
		this.name = name;
	}

}
