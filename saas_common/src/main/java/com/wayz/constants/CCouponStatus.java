package com.wayz.constants;

/**
 * 代金券状态枚举
 * 
 * @author wade.liu@wayz.ai
 *
 */
public enum CCouponStatus {

	/** 字段相关 */
	/** 已失效 */
	INVALID((short) 0, "已失效"),
	/** 可用 */
	USABLE((short) 1, "可用"),
	/** 已使用 */
	USED((short) 2, "已使用");

	/** 属性相关 */
	/** 代金券状态值 */
	private short value = 0;
	/** 代金券状态描述 */
	private String description = null;

	/**
	 * 构造函数
	 * 
	 * @param value 代金券状态值
	 * @param description 代金券状态描述
	 */
	private CCouponStatus(short value, String description) {
		this.value = value;
		this.description = description;
	}

	/**
	 * 获取代金券状态值
	 * 
	 * @return 代金券状态值
	 */
	public short getValue() {
		return value;
	}

	/**
	 * 获取代金券状态描述
	 * 
	 * @return 代金券状态描述
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 获取代金券状态描述
	 * 
	 * @param value 代金券状态值
	 * @return 代金券状态描述
	 */
	public static String getDescription(Short value) {
		if (value != null) {
			for (CCouponStatus field : values()) {
				if (value.equals(field.value)) {
					return field.description;
				}
			}
		}
		return null;
	}

	/**
	 * 根据值获取代金券状态
	 * 
	 * @param value 代金券状态值
	 * @return 代金券状态
	 */
	public static CCouponStatus fromValue(Short value) {
		if (value != null) {
			for (CCouponStatus field : values()) {
				if (value.equals(field.value)) {
					return field;
				}
			}
		}
		return null;
	}

	/**
	 * 包含代金券状态值
	 * 
	 * @param value 代金券状态值
	 * @return 是否包含
	 */
	public static boolean contains(Short value) {
		if (value != null) {
			for (CCouponStatus field : values()) {
				if (value.equals(field.value)) {
					return true;
				}
			}
		}
		return false;
	}

}
