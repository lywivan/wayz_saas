package com.wayz.constants;

/**
 * 任务类型枚举
 * 
 * @author wade.liu@wayz.ai
 *
 */
public enum CTaskType {

	/** 字段相关 */
	/** 素材下载 */
	DOWNLOAD_SCREEN_MATERIAL((short) 1, "素材下载任务"),
	/** 节目更新 */
	UPDATE_MENU((short) 2, "节目更新"),
	/** 紧急插播更新 */
	UPDATE_EMERGENT((short) 3, "紧急插播更新"),
	/** 远程执行命令 */
	REMOTE_COMMAND((short) 4, "远程执行命令"),
	/** 播放广告计划 */
	PLAY_PLAN((short) 6, "播放指定广告"),
	/** 开启实时直播流 */
	START_LIVE((short) 7, "开始视频直播"),
	/** 结束实时直播流 */
	STOP_LIVE((short) 8, "结束视频直播"),
	/** 素材下载任务 */
	MATERIAL_DOWNLOAD((short) 9, "素材下载进度"),
	/** 开启直播广告 */
	START_LIVE_PLAN((short) 10, "开始直播广告"),
	/** 结束直播广告 */
	STOP_LIVE_PLAN((short) 11, "结束直播广告");

	/** 属性相关 */
	/** 任务类型值 */
	private short value = 0;
	/** 任务类型描述 */
	private String description = null;

	/**
	 * 构造函数
	 * 
	 * @param value 任务类型值
	 * @param description 任务类型描述
	 */
	private CTaskType(short value, String description) {
		this.value = value;
		this.description = description;
	}

	/**
	 * 获取任务类型值
	 * 
	 * @return 任务类型值
	 */
	public short getValue() {
		return value;
	}

	/**
	 * 获取任务类型描述
	 * 
	 * @return 任务类型描述
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 获取任务类型描述
	 * 
	 * @param value 任务类型值
	 * @return 任务类型描述
	 */
	public static String getDescription(Short value) {
		if (value != null) {
			for (CTaskType field : values()) {
				if (value.equals(field.value)) {
					return field.description;
				}
			}
		}
		return null;
	}

	/**
	 * 根据值获取任务类型
	 * 
	 * @param value 任务类型值
	 * @return 任务类型
	 */
	public static CTaskType fromValue(Short value) {
		if (value != null) {
			for (CTaskType field : values()) {
				if (value.equals(field.value)) {
					return field;
				}
			}
		}
		return null;
	}

	/**
	 * 包含任务类型值
	 * 
	 * @param value 任务类型值
	 * @return 是否包含
	 */
	public static boolean contains(Short value) {
		if (value != null) {
			for (CTaskType field : values()) {
				if (value.equals(field.value)) {
					return true;
				}
			}
		}
		return false;
	}

}
