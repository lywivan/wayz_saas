package com.wayz.constants;

/**
 * 广告素材类型枚举
 * 
 * @author wade.liu@wayz.ai
 *
 */
public enum CPlanMaterialType {

	/** 字段相关 */
	/** 其他 */
	OTHER((short) 0, "其他"),
	/** 图片 */
	IMAGE((short) 1, "图片"),
	/** 视频 */
	VIDEO((short) 2, "视频"),
	/** 音频 */
	AUDIO((short) 3, "音频"),
	/** 网页 */
	WEBVIEW((short) 4, "网页"),
	/** office文档 */
	OFFICE((short) 5, "office文档"),
	/** 直播流 */
	LIVE_STREAM((short) 6, "直播流"),
	/** 文本 */
	TEXT((short) 7, "文本");


	/** 属性相关 */
	/** 广告素材类型值 */
	private short value = 0;
	/** 广告素材类型描述 */
	private String description = null;

	/**
	 * 构造函数
	 * 
	 * @param value 广告素材类型值
	 * @param description 广告素材类型描述
	 */
	private CPlanMaterialType(short value, String description) {
		this.value = value;
		this.description = description;
	}

	/**
	 * 获取广告素材类型值
	 * 
	 * @return 广告素材类型值
	 */
	public short getValue() {
		return value;
	}

	/**
	 * 获取广告素材类型描述
	 * 
	 * @return 广告素材类型描述
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 获取广告素材类型描述
	 * 
	 * @param value 广告素材类型值
	 * @return 广告素材类型描述
	 */
	public static String getDescription(Short value) {
		if (value != null) {
			for (CPlanMaterialType field : values()) {
				if (value.equals(field.value)) {
					return field.description;
				}
			}
		}
		return null;
	}

	/**
	 * 根据值获取广告素材类型
	 * 
	 * @param value 广告素材类型值
	 * @return 广告素材类型
	 */
	public static CPlanMaterialType fromValue(Short value) {
		if (value != null) {
			for (CPlanMaterialType field : values()) {
				if (value.equals(field.value)) {
					return field;
				}
			}
		}
		return null;
	}

	/**
	 * 包含广告素材类型值
	 * 
	 * @param value 广告素材类型值
	 * @return 是否包含
	 */
	public static boolean contains(Short value) {
		if (value != null) {
			for (CPlanMaterialType field : values()) {
				if (value.equals(field.value)) {
					return true;
				}
			}
		}
		return false;
	}

}
