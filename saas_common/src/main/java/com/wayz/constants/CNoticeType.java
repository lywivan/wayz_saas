package com.wayz.constants;

/**
 * @author mike.ma@wayz.ai
 */
public enum CNoticeType {

    /** 创建中 */
    CREATEMEDIUM((short) 1, "创建中"),
    /** 创建中 */
    PUSHIN((short) 2, "已推送");


    /** 属性相关 */
    /** 消息类型值 */
    private short value = 0;
    /** 消息类型描述 */
    private String description = null;

    /**
     * 构造函数
     *
     * @param value 广告类型值
     * @param description 广告类型描述
     */
    private CNoticeType(short value, String description) {
        this.value = value;
        this.description = description;
    }

    /**
     * 获取广告类型值
     *
     * @return 广告类型值
     */
    public short getValue() {
        return value;
    }

    /**
     * 获取广告类型描述
     *
     * @return 广告类型描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 获取广告类型描述
     *
     * @param value 广告类型值
     * @return 广告类型描述
     */
    public static String getDescription(Short value) {
        if (value != null) {
            for (CNoticeType field : values()) {
                if (value.equals(field.value)) {
                    return field.description;
                }
            }
        }
        return null;
    }

    /**
     * 根据值获取广告类型
     *
     * @param value 广告类型值
     * @return 广告类型
     */
    public static CNoticeType fromValue(Short value) {
        if (value != null) {
            for (CNoticeType field : values()) {
                if (value.equals(field.value)) {
                    return field;
                }
            }
        }
        return null;
    }

    /**
     * 包含广告类型值
     *
     * @param value 广告类型值
     * @return 是否包含
     */
    public static boolean contains(Short value) {
        if (value != null) {
            for (CNoticeType field : values()) {
                if (value.equals(field.value)) {
                    return true;
                }
            }
        }
        return false;
    }
}
