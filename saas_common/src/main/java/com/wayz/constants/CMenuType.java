package com.wayz.constants;

/**
 * @className: CMenuType
 * @description: 菜单类型枚举类
 * <功能详细描述>
 * @author wade.liu@wayz.ai
 * @date: 2019/6/20
 */
public enum CMenuType {

    /** 字段相关 */
    /** 隐藏 */
    MENU((short) 0, "菜单"),
    /** 显示 */
    SHOWED((short) 1, "权限");

    /** 属性相关 */
    /** 菜单类型值 */
    private short value = 0;
    /** 菜单类型描述 */
    private String description = null;

    /**
     * 构造函数
     *
     * @param value 菜单类型值
     * @param description 菜单类型描述
     */
    CMenuType(short value, String description) {
        this.value = value;
        this.description = description;
    }

    /**
     * 获取菜单状态值
     *
     * @return 菜单状态值
     */
    public short getValue() {
        return value;
    }

    /**
     * 获取菜单状态描述
     *
     * @return 菜单状态描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 获取菜单状态描述
     *
     * @param value 菜单状态值
     * @return 菜单状态描述
     */
    public static String getDescription(Short value) {
        if (value != null) {
            for (CMenuType field : values()) {
                if (value.equals(field.value)) {
                    return field.description;
                }
            }
        }
        return null;
    }

    /**
     * 根据值获取菜单状态
     *
     * @param value 菜单状态值
     * @return 菜单状态
     */
    public static CMenuType fromValue(Short value) {
        if (value != null) {
            for (CMenuType field : values()) {
                if (value.equals(field.value)) {
                    return field;
                }
            }
        }
        return null;
    }

    /**
     * 包含菜单状态值
     *
     * @param value 菜单状态值
     * @return 是否包含
     */
    public static boolean contains(Short value) {
        if (value != null) {
            for (CMenuType field : values()) {
                if (value.equals(field.value)) {
                    return true;
                }
            }
        }
        return false;
    }


}
