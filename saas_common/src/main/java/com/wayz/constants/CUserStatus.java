package com.wayz.constants;

/**
 * 用户状态枚举
 * 
 * @author yinjy
 *
 */
public enum CUserStatus {

	/** 字段相关 */
	/** 禁用 */
	DISABLE((short) 0, "禁用"),
	/** 启用 */
	ENABLE((short) 1, "启用");

	/** 属性相关 */
	/** 用户状态值 */
	private short value = 0;
	/** 用户状态描述 */
	private String description = null;

	/**
	 * 构造函数
	 * 
	 * @param value 用户状态值
	 * @param description 用户状态描述
	 */
	private CUserStatus(short value, String description) {
		this.value = value;
		this.description = description;
	}

	/**
	 * 获取用户状态值
	 * 
	 * @return 用户状态值
	 */
	public short getValue() {
		return value;
	}

	/**
	 * 获取用户状态描述
	 * 
	 * @return 用户状态描述
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 获取用户状态描述
	 * 
	 * @param value 用户状态值
	 * @return 用户状态描述
	 */
	public static String getDescription(Short value) {
		if (value != null) {
			for (CUserStatus field : values()) {
				if (value.equals(field.value)) {
					return field.description;
				}
			}
		}
		return null;
	}

	/**
	 * 根据值获取用户状态
	 * 
	 * @param value 用户状态值
	 * @return 用户状态
	 */
	public static CUserStatus fromValue(Short value) {
		if (value != null) {
			for (CUserStatus field : values()) {
				if (value.equals(field.value)) {
					return field;
				}
			}
		}
		return null;
	}

	/**
	 * 包含用户状态值
	 * 
	 * @param value 用户状态值
	 * @return 是否包含
	 */
	public static boolean contains(Short value) {
		if (value != null) {
			for (CUserStatus field : values()) {
				if (value.equals(field.value)) {
					return true;
				}
			}
		}
		return false;
	}

}
