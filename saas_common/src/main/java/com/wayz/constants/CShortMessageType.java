package com.wayz.constants;

/**
 * 短信类型类
 * 
 * @author wade.liu@wayz.ai
 *
 */
public class CShortMessageType {

	/** 注册用户 */
	public static final short REGISTER = 1;
	/** 找回密码 */
	public static final short RETRIEVE = 2;
	/** 绑定手机 */
	public static final short BIND_PHONE = 3;
	/** 解绑手机 */
	public static final short UNBIND_PHONE = 4;
	/** 设备到期预警 */
	public static final short POSITION_EXPIRE = 5;

	/**
	 * 获取短信类型描述
	 * 
	 * @param shortMessageType 短信类型值
	 * @return 短信类型描述
	 */
	public static String getDescription(short shortMessageType) {
		if (shortMessageType == REGISTER) {
			return "注册用户";
		}
		if (shortMessageType == RETRIEVE) {
			return "找回密码";
		}
		if (shortMessageType == BIND_PHONE) {
			return "绑定手机";
		}
		if (shortMessageType == UNBIND_PHONE) {
			return "解绑手机";
		}
		if (shortMessageType == POSITION_EXPIRE) {
			return "设备到期预警";
		}

		return "";
	}

}
