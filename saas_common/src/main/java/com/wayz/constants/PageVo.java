/*
 * @date 2017年8月23日
 * @version V1.0
 */
package com.wayz.constants;

import java.io.Serializable;
import java.util.List;

/**
 * <p>分页公共类</p>
 * 
 * @date 2017年8月23日
 * @author mahao
 */
public class PageVo<T> implements Serializable {

	/**
	 * @Fields serialVersionUID
	 */

	private static final long serialVersionUID = 1L;
	/** 分页数量 */
	private Integer totalCount = null;
	/** 列表数据 */
	private List<T> list = null;

	/**
	 * @return totalCount
	 */
	public Integer getTotalCount() {
		return totalCount;
	}

	/**
	 * <p>设置 totalCount</p>
	 * 
	 * @param totalCount
	 */
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	/**
	 * @return list
	 */
	public List<T> getList() {
		return list;
	}

	/**
	 * <p>设置 list</p>
	 * 
	 * @param list
	 */
	public void setList(List<T> list) {
		this.list = list;
	}

}
