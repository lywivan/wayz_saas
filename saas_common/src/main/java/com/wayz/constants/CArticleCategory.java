package com.wayz.constants;

/**
 * 新闻分类枚举
 * 
 * @author mike.ma@wayz.ai
 *
 */
public enum CArticleCategory {

	/** 字段相关 */
	/** 媒体报道 */
	OOHLINK_MEDIA((short) 0, "奥凌资讯"),
	/** 行业动态 */
	INDUSTRY_DYNAMICS((short) 1, "行业动态"),
	/** 最新活动 */
	NEW_ACTIVITY((short) 2, "最新活动");

	/** 属性相关 */
	/** 新闻分类值 */
	private short value = 0;
	/** 新闻分类描述 */
	private String description = null;

	/**
	 * 构造函数
	 * 
	 * @param value 新闻分类值
	 * @param description 新闻分类描述
	 */
	private CArticleCategory(short value, String description) {
		this.value = value;
		this.description = description;
	}

	/**
	 * 获取新闻分类值
	 * 
	 * @return 新闻分类值
	 */
	public short getValue() {
		return value;
	}

	/**
	 * 获取新闻分类描述
	 * 
	 * @return 新闻分类描述
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 获取新闻分类描述
	 * 
	 * @param value 新闻分类值
	 * @return 新闻分类描述
	 */
	public static String getDescription(Short value) {
		if (value != null) {
			for (CArticleCategory field : values()) {
				if (value.equals(field.value)) {
					return field.description;
				}
			}
		}
		return null;
	}

	/**
	 * 根据值获取新闻分类
	 * 
	 * @param value 新闻分类值
	 * @return 新闻分类
	 */
	public static CArticleCategory fromValue(Short value) {
		if (value != null) {
			for (CArticleCategory field : values()) {
				if (value.equals(field.value)) {
					return field;
				}
			}
		}
		return null;
	}

	/**
	 * 包含新闻分类值
	 * 
	 * @param value 新闻分类值
	 * @return 是否包含
	 */
	public static boolean contains(Short value) {
		if (value != null) {
			for (CArticleCategory field : values()) {
				if (value.equals(field.value)) {
					return true;
				}
			}
		}
		return false;
	}

}
