package com.wayz.constants;

/**
 * 上传文件状态枚举
 * 
 * @author yinjy
 *
 */
public enum CFileStatus {

	/** 字段相关 */
	/** 上传中 */
	UPLOADING((short) 1, "上传中"),
	/** 上传成功 */
	SUCCESS((short) 2, "上传成功"),
	/** 上传失败 */
	FAILURE((short) 3, "上传失败");

	/** 属性相关 */
	/** 上传文件状态值 */
	private short value = 0;
	/** 上传文件状态描述 */
	private String description = null;

	/**
	 * 构造函数
	 * 
	 * @param value 上传文件状态值
	 * @param description 上传文件状态描述
	 */
	private CFileStatus(short value, String description) {
		this.value = value;
		this.description = description;
	}

	/**
	 * 获取上传文件状态值
	 * 
	 * @return 上传文件状态值
	 */
	public short getValue() {
		return value;
	}

	/**
	 * 获取上传文件状态描述
	 * 
	 * @return 上传文件状态描述
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 获取上传文件状态描述
	 * 
	 * @param value 上传文件状态值
	 * @return 上传文件状态描述
	 */
	public static String getDescription(Short value) {
		if (value != null) {
			for (CFileStatus field : values()) {
				if (value.equals(field.value)) {
					return field.description;
				}
			}
		}
		return null;
	}

	/**
	 * 根据值获取上传文件状态
	 * 
	 * @param value 上传文件状态值
	 * @return 上传文件状态
	 */
	public static CFileStatus fromValue(Short value) {
		if (value != null) {
			for (CFileStatus field : values()) {
				if (value.equals(field.value)) {
					return field;
				}
			}
		}
		return null;
	}

	/**
	 * 包含上传文件状态值
	 * 
	 * @param value 上传文件状态值
	 * @return 是否包含
	 */
	public static boolean contains(Short value) {
		if (value != null) {
			for (CFileStatus field : values()) {
				if (value.equals(field.value)) {
					return true;
				}
			}
		}
		return false;
	}

}
