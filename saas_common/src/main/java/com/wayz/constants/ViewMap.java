package com.wayz.constants;

import java.io.Serializable;

/**
 * 视图通用map类
 * 
 * @author wade.liu@wayz.ai
 *
 */
public class ViewMap implements Serializable {
	/** 标识 */
	private Long id = null;
	/** 名称 */
	private String name = null;
	/** 用户数限制 */
	private Integer userLimit = null;
	/** 素材空间限制 */
	private Long materialLimit = null;

	/**
	 * 获取标识
	 * 
	 * @return 标识
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 设置标识
	 * 
	 * @param id 标识
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 获取名称
	 * 
	 * @return 名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置名称
	 * 
	 * @param name 名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	public Integer getUserLimit() {
		return userLimit;
	}

	public void setUserLimit(Integer userLimit) {
		this.userLimit = userLimit;
	}

	public Long getMaterialLimit() {
		return materialLimit;
	}

	public void setMaterialLimit(Long materialLimit) {
		this.materialLimit = materialLimit;
	}
}
