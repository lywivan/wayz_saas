package com.wayz.constants;

/**
 * 审核枚举类
 * @author wade.liu@wayz.ai
 * @Date: 2019/12/6 下午5:13
 */
public enum CAuditMediaStatus {
    /** 字段相关 */
    /** 未提交审核 */
    NOT_AUDIT((short) 0, "未提交审核"),
    /** 待审核 */
    PENDING_AUDIT((short) 1, "待审核"),
    /** 审核通过 */
    PASS_AUDIT((short) 2, "审核通过"),
    /** 审核未通过 */
    NOPASS_AUDIT((short) 3, "审核未通过");

    /** 属性相关 */
    /** 审核状态值 */
    private short value = 0;
    /** 审核状态描述 */
    private String description = null;

    /**
     * 构造函数
     *
     * @param value 审核状态值
     * @param description 审核状态描述
     */
    private CAuditMediaStatus(short value, String description) {
        this.value = value;
        this.description = description;
    }

    /**
     * 获取审核状态值
     *
     * @return 审核状态值
     */
    public short getValue() {
        return value;
    }

    /**
     * 获取审核状态描述
     *
     * @return 审核状态描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 获取审核状态描述
     *
     * @param value 审核状态值
     * @return 审核状态描述
     */
    public static String getDescription(Short value) {
        if (value != null) {
            for (CAuditMediaStatus field : values()) {
                if (value.equals(field.value)) {
                    return field.description;
                }
            }
        }
        return null;
    }

    /**
     * 根据值获取审核状态
     *
     * @param value 审核状态值
     * @return 审核状态
     */
    public static CAuditMediaStatus fromValue(Short value) {
        if (value != null) {
            for (CAuditMediaStatus field : values()) {
                if (value.equals(field.value)) {
                    return field;
                }
            }
        }
        return null;
    }

    /**
     * 包含审核状态值
     *
     * @param value 审核状态值
     * @return 是否包含
     */
    public static boolean contains(Short value) {
        if (value != null) {
            for (CAuditMediaStatus field : values()) {
                if (value.equals(field.value)) {
                    return true;
                }
            }
        }
        return false;
    }
}
