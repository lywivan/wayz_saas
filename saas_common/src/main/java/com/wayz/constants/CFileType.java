package com.wayz.constants;

/**
 * 文件类型枚举
 * 
 * @author yinjy
 *
 */
public enum CFileType {

	/** 字段相关 */
	/** 其他文件 */
	OTHER((short) 0, "其他文件"),
	/** 用户头像 */
	AVATAR((short) 1, "用户头像"),
	/** 媒体位图片 */
	POSITION((short) 2, "媒体位图片"),
	/** 广告素材 */
	MATERIAL((short) 3, "广告素材"),
	/** 数据报告 */
	REPORT((short) 4, "数据报告"),
	/** 公司资料 */
	COMPANY_INFO((short) 5, "公司资料"),
	/** 效果监播 */
	PLAN_EFFECT((short) 6, "效果监播"),
	/** 截屏日志 */
	SNAP_SHOT((short) 7, "截屏日志");

	/** 属性相关 */
	/** 文件类型值 */
	private short value = 0;
	/** 文件类型描述 */
	private String description = null;

	/**
	 * 构造函数
	 * 
	 * @param value 文件类型值
	 * @param description 文件类型描述
	 */
	private CFileType(short value, String description) {
		this.value = value;
		this.description = description;
	}

	/**
	 * 获取文件类型值
	 * 
	 * @return 文件类型值
	 */
	public short getValue() {
		return value;
	}

	/**
	 * 获取文件类型描述
	 * 
	 * @return 文件类型描述
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 获取文件类型描述
	 * 
	 * @param value 文件类型值
	 * @return 文件类型描述
	 */
	public static String getDescription(Short value) {
		if (value != null) {
			for (CFileType field : values()) {
				if (value.equals(field.value)) {
					return field.description;
				}
			}
		}
		return null;
	}

	/**
	 * 获取文件存放目录
	 * 
	 * @param value 文件类型值
	 * @return 文件存放目录
	 */
	public static String getDirectory(Short value) {
		if (value != null) {
			if (value.equals(AVATAR.getValue())) {
				return "avatar";
			}
			else if (value.equals(POSITION.getValue())) {
				return "position";
			}
			else if (value.equals(MATERIAL.getValue())) {
				return "material";
			}
			else if (value.equals(REPORT.getValue())) {
				return "report";
			}
			else if (value.equals(COMPANY_INFO.getValue())) {
				return "companyInfo";
			}
			else if (value.equals(PLAN_EFFECT.getValue())) {
				return "planEffect";
			}
			else {
				return "other";
			}
		}
		return null;
	}

	/**
	 * 根据值获取文件类型
	 * 
	 * @param value 文件类型值
	 * @return 文件类型
	 */
	public static CFileType fromValue(Short value) {
		if (value != null) {
			for (CFileType field : values()) {
				if (value.equals(field.value)) {
					return field;
				}
			}
		}
		return null;
	}

	/**
	 * 包含文件类型值
	 * 
	 * @param value 文件类型值
	 * @return 是否包含
	 */
	public static boolean contains(Short value) {
		if (value != null) {
			for (CFileType field : values()) {
				if (value.equals(field.value)) {
					return true;
				}
			}
		}
		return false;
	}

}
