package com.wayz.constants;

/**
 * @className: CRoleType
 * @description: 角色类型 <功能详细描述>
 * @author wade.liu@wayz.ai
 * @date: 2019/6/13
 */
public enum CRoleType {

	/** 版本管理员 */
	VERSION_ADMIN((short) 0, "版本管理员"),
	/** 租户自建角色 */
	TENEMENT_APPENDAGE((short) 1, "租户自建");

	private short value;
	private String description;

	public short getValue() {
		return value;
	}

	public void setValue(short value) {
		this.value = value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 获取文件类型描述
	 * 
	 * @param value 文件类型值
	 * @return 文件类型描述
	 */
	public static String getDescription(Short value) {
		if (value != null) {
			for (CRoleType field : values()) {
				if (value.equals(field.value)) {
					return field.description;
				}
			}
		}
		return null;
	}

	/**
	 * 根据值获取文件类型
	 * 
	 * @param value 文件类型值
	 * @return 文件类型
	 */
	public static CRoleType fromValue(Short value) {
		if (value != null) {
			for (CRoleType field : values()) {
				if (value.equals(field.value)) {
					return field;
				}
			}
		}
		return null;
	}

	/**
	 * 包含文件类型值
	 * 
	 * @param value 文件类型值
	 * @return 是否包含
	 */
	public static boolean contains(Short value) {
		if (value != null) {
			for (CRoleType field : values()) {
				if (value.equals(field.value)) {
					return true;
				}
			}
		}
		return false;
	}

	CRoleType(short value, String description) {
		this.value = value;
		this.description = description;
	}
}
