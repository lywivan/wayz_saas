package com.wayz.constants;

/**
 * 公司状态枚举
 * 
 * @author wade.liu@wayz.ai
 *
 */
public enum CCompanyStatus {

	/** 字段相关 */
	/** 禁用 */
	DISABLE((short) 0, "禁用"),
	/** 正常 */
	ENABLE((short) 1, "正常"),
	/** 试用中 */
	TESTING((short) 2, "试用中"),
	/** 已过期 */
	EXPIRED((short) 3, "已过期");

	/** 属性相关 */
	/** 公司状态值 */
	private short value = 0;
	/** 公司状态描述 */
	private String description = null;

	/**
	 * 构造函数
	 * 
	 * @param value 公司状态值
	 * @param description 公司状态描述
	 */
	private CCompanyStatus(short value, String description) {
		this.value = value;
		this.description = description;
	}

	/**
	 * 获取公司状态值
	 * 
	 * @return 公司状态值
	 */
	public short getValue() {
		return value;
	}

	/**
	 * 获取公司状态描述
	 * 
	 * @return 公司状态描述
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 获取公司状态描述
	 * 
	 * @param value 公司状态值
	 * @return 公司状态描述
	 */
	public static String getDescription(Short value) {
		if (value != null) {
			for (CCompanyStatus field : values()) {
				if (value.equals(field.value)) {
					return field.description;
				}
			}
		}
		return null;
	}

	/**
	 * 根据值获取公司状态
	 * 
	 * @param value 公司状态值
	 * @return 公司状态
	 */
	public static CCompanyStatus fromValue(Short value) {
		if (value != null) {
			for (CCompanyStatus field : values()) {
				if (value.equals(field.value)) {
					return field;
				}
			}
		}
		return null;
	}

	/**
	 * 包含公司状态值
	 * 
	 * @param value 公司状态值
	 * @return 是否包含
	 */
	public static boolean contains(Short value) {
		if (value != null) {
			for (CCompanyStatus field : values()) {
				if (value.equals(field.value)) {
					return true;
				}
			}
		}
		return false;
	}

}
