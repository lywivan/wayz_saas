package com.wayz.constants;

/**
 * 媒体状态枚举
 * 
 * @author wade.liu@wayz.ai
 *
 */
public enum CPositionStatus {

	/** 字段相关 */
	/** 未开通 */
	UNOPEN((short) 0, "未开通"),
	/** 正常 */
	NORMAL((short) 1, "正常"),
	/** 过期 */
	EXPIRED((short) 2, "过期");

	/** 属性相关 */
	/** 媒体状态值 */
	private short value = 0;
	/** 媒体状态描述 */
	private String description = null;

	/**
	 * 构造函数
	 * 
	 * @param value 媒体状态值
	 * @param description 媒体状态描述
	 */
	private CPositionStatus(short value, String description) {
		this.value = value;
		this.description = description;
	}

	/**
	 * 获取媒体状态值
	 * 
	 * @return 媒体状态值
	 */
	public short getValue() {
		return value;
	}

	/**
	 * 获取媒体状态描述
	 * 
	 * @return 媒体状态描述
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 获取媒体状态描述
	 * 
	 * @param value 媒体状态值
	 * @return 媒体状态描述
	 */
	public static String getDescription(Short value) {
		if (value != null) {
			for (CPositionStatus field : values()) {
				if (value.equals(field.value)) {
					return field.description;
				}
			}
		}
		return null;
	}

	/**
	 * 根据值获取媒体状态
	 * 
	 * @param value 媒体状态值
	 * @return 媒体状态
	 */
	public static CPositionStatus fromValue(Short value) {
		if (value != null) {
			for (CPositionStatus field : values()) {
				if (value.equals(field.value)) {
					return field;
				}
			}
		}
		return null;
	}

	/**
	 * 包含媒体状态值
	 * 
	 * @param value 媒体状态值
	 * @return 是否包含
	 */
	public static boolean contains(Short value) {
		if (value != null) {
			for (CPositionStatus field : values()) {
				if (value.equals(field.value)) {
					return true;
				}
			}
		}
		return false;
	}

}
