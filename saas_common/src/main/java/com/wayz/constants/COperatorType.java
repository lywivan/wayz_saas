package com.wayz.constants;

/**
 * 操作员类型枚举
 * 
 * @author wade.liu@wayz.ai
 *
 */
public enum COperatorType {

	/** 字段相关 */
	/** 平台管理员 */
	ADMIN((short) 1, "平台管理员"),
	/** 代理 */
	PROXY((short) 2, "代理"),
	/** 租户 */
	TENANT((short) 3, "租户");

	/** 属性相关 */
	/** 操作员类型值 */
	private short value = 0;
	/** 操作员类型描述 */
	private String description = null;

	/**
	 * 构造函数
	 * 
	 * @param value 操作员类型值
	 * @param description 操作员类型描述
	 */
	private COperatorType(short value, String description) {
		this.value = value;
		this.description = description;
	}

	/**
	 * 获取操作员类型值
	 * 
	 * @return 操作员类型值
	 */
	public short getValue() {
		return value;
	}

	/**
	 * 获取操作员类型描述
	 * 
	 * @return 操作员类型描述
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 获取操作员类型描述
	 * 
	 * @param value 操作员类型值
	 * @return 操作员类型描述
	 */
	public static String getDescription(Short value) {
		if (value != null) {
			for (COperatorType field : values()) {
				if (value.equals(field.value)) {
					return field.description;
				}
			}
		}
		return null;
	}

	/**
	 * 根据值获取操作员类型
	 * 
	 * @param value 操作员类型值
	 * @return 操作员类型
	 */
	public static COperatorType fromValue(Short value) {
		if (value != null) {
			for (COperatorType field : values()) {
				if (value.equals(field.value)) {
					return field;
				}
			}
		}
		return null;
	}

	/**
	 * 包含操作员类型值
	 * 
	 * @param value 操作员类型值
	 * @return 是否包含
	 */
	public static boolean contains(Short value) {
		if (value != null) {
			for (COperatorType field : values()) {
				if (value.equals(field.value)) {
					return true;
				}
			}
		}
		return false;
	}

}
