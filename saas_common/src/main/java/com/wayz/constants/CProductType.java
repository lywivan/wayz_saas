package com.wayz.constants;

/**
 * 产品类型枚举
 * 
 * @author yinjy
 *
 */
public enum CProductType {

	/** 字段相关 */
	/** 账户充值 */
	RECHARGE((short) 0, "账户充值"),
	/** 点位新购 */
	NEWBUY((short) 1, "点位新购"),
	/** 点位续期 */
	RENEWAL((short) 2, "点位续期"),
	/** 空间 */
	SPACE((short) 3, "空间扩充"),
	/** 直播服务 */
	LIVE((short) 4, "直播服务");

	/** 属性相关 */
	/** 产品类型值 */
	private short value = 0;
	/** 产品类型描述 */
	private String description = null;

	/**
	 * 构造函数
	 * 
	 * @param value 产品类型值
	 * @param description 产品类型描述
	 */
	private CProductType(short value, String description) {
		this.value = value;
		this.description = description;
	}

	/**
	 * 获取产品类型值
	 * 
	 * @return 产品类型值
	 */
	public short getValue() {
		return value;
	}

	/**
	 * 获取产品类型描述
	 * 
	 * @return 产品类型描述
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 获取产品类型描述
	 * 
	 * @param value 产品类型值
	 * @return 产品类型描述
	 */
	public static String getDescription(Short value) {
		if (value != null) {
			for (CProductType field : values()) {
				if (value.equals(field.value)) {
					return field.description;
				}
			}
		}
		return null;
	}

	/**
	 * 根据值获取产品类型
	 * 
	 * @param value 产品类型值
	 * @return 产品类型
	 */
	public static CProductType fromValue(Short value) {
		if (value != null) {
			for (CProductType field : values()) {
				if (value.equals(field.value)) {
					return field;
				}
			}
		}
		return null;
	}

	/**
	 * 包含产品类型值
	 * 
	 * @param value 产品类型值
	 * @return 是否包含
	 */
	public static boolean contains(Short value) {
		if (value != null) {
			for (CProductType field : values()) {
				if (value.equals(field.value)) {
					return true;
				}
			}
		}
		return false;
	}

}
