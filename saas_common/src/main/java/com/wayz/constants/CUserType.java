package com.wayz.constants;

/**
 * 用户类型枚举
 * 
 * @author wade.liu@wayz.ai
 *
 */
public enum CUserType {

	/** 字段相关 */
	/** saas主账户*/
	MAIN_ACCOUNT((short) 0, "saas主账户"),
	/** saas主账户创建的附属账户*/
	SUBACCOUNTS((short) 1, "saas附属账户");

	/** 属性相关 */
	/** 用户类型值 */
	private short value = 0;
	/** 用户类型描述 */
	private String description = null;

	/**
	 * 构造函数
	 *
	 * @param value 用户类型值
	 * @param description 用户类型描述
	 */
	private CUserType(short value, String description) {
		this.value = value;
		this.description = description;
	}

	/**
	 * 获取用户类型值
	 * 
	 * @return 用户类型值
	 */
	public short getValue() {
		return value;
	}

	/**
	 * 获取用户类型描述
	 * 
	 * @return 用户类型描述
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 获取用户类型描述
	 * 
	 * @param value 用户类型值
	 * @return 用户类型描述
	 */
	public static String getDescription(Short value) {
		if (value != null) {
			for (CUserType field : values()) {
				if (value.equals(field.value)) {
					return field.description;
				}
			}
		}
		return null;
	}

	/**
	 * 根据值获取用户类型
	 * 
	 * @param value 用户类型值
	 * @return 用户类型
	 */
	public static CUserType fromValue(Short value) {
		if (value != null) {
			for (CUserType field : values()) {
				if (value.equals(field.value)) {
					return field;
				}
			}
		}
		return null;
	}

	/**
	 * 包含用户类型值
	 * 
	 * @param value 用户类型值
	 * @return 是否包含
	 */
	public static boolean contains(Short value) {
		if (value != null) {
			for (CUserType field : values()) {
				if (value.equals(field.value)) {
					return true;
				}
			}
		}
		return false;
	}

}
