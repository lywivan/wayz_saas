package com.wayz.constants;

/**
 * 表单数据分类枚举
 * 
 * @author mike.ma@wayz.ai
 *
 */
public enum CFormDataCategory {

	/** 字段相关 */
	/** 渠道合作 */
	CHANNEL_COOP((short) 1, "渠道合作"),
	/** 免费试用 */
	FREE_USE((short) 2, "免费试用"),
	/** 申请入驻 */
	APPLY_ENTRY((short) 3, "申请入驻"),
	/** 体验官 */
	EXPERIENCE_OFFICER((short) 4, "体验官");

	/** 属性相关 */
	/** 表单数据分类值 */
	private short value = 0;
	/** 表单数据分类描述 */
	private String description = null;

	/**
	 * 构造函数
	 * 
	 * @param value 表单数据分类值
	 * @param description 表单数据分类描述
	 */
	private CFormDataCategory(short value, String description) {
		this.value = value;
		this.description = description;
	}

	/**
	 * 获取表单数据分类值
	 * 
	 * @return 表单数据分类值
	 */
	public short getValue() {
		return value;
	}

	/**
	 * 获取表单数据分类描述
	 * 
	 * @return 表单数据分类描述
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 获取表单数据分类描述
	 * 
	 * @param value 表单数据分类值
	 * @return 表单数据分类描述
	 */
	public static String getDescription(Short value) {
		if (value != null) {
			for (CFormDataCategory field : values()) {
				if (value.equals(field.value)) {
					return field.description;
				}
			}
		}
		return null;
	}

	/**
	 * 根据值获取表单数据分类
	 * 
	 * @param value 表单数据分类值
	 * @return 表单数据分类
	 */
	public static CFormDataCategory fromValue(Short value) {
		if (value != null) {
			for (CFormDataCategory field : values()) {
				if (value.equals(field.value)) {
					return field;
				}
			}
		}
		return null;
	}

	/**
	 * 包含表单数据分类值
	 * 
	 * @param value 表单数据分类值
	 * @return 是否包含
	 */
	public static boolean contains(Short value) {
		if (value != null) {
			for (CFormDataCategory field : values()) {
				if (value.equals(field.value)) {
					return true;
				}
			}
		}
		return false;
	}

}
