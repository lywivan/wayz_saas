/**
 * xxxx
 *
 * @author:ivan.liu
 */
package com.wayz.constants;

import lombok.Data;

import java.io.Serializable;

@Data
public class PageDto implements Serializable {
    private Integer startIndex;
    private Integer pageSize;
}
