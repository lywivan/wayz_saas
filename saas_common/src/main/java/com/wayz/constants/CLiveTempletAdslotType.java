package com.wayz.constants;

/**
 * 直播模板分区类型枚举
 * 
 * @author gqLiu
 *
 */
public enum CLiveTempletAdslotType {

	/** 商品区 */
	GOODS_AREA((short) 0, "商品区"),
	/** 固定图片区 */
	IMAGE_FIXED((short) 1, "固定图片区"),
	/** 固定文本 */
	TEXT_FIXED((short) 2, "固定文本区"),
	/** 固定网页区 */
	WEB_FIXED((short) 3, "固定网页区"),
	/** 固定直播 */
	LIVE_AREA((short) 4, "直播流区"),
	/** 二维码区 */
	QR_CODE_AREA((short) 8, "二维码区");

	/** 属性相关 */
	/** 直播模板分区类型值 */
	private short value = 0;
	/** 直播模板分区类型描述 */
	private String description = null;

	/**
	 * 构造函数
	 * 
	 * @param value 直播模板分区类型值
	 * @param description 直播模板分区类型描述
	 */
	private CLiveTempletAdslotType(short value, String description) {
		this.value = value;
		this.description = description;
	}

	/**
	 * 获取直播模板分区类型值
	 * 
	 * @return 直播模板分区类型值
	 */
	public short getValue() {
		return value;
	}

	/**
	 * 获取直播模板分区类型描述
	 * 
	 * @return 直播模板分区类型描述
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 获取直播模板分区类型描述
	 * 
	 * @param value 直播模板分区类型值
	 * @return 直播模板分区类型描述
	 */
	public static String getDescription(Short value) {
		if (value != null) {
			for (CLiveTempletAdslotType field : values()) {
				if (value.equals(field.value)) {
					return field.description;
				}
			}
		}
		return null;
	}

	/**
	 * 根据值获取直播模板分区类型
	 * 
	 * @param value 直播模板分区类型值
	 * @return 直播模板分区类型
	 */
	public static CLiveTempletAdslotType fromValue(Short value) {
		if (value != null) {
			for (CLiveTempletAdslotType field : values()) {
				if (value.equals(field.value)) {
					return field;
				}
			}
		}
		return null;
	}

	/**
	 * 包含直播模板分区类型值
	 * 
	 * @param value 直播模板分区类型值
	 * @return 是否包含
	 */
	public static boolean contains(Short value) {
		if (value != null) {
			for (CLiveTempletAdslotType field : values()) {
				if (value.equals(field.value)) {
					return true;
				}
			}
		}
		return false;
	}

}
