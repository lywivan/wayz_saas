package com.wayz.constants;

/**
 * 审核方类型枚举
 * 
 * @author mike.ma@wayz.ai
 *
 */
public enum CAuditorType {

	/** 字段相关 */
	/** OOHLINK */
	OOHLINK((short) 1, "奥凌"),
	/** CIBN */
	CIBN((short) 2, "CIBN"),
	/** SCENE */
	SCENE((short) 3, "场景方"),
	/** 媒体方 */
	MEDIA((short) 4, "媒体方");

	/** 属性相关 */
	/** 审核方类型值 */
	private short value = 0;
	/** 审核方类型描述 */
	private String description = null;

	/**
	 * 构造函数
	 *
	 * @param value 审核方类型值
	 * @param description 审核方类型描述
	 */
	private CAuditorType(short value, String description) {
		this.value = value;
		this.description = description;
	}

	/**
	 * 获取审核方类型值
	 * 
	 * @return 审核方类型值
	 */
	public short getValue() {
		return value;
	}

	/**
	 * 获取审核方类型描述
	 * 
	 * @return 审核方类型描述
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 获取审核方类型描述
	 * 
	 * @param value 审核方类型值
	 * @return 审核方类型描述
	 */
	public static String getDescription(Short value) {
		if (value != null) {
			for (CAuditorType field : values()) {
				if (value.equals(field.value)) {
					return field.description;
				}
			}
		}
		return null;
	}

	/**
	 * 根据值获取审核方类型
	 * 
	 * @param value 审核方类型值
	 * @return 审核方类型
	 */
	public static CAuditorType fromValue(Short value) {
		if (value != null) {
			for (CAuditorType field : values()) {
				if (value.equals(field.value)) {
					return field;
				}
			}
		}
		return null;
	}

	/**
	 * 包含审核方类型值
	 * 
	 * @param value 审核方类型值
	 * @return 是否包含
	 */
	public static boolean contains(Short value) {
		if (value != null) {
			for (CAuditorType field : values()) {
				if (value.equals(field.value)) {
					return true;
				}
			}
		}
		return false;
	}

}
