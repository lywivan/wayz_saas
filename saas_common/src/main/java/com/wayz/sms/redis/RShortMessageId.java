package com.wayz.sms.redis;

/**
 * 短消息标识接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
public interface RShortMessageId {

	/**
	 * 递增短消息标识
	 * 
	 * @return 短消息标识
	 */
	public Long increment();

	/**
	 * 递增短消息标识
	 * 
	 * @param time 指定时间(毫秒)
	 * @return 短消息标识
	 */
	public Long increment(long time);

}
