package com.wayz.mapper;

import com.wayz.sms.entity.DShortMessage;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 短消息DAO接口
 * 
 * @author wade.liu@wayz.ai
 *
 */
@Repository("dShortMessageDAO")
public interface DShortMessageDAO {
	/**
	 * 创建短消息信息
	 *
	 * @param id 消息标识
	 * @param create 短消息创建信息
	 * @return 创建行数
	 */
	public Integer create(@Param("id") Long id, @Param("create") DShortMessage create);

	/**
	 * 修改短消息信息
	 *
	 * @param modify 短消息修改信息
	 * @return 修改行数
	 */
	public Integer modify(@Param("modify") DShortMessage modify);
}
