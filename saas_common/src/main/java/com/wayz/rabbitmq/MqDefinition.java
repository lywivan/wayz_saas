package com.wayz.rabbitmq;

/**
 * rabbitMQ相关定义
 */
public class MqDefinition {
    /**
     * 路由键定义
     */
    public final static String PLAY_HEART_BEAT_ROUTING_KEY = "play.heart.beat.routing";
    /**
     * 队列名定义
     */
    public final static String PLAY_HEART_BEAT_QUEUE = "play.heart.beat.queue";
    /**
     * 交换机名称定义
     */
    public final static String PLAY_EXCHANGE = "play.exchange";
}
