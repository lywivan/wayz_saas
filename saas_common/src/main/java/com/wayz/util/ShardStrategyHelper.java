package com.wayz.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 分表策略辅助类
 * 
 * @author wade.liu@wayz.ai
 *
 */
public class ShardStrategyHelper {

	/** 日期模式 */
	private final static String DATE_PATTERN = "yyyyMMdd";

	/**
	 * 获取标识日期表名
	 * 
	 * @param id 标识
	 * @param baseTableName 基础表名
	 * @return 标识日期表名
	 */
	public static String getIdDateTableName(String baseTableName, Long id) {
		// 检查空值
		if (id == null) {
			return baseTableName + "_" + new SimpleDateFormat(DATE_PATTERN).format(new Date());
		}

		// 转化日期
		String date = Long.toString(id);
		if (date == null || date.length() < 8) {
			return baseTableName + "_" + new SimpleDateFormat(DATE_PATTERN).format(new Date());
		}

		// 返回表名
		return baseTableName + "_" + date.substring(0, 8);
	}

	/**
	 * 获取今日日期表名
	 * 
	 * @param baseTableName 基础表名
	 * @return 今日日期表名
	 */
	public static String getTodayDateTableName(String baseTableName) {
		// 返回表名
		return baseTableName + "_" + new SimpleDateFormat(DATE_PATTERN).format(new Date());
	}

	/**
	 * 获取指定日期表名
	 * 
	 * @param baseTableName 基础表名
	 * @param time 指定时间
	 * @return 指定月份表名
	 */
	public static String getAssignedDateTableName(String baseTableName, long time) {
		// 返回表名
		return baseTableName + "_" + new SimpleDateFormat(DATE_PATTERN).format(new Date(time));
	}

	/**
	 * 获取指定日期表名
	 * 
	 * @param baseTableName 基础表名
	 * @param date 指定日期
	 * @return 指定日期表名
	 */
	public static String getAssignedDateTableName(String baseTableName, Date date) {
		// 返回表名
		return baseTableName + "_" + new SimpleDateFormat(DATE_PATTERN).format(date);
	}

	/**
	 * 获取指定日期表名
	 * 
	 * @param baseTableName 基础表名
	 * @param date 指定日期
	 * @return 指定日期表名
	 */
	public static String getAssignedDateTableName(String baseTableName, String date) {
		// 返回表名
		if (date == null) {
			return baseTableName + "_" + new SimpleDateFormat(DATE_PATTERN).format(new Date());
		}
		return baseTableName + "_" + date;
	}

}
