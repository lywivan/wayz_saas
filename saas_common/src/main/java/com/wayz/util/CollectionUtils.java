package com.wayz.util;

import java.util.Collection;
import java.util.Map;

/**
 * 
 * <p>集合工具类</p>
 * 
 * @date 2016年5月9日
 * @author mahao
 */
public final class CollectionUtils {

	/**
	 * 
	 * <p>map是空</p>
	 * 
	 * @param map
	 * @return
	 * @date 2016年5月9日
	 * @author mahao
	 */
	public static boolean isEmpty(Map<?, ?> map) {
		return map == null || map.isEmpty();
	}

	/**
	 * 
	 * <p>map不为空</p>
	 * 
	 * @param map
	 * @return
	 * @date 2016年5月9日
	 * @author mahao
	 */
	public static boolean isNotEmpty(Map<?, ?> map) {
		return !isEmpty(map);
	}

	/**
	 * 
	 * <p>集合为空</p>
	 * 
	 * @param c
	 * @return
	 * @date 2016年5月9日
	 * @author mahao
	 */
	public static boolean isEmpty(Collection<?> c) {
		return c == null || c.isEmpty();
	}

	/**
	 * 
	 * <p>集合不为空</p>
	 * 
	 * @param c
	 * @return
	 * @date 2016年5月9日
	 * @author mahao
	 */
	public static boolean isNotEmpty(Collection<?> c) {
		return !isEmpty(c);
	}

}
