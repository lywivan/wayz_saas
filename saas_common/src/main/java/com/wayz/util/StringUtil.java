package com.wayz.util;

/**
 *  字符串工具类 
 * 
 * @author ysm
 */
import java.util.ArrayList;
import java.util.List;

public class StringUtil {

	public static boolean isEmpty(String str) {
		return str == null || str.length() == 0;
	}

	public static boolean notEmpty(String str) {
		return str != null && str.length() > 0;
	}

	public static String[] List2String(List<String> list) {
		String[] node = new String[list.size()];
		for (int i = 0; i < list.size(); i++) {
			node[i] = list.get(i);
		}
		return node;
	}

	public static long toLong(String str) {
		try {
			return Long.parseLong(str);
		}
		catch (Exception e) {
			return 0;
		}
	}

	public static Float toFloat(String str) {
		try {
			return Float.parseFloat(str);
		}
		catch (Exception e) {
			return 0f;
		}
	}

	public static int toInt(String str) {
		try {
			return Integer.parseInt(str);
		}
		catch (Exception e) {
			return 0;
		}
	}

	/**
	 * 判断字符串是否可转换为数字
	 * 
	 * @param str 原字符串
	 * @return 转换后的整型
	 */
	public static boolean isDouble(String str) {
		try {
			Double.parseDouble(str);
		}
		catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * 判断字符串是否可转换为整数
	 * 
	 * @param str 原字符串
	 * @return 转换后的整型
	 */
	public static boolean isInteger(String str) {
		try {
			Integer.parseInt(str);
		}
		catch (Exception e) {
			return false;
		}
		return true;
	}

	public static boolean isNumber(String str) {
		java.util.regex.Pattern pattern = java.util.regex.Pattern.compile("[0-9].*");
		java.util.regex.Matcher match = pattern.matcher(str);
		if (match.matches() == false) {
			return false;
		}
		return true;
	}

	public static List<String> toStringList(String str) {
		List<String> test = new ArrayList<String>();
		for (String t : str.split(",")) {
			test.add(t);
		}
		return test;
		// return Arrays.asList(str.split(","));
	}

	public static List<Integer> toIntList(String str) {
		List<Integer> test = new ArrayList<Integer>();
		for (String t : str.split(",")) {
			test.add(Integer.parseInt(t));
		}
		return test;
	}

	public static String toString(List<?> longList) {
		if (longList == null || longList.isEmpty())
			return "";
		StringBuilder sb = new StringBuilder();
		for (Object aLong : longList) {
			if (sb.length() > 0)
				sb.append(',');
			sb.append(aLong);
		}
		return sb.toString();
	}

	public static String join(char sp, List<?> parameters) {
		StringBuilder sb = new StringBuilder();

		for (Object i : parameters) {
			if (i == null) {
				continue;
			}
			if (sb.length() > 0)
				sb.append(sp);
			sb.append("'").append(i).append("'");
		}
		return sb.toString();
	}

	public static String join(String sp, List<?> parameters) {
		StringBuilder sb = new StringBuilder();

		for (Object i : parameters) {
			if (i == null) {
				continue;
			}
			if (sb.length() > 0)
				sb.append(sp);
			sb.append(i);
		}
		return sb.toString();
	}
}
