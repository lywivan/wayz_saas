package com.wayz.exception;

/**
 * 异常编码枚举
 * 
 * @author yinjy
 *
 */
public enum ExceptionCode {

	/** 字段相关 */
	/** 签名错误 */
	SIGNATURE_ERROR(1, "签名错误"),
	/** 令牌无效 */
	TOKEN_INVALID(2, "令牌无效"),
	/** 权限不足 */
	RIGHT_NOT_ENOUGH(3, "权限不足"),
	/** 参数错误 */
	PARAMETER_ERROR(4, "参数错误"),
	/** 数据不存在 */
	NO_DATA_ERROR(5, "数据不存在"),
	/** 播控器未登记 */
	PLAYER_UNREGISTER(101, "播控未登记"),
	/** RTB播放器编码与媒体位标识不匹配 */
	RTB_UNMATCH(201, "设备编码与媒体位标识不匹配"),
	/** RTB 回调url错误 */
	RTB_NOTICE_ERROR(202, "Notice回调地址解析错误"),
	/** RTB 无广告 */
	RTB_NONE_ADVER(203, "无广告投放"),
	/** RTB 广告数据重复 */
	RTB_DATA_DUPLICATION(204, "竞价广告数据重复"),
	/** 无直播任务 */
	NO_LIVE_PLAN(301, "无直播任务"),
	/** 账号受限 */
	ACCOUNT_TESTING_LIMITED(401, "试用账号受限"),
	/** 账号过期 */
	ACCOUNT_EXPIRED(402, "账号已过期"),
	/** 账号被禁用 */
	ACCOUNT_FORBIDEN(403, "账号被禁用"),
	/** 账号或密码错误 */
	ACCOUNT_VERIFICATION(404, "账号密码错误"),
	/** 未知错误 */
	UNKNOWN_ERROR(9999, "未知错误");

	/** 属性相关 */
	/** 异常编码值 */
	private int value = 0;
	/** 异常编码描述 */
	private String description = null;

	/**
	 * 构造函数
	 * 
	 * @param value 异常编码值
	 * @param description 异常编码描述
	 */
	private ExceptionCode(int value, String description) {
		this.value = value;
		this.description = description;
	}

	/**
	 * 获取异常编码值
	 * 
	 * @return 异常编码值
	 */
	public int getValue() {
		return value;
	}

	/**
	 * 获取异常编码描述
	 * 
	 * @return 异常编码描述
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 获取异常编码描述
	 * 
	 * @param value 异常编码值
	 * @return 异常编码描述
	 */
	public static String getDescription(int value) {
		for (ExceptionCode field : values()) {
			if (field.value == value) {
				return field.description;
			}
		}
		return null;
	}

	/**
	 * 根据值获取异常编码
	 * 
	 * @param value 异常编码值
	 * @return 异常编码
	 */
	public static ExceptionCode fromValue(int value) {
		for (ExceptionCode field : values()) {
			if (field.value == value) {
				return field;
			}
		}
		return null;
	}

	/**
	 * 包含异常编码值
	 * 
	 * @param value 异常编码值
	 * @return 是否包含
	 */
	public static boolean contains(int value) {
		for (ExceptionCode field : values()) {
			if (field.value == value) {
				return true;
			}
		}
		return false;
	}

}