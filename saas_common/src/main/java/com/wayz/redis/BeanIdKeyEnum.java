package com.wayz.redis;

/**
 * @className: BeanIdKeyEnum
 * @description: 缓存中实体类key枚举类
 * 本类为包含实体类自增id得key  除hash（由实体类存储key）之外的所有redis的key
 * @author: 须尽欢_____
 * @date: 2019/7/3
 */
public enum BeanIdKeyEnum {

    // ============================管理后台id部分==================================
    /**管理后台角色key*/
    ROLE_ADMIN("platform:Id:Admin;Role","管理后台角色Id"),
    /**管理后台菜单key*/
    MENU_ADMIN("platform:Id:Admin:Menu","管理后台菜单id"),
    /**版本id*/
    VERSION_ADMIN("platform:Id:Admin:Version","版本id"),
    /**后台用户key*/
    USER_ADMIN("platform:Id:Admin:User","管理后台用户id"),
    /**角色菜单连接set集合key*/
    ROLE_MENU_HREF_ADMIN("platform:List:Admin:RRoleMenuList:","角色菜单对应关系集合"),
    
    //============================Saas 服务id部分=============================================
    /**saas用户key*/
    USER_SAAS("platform:Id:Saas:User","saas用户id"),
    /**saas角色key*/
    ROLE_SAAS("platform:Id:Saas:Role","saas角色id"),
    /**saas菜单key*/
    MENU_SAAS("platform:Id:Saas:Menu","saas菜单id"),
    /**公司key*/
    COMPANY_SAAS("platform:Id:Saas:Company","公司id"),
    /**系统消息通知key*/
    NOTICE_SAAS("platform:Id:Admin:Notice","消息通知id"),
    /**代理商Key*/
    PROXY_SAAS("platform:Id:Saas:ProxyCompany","代理商id"),
    /**代理商用户Key*/
    PROXY_USER_SAAS("platform:Id:Saas:ProxyUser","代理商用户id"),

    /**CIBN公司key*/
    COMPANY_CIBN("platform:Id:CompanyCibn","CIBN公司id"),
    /**CIBN备案快照 key*/
    CIBN_SNAPSHOT("platform:Id:CibnSnapshot","CIBN备案快照id"),

    /**新闻key*/
    WEBSITE_ARTICLE("platform:Id:Admin:websiteArticle","新闻id"),
    /**合作案列key*/
    WEBSITE_CASE("platform:Id:Admin:websiteCase","合作案列id"),

    /**行业key*/
    CATEGORY("platform:Id:Category","行业id"),
    /**场景key*/
    SCENE("platform:Id:Scene","场景id"),

    /**直播渠道key*/
    LIVE_CHANNEL("platform:Id:Admin:LiveChannel","直播渠道id"),
    /**审核方key*/
    AUDITOR("platform:Id:Admin:Auditor","审核方id"),
    /**审核方用户key*/
    AUDITOR_USER("platform:Id:Admin:AuditorUser","审核方用户id"),
    /**直播key*/
    LIVE_PLAN("platform:Id:LivePlan","直播id"),
    /**直播审核key*/
    LIVE_PLAN_AUDIT("platform:Id:LivePlanAudit","直播审核id"),
    /**直播屏幕key*/
    LIVE_PLAN_SCREEN("platform:Id:LivePlanScreen","直播屏幕id"),
    /**直播屏幕布局key*/
    LIVE_PLAN_SCREEN_ITEM("platform:Id:LivePlanScreenItem","直播屏幕布局id"),
    /**直播模板key*/
    LIVE_TEMPLET("platform:Id:LiveTemplet","直播模板id"),
    /**直播截屏key*/
    LIVE_SNAPSHOT("platform:Id:LiveSnapshot","直播截屏id"),
    /**联屏key*/
    JOIN_SCREEN("platform:Id:JoinScreen","联屏id"),
    /**联屏广告key*/
    JOIN_PLAN("platform:Id:JoinPlan","联屏广告id"),
    /**联屏广告屏幕key*/
    JOIN_PLAN_SCREEN_ITEM("platform:Id:JoinPlanScreenItem","联屏广告屏幕id"),
    /**联屏广告设备媒体位key*/
    JOIN_PLAN_SCREEN_POSITION("platform:Id:JoinPlanScreenPosition","联屏广告设备媒体位id"),
    /**联屏广告审核key*/
    JOIN_PLAN_AUDIT("platform:Id:JoinPlanAudit","联屏广告审核id"),
    /**联屏广告素材剪切策略key*/
    JOIN_MATERIAL_CUT_STRATEGY("platform:Id:JoinMaterialCutStrategy","联屏广告素材剪切策略id"),
    /**联屏广告剪切素材key*/
    JOIN_MATERIAL_CUT("platform:Id:JoinMaterialCut","联屏广告剪切素材id"),
    
    /**海报分辨率key*/
    POSTER_RESOLUTION("platform:Id:PosterResolution","海报分辨率id"),
    /**海报类别key*/
    POSTER_CATEGORY("platform:Id:PosterCategory","海报类别id"),
    /**海报模板key*/
    POSTER_TEMPLET("platform:Id:PosterTemplet","海报模板id"),
    
    //============================其它数据id=============================================
    /**登陆验证码图片key*/
    AUTHCODE_ADMIN("platform:String:Admin:SessionAuthCode:","登陆验证码"),
    /**登陆验证码图片key*/
    AUTHCODE_AGENT("platform:String:Agent:SessionAuthCode:","登陆验证码"),
    /** Admin登陆token */
    LOGIN_TOKEN_ADMIN("platform:String:Admin:TokenUser:","登陆token"),
    /** Agent登陆token */
    LOGIN_TOKEN_AGENT("platform:String:Agent:TokenUser:","登陆token"),
    /** Audit登陆token */
    LOGIN_TOKEN_AUDIT("platform:String:Audit:TokenUser:","登陆token");
    
    private String key;
    private String description;

    BeanIdKeyEnum(String key, String description) {
        this.key = key;
        this.description = description;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }}
